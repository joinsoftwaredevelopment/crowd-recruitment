import React from 'react';
import { Route, Switch , Redirect } from 'react-router-dom';
import AdminLayout from '../layouts/AdminLayout';


 const AdminRoutes = () => {

    return (
      <AdminLayout>
        <Switch>
          <Redirect from='*' to='/not-found' /> 
        </Switch>
      </AdminLayout>
    );
 
}

export default  EmptyRoutes ;
