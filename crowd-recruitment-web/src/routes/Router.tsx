import React from "react";
import { Router, Route, Switch } from "react-router-dom";

import Main from "../layouts/EmptyLayout";
import HomeLayout from "../layouts/HomeLayout";
import RegistrationLayout from "../layouts/RegistrationLayout";
import AdminLayout from "../layouts/AdminLayout";
import SourcerLayout from "../layouts/SourcerLayout";
import NewEmployerLayout from "../layouts/NewEmployerLayout";

// Main views
import HomePage from "../features/home/HomePage";
import SourcerHomePage from "../features/home/SourcerHomePage";
import SuccessMeeting from "../features/home/SuccessMeeting";
import LogInPage from "../features/user/LogIn";

// NEW HOMEPAGE
import EmployerHomePage from "../pages/homepage/Employer/EmployerHomePage";

// registration Views

import ThankYouPage from "../features/employer/EmployerThankYou";
import SourcerSignUp from "../features/sourcer/SignUp";
import EmployerSignUp from "../features/employer/SignUp";

import Experience from "../features/sourcer/Experience";
import JobTitle from "../features/sourcer/JobTitle";
import Access from "../features/sourcer/Access";
import Industry from "../features/sourcer/Industry";
import UploadResume from "../features/sourcer/UploadResume";

import { history } from "../helpers/history";
import ForgetPassword from "../features/user/ForgetPassword";
import EnterEmail from "../features/user/EnterEmail";

// ADMIN

import SourcerRequest from "../features/admin/SourcerRequests";
import EmployerRequest from "../features/admin/EmployerRequest";
import Info from "../features/admin/Info";
import LogIn from "../features/admin/LogIn";
import JobRequests from "../features/admin/JobRequests";
import SourcerBalance from "../features/admin/SourcerBalance";
import ReviewSourcerCandidates from "../features/admin/ReviewSourcerCandidates";
import PackagesListing from "../features/admin/PackagesListing";

// SOURCER
import MyJobs from "../features/sourcer/MyJobs";
import MySubmissions from "../features/sourcer/MySubmissions";
import MyDrafts from "../features/sourcer/MyDrafts";
import SourcerBalances from "../features/sourcer/SourcerBalances";
import EmployerProfile from "../features/employer/Profile";

// EMPLOYER
import SelectTemplate from "../features/employer/SelectTemplate";
import JobPostDetails from "../features/employer/JobPostDetails";
import JobPostInstructions from "../features/employer/JobPostInstructions";
import JobPostEngagment from "../features/employer/JobPostEngagment";
import JobPostObjective from "../features/employer/JobPostObjective";
import MyjobsEmployer from "../features/employer/MyjobsEmployer";
import JobPreview from "../features/employer/JobPreview";
import JobDrafts from "../features/employer/JobDrafts";
import JobClosed from "../features/employer/JobClosed";
import BookMeeting from "../features/employer/BookMeeting";
import ConfirmMeeting from "../features/employer/ConfirmMeeting";
import SourcerProfile from "../features/sourcer/Profile";
// EMPLOYER NEW
import SignUpEmployer from "../pages/employer/SignUp/SignUp";
import LoginEmployer from "../pages/employer/Login/Login";
import Dashboard from "../pages/employer/Dashboard/Dashboard";
import JobPreviewEmployer from "../pages/employer/JobPreview/JobPreview";
import SelectTemplateEmployer from "../pages/employer/JobTemplate/JobTemplate";
import EmployerJobDetails from "../pages/employer/JobPosting/JobDetails/JobDetails";
import EmployerJobInstruction from "../pages/employer/JobPosting/JobInstruction/JobInstruction";
import EmployerJobObjectives from "../pages/employer/JobPosting/JobObjectives/JobObjectives";
import EmployerJobEngagment from "../pages/employer/JobPosting/JobEngagment/JobEngagment";
import ProfileEmployer from "../pages/employer/Profile/Profile";
import EmployerForgotPassword from "../pages/employer/ForgotPassword/ForgotPassword";
import EmployerCreatePassword from "../pages/employer/CreatePassword/CreatePassword";
import CandidateInterview from "../pages/employer/CandidateInterview/CandidateInterview";

// CANDIDATENEW
import EmployerJobBoard from "../pages/candidate/JobBoard/JobBoard";

// CANDIDATES

import Candidates from "../features/candidate/Candidates";
import Summary from "../features/candidate/Summary";
import testParsing from "../features/candidate/testParsing";

import AdminDashboard from "../features/admin/AdminDashboard";
import EmployerDashboard from "../features/employer/EmployerDashboard";

import NotFound from "../features/home/NotFound";

import testDoc from "../features/sourcer/testDoc";
import JobDetails from "../features/sourcer/JobDetails";
import Calendar from "../features/home/Calendar";
import HomePageArabic from "../features/home/HomePageArabic";
import ReviewCandidate from "../features/admin/ReviewCandidate";
import JobBoard from "../features/home/JobBoard";
import Invoice from "../features/home/Invoice";

// COMMON
import EmailVerified from "../pages/common/EmailVerified";
import ThankYou from "../pages/common/ThankYou";
import ChangeEmail from "../pages/common/ChangeEmail";
import RecoverEmail from "../pages/common/RecoverEmail";

const Routes = () => {
  return (
    <Router history={history}>
      <Switch>
        <Route path="/login" component={LogInPage} />
        <Route path="/employer-signup" component={SignUpEmployer} />
        <Route path="/forgetPassword/:uuid" component={ForgetPassword} />
        <Route path="/enterEmail" component={EnterEmail} />
        <Route path="/successMeeting" component={SuccessMeeting} />
        <Route path="/admin/login" component={LogIn} />
        <Route path="/calendar" component={Calendar} />

        <Route path="/registration/:path?" exact>
          <RegistrationLayout>
            <Switch>
              {/* <Route path="/thankyou" component={ThankYouPage} /> */}
              {/* <Route
                path="/registration/employer-signup"
                component={EmployerSignUp}
              /> */}
              {/* <Route
                path="/registration/employer-signup"
                component={SignUpEmployer}
              /> */}

              <Route
                path="/registration/sourcer-signup"
                component={SourcerSignUp}
              />
              <Route
                path="/registration/sourcer-experience"
                component={Experience}
              />
              <Route
                path="/registration/sourcer-jobtitle"
                component={JobTitle}
              />
              <Route path="/registration/sourcer-access" component={Access} />
              <Route
                path="/registration/sourcer-industry"
                component={Industry}
              />
              <Route
                path="/registration/sourcer-upload-resume"
                component={UploadResume}
              />
            </Switch>
          </RegistrationLayout>
        </Route>

        <Route path="/employer/:path?/:id?/:tempId?" exact>
          <NewEmployerLayout>
            <Switch>
              <Route path="/employer/dashboard/:id?" component={Dashboard} />
              <Route
                path="/employer/JobPreviewEmployer/:id?"
                component={JobPreviewEmployer}
              />
              <Route
                path="/employer/SelectTemplateEmployer"
                component={SelectTemplateEmployer}
              />
              <Route
                path="/employer/EmployerJobDetails/:id?/:tempId?"
                component={EmployerJobDetails}
              />
              <Route
                path="/employer/EmployerJobInstructions/:id?"
                component={EmployerJobInstruction}
              />
              <Route
                path="/employer/EmployerJobObjectives/:id?"
                component={EmployerJobObjectives}
              />
              <Route
                path="/employer/EmployerJobEngagment/:id?"
                component={EmployerJobEngagment}
              />
              <Route
                path="/employer/ProfileEmployer/:id?"
                component={ProfileEmployer}
              />
              <Route
                path="/employer/CandidateInterview/:id?"
                component={CandidateInterview}
              />
              <Route
                path="/employer/EmpjobBoard/:id?"
                exact
                component={EmployerJobBoard}
              />
            </Switch>
          </NewEmployerLayout>
        </Route>

        <Route path="/admin/:path?/:id?" exact>
          <AdminLayout>
            <Switch>
              <Route path="/admin/sourcer-request" component={SourcerRequest} />
              <Route
                path="/admin/employer-request"
                component={EmployerRequest}
              />
              <Route path="/admin/sourcer-info/:id" component={Info} />
              <Route path="/admin/admin-dashboard" component={AdminDashboard} />

              <Route path="/admin/job-requests" component={JobRequests} />

              <Route
                path="/admin/review-sourcers/:id"
                component={ReviewSourcerCandidates}
              />
              <Route
                path="/admin/review-candidate/:id"
                component={ReviewCandidate}
              />
              <Route path="/admin/sourcer-balance" component={SourcerBalance} />
              <Route path="/admin/packages" component={PackagesListing} />
            </Switch>
          </AdminLayout>
        </Route>

        <Route path="/sourcer/:path?/:id?" exact>
          <SourcerLayout>
            <Switch>
              <Route path="/sourcer/myJobs" component={MyJobs} />
              <Route
                path="/sourcer/mySubmissions/:id"
                component={MySubmissions}
              />
              <Route path="/sourcer/myDrafts" component={MyDrafts} />
              <Route path="/sourcer/testDoc" component={testDoc} />
              <Route path="/sourcer/jobDetails/:id" component={JobDetails} />
              <Route
                path="/sourcer/SourcerBalances"
                component={SourcerBalances}
              />
              <Route path="/sourcer/Profile/:uuid" component={SourcerProfile} />
            </Switch>
          </SourcerLayout>
        </Route>

        <Route path="/candidate/:path?/:id?" exact>
          <SourcerLayout>
            <Switch>
              <Route
                path="/candidate/jobBoard/:id?"
                exact
                component={JobBoard}
              />
              <Route path="/candidate/candidates/:id?" component={Candidates} />
              <Route path="/candidate/summary/:id?" component={Summary} />

              <Route path="/candidate/testParsing/" component={testParsing} />
            </Switch>
          </SourcerLayout>
        </Route>

        {/* <Route path="/employer/:path?/:id?/:tempId?" exact>
          <SourcerLayout>
            <Switch>
              <Route
                path="/employer/JobPostDetails/:id?/:tempId?"
                component={JobPostDetails}
              />
              <Route
                path="/employer/JobPostObjective/:id?"
                component={JobPostObjective}
              />
              <Route
                path="/employer/JobPostInstructions/:id?"
                component={JobPostInstructions}
              />
              <Route
                path="/employer/JobPostEngagment/:id?"
                component={JobPostEngagment}
              />
              <Route
                path="/employer/MyjobsEmployer"
                component={MyjobsEmployer}
              />
              <Route
                path="/employer/SelectTemplate"
                component={SelectTemplate}
              />

              <Route path="/employer/JobPreview/:id" component={JobPreview} />
              <Route path="/employer/dashboard" component={EmployerDashboard} />
              <Route path="/employer/JobDrafts" component={JobDrafts} />
              <Route path="/employer/JobClosed" component={JobClosed} />
              <Route
                path="/employer/bookMeeting/:id?"
                component={BookMeeting}
              />

              <Route
                path="/employer/ConfirmMeeting/:id?"
                component={ConfirmMeeting}
              />
              <Route
                path="/employer/Profile/:uuid"
                component={EmployerProfile}
              />
            </Switch>
          </SourcerLayout>
        </Route> */}

        <Route
          path="/EmployerCreatePassword/:id?"
          exact
          component={EmployerCreatePassword}
        />
        <Route
          path="/EmployerForgotPassword"
          exact
          component={EmployerForgotPassword}
        />
        <Route path="/EmployerSignup" exact component={SignUpEmployer} />
        <Route path="/EmployerLogin" exact component={LoginEmployer} />
        <Route path="/EmailVerified/:id?" exact component={EmailVerified} />
        <Route path="/ThankYou/:id?" exact component={ThankYou} />
        <Route path="/RecoverEmail" exact component={RecoverEmail} />
        <Route path="/ChangeEmail/:id?" exact component={ChangeEmail} />
        {/* <Route path={["/", "/home"]} exact component={HomePage} /> */}
        <Route path={["/arhome"]} exact component={HomePageArabic} />
        <Route path={["/sourcerhome"]} exact component={SourcerHomePage} />
        <Route path={["/invoice"]} exact component={Invoice} />
        <Route
          path={["/", "/employerHome"]}
          exact
          component={EmployerHomePage}
        />
        {/* <Route path={["/jobBoard"]} exact component={JobBoard} /> */}
        <Route path="" component={NotFound} />
      </Switch>
    </Router>
  );
};

export default Routes;
