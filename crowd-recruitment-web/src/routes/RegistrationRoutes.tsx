import React, { useState, useEffect } from "react";
import { Route, Switch, Redirect } from "react-router-dom";
import RegistrationLayout from "../layouts/RegistrationLayout";
import EmployerThankYou from "../features/employer/EmployerThankYou";
import SourcerSignUp from "../features/sourcer/SignUp";
import EmployerSignUp from "../features/employer/SignUp";
import EmployerSignupForm from "../features/employer/EmployerSignupForm";

import Experience from "../features/sourcer/Experience";
import JobTitle from "../features/sourcer/JobTitle";
import Access from "../features/sourcer/Access";
import Industry from "../features/sourcer/Industry";
import UploadResume from "../features/sourcer/UploadResume";

import LogInForm from "../features/user/LogIn";
import EmptyLayout from "../layouts/EmptyLayout";
import LogIn from "../features/user/LogIn";
import ForgetPassword from "../features/user/ForgetPassword";

const RegistrationRoute = () => {
  return (
    <RegistrationLayout>
      <Switch>
        {/* <Route path="/employer/thankyou" component={EmployerThankYou} /> */}
        <Route path="/employer/SignUp" component={EmployerSignUp} />
        <Route path="/sourcer/SignUp" component={SourcerSignUp} />
        <Route path="/sourcer/Experience" component={Experience} />
        <Route path="/sourcer/Jobtitle" component={JobTitle} />
        <Route path="/sourcer/Access" component={Access} />
        <Route path="/sourcer/Industry" component={Industry} />
        <Route path="/sourcer/UploadResume" component={UploadResume} />
        <Route path="/" component={EmptyLayout}>
          {/* <Route component={LogIn}></Route> */}
          <Route component={ForgetPassword}></Route>
        </Route>
        {/* <Route path="/empty/login" component={LogInForm} /> */}
      </Switch>
    </RegistrationLayout>
  );
};

export default RegistrationRoute;
