import React from "react";
import { Route, Switch, Redirect } from "react-router-dom";
import EmptyLayout from "../layouts/EmptyLayout";

import HomePage from "../features/home/HomePage";
import LogInForm from "../features/user/LogIn";
import CreateNewPassword from "../features/user/ForgetPassword";

const EmptyRoutes = () => {
  return (
    <EmptyLayout>
      <Switch>
        <Route path="/employerHome" component={HomePage} />
        {/* <Route path="/login" component={LogInForm} /> */}
        <Route path="/createNewPassword" component={CreateNewPassword} />
        <Redirect from="*" to="/not-found" />
      </Switch>
    </EmptyLayout>
  );
};

export default EmptyRoutes;
