import baseUrl from "../helpers/InitAxios";
import { ISourcerModel } from "../models/ISourcerModel";
import { authHeader ,authHeaderWithFormData } from '../helpers/auth-header';
import { ISourcerExperienceModel } from "../models/ISourcerExperienceModel";
import { ISourcerJobModel } from "../models/ISourcerJobModel";
import { ISourcerAccessWebSitesModel } from "../models/ISourcerWebsiteModel";
import { ISourcerIndustryModel } from "../models/ISourcerIndustryModel";
import { ISourcerBasicInfo } from "../models/ISourcerBasicInfo";
import { ISourcerCareerInfo } from "../models/ISourcerCareerInfo";
import { ISourcerCVModel } from "../models/ISourcerCVModel";
import { ISourcerDetails } from "../models/ISourcerDetails";
import { IJobSummary } from "../models/IJobSummary";
import { IUserRequest } from "../models/IUserRequest";

const register = (sourcer : ISourcerModel) => {
  debugger;
  return baseUrl.post("/Sourcer/create-sourcer", sourcer);
}

const sourcerExperience = (sourcer : ISourcerExperienceModel) => {
  debugger;
  return baseUrl.post("/Sourcer/update-sourcer-experience", sourcer , { headers: authHeader()});
}

const sourcerJobTitle = (sourcer : ISourcerJobModel) => {
  debugger;
  return baseUrl.post("/Sourcer/update-sourcer-jobtitle", sourcer, { headers: authHeader()});
}

const sourcerWebsite = (sourcer : ISourcerAccessWebSitesModel) => {
  debugger;
  return baseUrl.post("/Sourcer/update-sourcer-websites", sourcer, { headers: authHeader()});
}

const sourcerIndustry = (sourcer : ISourcerIndustryModel) => {
  debugger;
  return baseUrl.post("/Sourcer/update-sourcer-industry", sourcer, { headers: authHeader()});
}

const sourcerCV = (sourcer:FormData | null) => {
  debugger;
  return baseUrl.post("/Sourcer/update-sourcer-cv", sourcer, { headers: authHeader()});
}

// Admin Acceptance
const getAll = (userStatusId:number) => {
  return baseUrl.get("/ManageSourcer/GetAllSourcers?userStatusId="+userStatusId, { headers: authHeader()});
};

const getSourcerInfo = (uuid:string) => {
  debugger;
  return baseUrl.get("/ManageSourcer/GetSourcerById?uuid="+uuid);
};

const acceptSourcer = (sourcer:IUserRequest) => {
  debugger;
  return baseUrl.post("/ManageSourcer/AcceptSourcer",sourcer);
};

const rejectSourcer = (sourcer:IUserRequest) => {
  debugger;
  return baseUrl.post("/ManageSourcer/DeclineSourcer",sourcer);
};


const openPdf = (fileName:string) => {
  debugger;
  return baseUrl.get("/Sourcer/open-pdf?fileName="+fileName);
};

const getSourcerBalances = (sourcerUuid:string) => {
  debugger;
  return baseUrl.get("/Sourcer/GetSourcersBalances?sourcerUuid="+sourcerUuid);
};

const getSourcerBalancesSummary = (sourcerUuid:string) => {
  debugger;
  return baseUrl.get("/Sourcer/GetSourcersBalanceSummary?sourcerUuid="+sourcerUuid);
};

const GetSourcerCandidates = (sourcerUuId:string , jobUuId : string) => {
  debugger;
  return baseUrl.get("/Candidate/GetSourcerCandidateBySourcerUuId", { headers: authHeader(),
    params: {
      sourcerUuId: sourcerUuId,
      jobUuId : jobUuId,
    }});
};

const GetSourcerCandidatesByStatus = (sourcerUuId:string, submissionStatusId : number, jobUuId : string) => {
  debugger;
    return baseUrl.get("/Candidate/GetSourcerCandidateBySubmissionStatusId", {
        headers: authHeader(),
        params: {
          sourcerUuId: sourcerUuId,
          submissionStatusId : submissionStatusId,
          jobUuId : jobUuId
        }
      });
  };

  const retrieveJobExperience = () => {
    debugger;
    return baseUrl.get("/Sourcer/get-sourcer-experienceId", { headers: authHeader()});
  };

  const GetJobtitleName = () => {
    debugger;
    return baseUrl.get("/Sourcer/get-sourcer-jobTitle", { headers: authHeader()});
  };

  const GetSourcerhasAccessToWebsite = () => {
    debugger;
    return baseUrl.get("/Sourcer/get-sourcer-website", { headers: authHeader()});
  };

  const GetSourcerIndustryId = () => {
    debugger;
    return baseUrl.get("/Sourcer/get-sourcer-industryId", { headers: authHeader()});
  };
  
  const getPdf = (fileName:string) => {
    debugger;
    return baseUrl.get("/Sourcer/open-pdf2?fileName="+fileName,{responseType: 'blob'});
  };

  const GetBasicInfo = (uuid: string) => {
    debugger;
    return baseUrl.get("/SourcerProfile/GetBasicInfo?uuid="+uuid, { headers: authHeader()});
  };

  const UpdateBasicInfo = (sourcerBasicInfo: ISourcerBasicInfo) => {
    debugger;
    return baseUrl.post("/SourcerProfile/UpdateBasicInfo",sourcerBasicInfo, { headers: authHeader()});
  };

  const UpdateCareerInfo = (sourcerCareerInfo: ISourcerCareerInfo) => {
    sourcerCareerInfo.industryId = Number(sourcerCareerInfo.industryId);
    sourcerCareerInfo.hasAccessToDifferentWebsite = Boolean(sourcerCareerInfo.hasAccessToDifferentWebsite);
    sourcerCareerInfo.experienceTypeId = Number(sourcerCareerInfo.experienceTypeId);
    debugger;
    return baseUrl.post("/SourcerProfile/UpdateCareerInfo",sourcerCareerInfo, { headers: authHeader()});
  };

  const GetCareerInfo = (uuid: string) => {
    debugger;
    return baseUrl.get("/SourcerProfile/GetCareerInfo?uuid="+uuid, { headers: authHeader()});
  };

export const sourcerService = {
    register,
    sourcerExperience,
    sourcerJobTitle ,
    sourcerWebsite ,
    sourcerIndustry,
    sourcerCV,
    getAll,
    getSourcerInfo,
    acceptSourcer,
    openPdf,
    getSourcerBalances,
    getSourcerBalancesSummary,
    //GetSourcerCandidates,
    rejectSourcer,
    GetSourcerCandidatesByStatus,
    retrieveJobExperience,
    GetJobtitleName,
    GetSourcerhasAccessToWebsite,
    GetSourcerIndustryId,
    getPdf,
    GetBasicInfo,
    UpdateBasicInfo,
    UpdateCareerInfo,
    GetCareerInfo
};

export default sourcerService;
