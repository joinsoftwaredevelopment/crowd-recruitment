import { authHeader } from "../helpers/auth-header";
import http from "../helpers/InitAxios";
import { IAcceptJob } from "../models/IAcceptJob";
import { IDeclineJob } from "../models/IDeclineJob";
import { IUpdateCandidateCvStatusModel } from "../models/IUpdateCandidateCvStatusModel";
import { IUpdateHiringStageModel } from "../models/IUpdateHiringStageModel";
import { IUpdateIsHiredModel } from "../models/IUpdateIsHiredModel";
import { IUpdateIsAcceptOfferModel } from "../models/IUpdateIsAcceptOfferModel";
import { IInterviewDetailsModel } from "../models/IInterviewDetailsModel";


const GetCandidateStatusHistory = (candidateUuId : string) => {
    debugger;
    return http.get("/Candidate/CandidateStatusHistory?candidateUuId="+candidateUuId, { headers: authHeader()});
  }

  const UpdateCandidateCvStatus = (UpdateCandidateCvStatusModel:IUpdateCandidateCvStatusModel) => {
    debugger;
    return http.post("/Candidate/Update-CandidateCv-Status",UpdateCandidateCvStatusModel, { headers: authHeader()});
  };

  const UpdateHiringStageStatus = (updateStageModel:IUpdateHiringStageModel) => {
    debugger;
    return http.post("/Candidate/update-hiring-stage",updateStageModel, { headers: authHeader()});
  };
  
  const UpdateIsHired = (IsHiredModel:IUpdateIsHiredModel) => {
    debugger;
    return http.post("/Candidate/update-isHired",IsHiredModel, { headers: authHeader()});
  };

  const UpdateIsAcceptOffer = (IsAcceptOfferModel:IUpdateIsAcceptOfferModel) => {
    debugger;
    return http.post("/Candidate/update-isAccept-offer",IsAcceptOfferModel, { headers: authHeader()});
  };

  const AddInterviewDetails = (InterviewDetailsModel:IInterviewDetailsModel) => {
    debugger;
    return http.post("/Interview/add-interview-details",InterviewDetailsModel, { headers: authHeader()});
  };

  const GetAllTemplates = () => {
    debugger;
    return http.get("/Interview/get-interview-temps", { headers: authHeader()});
  }

  const GetAllOfferTemplates = () => {
    debugger;
    return http.get("/Interview/get-offer-temps", { headers: authHeader()});
  }


  const GetMeetingDetails = (interviewId : number) => {
    debugger;
    return http.get("/Interview/get-interview-details-byId?interviewId="+interviewId, { headers: authHeader()});
  }

  const CancelInterview = (interviewId: number,isCanceled: boolean) => {
    debugger;
    return http.get("/Interview/cancel-interview?interviewId="+interviewId+"&isCanceled="+isCanceled, { headers: authHeader()});
  }

  const GetAllInterviews = () => {
    debugger;
    return http.get("/Interview/get-all-interviews", { headers: authHeader()});
  }

  const GetAllInterviewsByUuid = (uuid: string) => {
    debugger;
    return http.get("/Interview/get-all-interviews-byUuid?uuid="+uuid, { headers: authHeader()});
  }

  const SendOffer = (offerData:FormData) => {
    debugger;
      return http.post("/Interview/add-offer",offerData, {
        headers: {
          "content-type": "multipart/form-data",
        },
      });
    };  
  
  export default {
    GetCandidateStatusHistory,
    UpdateCandidateCvStatus,
    UpdateHiringStageStatus,
    UpdateIsHired,
    UpdateIsAcceptOffer,
    AddInterviewDetails,
    GetAllTemplates,
    CancelInterview,
    GetAllOfferTemplates,
    GetMeetingDetails,
    GetAllInterviews,
    SendOffer,
    GetAllInterviewsByUuid
  };