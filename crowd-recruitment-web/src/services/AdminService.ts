import { ISourcerPaymentModel } from '../models/ISourcerPaymentModel';
import http from "../helpers/InitAxios";
import { IUser } from "../models/IUser";
import { authHeader } from "../helpers/auth-header";
import { IUserRequest } from "../models/IUserRequest";
import { IJobCandidate } from "../models/IJobCandidate";


const logIn = (user:IUser) => {
  return http.post("/Admin/LogIn", user);
};

const logout = () => {
  localStorage.removeItem("user");
};

const sourcerBalances = () => {
  debugger;
  return http.get("/ManageSourcer/GetVwAdminSourcersBalance", { headers: authHeader()});
}


const acceptCandidate = (userRequestViewModel:IUserRequest) => {
  debugger;
  return http.post("/Candidate/AcceptCandidate",userRequestViewModel, { headers: authHeader()});
};

const rejectCandidate = (userRequestViewModel:IUserRequest) => {
  debugger;
  return http.post("/Candidate/DeclineCandidate",userRequestViewModel, { headers: authHeader()});
};

const SendMail = (candidate:IJobCandidate) => {
  debugger;
  return http.post("/Candidate/SendMail",candidate, { headers: authHeader()});
};

const GetJobSourcers = (jobUuId:string) => {
  debugger;
  return http.get("/Admin/GetJobSourcers?jobUuId="+jobUuId, { headers: authHeader()});
};

const sourcerBalancesByWeek = (startDate : Date,endDate : Date) => {
  debugger;
  return http.get("/ManageSourcer/GetVwAdminSourcersBalanceByWeek", {
    headers: authHeader(),
    params: {
      startDate: startDate,
      endDate : endDate
        }
  });}

  const UpdateAdminSourcersBalance = (sourcerPayment:ISourcerPaymentModel) => {
    debugger;
    return http.post("/ManageSourcer/UpdateAdminSourcersBalance",sourcerPayment, { headers: authHeader()});
  };


  export default {
    logIn,
    logout,
    sourcerBalances,
    GetJobSourcers,
    acceptCandidate,
    rejectCandidate,
    SendMail,
    sourcerBalancesByWeek,
    UpdateAdminSourcersBalance
  };