import http from "../helpers/InitAxios";
import { authHeader } from "../helpers/auth-header";
import { IPackage } from "../models/IPackage";

const AddPackage = (pricePackage :IPackage) => {
  debugger;
  return http.post("/Packages/AddPackage",pricePackage, { headers: authHeader()});
};

const GetPackageById = (packageId :number) => {
  debugger;
  return http.get("/Packages/GetPackageById?packageId="+packageId, { headers: authHeader()});
};

const EditPackage = (pricePackage :IPackage) => {
  debugger;
  return http.post("/Packages/EditPackage",pricePackage, { headers: authHeader()});
};

const DeletePackage = (packageId : number) => {
  debugger;
  return http.get("/Packages/DeletePackage?packageId="+packageId, { headers: authHeader()});
};

const GetAllPackages = () => {
  debugger;
  return http.get("/Packages/GetAllPackages", { headers: authHeader()});
};

  export default {
    AddPackage,
    GetPackageById,
    EditPackage,
    DeletePackage,
    GetAllPackages
  };