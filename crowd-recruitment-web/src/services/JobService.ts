import http from "../helpers/InitAxios";
import { IPostJobDetails } from "../models/IPostJobDetails";
import { IPostJobInstructions } from "../models/IPostJobInstructions";
import { IPostJobEngagment } from "../models/IPostJobEngagment";
import { IEmployerObjectives } from "../models/IEmployerObjectives";
import { IJobSummary } from "../models/IJobSummary";
import { IBookMeeting } from "../models/IBookMeeting";
import { ICloseJob } from "../models/ICloseJob";
import { authHeader } from "../helpers/auth-header";
import { number } from "yup/lib/locale";

const create = (job:IPostJobDetails) => {
  debugger;
  if(job.jobNationalityId == 0){
    job.jobNationalityId = 189;
  }

  if(job.seniorityLevelId == 0){
    job.seniorityLevelId = 1;
  }

  if(job.jobLocationId == 0){
    job.jobLocationId = 1;
  }

  return http.post("/Employer/CreateJob", job, { headers: authHeader()});
};

const createInstructions = (job:IPostJobInstructions) => {
  debugger;
  if(job.experienceLevelId == null){
    job.experienceLevelId = 1;
  }

  if(job.maxSalary == null){
    job.maxSalary = 0;
  }

  if(job.minSalary == null){
    job.minSalary = 0;
  }

  if(job.currencyId == null){
    job.currencyId = 122;
  }

  return http.post("/Employer/UpdateJobInstructions", job, { headers: authHeader()});
};

const createObjectives = (job:IEmployerObjectives) => {
  job.peopleToHireCount = Number(job.peopleToHireCount);
  if(job.peopleToHireCount == 0){
    job.peopleToHireCount = 1;
  }
  job.whenYouNeedToHire = Number(job.whenYouNeedToHire);
  
  job.isContinuousHire = Boolean(job.isContinuousHire);
  job.sourcingOutsideOfJoin = Boolean(job.sourcingOutsideOfJoin);


  debugger;
  return http.post("/Employer/UpdateJobObjectives", job, { headers: authHeader()});
};

const createEngagment = (job:IPostJobEngagment) => {
  debugger;
  return http.post("/Employer/UpdateJobEngagment", job, { headers: authHeader()});
};

const CloseJob = (job:ICloseJob) => {
  debugger;
  return http.post("/Employer/CloseJob", job, { headers: authHeader()});
};

const DeleteJob = (uuid: string) => {
  debugger;
  return http.get("/Employer/DeleteJob?uuid="+uuid, { headers: authHeader()});
};

const allJobsSummaries = () => {
  debugger;
  return http.get("/Job/job-summaries", { headers: authHeader()});
}

const getallJobsSummariesByEmp = () => {
  return http.get("/Employer/job-summaries", { headers: authHeader()});
};

const getallJobsDraftsByEmp = () => {
  return http.get("/Employer/job-drafts", { headers: authHeader()});
};

const getallJobsClosedByEmp = () => {
  return http.get("/Employer/job-closed", { headers: authHeader()});
};

const GetAllJobs = () => {
  return http.get("/Employer/GetAllJobs", { headers: authHeader()});
};

const sourcerJobsSummaries = (isActive : boolean ) => {
  debugger;
  return http.get("/Job/sourcer-job-summaries?isActive="+isActive, { headers: authHeader()});
}

const sourcerJobsSubmissions = (jobUuId : string ) => {
  debugger;
  return http.get("/Job/sourcer-job-submissions?jobUuId="+jobUuId, { headers: authHeader()});
}

const getJobDetails = (jobUuId : string ) => {
  debugger;
  return http.get("/Job/get-job-details?jobUuId="+jobUuId, { headers: authHeader()});
}

const addSourcerSubmission = (sourcer:FormData | null) => {
  debugger;
  return http.post("/Job/add-sourcer-submission", sourcer, { headers: authHeader()});
}

const getJobCandidates = (jobUuId : string ) => {
  debugger;
  return http.get("/Candidate/get-job-candidates?jobUuId="+jobUuId, { headers: authHeader()});
}
const getAcceptedJobCandidates = (jobUuId : string ) => {
  debugger;
  return http.get("/Candidate/get-accepted-job-candidates?jobUuId="+jobUuId, { headers: authHeader()});
}

const getJobCandidateByUuId = (candidateUuId : string ) => {
  debugger;
  return http.get("/Candidate/get-job-candidate-by-uuId?candidateUuId="+candidateUuId, { headers: authHeader()});
}

const createMeeting = (job:IBookMeeting) => {
  debugger;
  job.interviewerId = Number(job.interviewerId);
  return http.post("/User/BookMeeting", job);
};

const getAllCountries = () => {
  debugger;
  return http.get("/user/GetCountries", { headers: authHeader()});
}

const getAllCurrencies = () => {
  debugger;
  return http.get("/user/GetCurrencies", { headers: authHeader()});
}

const GetIndustries = () => {
  debugger;
  return http.get("/Job/GetAllIndustries", { headers: authHeader()});
}

const GetAllJobTemps = () => {
  debugger;
  return http.get("/Job/GetAllJobTemps", { headers: authHeader()});
}

const GetTemplatesByIndustry = (industryId : number) => {
  debugger;
  return http.get("/Job/GetTempByIndustryId?industryId="+industryId, { headers: authHeader()});
}

const GetTemplateById = (templateId : number) => {
  debugger;
  return http.get("/Job/GetJobTempById?templateId="+templateId, { headers: authHeader()});
}

export default {
    create,
    createInstructions,
    createEngagment,
    allJobsSummaries,
    sourcerJobsSummaries,
    sourcerJobsSubmissions,
    addSourcerSubmission,
    createObjectives,
    getallJobsSummariesByEmp,
    getJobDetails,
    getallJobsDraftsByEmp,
    getJobCandidates,
    getAcceptedJobCandidates,
    getJobCandidateByUuId,
    createMeeting,
    CloseJob,
    getallJobsClosedByEmp,
    getAllCountries,
    getAllCurrencies,
    GetIndustries,
    GetTemplatesByIndustry,
    GetTemplateById,
    GetAllJobTemps,
    DeleteJob,
    GetAllJobs
};