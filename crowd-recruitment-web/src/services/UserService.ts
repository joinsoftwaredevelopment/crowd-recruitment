import http from "../helpers/InitAxios";
import { IUser } from "../models/IUser";
import { IRsetPassword } from "../models/IRsetPassword";
import { IUpdatePassword } from "../models/IUpdatePassword";
import { authHeader ,authHeaderWithFormData } from '../helpers/auth-header';


const logIn = (user:IUser) => {
  return http.post("/User/LogIn", user);
};

const SendResetEmail = (user:IUser) => {
  return http.post("/User/Send-Reset-Email", user);
};

const ResePassword = (user:IRsetPassword) => {
  return http.post("/User/Rese-Password", user);
};

const UpdatePassword = (updatePassword: IUpdatePassword) => {
  return http.post("/User/UpdatePassword", updatePassword, { headers: authHeader()});
};

const logout = () => {
  localStorage.removeItem("user");
};

const verifyEmailAddress = (uuId:string) => {
  return http.post("/User/verify-email-address?uuid="+uuId);
};

const GetUserByUuId = (uuId:string) => {
  return http.get("/User/GetUserByUuId?uuid="+uuId);
};

const resendEmail = (uuid:string) => {
  return http.post("/User/resend-email?uuid="+uuid);
};

const changeEmail = (uuid:string , email : string) => {
  return http.post("/User/change-email?uuid="+uuid + "&email=" + email);
};

export default {
  logIn,
  logout,
  SendResetEmail,
  ResePassword,
  UpdatePassword,
  verifyEmailAddress,
  GetUserByUuId,
  resendEmail,
  changeEmail
};