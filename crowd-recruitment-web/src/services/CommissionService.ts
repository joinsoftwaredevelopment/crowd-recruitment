import { authHeader } from "../helpers/auth-header";
import http from "../helpers/InitAxios";
import { IAcceptJob } from "../models/IAcceptJob";
import { IDeclineJob } from "../models/IDeclineJob";

const GetAllJobs = (statusId : number) => {
    debugger;
    return http.get("/Commission/GetAllJobs?statusId="+statusId, { headers: authHeader()});
  }

  const AcceptJob = (job:IAcceptJob) => {
    debugger;
    return http.post("/Commission/AcceptJob",job);
  };

  const DeclineJob = (job:IDeclineJob) => {
    debugger;
    return http.post("/Commission/DeclineJob",job);
  };

  export default {
    GetAllJobs,
    AcceptJob,
    DeclineJob
  };