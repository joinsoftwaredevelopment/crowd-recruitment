import React, { useEffect, useState } from "react";
import { RootStateOrAny, useDispatch, useSelector } from "react-redux";
import { history } from "./helpers/history";
import { alertActions } from "./actions";
import { Redirect, Route, Switch } from "react-router-dom";
//Test comment
import AllRoutes from "./Routes";
import LogIn from "./features/user/LogIn";
import EmptyLayout from "./layouts/EmptyLayout";
import HomePage from "./features/home/HomePage";
import SourcerSignUp from "./features/sourcer/SignUp";
import RegistrationLayout from "./layouts/RegistrationLayout";
import Routes from "./routes/Router";
import Alert from "./components/Alerts/Alert";

import alertSuccess from "./assets/alert-success.png";
import { IJobSummary } from "./models/IJobSummary";
import { jobActions } from "./actions/jobActions";

function App() {
  const alert = useSelector((state: RootStateOrAny) => state.alert);
  const dispatch = useDispatch();
  const closeAlert = () => {
    dispatch(alertActions.clear());
  };

  useEffect(() => {
    history.listen((location, action) => {
      // clear alert on location change
      dispatch(alertActions.clear());
    });
  }, []);

  return (
    <div className="App">
      {alert.message && (
        // <div className={`alert ${alert.type}`}>
        //   {alert.message}
        //   <i onClick={closeAlert} className="fa fa-times"></i>
        // </div>
        <div className='flex-center'>
        <Alert type={alert.type} message={alert.message}></Alert>
        </div>

      )}
      <Routes />
    </div>
  );
}

export default App;
