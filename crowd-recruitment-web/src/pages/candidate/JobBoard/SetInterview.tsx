import React, { useEffect, useState } from "react";
import clsx from 'clsx';
import {
    createStyles,
    makeStyles,
    useTheme,
    Theme,
  } from "@material-ui/core/styles";
import Drawer from '@material-ui/core/Drawer';
import { IJobDetails } from "../../../models/IJobDetails";
import { EditorState, convertToRaw, ContentState } from "draft-js";
import Draft, {
  htmlToDraft,
  EmptyState,
  rawToDraft,
  draftToRaw,
  draftStateToHTML,
} from "react-wysiwyg-typescript";
import DatePicker from "react-datepicker";

import "react-datepicker/dist/react-datepicker.css";

import draftToHtml from "draftjs-to-html";
import Button from "../../../components/Buttons/Button";
import CandidateService from "../../../services/CandidateService";
import { IInterviewDetailsModel } from "../../../models/IInterviewDetailsModel";

const drawerWidth = 750;

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      display: 'flex',
    },
    appBar: {
      zIndex: theme.zIndex.drawer + 1,
    },
    drawer: {
      width: drawerWidth,
      flexShrink: 0,
    },
    menuButton: {
      marginRight: 36,
    },
    hide: {
      display: "none",
    },
    drawerOpen: {
      width: drawerWidth,
      transition: theme.transitions.create("width", {
        easing: theme.transitions.easing.sharp,
        duration: theme.transitions.duration.enteringScreen,
      }),
    },
    drawerClose: {
      transition: theme.transitions.create("width", {
        easing: theme.transitions.easing.sharp,
        duration: theme.transitions.duration.leavingScreen,
      }),
      overflowX: "hidden",
      width: 0,
      [theme.breakpoints.up("sm")]: {
        width:0,
      },
    },
    drawerPaper: {
      width: drawerWidth,
    },
    menuItem: {
      width: drawerWidth,
    },
    menuItemIcon: {
      color: '#97c05c',
    },
    drawerContainer: {
      overflow: 'auto',
    },
    content: {
      flexGrow: 1,
      padding: theme.spacing(3),
    },
  }),
);


 
interface Props {
  data: string;
  openInterview:boolean;
  handleSideBarInterview: (args:any,uuid:string) => void;
  updateInterview: () => void;

}

const SetInterview: React.FC<Props> = ({openInterview, data, handleSideBarInterview,updateInterview}) => {
    const classes = useStyles();
    const [openDrawer, setOpenDrawer] = React.useState(false);

    const getAllTemplates = () => {
      debugger;
      CandidateService.GetAllTemplates()
        .then((response) => {
          setTemplates(response.data.data);
        })
        .catch((e) => {
          console.log(e);
        });
    };

    useEffect(() => {
        setOpenDrawer(openInterview)
        getAllTemplates();
      });

    const toggleDrawer = (open: any) => {
        setOpenDrawer(open) 
      };
      const [editorState, seteditorState] = useState(htmlToDraft(''));
      const handleDescription = (draftText: any) => {
        seteditorState(draftText);
      };

      const [startDate, setStartDate] = useState(new Date());
      const [count, setCount] = useState("1");
      const [titleError, setTitleError] = useState(false);
      const [descriptionError, setDescriptionError] = useState(false);
      const [interviewCommentError, setInterviewCommentError] = useState(false);
  const [isLoading, setisLoading] = useState(false);
  const [value, setValue] = React.useState();
      

      const initialAddInterviewDetails: IInterviewDetailsModel = {

        submissionUUid: data,
        interviewerId: 1,
        interviewComment: "",
        interviewTypeId: 1,
        interviewDate: new Date(),
        interviewTime: "12:00",
        duration: 1,
        templateId: 0,
        emailSubject: "",
        emailDescription: "",
        interviewerTypeId: 1,
        id: 0,
      };

      const initialTitle = [
        {
          id: 0,
          emailTitleEn: "",
          emailtitleAr: "",
          emailbodyEn: "",
          emailbodyAr: "",
        },
      ];
      const [addInterviewDetails, setAddInterviewDetails] = useState(
        initialAddInterviewDetails
      );
      const [templates, setTemplates] = React.useState(initialTitle);


      const handleSetInterviewChange = (event: any) => {
        debugger;
        const { name, value } = event.target;
        setAddInterviewDetails({ ...addInterviewDetails, [name]: value });
    
        if (addInterviewDetails.emailSubject == "") {
          setTitleError(true);
        }
        if (addInterviewDetails.emailSubject != "") {
          setTitleError(false);
        }
      };

      const handleSetInterviewCommentChange = (event: any) => {
        debugger;
        const { name, value } = event.target;
        setAddInterviewDetails({ ...addInterviewDetails, [name]: value });
        if (addInterviewDetails.interviewComment == "") {
          setInterviewCommentError(true);
        }
        if (addInterviewDetails.interviewComment != "") {
          setInterviewCommentError(false);
        }
      };

      const handleTemplateChange = (event: React.ChangeEvent<any>) => {
        const { name, value } = event.target;
        setValue({ ...value, [name]: value });
        debugger;
        if (value != "0") {
          const currentTemplate = templates.find((m) => m.id == value);
          addInterviewDetails.emailSubject = currentTemplate.emailTitleEn;
          addInterviewDetails.emailDescription = currentTemplate.emailbodyEn;
          addInterviewDetails.templateId = currentTemplate.id;
          seteditorState(htmlToDraft(currentTemplate.emailbodyEn));
        } else {
          addInterviewDetails.emailSubject = "";
          addInterviewDetails.emailDescription = "";
          seteditorState(htmlToDraft(""));
          addInterviewDetails.templateId = 0;
        }
    
        setAddInterviewDetails(addInterviewDetails);
    
        if (
          addInterviewDetails.emailSubject == "" ||
          addInterviewDetails.emailDescription == ""
        ) {
          if (addInterviewDetails.emailSubject == "") {
            setTitleError(true);
          }
          if (addInterviewDetails.emailSubject != "") {
            setTitleError(false);
          }
          if (addInterviewDetails.emailDescription == "") {
            setDescriptionError(true);
          }
          if (addInterviewDetails.emailDescription != "") {
            setDescriptionError(false);
          }
        } else {
          setTitleError(false);
          setDescriptionError(false);
        }
      };

      const handleDraftChange = (draftText: any) => {
        seteditorState(draftText);
        // SAVING HTML TO SERVER
        const convertedHtml = draftToHtml(
          convertToRaw(editorState.getCurrentContent())
        );
    
        addInterviewDetails.emailDescription = convertedHtml;
        setAddInterviewDetails(addInterviewDetails);
    
        if (addInterviewDetails.emailDescription == "") {
          setDescriptionError(true);
        }
        if (addInterviewDetails.emailDescription != "") {
          setDescriptionError(false);
        }
        // const html = draftToHtml(draftText)
      };
    

      const saveInterviewDetails = () => {
        debugger;
    
        if (
          addInterviewDetails.interviewComment == "" ||
          addInterviewDetails.emailSubject == "" ||
          addInterviewDetails.emailDescription == "" ||
          addInterviewDetails.emailDescription.includes("<p></p>") == true
        ) {
          if (addInterviewDetails.emailSubject == "") {
            setTitleError(true);
          }
    
          if (addInterviewDetails.emailSubject != "") {
            setTitleError(false);
          }
    
          if (addInterviewDetails.emailDescription == "") {
            setDescriptionError(true);
          }
    
          if (addInterviewDetails.emailDescription.includes("<p></p>") == true) {
            setDescriptionError(true);
          }
    
          if (
            addInterviewDetails.emailDescription != "" &&
            addInterviewDetails.emailDescription.includes("<p></p>") == false
          ) {
            setDescriptionError(false);
          }
    
          if (addInterviewDetails.interviewComment == "") {
            setInterviewCommentError(true);
          }
    
          if (addInterviewDetails.interviewComment != "") {
            setInterviewCommentError(false);
          }
        } else {
          addInterviewDetails.interviewTypeId = Number(count);
          addInterviewDetails.interviewDate = startDate;
          setInterviewCommentError(false);
          addInterviewDetails.submissionUUid = data;
      setisLoading(true);
          CandidateService.AddInterviewDetails(addInterviewDetails)
            .then((response) => {
              debugger;
              handleSideBarInterview(false,data);
              setAddInterviewDetails(initialAddInterviewDetails);
              seteditorState(htmlToDraft(""));
      setisLoading(false);
              // update number of interviews, isMeetingAdded
              updateInterview();

            })
            .catch((e) => {
      setisLoading(false);
              
            });
        }
      };

  return (
    <div className='jobPreview'>
          <Drawer
            className={clsx(classes.drawer, {
            [classes.drawerOpen]: openDrawer,
            [classes.drawerClose]: !openDrawer,
            })}
            classes={{
            paper: clsx({
                [classes.drawerOpen]: openDrawer,
                [classes.drawerClose]: !openDrawer,
            }),
            }}
          anchor="right" open={openDrawer} onClose={() => toggleDrawer(false)}>
           <span
            onClick={() => handleSideBarInterview(false,data)}
           >
               <i className="fa fa-times previewClose"></i>
           </span>
           <div className="drawerBody newEmpLayout miniDrawerBody">
           <div className='font-16 bold'>New Interview</div>
            <div className='row'>
            <div className="col-md-12 text-center" style={{padding:'5px 0 30px 0'}}>
              
                      <div
                        className={count == "1" ? "activeCheck check" : "check"}
                        id=""
                        onClick={() => setCount("1")}
                      >
                        <i className="far fa-circle"></i>
                        Telephone
                      </div>

                      <div
                        className={count == "2" ? "activeCheck check" : "check"}
                        id=""
                        onClick={() => setCount("2")}
                      >
                        <i className="far fa-circle"></i>
                        On site
                      </div>

                      <div
                        className={count == "3" ? "activeCheck check" : "check"}
                        id=""
                        onClick={() => setCount("3")}
                      >
                        <i className="far fa-circle"></i>
                        Web Conference
                      </div>
              
                  </div>

            <div className="col-md-6 form-group customInput">
            <label>Interviewer Name</label>
            
            <select
                      onChange={handleSetInterviewChange}
                      value={addInterviewDetails.interviewerId}
                      className="form-control"
                      name="interviewerId"
                      id="interviewerId"
                    >
                      <option value="1">Basheer</option>

                      <option value="2">Jomana</option>

                      <option value="3">Sara</option>
                      <option value="4">Laura</option>
                    </select>
                  
             </div>

             <div className="col-md-6 form-group customInput">
            <label>Interview Comment</label>
            <input
                placeholder="Interview Comment"
                name="interviewComment"
                id="interviewComment"
                value={addInterviewDetails.interviewComment}
                className="form-control"
                onChange={handleSetInterviewCommentChange}
              />
              {interviewCommentError && (
                <div className="invalid-feedback">
                  Interview comment is required
                </div>
              )}{" "}
             </div>

             <div className="col-md-4 form-group customInput">
            <label>Interview Date</label>
            
            <div>
            <i className="far fa-calendar"></i>
            <DatePicker
              className="form-control w-100"
              selected={startDate}
              onChange={(date: any) => setStartDate(date)}
            />
            
                    </div>
             </div>
             <div className="col-md-4 form-group customInput">
            <label>Time</label>
            {/* <i className="far fa-clock"></i> */}
            <input
                        onChange={handleSetInterviewChange}
                        type="time"
                        className="form-control"
                        value={addInterviewDetails.interviewTime}
                        name="interviewTime"
                        id="interviewTime"
                      />
             </div>

             <div className="col-md-4 form-group customInput">
            <label>Duration</label>
            <select
                        onChange={handleSetInterviewChange}
                        value={addInterviewDetails.duration}
                        className="form-control"
                        name="duration"
                        id="duration"
                      >
                        <option value="1">30 minutes</option>

                        <option value="2">1 hour</option>

                        <option value="3">2 hours</option>
                      </select>
             </div>


             <div className="col-md-6 form-group customInput">
            <label>Choose Template</label>
            <select
                    value={addInterviewDetails.templateId}
                    className="form-control"
                    onChange={handleTemplateChange}
                  >
                    <option key="0" value="0">
                      {"You can choose from our templates"}
                    </option>

                    {templates.map((item) => (
                      <option key={item.id} value={item.id}>
                        {item.emailTitleEn}
                      </option>
                    ))}
                  </select>
                
             </div>

             <div className="col-md-6 form-group customInput">
            <label>Email Subject</label>
            <input
                    className="form-control"
                    name="emailSubject"
                    id="emailSubject"
                    placeholder="Email Subject"
                    value={addInterviewDetails.emailSubject}
                    type="text"
                    onChange={handleSetInterviewChange}
                  />
                  {titleError && (
                    <div className="invalid-feedback">
                      Email subject is required
                    </div>
                  )}{" "}
             </div>



             <div className="form-group col-md-12 customInput">
                    <label>Email description</label>
          
<Draft
                    editorState={editorState}
                    name="emailDescription"
                    id="emailDescription"
                    className="form-control"
                    onEditorStateChange={(editorState) =>
                      handleDraftChange(editorState)
                    }
                  />
                  {descriptionError && (
                    <div className="invalid-feedback">
                      Email description is required
                    </div>
                  )}{" "}
            </div>
            </div>

            <div className="row">
            <div className="btn-job col-md-12 mt-0">
              <div className="pull-right">
                <Button
                  isLoading={false}
                  border="none"
                  fontSize="16px"
                  font="Proxima-Regular"
                  color="#0195FF"
                  bgColor="#e1f0fb"
                  height="35px"
                  onClick={() => handleSideBarInterview(false,data)}
                  radius=""
                  width="124px"
                  children="Back"
                />
                <Button
                  isLoading={isLoading}
                  border="none"
                  fontSize="16px"
                  font="Proxima-Regular"
                  color="white"
                  bgColor="#0195FF"
                  height="35px"
                  onClick={saveInterviewDetails}
                  radius=""
                  width="124px"
                  children="Confirm"
                />
              </div>
            </div>
          </div>

           </div>
          </Drawer>
        </div>
  );
};

export default SetInterview;
