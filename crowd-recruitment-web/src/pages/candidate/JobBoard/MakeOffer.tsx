import React, { useEffect, useState } from "react";
import clsx from 'clsx';
import {
    createStyles,
    makeStyles,
    useTheme,
    Theme,
  } from "@material-ui/core/styles";
import Drawer from '@material-ui/core/Drawer';
import { IJobDetails } from "../../../models/IJobDetails";
import { useDropzone } from "react-dropzone";
import { EditorState, convertToRaw, ContentState } from "draft-js";
import Draft, {
  htmlToDraft,
  EmptyState,
  rawToDraft,
  draftToRaw,
  draftStateToHTML,
} from "react-wysiwyg-typescript";

import draftToHtml from "draftjs-to-html";
import Button from "../../../components/Buttons/Button";
import { IOfferModel } from "../../../models/IOfferModel";
import CandidateService from "../../../services/CandidateService";
import { IInterviewDetailsModel } from "../../../models/IInterviewDetailsModel";

const drawerWidth = 750;

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      display: 'flex',
    },
    appBar: {
      zIndex: theme.zIndex.drawer + 1,
    },
    drawer: {
      width: drawerWidth,
      flexShrink: 0,
    },
    menuButton: {
      marginRight: 36,
    },
    hide: {
      display: "none",
    },
    drawerOpen: {
      width: drawerWidth,
      transition: theme.transitions.create("width", {
        easing: theme.transitions.easing.sharp,
        duration: theme.transitions.duration.enteringScreen,
      }),
    },
    drawerClose: {
      transition: theme.transitions.create("width", {
        easing: theme.transitions.easing.sharp,
        duration: theme.transitions.duration.leavingScreen,
      }),
      overflowX: "hidden",
      width: 0,
      [theme.breakpoints.up("sm")]: {
        width:0,
      },
    },
    drawerPaper: {
      width: drawerWidth,
    },
    menuItem: {
      width: drawerWidth,
    },
    menuItemIcon: {
      color: '#97c05c',
    },
    drawerContainer: {
      overflow: 'auto',
    },
    content: {
      flexGrow: 1,
      padding: theme.spacing(3),
    },
  }),
);


 
interface Props {
  data: string;
  openOffer:boolean;
  handleSideBarOffer: (args:any,uuid:string) => void;
}

const MakeOffer: React.FC<Props> = ({openOffer, data, handleSideBarOffer}) => {
    const classes = useStyles();
    const [openDrawer, setOpenDrawer] = React.useState(false);

    useEffect(() => {
        setOpenDrawer(openOffer);
        getAllOfferTemplates();
      });

      const getAllOfferTemplates = () => {
        debugger;
        CandidateService.GetAllOfferTemplates()
          .then((response) => {
            setOfferTemplates(response.data.data);
          })
          .catch((e) => {
            console.log(e);
          });
      };

    const toggleDrawer = (open: any) => {
        setOpenDrawer(open) 
      };
      const [editorState, seteditorState] = useState(htmlToDraft(''));
      const handleDescription = (draftText: any) => {
        seteditorState(draftText);
      };

      const { acceptedFiles, getRootProps, getInputProps } = useDropzone({
        accept: ".pdf",
      });
    
      const files = acceptedFiles.map((file: any) => (
        <li key={file.path}>
          {file.path} - {file.size} bytes
        </li>
      ));

      function handleChange(e: any) {
        const { name, value } = e.target;
       setFileError(false);
      }

      const handleOfferChange = (event: any) => {
        debugger;
        const { name, value } = event.target;
        setOffer({ ...offer, [name]: value });
    
        if (offer.offerSubject == "") {
          setOfferSubjectError(true);
        }
        if (offer.offerSubject != "") {
          setOfferSubjectError(false);
        }
      };

      const handleRecuirDraftChange = (draftText: any) => {
        setRecuirEditorState(draftText);
        // SAVING HTML TO SERVER
        const convertedReqireHtml = draftToHtml(
          convertToRaw(recuirEditorState.getCurrentContent())
        );
        // const html = draftToHtml(draftText)
        offer.offerDescription = convertedReqireHtml;
        setOffer(offer);
    
        if (offer.offerDescription == "") {
          setOfferDescriptionError(true);
        }
        if (offer.offerDescription != "") {
          setOfferDescriptionError(false);
        }
      };
    

      const initialOffer: IOfferModel = {
        submissionUuid: "",
        offerSubject: "",
        offerDescription: "",
        offerFile: null,
      };

      const initialTitle = [
        {
          id: 0,
          emailTitleEn: "",
          emailtitleAr: "",
          emailbodyEn: "",
          emailbodyAr: "",
        },
      ];

      const initialAddInterviewDetails: IInterviewDetailsModel = {
        submissionUUid: "",
        interviewerId: 1,
        interviewComment: "",
        interviewTypeId: 1,
        interviewDate: new Date(),
        interviewTime: "12:00",
        duration: 1,
        templateId: 0,
        emailSubject: "",
        emailDescription: "",
        interviewerTypeId: 1,
        id: 0,
      };

      const [offer, setOffer] = useState(initialOffer);
      const [offerSubjectError, setOfferSubjectError] = useState(false);
      const [offerDescriptionError, setOfferDescriptionError] = useState(false);
      const [fileError, setFileError] = useState(false);
      const [isLoading, setisLoading] = useState(false);
      const [offerTemplates, setOfferTemplates] = React.useState(initialTitle);
      const [addInterviewDetails, setAddInterviewDetails] = useState(
        initialAddInterviewDetails
      );

      const [value, setValue] = React.useState();
      const html = "";
      const [recuirEditorState, setRecuirEditorState] = useState(htmlToDraft(html));

      const handleOfferTemplateChange = (event: React.ChangeEvent<any>) => {
        const { name, value } = event.target;
        setValue({ ...value, [name]: value });
        debugger;
        if (value != "0") {
          const currentOfferTemplate = offerTemplates.find((m) => m.id == value);
          offer.offerSubject = currentOfferTemplate.emailTitleEn;
          offer.offerDescription = currentOfferTemplate.emailbodyEn;
          setRecuirEditorState(htmlToDraft(currentOfferTemplate.emailbodyEn));
        } else {
          offer.offerSubject = "";
          offer.offerDescription = "";
          setRecuirEditorState(htmlToDraft(""));
        }
    
        setOffer(offer);
    
        if (offer.offerSubject == "" || offer.offerDescription == "") {
          if (offer.offerSubject == "") {
            setOfferSubjectError(true);
          }
          if (offer.offerSubject != "") {
            setOfferSubjectError(false);
          }
          if (offer.offerDescription == "") {
            setOfferDescriptionError(true);
          }
          if (addInterviewDetails.emailDescription != "") {
            setOfferDescriptionError(false);
          }
        } else {
          setOfferSubjectError(false);
          setOfferDescriptionError(false);
        }
      };

      const AddOffer = () => {
        debugger;
        if (
          offer.offerSubject == "" ||
          offer.offerDescription == "" ||
          acceptedFiles[0] == null
        ) {
          if (offer.offerSubject == "") {
            setOfferSubjectError(true);
          }
    
          if (offer.offerSubject != "") {
            setOfferSubjectError(false);
          }
    
          if (offer.offerDescription == "") {
            setOfferDescriptionError(true);
          }
    
          if (offer.offerDescription != "") {
            setOfferDescriptionError(false);
          }
    
          if (acceptedFiles[0] == null) {
            setFileError(true);
          }
    
          if (acceptedFiles[0] != null) {
            setFileError(false);
          }
        } else {
          offer.submissionUuid = data;
          // Validate then Assign file
          setOffer(offer);
    
          const formData = new FormData();
          formData.append("offerFile", acceptedFiles[0]);
          formData.append("submissionUuid", offer.submissionUuid);
          formData.append("offerSubject", offer.offerSubject);
          formData.append("offerDescription", offer.offerDescription);
      setisLoading(true);
          CandidateService.SendOffer(formData)
        .then((response) => {
          handleSideBarOffer(false,data);
      setisLoading(false);
        })
        .catch((e) => {
      setisLoading(false);
          console.log(e);
        });
          
        }
      };

  return (
    <div className='jobPreview'>
          <Drawer
            className={clsx(classes.drawer, {
            [classes.drawerOpen]: openDrawer,
            [classes.drawerClose]: !openDrawer,
            })}
            classes={{
            paper: clsx({
                [classes.drawerOpen]: openDrawer,
                [classes.drawerClose]: !openDrawer,
            }),
            }}
          anchor="right" open={openDrawer} onClose={() => toggleDrawer(false)}>
           <span
            onClick={() => handleSideBarOffer(false,data)}
           >
               <i className="fa fa-times previewClose"></i>
           </span>
           <div className="drawerBody newEmpLayout miniDrawerBody">
            <div className='font-16 bold'>Make Offer</div>

            <div className='row' style={{paddingTop:'30px'}}>
            <div className="col-md-6 form-group customInput">
            <label>Select Template</label>
            <select
                  value="0"
                  className="form-control"
                  onChange={handleOfferTemplateChange}
                >
                  <option key="0" value="0">
                    {"You can choose from our templates"}
                  </option>

                  {offerTemplates.map((item) => (
                    <option key={item.id} value={item.id}>
                      {item.emailTitleEn}
                    </option>
                  ))}
                </select>
             
             </div>

             <div className="col-md-6 form-group customInput">
            <label>Email Subject</label>
            <input
                  className="form-control"
                  name="offerSubject"
                  id="offerSubject"
                placeholder="Email Subject"
                  value={offer.offerSubject}
                  type="text"
                  onChange={handleOfferChange}
                />
                {offerSubjectError && (
                  <div className="invalid-feedback">
                    Email Subject is required
                  </div>
                )}{" "}
             </div>

             <div className="form-group col-md-12 customInput">
                    <label>Email description</label>
                    <Draft
                  editorState={recuirEditorState}
                  name="offerDescription"
                  id="offerDescription"
                  className="form-control"
                  onEditorStateChange={(recuirEditorState) =>
                    handleRecuirDraftChange(recuirEditorState)
                  }
                />
                {offerDescriptionError && (
                  <div className="invalid-feedback">
                    Email description is required
                  </div>
                )}{" "}
            </div>

            <div className="form-group col-md-12">
              <div
                onChange={handleChange}
                {...getRootProps({ className: "dropzone" })}
              >
                <input onChange={handleChange} {...getInputProps()} />
                <div className="file-drop" style={{ marginBottom: "15px" }}>
                  <div className="font-16 bold" style={{paddingBottom:'20px'}}>Drag and drop resume here</div>
                  <div>
                    Or click{" "}
                    <a className="anchor"> here to upload attachment</a>
                      
                  </div>
                </div>
              </div>
            </div>
            </div>

            <div className="row">
            <div className="btn-job col-md-12 mt-0">
            <div className='pull-left'>
            <aside>
                <h4>File</h4>
                <ul>{files}</ul>
                {fileError && (
                  <div className="invalid-feedback">Offer file is required</div>
                )}{" "}
              </aside>
            </div>
              <div className="pull-right">
                <Button
                  isLoading={false}
                  border="none"
                  fontSize="16px"
                  font="Proxima-Regular"
                  color="#0195FF"
                  bgColor="#e1f0fb"
                  height="35px"
                  onClick={() => handleSideBarOffer(false,data)}
                  radius=""
                  width="124px"
                  children="Back"
                />
                <Button
                  isLoading={isLoading}
                  border="none"
                  fontSize="16px"
                  font="Proxima-Regular"
                  color="white"
                  bgColor="#0195FF"
                  height="35px"
                  onClick={AddOffer}
                  radius=""
                  width="124px"
                  children="Confirm"
                />
              </div>
            </div>
          </div>

           </div>
          </Drawer>
        </div>
  );
};

export default MakeOffer;
