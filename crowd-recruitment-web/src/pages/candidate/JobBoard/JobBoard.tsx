import DataTable, { createTheme } from 'react-data-table-component';
import React, { useEffect, useState } from "react";
import {
  createStyles,
  Theme,
  withStyles,
  WithStyles,
} from "@material-ui/core/styles";
import empIco from "../../../../src/assets/job-board-icon.png";
import hot from "../../../../src/assets/hot.png";
import { Link, useHistory } from "react-router-dom";

import Board from 'react-trello'

import { useDropzone } from "react-dropzone";



import IconButton from "@material-ui/core/IconButton";
import CloseIcon from "@material-ui/icons/Close";
import Typography from "@material-ui/core/Typography";

import MakeOffer from './MakeOffer'
import SetInterview from './SetInterview'
import JobService from '../../../services/JobService';
import { IJobCandidate } from '../../../models/IJobCandidate';
import { hiringStagesConstants } from '../../../constants/hiringStagesConstants';
import { IUpdateCandidateCvStatusModel } from '../../../models/IUpdateCandidateCvStatusModel';
import CandidateService from '../../../services/CandidateService';
import { Dispatch } from "redux";
import { alertActions } from "../../../actions/alertActions";
import { useDispatch } from "react-redux";
import { IUpdateHiringStageModel } from '../../../models/IUpdateHiringStageModel';
import { IInterviewDetailsModel } from '../../../models/IInterviewDetailsModel';
import { IInterviewsModel } from '../../../models/IInterviewsModel';
import { IUpdateIsAcceptOfferModel } from '../../../models/IUpdateIsAcceptOfferModel';
import { IUpdateIsHiredModel } from '../../../models/IUpdateIsHiredModel';


export default function EmployerJobBoard(props: any) {

  useEffect(() => {
    getAllJobCandidates();
    getAllInterviews();
  }, []);

  const [allJobCandidates, setAllJobCandidates] = useState<IJobCandidate[]>([]);
  const [jobTitle, setJobTitle] = useState("");
  const [currentBoardClass, setCurrentBoardClass] = useState("");
  const initialAddInterviewDetails: IInterviewDetailsModel = {
    submissionUUid: "",
    interviewerId: 1,
    interviewComment: "",
    interviewTypeId: 1,
    interviewDate: new Date(),
    interviewTime: "12:00",
    duration: 1,
    templateId: 0,
    emailSubject: "",
    emailDescription: "",
    interviewerTypeId: 1,
    id: 0,
  };
  const [addInterviewDetails, setAddInterviewDetails] = useState(
    initialAddInterviewDetails
  );

  const getAllInterviews = () => {
    debugger;
    CandidateService.GetAllInterviews()
      .then((response) => {
        debugger;
        setAllInterviews(response.data.data);
      })
      .catch((e) => {
        console.log(e);
      });
  };

  const [allInterviews, setAllInterviews] = useState<IInterviewsModel[]>([]);

  const getAllJobCandidates = () => {
    debugger;
    JobService.getAcceptedJobCandidates(props.match.params.id)
      .then((response) => {
        debugger;
        setAllJobCandidates(response.data.data);
        setJobTitle(response.data.data[0].jobTitle);
        if (response.data.data[0].jobStatusId == 5) {
          setCurrentBoardClass("closed");
        }
      })
      .catch((e) => {
        console.log(e);
      });
  };

  const updateInterviewCount = () => {
    getAllJobCandidates();
    getAllInterviews();
  }; 

  const [activeRadio, setactiveRadio] = React.useState('approved');
  const handleRadio = (args:any,uuid: string) => {
    debugger;
    setactiveRadio(args);
    const acceptModel: IUpdateCandidateCvStatusModel = {
      isCvApproved: args == "approved"?true:false,
      submissionUuid: uuid,
    };

    CandidateService.UpdateCandidateCvStatus(acceptModel)
      .then((response) => {
        getAllJobCandidates();
      })
      .catch((e) => {
        console.log(e);
      });
  };

  const [offerStatus, setofferStatus] = React.useState('accepted');
  const handleOffer = (args:any,uuid: string) => {
    setofferStatus(args);
    const IsAcceptOfferModel: IUpdateIsAcceptOfferModel = {
      isOfferAccepted: args == "accepted"?true:false,
      submissionUuid: uuid,
    };

    CandidateService.UpdateIsAcceptOffer(IsAcceptOfferModel)
      .then((response) => {
        getAllJobCandidates();
      })
      .catch((e) => {
        console.log(e);
      });
  };

  const [hiredStatus, sethiredStatus] = React.useState('yes');
  const handleHired = (args:any,uuid: string) => {
    sethiredStatus(args);
    const IsHiredModel: IUpdateIsHiredModel = {
      isHired: args == "yes"?true:false,
      submissionUuid: uuid,
    };

    CandidateService.UpdateIsHired(IsHiredModel)
      .then((response) => {
        getAllJobCandidates();
      })
      .catch((e) => {
        console.log(e);
      });
  };
  const dispatch = useDispatch();

  const [openOffer, setOpenOffer] = React.useState(false);
  const [openInterview, setOpenInterview] = React.useState(false);
  const [currentUuid, setCurrentUuid] = React.useState("");
  const handleSideBarOffer = (args: any,uuid:string) => {
    setCurrentUuid(uuid);
    setOpenOffer(args);
  };

  const handleSideBarInterview = (args: any,uuid: string) => {
    setCurrentUuid(uuid);
    setOpenInterview(args);
  };

  const handleDragEnd = (
    cardId: any,
    sourceLaneId: any,
    targetLaneId: any,
    position: any,
    cardDetails: any
  ) => {
    debugger;
    console.log("cardId", cardId);
    if (
      sourceLaneId == hiringStagesConstants.Approved_By_Employer &&
      targetLaneId == hiringStagesConstants.Approved
    ) {
      return false;
    }
    if (
      sourceLaneId == hiringStagesConstants.Interviews &&
      targetLaneId == hiringStagesConstants.Approved_By_Employer
    ) {
      return false;
    }
    if (
      sourceLaneId == hiringStagesConstants.Offer &&
      targetLaneId == hiringStagesConstants.Interviews
    ) {
      return false;
    }
    if (
      sourceLaneId == hiringStagesConstants.Hiring &&
      targetLaneId == hiringStagesConstants.Offer
    ) {
      return false;
    }
    if (
      sourceLaneId == hiringStagesConstants.Hiring &&
      targetLaneId == hiringStagesConstants.Interviews
    ) {
      return false;
    }
    if (
      sourceLaneId == hiringStagesConstants.Hiring &&
      targetLaneId == hiringStagesConstants.Approved_By_Employer
    ) {
      return false;
    }
    if (
      sourceLaneId == hiringStagesConstants.Hiring &&
      targetLaneId == hiringStagesConstants.Approved
    ) {
      return false;
    }
    if (
      sourceLaneId == hiringStagesConstants.Offer &&
      targetLaneId == hiringStagesConstants.Approved_By_Employer
    ) {
      return false;
    }
    if (
      sourceLaneId == hiringStagesConstants.Offer &&
      targetLaneId == hiringStagesConstants.Approved
    ) {
      return false;
    }
    if (
      sourceLaneId == hiringStagesConstants.Approved_By_Employer &&
      targetLaneId == hiringStagesConstants.Offer
    ) {
      return false;
    }
    if (
      sourceLaneId == hiringStagesConstants.Approved_By_Employer &&
      targetLaneId == hiringStagesConstants.Hiring
    ) {
      return false;
    }
    if (
      sourceLaneId == hiringStagesConstants.Interviews &&
      targetLaneId == hiringStagesConstants.Approved
    ) {
      return false;
    }
    if (
      sourceLaneId == hiringStagesConstants.Interviews &&
      targetLaneId == hiringStagesConstants.Hiring
    ) {
      return false;
    }
    if (
      sourceLaneId == hiringStagesConstants.Approved &&
      targetLaneId == hiringStagesConstants.Interviews
    ) {
      return false;
    }
    if (
      sourceLaneId == hiringStagesConstants.Approved &&
      targetLaneId == hiringStagesConstants.Offer
    ) {
      return false;
    }
    if (
      sourceLaneId == hiringStagesConstants.Approved &&
      targetLaneId == hiringStagesConstants.Hiring
    ) {
      return false;
    }

    debugger;
    const currentCardDetails = allJobCandidates.find(
      (m) => m.submissionUuId == cardId
    );

    //alert(currentCardDetails.jobStatusId)

    if (sourceLaneId == targetLaneId) {
      return false;
    }

    if (
      sourceLaneId == hiringStagesConstants.Approved_By_Employer &&
      currentCardDetails.isCvApproved != true
    ) {
      debugger;
      dispatch(
        alertActions.error("Approve Cv to enable dragging to next stage")
      );
      return false;
    }

    if (
      sourceLaneId == hiringStagesConstants.Offer &&
      currentCardDetails.isAcceptOffer != true
    ) {
      dispatch(
        alertActions.error(
          "Offer should be accepted to enable dragging to next stage"
        )
      );
      return false;
    }

    if (
      sourceLaneId == hiringStagesConstants.Interviews &&
      currentCardDetails.hasInterview <= 0
    ) {
      dispatch(
        alertActions.error(
          "Candidate should have at least one interview to enable dragging to next stage"
        )
      );
      return false;
    }

    addInterviewDetails.submissionUUid = cardId;
    setAddInterviewDetails(addInterviewDetails);

    const updateStageModel: IUpdateHiringStageModel = {
      submissionUuid: cardId,
      hiringStageId: targetLaneId,
    };

    CandidateService.UpdateHiringStageStatus(updateStageModel)
      .then((response) => {
        getAllJobCandidates();
      })
      .catch((e) => {
        console.log(e);
      });
  };
  
  const cardsSourcing: any = [];

  allJobCandidates
    .filter(
      (m) => m.candidateSubmissionStatusId == hiringStagesConstants.Approved
    )
    .map((job: IJobCandidate) =>
      cardsSourcing.push({
        id: job.submissionUuId,
        title: "",
        description: (
          <div className='job-box boardBox OrangeCard'>
          <div className='topNavBoard'>
          <div className='pull-left data-card-light'><i className='fal fa-calendar'></i>{job.jobDate}</div>
          <div className='pull-right'><img src={hot}/></div>
          </div>
      
          <div className='clearBoth flowRoot' style={{margin:'0px 0px 10px'}}>
          <div className='float-left' style={{marginRight:'10px'}}>
           <img src={empIco}/>
          </div>
          <div className='float-left'>
          <div className="data-card-head">
          {job.candidateName}
          </div> 
          <div className="align-left">
          <Link
        to={"/employer/candidateinterview/" + job.submissionUuId}
        className="detail-link"
        >
          Show Details
        </Link>
          </div>
          </div>
          
          </div>
        <div className='cardStatus'>
        <ul>
          <span className='liS-orange'>
          <span className='cricle'></span>
          Sourcing</span>
        </ul>
        </div>
        </div>
        
        ),
        draggable: true,
      })
    );

    const cardsApproval: any = [];

  allJobCandidates
    .filter(
      (m) =>
        m.candidateSubmissionStatusId ==
        hiringStagesConstants.Approved_By_Employer
    )
    .map((job: IJobCandidate) =>
      cardsApproval.push({
        id: job.submissionUuId,
        title: "",
        description: (
          <div className='job-box boardBox GreenCard'>
          <div className='topNavBoard'>
          <div className='pull-left data-card-light'><i className='fal fa-calendar'></i>{job.jobDate}</div>
          <div className='pull-right'><img src={hot}/></div>
          </div>
      
          <div className='clearBoth flowRoot' style={{margin:'0px 0px 10px'}}>
          <div className='float-left' style={{marginRight:'10px'}}>
           <img src={empIco}/>
          </div>
          <div className='float-left'>
          <div className="data-card-head">
          {job.candidateName}
          </div> 
          <div className="align-left">
          <Link
        to={"/employer/candidateinterview/" + job.submissionUuId}
        className="detail-link"
        >
          Show Details
        </Link>
          </div>
          </div>
          
          </div>
        <div className='cardStatus bRadioBtn'>
        <div className='font-14'>CV approval</div>
        <div>
          <span
          className={
              job.isCvApproved == true ? "bRadio approveRadio rActive" : "bRadio approveRadio"
          }
          onClick={() =>handleRadio('approved',job.submissionUuId)}>
          <i className='fa fa-circle'></i>  
          Approved</span>
          <span
          className={
              job.isCvApproved == false ? "bRadio disApproveRadio rActive" : "bRadio disApproveRadio"
          }
          onClick={() => handleRadio('disapproved',job.submissionUuId)}>
          <i className='fa fa-circle'></i>  
          Disapproved</span>
        </div>
        </div>
        </div>
          
        ),
      })
    );
  
    const cardsInterviews: any = [
    
    ];
  
    allJobCandidates
      .filter(
        (m) => m.candidateSubmissionStatusId == hiringStagesConstants.Interviews
      )
      .map((job: IJobCandidate) =>
        cardsInterviews.push({
          id: job.submissionUuId,
          title: "",
          description: (
            <div className='job-box boardBox BlueCard'>
            <div className='topNavBoard'>
            <div className='pull-left data-card-light'><i className='fal fa-calendar'></i>{job.jobDate}</div>
            <div className='pull-right'><img src={hot}/></div>
            </div>
        
            <div className='clearBoth flowRoot' style={{margin:'0px 0px 10px'}}>
            <div className='float-left' style={{marginRight:'10px'}}>
             <img src={empIco}/>
            </div>
            <div className='float-left'>
            <div className="data-card-head">
            {job.candidateName}
            </div> 
            <div className="align-left">
            <Link
          to={"/employer/candidateinterview/" + job.submissionUuId}
          className="detail-link"
          >
            Show Details
          </Link>
            </div>
            </div>
            
            </div>
          <div className='cardStatus'>
          <ul className='pull-left'>
            <span className='liS-blue'>
            <span className='cricle'></span>
            {allInterviews.filter(
                      (interview) =>
                        interview.submissionUuid == job.submissionUuId &&
                        interview.jobId == job.jobId
                    ).length} Interviews </span>
          </ul>
          <div className='pull-right'>
          <button onClick={() => handleSideBarInterview(true,job.submissionUuId)}
           className='btn btn-approve board-btn'>Set Another</button>
          </div>
          </div>
          </div>
          ),
        })
      );
  
  
  //   {id: 'Card44', title: '', description: <div className='job-box boardBox GrayCard'>
  //   <div className='topNavBoard'>
  //   <div className='pull-left data-card-light'><i className='fal fa-calendar'></i>10/12/2021</div>
  //   <div className='pull-right'><img src={hot}/></div>
  //   </div>

  //   <div className='clearBoth flowRoot' style={{margin:'0px 0px 10px'}}>
  //   <div className='float-left' style={{marginRight:'10px'}}>
  //    <img src={empIco}/>
  //   </div>
  //   <div className='float-left'>
  //   <div className="data-card-head">
  //   John Willaims
  //   </div> 
  //   <div className="align-left">
  //   <Link
  // to=''
  // className="detail-link"
  // >
  //   Show Details
  // </Link>
  //   </div>
  //   </div>
    
  //   </div>
  // <div className='cardStatus'>
  // <ul className='pull-left'>
  // </ul>
  // <div className='pull-right'>
  // <button
  // onClick={() => handleSideBarOffer(true)}
  // className='btn btn-approve board-btn'>Set Offer</button>
  // </div>
  // </div>
  // </div>, draggable: true},

  const cardsOffering: any = [];
  allJobCandidates
    .filter((m) => m.candidateSubmissionStatusId == hiringStagesConstants.Offer)
    .map((job: IJobCandidate) =>
      cardsOffering.push({
        id: job.submissionUuId,
        title: "",
        description: <div className='job-box boardBox GreenCard'>
<div className='topNavBoard'>
<div className='pull-left data-card-light'><i className='fal fa-calendar'></i>{job.jobDate}</div>
<div className='pull-right'><img src={hot}/></div>
</div>

<div className='clearBoth flowRoot' style={{margin:'0px 0px 10px'}}>
<div className='float-left' style={{marginRight:'10px'}}>
 <img src={empIco}/>
</div>
<div className='float-left'>
<div className="data-card-head">
{job.candidateName}
</div> 
<div className="align-left">
<Link
to={"/employer/candidateinterview/" + job.submissionUuId}
className="detail-link"
>
Show Details
</Link>
</div>
</div>

</div>
<div className=''>
  <div style={{paddingBottom:'10px'}} className=''>
  <button
  onClick={() => handleSideBarOffer(true,job.submissionUuId)}
  className='btn btn-approve board-btn'>Set Offer</button>
  </div>
  </div>
<div className='cardStatus bRadioBtn'>
<div className='font-14'>Is offer accepted? </div>
<div>
<span
className={
  job.isAcceptOffer == true ? "bRadio approveRadio rActive"
    : "bRadio approveRadio"
}
onClick={() =>handleOffer('accepted',job.submissionUuId)}>
<i className='fa fa-circle'></i>  
Accepted</span>
<span
className={
  job.isAcceptOffer == false ? "bRadio disApproveRadio rActive"
    : "bRadio disApproveRadio"
}
onClick={() => handleOffer('refused',job.submissionUuId)}>
<i className='fa fa-circle'></i>  
Refused</span>
</div>
</div>

</div>,
      })
    );

    const cardsHiring: any = [];

  allJobCandidates
    .filter(
      (m) => m.candidateSubmissionStatusId == hiringStagesConstants.Hiring
    )
    .map((job: IJobCandidate) =>
      cardsHiring.push({
        id: job.submissionUuId,
        title: "",
        description: <div className='job-box boardBox GreenCard'>
        <div className='topNavBoard'>
        <div className='pull-left data-card-light'><i className='fal fa-calendar'></i>{job.jobDate}</div>
        <div className='pull-right'><img src={hot}/></div>
        </div>
        
        <div className='clearBoth flowRoot' style={{margin:'0px 0px 10px'}}>
        <div className='float-left' style={{marginRight:'10px'}}>
         <img src={empIco}/>
        </div>
        <div className='float-left'>
        <div className="data-card-head">
        {job.candidateName}
        </div> 
        <div className="align-left">
        <Link
        to={"/employer/candidateinterview/" + job.submissionUuId}
        className="detail-link"
        >
        Show Details
        </Link>
        </div>
        </div>
        
        </div>
        <div className='cardStatus bRadioBtn'>
        <div className='font-14'>Is this candidate hired? </div>
        <div>
        <span
        className={
          job.isHired == true ? "bRadio approveRadio rActive"
            : "bRadio approveRadio"
        }
        onClick={() =>handleHired('yes',job.submissionUuId)}>
        <i className='fa fa-circle'></i>  
        Yes</span>
        <span
        className={
          job.isHired == false ? "bRadio disApproveRadio rActive"
            : "bRadio disApproveRadio"
        }
        onClick={() => handleHired('no',job.submissionUuId)}>
        <i className='fa fa-circle'></i>  
        No</span>
        </div>
        </div>
        </div>
          
      })
    );

  const data = {
    lanes: [
      {
        id: hiringStagesConstants.Approved,
        title: 'Sourcing',
        cards: cardsSourcing
      },
      {
        id: hiringStagesConstants.Approved_By_Employer,
        title: 'CV Approval',
        cards: cardsApproval,
        currentPage: 1,
      },
      {
        id: hiringStagesConstants.Interviews,
        title: 'Interviews',
        cards: cardsInterviews,
      },
      {
        id: hiringStagesConstants.Offer,
        title: 'Offering',
        cards: cardsOffering,
      },
      {
        id: hiringStagesConstants.Hiring,
        title: 'Hiring',
        cards: cardsHiring,
      }
    ]
  } 


  return (
    <React.Fragment>

      <div className='newJobBoard'>
      <div
      className="bread-text"
      style={{ paddingBottom: "13px", paddingTop:'12px' }}
    >
      <span className="lt-head">
        <a href="/employer/dashboard">My Jobs</a>
      </span>
      <span className="lt-text">
        <i className="fal fa-angle-right"></i>
      </span>
      <span className="lt-head-bold">Candidates</span>
    </div>

      <Board data={data}
       handleDragEnd={handleDragEnd}
       hideCardDeleteIcon={true}
      /> 

      <MakeOffer
      handleSideBarOffer={handleSideBarOffer}
      data={currentUuid}
      openOffer={openOffer}
       />
      <SetInterview
      handleSideBarInterview={handleSideBarInterview}
      data={currentUuid}
      openInterview={openInterview}
      updateInterview = {updateInterviewCount}
       />
      </div>
    </React.Fragment>
  );
}
