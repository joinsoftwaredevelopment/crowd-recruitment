import React, { useState } from "react";
import PhoneInput from "react-phone-input-2";
import "react-phone-input-2/lib/style.css";
import Logo from "../../../../src/assets/cr-logo.png";
import TopDots from "../../../../src/assets/dots-top.png";
import BottomDots from "../../../../src/assets/dots-bottom.png";
import { CircularProgress } from "@material-ui/core";
import Anime from "react-anime";
import anime from "animejs/lib/anime.es.js";
import Button from "../../../components/Buttons/Button";
import { useHistory } from "react-router-dom";
import { useForm } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import * as Yup from "yup";
import { IUser } from "../../../models/IUser";
import UserService from "../../../services/UserService";
import { Dispatch } from "redux";
import { alertActions } from "../../../actions/alertActions";
import { useDispatch } from "react-redux";
import { history } from "../../../helpers/history";

function EmployerForgotPassword() {
  const initialUserState = {
    Email: "",
  };
  const [user, setUser] = useState(initialUserState);

  const [isLoading, setisLoading] = useState(false);

  let currentContent: any;

  const history = useHistory();

  const validationSchema = Yup.object().shape({
    Email: Yup.string().trim().required("Email is required").email(),
  });

  const handleInputChange = (event: any) => {
    debugger;
    const { name, value } = event.target;
    setUser({ ...user, [name]: value });
  };

  const { register, handleSubmit, reset, errors } = useForm({
    resolver: yupResolver(validationSchema),
  });

  const dispatch = useDispatch();

  function onSubmit(data: IUser) {
    setisLoading(true);
    debugger;
    // GetUserInfo
    UserService.SendResetEmail(data)
      .then((response) => {
        dispatch(
          alertActions.success(
            "We sent an email to your email address, check it to reset your password"
          )
        );
        history.push("/RecoverEmail");
        setisLoading(false);
      })
      .catch((e) => {
        dispatch(
          alertActions.error(
            "The email you entered isn't relatated with any accounts in our system, add a correct email or register a new account"
          )
        );
        setisLoading(false);
      });
  }

  return (
    <React.Fragment>
      <section className="authLayout">
        <div style={{ paddingBottom: "90px" }} className="auth-form">
          <div className="topDots">
            <Anime delay={anime.stagger(400)} scale={[0.1, 0.9]}>
              <img src={TopDots} />
            </Anime>
          </div>

          <div className="bottomDots">
            <Anime delay={anime.stagger(400)} scale={[0.1, 0.9]}>
              <img src={BottomDots} />
            </Anime>
          </div>

          <div
            className="back-link"
            onClick={() => history.push("/employerHome")}
          >
            <i className="fa fa-arrow-left"></i>
            Back to home page
          </div>

          <div style={{ marginBottom: "0px" }} className="auth-head">
            Forgot Password
          </div>
          <div style={{ marginBottom: "50px" }}>
            Type your Email address to recover password
          </div>

          <form
            className="row"
            onSubmit={handleSubmit(onSubmit)}
            onReset={reset}
          >
            <div className="col-md-12 form-group customInput">
              <label>Email Address</label>
              <i className="far fa-envelope"></i>
              <input
                name="Email"
                id="Email"
                placeholder="Email Address"
                defaultValue={user.Email}
                type="text"
                ref={register}
                className={`form-control ${errors.Email ? "is-invalid" : ""}`}
                onChange={handleInputChange}
              />
              <div className="invalid-feedback">{errors.Email?.message}</div>
            </div>

            <div className="col-md-12 pull-right">
              <Button
                isLoading={isLoading}
                border="none"
                fontSize="16px"
                font="Proxima-Regular"
                color="white"
                bgColor="#0195FF"
                height="35px"
                onClick={void 0}
                radius=""
                width="180px"
                children="Send"
              />
            </div>
          </form>
        </div>
        <div className="auth-logo-img">
          <Anime delay={anime.stagger(400)} scale={[0.1, 0.9]}>
            <img src={Logo} />
          </Anime>
        </div>
      </section>
    </React.Fragment>
  );
}
export default EmployerForgotPassword;
