import React, { useState, useEffect } from "react";
import { createStyles, makeStyles, Theme } from "@material-ui/core/styles";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import Grid from "@material-ui/core/Grid";
import thank from "../../../src/assets/thankful.png";
import JobService from "../../../services/JobService";
import { IJobDetails } from "../../../models/IJobDetails";

export default function JobPreviewEmployer(props: any) {
  const [jobDetails, setjobDetails] = useState<IJobDetails>({
    uuid: "",
    jobTitle: "",
    employmentTypeName: "",
    seniorityLevelName: "",
    companiesNotToSourceFrom: "",
    languageId: 2,
    jobLocation: "",
    companyName: "",
    mustHaveQualification: "",
    niceToHaveQualification: "",
    hiringNeeds: "",
    experienceLevelName: "",
    description: "",
    companyIndustry: "",
    jobStatusId: 3,
    jobStatusName: "",
    requirements: "",
    noOfSubmissions: 0,
    jobDate: new Date(),
  });
  const getJobDetails = () => {
    debugger;
    JobService.getJobDetails(props.match.params.id)
      .then((response) => {
        debugger;
        setjobDetails(response.data.data);
      })
      .catch((e) => {
        console.log(e);
      });
  };

  useEffect(() => {
    getJobDetails();
  }, []);
  return (
    <React.Fragment>
      <div className="">
        <div className="row">
          <div className="col-md-12">
            <div className="bread-text pull-left">
              <span className="lt-head">
                <a href="/employer/dashboard">My Jobs</a>
              </span>
              <span className="lt-text">
                <i className="fal fa-angle-right"></i>
              </span>
              <span className="lt-head-bold">Job Details</span>
            </div>
          </div>
        </div>

        <div className="row">
          <div className="col-md-9">
            <div className="sub-info-box">
              <div className="feature-set">
                <div className="bold-head eBold font-16">
                  {jobDetails.jobTitle}
                </div>
              </div>

              <div className="feature-set-info">
                <div className="sub-feature">
                  <div className="bold-head font-14">Job Description</div>

                  <div
                    className="light-text"
                    dangerouslySetInnerHTML={{ __html: jobDetails.description }}
                  />
                </div>
              </div>

              <div className="feature-set-info">
                <div className="sub-feature">
                  <div className="bold-head font-14">Requirements</div>

                  <div
                    className="light-text"
                    dangerouslySetInnerHTML={{
                      __html: jobDetails.requirements,
                    }}
                  />
                </div>

                <div className="feature-set-info">
                  <div className="sub-feature">
                    <div className="bold-head font-14">
                      Must-have qualifications
                    </div>

                    <div className="light-text">
                      {jobDetails.mustHaveQualification}
                    </div>
                  </div>
                </div>

                <div className="feature-set-info">
                  <div className="sub-feature">
                    <div className="bold-head font-14">
                      Companies not to source from
                    </div>

                    <div className="light-text">
                      {jobDetails.companiesNotToSourceFrom}
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className="col-md-3">
            <div className="sub-info-box">
              <div className="feature-set">
                <div className="bold-head eBold font-16">Job Details</div>
              </div>

              <div className="feature-set-row">
                <div className="bold-head font-14">
                  <span>
                    <i className="fa fa-building"></i>
                  </span>
                  <span>Company name</span>
                </div>

                <div className="light-text">{jobDetails.companyName}</div>
              </div>

              <div className="feature-set-row">
                <div className="bold-head font-14">
                  <span>
                    <i className="fa fa-chart-bar"></i>
                  </span>
                  <span>Seniority level</span>
                </div>

                <div className="light-text">
                  {jobDetails.seniorityLevelName}
                </div>
              </div>

              <div className="feature-set-row">
                <div className="bold-head font-14">
                  <span>
                    <i className="fa fa-clock"></i>
                  </span>
                  <span>Employment type</span>
                </div>

                <div className="light-text">
                  {jobDetails.employmentTypeName}
                </div>
              </div>

              <div className="feature-set-row">
                <div className="bold-head font-14">
                  <span>
                    <i className="fa fa-location"></i>
                  </span>
                  <span>Location</span>
                </div>

                <div className="light-text">{jobDetails.jobLocation}</div>
              </div>

              <div className="side-set">
                <div className="feature-set-row">
                  <div className="bold-head font-14">
                    <span>Company Industry</span>
                  </div>

                  <div className="light-text">{jobDetails.companyIndustry}</div>
                </div>

                <div className="feature-set-row">
                  <div className="bold-head font-14">
                    <span>Work experience</span>
                  </div>

                  <div className="light-text">
                    {jobDetails.experienceLevelName}
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </React.Fragment>
  );
}
