import React, { useState } from "react";
import PhoneInput from "react-phone-input-2";
import "react-phone-input-2/lib/style.css";
import Logo from "../../../../src/assets/cr-logo.png";
import TopDots from "../../../../src/assets/dots-top.png";
import BottomDots from "../../../../src/assets/dots-bottom.png";
import { CircularProgress } from "@material-ui/core";
import Anime from "react-anime";
import anime from "animejs/lib/anime.es.js";
import Button from "../../../components/Buttons/Button";
import { useForm, Controller } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import * as Yup from "yup";
import { RootStateOrAny, useDispatch, useSelector } from "react-redux";
import { employerActions } from "../../../actions/employerActions";
import { useHistory } from "react-router-dom";

function SignUpEmployer() {
  const history = useHistory();
  let currentContent: any;

  let content = {
    English: {
      firstName: "First Name",
      lastName: "Last Name",
      companyName: "Company Name",
      phoneNumber: "Phone Number",
      email: "Email Address",
      password: "Password",
      signUp: "Sign Up",
      loginWithLinkedin: "Login With LinkedIn",
      Phonenumberisrequired: "Phone number is required",
      Firstnameisrequired: "First name is required",
      Lastnameisrequired: "Last name is required",
      Mustbeonlydigits: "Must be only digits",
      Emailisrequired: "Email is required",
      Emailisinvalid: "Email is invalid",
      Passwordmustbeatleast: "Password must be at least 8 characters",
      Passwordisrequired: "Password is required",
      companynameisrequired: "Company name is required",
    },
    Arabic: {
      firstName: "الاسم الأول",
      lastName: "الاسم الأخير",
      companyName: "اسم الشركة",
      phoneNumber: "الهاتف الجوال",
      email: "البريد الإلكتروني",
      password: "كلمة المرور",
      signUp: "تسجيل",
      loginWithLinkedin: "الدخول بإستخدام لينكد ان",
      Phonenumberisrequired: "الهاتف الجوال مطلوب",
      Firstnameisrequired: "الاسم الأول مطلوب",
      Lastnameisrequired: "الاسم الأخير مطلوب",
      Mustbeonlydigits: "يجب أن يكون أرقام فقط",
      Emailisrequired: "البريد الإلكتروني مطلوب",
      Emailisinvalid: "البريد الإلكتروني غير صالح",
      Passwordmustbeatleast: "كلمة المرور مكونة من 8 أحرف علي الأقل",
      Passwordisrequired: "كلمة المرور مطلوبة",
      companynameisrequired: "اسم الشركة مطلوب",
    },
  };

  localStorage.getItem("currentLanguage") === "Arabic"
    ? (currentContent = content.Arabic)
    : (currentContent = content.English);

  const initialEmployeeState = {
    CompanyName: "",
    FirstName: "",
    LastName: "",
    PhoneNumber: "",
    Email: "",
    Password: "",
    confirmedPassword: "",
  };

  const dispatch = useDispatch();

  const [employer, setEmployee] = useState(initialEmployeeState);

  const handleInputChange = (event: any) => {
    const { name, value } = event.target;
    setEmployee({ ...employer, [name]: value });
  };

  const newEmployee = () => {
    setEmployee(initialEmployeeState);
  };

  // Validation
  // form validation rules
  const validationSchema = Yup.object().shape({
    CompanyName: Yup.string()
      .trim()
      .required(currentContent.companynameisrequired),
    FirstName: Yup.string().trim().required(currentContent.Firstnameisrequired),
    LastName: Yup.string().trim().required(currentContent.Lastnameisrequired),

    PhoneNumber: Yup.string()
      .trim()
      .required(currentContent.Phonenumberisrequired)
      .matches(/^[0-9]+$/, currentContent.Mustbeonlydigits),
    Email: Yup.string()
      .trim()
      .required(currentContent.Emailisrequired)
      .email(currentContent.Emailisinvalid),
    Password: Yup.string()
      .trim()
      .min(8, currentContent.Passwordmustbeatleast)
      .required(currentContent.Passwordisrequired),
    confirmedPassword: Yup.string().oneOf(
      [Yup.ref("Password"), null],
      "Two passwords must match"
    ),
  });

  // functions to build form returned by useForm() hook
  const { register, handleSubmit, reset, errors, control } = useForm({
    resolver: yupResolver(validationSchema),
  });

  const registering = useSelector(
    (state: RootStateOrAny) => state.registration.registering
  );

  //function onSubmit(data:any) {
  //saveEmployee();
  //}

  function onSubmit(data: any) {
    debugger;
    setisLoading(true);
    dispatch(employerActions.register(data));
  }

  const [isLoading, setisLoading] = useState(false);
  return (
    <React.Fragment>
      <section className="authLayout">
        <div className="auth-form">
          <div className="topDots">
            <Anime delay={anime.stagger(400)} scale={[0.1, 0.9]}>
              <img src={TopDots} />
            </Anime>
          </div>

          <div className="bottomDots">
            <Anime delay={anime.stagger(400)} scale={[0.1, 0.9]}>
              <img src={BottomDots} />
            </Anime>
          </div>

          <div
            className="back-link"
            onClick={() => history.push("/employerHome")}
          >
            <i className="fa fa-arrow-left"></i>
            Back to home page
          </div>

          <div className="auth-head">Register as Employer</div>
          <form
            className="row"
            onSubmit={handleSubmit(onSubmit)}
            onReset={reset}
          >
            <div className="col-md-12 form-group customInput">
              <label>Company Name</label>
              <i className="far fa-briefcase"></i>
              <input
                name="CompanyName"
                id="CompanyName"
                placeholder="Company Name"
                defaultValue={employer.CompanyName}
                type="text"
                ref={register}
                className={`form-control ${
                  errors.CompanyName ? "is-invalid" : ""
                }`}
                onChange={handleInputChange}
              />
              <div className="invalid-feedback">
                {errors.CompanyName?.message}
              </div>
            </div>
            <div className="col-md-6 form-group customInput">
              <label>First Name</label>
              <i className="far fa-user"></i>
              <input
                name="FirstName"
                id="FirstName"
                defaultValue={employer.FirstName}
                type="text"
                placeholder="First Name"
                ref={register}
                className={`form-control ${
                  errors.FirstName ? "is-invalid" : ""
                }`}
                onChange={handleInputChange}
              />
              <div className="invalid-feedback">
                {errors.FirstName?.message}
              </div>
            </div>
            <div className="col-md-6 form-group customInput">
              <label>Last Name</label>
              <i className="far fa-user"></i>
              <input
                name="LastName"
                id="LastName"
                placeholder="Last Name"
                defaultValue={employer.LastName}
                type="text"
                ref={register}
                className={`form-control ${
                  errors.LastName ? "is-invalid" : ""
                }`}
                onChange={handleInputChange}
              />
              <div className="invalid-feedback">{errors.LastName?.message}</div>
            </div>
            <div className="col-md-12 form-group customInput phoneInput">
              <label>Phone Number</label>
              <i className="far fa-phone"></i>

              <Controller
                name="PhoneNumber"
                control={control}
                defaultValue=""
                render={({ name, onBlur, onChange, value }) => (
                  <PhoneInput
                    //name={name}
                    value={value}
                    onBlur={onBlur}
                    onChange={onChange}
                    country={"sa"}
                    //style={{ width: "100%" }}
                    // label="Contacto telefónico"
                    // variant="outlined"
                    // margin="normal"
                    inputProps={{
                      name: "PhoneNumber",
                      id: "PhoneNumber",
                      required: true,
                      autoFocus: true,
                    }}
                  />
                )}
              />
              <div className="invalid-feedback">
                {errors.PhoneNumber?.message}
              </div>
            </div>
            <div className="col-md-12 form-group customInput">
              <label>Email Address</label>
              <i className="far fa-envelope"></i>
              <input
                name="Email"
                id="Email"
                placeholder="Email Address"
                defaultValue={employer.Email}
                type="text"
                ref={register}
                className={`form-control ${errors.Email ? "is-invalid" : ""}`}
                onChange={handleInputChange}
              />
              <div className="invalid-feedback">{errors.Email?.message}</div>
            </div>
            <div className="col-md-6 form-group customInput">
              <label>Password</label>
              <i className="far fa-lock"></i>

              <input
                name="Password"
                id="Password"
                placeholder="Password"
                defaultValue={employer.Password}
                type="password"
                ref={register}
                className={`form-control ${
                  errors.Password ? "is-invalid" : ""
                }`}
                onChange={handleInputChange}
              />
              <div className="invalid-feedback">{errors.Password?.message}</div>
            </div>
            <div className="col-md-6 form-group customInput">
              <label>Retype Password</label>
              <i className="far fa-lock"></i>
              <input
                name="confirmedPassword"
                id="confirmedPassword"
                placeholder="Retype Password"
                defaultValue={employer.confirmedPassword}
                type="password"
                ref={register}
                className={`form-control ${
                  errors.confirmedPassword ? "is-invalid" : ""
                }`}
                onChange={handleInputChange}
              />
              <div className="invalid-feedback">
                {errors.confirmedPassword?.message}
              </div>
            </div>
            <div className="col-md-12 pull-right">
              <Button
                isLoading={registering && isLoading}
                border="none"
                fontSize="16px"
                font="Proxima-Regular"
                color="white"
                bgColor="#0195FF"
                height="35px"
                onClick={() => setisLoading(false)}
                radius=""
                width="180px"
                children="Sign up"
              />
            </div>
          </form>
        </div>
        <div className="auth-logo-img">
          <Anime delay={anime.stagger(400)} scale={[0.1, 0.9]}>
            <img src={Logo} />
          </Anime>
        </div>
      </section>
    </React.Fragment>
  );
}
export default SignUpEmployer;
