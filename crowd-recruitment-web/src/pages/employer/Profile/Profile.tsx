import React, { useEffect, useState } from "react";
import { createStyles, makeStyles, Theme } from "@material-ui/core/styles";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import Grid from "@material-ui/core/Grid";
import thank from "../../../src/assets/thankful.png";

import PasswordSettings from "./PasswordSettings";
import PackageSettings from "./PackageSettings";
import InfoSettings from "./InfoSettings";
import { employerProfileConstants } from "../../../constants/EmployerProfileConstants";

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {},
  })
);

export default function ProfileEmployer(props: any) {
  const classes = useStyles();

  const [currentTab, setCurrentTab] = useState(props.match.params.id);

  const toggleTab = (args: any) => {
    setCurrentTab(args);
  };

  useEffect(() => {
    // fetch your data when the props.location changes
    setCurrentTab(props.match.params.id);
  }, [props.match.params.id]);

  return (
    <React.Fragment>
      <div className="">
        <div className="row">
          <div className="col-md-12">
            <div
              className="bread-text pull-left"
              style={{ paddingBottom: "13px" }}
            >
              <span className="lt-head-bold">Profile Settings</span>
            </div>
          </div>
        </div>
        <div className="row">
          <div className="col-md-3">
            <div className="tabBox">
              <div
                onClick={() => toggleTab(employerProfileConstants.BASICINFO)}
                className={
                  currentTab == employerProfileConstants.BASICINFO
                    ? "tabList activeTab"
                    : "tabList"
                }
              >
                <i className="fal fa-info-circle"></i>
                Basic Info
              </div>
              <div
                onClick={() =>
                  toggleTab(employerProfileConstants.CHANGEPASSWORD)
                }
                className={
                  currentTab == employerProfileConstants.CHANGEPASSWORD
                    ? "tabList activeTab"
                    : "tabList"
                }
              >
                <i className="fal fa-lock"></i>
                Change password
              </div>
              <div
                onClick={() => toggleTab(employerProfileConstants.PACKAGES)}
                className={
                  currentTab == employerProfileConstants.PACKAGES
                    ? "tabList activeTab"
                    : "tabList"
                }
              >
                <i className="fal fa-archive"></i>
                Change Your Package{" "}
              </div>
            </div>
          </div>

          <div className="col-md-9">
            <div className="tabContent">
              <div>
                {currentTab == employerProfileConstants.BASICINFO && (
                  <InfoSettings data="data" />
                )}
              </div>

              <div className="">
                {currentTab == employerProfileConstants.CHANGEPASSWORD && (
                  <PasswordSettings data="data" />
                )}
              </div>

              {currentTab == employerProfileConstants.PACKAGES && (
                <PackageSettings data="data" />
              )}
            </div>
          </div>
        </div>
      </div>
    </React.Fragment>
  );
}
