import React, { useEffect, useState } from "react";
import "react-phone-input-2/lib/style.css";
import { IEmployer } from "../../../models/IEmployer";
import { yupResolver } from "@hookform/resolvers/yup";
import { Dispatch } from "redux";
import { alertActions } from "../../../actions/alertActions";
import { useDispatch } from "react-redux";
import * as Yup from "yup";
import { IResponseData } from '../../../models/IResponseData';
import { useForm, Controller } from "react-hook-form";
import EmployerService from "../../../services/EmployerService";
import Button from "../../../components/Buttons/Button";
import PackageService from "../../../services/PackageService";

interface Props {
  data: string;
}

const PackageSettings: React.FC<Props> = ({ data }) => {

  const [isLoading, setisLoading] = useState(false);

  const dispatch = useDispatch();
    const [selectedPackage, setSelectedPackage] = useState(1);
      const selectPackage = (args:any) => {
        setSelectedPackage(args);
      };

      const initialPackageList = [
        {
          uuid: "",
          text: "",
          packageId: 0
        },
      ];

      const initialEmployerDetails: IEmployer = {
        companyName: "",
        firstName: "",
        lastName: "",
        phoneNumber: "",
        email: "",
        password: "",
        newPassword: "",
        confirmedPassword: "",
        packageId: 0,
        uuid: ""
      };

      const [packageList, setPackageList] = React.useState(initialPackageList);

  const [allPricePackage, setAllPricePackage] = useState([]);

  const [employerDetails, setEmployerDetails] = useState(initialEmployerDetails);

  useEffect(() => {
    GetAllPackages();
    GetEmployerDetails();
  }, []);

  const GetAllPackages = () => {
    debugger;
    PackageService
      .GetAllPackages()
      .then((response) => {
        debugger;
        setAllPricePackage(response.data.data.packages);
        setPackageList(response.data.data.packagesFeatures);
      })
      .catch((e) => {
        console.log(e);
      });
  };

  const GetEmployerDetails = () => {
    debugger;
    EmployerService
      .GetEmployerDetalis()
      .then((response) => {
        debugger;
        setEmployerDetails(response.data.data);
        setSelectedPackage(response.data.data.packageId);
      })
      .catch((e) => {
        console.log(e);
      });
  };
  
  const UpdatePackage = () => {
    debugger;
    if(Number(selectedPackage) > 0){
      setisLoading(true);
      employerDetails.packageId = selectedPackage;
      EmployerService
      .UpdatePackage(employerDetails)
        .then(
          user => { 
            debugger;
            GetEmployerDetails();
            let responseDetails : IResponseData  =  user.data;
            dispatch(
              alertActions.success(responseDetails.message));
            setisLoading(false);
          },
          error => {
            debugger;
            let responseDetails : IResponseData  =  error.response.data
            dispatch(alertActions.error(responseDetails.message));
            setisLoading(false);
          }
      );
    }

    else{
      setisLoading(false);
      dispatch(alertActions.error("Select at least one package"));
    }
    
  };

  const drawPackages = allPricePackage.map((item) => (
    <div className='col-md-4'>
    <div className='packageBox'>
    <div className='packageUpper'>
    <div>{item.packageTitle}</div>
    <div className='package-price'>${item.packagePrice}</div>
    <div>per month, paid yearly</div>
    </div>
    <div className='packageBox-body'>
    <ul>
      
      {packageList.filter((temp) => temp.packageId == item.packageId
    )
    .map((k) => {
      return (
        <li>
          {k.text}
      </li>
      );
    })}
    </ul>
    </div>
    </div>
    
    <div
                onClick={() => selectPackage(item.packageId)}
                className={
                selectedPackage == item.packageId
                ? "selectPackageBtn activePackage"
                : "selectPackageBtn"
                }
              >
                
              </div>
              
    </div>

 ));

  return (
    <div className="row">
      {drawPackages}

      <div className='col-md-12 pull-right'>
      <Button
                                isLoading={isLoading}
                                border="none"
                                fontSize="16px"
                                font="Proxima-Medium"
                                color="white"
                                bgColor="#0195FF"
                                height="35px"
                                onClick={UpdatePackage}
                                radius=""
                                width="124px"
                                children="Save Changes"
                            />
      </div>
</div>
  );
};

export default PackageSettings;
