import React, { useEffect, useState } from "react";
import { yupResolver } from "@hookform/resolvers/yup";
import { Dispatch } from "redux";
import { alertActions } from "../../../actions/alertActions";
import { useForm, Controller } from "react-hook-form";
import { useDispatch } from "react-redux";
import * as Yup from "yup";
import { IResponseData } from "../../../models/IResponseData";
import { IUpdatePassword } from "../../../models/IUpdatePassword";
import UserService from "../../../services/UserService";
import Button from "../../../components/Buttons/Button";

interface Props {
  data: string;
}

const PasswordSettings: React.FC<Props> = ({ data }) => {
  const handlePasswordInputChange = (event: any) => {
    debugger;
    const { name, value } = event.target;
    setUpdatePassword({ ...updatePassword, [name]: value });
  };

  const initialUpdatePassword: IUpdatePassword = {
    newPassword: "",
    password: "",
    uuid: "",
    confirmedPassword: "",
  };

  const [updatePassword, setUpdatePassword] = useState(initialUpdatePassword);

  const [isLoading, setisLoading] = useState(false);

  const dispatch = useDispatch();
  const validationPasswordSchema = Yup.object().shape({
    password: Yup.string()
      .trim()
      .min(8, "Password must be at least 8 digits")
      .required("Old password is required"),
    newPassword: Yup.string()
      .trim()
      .min(8, "Password must be at least 8 digits")
      .required("New password is required"),
    confirmedPassword: Yup.string().oneOf(
      [Yup.ref("newPassword"), null],
      "Two passwords must match"
    ),
  });

  const {
    register: register2,
    handleSubmit: handleSubmit2,
    reset: reset2,
    errors: errors2,
  } = useForm({
    resolver: yupResolver(validationPasswordSchema),
    mode: "onBlur",
  });

  function onSubmit2(data: any) {
    debugger;
    UpdatePassword();
  }

  const UpdatePassword = () => {
    debugger;
    setisLoading(true);
    UserService.UpdatePassword(updatePassword).then(
      (user) => {
        debugger;
        let responseDetails: IResponseData = user.data;
        dispatch(alertActions.success(responseDetails.message));
        setisLoading(false);
      },
      (error) => {
        debugger;
        let responseDetails: IResponseData = error.response.data;
        dispatch(alertActions.error(responseDetails.message));
        setisLoading(false);
      }
    );
  };

  return (
    <form className="" onSubmit={handleSubmit2(onSubmit2)}>
      <div className="row">
        <div className="form-group customInput col-md-12">
          <label>Old Password</label>
          <i className="far fa-lock"></i>

          <input
            type="password"
            name="password"
            id="password"
            placeholder="Old Password"
            defaultValue={updatePassword.password}
            ref={register2}
            className={`form-control ${errors2.password ? "is-invalid" : ""}`}
            onChange={handlePasswordInputChange}
          />
          <div className="invalid-feedback">{errors2.password?.message}</div>
        </div>

        <div className="form-group customInput col-md-6">
          <label>New Password</label>
          <i className="far fa-lock"></i>
          <input
            name="newPassword"
            id="newPassword"
            placeholder="New Password"
            defaultValue={updatePassword.newPassword}
            type="password"
            ref={register2}
            className={`form-control ${
              errors2.newPassword ? "is-invalid" : ""
            }`}
            onChange={handlePasswordInputChange}
          />
          <div className="invalid-feedback">{errors2.newPassword?.message}</div>
        </div>

        <div className="form-group customInput col-md-6">
          <label>Confirm Password</label>
          <i className="far fa-lock"></i>

          <input
            name="confirmedPassword"
            id="confirmedPassword"
            placeholder="Confirm Password"
            defaultValue={updatePassword.confirmedPassword}
            type="password"
            ref={register2}
            className={`form-control ${
              errors2.confirmedPassword ? "is-invalid" : ""
            }`}
            onChange={handlePasswordInputChange}
          />
          <div className="invalid-feedback">
            {errors2.confirmedPassword?.message}
          </div>
        </div>

        <div className="col-md-12 pull-right">
          <Button
            isLoading={isLoading}
            border="none"
            fontSize="16px"
            font="Proxima-Medium"
            color="white"
            bgColor="#0195FF"
            height="35px"
            onClick={void 0}
            radius=""
            width="124px"
            children="Save Changes"
          />
        </div>
      </div>
    </form>
  );
};

export default PasswordSettings;
