import React, { useEffect, useState } from "react";
import PhoneInput from "react-phone-input-2";
import "react-phone-input-2/lib/style.css";
import { IEmployer } from "../../../models/IEmployer";
import { yupResolver } from "@hookform/resolvers/yup";
import { Dispatch } from "redux";
import { alertActions } from "../../../actions/alertActions";
import { useDispatch } from "react-redux";
import * as Yup from "yup";
import { IResponseData } from '../../../models/IResponseData';
import { useForm, Controller } from "react-hook-form";
import EmployerService from "../../../services/EmployerService";
import Button from "../../../components/Buttons/Button";


interface Props {
  data: string;
}

const InfoSettings: React.FC<Props> = ({ data }) => {
  
  const initialEmployerDetails: IEmployer = {
    companyName: "",
    firstName: "",
    lastName: "",
    phoneNumber: "",
    email: "",
    password: "",
    newPassword: "",
    confirmedPassword: "",
    packageId: 0,
    uuid: ""
  };

  const [employerDetails, setEmployerDetails] = useState(initialEmployerDetails);

  const [isLoading, setisLoading] = useState(false);

  const handleInputChange = (event: any) => {
    const { name, value } = event.target;
    setEmployerDetails({ ...employerDetails, [name]: value });
  };

  const dispatch = useDispatch();

  // form validation rules
  const validationSchema = Yup.object().shape({
    companyName: Yup.string().trim().required("Company name is required"),
    firstName: Yup.string().trim().required("First name is required"),
    lastName: Yup.string().trim().required("Last name is required"),
    // phoneNumber: Yup.string()
    //   .trim()
    //   .required("Phone number is required")
    //   .matches(/^[0-9]+$/,"Phone number Must be only digits"),
    email: Yup.string()
      .trim()
      .required("Email is required")
      .email("Email is invalid"),
  });

  // functions to build form returned by useForm() hook
  const { register, handleSubmit, reset, errors, control } = useForm({
    resolver: yupResolver(validationSchema),
  });

  function onSubmit(data: any) {
    debugger;
    UpdateEmployerDetails();
  }

  useEffect(() => {
    GetEmployerDetails();
  }, []);

  const GetEmployerDetails = () => {
    debugger;
    EmployerService
      .GetEmployerDetalis()
      .then((response) => {
        debugger;
        setEmployerDetails(response.data.data);
      })
      .catch((e) => {
        console.log(e);
      });
  };

  const UpdateEmployerDetails = () => {
    debugger;
    setisLoading(true);
    EmployerService
      .UpdateEmployerDetails(employerDetails)
        .then(
          user => { 
            debugger;
            GetEmployerDetails();
            let responseDetails : IResponseData  =  user.data;
            dispatch(
              alertActions.success(responseDetails.message));
            setisLoading(false);

          },
          error => {
            debugger;
            let responseDetails : IResponseData  =  error.response.data
            dispatch(alertActions.error(responseDetails.message));
            setisLoading(false);

          }
      );
  };

  function handleOnChange(
    value: any,
    data: any,
    event: any,
    formattedValue: any
  ) {
    console.log("value", value);
    employerDetails.phoneNumber = value;
    setEmployerDetails(employerDetails);
  }

  return (

    <div className='row'>
      <form onSubmit={handleSubmit(onSubmit)} onReset={reset} >
      <div className='form-group customInput col-md-12'>
      <label>Company Name</label>
      <i className="far fa-briefcase"></i>
      
    <input
                  name="companyName"
                  id="companyName"
                  placeholder="Company Name"
                  value={employerDetails.companyName}
                  type="text"
                  ref={register}
                  className={`form-control ${
                    errors.companyName ? "is-invalid" : ""
                  }`}
                  onChange={handleInputChange}
                />
                <div className="invalid-feedback">
                  {errors.companyName?.message}
                </div>
      </div>

      <div className='form-group customInput col-md-6'>
      <label>First Name</label>
      <i className="far fa-user"></i>
    <input
                  name="firstName"
                  id="firstName"
                  placeholder="First Name"
                  value={employerDetails.firstName}
                  type="text"
                  ref={register}
                  className={`form-control ${
                    errors.firstName ? "is-invalid" : ""
                  }`}
                  onChange={handleInputChange}
                />
                <div className="invalid-feedback">
                  {errors.firstName?.message}
                </div>
      </div>

      <div className='form-group customInput col-md-6'>
      <label>Last Name</label>
      <i className="far fa-user"></i>
      
    <input
                  name="lastName"
                  id="lastName"
                  placeholder="Last Name"
                  value={employerDetails.lastName}
                  type="text"
                  ref={register}
                  className={`form-control ${
                    errors.lastName ? "is-invalid" : ""
                  }`}
                  onChange={handleInputChange}
                />
                <div className="invalid-feedback">
                  {errors.lastName?.message}
                </div>
      </div>

      <div className="col-md-6 form-group customInput phoneInput">
              <label>Phone Number</label>
              <i className="far fa-phone"></i>
                  
                  <Controller
                  name="phoneNumber"
                  control={control}
                  defaultValue=""
                  render={({ name, onBlur, onChange, value }) => (
                    <PhoneInput
                      value={employerDetails.phoneNumber}
                      onBlur={onBlur}
                      onChange={handleOnChange}
                      country={"sa"}
                      inputProps={{
                        name: "phoneNumber",
                        id: "phoneNumber",
                        required: true,
                        autoFocus: true,
                      }}
                    />
                  )}
                />
                <div className="invalid-feedback">
                  {errors.phoneNumber?.message}
                </div>
            </div>

            <div className='form-group customInput col-md-6'>
            <label>Email</label>
            <i className="far fa-envelope"></i>
          <input
                  name="email"
                  id="email"
                  placeholder="Email"
                  value={employerDetails.email}
                  type="text"
                  ref={register}
                  className={`form-control ${errors.email ? "is-invalid" : ""}`}
                  onChange={handleInputChange}
                />
                <div className="invalid-feedback">{errors.email?.message}</div>
            </div>
      
      <div className='col-md-12 pull-right'>
      <Button
                                isLoading={isLoading}
                                border="none"
                                fontSize="16px"
                                font="Proxima-Medium"
                                color="white"
                                bgColor="#0195FF"
                                height="35px"
                                onClick={void (0)}
                                radius=""
                                width="124px"
                                children="Save Changes"
                            />
      </div>
      
      </form>
      </div>
    
    
  );
};

export default InfoSettings;
