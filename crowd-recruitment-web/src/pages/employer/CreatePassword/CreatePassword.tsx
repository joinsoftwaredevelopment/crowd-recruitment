import React, { useState } from "react";
import PhoneInput from "react-phone-input-2";
import "react-phone-input-2/lib/style.css";
import Logo from "../../../../src/assets/cr-logo.png";
import TopDots from "../../../../src/assets/dots-top.png";
import BottomDots from "../../../../src/assets/dots-bottom.png";
import { CircularProgress } from "@material-ui/core";
import Anime from "react-anime";
import anime from "animejs/lib/anime.es.js";
import Button from "../../../components/Buttons/Button";
import { useHistory } from "react-router-dom";
import { yupResolver } from "@hookform/resolvers/yup";
import * as Yup from "yup";
import { IRsetPassword } from "../../../models/IRsetPassword";
import UserService from "../../../services/UserService";
import { Dispatch } from "redux";
import { alertActions } from "../../../actions/alertActions";
import { useDispatch } from "react-redux";
import { useForm } from "react-hook-form";
import { history } from "../../../helpers/history";

function EmployerConfirmPassword(props: any) {
  const history = useHistory();
  const initialUserState = {
    Password: "",
    ConfirmPassword: "",
    uuid: "",
  };

  const dispatch = useDispatch();

  const [user, setUser] = useState(initialUserState);
  const [isLoading, setisLoading] = useState(false);

  const handleInputChange = (event: any) => {
    debugger;
    const { name, value } = event.target;
    setUser({ ...user, [name]: value });
  };

  const validationSchema = Yup.object().shape({
    Password: Yup.string()
      .trim()
      .min(8, "Password must be at least 8 digits")
      .required("Password is required"),
    ConfirmPassword: Yup.string().oneOf(
      [Yup.ref("Password"), null],
      "Two passwords must match"
    ),
  });

  // functions to build form returned by useForm() hook
  const { register, handleSubmit, reset, errors } = useForm({
    resolver: yupResolver(validationSchema),
  });

  function onSubmit(data: IRsetPassword) {
    debugger;
    setisLoading(true);
    data.Uuid = props.match.params.id;
    debugger;
    UserService.ResePassword(data)
      .then((response) => {
        history.push("/employerlogin");
        setisLoading(false);
      })
      .catch((e) => {
        dispatch(alertActions.error("Failed to update your password"));
        setisLoading(false);
      });
  }
  return (
    <React.Fragment>
      <section className="authLayout">
        <div style={{ paddingBottom: "90px" }} className="auth-form">
          <div className="topDots">
            <Anime delay={anime.stagger(400)} scale={[0.1, 0.9]}>
              <img src={TopDots} />
            </Anime>
          </div>

          <div className="bottomDots">
            <Anime delay={anime.stagger(400)} scale={[0.1, 0.9]}>
              <img src={BottomDots} />
            </Anime>
          </div>

          <div
            className="back-link"
            onClick={() => history.push("/employerHome")}
          >
            <i className="fa fa-arrow-left"></i>
            Back to home page
          </div>

          <div style={{ marginBottom: "50px" }} className="auth-head">
            Create new password
          </div>
          <form
            className="row"
            onSubmit={handleSubmit(onSubmit)}
            onReset={reset}
          >
            <div className="col-md-12 form-group customInput">
              <label>New password</label>
              <i className="far fa-lock"></i>
              <input
                name="Password"
                id="Password"
                placeholder="New password"
                defaultValue={user.Password}
                type="password"
                ref={register}
                className={`form-control ${
                  errors.Password ? "is-invalid" : ""
                }`}
                onChange={handleInputChange}
              />
              <div className="invalid-feedback">{errors.Password?.message}</div>
            </div>

            <div className="col-md-12 form-group customInput">
              <label>Confirm password</label>
              <i className="far fa-lock"></i>
              <input
                name="ConfirmPassword"
                id="ConfirmPassword"
                placeholder="Confirm password"
                defaultValue={user.ConfirmPassword}
                type="password"
                ref={register}
                className={`form-control ${
                  errors.ConfirmPassword ? "is-invalid" : ""
                }`}
                onChange={handleInputChange}
              />
              <div className="invalid-feedback">
                {errors.ConfirmPassword?.message}
              </div>
            </div>

            <div className="col-md-12 pull-right">
              <Button
                isLoading={isLoading}
                border="none"
                fontSize="16px"
                font="Proxima-Regular"
                color="white"
                bgColor="#0195FF"
                height="35px"
                onClick={void 0}
                radius=""
                width="180px"
                children="Finish"
              />
            </div>
          </form>
        </div>
        <div className="auth-logo-img">
          <Anime delay={anime.stagger(400)} scale={[0.1, 0.9]}>
            <img src={Logo} />
          </Anime>
        </div>
      </section>
    </React.Fragment>
  );
}
export default EmployerConfirmPassword;
