import React, { useState } from "react";
import PhoneInput from "react-phone-input-2";
import "react-phone-input-2/lib/style.css";
import Logo from "../../../../src/assets/cr-logo.png";
import TopDots from "../../../../src/assets/dots-top.png";
import BottomDots from "../../../../src/assets/dots-bottom.png";
import { CircularProgress } from "@material-ui/core";
import Anime from "react-anime";
import anime from "animejs/lib/anime.es.js";
import Button from "../../../components/Buttons/Button";
import { useHistory } from "react-router-dom";
import { useForm } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import * as Yup from "yup";
import { IUser } from "../../../models/IUser";
import { userActions } from "../../../actions/userActions";
import { RootStateOrAny, useDispatch, useSelector } from "react-redux";

function LoginEmployer() {
  const history = useHistory();
  let currentContent: any;
  const [isLoading, setisLoading] = useState(false);

  let content = {
    English: {
      Email: "Email",
      password: "Password",
      logIn: "Sign In",
      employer: "Employer",
      sourcer: "Sourcer",
      Emailisrequired: "Email is required",
      Passwordisrequired: "Password is required",
      Forgotpassword: "Forgot password ?",
    },
    Arabic: {
      Email: "البريد الإلكترونى",
      password: "كلمة المرور",
      logIn: "تسجيل الدخول",
      employer: "مقدم الوظيفة",
      sourcer: "CV مقدم",
      Emailisrequired: "البريد الإلكتروني مطلوب",
      Passwordisrequired: "كلمة المرور مطلوبة",
      Forgotpassword: "نسيت كلمةالمرور ؟",
    },
  };

  localStorage.getItem("currentLanguage") === "Arabic"
    ? (currentContent = content.Arabic)
    : (currentContent = content.English);

  const initialUserState = {
    Email: "",
    Password: "",
    RoleId: 2,
    submitted: false,
  };

  const dispatch = useDispatch();

  const [user, setUser] = useState(initialUserState);
  const [count, setCount] = useState("emp");

  const handleInputChange = (event: any) => {
    debugger;
    const { name, value } = event.target;
    setUser({ ...user, [name]: value });
  };

  // Validation
  // form validation rules
  const validationSchema = Yup.object().shape({
    Email: Yup.string().trim().required(currentContent.Emailisrequired).email(),

    Password: Yup.string().trim().required(currentContent.Passwordisrequired),
  });

  // functions to build form returned by useForm() hook
  const { register, handleSubmit, reset, errors } = useForm({
    resolver: yupResolver(validationSchema),
  });

  const registering = useSelector(
    (state: RootStateOrAny) => state.registration.registering
  );

  function onSubmit(data: IUser) {
    debugger;
    setisLoading(true);
    data.RoleId = 2;
    if (count == "sour") {
      data.RoleId = 3;
    }
    debugger;
    dispatch(userActions.login(data));
  }

  return (
    <React.Fragment>
      <section className="authLayout">
        <div style={{ paddingBottom: "90px" }} className="auth-form">
          <div className="topDots">
            <Anime delay={anime.stagger(400)} scale={[0.1, 0.9]}>
              <img src={TopDots} />
            </Anime>
          </div>

          <div className="bottomDots">
            <Anime delay={anime.stagger(400)} scale={[0.1, 0.9]}>
              <img src={BottomDots} />
            </Anime>
          </div>

          <div
            className="back-link"
            onClick={() => history.push("/employerHome")}
          >
            <i className="fa fa-arrow-left"></i>
            Back to home page
          </div>

          <div style={{ marginBottom: "50px" }} className="auth-head">
            Login as Employer
          </div>
          <form
            onSubmit={handleSubmit(onSubmit)}
            onReset={reset}
            className="row"
          >
            <div className="col-md-12 form-group customInput">
              <label>Email Address</label>
              <i className="far fa-envelope"></i>
              <input
                name="Email"
                id="Email"
                placeholder="Email Address"
                defaultValue={user.Email}
                type="text"
                ref={register}
                className={`form-control ${errors.Email ? "is-invalid" : ""}`}
                onChange={handleInputChange}
              />
              <div className="invalid-feedback">{errors.Email?.message}</div>
            </div>
            <div className="col-md-12 form-group customInput">
              <label>Password</label>
              <i className="far fa-lock"></i>

              <input
                name="Password"
                id="Password"
                placeholder="Password"
                defaultValue={user.Password}
                type="password"
                ref={register}
                className={`form-control ${
                  errors.Password ? "is-invalid" : ""
                }`}
                onChange={handleInputChange}
              />
              <div className="invalid-feedback">{errors.Password?.message}</div>
            </div>

            <div className="col-md-12 pull-right">
              <span
                onClick={() => {
                  history.push("/employerforgotpassword");
                }}
                className="red newFPassword"
              >
                Forgot Password?
              </span>
              <Button
                isLoading={registering && isLoading}
                border="none"
                fontSize="16px"
                font="Proxima-Regular"
                color="white"
                bgColor="#0195FF"
                height="35px"
                onClick={() => setisLoading(false)}
                radius=""
                width="180px"
                children="Login"
              />
            </div>
          </form>
        </div>
        <div className="auth-logo-img">
          <Anime delay={anime.stagger(400)} scale={[0.1, 0.9]}>
            <img src={Logo} />
          </Anime>
        </div>
      </section>
    </React.Fragment>
  );
}
export default LoginEmployer;
