import React, { useEffect, useState } from "react";
import { createStyles, makeStyles, Theme } from "@material-ui/core/styles";
import TextField from "@material-ui/core/TextField";
import Button from "../../../components/Buttons/Button";
import Grid from "@material-ui/core/Grid";
import thank from "../../../src/assets/thankful.png";
import Select from "react-select";
import jobService from "../../../services/JobService";
import { IJobIndustry } from "../../../models/IJobIndustry";
import { IJobDetails } from "../../../models/IJobDetails";
import { history } from "../../../helpers/history";

export default function SelectTemplateEmployer() {
  const [selectedTemplate, setselectedTemplate] = React.useState("");
  const setTemplateClass = (args: any) => {
    setselectedTemplate(args);
  };

  const options = [
    { value: "chocolate", label: "Chocolate" },
    { value: "strawberry", label: "Strawberry" },
    { value: "vanilla", label: "Vanilla" },
  ];
  const [count, setCount] = useState(1);

  const [industries, setIndustries] = useState<IJobIndustry[]>([]);
  const [templates, setTemplates] = useState<any[]>([]);
  const [allTemplates, setAllTemplates] = useState<any[]>([]);
  const [currentTempId, setCurrentTempId] = useState(2);

  const [currentSelectedTemplate, setCurrentSelectedTemplate] = useState<any>({
    jobTemplateId: 2,
    jobTitle: "Data Analyst / Data Scientist",
  });

  const [currentTemplate, setCurrentTemplate] = useState<IJobDetails>({
    uuid: "",
    jobTitle: "",
    employmentTypeName: "",
    seniorityLevelName: "",
    companiesNotToSourceFrom: "",
    languageId: 2,
    jobLocation: "",
    companyName: "",
    mustHaveQualification: "",
    niceToHaveQualification: "",
    hiringNeeds: "",
    experienceLevelName: "",
    description: "",
    companyIndustry: "",
    jobStatusId: 3,
    jobStatusName: "",
    requirements: "",
    noOfSubmissions: 0,
    jobDate: new Date(),
  });

  function handleChange(selectedOption: any) {
    debugger;
    setCurrentSelectedTemplate(selectedOption);
    setCurrentTempId(selectedOption.jobTemplateId);
    GetTemplateById(selectedOption.jobTemplateId);
  }

  useEffect(() => {
    GetIndustries();
    GetTemplatesByIndustry(1);
    GetTemplateById(2);
    GetAllJobTemps();
  }, []);

  const GetTemplatesByIndustry = (industryId: number) => {
    debugger;
    setCount(industryId);
    jobService
      .GetTemplatesByIndustry(industryId)
      .then((response) => {
        debugger;
        setTemplates(response.data.data);
      })
      .catch((e) => {
        console.log(e);
      });
  };

  const GetIndustries = () => {
    jobService
      .GetIndustries()
      .then((response) => {
        debugger;
        setIndustries(response.data.data);
      })
      .catch((e) => {
        console.log(e);
      });
  };

  const GetAllJobTemps = () => {
    jobService
      .GetAllJobTemps()
      .then((response) => {
        debugger;
        setAllTemplates(response.data.data);
      })
      .catch((e) => {
        console.log(e);
      });
  };

  const GetTemplateById = (templateId: number) => {
    debugger;
    setTemplateClass(templateId);
    setCurrentTempId(templateId);
    jobService
      .GetTemplateById(templateId)
      .then((response) => {
        debugger;
        setCurrentTemplate(response.data.data);
      })
      .catch((e) => {
        console.log(e);
      });
  };

  return (
    <React.Fragment>
      <div className="">
        <div className="row">
          <div className="col-md-12">
            <div className="bread-text pull-left">
              <span className="lt-head-bold">Post Job</span>
            </div>
          </div>
        </div>
        <div className="row">
          <div className="col-md-3">
            <div className="form-group">
              <label>Search a specialist </label>

              <Select
                options={allTemplates}
                value={currentSelectedTemplate}
                getOptionLabel={(option) => option.jobTitle}
                getOptionValue={(option) => option.jobTemplateId.toString()}
                onChange={handleChange}
              />
            </div>

            <div className="form-group listing-content">
              <div className="parent-listing">
                {industries.map((m) => {
                  return (
                    <div className="parent-listing">
                      <div className="parent-listing-row">{m.translation}</div>

                      {allTemplates
                        .filter(
                          (temp) => temp.jobIndustryId == m.companyIndustryId
                        )
                        .map((k) => {
                          return (
                            <div
                              className={
                                selectedTemplate == k.jobTemplateId
                                  ? "listing-content-row activeTemp"
                                  : "listing-content-row"
                              }
                              onClick={() => GetTemplateById(k.jobTemplateId)}
                            >
                              {k.jobTitle}
                            </div>
                          );
                        })}
                    </div>
                  );
                })}
              </div>
            </div>

            <div className="pull-right" style={{ marginTop: "30px" }}>
              <Button
                isLoading={false}
                border="none"
                fontSize="16px"
                font="Proxima-Regular"
                color="white"
                bgColor="#0195FF"
                height="35px"
                onClick={() =>
                  history.push(
                    "/employer/employerjobdetails/0/" + currentTempId
                  )
                }
                radius=""
                width="124px"
                children="Next"
              />
            </div>
          </div>
          <div className="col-md-6">
            <div className="sub-info-box">
              <div className="feature-set">
                <div className="bold-head eBold font-16">
                  {currentTemplate.jobTitle}
                </div>
              </div>

              <div className="feature-set-info">
                <div className="sub-feature">
                  <div className="bold-head font-14">Job Description</div>

                  <div
                    className="light-text"
                    dangerouslySetInnerHTML={{
                      __html: currentTemplate.description,
                    }}
                  />
                </div>
              </div>

              <div className="feature-set-info">
                <div className="sub-feature">
                  <div className="bold-head font-14">Requirements</div>

                  <div
                    className="light-text"
                    dangerouslySetInnerHTML={{
                      __html: currentTemplate.requirements,
                    }}
                  />
                </div>
              </div>

              <div className="feature-set-info">
                <div className="sub-feature">
                  <div className="bold-head font-14">
                    Must-have qualifications
                  </div>

                  <div className="light-text">
                    {currentTemplate.mustHaveQualification}
                  </div>
                </div>
              </div>

              <div className="feature-set-info">
                <div className="sub-feature">
                  <div className="bold-head font-14">
                    Nice to have qualifications
                  </div>

                  <div className="light-text">
                    {currentTemplate.niceToHaveQualification}
                  </div>
                </div>
              </div>

              <div className="feature-set-info">
                <div className="sub-feature">
                  <div className="bold-head font-14">
                    Companies not to source from
                  </div>

                  <div className="light-text">
                    {currentTemplate.companiesNotToSourceFrom}
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className="col-md-3">
            <div className="sub-info-box">
              <div className="feature-set">
                <div className="bold-head eBold font-16">Job Details</div>
              </div>

              {/* <div className="feature-set-row">
                <div className="bold-head font-14">
                  <span>
                    <i className="fa fa-building"></i>
                  </span>
                  <span>Company name</span>
                </div>

                <div className="light-text">{currentTemplate.companyName}</div>
              </div> */}

              {/* <div className="feature-set-row">
                <div className="bold-head font-14">
                  <span>
                    <i className="fa fa-chart-bar"></i>
                  </span>
                  <span>Seniority level</span>
                </div>

                <div className="light-text">{currentTemplate.seniorityLevelName}</div>
              </div> */}

              <div className="feature-set-row">
                <div className="bold-head font-14">
                  <span>
                    <i className="fa fa-clock"></i>
                  </span>
                  <span>Employment type</span>
                </div>

                <div className="light-text">
                  {currentTemplate.employmentTypeName}
                </div>
              </div>

              <div className="feature-set-row">
                <div className="bold-head font-14">
                  <span>
                    <i className="fa fa-location"></i>
                  </span>
                  <span>Location</span>
                </div>

                <div className="light-text">{currentTemplate.jobLocation}</div>
              </div>

              <div className="side-set">
                {/* <div className="feature-set-row">
                  <div className="bold-head font-14">
                    <span>Company Industry</span>
                  </div>

                  <div className="light-text">
                    {currentTemplate.companyIndustry}
                  </div>
                </div> */}

                <div className="feature-set-row">
                  <div className="bold-head font-14">
                    <span>Work experience</span>
                  </div>

                  <div className="light-text">
                    {/* {currentTemplate.experienceLevelName} */} 4 - 6
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </React.Fragment>
  );
}
