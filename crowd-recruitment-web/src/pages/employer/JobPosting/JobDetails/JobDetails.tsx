import React, { useEffect, useState } from "react";
import { createStyles, makeStyles, Theme } from "@material-ui/core/styles";
import TextField from "@material-ui/core/TextField";
import Button from "../../../../components/Buttons/Button";
import Grid from "@material-ui/core/Grid";
import thank from "../../../src/assets/thankful.png";
import { EditorState, convertToRaw, ContentState } from "draft-js";
import Draft, {
  htmlToDraft,
  EmptyState,
  rawToDraft,
  draftToRaw,
  draftStateToHTML,
} from "react-wysiwyg-typescript";

import draftToHtml from "draftjs-to-html";
import Filters from "../Filters";
import { IPostJobDetailsCountries } from "../../../../models/IPostJobDetailsCountries";
import EmployerService from "../../../../services/EmployerService";
import { useForm } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import * as Yup from "yup";
import { useDispatch, useSelector } from "react-redux";
import { history } from "../../../../helpers/history";
import { IPostJobDetails } from "../../../../models/IPostJobDetails";
import JobService from "../../../../services/JobService";
import Select from "react-select";


const colourStyles = {
  control: (styles:any) => ({ ...styles, paddingLeft: '0px',
  borderTop: 0,
  borderLeft: 0,
  borderRight: 0,
  borderRadius: '0px',
  })
}

export default function EmployerJobDetails(props: any) {
  const initialJobState = {
    uuId: "",
    jobTitle: "",
    seniorityLevelId: 1,
    employerTypeId: 1,
    jobDescription: "",
    industryId: 1,
    jobLocationId: 1,
    jobNationalityId: 189,
    requirements: "",
    jobTemplateId: 0,
  };

  const [isLoading, setisLoading] = useState(false);

  const [currentCountries, setcurrentCountries] = useState<
    IPostJobDetailsCountries[]
  >([]);

  const [descriptionError, setDescriptionError] = useState(false);
  const [requiredError, setRequiredError] = useState(false);

  useEffect(() => {
    retrieveJob(props.match.params.id, props.match.params.tempId);
  }, []);

  const retrieveJob = (uuid: string, tempId: number) => {
    debugger;
    EmployerService.GetJobDetailsInfo(uuid, tempId)
      .then((response: any) => {
        if (response.data.data.job.jobTitle != null) {
          setJob(response.data.data.job);
        }
        setcurrentCountries(response.data.data.countriesTranslations);
        debugger;
        setSelectedCountry(
          (
            response.data.data
              .countriesTranslations as IPostJobDetailsCountries[]
          ).find((m) => m.countryId == response.data.data.job.jobNationalityId)
        );

        debugger;
        seteditorState(htmlToDraft(response.data.data.job.jobDescription));

        setRecuirEditorState(htmlToDraft(response.data.data.job.requirements));
      })
      .catch((e: any) => {
        console.log(e);
      });
  };

  const handleDescDraftChange = (draftText: any) => {
    seteditorState(draftText);
    // SAVING HTML TO SERVER
    const convertedHtml = draftToHtml(
      convertToRaw(editorState.getCurrentContent())
    );
    // const html = draftToHtml(draftText)
    job.jobDescription = convertedHtml;
    setJob(job);
  };

  const handleRecuirDraftChange = (draftText: any) => {
    setRecuirEditorState(draftText);
    // SAVING HTML TO SERVER
    const convertedReqireHtml = draftToHtml(
      convertToRaw(recuirEditorState.getCurrentContent())
    );
    // const html = draftToHtml(draftText)
    job.requirements = convertedReqireHtml;
    setJob(job);
  };

  const dispatch = useDispatch();

  const [job, setJob] = useState(initialJobState);
  const [showLoader, setShowLoader] = React.useState(false);

  const html = job.jobDescription;
  const [editorState, seteditorState] = useState(htmlToDraft(html));
  const [recuirEditorState, setRecuirEditorState] = useState(htmlToDraft(html));

  const handleInputChange = (event: React.ChangeEvent<any>) => {
    const { name, value } = event.target;
    setJob({ ...job, [name]: value });
  };

  const saveJob = () => {
    setisLoading(true);
    var data: IPostJobDetails = {
      jobTitle: job.jobTitle,
      seniorityLevelId: job.seniorityLevelId,
      employerTypeId: job.employerTypeId,
      jobDescription: job.jobDescription,
      industryId: job.industryId,
      jobLocationId: job.jobLocationId,
      jobNationalityId: job.jobNationalityId,
      requirements: job.requirements,
      uuId: props.match.params.id,
      jobTemplateId: props.match.params.tempId,
    };

    JobService.create(data).then(
      (response) => {
        history.push(
          "/employer/EmployerJobInstructions/" + response.data.data.uuid
        );
      },
      (error) => {
        alert("failed to add job");
        setisLoading(false);
        //history.push('/employer/JobPostInstructions?uuid=sddsd');
      }
    );
  };

  // Validation
  const validationSchema = Yup.object().shape({
    jobTitle: Yup.string().required("Job Title is required"),

    //jobDescription: Yup.string().required("Job Description is required"),

    //requirements: Yup.string().required("Job requirements is required"),
  });

  // functions to build form returned by useForm() hook
  const { register, handleSubmit, reset, errors } = useForm({
    resolver: yupResolver(validationSchema),
  });

  function onSubmit(data: any) {
    debugger;
    if (job.jobDescription == "" && job.requirements == "") {
      if (job.jobDescription == "") {
        setDescriptionError(true);
      }
      if (job.jobDescription != "") {
        setDescriptionError(false);
      }
      if (job.requirements == "") {
        setRequiredError(true);
      }
      if (job.requirements != "") {
        setRequiredError(false);
      }
    } else {
      saveJob();
    }
  }
  const [scroll, setScroll] = useState(false);
  useEffect(() => {
    window.addEventListener("scroll", () => {
      setScroll(window.scrollY > 50);
    });
  }, []);

  const [value, setSetvalue] = useState("");

  function handleChangeSelect(event: React.ChangeEvent<HTMLInputElement>) {
    console.log("id is", event.target.id);
    setSetvalue(event.target.value);
  }

  const [selectedCountry, setSelectedCountry] =
    useState<IPostJobDetailsCountries>({
      countryId: 189,
      name: "Saudi Arabia",
    });

  // const [selectedOption, setselectedOption] = useState([selectedCountry] as IPostJobDetailsCountries[])

  function handleChange(selectedOption: any) {
    setSelectedCountry(selectedOption);
    //job.jobNationalityId = Number(selectedOption.countryId);
    job.jobNationalityId = selectedOption.countryId;
    setJob(job);
  }

  return (
    <React.Fragment>
      <div className="jobPosting">
        <div className="row">
          <div className="col-md-12">
            <div
              className="bread-text pull-left"
              style={{ paddingBottom: "13px" }}
            >
              <span className="lt-head">
                <a href="/employer/dashboard">My Jobs</a>
              </span>
              <span className="lt-text">
                <i className="fal fa-angle-right"></i>
              </span>
              <span className="lt-head-bold">Add Job</span>
            </div>
            <div className="pull-right pull-right-disable-res">
              <Filters SelectedVal="Details" />
            </div>
          </div>
        </div>
        <form onSubmit={handleSubmit(onSubmit)} onReset={reset}>
          <div className="row">
            <div className="col-md-12">
              <div className="sub-info-box">
                <div className="blueHead-18">Details</div>

                <form className="row">
                  <div className="col-md-12 form-group customInput">
                    <label>Job title</label>
                    <input
                      name="jobTitle"
                      id="jobTitle"
                      placeholder="Job title"
                      defaultValue={job.jobTitle}
                      type="text"
                      ref={register}
                      className={`form-control ${
                        errors.jobTitle ? "is-invalid" : ""
                      }`}
                      onChange={handleInputChange}
                    />
                    <div className="invalid-feedback">
                      {errors.jobTitle?.message}
                    </div>
                  </div>

                  <div className="col-md-6 form-group customInput">
                    <label>Seniority level</label>
                    <select
                      onChange={handleInputChange}
                      value={job.seniorityLevelId}
                      className="form-control"
                      name="seniorityLevelId"
                      id="seniorityLevelId"
                    >
                      <option value="1">Student / Intern</option>

                      <option value="2">Entry Level</option>

                      <option value="3">Associate</option>

                      <option value="4">Mid-senior Level</option>

                      <option value="5">Director</option>

                      <option value="6">Executive</option>
                    </select>
                  </div>

                  <div className="col-md-6 form-group customInput">
                    <label>Employment type</label>
                    <select
                      onChange={handleInputChange}
                      className="form-control"
                      value={job.employerTypeId}
                      name="employerTypeId"
                      id="employerTypeId"
                    >
                      <option value="1">Full-Time</option>

                      <option value="2">Part-Time</option>

                      <option value="3">Contract</option>

                      <option value="4">Temporary</option>

                      <option value="5">Volunteer</option>

                      <option value="6">Internship</option>

                      <option value="7">Per diem</option>

                      <option value="8">Other</option>
                    </select>
                  </div>

                  <div className="col-md-6 form-group customInput">
                    <label>Company industry</label>
                    <select
                      onChange={handleInputChange}
                      value={job.industryId}
                      className="form-control"
                      name="industryId"
                      id="industryId"
                    >
                      <option value="1">Business</option>
                      <option value="2">Tech</option>
                      <option value="3">Health care</option>
                      <option value="4">Admin & Sales</option>
                    </select>
                  </div>

                  <div className="col-md-6 form-group customInput">
                    <label>Location</label>
                    <select
                      onChange={handleInputChange}
                      className="form-control"
                      value={job.jobLocationId}
                      name="jobLocationId"
                      id="jobLocationId"
                    >
                      <option value="1">On-site</option>
                      <option value="2">On-site and open to remote</option>
                      <option value="3">Remote Only</option>
                    </select>
                  </div>

                  <div className="col-md-6 form-group customInput">
                    <label>Select Country</label>
                    <Select
                      styles={colourStyles}
                      value={selectedCountry}
                      onChange={handleChange}
                      options={currentCountries}
                      getOptionLabel={(option) => option.name}
                      getOptionValue={(option) => option.countryId.toString()}
                    />
                  </div>

                  <div
                  style={{visibility:'hidden',height:'62px'}}
                  className='col-md-6 form-group customInput'>
                  </div>

                  <div className="form-group col-md-6 customInput">
                    <label>Job description</label>
                    <Draft
                      editorState={editorState}
                      name="jobDescription"
                      id="jobDescription"
                      // value={job.firstMailDescription}
                      ref={register}
                      className={`form-control ${
                        errors.firstMailDescription ? "is-invalid" : ""
                      }`}
                      // onEditorStateChange={(editorState) => { seteditorState(editorState) }}
                      onEditorStateChange={(editorState) =>
                        handleDescDraftChange(editorState)
                      }
                    />
                    {descriptionError && (
                      <div className="invalid-feedback">
                        Job description is required
                      </div>
                    )}{" "}
                  </div>

                  <div className="form-group col-md-6 customInput">
                    <label>Job requirements</label>
                    <Draft
                      editorState={recuirEditorState}
                      name="requirements"
                      id="requirements"
                      // value={job.firstMailDescription}
                      ref={register}
                      className={`form-control ${
                        errors.firstMailDescription ? "is-invalid" : ""
                      }`}
                      // onEditorStateChange={(editorState) => { seteditorState(editorState) }}
                      onEditorStateChange={(recuirEditorState) =>
                        handleRecuirDraftChange(recuirEditorState)
                      }
                    />
                    {requiredError && (
                      <div className="invalid-feedback">
                        Job requirements are required
                      </div>
                    )}{" "}
                  </div>
                </form>
              </div>
            </div>
          </div>

          <div className="row">
            <div className="btn-job col-md-12">
              <div className="pull-right">
                <Button
                  isLoading={false}
                  border="none"
                  fontSize="16px"
                  font="Proxima-Regular"
                  color="#0195FF"
                  bgColor="#e1f0fb"
                  height="35px"
                  onClick={() => {
                    history.push("/employer/dashboard");
                  }}
                  radius=""
                  width="124px"
                  children="Back"
                />
                <Button
                  isLoading={isLoading}
                  border="none"
                  fontSize="16px"
                  font="Proxima-Regular"
                  color="white"
                  bgColor="#0195FF"
                  height="35px"
                  onClick={void 0}
                  radius=""
                  width="124px"
                  children="Next"
                />
              </div>
            </div>
          </div>
        </form>
      </div>
    </React.Fragment>
  );
}
