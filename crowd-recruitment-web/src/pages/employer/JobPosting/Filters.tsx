import React from "react";
import { useHistory } from "react-router-dom";
export default function Filters(props: any) {
  const history = useHistory();
  debugger;
  const [selectedFilter, setselectedFilter] = React.useState(props.SelectedVal);

  const filtersArr = [
    {
      filterStatus: "Details",
      filterName: "Details",
      iconClass: "fal fa-bars",
    },
    {
      filterStatus: "Instructions",
      filterName: "Instructions",
      iconClass: "fal fa-file",
    },
    {
      filterStatus: "Objectives",
      filterName: "Objectives",
      iconClass: "fal fa-medal",
    },
    {
      filterStatus: "Engagment",
      filterName: "Engagement",
      iconClass: "fal fa-users",
    },
  ];
  const setFilterClass = (args: any) => {
    setselectedFilter(args);
    history.push("/employer/EmployerJob" + args);
  };

  return (
    <React.Fragment>
      <div className="newFilters">
        {filtersArr.map((filter) => {
          return (
            <span
              onClick={() => setFilterClass(filter.filterStatus)}
              className={
                selectedFilter == filter.filterStatus
                  ? "newfilterItem activeItem"
                  : "newfilterItem"
              }
            >
              <i className={filter.iconClass}></i>
              {filter.filterName}
            </span>
          );
        })}
      </div>
    </React.Fragment>
  );
}
