import React, { useEffect, useState } from "react";
import { createStyles, makeStyles, Theme } from "@material-ui/core/styles";
import TextField from "@material-ui/core/TextField";
import Button from "../../../../components/Buttons/Button";
import Grid from "@material-ui/core/Grid";
import thank from "../../../src/assets/thankful.png";
import Filters from "../Filters";
import jobService from "../../../../services/JobService";
import { useForm } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import * as Yup from "yup";
import { useDispatch, useSelector } from "react-redux";
import { history } from "../../../../helpers/history";
import employerService from "../../../../services/EmployerService";
import { v4 as uuidv4 } from "uuid";
import { ICurrency } from "../../../../models/ICurrency";
import { IPostJobInstructions } from "../../../../models/IPostJobInstructions";

const initialMustList = [
  {
    id: "",
    name: "",
  },
];

const initialNiceList = [
  {
    id: "",
    name: "",
  },
];

const initialSourceList = [
  {
    id: "",
    name: "",
  },
];
export default function EmployerJobInstruction(props: any) {
  const [mustList, setMustList] = React.useState(initialMustList);
  const [niceList, setNiceList] = React.useState(initialNiceList);
  const [notSourceList, setNotSource] = React.useState(initialSourceList);
  const [name, setName] = React.useState("");
  const [niceName, setNiceName] = React.useState("");
  const [sourcename, setSourceName] = React.useState("");
  const [mustShown, setMustShown] = React.useState("none");
  const [niceShown, setNiceShown] = React.useState("none");
  const [isLoading, setisLoading] = useState(false);

  const [showLoader, setShowLoader] = React.useState(false);

  const initialJobState = {
    uuid: "",
    experienceLevelId: 1,
    minSalary: 0,
    maxSalary: 0,
    currencyId: 122,
  };

  const dispatch = useDispatch();

  const [job, setJob] = useState(initialJobState);

  useEffect(() => {
    retrieveJob(props.match.params.id);
    getCurrencies();
  }, []);

  const retrieveJob = (uuid: string) => {
    debugger;
    employerService
      .GetJobInstructionsInfo(uuid)
      .then((response) => {
        debugger;
        setJob(response.data.data.job);
        setMustList(response.data.data.mustHaveSkillsVM);
        setNiceList(response.data.data.niceHaveSkillsVM);
        setNotSource(response.data.data.notToSourceVM);
      })
      .catch((e) => {
        console.log(e);
      });
  };

  const [currentCurrencies, setCurrentCurrencies] = useState<ICurrency[]>([]);

  const handleInputChange = (event: React.ChangeEvent<any>) => {
    const { name, value } = event.target;
    setJob({ ...job, [name]: value });
  };

  const getCurrencies = () => {
    debugger;
    jobService
      .getAllCurrencies()
      .then((response) => {
        debugger;
        setCurrentCurrencies(response.data.data);
      })
      .catch((e) => {
        console.log(e);
      });
  };

  const saveJob = () => {
    debugger;
    var data: IPostJobInstructions = {
      uuid: props.match.params.id,
      experienceLevelId: job.experienceLevelId,
      currencyId: job.currencyId,
      minSalary: job.minSalary,
      maxSalary: job.maxSalary,
      mustHaveSkills: mustList,
      niceHaveQualification: niceList,
      notToSource: notSourceList,
    };

    if (mustList.length > 0 && niceList.length > 0) {
      setisLoading(true);
      jobService.createInstructions(data).then(
        (response) => {
          history.push(
            "/employer/EmployerJobObjectives/" + response.data.data.uuid
          );
        },
        (error) => {
          alert("failed to add job instructions");
          setisLoading(false);
          //history.push('/employer/JobPostInstructions?uuid=sddsd');
        }
      );
    } else {
      if (mustList.length == 0) {
        setMustShown("inline");
        setisLoading(false);
      }

      if (niceList.length == 0) {
        setNiceShown("inline");
        setisLoading(false);
      }
    }
  };

  // Validation
  const validationSchema = Yup.object().shape({
    minSalary: Yup.number().min(0),
    maxSalary: Yup.number()
      .min(0)
      .when("minSalary", (minSalary: number, schema: any) => {
        return schema.test({
          test: (maxSalary: number) => maxSalary >= minSalary,
          message: "max salary should be greater than min salary",
        });
      }),
  });

  // functions to build form returned by useForm() hook
  const { register, handleSubmit, reset, errors } = useForm({
    resolver: yupResolver(validationSchema),
  });

  function onSubmit(data: any) {
    debugger;
    saveJob();
  }

  function handleChange(event: any) {
    // track input field's state
    setName(event.target.value);
  }

  function handleAddMust() {
    // add item
    if (name != "") {
      const newList = mustList.concat({ name: name, id: uuidv4() });
      setMustList(newList);
      setMustShown("none");
      setName("");
    }
  }

  function handleNiceChange(event: any) {
    debugger;
    // track input field's state
    setNiceName(event.target.value);
    setNiceShown("none");
  }

  function handleNiceAdd() {
    debugger;
    // add item
    if (niceName != "") {
      const newList = niceList.concat({ name: niceName, id: uuidv4() });

      setNiceList(newList);
      setNiceName("");
    }
  }

  function handleSourceChange(event: any) {
    // track input field's state
    setSourceName(event.target.value);
  }

  function handleSourceAdd() {
    // add item

    if (sourcename != "") {
      const newList = notSourceList.concat({ name: sourcename, id: uuidv4() });

      setNotSource(newList);
      setSourceName("");
    }
  }

  function DeleteMustSkill(id: string) {
    // add item
    debugger;
    const newList = mustList.filter((m) => m.id != id);

    setMustList(newList);
  }

  function DeleteNiceSkill(id: string) {
    // add item
    debugger;
    const newList = niceList.filter((m) => m.id != id);

    setNiceList(newList);
  }

  function DeleteNotSourcrSkill(id: string) {
    // add item
    debugger;
    const newList = notSourceList.filter((m) => m.id != id);

    setNotSource(newList);
  }
  return (
    <React.Fragment>
      <div className="jobPosting">
        <div className="row">
          <div className="col-md-12">
            <div
              className="bread-text pull-left"
              style={{ paddingBottom: "13px" }}
            >
              <span className="lt-head">
                <a href="/employer/dashboard">My Jobs</a>
              </span>
              <span className="lt-text">
                <i className="fal fa-angle-right"></i>
              </span>
              <span className="lt-head-bold">Add Job</span>
            </div>
            <div className="pull-right pull-right-disable-res">
              <Filters SelectedVal="Instructions" />
            </div>
          </div>
        </div>
        <form onSubmit={handleSubmit(onSubmit)} onReset={reset}>
          <div className="row">
            <div className="col-md-12">
              <div className="sub-info-box">
                <div className="blueHead-18">Instructions</div>

                <div className="row">
                  <div className="col-md-4 form-group customInput">
                    <label>Min Salary</label>
                    <input
                      name="minSalary"
                      id="minSalary"
                      placeholder="Min Salary"
                      value={job.minSalary}
                      type="Number"
                      min="0"
                      ref={register}
                      className={`form-control ${
                        errors.minSalary ? "is-invalid" : ""
                      }`}
                      onChange={handleInputChange}
                    />
                    <div className="invalid-feedback">
                      {errors.minSalary?.message}
                    </div>
                  </div>

                  <div className="col-md-4 form-group customInput">
                    <label>Max Salary</label>

                    <input
                      name="maxSalary"
                      id="maxSalary"
                      placeholder="Max Salary"
                      value={job.maxSalary}
                      type="Number"
                      min="0"
                      ref={register}
                      className={`form-control ${
                        errors.maxSalary ? "is-invalid" : ""
                      }`}
                      onChange={handleInputChange}
                    />
                    <div className="invalid-feedback">
                      {errors.maxSalary?.message}
                    </div>
                  </div>

                  <div className="col-md-4 form-group customInput">
                    <label>Currency</label>
                    <select
                      onChange={handleInputChange}
                      className="form-control"
                      name="currencyId"
                      id="currencyId"
                      value={String(job.currencyId)}
                    >
                      {currentCurrencies.map((item) => (
                        <option value={item.currencyId}>
                          {item.currencyName}
                        </option>
                      ))}
                    </select>
                  </div>

                  <div className="col-md-4 form-group customInput">
                    <label>Experience</label>
                    <select
                      onChange={handleInputChange}
                      className="form-control"
                      name="experienceLevelId"
                      id="experienceLevelId"
                      value={job.experienceLevelId}
                    >
                      <option value="1">0 - 2</option>

                      <option value="2">3 - 5</option>

                      <option value="3">6 - 10</option>

                      <option value="4">10 +</option>
                    </select>
                  </div>

                  <div className="col-md-6 form-group tag-Input">
                    <label>Must have qualifications</label>

                    <input
                      className="form-control"
                      value={name}
                      onChange={handleChange}
                      type="text"
                    />
                    <span onClick={handleAddMust}>
                      <i className="fa fa-plus add-tag"></i>
                    </span>

                    <ul className="tag-output">
                      {mustList.map((item) => (
                        <li key={item.id}>
                          {item.name}
                          <span>
                            <i
                              className="fa fa-times close-tag"
                              onClick={() => DeleteMustSkill(item.id)}
                            ></i>
                          </span>
                        </li>
                      ))}
                    </ul>

                    <div
                      className="invalid-feedback"
                      style={{ display: mustShown }}
                    >
                      Add at least one qualification
                    </div>
                  </div>

                  <div className="col-md-6 form-group tag-Input">
                    <label>Nice to have qualifications</label>
                    <input
                      className="form-control"
                      value={niceName}
                      onChange={handleNiceChange}
                      type="text"
                    />
                    <span onClick={handleNiceAdd}>
                      <i className="fa fa-plus add-tag"></i>
                    </span>

                    <ul className="tag-output">
                      {niceList.map((item) => (
                        <li key={item.id}>
                          {item.name}
                          <span>
                            <i
                              className="fa fa-times close-tag"
                              onClick={() => DeleteNiceSkill(item.id)}
                            ></i>
                          </span>
                        </li>
                      ))}
                    </ul>

                    <div
                      className="invalid-feedback"
                      style={{ display: niceShown }}
                    >
                      Add at least one qualification
                    </div>
                  </div>

                  <div className="col-md-12 form-group tag-Input">
                    <label>Companies not to source from</label>

                    <input
                      className="form-control"
                      value={sourcename}
                      onChange={handleSourceChange}
                      type="text"
                    />
                    <span onClick={handleSourceAdd}>
                      <i className="fa fa-plus add-tag"></i>
                    </span>

                    <ul className="tag-output">
                      {notSourceList.map((item) => (
                        <li key={item.id}>
                          {item.name}
                          <span>
                            <i
                              className="fa fa-times close-tag"
                              onClick={() => DeleteNotSourcrSkill(item.id)}
                            ></i>
                          </span>
                        </li>
                      ))}
                    </ul>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div className="row">
            <div className="btn-job col-md-12">
              <div className="pull-right">
                <Button
                  isLoading={false}
                  border="none"
                  fontSize="16px"
                  font="Proxima-Regular"
                  color="#0195FF"
                  bgColor="#e1f0fb"
                  height="35px"
                  onClick={() => {
                    history.push(
                      "/employer/employerjobdetails/" +
                        props.match.params.id +
                        "/0"
                    );
                  }}
                  radius=""
                  width="124px"
                  children="Back"
                />
                <Button
                  isLoading={isLoading}
                  border="none"
                  fontSize="16px"
                  font="Proxima-Regular"
                  color="white"
                  bgColor="#0195FF"
                  height="35px"
                  onClick={void 0}
                  radius=""
                  width="124px"
                  children="Next"
                />
              </div>
            </div>
          </div>
        </form>
      </div>
    </React.Fragment>
  );
}
