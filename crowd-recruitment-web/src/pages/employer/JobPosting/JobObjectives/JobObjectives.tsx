import React, { useEffect, useState } from "react";
import { createStyles, makeStyles, Theme } from "@material-ui/core/styles";
import TextField from "@material-ui/core/TextField";
import Button from "../../../../components/Buttons/Button";
import Grid from "@material-ui/core/Grid";
import thank from "../../../src/assets/thankful.png";
import Filters from "../Filters";
import { IEmployerObjectives } from "../../../../models/IEmployerObjectives";
import jobService from "../../../../services/JobService";
import { useForm } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import * as Yup from "yup";
import { useDispatch, useSelector } from "react-redux";
import { history } from "../../../../helpers/history";
import employerService from "../../../../services/EmployerService";

export default function EmployerJobObjectives(props: any) {
  const [count, setCount] = useState("1check");

  const initialJobState = {
    UUID: "",
    isContinuousHire: false,
    peopleToHireCount: 1,
    whenYouNeedToHire: 1,
    sourcingOutsideOfJoin: true,
    findingJobDifficultyLevelId: 1,
  };

  const dispatch = useDispatch();

  const [job, setJob] = useState(initialJobState);
  const [isLoading, setisLoading] = useState(false);

  const [showLoader, setShowLoader] = React.useState(false);

  useEffect(() => {
    retrieveJob(props.match.params.id);
  }, []);

  const retrieveJob = (uuid: string) => {
    debugger;
    employerService
      .GetJobInfo(uuid)
      .then((response) => {
        debugger;
        setJob(response.data.data);

        if (response.data.data.findingJobDifficultyLevelId == 1) {
          setCount("1check");
        }

        if (response.data.data.findingJobDifficultyLevelId == 2) {
          setCount("2check");
        }
        if (response.data.data.findingJobDifficultyLevelId == 3) {
          setCount("3check");
        }
      })
      .catch((e) => {
        console.log(e);
      });
  };

  const handleInputChange = (event: React.ChangeEvent<any>) => {
    const { name, value } = event.target;
    setJob({ ...job, [name]: value });
  };

  const saveJob = () => {
    setisLoading(true);
    debugger;
    var data: IEmployerObjectives = {
      UUID: props.match.params.id,
      isContinuousHire: job.isContinuousHire,
      peopleToHireCount: job.peopleToHireCount,
      whenYouNeedToHire: job.whenYouNeedToHire,
      sourcingOutsideOfJoin: job.sourcingOutsideOfJoin,
      findingJobDifficultyLevelId: count,
    };

    jobService.createObjectives(data).then(
      (response) => {
        history.push(
          "/employer/EmployerJobEngagment/" + response.data.data.uuid
        );
        setisLoading(false);
      },
      (error) => {
        alert("failed to add job instructions");
        setisLoading(false);
        //history.push('/employer/JobPostInstructions?uuid=sddsd');
      }
    );
  };

  // Validation
  const validationSchema = Yup.object().shape({
    peopleToHireCount: Yup.string().required(
      "People to hire count is required"
    ).test('len',"Max lenth is 4 digits",val => val.length <= 4),
  });

  // functions to build form returned by useForm() hook
  const { register, handleSubmit, reset, errors } = useForm({
    resolver: yupResolver(validationSchema),
  });

  function onSubmit(data: any) {
    debugger;
    saveJob();
  }
  const [scroll, setScroll] = useState(false);
  useEffect(() => {
    window.addEventListener("scroll", () => {
      setScroll(window.scrollY > 50);
    });
  }, []);
  return (
    <React.Fragment>
      <div className="jobPosting">
        <div className="row">
          <div className="col-md-12">
            <div
              className="bread-text pull-left"
              style={{ paddingBottom: "13px" }}
            >
              <span className="lt-head">
                <a href="/employer/dashboard">My Jobs</a>
              </span>
              <span className="lt-text">
                <i className="fal fa-angle-right"></i>
              </span>
              <span className="lt-head-bold">Add Job</span>
            </div>
            <div className="pull-right pull-right-disable-res">
              <Filters SelectedVal="Objectives" />
            </div>
          </div>
        </div>
        <form onSubmit={handleSubmit(onSubmit)} onReset={reset}>
          <div className="row">
            <div className="col-md-12">
              <div className="sub-info-box">
                <div className="blueHead-18">Objectives</div>

                <div className="row">
                  <div className="col-md-6 form-group customInput">
                    <label>What are your hiring needs for this job ?</label>
                    <select
                      onChange={handleInputChange}
                      className="form-control"
                      name="isContinuousHire"
                      id="isContinuousHire"
                      value={String(job.isContinuousHire)}
                    >
                      <option value="false">One Time</option>

                      <option value="true">Constantly</option>
                    </select>
                  </div>

                  <div className="col-md-6 form-group customInput">
                    <label>How many people are you aiming to hire ?</label>
                    <input
                      name="peopleToHireCount"
                      id="peopleToHireCount"
                      value={job.peopleToHireCount}
                      type="Number"
                      maxLength={8}
                      min="1"
                      max="100000"
                      ref={register}
                      className={`form-control ${
                        errors.peopleToHireCount ? "is-invalid" : ""
                      }`}
                      onChange={handleInputChange}
                    />
                    <div className="invalid-feedback">
                      {errors.peopleToHireCount?.message}
                    </div>
                  </div>

                  <div className="col-md-6 form-group customInput">
                    <label>When do you need to hire by ?</label>
                    <select
                      onChange={handleInputChange}
                      className="form-control"
                      name="whenYouNeedToHire"
                      id="whenYouNeedToHire"
                      value={job.whenYouNeedToHire}
                    >
                      <option value="1">As Soon as Possible</option>

                      <option value="2">In the coming days</option>

                      <option value="3">No deadline , just pipelinling</option>
                    </select>
                  </div>

                  <div className="col-md-6 form-group">
                    <label className="customLabel">
                      How difficult would you say it is to find great profiles
                      for this job ?
                    </label>
                    <div>
                      <div
                        className={
                          count == "1check" ? "activeCheck check" : "check"
                        }
                        id=""
                        onClick={() => setCount("1check")}
                      >
                        <i className="far fa-circle"></i>
                        Not difficult
                      </div>

                      <div
                        className={
                          count == "2check" ? "activeCheck check" : "check"
                        }
                        id=""
                        onClick={() => setCount("2check")}
                      >
                        <i className="far fa-circle"></i>
                        Medium
                      </div>

                      <div
                        className={
                          count == "3check" ? "activeCheck check" : "check"
                        }
                        id=""
                        onClick={() => setCount("3check")}
                      >
                        <i className="far fa-circle"></i>
                        Very difficult
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div className="row">
            <div className="btn-job col-md-12">
              <div className="pull-right">
                <Button
                  isLoading={false}
                  border="none"
                  fontSize="16px"
                  font="Proxima-Regular"
                  color="#0195FF"
                  bgColor="#e1f0fb"
                  height="35px"
                  onClick={() => {
                    history.push(
                      "/employer/EmployerJobInstructions/" +
                        props.match.params.id
                    );
                  }}
                  radius=""
                  width="124px"
                  children="Back"
                />
                <Button
                  isLoading={isLoading}
                  border="none"
                  fontSize="16px"
                  font="Proxima-Regular"
                  color="white"
                  bgColor="#0195FF"
                  height="35px"
                  onClick={void 0}
                  radius=""
                  width="124px"
                  children="Next"
                />
              </div>
            </div>
          </div>
        </form>
      </div>
    </React.Fragment>
  );
}
