import React, { useEffect } from "react";
import clsx from 'clsx';
import {
    createStyles,
    makeStyles,
    useTheme,
    Theme,
  } from "@material-ui/core/styles";
import Drawer from '@material-ui/core/Drawer';
import Button from '@material-ui/core/Button';
import { IJobDetails } from "../../../models/IJobDetails";

const drawerWidth = 1150;

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      display: 'flex',
    },
    appBar: {
      zIndex: theme.zIndex.drawer + 1,
    },
    drawer: {
      width: drawerWidth,
      flexShrink: 0,
    },
    menuButton: {
      marginRight: 36,
    },
    hide: {
      display: "none",
    },
    drawerOpen: {
      width: drawerWidth,
      transition: theme.transitions.create("width", {
        easing: theme.transitions.easing.sharp,
        duration: theme.transitions.duration.enteringScreen,
      }),
    },
    drawerClose: {
      transition: theme.transitions.create("width", {
        easing: theme.transitions.easing.sharp,
        duration: theme.transitions.duration.leavingScreen,
      }),
      overflowX: "hidden",
      width: 0,
      [theme.breakpoints.up("sm")]: {
        width:0,
      },
    },
    drawerPaper: {
      width: drawerWidth,
    },
    menuItem: {
      width: drawerWidth,
    },
    menuItemIcon: {
      color: '#97c05c',
    },
    drawerContainer: {
      overflow: 'auto',
    },
    content: {
      flexGrow: 1,
      padding: theme.spacing(3),
    },
  }),
);


 
interface Props {
  data: IJobDetails;
  openPreview:boolean;
  handleSideBar: (args:any) => void;
}

const JobPreviewSideBar: React.FC<Props> = ({openPreview, data, handleSideBar}) => {
    const classes = useStyles();
    const [openDrawer, setOpenDrawer] = React.useState(false);

    useEffect(() => {
        setOpenDrawer(openPreview)
      });

    const toggleDrawer = (open: any) => {
        setOpenDrawer(open) 
      };

  return (
    <div className='jobPreview'>
          <Drawer
            className={clsx(classes.drawer, {
            [classes.drawerOpen]: openDrawer,
            [classes.drawerClose]: !openDrawer,
            })}
            classes={{
            paper: clsx({
                [classes.drawerOpen]: openDrawer,
                [classes.drawerClose]: !openDrawer,
            }),
            }}
          anchor="right" open={openDrawer} onClose={() => toggleDrawer(false)}>
           <span
            onClick={() => handleSideBar(false)}
           >
               <i className="fa fa-times previewClose"></i>
           </span>
           <div className="drawerBody newEmpLayout">
  
           <div className='row'>
   <div className='col-md-9'>
      <div className='sub-info-box'>
         <div className='feature-set'>
            <div className="bold-head eBold font-16">
               {data.jobTitle}
            </div>
         </div>
         <div className="feature-set-info">
            <div className="sub-feature">
               <div className="bold-head font-14">Job Description</div>
               <div className="light-text" dangerouslySetInnerHTML={{ __html: data.description }} />
            </div>
         </div>
         <div className="feature-set-info">
         <div className="sub-feature">
            <div className="bold-head font-14">Requirements</div>
            <div className="light-text">
            <div className="light-text" dangerouslySetInnerHTML={{ __html: data.requirements }} />
            </div>
         </div>
         </div>
         <div className="feature-set-info">
            <div className="sub-feature">
               <div className="bold-head font-14">Must-have qualifications</div>
               <div className="light-text">
                  {data.mustHaveQualification}
               </div>
            </div>
         </div>

         <div className="feature-set-info">
            <div className="sub-feature">
               <div className="bold-head font-14">Nice-have qualifications</div>
               <div className="light-text">
                  {data.niceToHaveQualification}
               </div>
            </div>
         </div>

         <div className="feature-set-info">
            <div className="sub-feature">
               <div className="bold-head font-14">Companies not to source from</div>
               <div className="light-text">
               {data.companiesNotToSourceFrom}
               </div>
            </div>
         </div>
      </div>


   </div>
   <div className='col-md-3'>
      <div className='sub-info-box'>
         <div className='feature-set'>
            <div className="bold-head eBold font-16">
               Job Details
            </div>
         </div>
         <div className="feature-set-row">
            <div className="bold-head font-14">
               <span>
               <i className="fa fa-building"></i>
               </span>
               <span>Company name</span>
            </div>
            <div className="light-text">{data.companyName}</div>
         </div>
         <div className="feature-set-row">
            <div className="bold-head font-14">
               <span>
               <i className="fa fa-chart-bar"></i>
               </span>
               <span>Seniority level</span>
            </div>
            <div className="light-text">{data.seniorityLevelName}</div>
         </div>
         <div className="feature-set-row">
            <div className="bold-head font-14">
               <span>
               <i className="fa fa-clock"></i>
               </span>
               <span>Employment type</span>
            </div>
            <div className="light-text">{data.employmentTypeName}</div>
         </div>
         <div className="feature-set-row">
            <div className="bold-head font-14">
               <span>
               <i className="fa fa-location"></i>
               </span>
               <span>Location</span>
            </div>
            <div className="light-text">{data.jobLocation}</div>
         </div>
         <div className='side-set'>
            <div className="feature-set-row">
               <div className="bold-head font-14">
                  <span>Company Industry</span>
               </div>
               <div className="light-text">{data.companyIndustry}</div>
            </div>
            <div className="feature-set-row">
               <div className="bold-head font-14">
                  <span>Work experience</span>
               </div>
               <div className="light-text">{data.experienceLevelName}</div>
            </div>
         </div>
      </div>
   </div>
</div>


           </div>
          </Drawer>
        </div>
  );
};

export default JobPreviewSideBar;
