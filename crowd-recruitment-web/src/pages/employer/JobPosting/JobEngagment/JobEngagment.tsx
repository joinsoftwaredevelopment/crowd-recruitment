import React, { useEffect, useState } from "react";
import {
  createStyles,
  Theme,
  WithStyles,
  makeStyles,
  withStyles,
} from "@material-ui/core/styles";
import ModalBg from "../../../../../src/assets/modal-bg.png";
import TextField from "@material-ui/core/TextField";
import Button from "../../../../components/Buttons/Button";
import Grid from "@material-ui/core/Grid";
import questionMark from "../../../../../src/assets/question-mark.png";
import Filters from "../Filters";
import JobPreviewSideBar from "../JobPreviewSideBar";
import { EditorState, convertToRaw, ContentState } from "draft-js";
import Draft, {
  htmlToDraft,
  EmptyState,
  rawToDraft,
  draftToRaw,
  draftStateToHTML,
} from "react-wysiwyg-typescript";

import draftToHtml from "draftjs-to-html";

import IconButton from "@material-ui/core/IconButton";
import CloseIcon from "@material-ui/icons/Close";
import Typography from "@material-ui/core/Typography";

import Dialog from "@material-ui/core/Dialog";
import MuiDialogTitle from "@material-ui/core/DialogTitle";
import MuiDialogContent from "@material-ui/core/DialogContent";
import MuiDialogActions from "@material-ui/core/DialogActions";
import { history } from "../../../../helpers/history";
import { IPostJobEngagment } from "../../../../models/IPostJobEngagment";
import { IJobDetails } from "../../../../models/IJobDetails";
import jobService from "../../../../services/JobService";

import { useForm } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import * as Yup from "yup";
import employerService from "../../../../services/EmployerService";

const styles = (theme: Theme) =>
  createStyles({
    root: {
      margin: 0,
      padding: theme.spacing(2),
    },
    closeButton: {
      position: "absolute",
      right: theme.spacing(1),
      top: theme.spacing(1),
      color: theme.palette.grey[500],
    },
  });

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {},
  })
);

export default function EmployerJobEngagment(props: any) {
  const [count, setCount] = useState("1check");
  const [editorState, seteditorState] = useState(htmlToDraft(""));
  const [openPostJob, setOpenPostJob] = React.useState(false);
  const [openPreview, setOpenPreview] = React.useState(false);
  const [isLoading, setisLoading] = useState(false);

  const HandleOpenPostJob = () => {
    if (job.firstMailSubject == "" || job.firstMailDescription == "") {
      if (job.firstMailSubject == "") {
        setTitleError(true);
      }
      if (job.firstMailSubject != "") {
        setTitleError(false);
      }
      if (job.firstMailDescription == "") {
        setDescriptionError(true);
      }
      if (job.firstMailDescription != "") {
        setDescriptionError(false);
      }
    } else {
      setOpenPostJob(true);
    }
  };

  const handleClose = () => {
    setOpenPostJob(false);
  };
  const handleSideBar = (args: any) => {
    setOpenPreview(args);
  };
  const handleDescDraftChange = (draftText: any) => {
    seteditorState(draftText);
  };

  const initialTitle = [
    {
      id: 0,
      emailTitleEn: "",
      emailtitleAr: "",
      emailbodyEn: "",
      emailbodyAr: "",
    },
  ];

  const [templates, setTemplates] = React.useState(initialTitle);

  const [descriptionError, setDescriptionError] = useState(false);
  const [titleError, setTitleError] = useState(false);
  const [value, setValue] = React.useState();

  const initialJobState = {
    uuid: "",
    firstMailSubject: "",
    firstMailDescription: "",
    engagementTemplateId: 0,
  };

  const [jobDetails, setjobDetails] = useState<IJobDetails>({
    uuid: "",
    jobTitle: "",
    employmentTypeName: "",
    seniorityLevelName: "",
    companiesNotToSourceFrom: "",
    languageId: 2,
    jobLocation: "",
    companyName: "",
    mustHaveQualification: "",
    niceToHaveQualification: "",
    hiringNeeds: "",
    experienceLevelName: "",
    description: "",
    companyIndustry: "",
    jobStatusId: 2,
    jobStatusName: "",
    requirements: "",
    noOfSubmissions: 0,
    jobDate: new Date(),
  });

  const getJobDetails = () => {
    debugger;
    jobService
      .getJobDetails(props.match.params.id)
      .then((response) => {
        debugger;
        setjobDetails(response.data.data);
      })
      .catch((e) => {
        console.log(e);
      });
  };

  const [job, setJob] = useState(initialJobState);
  // TO SET A DEFAULT HTML
  const html = "";
  const [openReason, setOpenReason] = useState(false);

  useEffect(() => {
    retrieveJob(props.match.params.id);
    getJobDetails();
  }, []);

  const retrieveJob = (uuid: string) => {
    debugger;
    employerService
      .GetJobEngagementsInfo(uuid)
      .then((response) => {
        debugger;
        if (response.data.data.job.firstMailSubject != null) {
          debugger;

          seteditorState(
            htmlToDraft(response.data.data.job.firstMailDescription)
          );

          setJob(response.data.data.job);
        }
        setTemplates(response.data.data.templates);
      })
      .catch((e) => {
        console.log(e);
      });
  };

  const handleInputChange = (event: any) => {
    const { name, value } = event.target;
    setJob({ ...job, [name]: value });

    if (job.firstMailSubject == "") {
      setTitleError(true);
    }
    if (job.firstMailSubject != "") {
      setTitleError(false);
    }
  };

  const handleDraftChange = (draftText: any) => {
    seteditorState(draftText);
    // SAVING HTML TO SERVER
    const convertedHtml = draftToHtml(
      convertToRaw(editorState.getCurrentContent())
    );
    // const html = draftToHtml(draftText)
    job.firstMailDescription = convertedHtml;
    setJob(job);

    if (job.firstMailDescription == "") {
      setDescriptionError(true);
    }
    if (job.firstMailDescription != "") {
      setDescriptionError(false);
    }
  };

  const handleTemplateChange = (event: React.ChangeEvent<any>) => {
    const { name, value } = event.target;
    setValue({ ...value, [name]: value });
    debugger;
    if (value != "0") {
      const currentTemplate = templates.find((m) => m.id == value);
      job.firstMailSubject = currentTemplate.emailTitleEn;
      job.firstMailDescription = currentTemplate.emailbodyEn;
      job.engagementTemplateId = currentTemplate.id;
      seteditorState(htmlToDraft(currentTemplate.emailbodyEn));
    } else {
      job.firstMailSubject = "";
      job.firstMailDescription = "";
      seteditorState(htmlToDraft(""));
      job.engagementTemplateId = 0;
    }

    setJob(job);

    if (job.firstMailSubject == "" || job.firstMailDescription == "") {
      if (job.firstMailSubject == "") {
        setTitleError(true);
      }
      if (job.firstMailSubject != "") {
        setTitleError(false);
      }
      if (job.firstMailDescription == "") {
        setDescriptionError(true);
      }
      if (job.firstMailDescription != "") {
        setDescriptionError(false);
      }
    } else {
      setTitleError(false);
      setDescriptionError(false);
    }
  };

  const saveJob = (statusId: number) => {
    setisLoading(true);
    var data: IPostJobEngagment = {
      uuid: props.match.params.id,
      firstMailSubject: job.firstMailSubject,
      firstMailDescription: job.firstMailDescription,
      engagementTemplateId: job.engagementTemplateId,
      statusId: statusId,
    };

    jobService.createEngagment(data).then(
      (response) => {
        history.push("/employer/dashboard/0");
        setisLoading(false);
      },
      (error) => {
        alert("failed to add job engagement");
        setisLoading(false);
      }
    );
  };

  // Validation
  const validationSchema = Yup.object().shape({
    //firstMailSubject: Yup.string().required("FirstMailSubject is required"),
    //firstMailDescription: Yup.string().required(
    // "FirstMailDescription is required"
    //),
  });

  // functions to build form returned by useForm() hook
  const { register, handleSubmit, reset, errors } = useForm({
    resolver: yupResolver(validationSchema),
  });

  function onSubmit(data: any) {
    debugger;
    saveJob(2);
  }

  return (
    <React.Fragment>
      <div className="jobPosting">
        <JobPreviewSideBar
          handleSideBar={handleSideBar}
          data={jobDetails}
          openPreview={openPreview}
        />
        <div className="row">
          <div className="col-md-12">
            <div
              className="bread-text pull-left"
              style={{ paddingBottom: "13px" }}
            >
              <span className="lt-head">
                <a href="/employer/dashboard">My Jobs</a>
              </span>
              <span className="lt-text">
                <i className="fal fa-angle-right"></i>
              </span>
              <span className="lt-head-bold">Add Job</span>
            </div>
            <div className="pull-right pull-right-disable-res">
              <Filters SelectedVal="Engagment" />
            </div>
          </div>
        </div>
        <form onSubmit={handleSubmit(onSubmit)} onReset={reset}>
          <div className="row">
            <div className="col-md-12">
              <div className="sub-info-box">
                <div className="blueHead-18">Engagment</div>

                <div className="row">
                  <div className="col-md-6 form-group customInput">
                    <label>Select Template</label>
                    <select
                      value={job.engagementTemplateId}
                      className="form-control"
                      onChange={handleTemplateChange}
                    >
                      <option key="0" value="0">
                        You can choose from our templates
                      </option>

                      {templates.map((item) => (
                        <option key={item.id} value={item.id}>
                          {item.emailTitleEn}
                        </option>
                      ))}
                    </select>
                  </div>

                  <div className="col-md-6 form-group customInput">
                    <label>First email subject</label>
                    <input
                      name="firstMailSubject"
                      id="firstMailSubject"
                      placeholder="Email subject"
                      value={job.firstMailSubject}
                      //disabled={true}
                      type="text"
                      ref={register}
                      className={`form-control ${
                        errors.firstMailSubject ? "is-invalid" : ""
                      }`}
                      onChange={handleInputChange}
                    />
                    {titleError && (
                      <div className="invalid-feedback">
                        First mail subject is required
                      </div>
                    )}{" "}
                  </div>

                  <div className="col-md-12 form-group customInput">
                    <label>First email description</label>
                    <Draft
                      //readOnly={true}
                      editorState={editorState}
                      name="firstMailDescription"
                      id="firstMailDescription"
                      // value={job.firstMailDescription}
                      ref={register}
                      className={`form-control ${
                        errors.firstMailDescription ? "is-invalid" : ""
                      }`}
                      // onEditorStateChange={(editorState) => { seteditorState(editorState) }}
                      onEditorStateChange={(editorState) =>
                        handleDraftChange(editorState)
                      }
                    />
                    {descriptionError && (
                      <div className="invalid-feedback">
                        First mail description is required
                      </div>
                    )}{" "}
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div className="row">
            <div className="btn-job col-md-12">
              <div className="pull-left">
                <Button
                  isLoading={false}
                  border="none"
                  fontSize="16px"
                  font="Proxima-Regular"
                  color="#989898"
                  bgColor="#DBDBDB"
                  height="35px"
                  onClick={() => {
                    history.push(
                      "/employer/EmployerJobObjectives/" + props.match.params.id
                    );
                  }}
                  radius=""
                  width="124px"
                  children="Back"
                />
              </div>

              <div className="pull-right">
                <span onClick={() => saveJob(1)} className="draftLink">
                  Save as draft
                </span>
                {/* <Button
                                isLoading={false}
                                border="none"
                                fontSize="16px"
                                font="Proxima-Regular"
                                color="#0195FF"
                                bgColor="#e1f0fb"
                                height="35px"
                                onClick={() => handleSideBar(true)}
                                radius=""
                                width="124px"
                                children="Preview"
                            /> */}

                <button
                  type="button"
                  onClick={() => handleSideBar(true)}
                  className="btn btn-approve"
                  style={{
                    color: "#0195FF",
                    width: "124px",
                    backgroundColor: "#e1f0fb",
                    height: "35px",
                  }}
                >
                  Preview
                </button>

                {/* <Button
                                isLoading={false}
                                border="none"
                                fontSize="16px"
                                font="Proxima-Regular"
                                color="white"
                                bgColor="#0195FF"
                                height="35px"
                                onClick={HandleOpenPostJob}
                                radius=""
                                width="124px"
                                children="Post Job"
                            /> */}

                <button
                  type="button"
                  onClick={HandleOpenPostJob}
                  style={{ color: "white", width: "124px" }}
                  className="btn btn-approve"
                >
                  Post Job
                </button>
              </div>
            </div>
          </div>
        </form>
      </div>

      <Dialog
        fullWidth={true}
        maxWidth={"xs"}
        onClose={handleClose}
        aria-labelledby="customized-dialog-title"
        open={openPostJob}
      >
        <img className="modalBg" src={ModalBg} />
        <DialogTitle
          id="customized-dialog-title3"
          onClose={handleClose}
          children=""
        ></DialogTitle>

        <DialogContent className="text-center">
          <img style={{ marginTop: "20px" }} src={questionMark} />

          <div className="head-job-pop">
            <div className="blackHead">
              Are you sure you want to add this job post?
            </div>
          </div>
          <div className="text-center job-pop-btns">
            <button onClick={handleClose} className="btn btn-decline">
              No
            </button>

            <Button
              isLoading={isLoading}
              border="none"
              fontSize="16px"
              font="Proxima-Regular"
              color="white"
              bgColor="#0195FF"
              height="35px"
              onClick={() => saveJob(2)}
              radius=""
              width="80px"
              children="Yes"
            />
          </div>
        </DialogContent>
      </Dialog>
    </React.Fragment>
  );
}

export interface DialogTitleProps extends WithStyles<typeof styles> {
  id: string;
  children: React.ReactNode;
  onClose: () => void;
}

const DialogTitle = withStyles(styles)((props: DialogTitleProps) => {
  const { children, classes, onClose, ...other } = props;
  return (
    <MuiDialogTitle disableTypography className={classes.root} {...other}>
      <Typography variant="h6">{children}</Typography>
      {onClose ? (
        <IconButton
          aria-label="close"
          className={classes.closeButton}
          onClick={onClose}
        >
          <CloseIcon />
        </IconButton>
      ) : null}
    </MuiDialogTitle>
  );
});

const DialogContent = withStyles((theme: Theme) => ({
  root: {
    padding: theme.spacing(2),
  },
}))(MuiDialogContent);
