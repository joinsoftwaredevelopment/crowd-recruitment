import React, { useState,useEffect } from "react";
import { createStyles, makeStyles, Theme } from "@material-ui/core/styles";
import TextField from "@material-ui/core/TextField";
import Grid from "@material-ui/core/Grid";
import thank from "../../../src/assets/thankful.png";
import { Dispatch } from "redux";
import { alertActions } from "../../../actions/alertActions";
import { useDispatch } from "react-redux";


import { useDropzone } from "react-dropzone";
import { EditorState, convertToRaw, ContentState } from "draft-js";
import Draft, {
  htmlToDraft,
  EmptyState,
  rawToDraft,
  draftToRaw,
  draftStateToHTML,
} from "react-wysiwyg-typescript";

import draftToHtml from "draftjs-to-html";
import Button from "../../../components/Buttons/Button";
import CandidateService from "../../../services/CandidateService";
import { IOfferModel } from "../../../models/IOfferModel";
import { IInterviewDetailsModel } from "../../../models/IInterviewDetailsModel";

export default function Offering(props:any) {

  useEffect(() => {
    getAllOfferTemplates();
  });

  const getAllOfferTemplates = () => {
    debugger;
    CandidateService.GetAllOfferTemplates()
      .then((response) => {
        setOfferTemplates(response.data.data);
      })
      .catch((e) => {
        console.log(e);
      });
  };
    const { acceptedFiles, getRootProps, getInputProps } = useDropzone({
        accept: ".pdf",
      });
    
      const files = acceptedFiles.map((file: any) => (
        <li key={file.path}>
          {file.path} - {file.size} bytes
        </li>
      ));

      function handleChange(e: any) {
        const { name, value } = e.target;
       setFileError(false);
      }

      const handleOfferChange = (event: any) => {
        debugger;
        const { name, value } = event.target;
        setOffer({ ...offer, [name]: value });
    
        if (offer.offerSubject == "") {
          setOfferSubjectError(true);
        }
        if (offer.offerSubject != "") {
          setOfferSubjectError(false);
        }
      };

      const handleRecuirDraftChange = (draftText: any) => {
        setRecuirEditorState(draftText);
        // SAVING HTML TO SERVER
        const convertedReqireHtml = draftToHtml(
          convertToRaw(recuirEditorState.getCurrentContent())
        );
        // const html = draftToHtml(draftText)
        offer.offerDescription = convertedReqireHtml;
        setOffer(offer);
    
        if (offer.offerDescription == "") {
          setOfferDescriptionError(true);
        }
        if (offer.offerDescription != "") {
          setOfferDescriptionError(false);
        }
      };
    

      const initialOffer: IOfferModel = {
        submissionUuid: "",
        offerSubject: "",
        offerDescription: "",
        offerFile: null,
      };

      const initialTitle = [
        {
          id: 0,
          emailTitleEn: "",
          emailtitleAr: "",
          emailbodyEn: "",
          emailbodyAr: "",
        },
      ];

      const initialAddInterviewDetails: IInterviewDetailsModel = {
        submissionUUid: "",
        interviewerId: 1,
        interviewComment: "",
        interviewTypeId: 1,
        interviewDate: new Date(),
        interviewTime: "12:00",
        duration: 1,
        templateId: 0,
        emailSubject: "",
        emailDescription: "",
        interviewerTypeId: 1,
        id: 0,
      };

      const [offer, setOffer] = useState(initialOffer);
      const [offerSubjectError, setOfferSubjectError] = useState(false);
      const [offerDescriptionError, setOfferDescriptionError] = useState(false);
      const [fileError, setFileError] = useState(false);
      const [isLoading, setisLoading] = useState(false);
      const [offerTemplates, setOfferTemplates] = React.useState(initialTitle);
      const [addInterviewDetails, setAddInterviewDetails] = useState(
        initialAddInterviewDetails
      );

      const dispatch = useDispatch();

      const [value, setValue] = React.useState();
      const html = "";
      const [recuirEditorState, setRecuirEditorState] = useState(htmlToDraft(html));

      const handleOfferTemplateChange = (event: React.ChangeEvent<any>) => {
        const { name, value } = event.target;
        setValue({ ...value, [name]: value });
        debugger;
        if (value != "0") {
          const currentOfferTemplate = offerTemplates.find((m) => m.id == value);
          offer.offerSubject = currentOfferTemplate.emailTitleEn;
          offer.offerDescription = currentOfferTemplate.emailbodyEn;
          setRecuirEditorState(htmlToDraft(currentOfferTemplate.emailbodyEn));
        } else {
          offer.offerSubject = "";
          offer.offerDescription = "";
          setRecuirEditorState(htmlToDraft(""));
        }
    
        setOffer(offer);
    
        if (offer.offerSubject == "" || offer.offerDescription == "") {
          if (offer.offerSubject == "") {
            setOfferSubjectError(true);
          }
          if (offer.offerSubject != "") {
            setOfferSubjectError(false);
          }
          if (offer.offerDescription == "") {
            setOfferDescriptionError(true);
          }
          if (addInterviewDetails.emailDescription != "") {
            setOfferDescriptionError(false);
          }
        } else {
          setOfferSubjectError(false);
          setOfferDescriptionError(false);
        }
      };

      const AddOffer = () => {
        debugger;
        if (
          offer.offerSubject == "" ||
          offer.offerDescription == "" ||
          acceptedFiles[0] == null
        ) {
          if (offer.offerSubject == "") {
            setOfferSubjectError(true);
          }
    
          if (offer.offerSubject != "") {
            setOfferSubjectError(false);
          }
    
          if (offer.offerDescription == "") {
            setOfferDescriptionError(true);
          }
    
          if (offer.offerDescription != "") {
            setOfferDescriptionError(false);
          }
    
          if (acceptedFiles[0] == null) {
            setFileError(true);
          }
    
          if (acceptedFiles[0] != null) {
            setFileError(false);
          }
        } else {
          offer.submissionUuid = props.uuid;
          // Validate then Assign file
          setOffer(offer);
    
          const formData = new FormData();
          formData.append("offerFile", acceptedFiles[0]);
          formData.append("submissionUuid", offer.submissionUuid);
          formData.append("offerSubject", offer.offerSubject);
          formData.append("offerDescription", offer.offerDescription);
      setisLoading(true);
          CandidateService.SendOffer(formData)
        .then((response) => {
          dispatch(
            alertActions.success("Offer has been sent successfully")
          );
      setisLoading(false);
        })
        .catch((e) => {
      setisLoading(false);
          console.log(e);
        });
          
        }
      };

  return (
    <React.Fragment>
      <div className="padBox-r-m">

      <div className="newEmpLayout">

            <div className='row'>
            <div className="col-md-6 form-group customInput">
            <label>Select Template</label>
            <select
                  value="0"
                  className="form-control"
                  onChange={handleOfferTemplateChange}
                >
                  <option key="0" value="0">
                    {"You can choose from our templates"}
                  </option>

                  {offerTemplates.map((item) => (
                    <option key={item.id} value={item.id}>
                      {item.emailTitleEn}
                    </option>
                  ))}
                </select>
             
             </div>

             <div className="col-md-6 form-group customInput">
            <label>Email Subject</label>
            <input
                  className="form-control"
                  name="offerSubject"
                  id="offerSubject"
                placeholder="Email Subject"
                  value={offer.offerSubject}
                  type="text"
                  onChange={handleOfferChange}
                />
                {offerSubjectError && (
                  <div className="invalid-feedback">
                    Email Subject is required
                  </div>
                )}{" "}
             </div>

             <div className="form-group col-md-12 customInput">
                    <label>Email description</label>
                    <Draft
                  editorState={recuirEditorState}
                  name="offerDescription"
                  id="offerDescription"
                  className="form-control"
                  onEditorStateChange={(recuirEditorState) =>
                    handleRecuirDraftChange(recuirEditorState)
                  }
                />
                {offerDescriptionError && (
                  <div className="invalid-feedback">
                    Email description is required
                  </div>
                )}{" "}
            </div>

            <div className="form-group col-md-12">
              <div
                onChange={handleChange}
                {...getRootProps({ className: "dropzone" })}
              >
                <input onChange={handleChange} {...getInputProps()} />
                <div className="file-drop" style={{ marginBottom: "15px" }}>
                  <div className="font-16 bold" style={{paddingBottom:'20px'}}>Drag and drop resume here</div>
                  <div>
                    Or click{" "}
                    <a className="anchor"> here to upload attachment</a>
                      
                  </div>
                </div>
              </div>
            </div>
            </div>

            <div className="row">
            <div className="btn-job col-md-12 mt-0">
            <div className='pull-left'>
            <aside>
                <h4>File</h4>
                <ul>{files}</ul>
                {fileError && (
                  <div className="invalid-feedback">Offer file is required</div>
                )}{" "}
              </aside>
            </div>
              <div className="pull-right">
                
                <Button
                  isLoading={isLoading}
                  border="none"
                  fontSize="16px"
                  font="Proxima-Regular"
                  color="white"
                  bgColor="#0195FF"
                  height="35px"
                  onClick={AddOffer}
                  radius=""
                  width="124px"
                  children="Confirm"
                />
              </div>
            </div>
          </div>

           </div>
      </div>

    </React.Fragment>
  );
}
