import React, { useEffect } from "react";
import { hiringStagesConstants } from "../../../constants/hiringStagesConstants";

export default function Filters(props: any) {
  debugger;
  const [selectedFilter, setselectedFilter] = React.useState(props.jobStatus);
  const filtersArr = [
    {
      filterStatus: hiringStagesConstants.Approved_By_Employer,
      filterName: "Profile",
      iconClass: "fal fa-user",
    },
    {
      filterStatus: hiringStagesConstants.Interviews,
      filterName: "Interviews",
      iconClass: "fal fa-users",
    },
    {
      filterStatus: hiringStagesConstants.Offer,
      filterName: "Offering",
      iconClass: "fal fa-star",
    },
  ];
  const setFilterClass = (args: any) => {
    debugger;
    setselectedFilter(args);
    props.sendDataToParent(args);
  };

  return (
    <React.Fragment>
      <div className="newFilters">
        <div
          className="bread-text pull-left"
          style={{ paddingBottom: "13px", paddingTop: "12px" }}
        >
          <span className="lt-head">
            <a href="/employer/dashboard/0">My Jobs</a>
          </span>
          <span className="lt-text">
            <i className="fal fa-angle-right"></i>
          </span>
          <span className="lt-head-bold">Candidates</span>
        </div>
        <div className="pull-right">
          {filtersArr.map((filter) => {
            return (
              <span
                hidden={
                  (filter.filterName == "Interviews" &&
                    props.candidateStatus <
                      hiringStagesConstants.Approved_By_Employer) ||
                  (filter.filterName == "Offering" &&
                    props.candidateStatus < hiringStagesConstants.Interviews)
                }
                key={filter.filterName}
                onClick={() => setFilterClass(filter.filterStatus)}
                className={
                  selectedFilter == filter.filterStatus
                    ? "newfilterItem activeItem"
                    : "newfilterItem"
                }
              >
                <i className={filter.iconClass}></i>
                {filter.filterName}
              </span>
            );
          })}
        </div>
      </div>
    </React.Fragment>
  );
}
