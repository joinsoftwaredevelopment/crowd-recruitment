import React, { useEffect } from "react";
import { createStyles, makeStyles, Theme } from "@material-ui/core/styles";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import Grid from "@material-ui/core/Grid";
import thank from "../../../src/assets/thankful.png";

export default function Profile(props: any) {
  return (
    <React.Fragment>
      <div className="">
      <iframe
        src={props.cvPath}
        width="100%"
        height="500px"
        />
      </div>
    </React.Fragment>
  );
}
