import React, { useState, useEffect } from "react";
import { createStyles, makeStyles, Theme } from "@material-ui/core/styles";
import TextField from "@material-ui/core/TextField";
import Button from "../../../components/Buttons/Button";
import Grid from "@material-ui/core/Grid";
import thank from "../../../src/assets/thankful.png";
import CandidateService from "../../../services/CandidateService";
import { IInterviewsModel } from "../../../models/IInterviewsModel";
import SetInterview from "./SetInterview";
import AddInterview from '../../candidate/JobBoard/SetInterview'
import { IInterviewDetailsModel } from "../../../models/IInterviewDetailsModel";

export default function Interviews(props: any) {
  useEffect(() => {
    getAllInterviews();
  }, []);

  const updateInterviewCount = () => {
    getAllInterviews();
  };

  const [allInterviews, setAllInterviews] = useState<IInterviewsModel[]>([]);

  const getAllInterviews = () => {
    debugger;
    CandidateService.GetAllInterviewsByUuid(props.uuid)
      .then((response) => {
        debugger;
        setAllInterviews(response.data.data);
      })
      .catch((e) => {
        console.log(e);
      });
  };

  const DeleteMeeting = (interviewId: number, isCanceled: boolean) => {
    CandidateService.CancelInterview(interviewId, isCanceled)
      .then((response) => {
        debugger;
        //window.location.reload();
        // getAllJobCandidates();
        getAllInterviews();
      })
      .catch((e) => {
        //window.location.reload();
        // getAllJobCandidates();
        getAllInterviews();
      });
  };

  var dateFormat = require("dateformat");

  const [showMore, setshowMore] = useState(true);
  const handleShowMore = (args: any) => {
    setshowMore(args);
  };
  const [currentUuid, setCurrentUuid] = React.useState("");

  const initialAddInterviewDetails: IInterviewDetailsModel = {
    submissionUUid: currentUuid,
    interviewerId: 1,
    interviewComment: "",
    interviewTypeId: 1,
    interviewDate: new Date(),
    interviewTime: "12:00",
    duration: 1,
    templateId: 0,
    emailSubject: "",
    emailDescription: "",
    interviewerTypeId: 1,
    id: 0,
  };

  const [addInterviewDetails, setAddInterviewDetails] = useState(
    initialAddInterviewDetails
  );

  const [currentId, setCurrentId] = React.useState(0);
  const [openInterview, setOpenInterview] = React.useState(false);
  const [openEditInterview, setOpenEditInterview] = React.useState(false);

  const GetMeetingDetails = (interviewId: number, submissionUuId: string) => {
    debugger;
    CandidateService.GetMeetingDetails(interviewId)
      .then((response) => {
        debugger;
        addInterviewDetails.id = response.data.data.id;
        addInterviewDetails.submissionUUid = submissionUuId;
        setAddInterviewDetails(addInterviewDetails);
        setAddInterviewDetails(response.data.data);
      })
      .catch((e) => {
        console.log(e);
      });
  };

  const initialTitle = [
    {
      id: 0,
      emailTitleEn: "",
      emailtitleAr: "",
      emailbodyEn: "",
      emailbodyAr: "",
    },
  ];

  const [templates, setTemplates] = React.useState(initialTitle);

  const getAllTemplates = () => {
    debugger;
    CandidateService.GetAllTemplates()
      .then((response) => {
        setTemplates(response.data.data);
      })
      .catch((e) => {
        console.log(e);
      });
  };

  const handleSideBarInterview = (args: any, uuid: string, id: number) => {
    setCurrentUuid(uuid);
    setCurrentId(id);
    setOpenInterview(args);
    getAllTemplates();
    GetMeetingDetails(id, uuid);
  };

  const handleSideBar = (args: any,uuid: string) => {
    debugger;
    setCurrentUuid(uuid);
    setOpenEditInterview(args);
  };

  return (
    <React.Fragment>
      <div className="padBox">
        <button
          onClick={() => handleSideBar(true, props.uuid)}
          className="btn btn-decline "
        >
          <i className="fal fa-plus"></i> Add Interview
        </button>
        <hr></hr>
        {allInterviews.map((item: IInterviewsModel) => (
          <div className="meetingRowParent">
            <div className="meetingRow">
              <div className="font-14 pb-5">
                <span className="blue bold">Interview Type: </span>
                <span className="black bold">
                  {item.interviewTypeId == 1
                    ? "Telephone"
                    : item.interviewTypeId == 2
                    ? "On site"
                    : "Web conferenc"}
                </span>
              </div>

              <div className="flowRoot">
                <div className="pull-left">
                  {/* <span className='bold light'>Tuesday : </span> */}
                  <span className="light">
                    {dateFormat(item.interviewDate, "d mmmm, yyyy")} ,{" "}
                    {item.interviewTime}
                  </span>
                </div>
                <div className="pull-right">
                  {showMore ? (
                    <div>
                      <Button
                        isLoading={false}
                        border="none"
                        fontSize="14px"
                        font="Proxima-Regular"
                        color="#fff"
                        bgColor="#0195FF"
                        height="32px"
                        onClick={() => handleShowMore(false)}
                        radius=""
                        width="100px"
                        children="Show More"
                      />
                    </div>
                  ) : (
                    <div>
                      <a
                        className="greenColor font-12"
                        onClick={() =>
                          handleSideBarInterview(
                            true,
                            item.submissionUuid,
                            item.id
                          )
                        }
                      >
                        Edit
                      </a>
                      <a
                        className="redColor font-12 pl-10"
                        onClick={() => DeleteMeeting(item.id, true)}
                      >
                        Delete
                      </a>
                    </div>
                  )}
                </div>
              </div>
            </div>

            {!showMore ? (
              <div className="padBox">
                <div className="row">
                  <div className="col-md-4">
                    <div className="meetingRowGroup">
                      <div className="mettingRowLabel">Interviewer Name</div>
                      <div className="font-14">{item.interviewerName}</div>
                    </div>
                  </div>

                  <div className="col-md-4">
                    <div className="meetingRowGroup">
                      <div className="mettingRowLabel">Email Subject</div>
                      <div className="font-14">{item.emailSubject}</div>
                    </div>
                  </div>
                </div>

                <div className="row">
                  <div className="col-md-4">
                    <div className="meetingRowGroup">
                      <div className="mettingRowLabel">Interview Date</div>
                      <div className="font-14">
                        {dateFormat(item.interviewDate, "d mmmm, yyyy")} ,{" "}
                        {item.interviewTime}
                      </div>
                    </div>
                  </div>

                  <div className="col-md-4">
                    <div className="meetingRowGroup">
                      <div className="mettingRowLabel">Time</div>
                      <div className="font-14">{item.interviewTime}</div>
                    </div>
                  </div>

                  <div className="col-md-4">
                    <div className="meetingRowGroup">
                      <div className="mettingRowLabel">Duration</div>
                      <div className="font-14">{item.duration}</div>
                    </div>
                  </div>

                  <div className="col-md-12">
                    <div className="meetingRowGroup">
                      <div className="mettingRowLabel">Interview Comment</div>
                      <div className="font-14">{item.interviewComment}</div>
                    </div>
                  </div>

                  <div className="col-md-12">
                    <div className="meetingRowGroup">
                      <div className="mettingRowLabel">Email Desctiption</div>
                      <div
                        className="font-14"
                        dangerouslySetInnerHTML={{
                          __html: item.emailDescription,
                        }}
                      />
                    </div>
                  </div>

                  {/* <div className='col-md-12'>
      <div className='meetingRowGroup'>
      <div className='mettingRowLabel'>Email Offer</div>
      <div className='grayBox'>
      [JOB TITLE] Job offer</div>
      </div>

      </div> */}
                </div>

                <div className="flowRoot">
                  <div className="pull-right">
                    <Button
                      isLoading={false}
                      border="none"
                      fontSize="14px"
                      font="Proxima-Regular"
                      color="#fff"
                      bgColor="#0195FF"
                      height="32px"
                      onClick={() => handleShowMore(true)}
                      radius=""
                      width="100px"
                      children="Show Less"
                    />
                  </div>
                </div>
              </div>
            ) : (
              ""
            )}
          </div>
        ))}
        <SetInterview
          handleSideBarInterview={handleSideBarInterview}
          data={currentUuid}
          interviewId={currentId}
          openInterview={openInterview}
          InterviewDetails={addInterviewDetails}
          AllTemplates={templates}
          updateInterview={updateInterviewCount}
        />

        <AddInterview
      handleSideBarInterview={handleSideBar}
      data={currentUuid}
      openInterview={openEditInterview}
      updateInterview = {updateInterviewCount}
       />

      </div>
    </React.Fragment>
  );
}
