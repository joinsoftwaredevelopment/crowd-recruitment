import React, { useState, useEffect } from "react";
import { createStyles, makeStyles, Theme } from "@material-ui/core/styles";
import TextField from "@material-ui/core/TextField";
import Button from "../../../components/Buttons/Button";
import Grid from "@material-ui/core/Grid";
import thank from "../../../src/assets/thankful.png";
import empIco from "../../../../src/assets/job-board-icon.png";
import Filters from "./Filters";
import Profile from "./Profile";
import Offering from "./Offering";
import Interviews from "./Interviews";
import { hiringStagesConstants } from "../../../constants/hiringStagesConstants";
import JobService from "../../../services/JobService";
import { IJobCandidate } from "../../../models/IJobCandidate";
import CandidateService from "../../../services/CandidateService";
import { ICandidatStatusHistory } from "../../../models/ICandidatStatusHistory";
import sourcerService from "../../../services/SourcerService";
import { constants } from "buffer";
import { IInterviewsModel } from "../../../models/IInterviewsModel";

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {},
  })
);

export default function CandidateInterview(props: any) {
  const [selectedComponent, setselectedComponent] = React.useState(
    hiringStagesConstants.Approved_By_Employer
  );
  const sendDataToParent = (args: any) => {
    setselectedComponent(args);
  };

  const [currentCandidate, setCurrentCandidate] = useState<IJobCandidate>({
    candidateName: "",
    countryName: "",
    cvName: "",
    jobDate: "",
    jobId: 0,
    jobTitle: "",
    jobUuId: "",
    languageId: 2,
    linkedInProfile: "",
    rowNumber: 0,
    sourcerId: 0,
    sourcerName: "",
    submissionDate: "",
    submissionUuId: "",
    candidateSubmissionStatusId: 0,
    firstMailSubject: "",
    firstMailDescription: "",
    email: "",
    rejectionReason: "",
    candidateUuId: props.match.params.id,
    submissionStatusName: "",
    isCvApproved: false,
    isAcceptOffer: false,
    isHired: false,
    hasInterview: 0,
    jobStatusId: 0,
  });

  var dateFormat = require("dateformat");

  const [candidateStatusHistory, setAllCandidateStatusHistory] = useState<
    ICandidatStatusHistory[]
  >([]);

  const [path, setPath] = useState("");

  const openPdf = (fileName: string) => {
    sourcerService
      .getPdf(fileName)
      .then((response) => {
        debugger;
        console.log(response.data);
        //Create a Blob from the PDF Stream
        const file = new Blob([response.data], { type: "application/pdf" });
        //Build a URL from the file
        const fileURL = URL.createObjectURL(file);
        setPath(fileURL);
        //Open the URL on new Window
        //window.open(fileURL);
      })
      .catch((e) => {
        console.log(e);
      });
  };

  const getJobCandidateByUuId = () => {
    JobService.getJobCandidateByUuId(props.match.params.id)
      .then((response) => {
        debugger;
        setCurrentCandidate(response.data.data);
        currentCandidate.cvName = response.data.data.cvName;
        openPdf(response.data.data.cvName);
      })
      .catch((e) => {
        console.log(e);
      });
  };

  const getCandidateStatusHistoryByUuId = () => {
    debugger;
    CandidateService.GetCandidateStatusHistory(props.match.params.id)
      .then((response) => {
        console.log(response.data);
        setAllCandidateStatusHistory(response.data.data);
      })
      .catch((e) => {
        console.log(e);
      });
  };
  useEffect(() => {
    getJobCandidateByUuId();
    getCandidateStatusHistoryByUuId();
  }, []);

  return (
    <React.Fragment>
      <div className="candInterview">
        <Filters
          jobStatus={hiringStagesConstants.Approved_By_Employer}
          sendDataToParent={sendDataToParent}
          cvPath={path}
          candidateStatus={currentCandidate.candidateSubmissionStatusId}
        />
        <div className="row">
          <div className="col-md-4">
            <div className="noPadBox">
              <div className="profileInfo text-center">
                <div className="data-img">
                  <img src={empIco} />
                </div>
                <div className="data-card-head">
                  {currentCandidate.candidateName}
                </div>
                <div className="data-card-subhead">
                  {currentCandidate.jobTitle}
                </div>
                <div className="data-card-light">
                  {currentCandidate.countryName}
                </div>

                <div className="data-links">
                  <span className="table-link">
                    <a target="_blank" href={currentCandidate.linkedInProfile}>
                      in
                    </a>
                  </span>
                  {/* <span className='table-link'> <i className='fal fa-phone'></i> </span>
          <span className='table-link'><i className='fal fa-envelope'></i></span> */}
                </div>
              </div>

              <div className="track-order-list">
                <ul className="list-unstyled">
                  {candidateStatusHistory.map(
                    (status: ICandidatStatusHistory) => (
                      <li
                        className={
                          status.candidateSubmissionStatusId ==
                          hiringStagesConstants.Approved_By_Employer
                            ? "completed"
                            : ""
                        }
                      >
                        <h5
                          className={
                            status.candidateSubmissionStatusId ==
                            hiringStagesConstants.Approved_By_Employer
                              ? "mt-0 mb-1 greenStat"
                              : "mt-0 mb-1"
                          }
                        >
                          {status.statusName}{" "}
                        </h5>
                        <p className="text-muted">
                          {dateFormat(
                            status.createDate,
                            "d mmmm, yyyy HH:MM TT"
                          )}
                        </p>

                        {status.candidateSubmissionStatusId ==
                        hiringStagesConstants.Interviews ? (
                          <div className="pull-left">
                            {currentCandidate.hasInterview ? (
                              <div className="greenColor font-12">
                                {" "}
                                Interview added
                              </div>
                            ) : (
                              <div className="redColor font-12">
                                {" "}
                                No interview yet
                              </div>
                            )}
                          </div>
                        ) : (
                          ""
                        )}

                        {status.candidateSubmissionStatusId ==
                        hiringStagesConstants.Offer ? (
                          <div className="pull-left">
                            {currentCandidate.isAcceptOffer ? (
                              <div className="greenColor font-12">
                                {" "}
                                Accepted
                              </div>
                            ) : (
                              <div className="redColor font-12">
                                {" "}
                                Hasn't accepted
                              </div>
                            )}
                          </div>
                        ) : (
                          ""
                        )}

                        {status.candidateSubmissionStatusId ==
                        hiringStagesConstants.Hiring ? (
                          <div className="pull-left">
                            {currentCandidate.isHired ? (
                              <div className="greenColor font-12"> Hired</div>
                            ) : (
                              <div className="redColor font-12">
                                {" "}
                                Hasn't hired yet
                              </div>
                            )}
                          </div>
                        ) : (
                          ""
                        )}
                      </li>
                    )
                  )}
                </ul>
              </div>
            </div>
          </div>
          <div className="col-md-8">
            {selectedComponent == hiringStagesConstants.Approved_By_Employer ? (
              <Profile cvPath={path} />
            ) : (
              ""
            )}
            {selectedComponent == hiringStagesConstants.Offer ? (
              <Offering uuid={currentCandidate.submissionUuId} />
            ) : (
              ""
            )}
            {selectedComponent == hiringStagesConstants.Interviews ? (
              <Interviews uuid={currentCandidate.submissionUuId} />
            ) : (
              ""
            )}
          </div>
        </div>
      </div>
    </React.Fragment>
  );
}
