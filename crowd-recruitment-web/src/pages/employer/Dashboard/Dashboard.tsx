import React, { useState, useEffect } from "react";
import {
  createStyles,
  Theme,
  WithStyles,
  makeStyles,
  withStyles,
} from "@material-ui/core/styles";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import Grid from "@material-ui/core/Grid";
import decline from "../../../../src/assets/close.png";
import confirm from "../../../../src/assets/decline.png";
import ModalBg from "../../../../src/assets/modal-bg.png";
import fill1 from "../../../../src/assets/n-fill-1.png";
import fill2 from "../../../../src/assets/n-fill-2.png";
import JobCard from "./JobCard";
import Filters from "./Filters";
import CircularProgress from "@material-ui/core/CircularProgress";
import { IJobSummary } from "../../../models/IJobSummary";
import JobService from "../../../services/JobService";
import { jobStatusConstant } from "../../../constants/jobStatusConstant";
import { history } from "../../../helpers/history";

import IconButton from "@material-ui/core/IconButton";
import CloseIcon from "@material-ui/icons/Close";
import Typography from "@material-ui/core/Typography";
import { ICloseJob } from "../../../models/ICloseJob";

import Dialog from "@material-ui/core/Dialog";
import MuiDialogTitle from "@material-ui/core/DialogTitle";
import MuiDialogContent from "@material-ui/core/DialogContent";
import MuiDialogActions from "@material-ui/core/DialogActions";
import classes from "*.module.css";
import Counters from "./Counters";

const styles = (theme: Theme) =>
  createStyles({
    root: {
      margin: 0,
      padding: theme.spacing(2),
    },
    closeButton: {
      position: "absolute",
      right: theme.spacing(1),
      top: theme.spacing(1),
      color: theme.palette.grey[500],
    },
  });

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {},
  })
);

export default function Dashboard(props: any) {
  const [openCloseJob, setOpenCloseJob] = React.useState(false);
  const [openFillJob, setOpenFillJob] = React.useState(false);
  const [openReason, setOpenReason] = React.useState(false);
  const [openDeleteJob, setOpenDeleteJob] = React.useState(false);

  const [rejectionReason, setRejectionReason] = React.useState("");
  const [deletedID, setDeletedID] = React.useState("");
  const [currentTemp, setCurrentTemp] = React.useState(1);

  const [fillBoxState, setfillBoxState] = React.useState(1);
  const [switchState, setSwitchState] = React.useState(false);
  const handleSwitch = (event: React.ChangeEvent<HTMLInputElement>) => {
    console.log("HI this is switch ");
    setSwitchState(!switchState);
  };
  const handleClose = () => {
    setOpenCloseJob(false);
    setOpenFillJob(false);
    setOpenReason(false);
    setOpenDeleteJob(false);
  };
  const openClosePopup = (args: any) => {
    setOpenCloseJob(true);
  };
  const openDeletePopup = (args: any) => {
    setDeletedID(args);
    setOpenDeleteJob(true);
  };
  const openReasonPopup = (args: any) => {
    console.log(args);
    setRejectionReason(args);
    setOpenReason(true);
  };
  const openFillPopup = () => {
    setOpenFillJob(true);
  };
  const chooseFill = (args: any) => {
    setfillBoxState(args);
    if (args == 2) {
      setCurrentTemp(2);
    } else {
      setCurrentTemp(1);
    }
  };

  const RedirectToFill = () => {
    if (currentTemp == 2) {
      history.push("/employer/EmployerJobDetails/0/0");
    } else {
      history.push("/employer/selecttemplateemployer");
    }
  };

  const [showLoader, setShowLoader] = React.useState(false);
  const [jobs, setJobs] = useState<IJobSummary[]>([]);
  const [filteredJobs, setFilteredJobs] = useState<IJobSummary[]>([]);

  useEffect(() => {
    debugger;
    setShowLoader(true);
    if (props.match.params.id == undefined) retrieveAllJobs();
    else retrieveAllJobsByStatusId();
  }, []);

  const retrieveAllJobs = () => {
    //debugger;
    JobService.GetAllJobs()
      .then((response) => {
        debugger;
        setJobs(response.data.data);
        setFilteredJobs(response.data.data);
        setShowLoader(false);
      })
      .catch((e) => {
        console.log(e);
        setShowLoader(false);
      });
  };

  const retrieveAllJobsByStatusId = () => {
    debugger;
    JobService.GetAllJobs()
      .then((response) => {
        debugger;

        let jobsData: IJobSummary[] = response.data.data;
        setJobs(jobsData);
        if (props.match.params.id != undefined && props.match.params.id != 0)
          jobsData = response.data.data.filter(
            (m: IJobSummary) => m.jobStatusId == props.match.params.id
          );

        setFilteredJobs(jobsData);
        setShowLoader(false);
      })
      .catch((e) => {
        console.log(e);
        setShowLoader(false);
      });
  };

  const DeleteJob = () => {
    debugger;
    JobService.DeleteJob(deletedID)
      .then((response) => {
        retrieveAllJobs();
        handleClose();
      })
      .catch((e) => {
        console.log(e);
      });
  };

  function handleChange(filteredJobs: any) {
    setShowLoader(true);
    setFilteredJobs(filteredJobs);
    setShowLoader(false);
  }

  const CloseJob = (uuId: string) => {
    debugger;
    const currentJob: ICloseJob = {
      uuId: uuId,
    };
    JobService.CloseJob(currentJob)
      .then((response) => {
        if (props.match.params.id == undefined) {
          retrieveAllJobs();
        } else {
          retrieveAllJobsByStatusId();
        }
      })
      .catch((e) => {
        console.log(e);
      });
  };

  return (
    <React.Fragment>
      <div className="empJobDashboard">
        <div className="" style={{ marginTop: "5px" }}>
          <Filters
            jobStatus={props.match.params.id}
            openFillPopup={openFillPopup}
            jobs={jobs}
            onChange={handleChange}
          />
          <div className="row" style={{ marginTop: "5px" }}>
            {showLoader === true ? <CircularProgress /> : ""}
            <Counters jobs={jobs} />
          </div>

          <div className="row" style={{ marginTop: "15px" }}>
            {filteredJobs.map((m) => {
              return (
                <div key={m.jobId} className="col-md-4">
                  <JobCard
                    uuid={m.uuid}
                    rejectionReason={m.rejectionReason}
                    openDeletePopup={openDeletePopup}
                    handleSwitch={handleSwitch}
                    switchState={switchState}
                    openReasonPopup={openReasonPopup}
                    openClosePopup={openClosePopup}
                    type={m.jobStatusName.toLowerCase()}
                    title={m.jobTitle}
                    location={m.jobLocation}
                    jobStatusId={m.jobStatusId}
                    edited={m.jobDate}
                    closeJob={() => CloseJob(m.uuid)}
                  />
                </div>
              );
            })}
          </div>
        </div>

        <Dialog
          fullWidth={true}
          maxWidth={"xs"}
          onClose={handleClose}
          aria-labelledby="customized-dialog-title"
          open={openDeleteJob}
        >
          <img className="modalBg" src={ModalBg} />
          <DialogTitle
            id="customized-dialog-title3"
            onClose={handleClose}
            children=""
          ></DialogTitle>

          <DialogContent className="text-center">
            <img style={{ marginTop: "20px" }} src={decline} />

            <div className="head-job-pop">
              <div className="blackHead">
                Are you sure you want to delete this job?
              </div>
            </div>
            <div className="text-center job-pop-btns">
              <button onClick={handleClose} className="btn btn-decline">
                No
              </button>
              <button onClick={DeleteJob} className="btn btn-approve">
                Yes
              </button>
            </div>
          </DialogContent>
        </Dialog>

        <Dialog
          fullWidth={true}
          maxWidth={"xs"}
          onClose={handleClose}
          aria-labelledby="customized-dialog-title"
          open={openCloseJob}
        >
          <img className="modalBg" src={ModalBg} />
          <DialogTitle
            id="customized-dialog-title3"
            onClose={handleClose}
            children=""
          ></DialogTitle>

          <DialogContent className="text-center">
            <img style={{ marginTop: "20px" }} src={decline} />

            <div className="head-job-pop">
              <div className="blackHead">
                Are you sure you want to close this job?
              </div>
            </div>
            <div className="text-center job-pop-btns">
              <button onClick={handleClose} className="btn btn-decline">
                No
              </button>
              <button onClick={handleClose} className="btn btn-approve">
                Yes
              </button>
            </div>
          </DialogContent>
        </Dialog>

        <Dialog
          fullWidth={true}
          maxWidth={"xs"}
          onClose={handleClose}
          aria-labelledby="customized-dialog-title"
          open={openReason}
        >
          <img className="modalBg" src={ModalBg} />
          <DialogTitle
            id="customized-dialog-title3"
            onClose={handleClose}
            children=""
          ></DialogTitle>

          <DialogContent className="text-center">
            <img style={{ marginTop: "20px" }} src={confirm} />

            <div className="head-job-pop">
              <div className="blackHead">
                Job has been declinded for the following reason
              </div>
              <div className="shortBorder"></div>
            </div>
            <p className="blurText font-14">{rejectionReason}</p>
            <div className="text-center job-pop-btns">
              <button onClick={handleClose} className="btn btn-approve">
                Ok
              </button>
            </div>
          </DialogContent>
        </Dialog>

        <Dialog
          fullWidth={true}
          maxWidth={"xs"}
          onClose={handleClose}
          aria-labelledby="customized-dialog-title"
          open={openFillJob}
        >
          <img className="modalBg" src={ModalBg} />
          <DialogTitle
            id="customized-dialog-title3"
            onClose={handleClose}
            children=""
          ></DialogTitle>

          <DialogContent className="text-center">
            <div className="head-job-pop">
              <div className="blackHead">Choose how to fill Your job post</div>
              <div className="shortBorder"></div>
            </div>
            <div className="fillOptions">
              <div
                style={{ marginBottom: "30px" }}
                className={fillBoxState == 1 ? "fillBox activeF" : "fillBox"}
                onClick={() => chooseFill(1)}
              >
                <img style={{ marginTop: "0px" }} src={fill1} />
                <div style={{ marginTop: "10px" }}>Choose from library</div>
              </div>

              <div
                className={fillBoxState == 2 ? "fillBox activeF" : "fillBox"}
                onClick={() => chooseFill(2)}
              >
                <img style={{ marginTop: "0px" }} src={fill2} />
                <div style={{ marginTop: "10px" }}>Fill Data from scratch</div>
              </div>
            </div>

            <div className="text-center job-pop-btns">
              <button onClick={RedirectToFill} className="btn btn-approve">
                Next
              </button>
            </div>
          </DialogContent>
        </Dialog>
      </div>
    </React.Fragment>
  );
}

export interface DialogTitleProps extends WithStyles<typeof styles> {
  id: string;
  children: React.ReactNode;
  onClose: () => void;
}

const DialogTitle = withStyles(styles)((props: DialogTitleProps) => {
  const { children, classes, onClose, ...other } = props;
  return (
    <MuiDialogTitle disableTypography className={classes.root} {...other}>
      <Typography variant="h6">{children}</Typography>
      {onClose ? (
        <IconButton
          aria-label="close"
          className={classes.closeButton}
          onClick={onClose}
        >
          <CloseIcon />
        </IconButton>
      ) : null}
    </MuiDialogTitle>
  );
});

const DialogContent = withStyles((theme: Theme) => ({
  root: {
    padding: theme.spacing(2),
  },
}))(MuiDialogContent);
