import React, { useEffect } from "react";
import { jobStatusConstant } from "../../../constants/jobStatusConstant";
import { IJobSummary } from "../../../models/IJobSummary";
import { useHistory } from "react-router-dom";
import { jobActions } from "../../../actions/jobActions";

export default function Filters(props: any) {
  //debugger;
  const [selectedFilter, setselectedFilter] = React.useState(props.jobStatus);
  const history = useHistory();

  const filtersArr = [
    {
      filterStatus: jobStatusConstant.All,
      filterName: "All Jobs",
      iconClass: "fal fa-bars",
    },
    {
      filterStatus: jobStatusConstant.Opened,
      filterName: "Active",
      iconClass: "fal fa-check",
    },
    {
      filterStatus: jobStatusConstant.Closed,
      filterName: "Closed",
      iconClass: "fal fa-times",
    },
    {
      filterStatus: jobStatusConstant.Draft,
      filterName: "Draft",
      iconClass: "fal fa-pen",
    },
    {
      filterStatus: jobStatusConstant.Posted,
      filterName: "Pending",
      iconClass: "fal fa-spinner",
    },
  ];

  const setFilterClass = (args: any) => {
    history.push("/employer/dashboard/" + args);
    setselectedFilter(args);
    jobActions.retrieveAllJobs().then((response) => {
      debugger;
      let jobsData: IJobSummary[] = response.data.data;
      if (Number(args) == jobStatusConstant.All) {
        props.onChange(jobsData);
      } else {
        props.onChange(jobsData.filter((m) => m.jobStatusId == Number(args)));
      }
    });
  };

  useEffect(() => {
    // fetch your data when the props.location changes
    //console.log(props.jobStatus);
    setFilterClass(props.jobStatus);
  }, [props.jobStatus]);

  return (
    <React.Fragment>
      <div className="newFilters">
        <div className="pull-left">
          <div className="bold-head">My Jobs</div>
        </div>
        <div className="pull-right">
          {filtersArr.map((filter) => {
            return (
              <span
                key={filter.filterName}
                onClick={() => setFilterClass(filter.filterStatus)}
                className={
                  selectedFilter == filter.filterStatus
                    ? "newfilterItem activeItem"
                    : "newfilterItem"
                }
              >
                <i className={filter.iconClass}></i>
                {filter.filterName}
              </span>
            );
          })}

          <button onClick={props.openFillPopup} className="btn btn-decline">
            <i className="fal fa-plus"></i>Add Job
          </button>
        </div>
      </div>
    </React.Fragment>
  );
}
