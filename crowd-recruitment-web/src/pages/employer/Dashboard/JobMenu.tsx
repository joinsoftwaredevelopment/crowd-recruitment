import React, { useState, useEffect } from "react";
import { jobStatusConstant } from "../../../constants/jobStatusConstant";
import { IJobSummary } from "../../../models/IJobSummary";
import { useHistory } from "react-router-dom";
import JobService from "../../../services/JobService";
import { jobActions } from "../../../actions/jobActions";

export default function JobMenu(props: any) {
  debugger;
  const [selectedFilter, setselectedFilter] = React.useState(props.jobStatus);
  const [jobs, setJobs] = useState<IJobSummary[]>([]);

  const history = useHistory();

  const NoOfJobsByStatus = (jobStatusId: any) =>
    jobs.filter((m: IJobSummary) => m.jobStatusId == jobStatusId).length;

  const JobPagesArr = [
    {
      status: jobStatusConstant.Opened,
      title: "Active",
      className: "blueBg",
      count: NoOfJobsByStatus(Number(jobStatusConstant.Opened)),
    },
    {
      status: jobStatusConstant.Closed,
      title: "Closed",
      className: "redBg",
      count: NoOfJobsByStatus(Number(jobStatusConstant.Closed)),
    },
    {
      status: jobStatusConstant.Draft,
      title: "Draft",
      className: "grayBg",
      count: NoOfJobsByStatus(Number(jobStatusConstant.Draft)),
    },
    {
      status: jobStatusConstant.Posted,
      title: "Pending",
      className: "orangeBg",
      count: NoOfJobsByStatus(Number(jobStatusConstant.Posted)),
    },
  ];

  const setFilterClass = (args: any) => {
    history.push("/employer/dashboard/" + args);
  };

  return (
    <React.Fragment>
      {JobPagesArr.map((page) => {
        return (
          <div key={page.title} className="customMenuList">
            <div
              className={
                selectedFilter == page.className
                  ? "customMenuList activeItem"
                  : "customMenuList"
              }
              onClick={() => setFilterClass(page.status)}
            >
              <i className="far fa-circle"></i>
              <span>{page.title}</span>
            </div>
            <span className={"lableMini  pull-right " + page.className}>
              {page.count}
            </span>
          </div>
        );
      })}
    </React.Fragment>
  );
}
