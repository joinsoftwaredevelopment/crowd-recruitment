import React, { useEffect, useState } from "react";
import { jobActions } from "../../../actions/jobActions";
import Widget from "../../../components/Widgets/Widgets";
import { jobStatusConstant } from "../../../constants/jobStatusConstant";
import { IJobSummary } from "../../../models/IJobSummary";

export default function Counters(props: any) {
  const [jobs, setJobs] = useState<IJobSummary[]>([]);

  useEffect(() => {
    jobActions.retrieveAllJobs().then((response) => {
      //debugger;
      setJobs(response.data.data);
    });
  }, []);

  const NoOfJobsByStatus = (jobStatusId: any) =>
    jobs.filter((m: IJobSummary) => m.jobStatusId == jobStatusId).length;

  const WidgetArr = [
    {
      color: "#0195FF",
      icon: "fal fa-check",
      title: "Opened Jobs",
      number: NoOfJobsByStatus(jobStatusConstant.Opened),
    },
    {
      color: "#E96666",
      icon: "fal fa-times",
      title: "Closed Jobs",
      number: NoOfJobsByStatus(jobStatusConstant.Closed),
    },
    {
      color: "#989898",
      icon: "fal fa-pen",
      title: "Drafted Jobs",
      number: NoOfJobsByStatus(jobStatusConstant.Draft),
    },
    {
      color: "#FF7800",
      icon: "fal fa-times",
      title: "Pending Jobs",
      number: NoOfJobsByStatus(jobStatusConstant.Posted),
    },
  ];

  return (
    <React.Fragment>
      {WidgetArr.map((widget) => {
        return (
          <div key={widget.title} className="col-md-3">
            <Widget
              color={widget.color}
              icon={widget.icon}
              title={widget.title}
              number={widget.number}
            />
          </div>
        );
      })}
    </React.Fragment>
  );
}
