import React from "react";
import empIco from "../../../../src/assets/emp-ico.png";
import Button from "../../../components/Buttons/Button";
import Switch from "@material-ui/core/Switch";
import { history } from "../../../helpers/history";
import { jobStatusConstant } from "../../../constants/jobStatusConstant";
import JobService from "../../../services/JobService";
import { ICloseJob } from "../../../models/ICloseJob";

interface Props {
  uuid: string;
  rejectionReason: string;
  type: string;
  title: string;
  location: string;
  jobStatusId: number;
  edited: string;
  switchState: boolean;
  openClosePopup: (args: any) => void;
  openReasonPopup: (args: any) => void;
  openDeletePopup: (args: any) => void;
  handleSwitch: (event: React.ChangeEvent<HTMLInputElement>) => void;
  closeJob: (args: any) => void;
}

const JobCard: React.FC<Props> = ({
  uuid,
  rejectionReason,
  type,
  title,
  location,
  edited,
  jobStatusId,
  openClosePopup,
  openReasonPopup,
  openDeletePopup,
  switchState,
  handleSwitch,
  closeJob,
}) => {
  // const CloseJob = () => {
  //   debugger;
  //   const currentJob: ICloseJob = {
  //     uuId: uuid,
  //   };
  //   JobService.CloseJob(currentJob)
  //     .then((response) => {
  //       window.location.reload();
  //     })
  //     .catch((e) => {
  //       console.log(e);
  //     });
  // };
  return (
    <div className={"job-box " + type} style={{ borderColor: type }}>
      <div className="clearBoth flowRoot" style={{ height: "38px" }}>
        <div className="pull-left">
          <div className="jobLabel">{type}</div>
        </div>
        <div className="pull-right">
          {type == "opened" ? (
            <Switch
              id={uuid}
              key={uuid}
              name={uuid}
              checked={jobStatusId == jobStatusConstant.Closed ? false : true}
              onChange={closeJob}
              color="primary"
            />
          ) : (
            ""
          )}

          {type == "draft" ? (
            <i
              onClick={() => openDeletePopup(uuid)}
              className="far fa-times-circle crossIco"
            ></i>
          ) : (
            ""
          )}
        </div>
      </div>

      <div className="clearBoth flowRoot" style={{ margin: "0px 0px 10px" }}>
        <div className="float-left" style={{ marginRight: "10px" }}>
          <img src={empIco} />
        </div>
        <div className="float-left">
          <div className="data-card-head">{title}</div>
          <div className="data-card-light">{location}</div>
        </div>
      </div>

      <div className="job-card-btm">
        <div className="data-card-light pull-left">
          <i className="fal fa-clock" style={{ paddingRight: "5px" }}></i>
          {edited}
        </div>
        <div className="pull-right">
          {type == "draft" ? (
            <Button
              isLoading={false}
              border="none"
              fontSize="12px"
              font="Proxima-Regular"
              color="#0195ff"
              bgColor="#e1f0fb"
              height="25px"
              onClick={() =>
                history.push("/employer/employerjobdetails/" + uuid + "/0")
              }
              radius=""
              width=""
              children="Continue sourcing"
            />
          ) : (
            ""
          )}

          {type == "opened" ? (
            <Button
              isLoading={false}
              border="none"
              fontSize="12px"
              font="Proxima-Regular"
              color="#0195ff"
              bgColor="#e1f0fb"
              height="25px"
              onClick={() => history.push("/employer/EmpjobBoard/" + uuid)}
              radius=""
              width=""
              children="Candidates"
            />
          ) : (
            ""
          )}

          {type == "declined" ? (
            <Button
              isLoading={false}
              border="none"
              fontSize="12px"
              font="Proxima-Regular"
              color="#E96666"
              bgColor="#FBE7E7"
              height="25px"
              onClick={() => openReasonPopup(rejectionReason)}
              radius=""
              width=""
              children="Reason"
            />
          ) : (
            ""
          )}
          <Button
            isLoading={false}
            border="none"
            fontSize="12px"
            font="Proxima-Regular"
            color="white"
            bgColor="#0195FF"
            height="25px"
            onClick={() => history.push("/employer/jobpreviewemployer/" + uuid)}
            radius=""
            width=""
            children="Show details"
          />
        </div>
      </div>
    </div>
  );
};

export default JobCard;
