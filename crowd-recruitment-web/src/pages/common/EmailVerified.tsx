import React, { useEffect } from "react";
import { createStyles, makeStyles, Theme } from "@material-ui/core/styles";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import Grid from "@material-ui/core/Grid";
import thank from "../../../src/assets/thankful.png";
import { useHistory } from "react-router-dom";

import TopDots from "../../../src/assets/dots-top.png";
import BottomDots from "../../../src/assets/dots-bottom.png";
import Anime from "react-anime";
import anime from "animejs/lib/anime.es.js";
import UserService from "../../services/UserService";

export default function EmailVerified(props: any) {
  const history = useHistory();

  useEffect(() => {
    verifyEmail();
  }, []);

  const verifyEmail = () => {
    debugger;
    UserService.verifyEmailAddress(props.match.params.id)
      .then((response) => {
        debugger;
      })
      .catch((e) => {
        console.log(e);
      });
  };

  return (
    <React.Fragment>
      <section className="authLayout commonPage">
        <div className="container">
          <div className="row">
            <div className="col-md-2"></div>
            <div className="col-md-8">
              <div className="auth-white-bg thankyou text-center thankyou-padding">
                <div
                  onClick={() => history.push("/employerHome")}
                  className="back-link pointer"
                  style={{ textAlign: "left" }}
                >
                  <i className="fa fa-arrow-left"></i>
                  Back to home page
                </div>
                <div className="topDots">
                  <Anime delay={anime.stagger(400)} scale={[0.1, 0.9]}>
                    <img src={TopDots} />
                  </Anime>
                </div>

                <div className="bottomDots">
                  <Anime delay={anime.stagger(400)} scale={[0.1, 0.9]}>
                    <img src={BottomDots} />
                  </Anime>
                </div>
                <img style={{ margin: "20px 0" }} src={thank} />
                <div className="blackHead">
                  Your email has succesfully verified
                </div>
                <div className="shortBorder"></div>
                <p className="blurText">
                  Thank you for applying with us, now you can book a short
                  meeting with us to understand more about us
                </p>

                <a
                  onClick={() => {
                    history.push("/calendar");
                  }}
                  className="pointer bold"
                >
                  Book a meeting
                </a>
              </div>
            </div>
            <div className="col-md-2"></div>
          </div>
        </div>
      </section>
    </React.Fragment>
  );
}
