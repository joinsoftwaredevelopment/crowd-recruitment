import React, { useState, useEffect } from "react";
import { createStyles, makeStyles, Theme } from "@material-ui/core/styles";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import Grid from "@material-ui/core/Grid";
import thank from "../../../src/assets/thankful.png";
import { useHistory } from "react-router-dom";
import TopDots from "../../../src/assets/dots-top.png";
import BottomDots from "../../../src/assets/dots-bottom.png";
import Anime from "react-anime";
import anime from "animejs/lib/anime.es.js";
import UserService from "../../services/UserService";
import { useDispatch } from "react-redux";
import { alertActions } from "../../actions";
import { IResponseData } from "../../models/IResponseData";

export default function ThankYou(props: any) {
  const history = useHistory();
  const dispatch = useDispatch();

  const [email, setEmail] = useState("");

  useEffect(() => {
    GetUserByUuId();
  }, []);

  const GetUserByUuId = () => {
    debugger;
    UserService.GetUserByUuId(props.match.params.id)
      .then((response) => {
        debugger;
        setEmail(response.data.data.email);
      })
      .catch((e) => {
        console.log(e);
      });
  };

  const ResendEmail = () => {
    debugger;
    UserService.resendEmail(props.match.params.id).then(
      (user) => {
        debugger;
        let responseDetails: IResponseData = user.data;
        dispatch(alertActions.success(responseDetails.message));
      },
      (error) => {
        debugger;
        let responseDetails: IResponseData = error.response.data;
        dispatch(alertActions.error(responseDetails.message));
      }
    );
  };

  return (
    <React.Fragment>
      <section className="authLayout commonPage">
        <div className="container">
          <div className="row">
            <div className="col-md-2"></div>
            <div className="col-md-8">
              <div className="auth-white-bg thankyou text-center thankyou-padding">
                <div
                  className="back-link"
                  onClick={() => history.push("/employerHome")}
                  style={{ textAlign: "left" }}
                >
                  <i className="fa fa-arrow-left"></i>
                  Back to home page
                </div>
                <div className="topDots">
                  <Anime delay={anime.stagger(400)} scale={[0.1, 0.9]}>
                    <img src={TopDots} />
                  </Anime>
                </div>

                <div className="bottomDots">
                  <Anime delay={anime.stagger(400)} scale={[0.1, 0.9]}>
                    <img src={BottomDots} />
                  </Anime>
                </div>
                <img style={{ margin: "20px 0" }} src={thank} />
                <div className="blackHead">Thank You for applying with us!</div>
                <div className="shortBorder"></div>
                <p className="blurText">
                  We sent a mail to your mail{" "}
                  <a className="pointer bold">{email}</a> please check and
                  verify
                </p>

                <div
                  onClick={() =>
                    history.push("/ChangeEmail/" + props.match.params.id)
                  }
                  className="pointer bold blue"
                >
                  Change Email
                </div>

                <div style={{ marginTop: "30px" }}>
                  Didn't receive a message,{" "}
                  <a className="pointer bold" onClick={() => ResendEmail()}>
                    Click to resend
                  </a>
                </div>
              </div>
            </div>
            <div className="col-md-2"></div>
          </div>
        </div>
      </section>
    </React.Fragment>
  );
}
