import React, { useState } from "react";
import { createStyles, makeStyles, Theme } from "@material-ui/core/styles";
import TextField from "@material-ui/core/TextField";
import Grid from "@material-ui/core/Grid";
import thank from "../../../src/assets/thankful.png";
import { useHistory } from "react-router-dom";

import TopDots from "../../../src/assets/dots-top.png";
import BottomDots from "../../../src/assets/dots-bottom.png";
import Anime from "react-anime";
import anime from "animejs/lib/anime.es.js";
import Button from "../../components/Buttons/Button";

import { useForm } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import * as Yup from "yup";
import UserService from "../../services/UserService";
import { IResponseData } from "../../models/IResponseData";
import { alertActions } from "../../actions";
import { RootStateOrAny, useDispatch, useSelector } from "react-redux";

export default function ChangeEmail(props: any) {
  const history = useHistory();
  const [email, setEmail] = useState("");
  const [loading, setLoading] = useState(false);
  const dispatch = useDispatch();
  const registering = useSelector(
    (state: RootStateOrAny) => state.registration.registering
  );

  const handleInputChange = (event: any) => {
    debugger;
    const { name, value } = event.target;
    setEmail(value);
  };

  // Validation
  // form validation rules
  const validationSchema = Yup.object().shape({
    EmailAddress: Yup.string().trim().required("Email is required").email(),
  });

  // functions to build form returned by useForm() hook
  const { register, handleSubmit, reset, errors } = useForm({
    resolver: yupResolver(validationSchema),
  });

  function onSubmit(data: any) {
    debugger;
    setLoading(true);
    debugger;
    debugger;
    UserService.changeEmail(props.match.params.id, email).then(
      (user) => {
        debugger;
        let responseDetails: IResponseData = user.data;
        dispatch(alertActions.success(responseDetails.message));
        history.push("/ThankYou/" + props.match.params.id);
      },
      (error) => {
        debugger;
        let responseDetails: IResponseData = error.response.data;
        dispatch(alertActions.error(responseDetails.message));
      }
    );
  }

  return (
    <React.Fragment>
      <section className="authLayout commonPage">
        <div className="container">
          <div className="row">
            <div className="col-md-2"></div>
            <div className="col-md-8">
              <div className="auth-white-bg thankyou text-center thankyou-padding">
                <div
                  className="back-link"
                  onClick={() =>
                    history.push("/ThankYou/" + props.match.params.id)
                  }
                  style={{ textAlign: "left" }}
                >
                  <i className="fa fa-arrow-left"></i>
                  Back to home page
                </div>
                <div className="topDots">
                  <Anime delay={anime.stagger(400)} scale={[0.1, 0.9]}>
                    <img src={TopDots} />
                  </Anime>
                </div>

                <div className="bottomDots">
                  <Anime delay={anime.stagger(400)} scale={[0.1, 0.9]}>
                    <img src={BottomDots} />
                  </Anime>
                </div>
                <img style={{ margin: "20px 0" }} src={thank} />
                <div className="blackHead">Please type your email</div>
                <div className="shortBorder"></div>

                <form onSubmit={handleSubmit(onSubmit)} onReset={reset}>
                  <div className="form-group customInput changeEmail">
                    <label>Email Address</label>
                    <i className="far fa-envelope"></i>
                    <input
                      name="EmailAddress"
                      id="EmailAddress"
                      defaultValue={email}
                      placeholder="Email Address"
                      type="text"
                      ref={register}
                      className={`form-control ${
                        errors.EmailAddress ? "is-invalid" : ""
                      }`}
                      onChange={handleInputChange}
                    />
                    <div className="invalid-feedback">
                      {errors.EmailAddress?.message}
                    </div>
                  </div>

                  <div className="text-center">
                    <Button
                      isLoading={registering && loading}
                      border="none"
                      fontSize="16px"
                      font="Proxima-Regular"
                      color="white"
                      bgColor="#0195FF"
                      height="35px"
                      onClick={() => setLoading(false)}
                      radius=""
                      width="150px"
                      children="Done"
                    />
                  </div>
                </form>
              </div>
            </div>
            <div className="col-md-2"></div>
          </div>
        </div>
      </section>
    </React.Fragment>
  );
}
