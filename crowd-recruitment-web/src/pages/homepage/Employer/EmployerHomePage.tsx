import React, { useEffect, useState } from "react";

import { createStyles, makeStyles, Theme } from "@material-ui/core/styles";

import TextField from "@material-ui/core/TextField";
import arrowIcon from "../../../assets/arrow-icon.png";
import qMark from "../../../assets/qMark.png";
import split1 from "../../../assets/5.png";
import split2 from "../../../assets/7.png";
import split3 from "../../../assets/6.png";
import split4 from "../../../assets/8.png";
import circles from "../../../assets/circles.png";
import card1 from "../../../assets/card-1.png";
import card2 from "../../../assets/card-2.png";
import card3 from "../../../assets/card-3.png";
import { AppHeader } from "../../../components/appHeader";
import AppFooter from "../../../components/appFooter";
import dumSlider from "../../../assets/dum-slider.png";
import sliderImg from "../../../assets/profile.png";
import arrowRight from "../../../assets/arrow-icon.png";
import arrowPrev from "../../../assets/arrow-prev.png";
import lowcost from "../../../assets/low-cost.png";
import Slider from "react-slick";
import "slick-carousel/slick/slick-theme.css";
import "slick-carousel/slick/slick.css";

import quality from "../../../assets/quality.png";
import fast from "../../../assets/fast.png";
import serv1 from "../../../assets/serv-1.png";
import serv2 from "../../../assets/serv-2.png";
import serv3 from "../../../assets/serv-3.png";
import serv4 from "../../../assets/serv-4.png";
import budget from "../../../assets/budget.png";
import sliderDummy from "../../../assets/slider-dummy.png";
import Button from "../../../components/Buttons/Button";

import Fade from "react-reveal/Fade";
import PackageService from "../../../services/PackageService";

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {},
  })
);

export default function EmployerHomePage() {
  const classes = useStyles();

  const initialPackageList = [
    {
      uuid: "",
      text: "",
      packageId: 0,
    },
  ];
  const [packageList, setPackageList] = React.useState(initialPackageList);
  const [allPricePackage, setAllPricePackage] = useState([]);

  useEffect(() => {
    GetAllPackages();
  }, []);

  const GetAllPackages = () => {
    debugger;
    PackageService.GetAllPackages()
      .then((response) => {
        debugger;
        setAllPricePackage(response.data.data.packages);
        setPackageList(response.data.data.packagesFeatures);
      })
      .catch((e) => {
        console.log(e);
      });
  };

  function SampleNextArrow(props: any) {
    const { className, style, onClick } = props;
    return (
      <div className={className} onClick={onClick}>
        <img className="sL-next" src={arrowRight} />
      </div>
    );
  }

  function SamplePrevArrow(props: any) {
    const { className, style, onClick } = props;
    return (
      <div className={className} onClick={onClick}>
        <img className="sL-prev" src={arrowPrev} />
      </div>
    );
  }

  var settings = {
    className: "",
    centerMode: true,
    infinite: true,
    // centerPadding: "88px",
    slidesToShow: 2,
    speed: 500,
    focusOnSelect: true,
    nextArrow: <SampleNextArrow />,
    prevArrow: <SamplePrevArrow />,
    responsive: [
      {
        breakpoint: 768,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
          infinite: true,
          dots: false,
        },
      },
    ],
  };

  return (
    <React.Fragment>
      <div className="layout-home newLayoutHome">
        <AppHeader />

        <section className="hero-section">
          <div className="container">
            <div className="row banner-text text-center">
              <div className="blackHead">
                Impossible hiring goals?
                <br />
                We have the solution.
              </div>

              <p className="">
                <img src={qMark} />
                Croud recruitment sourcing technology blends 4,000 Sourcers
                <br />
                and AI to find talent within hours
              </p>

              <Button
                isLoading={false}
                border="none"
                fontSize="16px"
                font="Proxima-Regular"
                color="#0195FF"
                bgColor="#fff"
                height="43px"
                onClick={void 0}
                radius=""
                width="216px"
                children="Discover"
              />
            </div>
          </div>
        </section>

        <section className="features">
          <div className="container">
            <div className="row">
              <div className="col-md-4">
                <div className="srcFeature">
                  <img src={fast} />
                  <div className="featureHead">20x Faster</div>
                  <p>
                    We simplifies candidate sourcing and outreach so you can
                    focus on what really matters - people
                  </p>
                  <span className="shortLine"></span>
                </div>
              </div>

              <div className="col-md-4">
                <div className="srcFeature">
                  <img src={quality} />
                  <div className="featureHead">50% Cheaper</div>
                  <p>
                    Our flexible sourcing solution is 50% cheaper than in-house
                    sourcing and can quickly scale up or down according to your
                    hiring needs
                  </p>
                  <span className="shortLine"></span>
                </div>
              </div>

              <div className="col-md-4">
                <div className="srcFeature">
                  <img src={lowcost} />
                  <div className="featureHead">10x Higher Quality</div>
                  <p>
                    The unique combination of AI and a network of top sourcers
                    ensure you will find the best talent. Unlike sourcing
                    robots, Visage understands your unique requirements and
                    delivers 10x higher profile acceptance rates
                  </p>
                  <span className="shortLine"></span>
                </div>
              </div>
            </div>
          </div>
        </section>

        <section className="serviceSection">
          <div className="container-fluid no-padding">
            <div className="">
              <div className="col-md-6 no-padding">
                <Fade left>
                  <div className="servBox">
                    <div className="splitHead">Easy Job Posting</div>
                    <div className="shortBorder"></div>
                    <p>
                      Define the role requirements , and submit. AI matches the
                      role to 5+ Top Sourcers with the most relevant expertise
                      for immediate sourcing. With a network of over 4,000
                      sourcers, Visage can support searches for any function,
                      domain, and industry.
                    </p>
                  </div>
                </Fade>
              </div>
              <div className="col-md-6 no-padding">
                <Fade right>
                  <img className="w-100" src={serv1} />
                </Fade>
              </div>
            </div>

            <div className="">
              <div className="col-md-6 no-padding">
                <Fade left>
                  <img className="w-100" src={serv2} />
                </Fade>
              </div>
              <div className="col-md-6 no-padding">
                <Fade right>
                  <div className="servBox">
                    <div className="splitHead">Smart Sourcing</div>
                    <div className="shortBorder"></div>
                    <p>
                      Our Top Sourcers compete to submit their best-matching
                      candidates within 24h. They are incentivized based on the
                      quality of submission. They will leave no stone unturned
                      to identify potential candidates.
                    </p>
                  </div>
                </Fade>
              </div>
            </div>

            <div className="">
              <div className="col-md-6 no-padding">
                <Fade right>
                  <div className="servBox">
                    <div className="splitHead">Qualify</div>
                    <div className="shortBorder"></div>
                    <p>
                      AI expedites the matching process to identify top
                      candidates. Algorithms disqualify candidates based on
                      structured data (job history, title, skills, etc.). We
                      take into account your feedback (candidates
                      accepted/rejected) using machine learning
                    </p>
                  </div>
                </Fade>
              </div>
              <div className="col-md-6 no-padding">
                <Fade left>
                  <img className="w-100" src={serv3} />
                </Fade>
              </div>
            </div>

            <div className="">
              <div className="col-md-6 no-padding">
                <Fade left>
                  <img className="w-100" src={serv4} />
                </Fade>
              </div>
              <div className="col-md-6 no-padding">
                <Fade right>
                  <div className="servBox">
                    <div className="splitHead">Engage</div>
                    <div className="shortBorder"></div>
                    <p>
                      Review candidate profiles and provide feedback to improve
                      your next batch of candidates. In one-click, engage your
                      shortlist with custom messages via email and SMS. See
                      candidate responses directly in your inbox
                    </p>
                  </div>
                </Fade>
              </div>
            </div>
          </div>
        </section>

        <section className="newPackages">
          <div className="splitHead text-center">
            {" "}
            Packaging Plans
            <div className="shortBorder"></div>
          </div>

          <div className="container-fluid no-padding">
            <div>
              {allPricePackage.map((item) => (
                <div className="col-md-3 no-padding">
                  <div className="packageBox">
                    <div className="">
                      <img src={budget} />
                      <div>{item.packageTitle}</div>
                      <div className="package-price">${item.packagePrice}</div>
                      <div>per month, paid yearly</div>
                    </div>
                    <div className="packageBox-body">
                      <ul>
                        {packageList
                          .filter((temp) => temp.packageId == item.packageId)
                          .map((k) => {
                            return <li>{k.text}</li>;
                          })}
                      </ul>
                    </div>
                  </div>
                </div>
              ))}
            </div>
          </div>
        </section>

        <section className="slider-sec slider-sec-new">
          <div className="container">
            <div className="row">
              <Slider {...settings}>
                <div className="col-md-6">
                  <div className="sliderBox">
                    <div className="sliderBoxChild">
                      <img className="float-left" src={sliderDummy} />

                      <div>
                        <div className="font-16 bold">John Smith</div>
                        <div className="font-14">
                          Marketing Coordinator (Entertainment)
                        </div>
                        <div className="font-12 light">
                          Dubai - United Arab Emaraties
                        </div>
                      </div>
                    </div>

                    <div className="font-14">
                      Lorem Ipsum is simply dummy text of the printing and
                      typesetting industry. Lorem Ipsum has been the industry's
                      standard dummy text ever since the
                    </div>
                  </div>
                </div>
                <div className="col-md-6">
                  <div className="sliderBox">
                    <div className="sliderBoxChild">
                      <img className="float-left" src={sliderDummy} />

                      <div>
                        <div className="font-16 bold">John Smith</div>
                        <div className="font-14">
                          Marketing Coordinator (Entertainment)
                        </div>
                        <div className="font-12 light">
                          Dubai - United Arab Emaraties
                        </div>
                      </div>
                    </div>

                    <div className="font-14">
                      Lorem Ipsum is simply dummy text of the printing and
                      typesetting industry. Lorem Ipsum has been the industry's
                      standard dummy text ever since the
                    </div>
                  </div>
                </div>
                <div className="col-md-6">
                  <div className="sliderBox">
                    <div className="sliderBoxChild">
                      <img className="float-left" src={sliderDummy} />

                      <div>
                        <div className="font-16 bold">John Smith</div>
                        <div className="font-14">
                          Marketing Coordinator (Entertainment)
                        </div>
                        <div className="font-12 light">
                          Dubai - United Arab Emaraties
                        </div>
                      </div>
                    </div>

                    <div className="font-14">
                      Lorem Ipsum is simply dummy text of the printing and
                      typesetting industry. Lorem Ipsum has been the industry's
                      standard dummy text ever since the
                    </div>
                  </div>
                </div>
                <div className="col-md-6">
                  <div className="sliderBox">
                    <div className="sliderBoxChild">
                      <img className="float-left" src={sliderDummy} />

                      <div>
                        <div className="font-16 bold">John Smith</div>
                        <div className="font-14">
                          Marketing Coordinator (Entertainment)
                        </div>
                        <div className="font-12 light">
                          Dubai - United Arab Emaraties
                        </div>
                      </div>
                    </div>

                    <div className="font-14">
                      Lorem Ipsum is simply dummy text of the printing and
                      typesetting industry. Lorem Ipsum has been the industry's
                      standard dummy text ever since the
                    </div>
                  </div>
                </div>
              </Slider>
            </div>
          </div>
        </section>
        <section className="blueSec newBluSec">
          <div className="container">
            <div className="row">
              <div className="col-md-4">
                <div className="miniBlue">
                  <div className="font-16 white bold">Contact Us</div>

                  <div className="blueSocial">
                    <i className="fal fa-location"></i>
                    <span>Location, street, Saudi Arbia</span>
                  </div>

                  <div className="blueSocial">
                    <i className="fal fa-phone"></i>
                    <span>+966 1475147546</span>
                  </div>

                  <div className="blueSocial">
                    <i className="fal fa-phone"></i>
                    <span>+966 1475147546</span>
                  </div>

                  <div className="blueSocial">
                    <i className="fal fa-envelope"></i>
                    <span>admin@mail.info</span>
                  </div>
                </div>
              </div>
              <div className="col-md-8">
                <div className="bluesecHead">
                  Ready to save time and resources?
                </div>

                <div className="inputBlue">
                  <img className="reqImg" src={arrowIcon} />
                  <input
                    placeholder="Enter Your Email"
                    className="form-control"
                  />
                </div>
              </div>
            </div>
          </div>

          <div></div>
        </section>
      </div>
      <AppFooter />
    </React.Fragment>
  );
}
