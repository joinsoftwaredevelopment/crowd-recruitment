import axios from "axios";

export default  axios.create({
  baseURL: "http://localhost:44399/api",
  //baseURL: "https://api-crowd-recruitment.joinsolutions.app/api",
  headers: {
    "Content-type": "application/json"
  }
});