export function authHeader() {
    // return authorization header with jwt token
    let user = JSON.parse(localStorage.getItem('user') || '{}');

    if (user && user.token) {
        //debugger;
        return { 'Authorization': 'Bearer ' + user.token   };
    } else {
        return {};
    }
}

export function authHeaderWithFormData() {
    // return authorization header with jwt token
    let user = JSON.parse(localStorage.getItem('user') || '{}');

    if (user && user.token) {
        //debugger;
        return { 'Authorization': 'Bearer ' + user.token  ,'content-type': 'multipart/form-data' };
    } else {
        return {};
    }
}

