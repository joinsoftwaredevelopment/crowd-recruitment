export interface IInterviewsModel {
     id : number;
     candidateName: string;
     candidateEmail: string;
     interviewDate: Date;
     duration: number;
     companyName: string;
     interviewerName: string
     emailSubject: string;
     emailDescription: string;
     jobId: number;
     jobTitle: string;
     isCanceled: boolean;
     isEmailSent: boolean; 
     interviewTime: Date;
     submissionUuid: string;
     interviewComment: string;
     interviewerId: number;
     candidateId: number;
     interviewTypeId: number;
     templateId: number;
     createdBy: number;
     createdDate: Date;
     interviewerTypeId: number;
  }