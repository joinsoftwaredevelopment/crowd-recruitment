export interface IJobSubmission {
    jobId: number;
    uUID : string,
    jobTitle: string;
    isActive: boolean;
    languageId: number;
    jobLocationTranslation : string,
    sourcerId ?: number,
    cvName : string;
    candidateName: string;
    candidateSubmissionStatusId: number;
    rejectionReason : string;
    candidateStatusName : string;
  }