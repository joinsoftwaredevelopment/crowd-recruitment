export interface IJobsInfo {
    jobLocationTranslation : string;
    jobTitle : string;
    uuid : string,
    jobDate: string;
    jobStatusId : number;
    reason : string;
    jobStatusName:string;
    isActive:boolean;
  }