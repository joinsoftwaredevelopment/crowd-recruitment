export interface IDeclineJob {
    uuid: string, 
    rejectionReason : string
  }