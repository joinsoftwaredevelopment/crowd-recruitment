export interface IUpdateIsAcceptOfferModel {
  submissionUuid: string, 
  isOfferAccepted : boolean
  }