export interface IRsetPassword {
    Password: string;
    ConfirmPassword: string;
    Uuid: string;
  }