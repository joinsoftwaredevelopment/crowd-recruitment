export interface IUserRequest {
    uuid: string;
    rejectedReason: string;
  }