export interface IAdminSourcersBalances {
    fullName: string;
    jobCount : number;
    submissionCount : number;
    totalBalance : number;
    acceptanceRatio : number;
    acceptanceCount:number;
    declinedCount:number

  }