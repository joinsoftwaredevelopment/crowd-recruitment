        export interface IEmployerObjectives {
            UUID: string;
            isContinuousHire: boolean;
            peopleToHireCount : number;
            whenYouNeedToHire: number;
            sourcingOutsideOfJoin: boolean;
            findingJobDifficultyLevelId : string;
          }