export interface IJobRequestsDetails {
  uuid: string;
    jobTitle: string;
    description : string;
    employmentTypeName : string,
    seniorityLevelName: string,
    companiesNotToSourceFrom: string;
    languageId: number;
    jobLocation : string,
    companyName : string,
    mustHaveQualification : string;
    niceToHaveQualification : string;
    timetoHireId : number;
    timetoHireName : string;
    experienceLevelName : string;
    companyIndustry : string;
    jobStatusId : number;
    jobStatusName : string;
    requirements : string;
    noOfSubmissions : number;
    jobDate : Date;
    peopleToHireCount: number;
    findingJobDifficultyLevelId: number;
    findingJobDifficultyName: string;
    minSalary: number;
    maxSalary: number;
    currencyName: string;
    firstMailSubject : string;
    firstMailDescription : string;
  }