export interface IJobSummary {
    rowNumber: number;
    jobId: number;
    uuid : string,
    jobTitle: string;
    isActive: boolean;
    submissionCount: number;
    languageId: number;
    jobLocation : string,
    sourcerId ?: number,
    commission : number;
    jobDate : string;
    employerFname : string;
    employerLname : string;
    remainingCount : number;
    noOfSubmissions : number;
    employerName : string;
    jobStatusId : number;
    jobStatusName : string;
    rejectionReason : string;
    seniorityLevelName : string;
    countryName: string;
    submissonCount: number;
}