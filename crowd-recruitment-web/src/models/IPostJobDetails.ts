export interface IPostJobDetails {
  uuId: string;
  jobTitle: string;
  seniorityLevelId: number;
  employerTypeId: number;
  jobDescription: string; 
  industryId: number;
  jobLocationId: number;
  jobNationalityId: number;
  requirements : string;
  jobTemplateId:number;
  }