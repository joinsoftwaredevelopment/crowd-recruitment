export interface ISourcerDetails {
    uuid: string;
    createdDate: string;
    fullName: string;
    phoneNumber: string;
    email: string;
    experience: string;
    jobTitle: string;
    haveAccessToDifferentWebsite: boolean;
    industryYouInterested: string;
    cvPath: string;
    linkedInProfile: string;
    rejectedReason:string;
  }