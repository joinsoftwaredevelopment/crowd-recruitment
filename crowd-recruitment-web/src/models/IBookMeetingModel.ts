export interface IBookMeetingModel {
         uuid :string
         timePeriod :string;
         timeZone :string;
         mettingDate :string;
         interviewerName :string;
         zoomLink :string;
         email :string;
         userName :string;
  }