import { BooleanSchema } from 'yup';
import { date } from 'yup/lib/locale';
export interface IJobCandidate {
       rowNumber : number,
       jobId: number,
       jobUuId:string,
       jobTitle:string,
       jobDate: string,
       sourcerId:number,
       sourcerName:string,
       submissionUuId:string,
       candidateName:string,
       cvName:string,
       linkedInProfile:string,
       countryName:string,
       submissionDate:string,
       languageId:number,
       candidateSubmissionStatusId : number;
       firstMailSubject : string;
       firstMailDescription : string,
       email:string;
       rejectionReason : string;
       candidateUuId : string;
       submissionStatusName : string;
       isCvApproved : boolean;
       isAcceptOffer : boolean;
       isHired : boolean;
       hasInterview: number;
       jobStatusId: number;
  }