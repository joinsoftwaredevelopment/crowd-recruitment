export interface IPostJobInstructions {
  uuid: string; 
  experienceLevelId : number;
  mustHaveSkills : any,
  niceHaveQualification : any,
  notToSource : any,
  currencyId: number,
  minSalary: number,
  maxSalary: number
  }