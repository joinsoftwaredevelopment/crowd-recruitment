export interface IJobDetails {
  uuid: string;
    jobTitle: string;
    description : string;
    employmentTypeName : string,
    seniorityLevelName: string,
    companiesNotToSourceFrom: string;
    languageId: number;
    jobLocation : string,
    companyName : string,
    mustHaveQualification : string;
    niceToHaveQualification : string;
    hiringNeeds : string;
    experienceLevelName : string;
    companyIndustry : string;
    jobStatusId : number;
    jobStatusName : string;
    requirements : string;
    noOfSubmissions : number;
    jobDate : Date
  }