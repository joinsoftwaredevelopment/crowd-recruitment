export interface IInterviewDetailsModel {
    interviewerId : number;
    interviewComment: string;
    interviewTypeId: number;
    interviewDate: Date;
    interviewTime: string;
    duration: number;
    templateId: number;
    emailSubject: string;
    emailDescription: string;
    interviewerTypeId : number;
    id : number;
    submissionUUid : string;
  }