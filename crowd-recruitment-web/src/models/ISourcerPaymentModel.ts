export interface ISourcerPaymentModel {
    startDate :Date;
    endDate :Date;
    sourcerId :number;
}