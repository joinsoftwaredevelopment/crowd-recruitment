export interface IUpdatePassword {
  uuid: string;
  password: string;
  newPassword: string;
  confirmedPassword: string;
  }