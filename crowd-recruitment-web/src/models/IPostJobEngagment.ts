export interface IPostJobEngagment {
  uuid: string; 
  firstMailSubject : string;
  firstMailDescription : string;
  statusId : number;
  engagementTemplateId : number;
  }