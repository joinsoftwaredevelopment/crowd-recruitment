export interface ISourcersBalances {
    employerName :string;
    candidateName :string;
    submissionDate :string;
    jobTitle :string;
    candidateCommission :string;
    sourcerUuid : string;
    candidateUuId : string;
}