export interface ISourcerBasicInfo {
    uuid: string;
    firstName: string;
    lastName: string;
    phoneNumber: string;
    email: string;
    jobTitle: string;
    cvName: string;
    linkedInProfile:string;
  }