export interface IEmployerDetails {
    uuid: string;
    createdDate: string;
    companyName: string;
    fullName: string;
    phoneNumber: string;
    email: string;
    rejectedReason:string;
    openedJobsCount : number;
  }