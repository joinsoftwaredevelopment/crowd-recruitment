import { NullLiteral } from "typescript";

export interface ISourcerCVModel {
    Uuid :string;
    candidateName : string ;
    JobUuId:string;
    CvName : string;
    LinkedInProfile : string ;
    Email : string ;
    Location : string ;
    UploadedFile : File | Blob;
    countryId : number;
}