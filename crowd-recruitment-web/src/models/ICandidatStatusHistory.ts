export interface ICandidatStatusHistory {
    candidateSubmissionStatusId: number, 
    candidateUuId : string,
    statusName : string;
    languageId : number;
    createDate : Date;
  }