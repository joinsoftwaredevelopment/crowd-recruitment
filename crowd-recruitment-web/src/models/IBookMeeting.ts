export interface IBookMeeting {
    uuid: string, 
    timePeriodId : string,
    timeZoneId : number,
    timeMettingId : string,
    interviewerId : number,
    day : Date
  }