export interface IJobSourcers {
    jobUuId: string;
    jobTitle : string,
    sourcerName: string;
    sourcerJobTitle: string;
    sourcerUuId: string;
    submissionCount : number;
  }