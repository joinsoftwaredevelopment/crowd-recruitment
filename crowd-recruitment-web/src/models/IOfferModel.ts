export interface IOfferModel {
  submissionUuid: string;
  offerSubject: string;
  offerDescription: string;
  offerFile: File;
  }