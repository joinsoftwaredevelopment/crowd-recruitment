export interface ISourcerCareerInfo {
    uuid: string, 
    experienceTypeId : number,
    hasAccessToDifferentWebsite: boolean;
    industryId: number;
  }