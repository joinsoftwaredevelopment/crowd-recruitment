export interface ISourcersBalancesSummary {
    sourcerUuid :string;
    approvedCount :number;
    declinedCount :number;
    totalBalance : number;
    rate : number;
}