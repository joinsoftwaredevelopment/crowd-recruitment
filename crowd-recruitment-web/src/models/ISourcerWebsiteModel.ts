export interface ISourcerAccessWebSitesModel {
    Uuid :string;
    HasAccessToDifferentWebsite ?: boolean
}