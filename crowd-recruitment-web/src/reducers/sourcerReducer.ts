import { sourcerConstants } from '../constants/indexConstant';

export function sourcers(state = {}, action: any) {
    switch (action.type) {
        case sourcerConstants.CREATE_SOURCER_REQUEST:
            return {
                loading: true
            };
        case sourcerConstants.CREATE_SOURCER_SUCCESS:
            debugger;
            return {
                user : {uuid :action.user.data.uuid , 
                        resgistrationStep : action.user.data.registrationStep , 
                        token : action.user.data.token} 
            };
        case sourcerConstants.CREATE_SOURCER_FAILURE:
            return {
                error: action.error
            };
        case sourcerConstants.SOURCER_EXPERIENCE_REQUEST:
            return {
                loading: true
            };
        case sourcerConstants.SOURCER_EXPERIENCE_SUCCESS:
            debugger;
            return {
                user : {uuid :action.user.data.uuid , 
                        resgistrationStep : action.user.data.registrationStep , 
                        token : action.user.data.token} 
            };
        case sourcerConstants.SOURCER_EXPERIENCE_FAILURE:
            return {
                error: action.error
            };
        case sourcerConstants.SOURCER_JOBTITLE_REQUEST:
            return {
                loading: true
            };
        case sourcerConstants.SOURCER_JOBTITLE_SUCCESS:
            return {
                user : {uuid :action.user.data.uuid , 
                        resgistrationStep : action.user.data.registrationStep , 
                        token : action.user.data.token} 
            };
        case sourcerConstants.SOURCER_JOBTITLE_FAILURE:
            return {
                error: action.error
            };
        case sourcerConstants.SOURCER_ACCESS_WEBSITES_REQUEST:
            return {
                loading: true
            };
        case sourcerConstants.SOURCER_ACCESS_WEBSITES_SUCCESS:
            return {
                user : {uuid :action.user.data.uuid , 
                        resgistrationStep : action.user.data.registrationStep , 
                        token : action.user.data.token} 
            };
        case sourcerConstants.SOURCER_ACCESS_WEBSITES_FAILURE:
            return {
                error: action.error
            };
        case sourcerConstants.SOURCER_Industry_REQUEST:
            return {
                loading: true
            };
        case sourcerConstants.SOURCER_Industry_SUCCESS:
            return {
                user : {uuid :action.user.data.uuid , 
                        resgistrationStep : action.user.data.registrationStep , 
                        token : action.user.data.token} 
            };
        case sourcerConstants.SOURCER_Industry_FAILURE:
            return {
                error: action.error
            };
        case sourcerConstants.SOURCER_UPLOAD_CV_REQUEST:
            return {
                loading: true
            };
        case sourcerConstants.SOURCER_UPLOAD_CV_SUCCESS:
            return {
                user : {uuid :action.user.data.uuid , 
                        resgistrationStep : action.user.data.registrationStep , 
                        token : action.user.data.token} 
            };
        case sourcerConstants.SOURCER_UPLOAD_CV_FAILURE:
            return {
                error: action.error
            };
        case sourcerConstants.ALL_JOB_SUMMAIES_REQUEST:
            return {
                loading: true
            };
        case sourcerConstants.ALL_JOB_SUMMAIES_SUCCESS:
            return {
                 user : {uuid :action.user.data.uuid , 
                      resgistrationStep : action.user.data.registrationStep , 
                    token : action.user.data.token} 
            };
        case sourcerConstants.ALL_JOB_SUMMAIES_FAILURE:
            return {
                error: action.error
            };
        case sourcerConstants.SOURCER_JOB_SUMMAREY_REQUEST:
            return {
                loading: true
            };
        case sourcerConstants.SOURCER_JOB_SUMMAREY_SUCCESS:
            return {
                user : {uuid :action.user.data.uuid , 
                          resgistrationStep : action.user.data.registrationStep , 
                        token : action.user.data.token} 
            };
        case sourcerConstants.SOURCER_JOB_SUMMAREY_FAILURE:
            return {
                error: action.error
            };
        default:
            return state
    }
}