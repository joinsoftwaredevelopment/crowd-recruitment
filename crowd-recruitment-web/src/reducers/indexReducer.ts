import { combineReducers } from 'redux';

import { authentication } from './authenticationReducers';
import { registration } from './registrationReducer';
//import { users } from './users.reducer';
import { alert } from './alertReducer';
import { sourcers } from './sourcerReducer';
import { jobs } from './jobReducer';

const rootReducer = combineReducers({
    authentication,
    registration,
    //users,
    alert,
    sourcers,
    jobs
});

export default rootReducer;