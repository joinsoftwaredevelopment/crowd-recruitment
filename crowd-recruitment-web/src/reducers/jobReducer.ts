import { retrieveJobConstants } from "../constants/retrieveJobConstants";

export function jobs(state = {}, action : any) {
    switch (action.type) {
        case retrieveJobConstants.RETRIEVE_JOB_REQUEST:
            return { action};
        case retrieveJobConstants.RETRIEVE_JOB_SUCCESS:
            return { action};
        case retrieveJobConstants.RETRIEVE_JOB_FAILURE:
            return { action};
        default:
            return state
    }
}