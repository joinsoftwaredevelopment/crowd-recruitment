import { userConstants } from '../constants/userConstants';

let user = JSON.parse(localStorage.getItem('user') || '{}');

const initialState = user
  ? { isLoggedIn: true, user }
  : { isLoggedIn: false, user: null };

export function authentication(state = initialState, action : any) {
    switch (action.type) {

    // Register
    case userConstants.REGISTER_SUCCESS:
      return {
        ...state,
        isLoggedIn: false,
      };
    case userConstants.REGISTER_FAILURE:
      return {
        ...state,
        isLoggedIn: false,
      };

      // Login
      case userConstants.LOGIN_SUCCESS:
        return {
          ...state,
          isLoggedIn: true,
          user: action.user,
        };

      case userConstants.LOGIN_FAILURE:
        return {
          ...state,
          isLoggedIn: false,
          user: null,
        };

        // Log out
      case userConstants.LOGOUT:
        return {
          ...state,
          isLoggedIn: false,
          user: null,
        };
      default:
        return state;
    }
}