import { userConstants } from '../constants/userConstants';

export function registration(state = {}, action : any) {
    switch (action.type) {
        case userConstants.REGISTER_REQUEST:
            return { registering: true };
        case userConstants.REGISTER_SUCCESS:
            return {};
        case userConstants.REGISTER_FAILURE:
            return {};
        case userConstants.LOGIN_REQUEST:
            return { registering: true };
        case userConstants.LOGIN_SUCCESS:
            return {};
        case userConstants.LOGIN_FAILURE:
            return {};  
        default:
            return state
    }
}