export interface IEmployer {
  uuid: string;
    companyName: string;
    firstName: string;
    lastName: string;
    phoneNumber: string;
    email: string;
    password: string;
    packageId: number;
    newPassword: string;
    confirmedPassword: string;
  }