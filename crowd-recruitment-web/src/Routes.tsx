import React, { Component,useState} from "react";
import { BrowserRouter,HashRouter, Route, Redirect, Switch } from "react-router-dom";
import RegistrationRoute from './routes/RegistrationRoutes'
import NaveBar from '../src/features/NaveBar'
import EmptyRoute from "./routes/EmptyRoutes";

function AllRoutes() {

        return ( 
            <div>
                <BrowserRouter forceRefresh={true}>
                <Switch>                          
                <RegistrationRoute />
                <EmptyRoute />
                </Switch>
                </BrowserRouter>
            </div>
         );
    }

export default AllRoutes;