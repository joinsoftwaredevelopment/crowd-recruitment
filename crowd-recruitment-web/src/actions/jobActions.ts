import { alertActions } from ".";
import { jobConstants } from "../constants/jobConstants";
import { IJobSummary } from "../models/IJobSummary";
import { IResponseData } from "../models/IResponseData";
import JobService from "../services/JobService";


const AllJobSummaries = () => {
    
    return (dispatch : any) => {
        dispatch(request(null));

        JobService.allJobsSummaries()
            .then(
                user => { 
                    let responseDetails : IResponseData  =  user.data
                    dispatch(success(responseDetails));
                    dispatch(alertActions.success(responseDetails.message));
                },
                error => {
                    let responseDetails : IResponseData  =  error.response.data
                    dispatch(failure(responseDetails));
                    dispatch(alertActions.error(responseDetails.message));
                }
            );
    };

    function request(user:any) { return { type: jobConstants.ALL_JOB_SUMMAIES_REQUEST, user } }
    function success(user: any) { return { type: jobConstants.ALL_JOB_SUMMAIES_SUCCESS, user } }
    function failure(error:any) { return { type: jobConstants.ALL_JOB_SUMMAIES_FAILURE, error } }
}

const SourcerJobsSummaries = (isActive : boolean) => {
    
    return (dispatch : any) => {
        dispatch(request(isActive));

        JobService.sourcerJobsSummaries(isActive)
            .then(
                user => { 
                    let responseDetails : IResponseData  =  user.data
                    dispatch(success(responseDetails));
                    dispatch(alertActions.success(responseDetails.message));
                },
                error => {
                    let responseDetails : IResponseData  =  error.response.data
                    dispatch(failure(responseDetails));
                    dispatch(alertActions.error(responseDetails.message));
                }
            );
    };

    function request(user:any) { return { type: jobConstants.SOURCER_JOB_SUMMAREY_REQUEST, user } }
    function success(user: any) { return { type: jobConstants.SOURCER_JOB_SUMMAREY_SUCCESS, user } }
    function failure(error:any) { return { type: jobConstants.SOURCER_JOB_SUMMAREY_FAILURE, error } }
}

const retrieveAllJobs = () => {
    return JobService.GetAllJobs();
  };

export const jobActions = {
    AllJobSummaries,
    SourcerJobsSummaries,
    retrieveAllJobs
};
