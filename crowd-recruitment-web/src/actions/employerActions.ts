import { userConstants } from '../constants/userConstants';
import  employerService  from '../services/EmployerService';
import { alertActions } from './alertActions';
import { history } from '../helpers/history';
import { IEmployer } from '../models/IEmployer';
import { Dispatch } from 'redux';
import { IResponseData } from '../models/IResponseData';
import { sourcerConstants } from '../constants/sourcerConstant';

export const employerActions = {
    register
};

function register(employer : IEmployer) {

    return (dispatch : any) => {
        dispatch(request(employer));
        debugger;
        employerService.create(employer)
            .then(
                user => { 
                    debugger;
                    let responseDetails  =  user.data
                    dispatch(success(responseDetails));
                    debugger;
                    
                    history.push('/thankyou/'+responseDetails.data.uuId);
                    dispatch(alertActions.success(responseDetails.message));
                
                },
                error => {
                    debugger;
                    let responseDetails : IResponseData  =  error.response.data
                    dispatch(failure(responseDetails));
                    dispatch(alertActions.error(responseDetails.message));
                }
            );
    };

    function request(user:any) { return { type: userConstants.REGISTER_REQUEST, user } }
    function success(user: any) { return { type: userConstants.REGISTER_SUCCESS, user } }
    function failure(error:any) { return { type: userConstants.REGISTER_FAILURE, error } }
}
