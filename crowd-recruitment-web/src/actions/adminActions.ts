import { userConstants } from '../constants/userConstants';
import { alertActions } from './alertActions';
import { history } from '../helpers/history';
import { IUser } from '../models/IUser';
import { Dispatch } from 'redux';
import { IResponseData } from '../models/IResponseData';
import AdminService from '../services/AdminService';

export const adminActions = {
    login,
    logout
};

function login(currentUser:IUser) {
    return (dispatch:any) => {
        dispatch(request(currentUser));

        AdminService.logIn(currentUser)
            .then(
                user => { 

                    let responseDetails : IResponseData  =  user.data ;

                    dispatch(success(responseDetails));

                    // store user details and jwt token in local storage to keep user logged in between page refreshes
                    localStorage.setItem('user', JSON.stringify(responseDetails.data));

                    history.push("/admin/employer-request");
                    
                },
                error => {
                    let responseDetails : IResponseData  =  error.response.data
                    dispatch(failure(responseDetails));
                    dispatch(alertActions.error(responseDetails.message));
                }
            );
    };

    function request(user:any) { return { type: userConstants.LOGIN_REQUEST, user } }
    function success(user:any) { return { type: userConstants.LOGIN_SUCCESS, user } }
    function failure(error:any) { return { type: userConstants.LOGIN_FAILURE, error } }
}

function logout() {
    AdminService.logout();
    return { type: userConstants.LOGOUT };
}
