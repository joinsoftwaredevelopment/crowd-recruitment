import { IJobSummary } from '../models/IJobSummary';
import { sourcerConstants } from './../constants/sourcerConstant';
import { ISourcerCVModel } from '../models/ISourcerCVModel';
import { ISourcerIndustryModel } from '../models/ISourcerIndustryModel';
import { ISourcerAccessWebSitesModel } from '../models/ISourcerWebsiteModel';
import { ISourcerJobModel } from '../models/ISourcerJobModel';
import { IResponseData } from '../models/IResponseData';
import  sourcerService  from '../services/SourcerService';
import { alertActions } from './alertActions';
import { history } from '../helpers/history';
import { ISourcerModel } from '../models/ISourcerModel';
import { ISourcerExperienceModel } from '../models/ISourcerExperienceModel';
import { userConstants } from '../constants/userConstants';


const Register = (sourcer : ISourcerModel) => {
    
    return (dispatch : any) => {
        dispatch(request(sourcer));

        sourcerService.register(sourcer)
            .then(
                user => { 
                    let responseDetails : IResponseData  =  user.data
                    dispatch(success(responseDetails));
                    history.push('/registration/sourcer-experience');
                    dispatch(alertActions.success(responseDetails.message));
                    
                    // store user details and jwt token in local storage to keep user logged in between page refreshes
                    localStorage.setItem('user', JSON.stringify(responseDetails.data));

                },
                error => {
                    let responseDetails : IResponseData  =  error.response.data
                    dispatch(failure(responseDetails));
                    dispatch(alertActions.error(responseDetails.message));
                }
            );
    };

    function request(user:any) { return { type: userConstants.REGISTER_REQUEST, user } }
    function success(user: any) { return { type: userConstants.REGISTER_SUCCESS, user } }
    function failure(error:any) { return { type: userConstants.REGISTER_FAILURE, error } }
}

const SourcerExperience = (sourcer : ISourcerExperienceModel) => {
    
    return (dispatch : any) => {
        dispatch(request(sourcer));

        sourcerService.sourcerExperience(sourcer)
            .then(
                user => { 
                    let responseDetails : IResponseData  =  user.data
                    dispatch(success(responseDetails));
                    history.push('/registration/sourcer-jobtitle');
                    dispatch(alertActions.success(responseDetails.message));
                },
                error => {
                    let responseDetails : IResponseData  =  error.response.data
                    dispatch(failure(responseDetails));
                    dispatch(alertActions.error(responseDetails.message));
                }
            );
    };

    function request(user:any) { return { type: sourcerConstants.SOURCER_EXPERIENCE_REQUEST, user } }
    function success(user: any) { return { type: sourcerConstants.SOURCER_EXPERIENCE_SUCCESS, user } }
    function failure(error:any) { return { type: sourcerConstants.SOURCER_EXPERIENCE_FAILURE, error } }
}

const SourcerJobTitle = (sourcer : ISourcerJobModel) => {
    
    return (dispatch : any) => {
        dispatch(request(sourcer));

        sourcerService.sourcerJobTitle(sourcer)
            .then(
                user => { 
                    let responseDetails : IResponseData  =  user.data
                    dispatch(success(responseDetails));
                    history.push('/registration/sourcer-access');
                    dispatch(alertActions.success(responseDetails.message));
                },
                error => {
                    let responseDetails : IResponseData  =  error.response.data
                    dispatch(failure(responseDetails));
                    dispatch(alertActions.error(responseDetails.message));
                }
            );
    };

    function request(user:any) { return { type: sourcerConstants.SOURCER_JOBTITLE_REQUEST, user } }
    function success(user: any) { return { type: sourcerConstants.SOURCER_JOBTITLE_SUCCESS, user } }
    function failure(error:any) { return { type: sourcerConstants.SOURCER_JOBTITLE_FAILURE, error } }
}

const SourcerAccessWebSites = (sourcer : ISourcerAccessWebSitesModel) => {
    
    return (dispatch : any) => {
        dispatch(request(sourcer));

        sourcerService.sourcerWebsite(sourcer)
            .then(
                user => { 
                    let responseDetails : IResponseData  =  user.data
                    dispatch(success(responseDetails));
                    history.push('/registration/sourcer-industry');
                    dispatch(alertActions.success(responseDetails.message));
                },
                error => {
                    let responseDetails : IResponseData  =  error.response.data
                    dispatch(failure(responseDetails));
                    dispatch(alertActions.error(responseDetails.message));
                }
            );
    };

    function request(user:any) { return { type: sourcerConstants.SOURCER_ACCESS_WEBSITES_REQUEST, user } }
    function success(user: any) { return { type: sourcerConstants.SOURCER_ACCESS_WEBSITES_SUCCESS, user } }
    function failure(error:any) { return { type: sourcerConstants.SOURCER_ACCESS_WEBSITES_FAILURE, error } }
}

const SourcerIndustry = (sourcer : ISourcerIndustryModel) => {
    
    return (dispatch : any) => {
        dispatch(request(sourcer));

        sourcerService.sourcerIndustry(sourcer)
            .then(
                user => { 
                    let responseDetails : IResponseData  =  user.data
                    dispatch(success(responseDetails));
                    history.push('/registration/sourcer-upload-resume');
                    dispatch(alertActions.success(responseDetails.message));
                },
                error => {
                    let responseDetails : IResponseData  =  error.response.data
                    dispatch(failure(responseDetails));
                    dispatch(alertActions.error(responseDetails.message));
                }
            );
    };

    function request(user:any) { return { type: sourcerConstants.SOURCER_Industry_REQUEST, user } }
    function success(user: any) { return { type: sourcerConstants.SOURCER_Industry_SUCCESS, user } }
    function failure(error:any) { return { type: sourcerConstants.SOURCER_Industry_FAILURE, error } }
}

const SourcerUploadCV = (sourcer : FormData | null) => {
    
    return (dispatch : any) => {
        dispatch(request(sourcer));

        sourcerService.sourcerCV(sourcer)
            .then(
                user => { 
                    let responseDetails  =  user.data
                    dispatch(success(responseDetails));
                    debugger;
                    history.push('/thankyou/'+responseDetails.data.uuId);
                    dispatch(alertActions.success(responseDetails.message));
                },
                error => {
                    let responseDetails : IResponseData  =  error.response.data
                    dispatch(failure(responseDetails));
                    dispatch(alertActions.error(responseDetails.message));
                }
            );
    };

    function request(user:any) { return { type: sourcerConstants.SOURCER_UPLOAD_CV_REQUEST, user } }
    function success(user: any) { return { type: sourcerConstants.SOURCER_UPLOAD_CV_SUCCESS, user } }
    function failure(error:any) { return { type: sourcerConstants.SOURCER_UPLOAD_CV_FAILURE, error } }
}


export const sourcerActions = {
    Register ,
    SourcerExperience,
    SourcerJobTitle ,
    SourcerAccessWebSites ,
    SourcerIndustry,
    SourcerUploadCV,
};
