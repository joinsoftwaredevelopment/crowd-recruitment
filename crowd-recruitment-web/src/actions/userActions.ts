import { registrationStepsConstant } from './../constants/registrationStepsConstant';
import { roleConstants } from './../constants/roleConstants';
import { IResponseData } from '../models/IResponseData';
import { userConstants } from '../constants/userConstants';
import  userService  from '../services/UserService';
import { alertActions } from './alertActions';
import { history } from '../helpers/history';
import { IUser } from '../models/IUser';
import { Dispatch } from 'redux';

export const userActions = {
    login,
    logout
};

function login(currentUser:IUser) {
    return (dispatch:any) => {
        dispatch(request(currentUser));

        userService.logIn(currentUser)
            .then(
                user => { 
                    debugger;

                    let responseDetails : IResponseData  =  user.data
                    dispatch(success(responseDetails));
                
                    // store user details and jwt token in local storage to keep user logged in between page refreshes
                    localStorage.setItem('user', JSON.stringify(responseDetails.data));
                    if(user.data.data.userRole == roleConstants.EMPLOYER )
                    {
                        //employer  home page
                        history.push('/employer/dashboard/0');
                    }
                    else if(user.data.data.userRole == roleConstants.SOURCER )
                    {
                        //sourcer home page
                        if(user.data.data.userRegistrationStep == registrationStepsConstant.Simple_Form_Step )
                           history.push('/registration/sourcer-experience');
                        else if(user.data.data.userRegistrationStep == registrationStepsConstant.Experience_Step )
                        history.push('/registration/sourcer-jobtitle');
                        else if(user.data.data.userRegistrationStep == registrationStepsConstant.Sourcer_Title_Step )
                        history.push('/registration/sourcer-access');
                        else if(user.data.data.userRegistrationStep == registrationStepsConstant.Website_Step )
                        history.push('/registration/sourcer-industry');
                        else if(user.data.data.userRegistrationStep == registrationStepsConstant.Industries_Step )
                        history.push('/registration/sourcer-upload-resume');
                        else if(user.data.data.userRegistrationStep == registrationStepsConstant.Attach_CV_Step )
                        history.push('/sourcer/myJobs');
                    }
                    dispatch(alertActions.success(responseDetails.message));
                


                },
                error => {
                    let responseDetails : IResponseData  =  error.response.data
                    dispatch(failure(responseDetails));
                    dispatch(alertActions.error(responseDetails.message));
                }
            );
    };

    function request(user:any) { return { type: userConstants.LOGIN_REQUEST, user } }
    function success(user:any) { return { type: userConstants.LOGIN_SUCCESS, user } }
    function failure(error:any) { return { type: userConstants.LOGIN_FAILURE, error } }
}

function logout() {
    userService.logout();
    return { type: userConstants.LOGOUT };
}
