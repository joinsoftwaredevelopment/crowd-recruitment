import http from "../helpers/InitAxios";
import { IEmployer } from "../models/IEmployer";
import { IEmployerDetails } from "../models/IEmployerDetails";
import { authHeader ,authHeaderWithFormData } from '../helpers/auth-header';
import { IUserRequest } from "../models/IUserRequest";

const create = (employer:IEmployer) => {
  debugger;
  return http.post("/Employer/create-employer", employer);
};

const getAll = (userStatusId :number) => {
  return http.get("/MangeEmployer/GetAllEmployers?userStatusId="+userStatusId, { headers: authHeader()});
};

const GetJobDetailsInfo = (uuid:string, tempId: number) => {
  debugger;
  return http.get("/Employer/GetJobDetailsInfo?uuid="+uuid + "&tempId="+tempId, { headers: authHeader()});
};

const GetJobInstructionsInfo = (uuid:string) => {
  debugger;
  return http.get("/Employer/GetJobInstructionsInfo?uuid="+uuid, { headers: authHeader()});
};

const GetJobEngagementsInfo = (uuid:string) => {
  debugger;
  return http.get("/Employer/GetJobEngagementsInfo?uuid="+uuid, { headers: authHeader()});
};


const GetJobInfo = (uuid:string) => {
  debugger;
  return http.get("/Employer/GetJobByUuid?uuid="+uuid, { headers: authHeader()});
};

const acceptEmployer = (userRequestViewModel:IUserRequest) => {
  debugger;
  return http.post("/MangeEmployer/AcceptEmployer",userRequestViewModel, { headers: authHeader()});
};

const rejectEmployer = (userRequestViewModel:IUserRequest) => {
  debugger;
  return http.post("/MangeEmployer/DeclineEmployer",userRequestViewModel, { headers: authHeader()});
};


const getMeeting = (uuid:string) => {
  debugger;
  return http.get("/User/GetMeetingByUuid?uuid="+uuid, { headers: authHeader()});
};

const acceptCandidate = (userRequestViewModel:IUserRequest) => {
  debugger;
  return http.post("/Candidate/EmployerAcceptCandidate",userRequestViewModel, { headers: authHeader()});
};

const rejectCandidate = (userRequestViewModel:IUserRequest) => {
  debugger;
  return http.post("/Candidate/EmployerDeclineCandidate",userRequestViewModel, { headers: authHeader()});
};

const GetEmployerDetalis = () => {
  debugger;
  return http.get("/EmployerProfile/GetEmployerDetails?uuid=''", { headers: authHeader()});
};

const UpdateEmployerDetails = (empoyerDetails: IEmployer) => {
  debugger;
  return http.post("/EmployerProfile/UpdateEmployerDetails",empoyerDetails, { headers: authHeader()});
};

const UpdatePackage = (empoyerDetails: IEmployer) => {
  debugger;
  return http.post("/EmployerProfile/UpdatePackage",empoyerDetails, { headers: authHeader()});
};


  export default {
    create,
    getAll,
    acceptEmployer,
    GetJobDetailsInfo,
    GetJobInfo,
    GetJobInstructionsInfo,
    getMeeting,
    rejectEmployer,
    GetJobEngagementsInfo,
    acceptCandidate,
    rejectCandidate,
    GetEmployerDetalis,
    UpdateEmployerDetails,
    UpdatePackage
  };