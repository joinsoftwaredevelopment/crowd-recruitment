import React from "react";
import "./headerRegistration.css";
import { useHistory } from "react-router-dom";
import Button from "@material-ui/core/Button";
import Menu from "@material-ui/core/Menu";
import MenuItem from "@material-ui/core/MenuItem";
import userDum from "../../src/assets/user-dum.jpg";
import { roleConstants } from "../constants/roleConstants";
import { history } from "../helpers/history";

interface IProps {}

interface IState {
  drawerState: number;
  brandName: string;
}

enum DrawerState {
  Initial = 1,
  SlideIn = 2,
  SlideOut = 3,
}

export class AppHeaderArabic extends React.Component<IProps, IState> {
  constructor(props: any) {
    super(props);

    this.state = {
      drawerState: DrawerState.Initial,
      brandName: "CR",
    };
  }

  toggleDrawer = () => {
    let drawerState = DrawerState.Initial;
    switch (this.state.drawerState) {
      case DrawerState.Initial: {
        drawerState = DrawerState.SlideIn;
        break;
      }
      case DrawerState.SlideIn: {
        drawerState = DrawerState.SlideOut;
        break;
      }
      case DrawerState.SlideOut: {
        drawerState = DrawerState.SlideIn;
      }
    }

    this.setState((state) => ({
      drawerState: drawerState,
    }));
  };

  render() {
    return (
      <div className="navbar appHeader">
        <div className="navbar-background" />
        <i className="drawer-icon" onClick={this.toggleDrawer}>
          menu
        </i>
        <h1
          onClick={() => {
            history.push("/employerHome");
          }}
          className="brand"
        >
          {this.state.brandName}
        </h1>
        <div className="buttons-wrapper">
          <Buttons />
        </div>

        <Drawer
          drawerState={this.state.drawerState}
          toggleDrawer={this.toggleDrawer}
        >
          <Buttons />
        </Drawer>
      </div>
    );
  }
}

const Buttons = (props: any) => {
  const history = useHistory();

  const [anchorEl, setAnchorEl] = React.useState<null | HTMLElement>(null);

  const handleClick = (event: React.MouseEvent<HTMLButtonElement>) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  var currentPath = window.location.pathname;
  console.log(currentPath);

  if (
    currentPath.includes("login") ||
    currentPath.includes("employer-signup") ||
    currentPath.includes("sourcer-signup") ||
    currentPath.includes("logout")
  ) {
    localStorage.setItem("user", "");
  }
  // check token if user has a token remove (login/ become sourcer / become employer) links
  let user = JSON.parse(localStorage.getItem("user") || "{}");
  //console.log((user && user.token));
  return (
    <>
      <button
        onClick={() => {
          history.push("/employerHome");
        }}
      >
        الرئيسية
      </button>
      <button
        hidden={user && user.token}
        onClick={() => {
          history.push("/login");
        }}
      >
        تسجيل دخول
      </button>
      <button
        hidden={user && user.token}
        onClick={() => {
          history.push("/registration/employer-signup");
        }}
      >
        تسجيل حساب
      </button>
      <button
        hidden={user && user.token}
        onClick={() => {
          history.push("/sourcerhome");
        }}
      >
        اصبح منقح
      </button>
      <button
        hidden={
          !(user && user.token && user.userRole == roleConstants.EMPLOYER)
        }
        onClick={() => {
          history.push("/employer/dashboard/0");
        }}
      >
        {" "}
        My Jobs{" "}
      </button>
      <button
        hidden={!(user && user.token && user.userRole == roleConstants.SOURCER)}
        onClick={() => {
          history.push("/sourcer/myJobs");
        }}
      >
        {" "}
        My Jobs{" "}
      </button>
      <button
        hidden={!(user && user.token && user.userRole == roleConstants.SOURCER)}
        onClick={() => {
          history.push("/sourcer/SourcerBalances");
        }}
      >
        {" "}
        Balances{" "}
      </button>
      <button hidden={!(user && user.token)}>
        {/* <i className='fa fa-bell'></i>
       </button>
      <button>  */}
        <Button
          className="nav-drop"
          aria-controls="simple-menu"
          aria-haspopup="true"
          onClick={handleClick}
        >
          <img src={userDum} />
          {user.userName}
          <i style={{ paddingLeft: "10px" }} className="fa fa-chevron-down"></i>
        </Button>
        <Menu
          id="simple-menu"
          anchorEl={anchorEl}
          keepMounted
          open={Boolean(anchorEl)}
          onClose={handleClose}
        >
          {/* <MenuItem onClick={handleClose}>Profile</MenuItem> */}
          <MenuItem
            style={{
              display: !(
                user &&
                user.token &&
                user.userRole == roleConstants.ADMIN
              )
                ? "none"
                : "inline",
            }}
            onClick={() => {
              history.push("/admin/employer-request");
              handleClose();
            }}
          >
            Dashboard
          </MenuItem>
          <MenuItem
            onClick={() => {
              localStorage.setItem("user", "");
              history.push("/employerHome");
              handleClose();
            }}
          >
            Logout
          </MenuItem>
        </Menu>
      </button>
      <button
        onClick={() => history.push("/employerHome")}
        className="lang-btn"
        style={{ padding: "0 10px" }}
      >
        English
      </button>
    </>
  );
};

const Drawer = (props: any) => {
  let toggleDrawer: Function = props.toggleDrawer;
  let drawerState: DrawerState = props.drawerState;

  return (
    <div className="drawer">
      <DrawerMenu drawerState={drawerState}>{props.children}</DrawerMenu>
      <DrawerTapoutArea drawerState={drawerState} toggleDrawer={toggleDrawer} />
    </div>
  );
};

const DrawerMenu = (props: any) => {
  if (props.drawerState === DrawerState.SlideIn) {
    return (
      <div className="drawer-menu animated slideInLeft">{props.children}</div>
    );
  } else if (props.drawerState === DrawerState.SlideOut)
    return (
      <div className="drawer-menu animated slideOutLeft">{props.children}</div>
    );
  else return <div />;
};

const DrawerTapoutArea = (props: any) => {
  return (
    <>
      {props.drawerState === DrawerState.SlideIn ? (
        <div className="drawer-tapout-area" onClick={props.toggleDrawer} />
      ) : null}
    </>
  );
};
// end of Navbar.tsx-------------------------------------------------------------
