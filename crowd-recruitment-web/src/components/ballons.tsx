import React from 'react';
import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import Grid from '@material-ui/core/Grid';
//import baloons from '../../src/assets/baloons.png'


const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        root: {
        },
    }),
);

export default function Ballons() {
    const classes = useStyles();

    return (
        <React.Fragment>
            <div className='container'>
                <div className='row'>
                    <div className='col-md-3'></div>
                    <div className='col-md-6'>
                   <div className='auth-white-bg thankyou text-center' style={{marginBottom:'30px'}}>
                   <img style={{margin:'20px 0'}} src={""}/>
                   <div className='blackHead'>
                   Booking Confirmed
                   </div>
                   <p className='blurText'>
                  You're booked with Hamzah. <br/>
                  An invivtation has been emailed to you.

                   </p>

                   <div className='blackHead'>
                  23 Dec 2009
                  <br/>
                  04:34 Pm
                   </div>

                   
                   </div>
                    </div>
                    <div className='col-md-3'></div>
                </div>
            </div>

        </React.Fragment>

    );
}
