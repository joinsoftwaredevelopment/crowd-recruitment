import React, { useEffect, useState } from 'react';
import alertSuccess from "../../assets/alert-success.png";
import alertError from "../../assets/alert-error.png";
import { alertActions } from "../../actions";
import { useDispatch } from "react-redux";

// USE LIKE THAT
// <Alert
// type="alert-warning"
// message="its a warning"
// />

interface Props {
  type: string;
  message: string;
}

const Alert: React.FC<Props> = ({ type, message }) => {
  const dispatch = useDispatch();
  const [count, setCount] = useState(0);


  useEffect(() => {
    const timer = setTimeout(() => {
      dispatch(alertActions.clear());
    }, 7000);
    return () => clearTimeout(timer);
  }, []);
  
  const closeAlert = () => {
    //    write close alert store logic here
    dispatch(alertActions.clear());
  };
  return (
    <div className={`alert ${type}`}>
      { type == "alert-danger" ?  <img src={alertError} /> :  <img src={alertSuccess} />}
      <img src={alertSuccess} />
      <span className="alertBox">
        {type == "alert-danger" && <i className="fal fa-times"></i>}
        {type == "alert-success" && <i className="fal fa-check-circle"></i>}
        {type == "alert-info" && <i className="fal fa-info"></i>}
        {type == "alert-warning" && (
          <i className="fal fa-exclamation-triangle"></i>
        )}
      </span>
      <span className="alertText">{message}</span>
      <i onClick={closeAlert} className="fal fa-times-circle"></i>
    </div>
  );
};

export default Alert;
