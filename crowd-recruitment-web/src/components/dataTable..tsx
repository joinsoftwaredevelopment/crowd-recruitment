import DataTable, { createTheme } from 'react-data-table-component';
import React from "react";
import { createStyles, makeStyles, Theme } from "@material-ui/core/styles";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import Grid from "@material-ui/core/Grid";
import thank from "../../../src/assets/thankful.png";
  
    const columns =  [
      {
        name: 'Title',
        selector: 'title',
        sortable: true,
      },
      {
        name: 'Director',
        selector: 'director',
        sortable: true,
      },
      {
        name: 'Year',
        selector: 'year',
        sortable: true,
      },
]

const data = [{ title: 1, director: 'Conan the Barbarian', year: '1982' }]

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {},
  })
);

export default function EmployerThankYou() {
  localStorage.setItem("user", "");

  const classes = useStyles();

  return (
    <React.Fragment>
      <div className="container">
      <DataTable
        title="Movie List"
        columns={columns}
        data={data}
      />

      </div>
    </React.Fragment>
  );
}
