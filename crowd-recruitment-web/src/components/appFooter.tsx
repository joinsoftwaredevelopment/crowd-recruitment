import React from 'react';
import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import Grid from '@material-ui/core/Grid';
import thank from '../../../src/assets/thankful.png'


const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        root: {
        },
    }),
);

export default function AppFooter() {
    const classes = useStyles();

    return (
        <React.Fragment>
        <section className='appFooter'>
        Copy rights By JoinSolutions
        </section>

        </React.Fragment>

    );
}
