
import React, { useEffect, useState } from "react";
import { createStyles, makeStyles, Theme } from "@material-ui/core/styles";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import ReactAutocomplete from 'react-autocomplete';


const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {},
  })
);

export default function AutoComplete() {

  const classes = useStyles();
  const [value, setSetvalue] = useState("");

  function handleChange(event: React.ChangeEvent<HTMLInputElement>) {
    setSetvalue(event.target.value);
  };

  function selectValue(value: any) {
    setSetvalue(value);
  }
  

  return (
    <React.Fragment>
<div className='autocom'>
<ReactAutocomplete
        items={[
          { id: 'foo', label: 'foo' },
          { id: 'bar', label: 'bar' },
          { id: 'baz', label: 'baz' },
        ]}
        shouldItemRender={(item:any, value:any) => item.label.toLowerCase().indexOf(value.toLowerCase()) > -1}
        getItemValue={item => item.label}
        renderItem={(item, highlighted) =>
          <div
            key={item.id}
            style={{ backgroundColor: highlighted ? '#eee' : 'transparent'}}
          >
            {item.label}
          </div>
        }
        value={value}
        onChange={(event) => handleChange(event)}
        onSelect={(value) => selectValue( value )}
      />
</div>
    </React.Fragment>
  );
}
