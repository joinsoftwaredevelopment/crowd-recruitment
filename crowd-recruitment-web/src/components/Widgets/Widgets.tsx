import React from "react";

interface Props {
  color: string;
  icon: string;
  title: string;
  number: number;
}

const Widget: React.FC<Props> = ({ color, icon, title, number }) => {
  return (
    <div className="widget">
      <span
        style={{
          backgroundColor: color,
        }}
        className="widgetIcon"
      >
        <i className={icon}></i>
      </span>
      <span className="widgetTitle">
        {title}
        <span className="widgetNumber">{number}</span>
      </span>
    </div>
  );
};

export default Widget;
