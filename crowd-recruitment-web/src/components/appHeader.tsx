import React from "react";
import "./headerRegistration.css";
import { useHistory } from "react-router-dom";
import Button from "@material-ui/core/Button";
import Menu from "@material-ui/core/Menu";
import MenuItem from "@material-ui/core/MenuItem";
import userDum from "../../src/assets/user-dum.jpg";
import { roleConstants } from "../constants/roleConstants";
import { history } from "../helpers/history";
import { registrationStepsConstant } from "../constants/registrationStepsConstant";

interface IProps {}

interface IState {
  drawerState: number;
  brandName: string;
}

enum DrawerState {
  Initial = 1,
  SlideIn = 2,
  SlideOut = 3,
}

export class AppHeader extends React.Component<IProps, IState> {
  constructor(props: any) {
    super(props);

    this.state = {
      drawerState: DrawerState.Initial,
      brandName: "CR",
    };
  }

  toggleDrawer = () => {
    let drawerState = DrawerState.Initial;
    switch (this.state.drawerState) {
      case DrawerState.Initial: {
        drawerState = DrawerState.SlideIn;
        break;
      }
      case DrawerState.SlideIn: {
        drawerState = DrawerState.SlideOut;
        break;
      }
      case DrawerState.SlideOut: {
        drawerState = DrawerState.SlideIn;
      }
    }

    this.setState((state) => ({
      drawerState: drawerState,
    }));
  };

  render() {
    return (
      <div className="navbar appHeader">
        <div className="navbar-background" />
        <i className="drawer-icon" onClick={this.toggleDrawer}>
          menu
        </i>
        <h1
          onClick={() => {
            history.push("/employerHome");
          }}
          className="brand"
        >
          {this.state.brandName}
        </h1>
        <div className="buttons-wrapper">
          <Buttons />
        </div>

        <Drawer
          drawerState={this.state.drawerState}
          toggleDrawer={this.toggleDrawer}
        >
          <Buttons />
        </Drawer>
      </div>
    );
  }
}

let currentContent: any;

let content = {
  English: {
    Home: "Home",
    SignIn: "Sign In",
    SignUp: "Sign Up",
    BecomeSourcer: "For Sourcers",
    LogOut: "Logout",
    MyJobs: "My Jobs",
    Balances: "Balance",
    Dashboard: "Dashboard",
  },
  Arabic: {
    Home: "الرئيسية",
    SignIn: "دخول",
    SignUp: "تسجيل",
    BecomeSourcer: "CVS لمقدمي",
    LogOut: "الخروج",
    MyJobs: "وظائفي",
    Balances: "الرصيد",
    Dashboard: "لوحة التحكم",
  },
};

localStorage.getItem("currentLanguage") === "Arabic"
  ? (currentContent = content.Arabic)
  : (currentContent = content.English);

const Buttons = (props: any) => {
  const history = useHistory();

  const [anchorEl, setAnchorEl] = React.useState<null | HTMLElement>(null);

  const handleClick = (event: React.MouseEvent<HTMLButtonElement>) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  var currentPath = window.location.pathname;
  console.log(currentPath);

  if (
    currentPath.includes("login") ||
    currentPath.includes("employer-signup") ||
    currentPath.includes("sourcer-signup") ||
    currentPath.includes("logout")
  ) {
    localStorage.setItem("user", "");
  }
  // check token if user has a token remove (login/ become sourcer / become employer) links
  let user = JSON.parse(localStorage.getItem("user") || "{}");

  function changeLanguage(languageName: string) {
    debugger;
    localStorage.setItem("currentLanguage", languageName);
    window.location.reload();
  }
  //console.log((user && user.token));
  return (
    <>
      <button
        onClick={() => {
          history.push("/employerHome");
        }}
      >
        {currentContent.Home}
      </button>
      <button
        hidden={user && user.token}
        onClick={() => {
          history.push("/employerlogin");
        }}
      >
        {currentContent.SignIn}
      </button>
      <button
        hidden={user && user.token}
        onClick={() => {
          history.push("/employer-signup");
        }}
      >
        {currentContent.SignUp}
      </button>
      <button
        hidden={user && user.token}
        onClick={() => {
          history.push("/sourcerhome");
        }}
      >
        {currentContent.BecomeSourcer}
      </button>
      <button
        hidden={
          !(user && user.token && user.userRole == roleConstants.EMPLOYER)
        }
        onClick={() => {
          history.push("/employer/dashboard/0");
        }}
      >
        {" "}
        {currentContent.MyJobs}{" "}
      </button>
      <button
        hidden={
          !(
            user &&
            user.token &&
            user.userRole == roleConstants.SOURCER &&
            user.userRegistrationStep ==
              registrationStepsConstant.Attach_CV_Step
          )
        }
        onClick={() => {
          history.push("/sourcer/myJobs");
        }}
      >
        {" "}
        {currentContent.MyJobs}{" "}
      </button>
      <button
        hidden={
          !(
            user &&
            user.token &&
            user.userRole == roleConstants.SOURCER &&
            user.userRegistrationStep ==
              registrationStepsConstant.Attach_CV_Step
          )
        }
        onClick={() => {
          history.push("/sourcer/SourcerBalances");
        }}
      >
        {" "}
        {currentContent.Balances}{" "}
      </button>
      <button hidden={!(user && user.token)}>
        {/* <i className='fa fa-bell'></i>
       </button>
      <button>  */}
        <Button
          className="nav-drop"
          aria-controls="simple-menu"
          aria-haspopup="true"
          onClick={handleClick}
        >
          <img src={userDum} />
          {user.userName}
          <i style={{ paddingLeft: "10px" }} className="fa fa-chevron-down"></i>
        </Button>
        <Menu
          id="simple-menu"
          anchorEl={anchorEl}
          keepMounted
          open={Boolean(anchorEl)}
          onClose={handleClose}
        >
          {/* <MenuItem onClick={handleClose}>Profile</MenuItem> */}
          <MenuItem
            style={{
              display: !(
                user &&
                user.token &&
                user.userRole == roleConstants.ADMIN
              )
                ? "none"
                : "inline",
            }}
            onClick={() => {
              history.push("/admin/employer-request");
              handleClose();
            }}
          >
            {currentContent.Dashboard}
          </MenuItem>

          {user.userRole == roleConstants.EMPLOYER ? (
            <MenuItem
              onClick={() => {
                history.push("/employer/ProfileEmployer/1");
                handleClose();
              }}
            >
              Profile
            </MenuItem>
          ) : user.userRole == roleConstants.SOURCER ? (
            <MenuItem
              onClick={() => {
                history.push("/sourcer/Profile/" + user.uuid);
                handleClose();
              }}
            >
              Profile
            </MenuItem>
          ) : (
            ""
          )}

          <MenuItem
            onClick={() => {
              localStorage.setItem("user", "");
              history.push("/employerHome");
              handleClose();
            }}
          >
            {currentContent.LogOut}
          </MenuItem>
        </Menu>
      </button>

      <button
        onClick={() =>
          localStorage.getItem("currentLanguage") === "Arabic"
            ? changeLanguage("English")
            : changeLanguage("Arabic")
        }
        className="lang-btn"
        style={{ padding: "0 10px" }}
      >
        {localStorage.getItem("currentLanguage") === "Arabic"
          ? "English"
          : "العربية"}
      </button>
    </>
  );
};

const Drawer = (props: any) => {
  let toggleDrawer: Function = props.toggleDrawer;
  let drawerState: DrawerState = props.drawerState;

  return (
    <div className="drawer">
      <DrawerMenu drawerState={drawerState}>{props.children}</DrawerMenu>
      <DrawerTapoutArea drawerState={drawerState} toggleDrawer={toggleDrawer} />
    </div>
  );
};

const DrawerMenu = (props: any) => {
  if (props.drawerState === DrawerState.SlideIn) {
    return (
      <div className="drawer-menu animated slideInLeft">{props.children}</div>
    );
  } else if (props.drawerState === DrawerState.SlideOut)
    return (
      <div className="drawer-menu animated slideOutLeft">{props.children}</div>
    );
  else return <div />;
};

const DrawerTapoutArea = (props: any) => {
  return (
    <>
      {props.drawerState === DrawerState.SlideIn ? (
        <div className="drawer-tapout-area" onClick={props.toggleDrawer} />
      ) : null}
    </>
  );
};
// end of Navbar.tsx-------------------------------------------------------------
