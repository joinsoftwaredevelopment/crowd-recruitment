import React from "react";
import CircularProgress from '@material-ui/core/CircularProgress';

interface Props {
  border: string;
  color: string;
  children?: React.ReactNode;
  height: string;
  onClick: () => void;
  radius: string
  width: string;
  bgColor:string,
  font:string,
  fontSize:string,
  isLoading:boolean,
}

const Button: React.FC<Props> = ({ 
    border,
    color,
    children,
    height,
    onClick, 
    radius,
    width,
    bgColor,
    font,
    fontSize,
    isLoading,
  }) => { 
  return (
    <button 
      className='btn'
      type="submit"
      onClick={onClick}
      style={{
         pointerEvents:isLoading ? 'none' :'all',
         backgroundColor: bgColor,
         color:color,
         border,
         borderRadius: radius,
         height,
         width,
         fontSize:fontSize,
         opacity:isLoading ? 0.5 : 1
      }}
    >
    {isLoading ? <CircularProgress color='secondary' /> :''}
    {children}
    </button>
  );
}

export default Button;