export const SubmissionStatusConstant = {
    Draft : 1,
    Submitted : 2,
    Approved : 3,
    Disqualified : 4,
    ApprovedByEmployer:5,
    DeclinedByEmployer:6
};