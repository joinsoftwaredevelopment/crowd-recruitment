export const registrationStepsConstant = {
    Simple_Form_Step : 1 ,
    Experience_Step : 2 ,
    Sourcer_Title_Step : 3 ,
    Website_Step : 4 ,
    Industries_Step : 5,
    Attach_CV_Step : 6,
    Book_A_Meeting : 7
};
