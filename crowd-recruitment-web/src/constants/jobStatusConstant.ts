export const jobStatusConstant = {
    Draft : 1,  
    Posted : 2,
    Opened : 3,
    Declined : 4,
    Closed : 5,
    All: 0,
};