import React from 'react';
import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import Grid from '@material-ui/core/Grid';
import AppFooter from '../components/appFooter';

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        root: {
        },
    }),
);

export default class EmptyLayout extends React.Component<{}, {}> {
    render() {
        const { children } = this.props;

        return (
            <React.Fragment>             
                <div className="layout-empty">
                    {children}
                </div>
                <AppFooter/>
            </React.Fragment>

        );
    };
};
