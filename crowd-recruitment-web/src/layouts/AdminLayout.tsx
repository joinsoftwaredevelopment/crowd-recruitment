import React from "react";
import clsx from "clsx";
import {
  createStyles,
  makeStyles,
  useTheme,
  Theme,
} from "@material-ui/core/styles";
import Tooltip from "@material-ui/core/Tooltip";
import Drawer from "@material-ui/core/Drawer";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import List from "@material-ui/core/List";
import CssBaseline from "@material-ui/core/CssBaseline";
import Typography from "@material-ui/core/Typography";
import Divider from "@material-ui/core/Divider";
import IconButton from "@material-ui/core/IconButton";
import MenuIcon from "@material-ui/icons/Menu";
import ChevronLeftIcon from "@material-ui/icons/ChevronLeft";
import ChevronRightIcon from "@material-ui/icons/ChevronRight";
import ListItem from "@material-ui/core/ListItem";
import ListItemIcon from "@material-ui/core/ListItemIcon";
import ListItemText from "@material-ui/core/ListItemText";
import InboxIcon from "@material-ui/icons/MoveToInbox";
import MailIcon from "@material-ui/icons/Mail";
import { useHistory } from "react-router-dom";
import bman from "../../src/assets/business-man.png";

import MonetizationOnIcon from "@material-ui/icons/MonetizationOn";
import BusinessCenterIcon from "@material-ui/icons/BusinessCenter";
import AssignmentIndIcon from "@material-ui/icons/AssignmentInd";
import PersonIcon from "@material-ui/icons/Person";
import FolderSpecialIcon from '@material-ui/icons/FolderSpecial';
const drawerWidth = 240;

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      display: "flex",
    },
    appBar: {
      zIndex: theme.zIndex.drawer + 1,
      transition: theme.transitions.create(["width", "margin"], {
        easing: theme.transitions.easing.sharp,
        duration: theme.transitions.duration.leavingScreen,
      }),
    },
    appBarShift: {
      marginLeft: drawerWidth,
      width: `calc(100% - ${drawerWidth}px)`,
      transition: theme.transitions.create(["width", "margin"], {
        easing: theme.transitions.easing.sharp,
        duration: theme.transitions.duration.enteringScreen,
      }),
    },
    menuButton: {
      marginRight: 36,
    },
    hide: {
      display: "none",
    },
    drawer: {
      width: drawerWidth,
      flexShrink: 0,
      whiteSpace: "nowrap",
    },
    drawerOpen: {
      width: drawerWidth,
      transition: theme.transitions.create("width", {
        easing: theme.transitions.easing.sharp,
        duration: theme.transitions.duration.enteringScreen,
      }),
    },
    drawerClose: {
      transition: theme.transitions.create("width", {
        easing: theme.transitions.easing.sharp,
        duration: theme.transitions.duration.leavingScreen,
      }),
      overflowX: "hidden",
      width: theme.spacing(7) + 1,
      [theme.breakpoints.up("sm")]: {
        width: theme.spacing(9) + 1,
      },
    },
    toolbar: {
      display: "flex",
      alignItems: "center",
      justifyContent: "flex-end",
      padding: theme.spacing(0, 1),
      // necessary for content to be below app bar
      ...theme.mixins.toolbar,
    },
    content: {
      flexGrow: 1,
      padding: theme.spacing(3),
    },
  })
);

interface TabPanelProps {
  children?: React.ReactNode;
}

export default function AdminLayout(props: TabPanelProps) {
  const { children } = props;
  const classes = useStyles();
  const theme = useTheme();
  const [open, setOpen] = React.useState(true);
  const history = useHistory();

  // check token if user has a token remove (login/ become sourcer / become employer) links
  let user = JSON.parse(localStorage.getItem("user") || "{}");
  //console.log((user && user.token));

  const handleDrawerOpen = () => {
    setOpen(true);
  };

  const handleDrawerClose = () => {
    setOpen(false);
  };

  return (
    <div className={classes.root}>
      <CssBaseline />
      <AppBar
        position="fixed"
        className={clsx(classes.appBar, {
          [classes.appBarShift]: open,
        })}
      >
        <Toolbar>
          <IconButton
            color="inherit"
            aria-label="open drawer"
            onClick={handleDrawerOpen}
            edge="start"
            className={clsx(classes.menuButton, {
              [classes.hide]: open,
            })}
          >
            <MenuIcon />
          </IconButton>
          <Typography variant="h6" noWrap>
            CR
          </Typography>
          <div className="pull-right">
            <span
              style={{ padding: "0 10px", cursor: "pointer" }}
              onClick={() => {
                history.push("/employerHome");
              }}
            >
              Home
            </span>

            <span
              onClick={() => {
                localStorage.setItem("user", "");
                history.push("/employerHome");
              }}
              style={{ padding: "0 10px", cursor: "pointer" }}
            >
              Logout
            </span>
            <span className="lang-btn" style={{ padding: "0 10px" }}>
              EN
            </span>
          </div>
        </Toolbar>
      </AppBar>
      <Drawer
        variant="permanent"
        className={clsx(classes.drawer, {
          [classes.drawerOpen]: open,
          [classes.drawerClose]: !open,
        })}
        classes={{
          paper: clsx({
            [classes.drawerOpen]: open,
            [classes.drawerClose]: !open,
          }),
        }}
      >
        <div className={classes.toolbar}>
          <img className="admin-img" src={bman} />
          <span className="admin-text">Hi, {user.userName}</span>
          <IconButton onClick={handleDrawerClose}>
            {theme.direction === "rtl" ? (
              <ChevronRightIcon />
            ) : (
              <ChevronLeftIcon />
            )}
          </IconButton>
        </div>
        <Divider />
        <List>
          {[
            "Employer Requests",
            "Sourcer Requests",
            "Job Requests",
            "Sourcer Balance",
            "Price Packages"
          ].map((text, index) => (
            <ListItem
              button
              key={text}
              onClick={() => {
                if (text == "Employer Requests") {
                  history.push("/admin/employer-request");
                }
                if (text == "Sourcer Requests") {
                  history.push("/admin/sourcer-request");
                }
                if (text == "Job Requests") {
                  history.push("/admin/job-requests");
                }
                if (text == "Sourcer Balance") {
                  history.push("/admin/sourcer-balance");
                }
                if (text == "Price Packages") {
                  history.push("/admin/packages");
                }
              }}
            >
              <ListItemIcon>
                {index == 0 && (
                  <Tooltip title="Employer Requests">
                    <PersonIcon />
                  </Tooltip>
                )}
                {index == 1 && (
                  <Tooltip title="Sourcer Requests">
                    <AssignmentIndIcon />
                  </Tooltip>
                )}
                {index == 2 && (
                  <Tooltip title="Job Requests">
                    <BusinessCenterIcon />
                  </Tooltip>
                )}
                {index == 3 && (
                  <Tooltip title="Sourcer Balance">
                    <MonetizationOnIcon />
                  </Tooltip>
                )}
                {index == 4 && (
                  <Tooltip title="Price Packages">
                    <FolderSpecialIcon />
                  </Tooltip>
                )}
              </ListItemIcon>
              <ListItemText primary={text} />
            </ListItem>
          ))}
        </List>
      </Drawer>
      <main className={classes.content}>
        <div className={classes.toolbar} />
        {children}
      </main>
    </div>
  );
}
