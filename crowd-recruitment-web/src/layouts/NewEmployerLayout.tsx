import React, { useEffect, useState } from "react";
import clsx from "clsx";
import {
  createStyles,
  makeStyles,
  useTheme,
  Theme,
} from "@material-ui/core/styles";
import Tooltip from "@material-ui/core/Tooltip";
import Drawer from "@material-ui/core/Drawer";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import List from "@material-ui/core/List";
import CssBaseline from "@material-ui/core/CssBaseline";
import Typography from "@material-ui/core/Typography";
import Divider from "@material-ui/core/Divider";
import IconButton from "@material-ui/core/IconButton";
import MenuIcon from "@material-ui/icons/Menu";
import ChevronLeftIcon from "@material-ui/icons/ChevronLeft";
import ChevronRightIcon from "@material-ui/icons/ChevronRight";
import ListItem from "@material-ui/core/ListItem";
import ListItemIcon from "@material-ui/core/ListItemIcon";
import ListItemText from "@material-ui/core/ListItemText";
import InboxIcon from "@material-ui/icons/MoveToInbox";
import MailIcon from "@material-ui/icons/Mail";
import { useHistory } from "react-router-dom";
import candIco from "../../src/assets/cand-ico.png";

import MonetizationOnIcon from "@material-ui/icons/MonetizationOn";
import BusinessCenterIcon from "@material-ui/icons/BusinessCenter";
import AssignmentIndIcon from "@material-ui/icons/AssignmentInd";
import PersonIcon from "@material-ui/icons/Person";

import Collapse from "@material-ui/core/Collapse";

import IconExpandLess from "@material-ui/icons/ExpandLess";
import IconExpandMore from "@material-ui/icons/ExpandMore";
import IconDashboard from "@material-ui/icons/Dashboard";
import IconShoppingCart from "@material-ui/icons/ShoppingCart";
import IconPeople from "@material-ui/icons/People";
import IconBarChart from "@material-ui/icons/BarChart";
import WorkOutlineOutlinedIcon from "@material-ui/icons/WorkOutlineOutlined";
import PermIdentityOutlinedIcon from "@material-ui/icons/PermIdentityOutlined";
import AddCircleOutlineOutlinedIcon from "@material-ui/icons/AddCircleOutlineOutlined";
import ExitToAppOutlinedIcon from "@material-ui/icons/ExitToAppOutlined";
import JobMenu from "../pages/employer/Dashboard/JobMenu";
import { jobActions } from "../actions/jobActions";
import { IJobSummary } from "../models/IJobSummary";
import { jobStatusConstant } from "../constants/jobStatusConstant";

const drawerWidth = 240;

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      display: "flex",
    },
    appBar: {
      zIndex: theme.zIndex.drawer + 1,
    },
    drawer: {
      width: drawerWidth,
      flexShrink: 0,
    },
    menuButton: {
      marginRight: 36,
    },
    hide: {
      display: "none",
    },
    drawerOpen: {
      width: drawerWidth,
      transition: theme.transitions.create("width", {
        easing: theme.transitions.easing.sharp,
        duration: theme.transitions.duration.enteringScreen,
      }),
    },
    drawerClose: {
      transition: theme.transitions.create("width", {
        easing: theme.transitions.easing.sharp,
        duration: theme.transitions.duration.leavingScreen,
      }),
      overflowX: "hidden",
      width: 0,
      [theme.breakpoints.up("sm")]: {
        width: 0,
      },
    },
    drawerPaper: {
      width: drawerWidth,
    },
    menuItem: {
      width: drawerWidth,
    },
    menuItemIcon: {
      color: "#97c05c",
    },
    drawerContainer: {
      overflow: "auto",
    },
    content: {
      flexGrow: 1,
      padding: theme.spacing(3),
    },
  })
);

interface TabPanelProps {
  children?: React.ReactNode;
}

export default function NewEmployerLayout(props: TabPanelProps) {
  const { children } = props;
  const classes = useStyles();
  const theme = useTheme();
  const [open, setOpen] = React.useState(true);
  const [activeOption, setactiveOption] = React.useState(null);
  const history = useHistory();

  // check token if user has a token remove (login/ become sourcer / become employer) links
  let user = JSON.parse(localStorage.getItem("user") || "{}");
  //console.log((user && user.token));

  const handleDrawerOpen = () => {
    setOpen(!open);
  };

  const handleDrawerClose = () => {
    setOpen(false);
  };

  const handleActiveOption = (args: any) => {
    setactiveOption(args);
  };

  const [openDrop, setOpenDrop] = React.useState("d1");

  function handleClick(args: any) {
    setOpenDrop(args);
  }

  const [jobs, setJobs] = useState<IJobSummary[]>([]);

  useEffect(() => {
    jobActions.retrieveAllJobs().then((response) => {
      debugger;
      setJobs(response.data.data);
    });
  }, []);

  const NoOfJobsByStatus = (jobStatusId: any) =>
    jobs.filter((m: IJobSummary) => m.jobStatusId == jobStatusId).length;

  const JobPagesArr = [
    {
      status: jobStatusConstant.Opened,
      title: "Active",
      className: "blueBg",
      count: NoOfJobsByStatus(Number(jobStatusConstant.Opened)),
    },
    {
      status: jobStatusConstant.Closed,
      title: "Closed",
      className: "redBg",
      count: NoOfJobsByStatus(Number(jobStatusConstant.Closed)),
    },
    {
      status: jobStatusConstant.Draft,
      title: "Draft",
      className: "grayBg",
      count: NoOfJobsByStatus(Number(jobStatusConstant.Draft)),
    },
    {
      status: jobStatusConstant.Posted,
      title: "Pending",
      className: "orangeBg",
      count: NoOfJobsByStatus(Number(jobStatusConstant.Posted)),
    },
  ];

  return (
    <React.Fragment>
      <div
      id={open ? 'drawerOpen' : 'drawerClose'}
      className="newEmpLayout">
        <div className={classes.root}>
          <CssBaseline />
          <AppBar position="fixed" className={classes.appBar}>
            <Toolbar>
              <Typography variant="h6" noWrap>
                CR
                <IconButton
                id="openDrawer"
                color="inherit"
                aria-label="open drawer"
                onClick={handleDrawerOpen}
                edge="start"
                className={clsx(classes.menuButton)}
              >
                <MenuIcon />
              </IconButton>
              
              </Typography>

              <div className="pull-right">
                <span
                  style={{ padding: "0 10px", cursor: "pointer" }}
                  onClick={() => {
                    history.push("/employerHome");
                  }}
                >
                  Home
                </span>

                <span
                  onClick={() => {
                    localStorage.setItem("user", "");
                    history.push("/employerHome");
                  }}
                  style={{ padding: "0 10px", cursor: "pointer" }}
                >
                  Logout
                </span>
                <span className="lang-btn" style={{ padding: "0 10px" }}>
                  EN
                </span>
              </div>
            </Toolbar>
          </AppBar>
          <Drawer
            id='leftSideBar'
            variant="permanent"
            className={clsx(classes.drawer, {
              [classes.drawerOpen]: open,
              [classes.drawerClose]: !open,
            })}
            classes={{
              paper: clsx({
                [classes.drawerOpen]: open,
                [classes.drawerClose]: !open,
              }),
            }}
          >
            <div className="profileBox">
              <div className="float-left">
                <img className="admin-img" src={candIco} />
              </div>
              <div className="float-left sideInfo">
                <div>Welcome!</div>
                <div>{user.userName}</div>
              </div>
            </div>

            <List>
              <ListItem
                button
                onClick={() => handleClick("d1")}
                className={openDrop == "d1" ? "activeDrop" : ""}
              >
                <ListItemIcon className={classes.menuItemIcon}>
                  <WorkOutlineOutlinedIcon />
                </ListItemIcon>
                <ListItemText primary="My Jobs" />
                {openDrop == "d1" ? <IconExpandLess /> : <IconExpandMore />}
              </ListItem>
              <Collapse in={openDrop == "d1"} timeout="auto" unmountOnExit>
                <Divider />
                <div
                  onClick={() => {
                    history.push("/employer/EmployerJobDetails/0/0");
                    setactiveOption(11);
                  }}
                  className={
                    activeOption == 11
                      ? "customMenuList activeSideOpt"
                      : "customMenuList"
                  }
                >
                  <i className="far fa-plus-circle"></i>
                  <span>Add Job</span>
                </div>
                {JobPagesArr.map((page) => {
                  return (
                    <div
                      className={
                        page.status == activeOption
                          ? "customMenuList activeSideOpt"
                          : "customMenuList"
                      }
                      key={page.title}
                    >
                      <span
                        onClick={() => {
                          debugger;
                          history.push("/employer/dashboard/" + page.status);
                          setactiveOption(page.status);
                        }}
                      >
                        <i className="far fa-circle"></i>
                        <span>{page.title}</span>
                      </span>
                      <span
                        className={"lableMini  pull-right " + page.className}
                      >
                        {page.count}
                      </span>
                    </div>
                  );
                })}
              </Collapse>

              {/* <ListItem button onClick={() => handleClick('d3')}
      className={openDrop == 'd3' ? 'activeDrop' : ''}
      >
        <ListItemIcon className={classes.menuItemIcon}>
          <AddCircleOutlineOutlinedIcon />
        </ListItemIcon>
        <ListItemText primary="Add Job" />
      
      </ListItem> */}

              <ListItem
                button
                onClick={() => handleClick("d2")}
                className={openDrop == "d2" ? "activeDrop" : ""}
              >
                <ListItemIcon className={classes.menuItemIcon}>
                  <PermIdentityOutlinedIcon />
                </ListItemIcon>
                <ListItemText primary="Profile Settings" />
                {openDrop == "d2" ? <IconExpandLess /> : <IconExpandMore />}
              </ListItem>
              <Collapse in={openDrop == "d2"} timeout="auto" unmountOnExit>
                <Divider />
                <div
                  className={
                    activeOption == 8
                      ? "customMenuList activeSideOpt"
                      : "customMenuList"
                  }
                >
                  <div
                    onClick={() => {
                      history.push("/employer/ProfileEmployer/1");
                      setactiveOption(8);
                    }}
                  >
                    <i className="far fa-circle"></i>
                    <span>Basic Info</span>
                  </div>
                </div>
                <div
                  className={
                    activeOption == 9
                      ? "customMenuList activeSideOpt"
                      : "customMenuList"
                  }
                >
                  <div
                    onClick={() => {
                      history.push("/employer/ProfileEmployer/2");
                      setactiveOption(9);
                    }}
                  >
                    <i className="far fa-circle"></i>
                    <span>Change password</span>
                  </div>
                </div>
                <div
                  className={
                    activeOption == 10
                      ? "customMenuList activeSideOpt"
                      : "customMenuList"
                  }
                >
                  <div
                    onClick={() => {
                      history.push("/employer/ProfileEmployer/3");
                      setactiveOption(10);
                    }}
                  >
                    <i className="far fa-circle"></i>
                    <span>Packages</span>
                  </div>
                </div>
              </Collapse>

              <ListItem
                button
                onClick={() => handleClick("d4")}
                className={openDrop == "d4" ? "activeDrop" : ""}
              >
                <ListItemIcon className={classes.menuItemIcon}>
                  <ExitToAppOutlinedIcon />
                </ListItemIcon>
                <ListItemText
                  onClick={() => {
                    localStorage.setItem("user", "");
                    history.push("/employerHome");
                  }}
                  primary="LogOut"
                />
              </ListItem>
            </List>
          </Drawer>
          <main className={classes.content}>
            <div />
            {children}
          </main>
        </div>
      </div>
    </React.Fragment>
  );
}
