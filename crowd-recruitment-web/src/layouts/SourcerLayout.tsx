import React from 'react';
import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import Grid from '@material-ui/core/Grid';
import  {AppHeader}  from '../components/appHeader'

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        root: {
        },
    }),
);

export default class SourcerLayout extends React.Component<{}, {}> {
    render() {
        const { children } = this.props;

        return (
            <React.Fragment>
                 <div className="layout-home">
                <AppHeader />
                <div className="layout-sourcer">
                <div className='bg-inner'>
                
                </div>
                    {children}
                </div>
                </div>
            </React.Fragment>
           

        );
    };
};
