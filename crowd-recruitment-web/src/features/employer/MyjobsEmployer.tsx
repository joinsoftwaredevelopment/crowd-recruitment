import React from 'react';
import { createStyles, Theme, withStyles, WithStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import Grid from '@material-ui/core/Grid';
import thank from '../../../src/assets/thankful.png'
import decline from '../../../src/assets/decline.png'
import empIco from '../../../src/assets/emp-ico.png'
import Switch from '@material-ui/core/Switch';
import CircularProgress from '@material-ui/core/CircularProgress';

import IconButton from '@material-ui/core/IconButton';
import CloseIcon from '@material-ui/icons/Close';
import Typography from '@material-ui/core/Typography';

import Dialog from '@material-ui/core/Dialog';
import MuiDialogTitle from '@material-ui/core/DialogTitle';
import MuiDialogContent from '@material-ui/core/DialogContent';
import MuiDialogActions from '@material-ui/core/DialogActions';

const styles = (theme: Theme) =>
    createStyles({
        root: {
            margin: 0,
            padding: theme.spacing(2),
        },
        closeButton: {
            position: 'absolute',
            right: theme.spacing(1),
            top: theme.spacing(1),
            color: theme.palette.grey[500],
        },
    });

export interface DialogTitleProps extends WithStyles<typeof styles> {
    id: string;
    children: React.ReactNode;
    onClose: () => void;
}

const DialogTitle = withStyles(styles)((props: DialogTitleProps) => {
    const { children, classes, onClose, ...other } = props;
    return (
        <MuiDialogTitle disableTypography className={classes.root} {...other}>
            <Typography variant="h6">{children}</Typography>
            {onClose ? (
                <IconButton aria-label="close" className={classes.closeButton} onClick={onClose}>
                    <CloseIcon />
                </IconButton>
            ) : null}
        </MuiDialogTitle>
    );
});

const DialogContent = withStyles((theme: Theme) => ({
    root: {
        padding: theme.spacing(2),
    },
}))(MuiDialogContent);

const DialogActions = withStyles((theme: Theme) => ({
    root: {
        margin: 0,
        padding: theme.spacing(1),
    },
}))(MuiDialogActions);

export default function MyjobsEmployer() {
    const [open, setOpen] = React.useState(false);
    const [openCloseJob, setOpenCloseJob] = React.useState(false);
    const [state, setState] = React.useState({
        checkedA: true,
    });

    const handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        setState({ ...state, [event.target.name]: event.target.checked });
    };

    const handleClickOpen = () => {
        setOpen(true);
    };
    const handleClickOpenCloseJob = () => {
      setOpenCloseJob(true);
  };
    const handleClose = () => {
        setOpen(false);
        setOpenCloseJob(false);
    };


    return (
        <React.Fragment>
            <div className='container emp-job'>
                <div className='row'>
                    <div className='col-md-12'>
                        <div className='top-head'>
                            <div className="bold-head">My Jobs</div>
                            <button className='btn btn-decline'>
                                <i className='fa fa-plus'></i>
                Add Job
                </button>
                        </div>
                        <div className='job-bg'>
                            <div className='head-sec'>
                                <div className="bread-text">
                                    <span className="lt-head">My Jobs</span>


                                </div>



                                <div className='filter' style={{ paddingLeft: '70px' }}>
                                    <span className='active-filter highlight-filter'>Active</span>
                                    <span className='draft-filter'>Closed</span>
                                    <span className='non-active-filter'>Draft</span>
                                </div>

                                <div className='filter'>
                                    <span className='active-filter highlight-filter'>My Jobs</span>
                                    <span className='non-active-filter'>All Jobs</span>
                                </div>
                            </div>

                            <div className='row empJB'>
                            {/* <CircularProgress /> */}
                                <div className='col-md-4'>
                                    <div className='job-box'>
                                        <div className='switchBox'>

                                            <div className='pull-left'>

                                                <span>
                                                    Job status : <span className='green'>Closed</span>
                                                </span>

                                            </div>
                                            <div className='pull-right'>
                                                <Switch
                                                    checked={false}
                                                    onChange={handleChange}
                                                    color="primary"
                                                    name="checkedA"
                                                    inputProps={{ 'aria-label': 'primary checkbox' }}
                                                />
                                            </div>

                                        </div>
                                        <div className='remain-img-box'>
                                            <img src={empIco} />
                                        </div>

                                        <div className="data-card-head">Sales, Marketing and PR</div>
                                        <div className="data-card-light">Dubai - United Arab Emaraties</div>
                                        <div className="data-card-light">
                                            <img className='roundImg' src={thank} />
                Owner: shrook ashraf
                </div>

                                        <div className='job-card-btm'>
                                            <div className="data-card-light">  Edited 20 hours ago </div>
                                            <div className="data-card-light-green">
                                                <span className='red' onClick={handleClickOpen}>View Reason</span>
                                            </div>

                                        </div>
                                    </div>
                                </div>

                                <div className='col-md-4'>
                                    <div className='job-box'>
                                        <div className='switchBox'>

                                            <div className='pull-left'>

                                                <span>
                                                    Job status : <span className='green'>Opened</span>
                                                </span>

                                            </div>
                                            <div className='pull-right'>
                                                <Switch
                                                    checked={state.checkedA}
                                                    onChange={handleChange}
                                                    color="primary"
                                                    name="checkedA"
                                                    inputProps={{ 'aria-label': 'primary checkbox' }}
                                                />
                                            </div>

                                        </div>
                                        <div className='remain-img-box'>
                                            <img src={empIco} />
                                        </div>

                                        <div className="data-card-head">Sales, Marketing and PR</div>
                                        <div className="data-card-light">Dubai - United Arab Emaraties</div>
                                        <div className="data-card-light">
                                            <img className='roundImg' src={thank} />
                Owner: shrook ashraf
                </div>

                                        <div className='job-card-btm'>
                                            <div className="data-card-light">  Edited 20 hours ago </div>
                                            <div className="data-card-light-green">
                                                <a>View Candidates</a>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>




            </div>


            <Dialog
                fullWidth={true}
                maxWidth={'sm'}
                onClose={handleClose} aria-labelledby="customized-dialog-title" open={open}>
                <DialogTitle id="customized-dialog-title3" onClose={handleClose}>
                    Reason
                </DialogTitle>

                <DialogContent
                    className='text-center'
                    dividers>

                    <img style={{ marginTop: '20px' }} src={decline} />

                    <div className='head-job-pop'>

                        <div className='blackHead'>
                            We are sorry to inform you that <br />
Ahmed Bashiir had declined your job post
                   </div>
                        <div className='shortBorder'></div>
                    </div>

                    <p className='blurText'>
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex e

                   </p>





                    <div className='text-center job-pop-btns'>

                        <button className='btn btn-approve'>OK</button>
                    </div>


                </DialogContent>


            </Dialog>

            
            <Dialog
                fullWidth={true}
                maxWidth={'sm'}
                onClose={handleClose} aria-labelledby="customized-dialog-title" open={openCloseJob}>
                <DialogTitle id="customized-dialog-title3" onClose={handleClose}>
                Close Job
                </DialogTitle>

                <DialogContent
                    className='text-center'
                    dividers>

                    <img style={{ marginTop: '20px' }} src={decline} />

                    <div className='head-job-pop'>

                        <div className='blackHead'>
                        Are you sure you want to close this job?
                   </div>
                       
                    </div>

                   




                    <div className='text-center job-pop-btns'>
                    <button className='btn btn-decline'>No</button>
                        <button className='btn btn-approve'>Yes</button>
                    </div>


                </DialogContent>


            </Dialog>


        </React.Fragment>

    );
}
