import React from 'react';
import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';




const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
    },
  }),
);

export default function EmployerSignupForm() {
  const classes = useStyles();

  return (
    <React.Fragment>
      <div className='container'>
        <div className='row'>
          <div className='col-md-3'></div>
          <div className='col-md-6'>
            <form className='auth-white-bg' noValidate autoComplete="off">
              <div className='form-group'>
                <TextField label="Company Name" />
              </div>

              <div className='form-group'>
                <TextField label="First Name" />
              </div>

              <div className='form-group'>
                <TextField label="Last Name" />
              </div>

              <div className='form-group'>
                <TextField type='number' label="Phone Number" />
              </div>

              <div className='form-group'>
                <TextField type='email' label="Email" />
              </div>

              <div className='auth-btns'>
                <Button variant="contained" color="primary">
                  Sign up
                </Button>

                <Button variant="contained" className='linkedin-btn'>Login with Linkedin</Button>
              </div>

            </form>
          </div>
          <div className='col-md-3'></div>
        </div>
      </div>

    </React.Fragment>

  );
}
