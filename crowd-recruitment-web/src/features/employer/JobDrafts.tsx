import React, { useState, useEffect } from "react";
import { 
  createStyles,
  Theme,
  withStyles,
  WithStyles,
  makeStyles,
 } from "@material-ui/core/styles";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import Grid from "@material-ui/core/Grid";
import thank from "../../../src/assets/thankful.png";
import { Link, useHistory } from "react-router-dom";
import { history } from "../../helpers/history";
import { IJobsInfo } from "../../models/IJobsInfo";
import JobService from "../../services/JobService";
import empIco from "../../../src/assets/emp-ico.png";
import { jobStatusConstant } from "../../constants/jobStatusConstant";
import CircularProgress from "@material-ui/core/CircularProgress";
import Switch from "@material-ui/core/Switch";
import { IJobSummary } from "../../models/IJobSummary";
import cross from "../../../src/assets/cross.png";

import IconButton from "@material-ui/core/IconButton";
import CloseIcon from "@material-ui/icons/Close";
import Typography from "@material-ui/core/Typography";

import Dialog from "@material-ui/core/Dialog";
import MuiDialogTitle from "@material-ui/core/DialogTitle";
import MuiDialogContent from "@material-ui/core/DialogContent";
import MuiDialogActions from "@material-ui/core/DialogActions";

const styles = (theme: Theme) =>
  createStyles({
    root: {
      margin: 0,
      padding: theme.spacing(2),
    },
    closeButton: {
      position: "absolute",
      right: theme.spacing(1),
      top: theme.spacing(1),
      color: theme.palette.grey[500],
    },
  });

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {},
  })
);

export interface DialogTitleProps extends WithStyles<typeof styles> {
  id: string;
  children: React.ReactNode;
  onClose: () => void;
}

const DialogTitle = withStyles(styles)((props: DialogTitleProps) => {
  const { children, classes, onClose, ...other } = props;
  return (
    <MuiDialogTitle disableTypography className={classes.root} {...other}>
      <Typography variant="h6">{children}</Typography>
      {onClose ? (
        <IconButton
          aria-label="close"
          className={classes.closeButton}
          onClick={onClose}
        >
          <CloseIcon />
        </IconButton>
      ) : null}
    </MuiDialogTitle>
  );
});

const DialogContent = withStyles((theme: Theme) => ({
  root: {
    padding: theme.spacing(2),
  },
}))(MuiDialogContent);

export default function JobDrafts() {


  const [jobs, setJobs] = useState<IJobSummary[]>([]);

  const [showLoader, setShowLoader] = React.useState(false);
  const [open, setOpen] = React.useState(false);

  const [deletedID, setDeletedID] = React.useState("");

  const handleClose = () => {
    setOpen(false);
  };
  const handleOpen = (uuid: string) => {
    debugger;
    setDeletedID(uuid);
    setOpen(true);
  };

  useEffect(() => {
    setShowLoader(true);
    retrieveEmployers();
  }, []);

  const retrieveEmployers = () => {
    JobService.getallJobsDraftsByEmp()
      .then((response) => {
        debugger;
        setJobs(response.data.data);
        setShowLoader(false);
      })
      .catch((e) => {
        console.log(e);
        setShowLoader(false);
      });
  };

  const DeleteJob = () => {
    debugger;
    JobService.DeleteJob(deletedID)
      .then((response) => {
        retrieveEmployers();
        handleClose();
      })
      .catch((e) => {
        console.log(e);
      });
  };

  return (
    <React.Fragment>
      <div className="container emp-job">
        <div className="row">
          <div className="col-md-12">
            <div className="top-head">
              <div className="bold-head">My Jobs</div>
              <button
                onClick={() => {
                  history.push("/employer/JobPostDetails");
                }}
                className="btn btn-decline"
              >
                <i className="fa fa-plus"></i>
                Add Job
              </button>
            </div>
            <div className="job-bg">
              <div className="head-sec">
                <div className="mini-head">My Jobs</div>

                <div className="filter" style={{ paddingLeft: "70px" }}>
                  <span
                    className="draft-filter border-rad-L-6"
                    onClick={() => {
                      history.push("/employer/dashboard");
                    }}
                  >
                    Active
                  </span>

                  <span
                    className="draft-filter"
                    onClick={() => {
                      history.push("/employer/JobClosed");
                    }}
                  >
                    Closed
                  </span>
                  <span className="non-active-filter border-rad-R-6 highlight-filter">
                    Draft
                  </span>
                </div>

                <div className="filter">
                  {/* <span className='active-filter highlight-filter'>My Jobs</span>
                    <span className='non-active-filter'>All Jobs</span> */}
                </div>
              </div>

              <div className="row empJB">
                {showLoader === true ? <CircularProgress /> : ""}
                {jobs.map((m) => {
                  return (
                    <div key={m.uuid} className="col-md-4">
                      <div className="job-box" key={m.uuid}>
                        <div className="switchBox">
                          <div className="pull-left">
                            <span>
                              Job status :{" "}
                              <span
                                className={
                                  m.jobStatusId == jobStatusConstant.Declined
                                    ? "red"
                                    : "green"
                                }
                              >
                                {m.jobStatusId == jobStatusConstant.Declined
                                  ? "Rejected"
                                  : m.jobStatusName}
                              </span>
                            </span>
                          </div>

                          <div className="pull-right">
                            {/* <Switch
                              id=""
                              key=""
                              checked={false}
                              color="primary"
                              name=""
                              inputProps={{ "aria-label": "primary checkbox" }}
                            /> */}
                            <i 
                            onClick={() => handleOpen(m.uuid)}
                            className='far fa-times-circle crossIco'></i>
                          </div>
                        </div>
                        <div className="remain-img-box">
                          <img src={empIco} />
                        </div>

                        <div className="data-card-head">{m.jobTitle}</div>

                        <div
                          style={{ position: "relative" }}
                          className="data-card-light"
                        >
                          {m.seniorityLevelName}
                          </div>
                          <div
                          style={{ position: "relative" }}
                          className="data-card-light"
                        >
                          {m.jobLocation}
                          </div>
                        <div
                          className="data-card-light"
                          style={{ position: "relative" }}
                        >
                          <img className="roundImg" src={thank} />
                          {m.countryName}
                          <Link
                            className="detail-link"
                            to={"/employer/JobPostDetails/" + m.uuid+"/0"}
                          >
                            Show Details
                          </Link>
                        </div>

                        <div className="job-card-btm">
                          <div className="data-card-light">
                            {m.jobDate}

                            
                       </div>

                       <div
                      key={m.uuid}
                       
                       onClick={() => {
                       history.push("/employer/JobPostDetails/" + m.uuid+"/0");
                       }}
                     >
                       Continue sourcing
                            {/* <Link to={"/employer/JobPreview/" + m.uuid}>
                              Details
                            </Link> */}
                          </div>

                          {/* <div className="data-card-light-green">
                            <a
                              hidden={m.jobStatusId != jobStatusConstant.Opened}
                              href={"/candidate/candidates/" + m.uuid}
                            >
                              View Candidates
                            </a>
                          </div> */}
                        </div>
                      </div>
                    </div>
                  );
                })}
              </div>
            </div>
          </div>
        </div>
      </div>

      <Dialog
      fullWidth={true}
      maxWidth={"sm"}
      onClose={handleClose}
      aria-labelledby="customized-dialog-title"
      open={open}
    >
      <DialogTitle id="customized-dialog-title3" onClose={handleClose}>
         Confirmation
      </DialogTitle>
      <DialogContent className="text-center" dividers>
        <img style={{ marginTop: "20px" }} src={cross} />

        <div className="head-job-pop">
          <div className="blackHead">
            Are you sure you want to delete this job?
          </div>
          <div className="shortBorder"></div>
        </div>

        

        <div className="text-center job-pop-btns">
        <button onClick={handleClose} className="btn btn-decline">
       Cancel
          </button>
          <button onClick={DeleteJob} className="btn btn-approve">
           Yes
          </button>
        </div>
      </DialogContent>
      
    </Dialog>
   
    </React.Fragment>
  );
}
