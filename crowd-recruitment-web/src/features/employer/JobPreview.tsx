import React, { useState, useEffect } from "react";
import {
  createStyles,
  Theme,
  withStyles,
  WithStyles,
} from "@material-ui/core/styles";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import Grid from "@material-ui/core/Grid";
import jobimg from "../../../src/assets/job-bg.png";
import Dialog from "@material-ui/core/Dialog";
import MuiDialogTitle from "@material-ui/core/DialogTitle";
import MuiDialogContent from "@material-ui/core/DialogContent";
import MuiDialogActions from "@material-ui/core/DialogActions";
import IconButton from "@material-ui/core/IconButton";
import CloseIcon from "@material-ui/icons/Close";
import Typography from "@material-ui/core/Typography";
import { useDropzone } from "react-dropzone";
import JobService from "../../services/JobService";
import { IJobSubmission } from "../../models/IJobSubmission";
import { ISourcerCVModel } from "../../models/ISourcerCVModel";
import { yupResolver } from "@hookform/resolvers/yup";
import { useForm } from "react-hook-form";
import * as Yup from "yup";
import { IJobDetails } from "../../models/IJobDetails";
import { useHistory } from "react-router-dom";
import Draft, {
  htmlToDraft,
  EmptyState,
  rawToDraft,
  draftToRaw,
  draftStateToHTML,
} from "react-wysiwyg-typescript";

const styles = (theme: Theme) =>
  createStyles({
    root: {
      margin: 0,
      padding: theme.spacing(2),
    },
    closeButton: {
      position: "absolute",
      right: theme.spacing(1),
      top: theme.spacing(1),
      color: theme.palette.grey[500],
    },
  });

export interface DialogTitleProps extends WithStyles<typeof styles> {
  id: string;
  children: React.ReactNode;
  onClose: () => void;
}

const DialogTitle = withStyles(styles)((props: DialogTitleProps) => {
  const { children, classes, onClose, ...other } = props;
  return (
    <MuiDialogTitle disableTypography className={classes.root} {...other}>
      <Typography variant="h6">{children}</Typography>
      {onClose ? (
        <IconButton
          aria-label="close"
          className={classes.closeButton}
          onClick={onClose}
        >
          <CloseIcon />
        </IconButton>
      ) : null}
    </MuiDialogTitle>
  );
});

const DialogContent = withStyles((theme: Theme) => ({
  root: {
    padding: theme.spacing(2),
  },
}))(MuiDialogContent);

const DialogActions = withStyles((theme: Theme) => ({
  root: {
    margin: 0,
    padding: theme.spacing(1),
  },
}))(MuiDialogActions);

export default function JobPreview(props: any) {
  const history = useHistory();
  const [open, setOpen] = React.useState(false);
  const handleClickOpen = () => {
    setOpen(true);
  };
  const handleClose = () => {
    setOpen(false);
  };

  const { acceptedFiles, getRootProps, getInputProps } = useDropzone({
    accept: ".pdf",
  });

  const files = acceptedFiles.map((file: any) => (
    <li key={file.path}>
      {file.path} - {file.size} bytes
    </li>
  ));

  const [sourcerSubmissions, setAllsourcerSubmissions] = useState<
    IJobSubmission[]
  >([]);

  const drawSourcerSubmissions = sourcerSubmissions.map(
    (job: IJobSubmission) => (
      <div className="col-md-4" key={job.jobId}>
        <div className="job-box">
          <div className="remain-img-box">
            <i className="fa fa-user"></i>
          </div>

          <div className="data-card-head">{job.cvName}</div>
          <div className="data-card-light">{job.jobTitle}</div>
          <div className="data-card-light">{job.jobLocationTranslation}</div>
        </div>
      </div>
    )
  );

  //inital sourcer state
  const [uploadCV, setUploadCV] = useState<ISourcerCVModel>({
    Uuid: "",
    JobUuId: "",
    CvName: "",
    LinkedInProfile: "",
    Email: "",
    Location: "",
    UploadedFile: acceptedFiles[0],
    candidateName: "",
    countryId: 189,
  });
  const [isUploaded, setIsUploaded] = useState(false);
  const [jobDetails, setjobDetails] = useState<IJobDetails>({
    uuid: "",
    jobTitle: "",
    employmentTypeName: "",
    seniorityLevelName: "",
    companiesNotToSourceFrom: "",
    languageId: 2,
    jobLocation: "",
    companyName: "",
    mustHaveQualification: "",
    niceToHaveQualification: "",
    hiringNeeds: "",
    experienceLevelName: "",
    description: "",
    companyIndustry: "",
    jobStatusId: 3,
    jobStatusName: "",
    requirements: "",
    noOfSubmissions: 0,
    jobDate: new Date(),
  });

  const getJobDetails = () => {
    debugger;
    JobService.getJobDetails(props.match.params.id)
      .then((response) => {
        debugger;
        //let d = document.createElement("div");
        //d.innerHTML = response.data.data.description;
        //response.data.data.description = d.innerText;
        //d.innerHTML = response.data.data.requirements;
        //response.data.data.requirements = d.innerText;
        setjobDetails(response.data.data);
      })
      .catch((e) => {
        console.log(e);
      });
  };

  useEffect(() => {
    getJobDetails();
  }, []);

  // form validation rules
  const validationRules = Yup.object().shape({
    LinkedInProfile: Yup.string().required("LinkedIn Profile is required"),
    Email: Yup.string().required("Email is required"),
    Location: Yup.string().required("Location is required"),
  });

  // functions to build form returned by useForm() hook
  const { register, handleSubmit, errors } = useForm<ISourcerCVModel>({
    resolver: yupResolver(validationRules),
  });

  function handleChange(e: any) {
    const { name, value } = e.target;
    // console.log(e.target);
    debugger;
    let file = acceptedFiles[0];
    if (file != null)
      setUploadCV((uploadCV) => ({
        ...uploadCV,
        [name]: value,
        UploadedFile: file,
        CvName: file.name,
      }));
    else setUploadCV((uploadCV) => ({ ...uploadCV, [name]: value }));
  }

  function UploadCVChanged() {
    setIsUploaded(true);
    console.log(isUploaded);
  }

  function onSubmit() {
    // display form data on success
    //alert(data);
    debugger;
    console.log(props.match.params.id);
    const formData = new FormData();
    formData.append("CvName", uploadCV.CvName);
    formData.append("UploadedFile", uploadCV.UploadedFile);
    formData.append("LinkedInProfile", uploadCV.LinkedInProfile);
    formData.append("Email", uploadCV.Email);
    formData.append("Location", uploadCV.Location);
    formData.append("JobUuId", props.match.params.id);

    debugger;
    JobService.addSourcerSubmission(formData)
      .then((response) => {
        debugger;
        setOpen(false);
      })
      .catch((e) => {
        console.log(e);
      });
  }

  return (
    <React.Fragment>
      {/* <div>
                <img src={jobimg}/>
            </div> */}
      <div className="container">
        {/* <Button variant="outlined" color="primary" onClick={handleClickOpen}>
        Open dialog
      </Button> */}

        <div className="row">
          <div className="col-md-12">
            <div className="bread-text pull-left">
              <span className="lt-head">
                <a href="/employer/dashboard">My Jobs</a>
              </span>
              <span className="lt-text">
                <i className="fas fa-angle-right"></i>
              </span>
              <span className="lt-head">{jobDetails.jobTitle}</span>
            </div>

            <div className="pull-right">
              Job status :{" "}
              <span className="green"> {jobDetails.jobStatusName}</span>
            </div>
          </div>
        </div>

        <div className="row">{drawSourcerSubmissions}</div>

        <div className="row sub-info">
          <div className="col-md-4">
            <div className="bold-head">{jobDetails.jobTitle}</div>
          </div>

          <div className="col-md-8 sub-info-right">
            {/* <span className='sub-row' onClick={handleClickOpen}> */}
            {/* <button  color="primary"  className='bold-clr-head'>
                    +  Add submission 
                    </button>                    */}
            {/* </span> */}
          </div>
        </div>

        <div className="row">
          <div className="col-md-12">
            <div className="sub-info-box">
              <div className="feature-set">
                <div className="feature-set-row">
                  <div className="bold-head">
                    <span>
                      <i className="fa fa-clock"></i>
                    </span>
                    <span>Employment type</span>
                  </div>

                  <div className="text">{jobDetails.employmentTypeName}</div>
                </div>

                <div className="feature-set-row">
                  <div className="bold-head">
                    <span>
                      <i className="fa fa-clock"></i>
                    </span>
                    <span>Seniority level</span>
                  </div>

                  <div className="text">{jobDetails.seniorityLevelName}</div>
                </div>

                <div className="feature-set-row">
                  <div className="bold-head">
                    <span>
                      <i className="fa fa-clock"></i>
                    </span>
                    <span>Company name</span>
                  </div>

                  <div className="text">{jobDetails.companyName}</div>
                </div>

                <div className="feature-set-row">
                  <div className="bold-head">
                    <span>
                      <i className="fa fa-clock"></i>
                    </span>
                    <span>Location</span>
                  </div>

                  <div className="text">{jobDetails.jobLocation}</div>
                </div>
              </div>

              <div className="feature-set-info">
                {/* <div className='sub-feature'>
                <div className='bold-head'>
                    Job function
                    </div>

                    <div className='light-text'>
                    Business Development - Marketing 
                    </div>
                </div> */}

                <div className="sub-feature">
                  <div className="bold-head">Company Industry</div>

                  <div className="light-text">{jobDetails.companyIndustry}</div>
                </div>
              </div>

              <div className="feature-set-info">
                <div className="sub-feature">
                  <div className="bold-head">Job Description</div>

                  
                   <div className='appColor'>
                   <div dangerouslySetInnerHTML={{ __html: jobDetails.description }} />
                    </div> 
                 
                </div>
              </div>

              <div className="feature-set-info">
                <div className="sub-feature">
                  <div className="bold-head">Job Requirements</div>
                   
                  <div className='appColor'>
                  <div dangerouslySetInnerHTML={{ __html: jobDetails.requirements }} />
                    </div> 
                  
                </div>
              </div>

              <div className="feature-set-info">
                <div className="sub-feature">
                  <div className="bold-head">Work experience</div>

                  <div className="light-text">
                    {jobDetails.experienceLevelName}
                  </div>
                </div>
              </div>

              <div className="feature-set-info">
                <div className="sub-feature">
                  <div className="bold-head">Must-have qualifications</div>

                  <div className="light-text">
                    {jobDetails.mustHaveQualification}
                  </div>
                </div>
              </div>

              <div className="feature-set-info">
                <div className="sub-feature">
                  <div className="bold-head">Companies not to source from</div>

                  <div className="light-text">
                    {jobDetails.companiesNotToSourceFrom}
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <Dialog
        fullWidth={true}
        maxWidth={"sm"}
        onClose={handleClose}
        aria-labelledby="customized-dialog-title"
        open={open}
      >
        <DialogTitle id="customized-dialog-title" onClose={handleClose}>
          Refer a candidate
        </DialogTitle>
        <form onSubmit={handleSubmit(onSubmit)} id="my-awesome-dropzone">
          <DialogContent dividers>
            <div>
              <div {...getRootProps({ className: "dropzone" })}>
                <input onChange={UploadCVChanged} {...getInputProps()} />
                <div className="file-drop" style={{ marginBottom: "15px" }}>
                  <div className="blackHead">Drag and drop resume here</div>
                  <div>
                    Or click <a className="anchor"> here to upload resume</a>
                  </div>
                  <div className="blurText" style={{ marginBottom: "0px" }}>
                    maximum file size: 2 MB{" "}
                  </div>
                </div>
              </div>
              <aside>
                <h4>File</h4>
                <ul>{files}</ul>
              </aside>
            </div>

            <div className="form-group">
              <label>Email</label>
              <input
                onChange={handleChange}
                type="email"
                name="Email"
                id="Email"
                placeholder="Email"
                className="form-control"
                value={uploadCV.Email}
                ref={register}
              />
            </div>

            <div className="form-group">
              <label>Linkedin Profile</label>
              <input
                onChange={handleChange}
                type="text"
                name="LinkedInProfile"
                id="LinkedInProfile"
                placeholder="LinkedIn Profile"
                className="form-control"
                value={uploadCV.LinkedInProfile}
                ref={register}
              />
            </div>

            <div className="form-group">
              <label>Location</label>
              <input
                onChange={handleChange}
                type="text"
                name="Location"
                id="Location"
                placeholder="Location"
                className="form-control"
                value={uploadCV.Location}
                ref={register}
              />
            </div>
          </DialogContent>
          <DialogActions>
            <Button type="submit" variant="contained" color="primary">
              Submit
            </Button>
          </DialogActions>
        </form>
      </Dialog>
    </React.Fragment>
  );
}
