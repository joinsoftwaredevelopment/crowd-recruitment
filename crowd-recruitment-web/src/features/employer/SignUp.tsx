import React, { useState } from "react";
import { IEmployer } from "../../models/IEmployer";
import employerService from "../../services/EmployerService";
import { useForm, Controller } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import * as Yup from "yup";
import Button from "@material-ui/core/Button";
import { RootStateOrAny, useDispatch, useSelector } from "react-redux";

import PhoneInput from "react-phone-input-2";
import "react-phone-input-2/lib/style.css";

import { employerActions } from "../../actions/employerActions";
import { CircularProgress } from "@material-ui/core";

interface Props {
  language: string;
}

function SignUp(props: Props) {
  let currentContent: any;

  let content = {
    English: {
      firstName: "First Name",
      lastName: "Last Name",
      companyName: "Company Name",
      phoneNumber: "Phone Number",
      email: "Email Address",
      password: "Password",
      signUp: "Sign Up",
      loginWithLinkedin: "Login With LinkedIn",
      Phonenumberisrequired: "Phone number is required",
      Firstnameisrequired : "First name is required",
      Lastnameisrequired : "Last name is required",
      Mustbeonlydigits : "Must be only digits",
      Emailisrequired : "Email is required",
      Emailisinvalid : "Email is invalid",
      Passwordmustbeatleast:"Password must be at least 8 characters",
      Passwordisrequired : "Password is required",
      companynameisrequired: "Company name is required",
    },
    Arabic: {
      firstName: "الاسم الأول",
      lastName: "الاسم الأخير",
      companyName: "اسم الشركة",
      phoneNumber: "الهاتف الجوال",
      email: "البريد الإلكتروني",
      password: "كلمة المرور",
      signUp: "تسجيل",
      loginWithLinkedin: "الدخول بإستخدام لينكد ان",
      Phonenumberisrequired: "الهاتف الجوال مطلوب",
      Firstnameisrequired : "الاسم الأول مطلوب",
      Lastnameisrequired : "الاسم الأخير مطلوب",
      Mustbeonlydigits : "يجب أن يكون أرقام فقط",
      Emailisrequired : "البريد الإلكتروني مطلوب",
      Emailisinvalid : "البريد الإلكتروني غير صالح",
      Passwordmustbeatleast:"كلمة المرور مكونة من 8 أحرف علي الأقل",
      Passwordisrequired : "كلمة المرور مطلوبة",
      companynameisrequired: "اسم الشركة مطلوب",
    },
  };

  localStorage.getItem("currentLanguage") === "Arabic"
    ? (currentContent = content.Arabic)
    : (currentContent = content.English);

  const initialEmployeeState = {
    CompanyName: "",
    FirstName: "",
    LastName: "",
    PhoneNumber: "",
    Email: "",
    Password: "",
  };

  const dispatch = useDispatch();

  const [employer, setEmployee] = useState(initialEmployeeState);

  const handleInputChange = (event: any) => {
    const { name, value } = event.target;
    setEmployee({ ...employer, [name]: value });
  };

  /* const saveEmployee = () => {
    var data:IEmployer = {
        CompanyName: employer.CompanyName,
        FirstName: employer.FirstName,
        LastName: employer.LastName,
        PhoneNumber: employer.PhoneNumber,
        Email: employer.Email,
        Password: employer.Password
    };

    employerService.create(data)
      .then(response => {
        debugger;
        console.log(response.data);
      })
      .catch(e => {
        console.log(e);
      });
  }; */

  const newEmployee = () => {
    setEmployee(initialEmployeeState);
  };

  // Validation
  // form validation rules
  const validationSchema = Yup.object().shape({
CompanyName: Yup.string().trim().required(currentContent.companynameisrequired),
    FirstName: Yup.string().trim().required(currentContent.Firstnameisrequired),
    LastName: Yup.string().trim().required(currentContent.Lastnameisrequired),

    PhoneNumber: Yup.string()
      .trim()
      .required(currentContent.Phonenumberisrequired)
      .matches(/^[0-9]+$/, currentContent.Mustbeonlydigits),
    Email: Yup.string()
      .trim()
      .required(currentContent.Emailisrequired)
      .email(currentContent.Emailisinvalid),
    Password: Yup.string()
      .trim()
      .min(8, currentContent.Passwordmustbeatleast)
      .required(currentContent.Passwordisrequired),
    // .test(
    //   "regex",
    //   "please choose a stronger password . Try a mix of letters , numbers and symbols.",
    //   (val) => {
    //     let regExp = new RegExp(
    //       "^(?=.*\\d)(?=.*[!@#$%^&*])(?=.*[a-z])(?=.*[A-Z]).{8,}$"
    //     );
    //     console.log(regExp.test(val), regExp, val);
    //     return regExp.test(val);
    //   }
    // ),
  });

  // functions to build form returned by useForm() hook
  const { register, handleSubmit, reset, errors, control } = useForm({
    resolver: yupResolver(validationSchema),
  });

  const registering = useSelector(
    (state: RootStateOrAny) => state.registration.registering
  );

  //function onSubmit(data:any) {
  //saveEmployee();
  //}

  function onSubmit(data: any) {
    // display form data on success
    //alert(data);
    debugger;
    dispatch(employerActions.register(data));
  }

  function handleOnChange(
    value: any,
    data: any,
    event: any,
    formattedValue: any
  ) {
    console.log("value", value);
  }

  return (
    <React.Fragment>
      <div className="container">
        <div className="row">
          <div className="col-md-3"></div>
          <div className="col-md-6">
            <form
              className="auth-white-bg"
              onSubmit={handleSubmit(onSubmit)}
              onReset={reset}
            >
              <div className="form-group">
                <label>{currentContent.companyName}</label>
                <input
                  name="CompanyName"
                  id="CompanyName"
                  value={employer.CompanyName}
                  type="text"
                  ref={register}
                  className={`form-control ${
                    errors.CompanyName ? "is-invalid" : ""
                  }`}
                  onChange={handleInputChange}
                />
                <div className="invalid-feedback">
                  {errors.CompanyName?.message}
                </div>
              </div>

              <div className="form-group">
                <label>{currentContent.firstName}</label>
                <input
                  name="FirstName"
                  id="FirstName"
                  value={employer.FirstName}
                  type="text"
                  ref={register}
                  className={`form-control ${
                    errors.FirstName ? "is-invalid" : ""
                  }`}
                  onChange={handleInputChange}
                />
                <div className="invalid-feedback">
                  {errors.FirstName?.message}
                </div>
              </div>

              <div className="form-group">
                <label>{currentContent.lastName}</label>
                <input
                  name="LastName"
                  id="LastName"
                  value={employer.LastName}
                  type="text"
                  ref={register}
                  className={`form-control ${
                    errors.LastName ? "is-invalid" : ""
                  }`}
                  onChange={handleInputChange}
                />
                <div className="invalid-feedback">
                  {errors.LastName?.message}
                </div>
              </div>

              <div className="form-group">
                <label>{currentContent.phoneNumber}</label>
                {/* Reference https://www.npmjs.com/package/react-phone-input-2 */}
                {/* <PhoneInput
                country={'sa'}
                /> */}
                {/* <input
                  name="PhoneNumber"
                  id="PhoneNumber"
                  value={employer.PhoneNumber}
                  type="text"
                  ref={register}
                  className={`form-control ${errors.PhoneNumber ? 'is-invalid' : ''}`}
                  onChange={handleInputChange}
                />  */}
                <Controller
                  name="PhoneNumber"
                  control={control}
                  defaultValue=""
                  render={({ name, onBlur, onChange, value }) => (
                    <PhoneInput
                      //name={name}
                      value={value}
                      onBlur={onBlur}
                      onChange={onChange}
                      country={"sa"}
                      //style={{ width: "100%" }}
                      // label="Contacto telefónico"
                      // variant="outlined"
                      // margin="normal"
                      inputProps={{
                        name: "PhoneNumber",
                        id: "PhoneNumber",
                        required: true,
                        autoFocus: true,
                      }}
                    />
                  )}
                />
                <div className="invalid-feedback">
                  {errors.PhoneNumber?.message}
                </div>
              </div>

              <div className="form-group">
                <label>{currentContent.email}</label>
                <input
                  name="Email"
                  id="Email"
                  value={employer.Email}
                  type="text"
                  ref={register}
                  className={`form-control ${errors.Email ? "is-invalid" : ""}`}
                  onChange={handleInputChange}
                />
                <div className="invalid-feedback">{errors.Email?.message}</div>
              </div>

              <div className="form-group">
                <label>{currentContent.password}</label>
                <input
                  name="Password"
                  id="Password"
                  value={employer.Password}
                  type="password"
                  ref={register}
                  className={`form-control ${
                    errors.Password ? "is-invalid" : ""
                  }`}
                  onChange={handleInputChange}
                />
                <div className="invalid-feedback">
                  {errors.Password?.message}
                </div>
              </div>

              <div className="auth-btns">
                {/* <button type="submit" color="primary">

                </button> */}

                <Button
                  disabled={registering}
                  type="submit"
                  variant="contained"
                  color="primary"
                >
                  {registering && (
                    <span className="loading">
                      <CircularProgress />
                    </span>
                  )}{" "}
                  {currentContent.signUp}
                </Button>
              </div>
            </form>
          </div>
          <div className="col-md-3"></div>
        </div>
      </div>
    </React.Fragment>
  );
}
export default SignUp;
