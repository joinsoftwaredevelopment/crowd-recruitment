import React, { useEffect, useState } from "react";
import ReactAutocomplete from "react-autocomplete";
import {
  createStyles,
  Theme,
  withStyles,
  WithStyles,
} from "@material-ui/core/styles";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import Grid from "@material-ui/core/Grid";
import jobimg from "../../../src/assets/job-bg.png";

import { IPostJobDetails } from "../../models/IPostJobDetails";
import { IPostJobDetailsCountries } from "../../models/IPostJobDetailsCountries";
import jobService from "../../services/JobService";
import { useForm } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import * as Yup from "yup";
import { useDispatch, useSelector } from "react-redux";
import { history } from "../../helpers/history";
import { create } from "yup/lib/array";
import employerService from "../../services/EmployerService";
import { debug } from "console";
import { CircularProgress } from "@material-ui/core";
import { EditorState, convertToRaw, ContentState } from "draft-js";
import Draft, {
  htmlToDraft,
  EmptyState,
  rawToDraft,
  draftToRaw,
  draftStateToHTML,
} from "react-wysiwyg-typescript";

import draftToHtml from "draftjs-to-html";

import Select from "react-select";

interface Props {
  language: string;
}

export default function JobPostDetails(props: any) {
  let currentContent: any;

  let content = {
    English: {
      Details: "Details",
      instructions: "Instructions",
      Objectives: "Objectives",
      Engagement: "Engagement",
      JobTitle: "Job title",
      SeniorityLevel: "Seniority level",
      EmploymentType: "Employment type",
      JobDescription: "Job description",
      JobRequirements: "Job requirements",
      CompanyIndustry: "Company industry",
      Location: "Location",
      SelectCountry: "Select Country",
      Next: "Next",
      Back: "Back to my jobs",
    },
    Arabic: {
      Details: "التفاصيل",
      instructions: "التعليمات",
      Objectives: "الأهداف",
      Engagement: "الإلتزامات",
      JobTitle: "لقب الوظيفة",
      SeniorityLevel: "الخبرة",
      EmploymentType: "نوع التوظيف",
      JobDescription: "وصف الوظيفة",
      JobRequirements: "متطلبات الوظيفة",
      CompanyIndustry: "مجال عمل الشركة",
      Location: "مكان العمل",
      SelectCountry: "اختر الدولة",
      Next: "التالي",
      Back: "العودة إلي وظائفي",
    },
  };

  localStorage.getItem("currentLanguage") === "Arabic"
    ? (currentContent = content.Arabic)
    : (currentContent = content.English);

  const initialJobState = {
    uuId: "",
    jobTitle: "",
    seniorityLevelId: 1,
    employerTypeId: 1,
    jobDescription: "",
    industryId: 1,
    jobLocationId: 1,
    jobNationalityId: 189,
    requirements: "",
    jobTemplateId: 0,
  };

  const [currentCountries, setcurrentCountries] = useState<
    IPostJobDetailsCountries[]
  >([]);

  const [descriptionError, setDescriptionError] = useState(false);
  const [requiredError, setRequiredError] = useState(false);

  useEffect(() => {
    retrieveJob(props.match.params.id, props.match.params.tempId);
  }, []);

  const retrieveJob = (uuid: string, tempId: number) => {
    debugger;
    employerService
      .GetJobDetailsInfo(uuid, tempId)
      .then((response: any) => {
        if (response.data.data.job.jobTitle != null) {
          setJob(response.data.data.job);
        }
        setcurrentCountries(response.data.data.countriesTranslations);
        debugger;
        setSelectedCountry(
          (response.data.data
            .countriesTranslations as IPostJobDetailsCountries[]).find(
            (m) => m.countryId == response.data.data.job.jobNationalityId
          )
        );


        debugger;
        seteditorState(htmlToDraft(response.data.data.job.jobDescription));

        setRecuirEditorState(htmlToDraft(response.data.data.job.requirements));
      })
      .catch((e: any) => {
        console.log(e);
      });
  };

  const handleDescDraftChange = (draftText: any) => {
    seteditorState(draftText);
    // SAVING HTML TO SERVER
    const convertedHtml = draftToHtml(
      convertToRaw(editorState.getCurrentContent())
    );
    // const html = draftToHtml(draftText)
    job.jobDescription = convertedHtml;
    setJob(job);
  };

  const handleRecuirDraftChange = (draftText: any) => {
    setRecuirEditorState(draftText);
    // SAVING HTML TO SERVER
    const convertedReqireHtml = draftToHtml(
      convertToRaw(recuirEditorState.getCurrentContent())
    );
    // const html = draftToHtml(draftText)
    job.requirements = convertedReqireHtml;
    setJob(job);
  };

  const dispatch = useDispatch();

  const [job, setJob] = useState(initialJobState);
  const [showLoader, setShowLoader] = React.useState(false);

  const html = job.jobDescription;
  const [editorState, seteditorState] = useState(htmlToDraft(html));
  const [recuirEditorState, setRecuirEditorState] = useState(htmlToDraft(html));

  const handleInputChange = (event: React.ChangeEvent<any>) => {
    const { name, value } = event.target;
    setJob({ ...job, [name]: value });
  };

  const saveJob = () => {
    setShowLoader(true);
    var data: IPostJobDetails = {
      jobTitle: job.jobTitle,
      seniorityLevelId: job.seniorityLevelId,
      employerTypeId: job.employerTypeId,
      jobDescription: job.jobDescription,
      industryId: job.industryId,
      jobLocationId: job.jobLocationId,
      jobNationalityId: job.jobNationalityId,
      requirements: job.requirements,
      uuId: props.match.params.id,
      jobTemplateId: props.match.params.tempId,
    };

    jobService.create(data).then(
      (response) => {
        history.push(
          "/employer/JobPostInstructions/" + response.data.data.uuid
        );
      },
      (error) => {
        alert("failed to add job");
        setShowLoader(true);
        //history.push('/employer/JobPostInstructions?uuid=sddsd');
      }
    );
  };

  // Validation
  const validationSchema = Yup.object().shape({
    jobTitle: Yup.string().required("Job Title is required"),

    //jobDescription: Yup.string().required("Job Description is required"),

    //requirements: Yup.string().required("Job requirements is required"),
  });

  // functions to build form returned by useForm() hook
  const { register, handleSubmit, reset, errors } = useForm({
    resolver: yupResolver(validationSchema),
  });

  function onSubmit(data: any) {
    debugger;
    if (job.jobDescription == "" && job.requirements == "") {
      if (job.jobDescription == "") {
        setDescriptionError(true);
      }
      if (job.jobDescription != "") {
        setDescriptionError(false);
      }
      if (job.requirements == "") {
        setRequiredError(true);
      }
      if (job.requirements != "") {
        setRequiredError(false);
      }
    } else {
      saveJob();
    }
  }
  const [scroll, setScroll] = useState(false);
  useEffect(() => {
    window.addEventListener("scroll", () => {
      setScroll(window.scrollY > 50);
    });
  }, []);

  const [value, setSetvalue] = useState("");

  function handleChangeSelect(event: React.ChangeEvent<HTMLInputElement>) {
    console.log("id is", event.target.id);
    setSetvalue(event.target.value);
  }

  // function selectValue(value: any, item: any) {
  //   setSetvalue(item.countryId);
  //   job.jobNationalityId = Number(item.countryId);
  //   setJob(job);
  // }

  // SET YOUR DEFAULT VALUE FOR COUNTRY HERE

  // const retval = [{countryId: job.jobNationalityId, name: currentCountries.find(m=> m.countryId == job.jobNationalityId).name}]

  const [
    selectedCountry,
    setSelectedCountry,
  ] = useState<IPostJobDetailsCountries>({
    countryId: 189,
    name: "Saudi Arabia",
  });

  // const [selectedOption, setselectedOption] = useState([selectedCountry] as IPostJobDetailsCountries[])

  function handleChange(selectedOption: any) {
    setSelectedCountry(selectedOption);
    //job.jobNationalityId = Number(selectedOption.countryId);
    job.jobNationalityId = selectedOption.countryId;
    setJob(job);
  }

  
  return (
    <React.Fragment>
      {/* <div>
        <img src={jobimg} />
      </div> */}

      <div
        className={
          scroll
            ? "filters-inbox job-details fixed-filter"
            : "filters-inbox job-details bg-white"
        }
      >
        <span className="plain active">{currentContent.Details}</span>
        <span className="plain">{currentContent.instructions}</span>
        <span className="plain">{currentContent.Objectives}</span>
        <span className="plain">{currentContent.Engagement}</span>
      </div>
      <div className="container">
        <form onSubmit={handleSubmit(onSubmit)} onReset={reset}>
          <div className="row">
            <div className="col-md-12">
              <div className="jb-box">
                <div className="bold-head" style={{ fontSize: "24px" }}>
                {currentContent.Details}
                  <div className="shortBorder-head"></div>
                </div>

                <div className="row">
                  <div className="form-group col-md-12">
                    <label>{currentContent.JobTitle}</label>

                    <input
                      name="jobTitle"
                      id="jobTitle"
                      value={job.jobTitle}
                      type="text"
                      ref={register}
                      className={`form-control ${
                        errors.jobTitle ? "is-invalid" : ""
                      }`}
                      onChange={handleInputChange}
                    />
                    <div className="invalid-feedback">
                      {errors.jobTitle?.message}
                    </div>
                  </div>

                  <div className="form-group col-md-6">
                    <label>{currentContent.SeniorityLevel}</label>
                    <select
                      onChange={handleInputChange}
                      value={job.seniorityLevelId}
                      className="form-control"
                      name="seniorityLevelId"
                      id="seniorityLevelId"
                    >
                      <option value="1">Student / Intern</option>

                      <option value="2">Entry Level</option>

                      <option value="3">Associate</option>

                      <option value="4">Mid-senior Level</option>

                      <option value="5">Director</option>

                      <option value="6">Executive</option>
                    </select>
                  </div>

                  <div className="form-group col-md-6">
                    <label>{currentContent.EmploymentType}</label>

                    <select
                      onChange={handleInputChange}
                      className="form-control"
                      value={job.employerTypeId}
                      name="employerTypeId"
                      id="employerTypeId"
                    >
                      <option value="1">Full-Time</option>

                      <option value="2">Part-Time</option>

                      <option value="3">Contract</option>

                      <option value="4">Temporary</option>

                      <option value="5">Volunteer</option>

                      <option value="6">Internship</option>

                      <option value="7">Per diem</option>

                      <option value="8">Other</option>
                    </select>
                  </div>

                  <div className="form-group col-md-12">
                    <label>{currentContent.JobDescription}</label>
                    {/* Visit  this package to check it's event and state change to get the output */}
                    {/* https://www.npmjs.com/package/react-wysiwyg-typescript */}
                    <Draft
                      editorState={editorState}
                      name="jobDescription"
                      id="jobDescription"
                      // value={job.firstMailDescription}
                      ref={register}
                      className={`form-control ${
                        errors.firstMailDescription ? "is-invalid" : ""
                      }`}
                      // onEditorStateChange={(editorState) => { seteditorState(editorState) }}
                      onEditorStateChange={(editorState) =>
                        handleDescDraftChange(editorState)
                      }
                    />
                    {/* <textarea
                      name="jobDescription"
                      id="jobDescription"
                      value={job.jobDescription}
                      ref={register}
                      style={{ height: 150 }}
                      className={`form-control ${
                        errors.jobDescription ? "is-invalid" : ""
                      }`}
                      onChange={handleInputChange}
                    /> */}
                    {descriptionError && (
                      <div className="invalid-feedback">
                        Job description is required
                      </div>
                    )}{" "}
                  </div>

                  <div className="form-group col-md-12">
                    <label>{currentContent.JobRequirements}</label>
                    <Draft
                      editorState={recuirEditorState}
                      name="requirements"
                      id="requirements"
                      // value={job.firstMailDescription}
                      ref={register}
                      className={`form-control ${
                        errors.firstMailDescription ? "is-invalid" : ""
                      }`}
                      // onEditorStateChange={(editorState) => { seteditorState(editorState) }}
                      onEditorStateChange={(recuirEditorState) =>
                        handleRecuirDraftChange(recuirEditorState)
                      }
                    />
                    {/* <textarea
                      name="requirements"
                      id="requirements"
                      value={job.requirements}
                      ref={register}
                      style={{ height: 150 }}
                      className={`form-control ${
                        errors.requirements ? "is-invalid" : ""
                      }`}
                      onChange={handleInputChange}
                    /> */}
                    {requiredError && (
                      <div className="invalid-feedback">
                        Job requirements are required
                      </div>
                    )}{" "}
                  </div>

                  <div className="form-group col-md-12">
                    <label>{currentContent.CompanyIndustry}</label>
                    <select
                      onChange={handleInputChange}
                      value={job.industryId}
                      className="form-control"
                      name="industryId"
                      id="industryId"
                    >
                      <option value="1">Business</option>
                      <option value="2">Tech</option>
                      <option value="3">Health care</option>
                      <option value="4">Admin & Sales</option>
                    </select>
                  </div>

                  <div className="form-group col-md-12">
                    <label>{currentContent.Location}</label>
                    <select
                      onChange={handleInputChange}
                      className="form-control"
                      value={job.jobLocationId}
                      name="jobLocationId"
                      id="jobLocationId"
                    >
                      <option value="1">On-site</option>
                      <option value="2">On-site and open to remote</option>
                      <option value="3">Remote Only</option>
                    </select>
                  </div>

                  <div className="form-group col-md-12">
                    <label>{currentContent.SelectCountry}</label>
                    <Select
                      value={selectedCountry}
                      onChange={handleChange}
                      options={currentCountries}
                      getOptionLabel={(option) => option.name}
                      getOptionValue={(option) => option.countryId.toString()}
                    />

                    {/* <div className='autocom'>
                      <ReactAutocomplete
                        wrapperStyle={{ display:'block'}}
                        menuStyle={{
                        position:'fixed',
                        zIndex:9999,
                        boxShadow: '0 2px 12px rgba(0, 0, 0, 0.1)',
                        height:'100px',
                        overflowY:'scroll',
                        display:'block',
                        }}
                        items={currentCountries}
                        shouldItemRender={(item: any, value: any) => item.name.toLowerCase().indexOf(value.toLowerCase()) > -1}
                        getItemValue={item => item.name}
                        renderItem={(item, highlighted) =>
                          <div
                            key={item.countryId}
                            style={{ width:'100%',backgroundColor: highlighted ? '#fff' : '#fff' }}
                          >
                            {item.name}
                            <span style={{display:'none'}} className={item.name} id={item.id}></span>
                          </div>
                        }
                        value={value}
                        onChange={(event) => handleChangeSelect(event)}
                        onSelect={(value, item) => selectValue(value, item)}
                      />
                    </div> */}
                  </div>
                </div>
              </div>

              <div className="btn-job">
                <div className="pull-left"></div>

                <div className="pull-right">
                  <button
                    onClick={() => {
                      history.push("/employer/dashboard");
                    }}
                    className="btn btn-default"
                    type="button"
                  >
                    {" "}
                    {currentContent.Back}{" "}
                  </button>
                  <button className="btn btn-approve" type="submit">
                    {showLoader === true ? (
                      <span className="loading">
                        <CircularProgress />
                      </span>
                    ) : (
                      ""
                    )}
                    {currentContent.Next}
                  </button>
                </div>
              </div>
            </div>
          </div>
        </form>
      </div>
    </React.Fragment>
  );
}
