import React, { useEffect, useState } from "react";
import { createStyles, makeStyles, Theme } from "@material-ui/core/styles";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import Grid from "@material-ui/core/Grid";
import thank from "../../../src/assets/thankful.png";
import { useForm, Controller } from "react-hook-form";

import PhoneInput from "react-phone-input-2";
import "react-phone-input-2/lib/style.css";

import PackageService from "../../services/PackageService";
import EmployerService from "../../services/EmployerService";
import UserService from "../../services/UserService";
import {IEmployer} from "../../models/IEmployer";

import { yupResolver } from "@hookform/resolvers/yup";
import { Dispatch } from "redux";
import { alertActions } from "../../actions/alertActions";
import { useDispatch } from "react-redux";
import * as Yup from "yup";
import { IResponseData } from '../../models/IResponseData';
import { IUpdatePassword } from "../../models/IUpdatePassword";


const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {},
  })
);

export default function EmployerProfile(props: any) {

  const classes = useStyles();
  
  const [currentTab, setCurrentTab] = useState(1);
  const [selectedPackage, setSelectedPackage] = useState(1);

  const toggleTab = (args:any) => {
    setCurrentTab(args);
  };

  const selectPackage = (args:any) => {
    setSelectedPackage(args);
    setPackagesError(false);
  };

  const initialPackageList = [
    {
      uuid: "",
      text: "",
      packageId: 0
    },
  ];

  const handleInputChange = (event: any) => {
    const { name, value } = event.target;
    setEmployerDetails({ ...employerDetails, [name]: value });
  };

  const handlePasswordInputChange = (event: any) => {
    debugger;
    const { name, value } = event.target;
    setUpdatePassword({ ...updatePassword, [name]: value });
  };

  const initialEmployerDetails: IEmployer = {
    companyName: "",
    firstName: "",
    lastName: "",
    phoneNumber: "",
    email: "",
    password: "",
    newPassword: "",
    confirmedPassword: "",
    packageId: 0,
    uuid: ""
  };

  const initialUpdatePassword: IUpdatePassword = {
    newPassword: "",
    password: "",
    uuid: "",
    confirmedPassword: "",
  };

  const [packageList, setPackageList] = React.useState(initialPackageList);

  const [allPricePackage, setAllPricePackage] = useState([]);

  const [employerDetails, setEmployerDetails] = useState(initialEmployerDetails);

  const [updatePassword, setUpdatePassword] = useState(initialUpdatePassword);

  const [packagesError, setPackagesError] = useState(false);
  
  const dispatch = useDispatch();

  // form validation rules
  const validationSchema = Yup.object().shape({
    companyName: Yup.string().trim().required("Company name is required"),
    firstName: Yup.string().trim().required("First name is required"),
    lastName: Yup.string().trim().required("Last name is required"),
    // phoneNumber: Yup.string()
    //   .trim()
    //   .required("Phone number is required")
    //   .matches(/^[0-9]+$/,"Phone number Must be only digits"),
    email: Yup.string()
      .trim()
      .required("Email is required")
      .email("Email is invalid"),
  });
debugger;
  const validationPasswordSchema = Yup.object().shape({
    password: Yup.string()
      .trim()
      .min(8, "Password must be at least 8 digits")
      .required("Old password is required"),
    newPassword: Yup.string()
      .trim()
      .min(8, "Password must be at least 8 digits")
      .required("New password is required"),
      confirmedPassword: Yup.string()
     .oneOf([Yup.ref('newPassword'), null], 'Two passwords must match')
  });


  // functions to build form returned by useForm() hook
  const { register, handleSubmit, reset, errors, control } = useForm({
    resolver: yupResolver(validationSchema),
  });

  const {
    register: register2,
    handleSubmit: handleSubmit2,
    reset: reset2,
    errors: errors2,
  } = useForm({
    resolver: yupResolver(validationPasswordSchema),
    mode: "onBlur",
  });

  function onSubmit(data: any) {
    debugger;
    UpdateEmployerDetails();
  }

  function onSubmit2(data: any) {
    debugger;
    UpdatePassword();
  }


  useEffect(() => {
    GetAllPackages();
    GetEmployerDetails();
  }, []);

  const GetAllPackages = () => {
    debugger;
    PackageService
      .GetAllPackages()
      .then((response) => {
        debugger;
        setAllPricePackage(response.data.data.packages);
        setPackageList(response.data.data.packagesFeatures);
      })
      .catch((e) => {
        console.log(e);
      });
  };

  const GetEmployerDetails = () => {
    debugger;
    EmployerService
      .GetEmployerDetalis()
      .then((response) => {
        debugger;
        setEmployerDetails(response.data.data);
        setSelectedPackage(response.data.data.packageId);
      })
      .catch((e) => {
        console.log(e);
      });
  };

  const UpdateEmployerDetails = () => {
    debugger;
    employerDetails.uuid = props.match.params.uuid;
    EmployerService
      .UpdateEmployerDetails(employerDetails)
        .then(
          user => { 
            debugger;
            GetEmployerDetails();
            let responseDetails : IResponseData  =  user.data;
            dispatch(
              alertActions.success(responseDetails.message));
          },
          error => {
            debugger;
            let responseDetails : IResponseData  =  error.response.data
            dispatch(alertActions.error(responseDetails.message));
          }
      );
  };

  const UpdatePassword = () => {
    debugger;
    updatePassword.uuid = props.match.params.uuid;
    UserService
      .UpdatePassword(updatePassword)
        .then(
          user => { 
            debugger;
            GetEmployerDetails();
            let responseDetails : IResponseData  =  user.data;
            dispatch(
              alertActions.success(responseDetails.message));
          },
          error => {
            debugger;
            let responseDetails : IResponseData  =  error.response.data
            dispatch(alertActions.error(responseDetails.message));
          }
      );
  };
 
  const UpdatePackage = () => {
    debugger;
    if(Number(selectedPackage) > 0){
      setPackagesError(false);
      employerDetails.uuid = props.match.params.uuid;
      employerDetails.packageId = selectedPackage;
      EmployerService
      .UpdatePackage(employerDetails)
        .then(
          user => { 
            debugger;
            GetEmployerDetails();
            let responseDetails : IResponseData  =  user.data;
            dispatch(
              alertActions.success(responseDetails.message));
          },
          error => {
            debugger;
            let responseDetails : IResponseData  =  error.response.data
            dispatch(alertActions.error(responseDetails.message));
          }
      );
    }

    else{
      setPackagesError(true);
    }
    
  };

  const drawPackages = allPricePackage.map((item) => (
    <div className='col-md-4'>
    <div className='packageBox'>
    <div className='packageUpper'>
    <div>{item.packageTitle}</div>
    <div className='package-price'>${item.packagePrice}</div>
    <div>per month, paid yearly</div>
    </div>
    <div className='packageBox-body'>
    <ul>
      
      {packageList.filter((temp) => temp.packageId == item.packageId
    )
    .map((k) => {
      return (
        <li>
          {k.text}
      </li>
      );
    })}
    </ul>
    </div>
    </div>
    
    <div
                onClick={() => selectPackage(item.packageId)}
                className={
                selectedPackage == item.packageId
                ? "selectPackageBtn activePackage"
                : "selectPackageBtn"
                }
              >
                
              </div>
              
    </div>

 ));

 function handleOnChange(
  value: any,
  data: any,
  event: any,
  formattedValue: any
) {
  console.log("value", value);
  employerDetails.phoneNumber = value;
  setEmployerDetails(employerDetails);
}

  return (
    <React.Fragment>
      <div className="container profilePage">
      <div className='row'>
      <div className='col-md-3'>
      <div className='tabBox'>
      <div
      onClick={() => toggleTab(1)}
      className={
        currentTab == 1
          ? "tabList activeTab"
          : "tabList"
      }
      
      >Basic Info</div>
      <div
      onClick={() => toggleTab(2)}
      className={
        currentTab == 2
          ? "tabList activeTab"
          : "tabList"
      }
      >Change password</div>
      <div
      onClick={() => toggleTab(3)}
      className={
        currentTab == 3
          ? "tabList activeTab"
          : "tabList"
      }
      >Change Your Package  </div>
      </div>
      </div>

      <div className='col-md-9'>
      <div className='tabContent'>

        <form
              onSubmit={handleSubmit(onSubmit)}
              onReset={reset}
            >
      {currentTab == 1 && 
      <div className='row'>
      <div className='form-group col-md-12'>
      <label>Company Name</label>
      <input
                  name="companyName"
                  id="companyName"
                  value={employerDetails.companyName}
                  type="text"
                  ref={register}
                  className={`form-control ${
                    errors.companyName ? "is-invalid" : ""
                  }`}
                  onChange={handleInputChange}
                />
                <div className="invalid-feedback">
                  {errors.companyName?.message}
                </div>
      </div>

      <div className='form-group col-md-6'>
      <label>First Name</label>
      <input
                  name="firstName"
                  id="firstName"
                  value={employerDetails.firstName}
                  type="text"
                  ref={register}
                  className={`form-control ${
                    errors.firstName ? "is-invalid" : ""
                  }`}
                  onChange={handleInputChange}
                />
                <div className="invalid-feedback">
                  {errors.firstName?.message}
                </div>
      </div>

      <div className='form-group col-md-6'>
      <label>Last Name</label>
      <input
                  name="lastName"
                  id="lastName"
                  value={employerDetails.lastName}
                  type="text"
                  ref={register}
                  className={`form-control ${
                    errors.lastName ? "is-invalid" : ""
                  }`}
                  onChange={handleInputChange}
                />
                <div className="invalid-feedback">
                  {errors.lastName?.message}
                </div>
      </div>

      <div className='form-group col-md-6'>
      <label>Phone Number</label>

      <Controller
                  name="phoneNumber"
                  control={control}
                  defaultValue=""
                  render={({ name, onBlur, onChange, value }) => (
                    <PhoneInput
                      value={employerDetails.phoneNumber}
                      onBlur={onBlur}
                      onChange={handleOnChange}
                      country={"sa"}
                      inputProps={{
                        name: "phoneNumber",
                        id: "phoneNumber",
                        required: true,
                        autoFocus: true,
                      }}
                    />
                  )}
                />
                <div className="invalid-feedback">
                  {errors.phoneNumber?.message}
                </div>
  
      </div>

      <div className='form-group col-md-6'>
      <label>Email Address</label>
      <input
                  name="email"
                  id="email"
                  value={employerDetails.email}
                  type="text"
                  ref={register}
                  className={`form-control ${errors.email ? "is-invalid" : ""}`}
                  onChange={handleInputChange}
                />
                <div className="invalid-feedback">{errors.email?.message}</div>
      </div>
      
      <div className='col-md-12 pull-right'>
      <button className='btn btn-approve'>Save Changes</button>
      </div>
      </div>


      }
</form>

<form className="" onSubmit={handleSubmit2(onSubmit2)}>
{currentTab == 2 && 
      <div className='row'>
      <div className='form-group col-md-12'>
      <label>Old Password</label>
      <input type='password'
     name="password"
     id="password"
     value={updatePassword.password}
     ref={register2}
     className={`form-control ${
       errors2.password ? "is-invalid" : ""
     }`}
     onChange={handlePasswordInputChange}
   />
   <div className="invalid-feedback">{errors2.password?.message}</div>
      </div>

      <div className='form-group col-md-6'>
      <label>New Password</label>
      <input
      name="newPassword"
      id="newPassword"
      value={updatePassword.newPassword}
      type="password"
      ref={register2}
      className={`form-control ${
        errors2.newPassword ? "is-invalid" : ""
      }`}
      onChange={handlePasswordInputChange}
    />
    <div className="invalid-feedback">{errors2.newPassword?.message}</div>
      </div>

      <div className='form-group col-md-6'>
      <label>Confirm New Password</label>
      <input
      name="confirmedPassword"
      id="confirmedPassword"
      value={updatePassword.confirmedPassword}
      type="password"
      ref={register2}
      className={`form-control ${
        errors2.confirmedPassword ? "is-invalid" : ""
      }`}
      onChange={handlePasswordInputChange}
    />
    <div className="invalid-feedback">{errors2.confirmedPassword?.message}</div>
      </div>
      <div className='col-md-12 pull-right'>
      <button className='btn btn-approve'>Save Changes</button>
      </div>

      </div>
      
      }
</form>
      
      {currentTab == 3 && 
              <div className='row'>
              {drawPackages}
              
      <div className='col-md-12 pull-right'>
      
      <button
       className='btn btn-approve'
       onClick={UpdatePackage}
       >Save Changes</button>
      </div>
      { packagesError == true ?<div className="invalid-feedback">
                  Select at least on package
                </div>:""
    }
              </div>
              
      }
      </div>
      </div>
      </div>

      </div>
    </React.Fragment>
  );
}
