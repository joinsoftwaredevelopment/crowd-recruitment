import React from "react";
import { createStyles, makeStyles, Theme } from "@material-ui/core/styles";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import Grid from "@material-ui/core/Grid";
import thank from "../../../src/assets/thankful.png";

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {},
  })
);

export default function EmployerThankYou() {
  localStorage.setItem("user", "");

  const classes = useStyles();

  let currentContent: any;

  let content = {
    English: {
      thankYou:"Thank You for applying with us!",
      WeSentMail: "We sent a mail to your mail, please visit it to activatate your account and to be able to book a meeting with us .",
    },
    Arabic: {
      thankYou:"شكرا لك علي التسجيل معنا",
      WeSentMail: "لقد راسلنا بريدك الإلكتروني الذي أدخلته في التسجيل كل ما عليك فعله ان تقوم بزيارة بريدك و تضغط علي الرابط المدرج فيه لتفعيل حسابك و تحجز مقابلة مع أحد ممثلينا الرائعين",
    },
  };

  localStorage.getItem("currentLanguage") === "Arabic"
    ? (currentContent = content.Arabic)
    : (currentContent = content.English);

  return (
    <React.Fragment>
      <div className="container">
        <div className="row">
          <div className="col-md-2"></div>
          <div className="col-md-8">
            <div className="auth-white-bg thankyou text-center thankyou-padding">
              <img style={{ margin: "20px 0" }} src={thank} />
              <div className="blackHead">{currentContent.thankYou}</div>
              <div className="shortBorder"></div>
              <p className="blurText">
              {currentContent.WeSentMail}
              </p>
            </div>
          </div>
          <div className="col-md-2"></div>
        </div>
      </div>
    </React.Fragment>
  );
}
