import React, { useEffect, useState } from "react";
import { createStyles, makeStyles, Theme } from "@material-ui/core/styles";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import Grid from "@material-ui/core/Grid";
import thank from "../../../src/assets/thankful.png";
import Select from "react-select";
import jobService from "../../services/JobService";
import { IJobIndustry } from "../../models/IJobIndustry";
import { IJobDetails } from "../../models/IJobDetails";
import { history } from "../../helpers/history";

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {},
  })
);
const options = [
  { value: "1", label: "UI/UX Designer" },
  { value: "2", label: "Android Developer" },
  { value: "3", label: "Hr Manager" },
];

export default function SelectTemplate() {
  const classes = useStyles();
  const [count, setCount] = useState(1);

  const [industries, setIndustries] = useState<IJobIndustry[]>([]);
  const [templates, setTemplates] = useState<any[]>([]);
  const [allTemplates, setAllTemplates] = useState<any[]>([]);
  const [currentTempId, setCurrentTempId] = useState(2);

  const [selectedTemplate, setSelectedTemplate] = useState<any>({
    jobTemplateId: 2,
    jobTitle: "Data Analyst / Data Scientist",
  });

  const [currentTemplate, setCurrentTemplate] = useState<IJobDetails>({
    uuid: "",
    jobTitle: "",
    employmentTypeName: "",
    seniorityLevelName: "",
    companiesNotToSourceFrom: "",
    languageId: 2,
    jobLocation: "",
    companyName: "",
    mustHaveQualification: "",
    niceToHaveQualification: "",
    hiringNeeds: "",
    experienceLevelName: "",
    description: "",
    companyIndustry: "",
    jobStatusId: 3,
    jobStatusName: "",
    requirements: "",
    noOfSubmissions: 0,
    jobDate: new Date(),
  });

  function handleChange(selectedOption: any) {
    debugger;
    setSelectedTemplate(selectedOption);
    setCurrentTempId(selectedOption.jobTemplateId);
    GetTemplateById(selectedOption.jobTemplateId);
  }

  useEffect(() => {
    GetIndustries();
    GetTemplatesByIndustry(1);
    GetTemplateById(2);
    GetAllJobTemps();
  }, []);

  const GetTemplatesByIndustry = (industryId: number) => {
    debugger;
    setCount(industryId);
    jobService
      .GetTemplatesByIndustry(industryId)
      .then((response) => {
        debugger;
        setTemplates(response.data.data);
      })
      .catch((e) => {
        console.log(e);
      });
  };

  const GetIndustries = () => {
    jobService
      .GetIndustries()
      .then((response) => {
        debugger;
        setIndustries(response.data.data);
      })
      .catch((e) => {
        console.log(e);
      });
  };

  const GetAllJobTemps = () => {
    jobService
      .GetAllJobTemps()
      .then((response) => {
        debugger;
        setAllTemplates(response.data.data);
      })
      .catch((e) => {
        console.log(e);
      });
  };

  const GetTemplateById = (templateId: number) => {
    debugger;
    setCurrentTempId(templateId);
    jobService
      .GetTemplateById(templateId)
      .then((response) => {
        debugger;
        setCurrentTemplate(response.data.data);
      })
      .catch((e) => {
        console.log(e);
      });
  };

  let currentContent: any;

  let content = {
    English: {
      Searchtemplate: "Search a template",
      Employmenttype: "Employment type",
      JobTitle: "JobTitle",
      Location: "Location",
      JobDescription: "Job Description",
      JobRequirements : "Job Requirements",
      Musthavequalifications: "Must have qualifications",
      Nicehavequalifications: "Nice to have qualifications",
      Back: "Back",
      Next: "Next",
    },
    Arabic: {
      Searchtemplate: "ابحث عن نموذج",
      Employmenttype: "نوع التوظيف",
      JobTitle: "عنوان الوظيفة",
      Location: "الموقع",
      JobDescription: "وصف الوظيفة",
      JobRequirements : "متطلبات الوظيفة",
      Musthavequalifications: "امكانات لابد من توافرها في المتقدم",
      Nicehavequalifications: "امكانيات من الجيد توافرها في المتقدم",
      Back: "العودة",
      Next: "متابعة",
    },
  };

  localStorage.getItem("currentLanguage") === "Arabic"
    ? (currentContent = content.Arabic)
    : (currentContent = content.English);

  return (
    <React.Fragment>
      <div className="container">
        <div className="sub-info-box selectTemplate">
          <div className="row">
            <div className="col-md-3">
              <div className="form-group col-md-12">
                <label>{currentContent.Searchtemplate}</label>
              
                <Select
                  options={allTemplates}
                  value={selectedTemplate}
                  getOptionLabel={(option) => option.jobTitle}
                  getOptionValue={(option) => option.jobTemplateId.toString()}
                  onChange={handleChange}
                />
              </div>

              <div className="form-group col-md-12 listing-content">
                {industries.map((m) => {
                  return (
                    <div className="parent-listing">
                      <div className="parent-listing-row">{m.translation}</div>

                      {allTemplates
                        .filter(
                          (temp) => temp.jobIndustryId == m.companyIndustryId
                        )
                        .map((k) => {
                          return (
                            <div
                              className="listing-content-row"
                              onClick={() => GetTemplateById(k.jobTemplateId)}
                            >
                              {k.jobTitle}
                            </div>
                          );
                        })}
                    </div>
                  );
                })}
              </div>
            </div>
            <div className="col-md-9">
              <div className="row">
                <div className="col-md-12">
                  <div className="sub-info-box">
                    <div className="feature-set">
                      <div className="row">
                        <div className="feature-set-row col-md-4">
                          <div className="bold-head">
                            <span>
                              <i className="fa fa-clock"></i>
                            </span>
                            <span>{currentContent.Employmenttype}</span>
                          </div>

                          <div className="text">
                            {currentTemplate.employmentTypeName}
                          </div>
                        </div>
                        <div className="feature-set-row col-md-4">
                          <div className="bold-head">
                            <span>
                              <i className="fa fa-clock"></i>
                            </span>
                            <span>{currentContent.JobTitle}</span>
                          </div>

                          <div className="text">{currentTemplate.jobTitle}</div>
                        </div>
                        {/* <div className="feature-set-row col-md-4">
                  <div className="bold-head">
                    <span>
                      <i className="fa fa-clock"></i>
                    </span>
                    <span>Seniority Level</span>
                  </div>

                  <div className="text">Dummy</div>
                </div> */}
                        {/* <div className="feature-set-row col-md-4">
                  <div className="bold-head">
                    <span>
                      <i className="fa fa-clock"></i>
                    </span>
                    <span>Company Name</span>
                  </div>

                  <div className="text">{currentTemplate.companyName}</div>
                </div> */}
                        <div className="feature-set-row col-md-4">
                          <div className="bold-head">
                            <span>
                              <i className="fa fa-clock"></i>
                            </span>
                            <span>{currentContent.Location}</span>
                          </div>

                          <div className="text">
                            {currentTemplate.jobLocation}
                          </div>
                        </div>
                        {/* <div className="feature-set-row col-md-4">
                  <div className="bold-head">
                    <span>
                      <i className="fa fa-clock"></i>
                    </span>
                    <span>Location To Source from</span>
                  </div>

                  <div className="text">Dummy</div>
                </div> */}
                      </div>
                    </div>

                    {/* <div className="feature-set-info">
              <div className="sub-feature">
                  <div className="bold-head">Job Function</div>

                  <div className="light-text">Dummy</div>
                </div>
              </div> */}

                    {/* <div className="feature-set-info">
              <div className="sub-feature">
                  <div className="bold-head">Company Industry</div>

                  <div className="light-text">Dummy</div>
                </div>
              </div> */}

                    <div className="feature-set-info">
                      <div className="sub-feature">
                        <div className="bold-head">{currentContent.JobDescription}</div>

                        <div
                          dangerouslySetInnerHTML={{
                            __html: currentTemplate.description,
                          }}
                        />
                      </div>
                    </div>

                    <div className="feature-set-info">
                      <div className="sub-feature">
                        <div className="bold-head">{currentContent.JobRequirements}</div>

                        <div
                          dangerouslySetInnerHTML={{
                            __html: currentTemplate.requirements,
                          }}
                        />
                      </div>
                    </div>

                    {/* <div className="feature-set-info">
              <div className="sub-feature">
                  <div className="bold-head">Work experience
</div>

                  <div className="light-text">Dummy</div>
                </div>
              </div> */}

                    <div className="feature-set-info">
                      <div className="sub-feature">
                        <div className="bold-head">
                        {currentContent.Musthavequalifications}
                        </div>

                        <div className="light-text">
                          {currentTemplate.mustHaveQualification}
                        </div>
                      </div>
                    </div>

                    <div className="feature-set-info">
                      <div className="sub-feature">
                        <div className="bold-head">
                        {currentContent.Nicehavequalifications}
                        </div>

                        <div className="light-text">
                          {currentTemplate.niceToHaveQualification}
                        </div>
                      </div>
                    </div>
                    {/* <div className="feature-set-info">
              <div className="sub-feature">
                  <div className="bold-head">Search Tips</div>

                  <div className="light-text">Dummy</div>
                </div>
              </div> */}
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className="btn-job">
          <div className="pull-right">
            <button
              className="btn btn-default"
              type="button"
              onClick={() => {
                history.push("/employer/dashboard");
              }}
            >
              {currentContent.Back}
            </button>
            <button
              className="btn btn-approve"
              type="submit"
              onClick={() => {
                history.push("/employer/JobPostDetails/0/" + currentTempId);
              }}
            >
              {currentContent.Next}
            </button>
          </div>
        </div>
      </div>
    </React.Fragment>
  );
}
