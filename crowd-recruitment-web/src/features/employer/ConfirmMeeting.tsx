import React, { useEffect } from 'react';
import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import Grid from '@material-ui/core/Grid';
import thank from '../../../src/assets/thankful.png'
import employerService from "../../services/EmployerService";
//import baloons from '../../../src/assets/baloons.png'
import { IBookMeetingModel } from '../../models/IBookMeetingModel';

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        root: {
        },
    }),
);

export default function ConfirmMeeting(props:any) {
    const classes = useStyles();
    const [currentMeeting, setCurrentMeeting] = React.useState<IBookMeetingModel>({
             email: '',
             interviewerName:'',
             mettingDate:'',
             timePeriod:'',
             timeZone:'',
             userName:'',
             uuid:'',
             zoomLink:''
    });

    useEffect(() => {
        getMeeting(props.match.params.id);
      });

      const getMeeting = (uuid:string) => {
        employerService.getMeeting(uuid)
          .then(response => {
            debugger;
            setCurrentMeeting(response.data);
          })
          .catch(e => {
            console.log(e);
          });
      };

    return (
        <React.Fragment>
        <div className='container'>
            <div className='row'>
                <div className='col-md-3'></div>
                <div className='col-md-6'>
               <div className='auth-white-bg thankyou text-center' style={{marginBottom:'30px'}}>
               <img style={{margin:'20px 0'}} src={""}/>
               <div className='blackHead'>
               Booking Confirmed
               </div>
               <p className='blurText'>
              You're booked with {currentMeeting.userName}. <br/>
              An invivtation has been emailed to you.

               </p>

               <div className='blackHead'>
              {currentMeeting.mettingDate}
              <br/>
              {currentMeeting.timePeriod}
               </div>

               
               </div>
                </div>
                <div className='col-md-3'></div>
            </div>
        </div>

    </React.Fragment>


    );
}
