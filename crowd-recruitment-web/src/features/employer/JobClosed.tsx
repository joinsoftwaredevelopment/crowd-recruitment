import React, { useState, useEffect } from "react";
import { createStyles, makeStyles, Theme } from "@material-ui/core/styles";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import Grid from "@material-ui/core/Grid";
import thank from "../../../src/assets/thankful.png";
import { Link, useHistory } from "react-router-dom";
import { history } from "../../helpers/history";
import { IJobsInfo } from "../../models/IJobsInfo";
import JobService from "../../services/JobService";
import empIco from "../../../src/assets/emp-ico.png";
import { jobStatusConstant } from "../../constants/jobStatusConstant";
import CircularProgress from "@material-ui/core/CircularProgress";
import Switch from "@material-ui/core/Switch";
import { IJobSummary } from "../../models/IJobSummary";

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {},
  })
);

export default function JobClosed() {
  const classes = useStyles();

  const [jobs, setJobs] = useState<IJobSummary[]>([]);
  const [showLoader, setShowLoader] = React.useState(false);

  useEffect(() => {
    setShowLoader(true);
    retrieveEmployers();
  }, []);

  const retrieveEmployers = () => {
    JobService.getallJobsClosedByEmp()
      .then((response) => {
        debugger;
        setJobs(response.data.data);
        setShowLoader(false);
      })
      .catch((e) => {
        console.log(e);
        setShowLoader(false);
      });
  };

  return (
    <React.Fragment>
      <div className="container emp-job">
        <div className="row">
          <div className="col-md-12">
            <div className="top-head">
              <div className="bold-head">My Jobs</div>
              <button
                onClick={() => {
                  history.push("/employer/JobPostDetails");
                }}
                className="btn btn-decline"
              >
                <i className="fa fa-plus"></i>
                Add Job
              </button>
            </div>
            <div className="job-bg">
              <div className="head-sec">
                <div className="mini-head">My Jobs</div>

                <div className="filter" style={{ paddingLeft: "70px" }}>
                  <span
                    className="draft-filter border-rad-L-6"
                    onClick={() => {
                      history.push("/employer/dashboard/0");
                    }}
                  >
                    Active
                  </span>
                  <span className="non-active-filter highlight-filter">
                    Closed
                  </span>
                  <span
                    onClick={() => {
                      history.push("/employer/JobDrafts");
                    }}
                    className="non-active-filter border-rad-R-6"
                  >
                    Draft
                  </span>
                </div>

                <div className="filter">
                  {/* <span className='active-filter highlight-filter'>My Jobs</span>
                    <span className='non-active-filter'>All Jobs</span> */}
                </div>
              </div>

              <div className="row empJB">
                {showLoader === true ? <CircularProgress /> : ""}
                {jobs.map((m) => {
                  return (
                    <div className="col-md-4">
                      <div className="job-box" key={m.uuid}>
                        <div className="switchBox">
                          <div className="pull-left">
                            <span>
                              Job status :{" "}
                              <span className="red">{m.jobStatusName}</span>
                            </span>
                          </div>

                          <div className="pull-right">
                            <Switch
                              id=""
                              key=""
                              checked={false}
                              color="primary"
                              name=""
                              inputProps={{ "aria-label": "primary checkbox" }}
                            />
                          </div>
                        </div>
                        <div className="remain-img-box">
                          <img src={empIco} />
                        </div>

                        <div className="data-card-head">{m.jobTitle}</div>
                        <div
                          style={{ position: "relative" }}
                          className="data-card-light"
                        >
                          {m.seniorityLevelName}
                        </div>
                        <div
                          style={{ position: "relative" }}
                          className="data-card-light"
                        >
                          {m.jobLocation}
                        </div>
                        <div
                          className="data-card-light"
                          style={{ position: "relative" }}
                        >
                          <img className="roundImg" src={thank} />

                          {m.countryName}
                          <Link
                            className="detail-link"
                            to={"/employer/JobPreview/" + m.uuid}
                          >
                            Show Details
                          </Link>
                        </div>

                        <div className="job-card-btm">
                          <div className="data-card-light">
                            {m.jobDate}
                            {/* <Link to={"/employer/JobPreview/" + m.uuid}>
                              Details
                            </Link> */}
                          </div>
                          <div className="data-card-light-green">
                            <a
                              hidden={m.jobStatusId != jobStatusConstant.Opened}
                              href={"/candidate/candidates/" + m.uuid}
                            >
                              View Candidates
                            </a>
                            {/* <span
                              hidden={
                                m.jobStatusId != jobStatusConstant.Declined
                              }
                              className="red"
                              onClick={() => handleClickOpen(m.reason)}
                            >
                              View Reason
                            </span> */}
                          </div>
                        </div>
                      </div>
                    </div>
                  );
                })}
              </div>
            </div>
          </div>
        </div>
      </div>
    </React.Fragment>
  );
}
