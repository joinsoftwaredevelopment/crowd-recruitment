import React, { useEffect, useState } from "react";
import {
  createStyles,
  Theme,
  withStyles,
  WithStyles,
  makeStyles,
} from "@material-ui/core/styles";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import Grid from "@material-ui/core/Grid";
import jobimg from "../../../src/assets/job-bg.png";
import qMark from "../../../src/assets/question-mark.png";
import Draft, {
  htmlToDraft,
  EmptyState,
  rawToDraft,
  draftToRaw,
  draftStateToHTML,
} from "react-wysiwyg-typescript";
import { stateToHTML } from "draft-js-export-html";

import { EditorState, convertToRaw, ContentState } from "draft-js";

import draftToHtml from "draftjs-to-html";

import { IPostJobEngagment } from "../../models/IPostJobEngagment";
import { IJobDetails } from "../../models/IJobDetails";
import jobService from "../../services/JobService";

import { useForm } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import * as Yup from "yup";
import { useDispatch, useSelector } from "react-redux";
import { history } from "../../helpers/history";
import { create } from "yup/lib/array";
import employerService from "../../services/EmployerService";
import { CircularProgress } from "@material-ui/core";

import IconButton from "@material-ui/core/IconButton";
import CloseIcon from "@material-ui/icons/Close";
import Typography from "@material-ui/core/Typography";

import Dialog from "@material-ui/core/Dialog";
import MuiDialogTitle from "@material-ui/core/DialogTitle";
import MuiDialogContent from "@material-ui/core/DialogContent";
import MuiDialogActions from "@material-ui/core/DialogActions";

const styles = (theme: Theme) =>
  createStyles({
    root: {
      margin: 0,
      padding: theme.spacing(2),
    },
    closeButton: {
      position: "absolute",
      right: theme.spacing(1),
      top: theme.spacing(1),
      color: theme.palette.grey[500],
    },
  });

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {},
  })
);

export interface DialogTitleProps extends WithStyles<typeof styles> {
  id: string;
  children: React.ReactNode;
  onClose: () => void;
}

const DialogTitle = withStyles(styles)((props: DialogTitleProps) => {
  const { children, classes, onClose, ...other } = props;
  return (
    <MuiDialogTitle disableTypography className={classes.root} {...other}>
      <Typography variant="h6">{children}</Typography>
      {onClose ? (
        <IconButton
          aria-label="close"
          className={classes.closeButton}
          onClick={onClose}
        >
          <CloseIcon />
        </IconButton>
      ) : null}
    </MuiDialogTitle>
  );
});

const DialogContent = withStyles((theme: Theme) => ({
  root: {
    padding: theme.spacing(2),
  },
}))(MuiDialogContent);

export interface DialogTitleProps extends WithStyles<typeof styles> {
  id: string;
  children: React.ReactNode;
  onClose: () => void;
}

const DialogActions = withStyles((theme: Theme) => ({
  root: {
    margin: 0,
    padding: theme.spacing(1),
  },
}))(MuiDialogActions);

export default function JobPostEngagment(props: any) {
  const initialTitle = [
    {
      id: 0,
      emailTitleEn: "",
      emailtitleAr: "",
      emailbodyEn: "",
      emailbodyAr: "",
    },
  ];

  const [templates, setTemplates] = React.useState(initialTitle);
  const [value, setValue] = React.useState();
  const [open, setOpen] = React.useState(false);
  const [openDetails, setopenDetails] = React.useState(false);

  const [descriptionError, setDescriptionError] = useState(false);
  const [titleError, setTitleError] = useState(false);

  const initialJobState = {
    uuid: "",
    firstMailSubject: "",
    firstMailDescription: "",
    engagementTemplateId: 0,
  };

  const dispatch = useDispatch();

  const [jobDetails, setjobDetails] = useState<IJobDetails>({
    uuid: "",
    jobTitle: "",
    employmentTypeName: "",
    seniorityLevelName: "",
    companiesNotToSourceFrom: "",
    languageId: 2,
    jobLocation: "",
    companyName: "",
    mustHaveQualification: "",
    niceToHaveQualification: "",
    hiringNeeds: "",
    experienceLevelName: "",
    description: "",
    companyIndustry: "",
    jobStatusId: 2,
    jobStatusName: "",
    requirements: "",
    noOfSubmissions: 0,
    jobDate: new Date(),
  });

  const getJobDetails = () => {
    debugger;
    jobService
      .getJobDetails(props.match.params.id)
      .then((response) => {
        debugger;
        setjobDetails(response.data.data);
      })
      .catch((e) => {
        console.log(e);
      });
  };

  const [job, setJob] = useState(initialJobState);
  // TO SET A DEFAULT HTML
  const html = "";
  const [editorState, seteditorState] = useState(htmlToDraft(html));
  const [openReason, setOpenReason] = useState(false);

  const handleClickOpenReason = () => {
    debugger;
    setOpenReason(true);
    handleClickOpen();
  };

  const handleClickOpen = () => {
    debugger;

    setopenDetails(true);
    getJobDetails();
  };

  const handleCloseReason = () => {
    setOpenReason(false);
  };
  // TO SET A DEFAULT HTML

  const [showLoader, setShowLoader] = React.useState(false);

  useEffect(() => {
    retrieveJob(props.match.params.id);
  }, []);

  const handleClose = () => {
    setOpen(false);
  };
  const handleOpen = () => {
    debugger;
    if (job.firstMailSubject == "" || job.firstMailDescription == "") {
      if (job.firstMailSubject == "") {
        setTitleError(true);
      }
      if (job.firstMailSubject != "") {
        setTitleError(false);
      }
      if (job.firstMailDescription == "") {
        setDescriptionError(true);
      }
      if (job.firstMailDescription != "") {
        setDescriptionError(false);
      }
    } else {
      setOpen(true);
    }
  };

  const retrieveJob = (uuid: string) => {
    debugger;
    employerService
      .GetJobEngagementsInfo(uuid)
      .then((response) => {
        debugger;
        if (response.data.data.job.firstMailSubject != null) {
          debugger;

          seteditorState(
            htmlToDraft(response.data.data.job.firstMailDescription)
          );

          setJob(response.data.data.job);
        }
        setTemplates(response.data.data.templates);
      })
      .catch((e) => {
        console.log(e);
      });
  };

  const handleInputChange = (event: any) => {
    const { name, value } = event.target;
    setJob({ ...job, [name]: value });

    if (job.firstMailSubject == "") {
      setTitleError(true);
    }
    if (job.firstMailSubject != "") {
      setTitleError(false);
    }
  };

  const handlePreviewClose = () => {
    setopenDetails(false);
  };

  const handleDraftChange = (draftText: any) => {
    seteditorState(draftText);
    // SAVING HTML TO SERVER
    const convertedHtml = draftToHtml(
      convertToRaw(editorState.getCurrentContent())
    );
    // const html = draftToHtml(draftText)
    job.firstMailDescription = convertedHtml;
    setJob(job);

    if (job.firstMailDescription == "") {
      setDescriptionError(true);
    }
    if (job.firstMailDescription != "") {
      setDescriptionError(false);
    }
  };

  const handleTemplateChange = (event: React.ChangeEvent<any>) => {
    const { name, value } = event.target;
    setValue({ ...value, [name]: value });
    debugger;
    if (value != "0") {
      const currentTemplate = templates.find((m) => m.id == value);
      job.firstMailSubject = currentTemplate.emailTitleEn;
      job.firstMailDescription = currentTemplate.emailbodyEn;
      job.engagementTemplateId = currentTemplate.id;
      seteditorState(htmlToDraft(currentTemplate.emailbodyEn));
    } else {
      job.firstMailSubject = "";
      job.firstMailDescription = "";
      seteditorState(htmlToDraft(""));
      job.engagementTemplateId = 0;
    }

    setJob(job);

    if (job.firstMailSubject == "" || job.firstMailDescription == "") {
      if (job.firstMailSubject == "") {
        setTitleError(true);
      }
      if (job.firstMailSubject != "") {
        setTitleError(false);
      }
      if (job.firstMailDescription == "") {
        setDescriptionError(true);
      }
      if (job.firstMailDescription != "") {
        setDescriptionError(false);
      }
    } else {
      setTitleError(false);
      setDescriptionError(false);
    }
  };

  const saveJob = (statusId: number) => {
    setShowLoader(true);
    var data: IPostJobEngagment = {
      uuid: props.match.params.id,
      firstMailSubject: job.firstMailSubject,
      firstMailDescription: job.firstMailDescription,
      engagementTemplateId: job.engagementTemplateId,
      statusId: statusId,
    };

    jobService.createEngagment(data).then(
      (response) => {
        history.push("/employer/dashboard");
      },
      (error) => {
        alert("failed to add job instructions");
        setShowLoader(false);
      }
    );
  };

  // Validation
  const validationSchema = Yup.object().shape({
    //firstMailSubject: Yup.string().required("FirstMailSubject is required"),
    //firstMailDescription: Yup.string().required(
    // "FirstMailDescription is required"
    //),
  });

  // functions to build form returned by useForm() hook
  const { register, handleSubmit, reset, errors } = useForm({
    resolver: yupResolver(validationSchema),
  });

  function onSubmit(data: any) {
    debugger;
    saveJob(2);
  }
  const [scroll, setScroll] = useState(false);
  useEffect(() => {
    window.addEventListener("scroll", () => {
      setScroll(window.scrollY > 50);
    });
  }, []);

  let currentContent: any;

  let content = {
    English: {
      Details: "Details",
      instructions: "Instructions",
      Objectives: "Objectives",
      Engagement: "Engagement",
      SelectTemplate: "Select Template",
      Firstemailsubject: "First email subject",
      Firstemaildescription: "First email description",
      Preview: "Preview",
      Saveasdraft: "Save as draft",
      Postjob: "Post job",
      Youcanchoosefromourtemplates:
        "You can choose from our templates by clicking here",
      JobTitle: "Job title",
      SeniorityLevel: "Seniority level",
      EmploymentType: "Employment type",
      JobDescription: "Job description",
      JobRequirements: "Job requirements",
      CompanyIndustry: "Company industry",
      companyName: "company name",
      Location: "Location",
      Experience: "Experience",
      Companiesnottosourcefrom: "Companies not to source from",
      Musthavequalifications: "Must have qualifications",
      Nicehavequalifications: "Nice to have qualifications",
      PostingConfirmation: "Posting Confirmation",
      Areyousureyouwanttoadd: "Are you sure you want to add this job post ?",
      Cancel: "Cancel",
      Next: "Next",
      Back: "Back to my jobs",
    },
    Arabic: {
      Details: "التفاصيل",
      instructions: "التعليمات",
      Objectives: "الأهداف",
      Engagement: "الإلتزامات",
      SelectTemplate: "اختر النموذج",
      Firstemailsubject: "عنوان أول بريد إلكتروني",
      Firstemaildescription: "وصف أول بريد إلكتروني",
      Preview: "مراجعة",
      Saveasdraft: "الحفظ كمهملة",
      Postjob: "إضافة الوظيفة",
      Youcanchoosefromourtemplates:
        "يمكنك الإختيار من نماذجنا لملأ هذه الخانات",
      JobTitle: "لقب الوظيفة",
      SeniorityLevel: "الخبرة",
      EmploymentType: "نوع التوظيف",
      JobDescription: "وصف الوظيفة",
      JobRequirements: "متطلبات الوظيفة",
      CompanyIndustry: "مجال عمل الشركة",
      companyName: "اسم الشركة",
      Location: "مكان العمل",
      Experience: "الخبرة",
      Companiesnottosourcefrom: "شركات غير مرغوبة في التعامل معها",
      Musthavequalifications: "امكانات لابد من توافرها في المتقدم",
      Nicehavequalifications: "امكانيات من الجيد توافرها في المتقدم",
      PostingConfirmation: "تأكيد الإضافة",
      Areyousureyouwanttoadd: "هل انت متأكد من إضافة هذه الوظيفة ؟",
      Cancel: "إلغاء",
      Next: "التالي",
      Back: "العودة إلي وظائفي",
    },
  };

  localStorage.getItem("currentLanguage") === "Arabic"
    ? (currentContent = content.Arabic)
    : (currentContent = content.English);

  return (
    <React.Fragment>
      {/* <div>
                <img src={jobimg} />
            </div> */}

      <div
        className={
          scroll
            ? "filters-inbox job-details fixed-filter"
            : "filters-inbox job-details bg-white"
        }
      >
        <span
          onClick={() => {
            history.push(
              "/employer/JobPostDetails/" + props.match.params.id + "/0"
            );
          }}
          className="plain"
        >
          {currentContent.Details}
        </span>
        <span
          onClick={() => {
            history.push(
              "/employer/JobPostInstructions/" + props.match.params.id
            );
          }}
          className="plain"
        >
          {currentContent.instructions}
        </span>
        <span
          onClick={() => {
            history.push("/employer/JobPostObjective/" + props.match.params.id);
          }}
          className="plain"
        >
          {currentContent.Objectives}
        </span>
        <span className="plain active">{currentContent.Engagement}</span>
      </div>
      <div className="container">
        <form onSubmit={handleSubmit(onSubmit)} onReset={reset}>
          <div className="row">
            <div className="col-md-12">
              <div className="jb-box">
                <div className="bold-head" style={{ fontSize: "24px" }}>
                  {currentContent.Engagement}
                  <div className="shortBorder-head"></div>
                </div>

                <div className="row">
                  <div className="form-group col-md-12">
                    <label>{currentContent.SelectTemplate}</label>
                    <select
                      value={job.engagementTemplateId}
                      className="form-control"
                      onChange={handleTemplateChange}
                    >
                      <option key="0" value="0">
                        {currentContent.Youcanchoosefromourtemplates}
                      </option>

                      {templates.map((item) => (
                        <option key={item.id} value={item.id}>
                          {item.emailTitleEn}
                        </option>
                      ))}
                    </select>
                  </div>

                  <div className="form-group col-md-12">
                    <label>{currentContent.Firstemailsubject}</label>
                    <input
                      name="firstMailSubject"
                      id="firstMailSubject"
                      value={job.firstMailSubject}
                      //disabled={true}
                      type="text"
                      ref={register}
                      className={`form-control ${
                        errors.firstMailSubject ? "is-invalid" : ""
                      }`}
                      onChange={handleInputChange}
                    />
                    {titleError && (
                      <div className="invalid-feedback">
                        First mail subject is required
                      </div>
                    )}{" "}
                  </div>

                  <div className="form-group col-md-12">
                    <label>{currentContent.Firstemaildescription}</label>
                    {/* Visit  this package to check it's event and state change to get the output */}
                    {/* https://www.npmjs.com/package/react-wysiwyg-typescript */}
                    <Draft
                      //readOnly={true}
                      editorState={editorState}
                      name="firstMailDescription"
                      id="firstMailDescription"
                      // value={job.firstMailDescription}
                      ref={register}
                      className={`form-control ${
                        errors.firstMailDescription ? "is-invalid" : ""
                      }`}
                      // onEditorStateChange={(editorState) => { seteditorState(editorState) }}
                      onEditorStateChange={(editorState) =>
                        handleDraftChange(editorState)
                      }
                    />
                    {/* GET HTML FROM EDITOR */}
                    {/* <textarea
                     disabled
                     style={{height:'300px',width:'100%'}}
                     value={draftToHtml(convertToRaw(editorState.getCurrentContent()))}
                       /> */}
                    {/* <textarea
                      name="firstMailDescription"
                      id="firstMailDescription"
                      value={job.firstMailDescription}
                      ref={register}
                      style={{ height: 200 }}
                      className={`form-control ${
                        errors.firstMailDescription ? "is-invalid" : ""
                      }`}
                      onChange={handleInputChange}
                    /> */}
                    {descriptionError && (
                      <div className="invalid-feedback">
                        First mail description is required
                      </div>
                    )}{" "}
                  </div>
                </div>
              </div>

              <div className="btn-job">
                <div className="pull-left">
                  <button
                    onClick={() => {
                      history.push("/employer/dashboard");
                    }}
                    className="btn btn-default"
                    type="button"
                  >
                    {" "}
                    {currentContent.Back}{" "}
                  </button>
                </div>

                <div className="pull-right">
                  <button
                    type="button"
                    className="btn btn-decline"
                    onClick={() => handleClickOpenReason()}
                  >
                    {currentContent.Preview}
                  </button>

                  <button
                    style={{ marginLeft: "10px" }}
                    type="button"
                    className="btn btn-decline"
                    onClick={() => saveJob(1)}
                  >
                    {currentContent.Saveasdraft}
                  </button>

                  <button
                    type="button"
                    onClick={handleOpen}
                    className="btn btn-approve"
                  >
                    {currentContent.Postjob}
                  </button>
                </div>
              </div>
            </div>
          </div>
        </form>

        <Dialog
          fullWidth={true}
          maxWidth={"lg"}
          onClose={handlePreviewClose}
          aria-labelledby="customized-dialog-title"
          open={openDetails}
        >
          <DialogTitle
            id="customized-dialog-title4"
            onClose={handlePreviewClose}
          >
            {jobDetails.jobTitle}
          </DialogTitle>

          <DialogContent dividers>
            <div className="row">
              <div className="col-md-12">
                <div className="sub-info-box" style={{ boxShadow: "none" }}>
                  <div className="feature-set">
                    <div className="feature-set-row">
                      <div className="bold-head">
                        <span>
                          <i className="fa fa-clock"></i>
                        </span>
                        <span>{currentContent.EmploymentType}</span>
                      </div>

                      <div className="text">
                        {jobDetails.employmentTypeName}
                      </div>
                    </div>

                    <div className="feature-set-row">
                      <div className="bold-head">
                        <span>
                          <i className="fa fa-clock"></i>
                        </span>
                        <span>{currentContent.SeniorityLevel}</span>
                      </div>

                      <div className="text">
                        {jobDetails.seniorityLevelName}
                      </div>
                    </div>

                    <div className="feature-set-row">
                      <div className="bold-head">
                        <span>
                          <i className="fa fa-clock"></i>
                        </span>
                        <span>{currentContent.companyName}</span>
                      </div>

                      <div className="text">{jobDetails.companyName}</div>
                    </div>

                    <div className="feature-set-row">
                      <div className="bold-head">
                        <span>
                          <i className="fa fa-clock"></i>
                        </span>
                        <span>{currentContent.Location}</span>
                      </div>

                      <div className="text">{jobDetails.jobLocation}</div>
                    </div>
                  </div>

                  <div className="feature-set-info">
                    <div className="sub-feature">
                      <div className="bold-head">{currentContent.JobTitle}</div>

                      <div className="light-text">{jobDetails.jobTitle}</div>
                    </div>
                  </div>

                  <div className="feature-set-info">
                    <div className="sub-feature">
                      <div className="bold-head">
                        {currentContent.CompanyIndustry}
                      </div>

                      <div className="light-text">
                        {jobDetails.companyIndustry}
                      </div>
                    </div>
                  </div>

                  <div className="feature-set-info">
                    <div className="sub-feature">
                      <div className="bold-head">
                        {currentContent.JobDescription}
                      </div>
                      <div className="appColor">
                        <div
                          dangerouslySetInnerHTML={{
                            __html: jobDetails.description,
                          }}
                        />
                      </div>
                    </div>
                  </div>

                  <div className="feature-set-info">
                    <div className="sub-feature">
                      <div className="bold-head">
                        {currentContent.JobRequirements}
                      </div>

                      <div className="appColor">
                        <div
                          dangerouslySetInnerHTML={{
                            __html: jobDetails.requirements,
                          }}
                        />
                      </div>
                    </div>
                  </div>

                  <div className="feature-set-info">
                    <div className="sub-feature">
                      <div className="bold-head">
                        {currentContent.Experience}
                      </div>

                      <div className="light-text">
                        {jobDetails.experienceLevelName}
                      </div>
                    </div>
                  </div>

                  <div className="feature-set-info">
                    <div className="sub-feature">
                      <div className="bold-head">
                        {currentContent.Musthavequalifications}
                      </div>

                      <div className="light-text">
                        {jobDetails.mustHaveQualification}
                      </div>
                    </div>
                  </div>

                  <div className="feature-set-info">
                    <div className="sub-feature">
                      <div className="bold-head">
                        {currentContent.Nicehavequalifications}
                      </div>

                      <div className="light-text">
                        {jobDetails.niceToHaveQualification}
                      </div>
                    </div>
                  </div>

                  <div className="feature-set-info">
                    <div className="sub-feature">
                      <div className="bold-head">
                        {currentContent.Companiesnottosourcefrom}
                      </div>

                      <div className="light-text">
                        {jobDetails.companiesNotToSourceFrom}
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </DialogContent>
        </Dialog>
      </div>

      <Dialog
        fullWidth={true}
        maxWidth={"sm"}
        onClose={handleClose}
        aria-labelledby="customized-dialog-title"
        open={open}
      >
        <DialogTitle id="customized-dialog-title3" onClose={handleClose}>
          {currentContent.PostingConfirmation}
        </DialogTitle>
        <DialogContent className="text-center" dividers>
          <img style={{ marginTop: "20px" }} src={qMark} />

          <div className="head-job-pop">
            <div className="blackHead">
              {currentContent.Areyousureyouwanttoadd}
            </div>
            <div className="shortBorder"></div>
          </div>

          <div className="text-center job-pop-btns">
            <button onClick={handleClose} className="btn btn-decline">
              {currentContent.Cancel}
            </button>
            <button onClick={() => saveJob(2)} className="btn btn-approve">
              {showLoader === true ? (
                <span className="loading">
                  <CircularProgress />
                </span>
              ) : (
                ""
              )}
              {currentContent.Postjob}
            </button>
          </div>
        </DialogContent>
      </Dialog>
    </React.Fragment>
  );
}
