import React, { useState, useEffect } from "react";
import {
  createStyles,
  Theme,
  withStyles,
  WithStyles,
  makeStyles,
} from "@material-ui/core/styles";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import Grid from "@material-ui/core/Grid";
import thank from "../../../src/assets/thankful.png";
import { Link, useHistory } from "react-router-dom";
import { history } from "../../helpers/history";
import { IJobsInfo } from "../../models/IJobsInfo";
import JobService from "../../services/JobService";
import empIco from "../../../src/assets/emp-ico.png";
import Switch from "@material-ui/core/Switch";
import { ICloseJob } from "../../models/ICloseJob";
import jobService from "../../services/JobService";
import { jobStatusConstant } from "../../constants/jobStatusConstant";
import decline from "../../../src/assets/decline.png";
import fill1 from "../../../src/assets/fill-1.png";
import fill2 from "../../../src/assets/fill-2.png";
import CircularProgress from "@material-ui/core/CircularProgress";

import IconButton from "@material-ui/core/IconButton";
import CloseIcon from "@material-ui/icons/Close";
import Typography from "@material-ui/core/Typography";

import Dialog from "@material-ui/core/Dialog";
import MuiDialogTitle from "@material-ui/core/DialogTitle";
import MuiDialogContent from "@material-ui/core/DialogContent";
import MuiDialogActions from "@material-ui/core/DialogActions";
import { IJobSummary } from "../../models/IJobSummary";
import { userStatusConstant } from "../../constants/userStatusConstant";
import cross from "../../../src/assets/cross.png";

const styles = (theme: Theme) =>
  createStyles({
    root: {
      margin: 0,
      padding: theme.spacing(2),
    },
    closeButton: {
      position: "absolute",
      right: theme.spacing(1),
      top: theme.spacing(1),
      color: theme.palette.grey[500],
    },
  });

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {},
  })
);

export default function EmployerDashboard() {
  const classes = useStyles();
  const [openCloseJob, setOpenCloseJob] = React.useState(false);
  const [jobs, setJobs] = useState<IJobSummary[]>([]);
  const [state, setState] = React.useState(true);
  const [isReject, isRejectState] = React.useState(true);
  const [open, setOpen] = React.useState(false);
  const [fillopen, setfillOpen] = React.useState(false);
  const [rejectionReason, setRejectionReason] = React.useState("");
  const [rejectID, setRejectID] = React.useState("");
  const [fillBoxState, setfillBoxState] = React.useState(null);

  const [userStatus, setSetUserStatus] = useState(1);

  const handleChange = (
    uuid: string,
    event: React.ChangeEvent<HTMLInputElement>
  ) => {
    debugger;
    setOpenCloseJob(true);
    setRejectID(uuid);
    var x = document.getElementById(uuid).parentNode as HTMLElement;
    var y = x.parentNode as HTMLElement;
    y.classList.remove("Mui-checked");
  };

  const handleClosedChange = (
    uuid: string,
    event: React.ChangeEvent<HTMLInputElement>
  ) => {
    debugger;
  };

  const CloseJob = () => {
    debugger;
    const currentJob: ICloseJob = {
      uuId: rejectID,
    };
    jobService
      .CloseJob(currentJob)
      .then((response) => {
        retrieveActiveJobs();
        handleCloseReason();
      })
      .catch((e) => {
        console.log(e);
      });
  };

  const rejectFunc = () => {
    CloseJob();
    setOpenCloseJob(false);
  };

  const handleClickOpen = (reason: any) => {
    setOpen(true);
    setRejectionReason(reason);
  };
  const handleClose = () => {
    setOpen(false);
    setfillOpen(false);
  };

  const handleCloseReason = () => {
    var x = document.getElementById(rejectID).parentNode as HTMLElement;
    var y = x.parentNode as HTMLElement;
    y.classList.add("Mui-checked");
    setOpenCloseJob(false);
  };

  const [showLoader, setShowLoader] = React.useState(false);
  const [deletedID, setDeletedID] = React.useState("");
  const [draftOpen, setDraftOpen] = React.useState(false);

  const handleDraftOpen = (uuid: string) => {
    debugger;
    setDeletedID(uuid);
    setDraftOpen(true);
  };

  const handleDraftClose = () => {
    setDraftOpen(false);
  };

  const DeleteJob = () => {
    debugger;
    JobService.DeleteJob(deletedID)
      .then((response) => {
        retrieveDraftedJobs();
        handleDraftClose();
      })
      .catch((e) => {
        console.log(e);
      });
  };

  useEffect(() => {
    setShowLoader(true);
    retrieveActiveJobs();
  }, []);

  const retrieveActiveJobs = () => {
    JobService.getallJobsSummariesByEmp()
      .then((response) => {
        setSetUserStatus(1);
        debugger;
        setJobs(response.data.data);
        setShowLoader(false);
      })
      .catch((e) => {
        console.log(e);
        setShowLoader(false);
      });
  };

  const retrieveDraftedJobs = () => {
    JobService.getallJobsDraftsByEmp()
      .then((response) => {
        setSetUserStatus(2);
        debugger;
        setJobs(response.data.data);
        setShowLoader(false);
      })
      .catch((e) => {
        console.log(e);
        setShowLoader(false);
      });
  };

  const retrieveClosedJobs = () => {
    JobService.getallJobsClosedByEmp()
      .then((response) => {
        setSetUserStatus(3);
        debugger;
        setJobs(response.data.data);
        setShowLoader(false);
      })
      .catch((e) => {
        console.log(e);
        setShowLoader(false);
      });
  };

  const chooseFill = (args: any) => {
    setfillBoxState(args);
    if (args == "2") {
      history.push("/employer/JobPostDetails/0/0");
    } else {
      history.push("/employer/SelectTemplate");
    }
  };

  let currentContent: any;

  let content = {
    English: {
      AddJob: "Add new job",
      Active: "Active",
      Closed: "Closed",
      Draft: "Draft",
      MyJobs: "My Jobs",
      JobStatus: "Job status",
      SowDetails: "Show details",
      ViewReason: "View rejection reason",
      ViewCandidates: "View candidates",
      Wearesorrytoinform: "We are sorry to inform you that we had declined",
      nextReason: "your job post for that reason",
      Ok: "Ok",
      CloseJob: "Close Job",
      Areyousure: "Are you sure you want to close this job?",
      Yes: "Yes",
      No: "No",
      Fillingjoboptions: "Filling job options",
      Choosehowtofill: "Choose how to fill your job post",
      ChooseFromLibrary: "Choose From our templates",
      FillDataFromScratch: "Fill Data From Scratch",
      Areyousureyouwanttodelete: "Are you sure you want to delete this job ?",
      Cancel: "Cancel",
      Delete: "Delete",
      DeleteConfirmation: "Delete confirmation",
    },
    Arabic: {
      AddJob: "إضافة وظيفة جديدة",
      Active: "مفتوحة",
      Closed: "مغلقة",
      Draft: "مهملة",
      MyJobs: "وظائفي",
      JobStatus: "حالة الوظيفة",
      SowDetails: "اظهار التفاصيل",
      ViewReason: "اظهار سبب الرفض",
      ViewCandidates: "اظهار المتقدمين",
      Wearesorrytoinform: "متأسفون لإبلاغكم بأنه تم رفض طلب ",
      nextReason: "وظيفتكم للسبب التالي",
      Ok: "موافق",
      CloseJob: "اغلاق الوظيفة",
      Areyousure: "هل متأكد من إغلاق الوظيفة ؟",
      Yes: "نعم",
      No: "لا",
      Fillingjoboptions: "خيارات اضافة الوظيفة",
      Choosehowtofill: "اختر أي طريقة تفضل لإضافة وظيفتك",
      ChooseFromLibrary: "الاختيار من نماذجنا المتعددة",
      FillDataFromScratch: "انشاء الوظيفة من الصفر",
      Areyousureyouwanttodelete: "هل متأكد من حذف هذه الوظيفة ؟",
      Cancel: "إلغاء",
      Delete: "حذف",
      DeleteConfirmation: "تأكيد الحذف",
    },
  };

  localStorage.getItem("currentLanguage") === "Arabic"
    ? (currentContent = content.Arabic)
    : (currentContent = content.English);

  return (
    <React.Fragment>
      <div className="container emp-job">
        <div className="row">
          <div className="col-md-12">
            <div className="top-head">
              <div className="bold-head">{currentContent.MyJobs}</div>
              <button
                onClick={() => {
                  setfillOpen(true);
                }}
                className="btn btn-decline"
              >
                <i className="fa fa-plus"></i>
                {currentContent.AddJob}
              </button>
            </div>
            <div className="job-bg">
              <div className="head-sec">
                <div className="mini-head"> {currentContent.MyJobs}</div>

                <div className="filter" style={{ paddingLeft: "70px" }}>
                  {/* <span className="active-filter border-rad-L-6 highlight-filter">
                    Active
                  </span>
                  <span
                    className="draft-filter"
                    onClick={() => {
                      history.push("/employer/JobClosed");
                    }}
                  >
                    Closed
                  </span>
                  <span
                    onClick={() => {
                      history.push("/employer/JobDrafts");
                    }}
                    className="non-active-filter border-rad-R-6"
                  >
                    Draft
                  </span>
                 */}
                  <span
                    id=""
                    onClick={retrieveActiveJobs}
                    className={
                      userStatus == userStatusConstant.Request
                        ? "active-filter highlight-filter border-rad-L-6"
                        : "non-active-filter border-rad-L-6"
                    }
                  >
                    {currentContent.Active}
                  </span>

                  <span
                    id=""
                    onClick={retrieveClosedJobs}
                    className={
                      userStatus == userStatusConstant.Declined
                        ? "active-filter highlight-filter"
                        : "non-active-filter"
                    }
                  >
                    {currentContent.Closed}
                  </span>

                  <span
                    id=""
                    onClick={retrieveDraftedJobs}
                    className={
                      userStatus == userStatusConstant.Approved
                        ? "active-filter highlight-filter border-rad-R-6"
                        : "non-active-filter border-rad-R-6"
                    }
                  >
                    {currentContent.Draft}
                  </span>
                </div>

                <div className="filter">
                  {/* <span className='active-filter highlight-filter'>My Jobs</span> */}
                  {/* <span className='non-active-filter'>All Jobs</span> */}
                </div>
              </div>

              <div className="row empJB employerDashboard">
                {showLoader === true ? <CircularProgress /> : ""}
                {jobs.map((m) => {
                  return (
                    <div
                      className="col-md-4"
                      // onClick={() => {
                      //   history.push("/employer/JobPreview/" + m.uuid);
                      // }}
                    >
                      <div className="job-box" key={m.uuid}>
                        <div className="switchBox">
                          <div className="pull-left">
                            <span>
                              {currentContent.JobStatus} :{" "}
                              <span
                                className={
                                  m.jobStatusId == jobStatusConstant.Closed ||
                                  m.jobStatusId == jobStatusConstant.Declined
                                    ? "red"
                                    : "green"
                                }
                              >
                                {m.jobStatusId == jobStatusConstant.Declined
                                  ? "Rejected"
                                  : m.jobStatusName}
                              </span>
                            </span>
                          </div>
                          <div className="pull-right">
                            {m.jobStatusId == jobStatusConstant.Draft ? (
                              <i
                                onClick={() => handleDraftOpen(m.uuid)}
                                className="far fa-times-circle crossIco"
                              ></i>
                            ) : (
                              <Switch
                                id={m.uuid}
                                key={m.uuid}
                                checked={
                                  m.jobStatusId == jobStatusConstant.Closed
                                    ? false
                                    : true
                                }
                                onChange={
                                  m.jobStatusId == jobStatusConstant.Posted ||
                                  m.jobStatusId == jobStatusConstant.Opened
                                    ? (event) => handleChange(m.uuid, event)
                                    : (event) =>
                                        handleClosedChange(m.uuid, event)
                                }
                                color="primary"
                                name={m.uuid}
                                inputProps={{
                                  "aria-label": "primary checkbox",
                                }}
                              />
                            )}
                          </div>
                        </div>
                        <div className="remain-img-box">
                          <img src={empIco} />
                        </div>

                        <div className="data-card-head">
                          {m.jobTitle} ({m.seniorityLevelName})
                        </div>

                        <div
                          style={{ position: "relative" }}
                          className="data-card-light"
                        >
                          {m.jobLocation}
                        </div>

                        <div
                          style={{ position: "relative" }}
                          className="data-card-light"
                        >
                          <img className="roundImg" src={thank} />

                          {m.countryName}

                          <Link
                            className="detail-link"
                            to={
                              m.jobStatusId == jobStatusConstant.Draft
                                ? "/employer/JobPostDetails/" + m.uuid + "/0"
                                : "/employer/JobPreview/" + m.uuid
                            }
                          >
                            {currentContent.ShowDetails}
                          </Link>
                          <br />
                          <a
                            hidden={m.jobStatusId != jobStatusConstant.Declined}
                            className="red"
                            onClick={() => handleClickOpen(m.rejectionReason)}
                          >
                            {currentContent.ViewReason}
                          </a>

                          {m.jobStatusId != jobStatusConstant.Draft &&
                          m.jobStatusId != jobStatusConstant.Declined ? (
                            <Link to={"/employer/JobPreview/" + m.uuid}>
                              Details
                            </Link>
                          ) : (
                            ""
                          )}

                          {/* {m.jobStatusId == jobStatusConstant.Declined
                                ? <Link style={{marginLeft:"12px"}}
                                to={"/employer/JobPostDetails/" + m.uuid+"/0"}>
                               Continue sourcing
                             </Link>
                               : ""
                                
                            } */}
                        </div>

                        <div className="job-card-btm">
                          <div className="data-card-light">{m.jobDate}</div>

                          {m.jobStatusId == jobStatusConstant.Draft ||
                          m.jobStatusId == jobStatusConstant.Declined ? (
                            <div
                              style={{ color: "#0195ff", textAlign: "right" }}
                              key={m.uuid}
                              onClick={() => {
                                history.push(
                                  "/employer/JobPostDetails/" + m.uuid + "/0"
                                );
                              }}
                            >
                              Continue sourcing
                            </div>
                          ) : (
                            ""
                          )}

                          <div
                            className="data-card-light"
                            onClick={() => {
                              history.push("/employer/JobPreview/" + m.uuid);
                            }}
                          ></div>
                          <div className="data-card-light-green">
                             {m.submissonCount > 0
                                ? <a
                                  hidden={m.jobStatusId != jobStatusConstant.Opened && m.jobStatusId != jobStatusConstant.Closed}
                                  href={"/candidate/jobBoard/" + m.uuid}
                                >
                                  {currentContent.ViewCandidates}
                                </a>
                               : ""    
                           }
                          </div>
                        </div>
                      </div>
                    </div>
                  );
                })}
              </div>
            </div>
          </div>
        </div>
      </div>

      <Dialog
        fullWidth={true}
        maxWidth={"sm"}
        onClose={handleCloseReason}
        aria-labelledby="customized-dialog-title"
        open={openCloseJob}
      >
        <DialogTitle id="customized-dialog-title3" onClose={handleCloseReason}>
          {currentContent.CloseJob}
        </DialogTitle>

        <DialogContent className="text-center" dividers>
          <img style={{ marginTop: "20px" }} src={decline} />

          <div className="head-job-pop">
            <div className="blackHead">{currentContent.Areyousure}</div>
          </div>
          <div className="text-center job-pop-btns">
            <button onClick={handleCloseReason} className="btn btn-decline">
              {currentContent.No}
            </button>
            <button onClick={CloseJob} className="btn btn-approve">
              {currentContent.Yes}
            </button>
          </div>
        </DialogContent>
      </Dialog>

      <Dialog
        fullWidth={true}
        maxWidth={"sm"}
        onClose={handleClose}
        aria-labelledby="customized-dialog-title"
        open={open}
      >
        <DialogTitle id="customized-dialog-title3" onClose={handleClose}>
          Reason
        </DialogTitle>
        <DialogContent className="text-center" dividers>
          <img style={{ marginTop: "20px" }} src={decline} />

          <div className="head-job-pop">
            <div className="blackHead">
              {currentContent.Wearesorrytoinform} <br />
              {currentContent.nextReason}
            </div>
            <div className="shortBorder"></div>
          </div>

          <p className="blurText">{rejectionReason}</p>

          <div className="text-center job-pop-btns">
            <button onClick={handleClose} className="btn btn-approve">
              {currentContent.Ok}
            </button>
          </div>
        </DialogContent>
      </Dialog>

      <Dialog
        fullWidth={true}
        maxWidth={"sm"}
        onClose={handleClose}
        aria-labelledby="customized-dialog-title"
        open={fillopen}
      >
        <DialogTitle id="customized-dialog-title3" onClose={handleClose}>
          {currentContent.Fillingjoboptions}
        </DialogTitle>
        <DialogContent className="text-center" dividers>
          <div className="head-job-pop">
            <div className="blackHead">{currentContent.Choosehowtofill}</div>
            <div className="shortBorder"></div>
          </div>

          <div
            style={{ marginBottom: "30px" }}
            className={fillBoxState == 1 ? "fillBox activeF" : "fillBox"}
            onClick={() => chooseFill("1")}
          >
            <img style={{ marginTop: "20px" }} src={fill1} />
            <div>{currentContent.ChooseFromLibrary}</div>
          </div>

          <div
            className={fillBoxState == 2 ? "fillBox activeF" : "fillBox"}
            onClick={() => chooseFill("2")}
          >
            <img style={{ marginTop: "20px" }} src={fill2} />
            <div>{currentContent.FillDataFromScratch}</div>
          </div>

          {/* <div className="text-center job-pop-btns">
            <button onClick={handleClose} className="btn btn-approve">
              OK
            </button>
          </div> */}
        </DialogContent>
      </Dialog>
      <Dialog
        fullWidth={true}
        maxWidth={"sm"}
        onClose={handleDraftClose}
        aria-labelledby="customized-dialog-title"
        open={draftOpen}
      >
        <DialogTitle id="customized-dialog-title3" onClose={handleDraftClose}>
          {currentContent.DeleteConfirmation}
        </DialogTitle>
        <DialogContent className="text-center" dividers>
          <img style={{ marginTop: "20px" }} src={cross} />

          <div className="head-job-pop">
            <div className="blackHead">
              {currentContent.Areyousureyouwanttodelete}
            </div>
            <div className="shortBorder"></div>
          </div>

          <div className="text-center job-pop-btns">
            <button onClick={handleDraftClose} className="btn btn-decline">
              {currentContent.Cancel}
            </button>
            <button onClick={DeleteJob} className="btn btn-approve">
              {currentContent.Delete}
            </button>
          </div>
        </DialogContent>
      </Dialog>
    </React.Fragment>
  );
}

export interface DialogTitleProps extends WithStyles<typeof styles> {
  id: string;
  children: React.ReactNode;
  onClose: () => void;
}

const DialogTitle = withStyles(styles)((props: DialogTitleProps) => {
  const { children, classes, onClose, ...other } = props;
  return (
    <MuiDialogTitle disableTypography className={classes.root} {...other}>
      <Typography variant="h6">{children}</Typography>
      {onClose ? (
        <IconButton
          aria-label="close"
          className={classes.closeButton}
          onClick={onClose}
        >
          <CloseIcon />
        </IconButton>
      ) : null}
    </MuiDialogTitle>
  );
});

const DialogContent = withStyles((theme: Theme) => ({
  root: {
    padding: theme.spacing(2),
  },
}))(MuiDialogContent);
