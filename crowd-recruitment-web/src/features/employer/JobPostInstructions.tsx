import React, { useEffect, useState } from "react";
import {
  createStyles,
  Theme,
  withStyles,
  WithStyles,
  makeStyles,
} from "@material-ui/core/styles";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import Grid from "@material-ui/core/Grid";
import jobimg from "../../../src/assets/job-bg.png";
import Autocomplete from "@material-ui/lab/Autocomplete";

import { IPostJobInstructions } from "../../models/IPostJobInstructions";
import { ICurrency } from "../../models/ICurrency";
import { IJobDetails } from "../../models/IJobDetails";
import { IMustSkills } from "../../models/IMustSkills";
import jobService from "../../services/JobService";
import { useForm } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import * as Yup from "yup";
import { useDispatch, useSelector } from "react-redux";
import { history } from "../../helpers/history";
import { create } from "yup/lib/array";
import employerService from "../../services/EmployerService";
import CircularProgress from "@material-ui/core/CircularProgress";
import { v4 as uuidv4 } from "uuid";

const initialMustList = [
  {
    id: "",
    name: "",
  },
];

const initialNiceList = [
  {
    id: "",
    name: "",
  },
];

const initialSourceList = [
  {
    id: "",
    name: "",
  },
];

export default function JobPostInstructions(props: any) {
  const [mustList, setMustList] = React.useState(initialMustList);
  const [niceList, setNiceList] = React.useState(initialNiceList);
  const [notSourceList, setNotSource] = React.useState(initialSourceList);
  const [name, setName] = React.useState("");
  const [niceName, setNiceName] = React.useState("");
  const [sourcename, setSourceName] = React.useState("");
  const [mustShown, setMustShown] = React.useState("none");
  const [niceShown, setNiceShown] = React.useState("none");

  const [showLoader, setShowLoader] = React.useState(false);

  const initialJobState = {
    uuid: "",
    experienceLevelId: 1,
    minSalary: 0,
    maxSalary: 0,
    currencyId: 122,
  };

  const dispatch = useDispatch();

  const [job, setJob] = useState(initialJobState);

  useEffect(() => {
    retrieveJob(props.match.params.id);
    getCurrencies();
  }, []);

  const retrieveJob = (uuid: string) => {
    debugger;
    employerService
      .GetJobInstructionsInfo(uuid)
      .then((response) => {
        debugger;
        setJob(response.data.data.job);
        setMustList(response.data.data.mustHaveSkillsVM);
        setNiceList(response.data.data.niceHaveSkillsVM);
        setNotSource(response.data.data.notToSourceVM);
      })
      .catch((e) => {
        console.log(e);
      });
  };

  const [currentCurrencies, setCurrentCurrencies] = useState<ICurrency[]>([]);

  const handleInputChange = (event: React.ChangeEvent<any>) => {
    const { name, value } = event.target;
    setJob({ ...job, [name]: value });
  };

  const getCurrencies = () => {
    debugger;
    jobService
      .getAllCurrencies()
      .then((response) => {
        debugger;
        setCurrentCurrencies(response.data.data);
      })
      .catch((e) => {
        console.log(e);
      });
  };

  const saveJob = () => {
    debugger;
    var data: IPostJobInstructions = {
      uuid: props.match.params.id,
      experienceLevelId: job.experienceLevelId,
      currencyId: job.currencyId,
      minSalary: job.minSalary,
      maxSalary: job.maxSalary,
      mustHaveSkills: mustList,
      niceHaveQualification: niceList,
      notToSource: notSourceList,
    };

    if (mustList.length > 0 && niceList.length > 0) {
      setShowLoader(true);
      jobService.createInstructions(data).then(
        (response) => {
          history.push("/employer/JobPostObjective/" + response.data.data.uuid);
        },
        (error) => {
          alert("failed to add job instructions");
          setShowLoader(false);
          //history.push('/employer/JobPostInstructions?uuid=sddsd');
        }
      );
    } else {
      if (mustList.length == 0) {
        setMustShown("inline");
      }

      if (niceList.length == 0) {
        setNiceShown("inline");
      }
    }
  };

  // Validation
  const validationSchema = Yup.object().shape({
    minSalary: Yup.number().min(0),
    maxSalary: Yup.number()
      .min(0)
      .when("minSalary", (minSalary: number, schema: any) => {
        return schema.test({
          test: (maxSalary: number) => maxSalary >= minSalary,
          message: "max salary should be greater than min salary",
        });
      }),
  });

  // functions to build form returned by useForm() hook
  const { register, handleSubmit, reset, errors } = useForm({
    resolver: yupResolver(validationSchema),
  });

  function onSubmit(data: any) {
    debugger;
    saveJob();
  }

  const useStyles = makeStyles((theme: Theme) =>
    createStyles({
      root: {
        width: "100%",
        "& > * + *": {
          marginTop: theme.spacing(3),
        },
      },
    })
  );

  const classes = useStyles();
  // Top 100 films as rated by IMDb users. http://www.imdb.com/chart/top
  const top100Films = [
    { title: "The Shawshank Redemption", year: 1994 },
    { title: "The Godfather", year: 1972 },
  ];

  function handleChange(event: any) {
    // track input field's state
    setName(event.target.value);
  }

  function handleAddMust() {
    // add item
    if (name != "") {
      const newList = mustList.concat({ name: name, id: uuidv4() });
      setMustList(newList);
      setMustShown("none");
      setName("");
    }
  }

  function handleNiceChange(event: any) {
    debugger;
    // track input field's state
    setNiceName(event.target.value);
    setNiceShown("none");
  }

  function handleNiceAdd() {
    debugger;
    // add item
    if (niceName != "") {
      const newList = niceList.concat({ name: niceName, id: uuidv4() });

      setNiceList(newList);
      setNiceName("");
    }
  }

  function handleSourceChange(event: any) {
    // track input field's state
    setSourceName(event.target.value);
  }

  function handleSourceAdd() {
    // add item

    if (sourcename != "") {
      const newList = notSourceList.concat({ name: sourcename, id: uuidv4() });

      setNotSource(newList);
      setSourceName("");
    }
  }

  function DeleteMustSkill(id: string) {
    // add item
    debugger;
    const newList = mustList.filter((m) => m.id != id);

    setMustList(newList);
  }

  function DeleteNiceSkill(id: string) {
    // add item
    debugger;
    const newList = niceList.filter((m) => m.id != id);

    setNiceList(newList);
  }

  function DeleteNotSourcrSkill(id: string) {
    // add item
    debugger;
    const newList = notSourceList.filter((m) => m.id != id);

    setNotSource(newList);
  }
  const [scroll, setScroll] = useState(false);
  useEffect(() => {
    window.addEventListener("scroll", () => {
      setScroll(window.scrollY > 50);
    });
  }, []);


  let currentContent: any;

  let content = {
    English: {
      Details: "Details",
      instructions: "Instructions",
      Objectives: "Objectives",
      Engagement: "Engagement",
      MinSalary: "Min Salary",
      MaxSalary: "Max Salary",
      Currency: "Currency",
      Experience: "Experience",
      Companiesnottosourcefrom: "Companies not to source from",
      Musthavequalifications: "Must have qualifications",
      Nicehavequalifications: "Nice to have qualifications",
      Next: "Next",
      Back: "Back to my jobs",
      Atleastonequalification: "Add at least one qualification"
    },
    Arabic: {
      Details: "التفاصيل",
      instructions: "التعليمات",
      Objectives: "الأهداف",
      Engagement: "الإلتزامات",
      MinSalary: "الحد الأدني للمرتب",
      MaxSalary: "الحد الأقصي للمرتب",
      Currency: "العملة",
      Experience: "الخبرة",
      Companiesnottosourcefrom: "شركات غير مرغوبة في التعامل معها",
      Musthavequalifications: "امكانات لابد من توافرها في المتقدم",
      Nicehavequalifications: "امكانيات من الجيد توافرها في المتقدم",
      Next: "التالي",
      Back: "العودة إلي وظائفي",
      Atleastonequalification: "اختر علي الأقل امكانية واحدة"
    },
  };

  localStorage.getItem("currentLanguage") === "Arabic"
    ? (currentContent = content.Arabic)
    : (currentContent = content.English);


  return (
    <React.Fragment>
      {/* <div>
                <img src={jobimg} />
            </div> */}

      <div
        className={
          scroll
            ? "filters-inbox job-details fixed-filter"
            : "filters-inbox job-details bg-white"
        }
      >
        <span
          onClick={() => {
            history.push("/employer/JobPostDetails/" + props.match.params.id+"/0");
          }}
          className="plain"
        >
          {currentContent.Details}
        </span>
        <span className="plain active">{currentContent.instructions}</span>
        <span className="plain">{currentContent.Objectives}</span>
        <span className="plain">{currentContent.Engagement}</span>
      </div>
      <div className="container">
        <form onSubmit={handleSubmit(onSubmit)} onReset={reset}>
          <div className="row">
            <div className="col-md-12">
              <div className="jb-box">
                <div className="bold-head" style={{ fontSize: "24px" }}>
                {currentContent.instructions}
                  <div className="shortBorder-head"></div>
                </div>

                <div className="row">
                  <div className="form-group col-md-4">
                    <label>{currentContent.MinSalary}</label>
                    <input
                      name="minSalary"
                      id="minSalary"
                      value={job.minSalary}
                      type="Number"
                      min="0"
                      ref={register}
                      className={`form-control ${
                        errors.minSalary ? "is-invalid" : ""
                      }`}
                      onChange={handleInputChange}
                    />
                    <div className="invalid-feedback">
                      {errors.minSalary?.message}
                    </div>
                  </div>

                  <div className="form-group col-md-4">
                    <label>{currentContent.MaxSalary}</label>
                    <input
                      name="maxSalary"
                      id="maxSalary"
                      value={job.maxSalary}
                      type="Number"
                      min="0"
                      ref={register}
                      className={`form-control ${
                        errors.maxSalary ? "is-invalid" : ""
                      }`}
                      onChange={handleInputChange}
                    />
                    <div className="invalid-feedback">
                      {errors.maxSalary?.message}
                    </div>
                  </div>

                  <div className="form-group col-md-4">
                    <label>{currentContent.Currency}</label>
                    <select
                      onChange={handleInputChange}
                      className="form-control"
                      name="currencyId"
                      id="currencyId"
                      value={String(job.currencyId)}
                    >
                      {currentCurrencies.map((item) => (
                        <option value={item.currencyId}>
                          {item.currencyName}
                        </option>
                      ))}
                    </select>
                  </div>
                </div>

                <div className="row">
                  <div className="form-group col-md-12">
                    <label>{currentContent.Experience}</label>
                    <select
                      onChange={handleInputChange}
                      className="form-control"
                      name="experienceLevelId"
                      id="experienceLevelId"
                      value={job.experienceLevelId}
                    >
                      <option value="1">0 - 2</option>

                      <option value="2">3 - 5</option>

                      <option value="3">6 - 10</option>

                      <option value="4">10 +</option>
                    </select>
                  </div>

                  {/* <div className='form-group col-md-12'>
                                     <label>Must-have qualifications</label>
                                    <div className={classes.root}>
                                        <Autocomplete
                                            multiple
                                            limitTags={2}
                                            id="multiple-limit-tags"
                                            options={top100Films}
                                            getOptionLabel={(option:any) => option.title}
                                          renderInput={(params:any) => (
                                          <TextField {...params} className='form-control' placeholder="E.g problem solving" />
                                            )}
                                        />
                                    
                                </div>
                                    </div>
 */}
                  <div className="form-group col-md-12 tag-Input">
                    <label>{currentContent.Musthavequalifications}</label>

                    <input
                      className="form-control"
                      value={name}
                      onChange={handleChange}
                      type="text"
                    />
                    <span onClick={handleAddMust}>
                      <i className="fa fa-plus add-tag"></i>
                    </span>

                    <ul className="tag-output">
                      {mustList.map((item) => (
                        <li key={item.id}>
                          {item.name}
                          <span>
                            <i
                              className="fa fa-times close-tag"
                              onClick={() => DeleteMustSkill(item.id)}
                            ></i>
                          </span>
                        </li>
                      ))}
                    </ul>

                    <div
                      className="invalid-feedback"
                      style={{ display: mustShown }}
                    >
                      {currentContent.Atleastonequalification}
                    </div>
                    {/* <button onClick={() => handleRemoveMust(item.id)}>-</button> */}

                    {/* <input 
                                    name="mustHaveQualification"
                 id="mustHaveQualification" 
                 value={job.mustHaveQualification}
                 type="text"
                 ref={register} 
                 className={`form-control ${errors.mustHaveQualification ? 'is-invalid' : ''}`}
                onChange={handleInputChange}
                                    />

<div className="invalid-feedback">{errors.mustHaveQualification?.message}
                                    </div> */}
                  </div>

                  <div className="form-group col-md-12 tag-Input">
                    <label>{currentContent.Nicehavequalifications}</label>
                    <input
                      className="form-control"
                      value={niceName}
                      onChange={handleNiceChange}
                      type="text"
                    />
                    <span onClick={handleNiceAdd}>
                      <i className="fa fa-plus add-tag"></i>
                    </span>

                    <ul className="tag-output">
                      {niceList.map((item) => (
                        <li key={item.id}>
                          {item.name}
                          <span>
                            <i
                              className="fa fa-times close-tag"
                              onClick={() => DeleteNiceSkill(item.id)}
                            ></i>
                          </span>
                        </li>
                      ))}
                    </ul>

                    <div
                      className="invalid-feedback"
                      style={{ display: niceShown }}
                    >
                      {currentContent.Atleastonequalification}
                    </div>
                  </div>

                  <div className="form-group col-md-12 tag-Input">
                    <label>{currentContent.Companiesnottosourcefrom}</label>
                    <input
                      className="form-control"
                      value={sourcename}
                      onChange={handleSourceChange}
                      type="text"
                    />
                    <span onClick={handleSourceAdd}>
                      <i className="fa fa-plus add-tag"></i>
                    </span>

                    <ul className="tag-output">
                      {notSourceList.map((item) => (
                        <li key={item.id}>
                          {item.name}
                          <span>
                            <i
                              className="fa fa-times close-tag"
                              onClick={() => DeleteNotSourcrSkill(item.id)}
                            ></i>
                          </span>
                        </li>
                      ))}
                    </ul>
                  </div>
                </div>
              </div>

              <div className="btn-job">
                <div className="pull-left"></div>

                <div className="pull-right">
                  <button
                    onClick={() => {
                      history.push("/employer/dashboard");
                    }}
                    className="btn btn-default"
                    type="button"
                  >
                    {" "}
                    {currentContent.Back}{" "}
                  </button>

                  <button className="btn btn-approve">
                    {showLoader === true ? (
                      <span className="loading">
                        <CircularProgress />
                      </span>
                    ) : (
                      ""
                    )}
                    {currentContent.Next}
                  </button>
                </div>
              </div>
            </div>
          </div>
        </form>
      </div>
    </React.Fragment>
  );
}
