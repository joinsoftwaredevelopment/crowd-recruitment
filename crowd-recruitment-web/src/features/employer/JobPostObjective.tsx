import React, { useEffect, useState } from "react";
import {
  createStyles,
  Theme,
  withStyles,
  WithStyles,
} from "@material-ui/core/styles";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import Grid from "@material-ui/core/Grid";
import jobimg from "../../../src/assets/job-bg.png";
import mag from "../../../src/assets/mag.png";
import arrowIcon from "../../../src/assets/arrow-icon.png";
import percent from "../../../src/assets/percentage.png";
import people from "../../../src/assets/meeting.png";
import comment from "../../../src/assets/conversation.png";
import person from "../../../src/assets/standing-up-man-.png";

import { IEmployerObjectives } from "../../models/IEmployerObjectives";
import jobService from "../../services/JobService";
import { useForm } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import * as Yup from "yup";
import { useDispatch, useSelector } from "react-redux";
import { history } from "../../helpers/history";
import { create } from "yup/lib/array";
import employerService from "../../services/EmployerService";
import { idText } from "typescript";
import { CircularProgress } from "@material-ui/core";

export default function JobPostObjective(props: any) {
  const [count, setCount] = useState("1check");

  const initialJobState = {
    UUID: "",
    isContinuousHire: false,
    peopleToHireCount: 1,
    whenYouNeedToHire: 1,
    sourcingOutsideOfJoin: true,
    findingJobDifficultyLevelId: 1,
  };

  const dispatch = useDispatch();

  const [job, setJob] = useState(initialJobState);

  const [showLoader, setShowLoader] = React.useState(false);

  useEffect(() => {
    retrieveJob(props.match.params.id);
  }, []);

  const retrieveJob = (uuid: string) => {
    debugger;
    employerService
      .GetJobInfo(uuid)
      .then((response) => {
        debugger;
        setJob(response.data.data);

        if (response.data.data.findingJobDifficultyLevelId == 1) {
          setCount("1check");
        }

        if (response.data.data.findingJobDifficultyLevelId == 2) {
          setCount("2check");
        }
        if (response.data.data.findingJobDifficultyLevelId == 3) {
          setCount("3check");
        }
      })
      .catch((e) => {
        console.log(e);
      });
  };

  const handleInputChange = (event: React.ChangeEvent<any>) => {
    const { name, value } = event.target;
    setJob({ ...job, [name]: value });
  };

  const saveJob = () => {
    setShowLoader(true);
    debugger;
    var data: IEmployerObjectives = {
      UUID: props.match.params.id,
      isContinuousHire: job.isContinuousHire,
      peopleToHireCount: job.peopleToHireCount,
      whenYouNeedToHire: job.whenYouNeedToHire,
      sourcingOutsideOfJoin: job.sourcingOutsideOfJoin,
      findingJobDifficultyLevelId: count,
    };

    jobService.createObjectives(data).then(
      (response) => {
        history.push("/employer/JobPostEngagment/" + response.data.data.uuid);
      },
      (error) => {
        alert("failed to add job instructions");
        setShowLoader(false);
        //history.push('/employer/JobPostInstructions?uuid=sddsd');
      }
    );
  };

  // Validation
  const validationSchema = Yup.object().shape({
    peopleToHireCount: Yup.string().required(
      "People to hire count is required"
    ),
  });

  // functions to build form returned by useForm() hook
  const { register, handleSubmit, reset, errors } = useForm({
    resolver: yupResolver(validationSchema),
  });

  function onSubmit(data: any) {
    debugger;
    saveJob();
  }
  const [scroll, setScroll] = useState(false);
  useEffect(() => {
    window.addEventListener("scroll", () => {
      setScroll(window.scrollY > 50);
    });
  }, []);

  let currentContent: any;

  let content = {
    English: {
      Details: "Details",
      instructions: "Instructions",
      Objectives: "Objectives",
      Engagement: "Engagement",
      Whatareyourhiringneeds: "What are your hiring needs for this job ?",
      Howmanypeople: "How many people are you aiming to hire ?",
      Whendoyouneed: "When do you need to hire by ?",
      Howdifficultwouldyousay: "How difficult would you say it is to find great profiles for this job ?",
      Next: "Next",
      Back: "Back to my jobs",
    },
    Arabic: {
      Details: "التفاصيل",
      instructions: "التعليمات",
      Objectives: "الأهداف",
      Engagement: "الإلتزامات",
      Whatareyourhiringneeds: "ما هي متطلباتك لهذه الوظيفة ؟",
      Howmanypeople: "كم عدد الأشخاص اللذين تنوي تعيينهم ؟",
      Whendoyouneed: "متي تحتاج أن تعين لهذه الوظيفة ؟",
      Howdifficultwouldyousay: "في رأيك ما مدي صعوبة الحصول علي أشخاص جيدين لشغل هذه الوظيفة ؟",
      Next: "التالي",
      Back: "العودة إلي وظائفي",
    },
  };

  localStorage.getItem("currentLanguage") === "Arabic"
    ? (currentContent = content.Arabic)
    : (currentContent = content.English);



  return (
    <React.Fragment>
      {/* <div>
                <img src={jobimg} />
            </div> */}

      <div
        className={
          scroll
            ? "filters-inbox job-details fixed-filter"
            : "filters-inbox job-details bg-white"
        }
      >
        <span
          onClick={() => {
            history.push("/employer/JobPostDetails/" + props.match.params.id+"/0");
          }}
          className="plain"
        >
          {currentContent.Details} 
        </span>
        <span
          onClick={() => {
            history.push(
              "/employer/JobPostInstructions/" + props.match.params.id
            );
          }}
          className="plain"
        >
          {currentContent.instructions}
        </span>
        <span className="plain active">{currentContent.Objectives}</span>
        <span className="plain">{currentContent.Engagement}</span>
      </div>
      <div className="container">
        <form onSubmit={handleSubmit(onSubmit)} onReset={reset}>
          <div className="row">
            <div className="col-md-12">
              <div className="jb-box">
                <div className="bold-head" style={{ fontSize: "24px" }}>
                {currentContent.Objectives}
                  <div className="shortBorder-head"></div>
                </div>

                <div className="row">
                  <div className="form-group col-md-6">
                    <label>{currentContent.Whatareyourhiringneeds}</label>
                    <select
                      onChange={handleInputChange}
                      className="form-control"
                      name="isContinuousHire"
                      id="isContinuousHire"
                      value={String(job.isContinuousHire)}
                    >
                      <option value="false">One Time</option>

                      <option value="true">Constantly</option>
                    </select>
                  </div>

                  <div className="form-group col-md-6">
                    <label>{currentContent.Howmanypeople}</label>
                    <input
                      name="peopleToHireCount"
                      id="peopleToHireCount"
                      value={job.peopleToHireCount}
                      type="Number"
                      min="1"
                      ref={register}
                      className={`form-control ${
                        errors.peopleToHireCount ? "is-invalid" : ""
                      }`}
                      onChange={handleInputChange}
                    />
                    <div className="invalid-feedback">
                      {errors.peopleToHireCount?.message}
                    </div>
                  </div>

                  <div className="form-group col-md-6">
                    <label>{currentContent.Whendoyouneed}</label>
                    <select
                      onChange={handleInputChange}
                      className="form-control"
                      name="whenYouNeedToHire"
                      id="whenYouNeedToHire"
                      value={job.whenYouNeedToHire}
                    >
                      <option value="1">As Soon as Possible</option>

                      <option value="2">In the coming days</option>

                      <option value="3">No deadline , just pipelinling</option>
                    </select>
                  </div>

                  {/* <div className='form-group col-md-12'>
                                    <label>Are you sourcing for this job outside of Join right now?</label>
                                    <select onChange={handleInputChange} className='form-control' name="sourcingOutsideOfJoin" id="sourcingOutsideOfJoin"
                                    value={String(job.sourcingOutsideOfJoin)}
                                    >
                                        <option value="yes">Yes</option>

                                        <option value="no">No</option>

                                    </select>
                                </div> */}

                  <div className="form-group col-md-12">
                    <label>
                    {currentContent.Howdifficultwouldyousay}
                    </label>
                    <div>
                      <div
                        className={
                          count == "1check" ? "activeCheck check" : "check"
                        }
                        id=""
                        onClick={() => setCount("1check")}
                      >
                        <i className="far fa-circle"></i>
                        Not difficult
                      </div>

                      <div
                        className={
                          count == "2check" ? "activeCheck check" : "check"
                        }
                        id=""
                        onClick={() => setCount("2check")}
                      >
                        <i className="far fa-circle"></i>
                        Medium
                      </div>

                      <div
                        className={
                          count == "3check" ? "activeCheck check" : "check"
                        }
                        id=""
                        onClick={() => setCount("3check")}
                      >
                        <i className="far fa-circle"></i>
                        Very difficult
                      </div>
                    </div>
                  </div>
                </div>

                {/* <div className='row'>
                            <div className='onsite objective'>
                                <span className='sep'></span>
                                <span className='onText'>Our predicted pipeline for this role based on your inputs</span>
                                <span className='sep'></span>
                            </div>
                            </div> */}

                {/* <div className='row'>

                       <div className='steps'>
                       <div className='step-block'>
                         <img src={mag}/>
                         
                         <div>
                        <span className='semibold'>
                        62-72
                        </span> 
                         <br/>
                         Reviewed
                        <br/>
                        profiles
                         </div>
                       </div>
                       <img className='arrowSte' src={arrowIcon}/>

                       <div className='step-block'>
                         <img src={comment}/>
                         
                         <div>
                        <span className='semibold'>
                        62-72
                        </span> 
                         <br/>
                        Contacted
                        <br/>
                        profiles
                         </div>
                       </div>
                       <img className='arrowSte' src={arrowIcon}/>

                       <div className='step-block'>
                         <img src={percent}/>
                         
                         <div>
                        <span className='semibold'>
                        1 - 2
                        </span> 
                         <br/>
                        Interseted
                        <br/>
                        Leads
                         </div>
                       </div>
                       <img className='arrowSte' src={arrowIcon}/>

                       <div className='step-block'>
                         <img src={people}/>
                         
                         <div>
                        <span className='semibold'>
                        4 - 5
                        </span> 
                         <br/>
                        Interviews
                     
                         </div>
                       </div>
                       <img className='arrowSte' src={arrowIcon}/>

                       <div className='step-block'>
                         <img src={person}/>
                         
                         <div>
                        <span className='semibold'>
                        1
                        </span> 
                         <br/>
                        Hire
                        <br/>
                        
                         </div>
                       </div>

                       </div>

                       </div>

                             */}
                {/* <div className='row '>
                            <div className='semibold-normal-minihead'>
                            According to our estimations, in order to get
                            <span className='semibold-clrd-minihead'> 1 hire</span>
         
                            
                            
                  in the next<span className='semibold-clrd-minihead'>7 days</span>, you <br/> need to 
                  
                  
                  <span className='semibold-clrd-minihead'>review 62-72 profiles</span>
                   and 
                   <span className='semibold-clrd-minihead'>contact at least 41-47 of them,</span>
                   
                   
                    so you can <br/>
                    
                   
                     <span className='semibold-clrd-minihead'>  get 6-7 interested </span>
                     leads and
                     <span className='semibold-clrd-minihead'>     4-5 interviews </span>
                  
                            </div>
                           
                            </div>
                             */}
              </div>

              <div className="btn-job">
                <div className="pull-left"></div>

                <div className="pull-right">
                  <button
                    onClick={() => {
                      history.push("/employer/dashboard");
                    }}
                    className="btn btn-default"
                    type="button"
                  >
                    {" "}
                    {currentContent.Back}{" "}
                  </button>

                  <button className="btn btn-approve">
                    {showLoader === true ? (
                      <span className="loading">
                        <CircularProgress />
                      </span>
                    ) : (
                      ""
                    )}
                    {currentContent.Next}
                  </button>
                </div>
              </div>
            </div>
          </div>
        </form>
      </div>
    </React.Fragment>
  );
}
