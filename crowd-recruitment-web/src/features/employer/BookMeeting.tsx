import React, { useState } from 'react';
import Calendar from 'react-calendar';
import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import Grid from '@material-ui/core/Grid';
import userDum from '../../../src/assets/user-dum.jpg'
import Slider from "react-slick";
import "slick-carousel/slick/slick-theme.css";
import "slick-carousel/slick/slick.css";
import 'react-calendar/dist/Calendar.css';
import { IBookMeeting } from "../../models/IBookMeeting";
import { BatteryAlertOutlined } from '@material-ui/icons';
import JobService from "../../services/JobService";
import { history } from '../../helpers/history';


// Reference for calender https://www.npmjs.com/package/react-calendar

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        root: {
        },
    }),
);

export default function BookMeeting(props : any) {
    const [count, setCount] = useState('1time');
    const [time, setTime] = useState('1check');
    const [selectedDate, setSelectedDate] = useState(new Date());

    const [timeZone, setTimeZone] = useState(1);
    const [interviewer, setInterviewer] = useState(1);

    const onChangeDate = (date : any) => {
        setSelectedDate(date);
    }
    
    const onChangeTimeZone = (zone : any) => {
        setTimeZone(zone);
    }

    const onChangeInterviewer = (interv : any) => {
        setInterviewer(interv);
    }

    const initialMeetingState = {
        uuid: "", 
        timePeriodId : '1time',
        timeZoneId : 1,
        timeMettingId : '1check',
        interviewerId : 1,
        day : new Date()
  };

    const classes = useStyles();
    const [value, onChange] = useState(new Date());

    var settings = {
        className: "center",
        centerMode: true,
        infinite: true,
        // centerPadding: "88px",
        slidesToShow: 3,
        speed: 500,
        focusOnSelect: true,
    };

   const  BookAMeeting =()=>{
       debugger;
       var data:IBookMeeting = {
        uuid: props.match.params.id, 
        timePeriodId: count,
        timeZoneId : timeZone,
        timeMettingId: time,
        interviewerId: interviewer,
        day : selectedDate
    };

    JobService.createMeeting(data)
        .then(
            response => {
                history.push('/employer/ConfirmMeeting/'+props.match.params.id);
            },
            error => {
                alert('failed to add job instructions');
                //history.push('/employer/JobPostInstructions?uuid=sddsd');
            }
        );
    }


    return (
        <React.Fragment>
            <div className='container book-meeting'>
                <div className='row'>
                    <div className='col-md-1'></div>
                    <div className='col-md-10'>
                        <div className='row'>
                            <div className='col-md-6 p0'>

                                <div className='book-left'>
                                    <Slider {...settings}>
                                        <div onClick={()=>onChangeInterviewer('1')}
                                        >
                                            <img src={userDum} />
                                            <div className='slider-text'>
                                                Book a meeting with
                                                <br />
                                                <span className='bold-head'>
                                                    Ahmed Bashiir
                                                </span>


                                            </div>

                                        </div>
                                        <div onClick={()=>onChangeInterviewer('2')}>
                                            <img src={userDum} />
                                            <div className='slider-text'>
                                                Book a meeting with
                                        <br />
                                                <span className='bold-head'>
                                                    Jomana
                                        </span>


                                            </div>

                                        </div>
                                        
                                        
                                        <div onClick={()=>onChangeInterviewer('3')}>
                                            <img src={userDum} />

                                            <div className='slider-text'>
                                                Book a meeting with
                                                <br />
                                                <span className='bold-head'>
                                                 Sara
                                                </span>


                                            </div>

                                        </div>


                                        <div onClick={()=>onChangeInterviewer('4')}>
                                            <img src={userDum} />
                                            <div className='slider-text'>
                                                Book a meeting with
                                        <br />
                                                <span className='bold-head'>
                                                    User 4
                                        </span>


                                            </div>

                                        </div>
                                        
                                        <div onClick={()=>onChangeInterviewer('5')}>
                                            <img src={userDum} />
                                            <div className='slider-text'>
                                                Book a meeting with
                                        <br />
                                                <span className='bold-head'>
                                                    User 5
                                        </span>


                                            </div>

                                        </div>
                                        

                                    </Slider>

                                    <div className='calendar'>
                                        <Calendar
                                                    onChange={onChangeDate}
                                            value={selectedDate}
                                        />
                                    </div>
                                </div>
                            </div>
                            <div className='col-md-6 p0'>
                                <div className='book-right'>
                                    <div className='bold-head'>
                                        How long do you need?
                    </div>

                                    <div className='duration-slots'>
                                        <div
                                            className={`duration-slots-box border-L-4 ${count == '1check' ? 'activeSlot' : ''}`}
                                            id=''
                                            onClick={() => setCount('1check')}
                                        >
                                            15 min
                    </div>

                                        <div
                                            className={`duration-slots-box border-rad-0 ${count == '2check' ? 'activeSlot' : ''}`}
                                            id=''
                                            onClick={() => setCount('2check')}
                                        >
                                            30 min
                    </div>

                                        <div
                                            className={`duration-slots-box border-R-4 ${count == '3check' ? 'activeSlot' : ''}`}
                                            id=''
                                            onClick={() => setCount('3check')}
                                        >
                                            1 hour
                    </div>
                                    </div>

                                    <div className='bold-head'>
                                        What time works the best?
                    </div>

                                    <select  className='transp-select' >
                                        <option value='1'>UTC</option>
                                        <option value='2'>ART </option>
                                        <option value='3'>GMT</option>
                                        <option value='4'>ECT</option>
                                        <option value='5'>EET</option>
                                        <option value='6'>EAT </option>
                                        <option value='7'>MET </option>
                                        <option value='8'>IST</option>
                                        <option value='9'>CNT</option>
                                        <option value='10'>BET </option>
                                        <option value='11'>CAT </option>
                                        
                                    </select>

                                    <div className='time-slots'>
                                        <div
                                            className={`time-row  ${time == '1time' ? 'tActive' : ''}`}
                                            id=''
                                            onClick={() => setTime('1time')}
                                        >
                                            <span className='text-white-time'>
                                                09 : 00 Am
                    </span>

                                            <span>
                                                <i className='fa fa-check'></i>
                                            </span>
                                        </div>

                                        <div
                                            className={`time-row  ${time == '2time' ? 'tActive' : ''}`}
                                            id=''
                                            onClick={() => setTime('2time')}
                                        >
                                            <span className='text-white-time'>
                                                10 : 00 Am
                    </span>

                                            <span>
                                                <i className='fa fa-check'></i>
                                            </span>
                                        </div>
                                   
                                   
                                        <div
                                            className={`time-row  ${time == '3time' ? 'tActive' : ''}`}
                                            id=''
                                            onClick={() => setTime('3time')}
                                        >
                                            <span className='text-white-time'>
                                                11 : 00 Am
                    </span>

                                            <span>
                                                <i className='fa fa-check'></i>
                                            </span>
                                        </div>
                                   
                                        
                                        <div
                                            className={`time-row  ${time == '4time' ? 'tActive' : ''}`}
                                            id=''
                                            onClick={() => setTime('4time')}
                                        >
                                            <span className='text-white-time'>
                                                12 : 00 pm
                    </span>

                                            <span>
                                                <i className='fa fa-check'></i>
                                            </span>
                                        </div>
                                   
                                        <div
                                            className={`time-row  ${time == '5time' ? 'tActive' : ''}`}
                                            id=''
                                            onClick={() => setTime('5time')}
                                        >
                                            <span className='text-white-time'>
                                                1 : 00 pm
                    </span>

                                            <span>
                                                <i className='fa fa-check'></i>
                                            </span>
                                        </div>
                                       
                                    </div>

                                    <div className='text-center'>
                                     <button onClick={BookAMeeting} className='btn btn-book'>Book</button>
                                    </div>

                                </div>


                            </div>
                        </div>
                    </div>
                    <div className='col-md-1'></div>
                </div>
            </div>

        </React.Fragment>

    );
}
