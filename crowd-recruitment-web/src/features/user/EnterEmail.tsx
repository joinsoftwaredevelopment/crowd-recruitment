import React, { useState } from "react";
import Button from "@material-ui/core/Button";
import { useForm } from "react-hook-form";
import dots from "../../../src/assets/dots.png";
import { yupResolver } from "@hookform/resolvers/yup";
import * as Yup from "yup";
import { IUser } from "../../models/IUser";
import UserService from "../../services/UserService";
import { Dispatch } from "redux";
import { alertActions } from "../../actions/alertActions";
import { useDispatch } from "react-redux";
import { history } from "../../helpers/history";

function EnterEmail() {
  const initialUserState = {
    Email: "",
  };
  const [user, setUser] = useState(initialUserState);

  let currentContent: any;

  let content = {
    English: {
      Email: "Email",
      Emailisrequired: "Email is required",
    },
    Arabic: {
      Email: "البريد الإلكترونى",
      Emailisrequired: "البريد الإلكتروني مطلوب",
    },
  };

  localStorage.getItem("currentLanguage") === "Arabic"
    ? (currentContent = content.Arabic)
    : (currentContent = content.English);

  const validationSchema = Yup.object().shape({
    Email: Yup.string().trim().required(currentContent.Emailisrequired).email(),
  });

  const handleInputChange = (event: any) => {
    debugger;
    const { name, value } = event.target;
    setUser({ ...user, [name]: value });
  };

  const { register, handleSubmit, reset, errors } = useForm({
    resolver: yupResolver(validationSchema),
  });

  const dispatch = useDispatch();

  function onSubmit(data: IUser) {
    debugger;
    // GetUserInfo
    UserService.SendResetEmail(data)
      .then((response) => {
        dispatch(
          alertActions.success(
            "We sent an email to your email address, check it to reset your password"
          )
        );
      })
      .catch((e) => {
        dispatch(
          alertActions.error(
            "The email you entered isn't relatated with any accounts in our system, add a correct email or register a new account"
          )
        );
      });
  }

  return (
    <React.Fragment>
      <section className="login-section">
        <img className="img-dots" src={dots} />
        <form className="" onSubmit={handleSubmit(onSubmit)} onReset={reset}>
          <div className="login-form-bg">
            <h1 onClick={() => history.push("/employerHome")} className="brand">
              CR
            </h1>
            <div className="blackHead" style={{ marginBottom: "30px" }}>
              Enter your email to reset your password
            </div>
            <div className="form-group">
              <label>Email</label>
              <input
                name="Email"
                id="Email"
                value={user.Email}
                type="text"
                ref={register}
                className={`form-control ${errors.Email ? "is-invalid" : ""}`}
                onChange={handleInputChange}
              />
              <div className="invalid-feedback">{errors.Email?.message}</div>
            </div>

            <div className="next-btn">
              <Button type="submit" variant="contained" color="primary">
                Submit
              </Button>
            </div>
          </div>
        </form>
      </section>
    </React.Fragment>
  );
}

export default EnterEmail;
