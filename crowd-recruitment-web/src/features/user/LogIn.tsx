import React, { useState } from "react";
import { IUser } from "../../models/IUser";
import { useForm } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import * as Yup from "yup";
import Button from "@material-ui/core/Button";
import { useDispatch, useSelector } from "react-redux";
import { userActions } from "../../actions/userActions";
import dots from "../../../src/assets/dots.png";
import { useHistory } from "react-router-dom";

interface Props {
  language: string;
}

function LogIn(props: Props) {
  const history = useHistory();

  let currentContent: any;

  let content = {
    English: {
      Email: "Email",
      password: "Password",
      logIn: "Sign In",
      employer: "Employer",
      sourcer: "Sourcer",
      Emailisrequired: "Email is required",
      Passwordisrequired: "Password is required",
      Forgotpassword: "Forgot password ?",
    },
    Arabic: {
      Email: "البريد الإلكترونى",
      password: "كلمة المرور",
      logIn: "تسجيل الدخول",
      employer: "مقدم الوظيفة",
      sourcer: "CV مقدم",
      Emailisrequired: "البريد الإلكتروني مطلوب",
      Passwordisrequired: "كلمة المرور مطلوبة",
      Forgotpassword: "نسيت كلمةالمرور ؟",
    },
  };

  localStorage.getItem("currentLanguage") === "Arabic"
    ? (currentContent = content.Arabic)
    : (currentContent = content.English);

  const initialUserState = {
    Email: "",
    Password: "",
    RoleId: 2,
    submitted: false,
  };

  const dispatch = useDispatch();

  const [user, setUser] = useState(initialUserState);
  const [count, setCount] = useState("emp");

  const handleInputChange = (event: any) => {
    debugger;
    const { name, value } = event.target;
    setUser({ ...user, [name]: value });
  };

  const newUser = () => {
    setUser(initialUserState);
  };

  // Validation
  // form validation rules
  const validationSchema = Yup.object().shape({
    Email: Yup.string().trim().required(currentContent.Emailisrequired).email(),

    Password: Yup.string().trim().required(currentContent.Passwordisrequired),
  });

  // functions to build form returned by useForm() hook
  const { register, handleSubmit, reset, errors } = useForm({
    resolver: yupResolver(validationSchema),
  });

  function onSubmit(data: IUser) {
    debugger;
    data.RoleId = 2;
    if (count == "sour") {
      data.RoleId = 3;
    }
    debugger;
    dispatch(userActions.login(data));
  }

  return (
    <React.Fragment>
      <section className="login-section">
        <img className="img-dots" src={dots} />
        <div className="login-form-bg">
          <h1 onClick={() => history.push("/employerHome")} className="brand">
            CR
          </h1>
          <div className="top-login-nav">
            <div
              onClick={() => setCount("emp")}
              className={`gen-tab emp ${
                count == "emp" ? " gen-tab-active" : ""
              }`}
            >
              {currentContent.employer}
            </div>
            <div
              onClick={() => setCount("sour")}
              className={`gen-tab sour ${
                count == "sour" ? " gen-tab-active" : ""
              }`}
            >
              {currentContent.sourcer}
            </div>
          </div>
          <form
            className="auth-white-bg"
            onSubmit={handleSubmit(onSubmit)}
            onReset={reset}
          >
            <div className="form-group">
              <label>{currentContent.Email}</label>
              <input
                name="Email"
                id="Email"
                value={user.Email}
                type="text"
                ref={register}
                className={`form-control ${errors.Email ? "is-invalid" : ""}`}
                onChange={handleInputChange}
              />
              <div className="invalid-feedback">{errors.Email?.message}</div>
            </div>

            <div className="form-group">
              <label>{currentContent.password}</label>
              <input
                name="Password"
                id="Password"
                value={user.Password}
                type="password"
                ref={register}
                className={`form-control ${
                  errors.Password ? "is-invalid" : ""
                }`}
                onChange={handleInputChange}
              />
              <div className="invalid-feedback">{errors.Password?.message}</div>
            </div>

            <a
              onClick={() => {
                history.push("/enterEmail");
              }}
              className="anchor"
              style={{ color: "red" }}
            >
              {currentContent.Forgotpassword}
            </a>

            <div className="next-btn">
              <Button type="submit" variant="contained" color="primary">
                {currentContent.logIn}
              </Button>
            </div>
          </form>
        </div>
      </section>
    </React.Fragment>
  );
}

export default LogIn;
