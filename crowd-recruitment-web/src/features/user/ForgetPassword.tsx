import React, { useState } from "react";
import Button from "@material-ui/core/Button";
import dots from "../../../src/assets/dots.png";
import { yupResolver } from "@hookform/resolvers/yup";
import * as Yup from "yup";
import { IRsetPassword } from "../../models/IRsetPassword";
import UserService from "../../services/UserService";
import { Dispatch } from "redux";
import { alertActions } from "../../actions/alertActions";
import { useDispatch } from "react-redux";
import { useForm } from "react-hook-form";
import { history } from "../../helpers/history";
import { useHistory } from "react-router-dom";

function CreateNewPassword(props: any) {
  let currentContent: any;

  let content = {
    English: {
      password: "Password",
      confirmPassword: "Password",
    },
    Arabic: {
      password: "كلمة المرور",
      confirmPassword: "تأكيد كلمة المرور",
    },
  };

  localStorage.getItem("currentLanguage") === "Arabic"
    ? (currentContent = content.Arabic)
    : (currentContent = content.English);

  const initialUserState = {
    Password: "",
    ConfirmPassword: "",
    uuid: "",
  };

  const dispatch = useDispatch();

  const [user, setUser] = useState(initialUserState);

  const handleInputChange = (event: any) => {
    debugger;
    const { name, value } = event.target;
    setUser({ ...user, [name]: value });
  };

  const validationSchema = Yup.object().shape({
    Password: Yup.string()
      .trim()
      .min(8, currentContent.Passwordmustbeatleast)
      .required(currentContent.Passwordisrequired),
    ConfirmPassword: Yup.string().oneOf(
      [Yup.ref("Password"), null],
      "Two passwords must match"
    ),
  });

  // functions to build form returned by useForm() hook
  const { register, handleSubmit, reset, errors } = useForm({
    resolver: yupResolver(validationSchema),
  });

  function onSubmit(data: IRsetPassword) {
    data.Uuid = props.match.params.uuid;
    debugger;
    UserService.ResePassword(data)
      .then((response) => {
        history.push("/login");
      })
      .catch((e) => {
        dispatch(alertActions.error("Failed to update your password"));
      });
  }

  return (
    <React.Fragment>
      <section className="login-section">
        <img className="img-dots" src={dots} />
        <div className="login-form-bg">
          <h1 onClick={() => history.push("/employerHome")} className="brand">
            CR
          </h1>
          <div className="blackHead" style={{ marginBottom: "30px" }}>
            Create your new password
          </div>
          <form className="" onSubmit={handleSubmit(onSubmit)} onReset={reset}>
            <div className="form-group">
              <label>New Password</label>
              <input
                name="Password"
                id="Password"
                value={user.Password}
                type="password"
                ref={register}
                className={`form-control ${
                  errors.Password ? "is-invalid" : ""
                }`}
                onChange={handleInputChange}
              />
              <div className="invalid-feedback">{errors.Password?.message}</div>
            </div>

            <div className="form-group">
              <label>Confirm Password</label>
              <input
                name="ConfirmPassword"
                id="ConfirmPassword"
                value={user.ConfirmPassword}
                type="password"
                ref={register}
                className={`form-control ${
                  errors.ConfirmPassword ? "is-invalid" : ""
                }`}
                onChange={handleInputChange}
              />
              <div className="invalid-feedback">
                {errors.ConfirmPassword?.message}
              </div>
            </div>

            <div className="next-btn">
              <Button type="submit" variant="contained" color="primary">
                Submit
              </Button>
            </div>
          </form>
        </div>
      </section>
    </React.Fragment>
  );
}

export default CreateNewPassword;
