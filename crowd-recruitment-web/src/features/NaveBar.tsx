import React from "react";


function NaveBar(props:any) {
  return (
    <nav className="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">
      <div className="container">
        <div className="language-select">
          <select
            className="custom-select"
            value={props.language}
            onChange={e => props.handleSetLanguage(e.target.value)}
          >
            <option value="English">English</option>
            <option value="Malayalam">Arabic</option>
          </select>
        </div>
      </div>
    </nav>
  );
}

export default NaveBar;