import React, { useState } from "react";
import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';


const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        root: {
        },
    }),
);

export default function LoginEmployer() {
    const classes = useStyles();

    return (
        <React.Fragment>
            <section className='login-section'>
                <div className='login-form-bg'>
                <div className='top-login-nav'>
                <div className='gen-tab emp gen-tab-active'>
                Employer
                </div>
                <div className='gen-tab sour'>
                Sourcer
                </div>
                </div>
                    <form className='auth-white-bg '>
                        <div className='form-group'>
                            <label>Email</label>
                            <input
                                name=""
                                id=""
                                value=''
                                type="text"
                                className='form-control'
                            />

                        </div>

                        <div className='form-group'>
                            <label>Password</label>
                            <input
                                name=""
                                id=""
                                value=''
                                type="password"
                                className='form-control'
                            />

                        </div>

                        <a className='anchor' style={{color:'red'}}>Forgot password?</a>

                        <div className='next-btn'>
                            <Button type="button" variant="contained" color="primary">
                                Login
                </Button>
                        </div>
                    </form>
                </div>
            </section>
        </React.Fragment>

    );
}
