import React, { useState, useEffect } from "react";
import {
  createStyles,
  Theme,
  withStyles,
  WithStyles,
  makeStyles,
} from "@material-ui/core/styles";
import tick from "../../../src/assets/tick.png";
import { history } from "../../helpers/history";

import AppBar from "@material-ui/core/AppBar";
import Tabs from "@material-ui/core/Tabs";
import Tab from "@material-ui/core/Tab";
import Typography from "@material-ui/core/Typography";
import Box from "@material-ui/core/Box";
import sourcerService from "../../services/SourcerService";
import { ISourcerDetails } from "../../models/ISourcerDetails";
import { IUserRequest } from "../../models/IUserRequest";
import { userStatusConstant } from "../../constants/userStatusConstant";

import IconButton from "@material-ui/core/IconButton";
import CloseIcon from "@material-ui/icons/Close";

import Dialog from "@material-ui/core/Dialog";
import MuiDialogTitle from "@material-ui/core/DialogTitle";
import MuiDialogContent from "@material-ui/core/DialogContent";
import MuiDialogActions from "@material-ui/core/DialogActions";
import questionMark from "../../../src/assets/question-mark.png";
import decline from "../../../src/assets/decline.png";

import { useForm } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import * as Yup from "yup";
import { Document, Page } from "react-pdf";

const styles = (theme: Theme) =>
  createStyles({
    root: {
      margin: 0,
      padding: theme.spacing(2),
    },
    closeButton: {
      position: "absolute",
      right: theme.spacing(1),
      top: theme.spacing(1),
      color: theme.palette.grey[500],
    },
  });

export interface DialogTitleProps extends WithStyles<typeof styles> {
  id: string;
  children: React.ReactNode;
  onClose: () => void;
}

const DialogTitle = withStyles(styles)((props: DialogTitleProps) => {
  const { children, classes, onClose, ...other } = props;
  return (
    <MuiDialogTitle disableTypography className={classes.root} {...other}>
      <Typography variant="h6">{children}</Typography>
      {onClose ? (
        <IconButton
          aria-label="close"
          className={classes.closeButton}
          onClick={onClose}
        >
          <CloseIcon />
        </IconButton>
      ) : null}
    </MuiDialogTitle>
  );
});

const DialogContent = withStyles((theme: Theme) => ({
  root: {
    padding: theme.spacing(2),
  },
}))(MuiDialogContent);

const DialogActions = withStyles((theme: Theme) => ({
  root: {
    margin: 0,
    padding: theme.spacing(1),
  },
}))(MuiDialogActions);

interface TabPanelProps {
  children?: React.ReactNode;
  index: any;
  value: any;
}

function TabPanel(props: TabPanelProps) {
  const { children, value, index, ...other } = props;

  return (
    <div
      role="tabpanel"
      hidden={value !== index}
      id={`simple-tabpanel-${index}`}
      aria-labelledby={`simple-tab-${index}`}
      {...other}
    >
      {value === index && <Box p={3}>{children}</Box>}
    </div>
  );
}

function a11yProps(index: any) {
  return {
    id: `simple-tab-${index}`,
    "aria-controls": `simple-tabpanel-${index}`,
  };
}

const useStyles = makeStyles((theme: Theme) => ({
  root: {
    flexGrow: 1,
    backgroundColor: theme.palette.background.paper,
  },
}));

export default function Info(props: any) {
  const classes = useStyles();

  const [value, setValue] = React.useState(0);

  const [reason, setReason] = useState<string>("");

  const handleChange = (event: React.ChangeEvent<{}>, newValue: number) => {
    setValue(newValue);
  };

  const initialSourcerState = {
    uuid: "",
    date: new Date(),
    fullName: "",
    phoneNumber: "",
    emailAddress: "",
    experience: "",
    jobTitle: "",
    haveAccessToDifferentWebsite: false,
    industryYouInterested: "",
    cvPath: "",
    linkedInProfile: "",
    rejectedReason: "",
    userStatusId: 1,
    registerationStep: 0,
  };
  const [currentSourcer, setCurrentSourcer] = useState(initialSourcerState);
  const [message, setMessage] = useState("");
  const [open, setOpen] = useState(false);
  const [pdfDoc, setPdfDoc] = useState("");
  const [path, setPath] = useState("");
  // const pdfDoc =  require("../../../src/assets/pdfDoc.pdf");
  // const [userStatus, setSetUserStatus] = useState(1);

  const validationSchema = Yup.object().shape({
    rejectedReason: Yup.string().required("Rejection reason is required"),
  });

  const getSourcer = (uuid: string) => {
    sourcerService
      .getSourcerInfo(uuid)
      .then((response) => {
        debugger;
        setCurrentSourcer(response.data.data);
        //alert(currentSourcer.cvPath)
        currentSourcer.cvPath = response.data.data.cvPath;
        console.log(response.data);

        openPdf(response.data.data.cvPath);
      })
      .catch((e) => {
        console.log(e);
      });
  };

  const openPdf = (fileName: string) => {
    sourcerService
      .getPdf(fileName)
      .then((response) => {
        debugger;
        console.log(response.data);
        //Create a Blob from the PDF Stream
        const file = new Blob([response.data], { type: "application/pdf" });
        //Build a URL from the file
        const fileURL = URL.createObjectURL(file);
        setPath(fileURL);
        //Open the URL on new Window
        //window.open(fileURL);
      })
      .catch((e) => {
        console.log(e);
      });
  };

  useEffect(() => {
    getSourcer(props.match.params.id);
  }, [props.id]);

  const acceptSourcer = (uuid: string) => {
    const currentSourcer: IUserRequest = {
      uuid: props.match.params.id,
      rejectedReason: "",
    };
    sourcerService
      .acceptSourcer(currentSourcer)
      .then((response) => {
        props.history.push("/admin/sourcer-request");
      })
      .catch((e) => {
        console.log(e);
      });
  };

  const declineSourcer = () => {
    const currentSourcer: IUserRequest = {
      uuid: props.match.params.id,
      rejectedReason: reason,
    };
    debugger;
    sourcerService
      .rejectSourcer(currentSourcer)
      .then((response) => {
        props.history.push("/admin/sourcer-request");
        handleClose();
        setReason("");
      })
      .catch((e) => {
        console.log(e);
      });
  };

  const handleInputChange = (event: React.ChangeEvent<any>) => {
    debugger;
    const { name, value } = event.target;
    setReason(value);
  };

  const { register, handleSubmit, reset, errors } = useForm({
    resolver: yupResolver(validationSchema),
  });

  function onSubmit() {
    declineSourcer();
  }

  const handleClose = () => {
    setOpen(false);
  };

  const handleClickOpen = () => {
    setOpen(true);
  };
  const [rejectionReason, setRejectionReason] = React.useState("");
  const [openReason, setOpenReason] = useState(false);

  const handleClickOpenReason = (reason: any) => {
    setOpenReason(true);
    setRejectionReason(reason);
  };

  const handleCloseReason = () => {
    setOpenReason(false);
  };

  const [numPages, setNumPages] = useState(null);
  const [pageNumber, setPageNumber] = useState(1);

  function onDocumentLoadSuccess() {
    setNumPages(numPages);
  }

  return (
    <React.Fragment>
      <div className="listing-admin">
        <div className="bread-text">
          <a
            className="lt-text"
            onClick={() => {
              history.push("/admin/sourcer-request");
            }}
          >
            Sourcers
          </a>
          <span className="lt-text">
            <i className="fas fa-angle-right"></i>
          </span>
          <span className="lt-head">{currentSourcer.fullName} Details</span>
        </div>
        <div className="row">
          <div className="col-md-5">
            <div className="data-card data-info">
              <div className="data-img">
                <img src={tick} />
              </div>
              <div className="data-card-head">{currentSourcer.fullName}</div>
              <div className="data-card-subhead">
                {currentSourcer.phoneNumber}
              </div>
              <div className="data-card-light">
                {currentSourcer.emailAddress}
              </div>

              <div className="data-links">
                <span className="table-link">
                  <a target="_blank" href={currentSourcer.linkedInProfile}>
                    <span className="table-link">in</span>
                  </a>
                </span>
                {/* <span className='table-link'>cv</span> */}
              </div>

              <div className="data-desc">
                <div className="data-card-head">Experience</div>
                <div className="data-card-light">
                  {currentSourcer.experience}
                </div>
              </div>
              <div className="data-desc">
                <div className="data-card-head">Industry</div>
                <div className="data-card-light">
                  {currentSourcer.industryYouInterested}
                </div>
              </div>

              <div className="data-btns">
                <button
                  style={{
                    display:
                      (currentSourcer.userStatusId ==
                        userStatusConstant.Request ||
                        currentSourcer.userStatusId ==
                          userStatusConstant.Approved) &&
                      currentSourcer.registerationStep == 6
                        ? "inline"
                        : "none",
                  }}
                  onClick={handleClickOpen}
                  className="btn btn-decline"
                >
                  Decline
                </button>
                <button
                  style={{
                    display:
                      (currentSourcer.userStatusId ==
                        userStatusConstant.Request ||
                        currentSourcer.userStatusId ==
                          userStatusConstant.Declined) &&
                      currentSourcer.registerationStep == 6
                        ? "inline"
                        : "none",
                  }}
                  className="btn btn-approve"
                  onClick={() => acceptSourcer(currentSourcer.uuid)}
                >
                  Approve
                </button>

                <button
                  style={{
                    display:
                      currentSourcer.userStatusId == userStatusConstant.Declined
                        ? "inline"
                        : "none",
                  }}
                  className="btn btn-info"
                  onClick={() =>
                    handleClickOpenReason(currentSourcer.rejectedReason)
                  }
                >
                  View Reason
                </button>
              </div>
            </div>
          </div>
          <div className="col-md-7">
            <div className="data-card">
              <AppBar position="static">
                <Tabs
                  value={value}
                  onChange={handleChange}
                  aria-label="simple tabs example"
                >
                  <Tab label="CV" {...a11yProps(0)} />
                  {/* <Tab label="Profile" {...a11yProps(1)} />
          <Tab label="Contact" {...a11yProps(2)} /> */}
                </Tabs>
              </AppBar>
              <TabPanel value={value} index={0}>
                <div>
                  <iframe
                    src={currentSourcer.cvPath != null ? path : ""}
                    width="100%"
                    height="500px"
                  />
                </div>
              </TabPanel>
              <TabPanel value={value} index={1}>
                Profile
              </TabPanel>
              <TabPanel value={value} index={2}>
                Contact
              </TabPanel>
            </div>
          </div>
        </div>
      </div>

      <Dialog
        fullWidth={true}
        maxWidth={"sm"}
        onClose={handleClose}
        aria-labelledby="customized-dialog-title"
        open={open}
      >
        <DialogTitle id="customized-dialog-title3" onClose={handleClose}>
          Decline Sourcer
        </DialogTitle>
        <form className="" onSubmit={handleSubmit(onSubmit)} onReset={reset}>
          <DialogContent className="text-center" dividers>
            <img style={{ marginTop: "20px" }} src={questionMark} />

            <div className="head-job-pop">
              <div className="blackHead">Type a reason to decline the job</div>
              <div className="shortBorder"></div>
            </div>

            <div className="form-group">
              <label>Enter your reason </label>
              <textarea
                name="rejectedReason"
                id="rejectedReason"
                value={reason}
                ref={register}
                rows={6}
                placeholder="Reason"
                className="form-control"
                onChange={handleInputChange}
              />
              <div className="invalid-feedback">
                {errors.rejectedReason?.message}
              </div>
            </div>

            <div className="text-center job-pop-btns">
              <button onClick={handleClose} className="btn btn-decline">
                Cancel
              </button>
              <button type="submit" className="btn btn-approve">
                Confirm
              </button>
            </div>
          </DialogContent>
        </form>
      </Dialog>
      <Dialog
        fullWidth={true}
        maxWidth={"sm"}
        onClose={handleCloseReason}
        aria-labelledby="customized-dialog-title"
        open={openReason}
      >
        <DialogTitle id="customized-dialog-title3" onClose={handleCloseReason}>
          Reason
        </DialogTitle>

        <DialogContent className="text-center" dividers>
          <img style={{ marginTop: "20px" }} src={decline} />

          <div className="head-job-pop">
            <div className="blackHead">
              We are sorry to inform you that <br />
              we had declined you
            </div>
            <div className="shortBorder"></div>
          </div>

          <p className="blurText">{rejectionReason}</p>

          <div className="text-center job-pop-btns">
            <button onClick={handleCloseReason} className="btn btn-approve">
              OK
            </button>
          </div>
        </DialogContent>
      </Dialog>
    </React.Fragment>
  );
}
