import React, { useState, useEffect } from "react";
import { createStyles, makeStyles, Theme } from "@material-ui/core/styles";
import { ISourcerDetails } from "../../models/ISourcerDetails";
import sourcerService from "../../services/SourcerService";
import { Link } from "react-router-dom";
import { userStatusConstant } from "../../constants/userStatusConstant";
import { IUserRequest } from "../../models/IUserRequest";
import DataTable, { createTheme } from "react-data-table-component";
import { useHistory } from "react-router-dom";

import DataTableExtensions from "react-data-table-component-extensions";
import "react-data-table-component-extensions/dist/index.css";

// import jsPDF from 'jspdf';
// import html2canvas from 'html2canvas';
// import autoTable from  "jspdf-autotable";

declare const require: any;
const { jsPDF } = require("jspdf");
require('jspdf-autotable');

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {},
  })
);

const SourcerRequest = () => {
  const classes = useStyles();
  const [userStatus, setSetUserStatus] = useState(1);
  const [Sourcers, setSourcers] = useState<ISourcerDetails[]>([]);
  const history = useHistory();

  useEffect(() => {
    retrieveSourcers(userStatus);
  }, []);

  const GetAllSourcersByStatusId = (userStatusId: any) => {
    console.log(userStatusId);
    setSetUserStatus(userStatusId);
    retrieveSourcers(userStatusId);
  };

  var dateFormat = require("dateformat");

  const retrieveSourcers = (userStatusId: number) => {
    sourcerService
      .getAll(userStatusId)
      .then((response) => {
        debugger;
        console.log(response.data.data);
        setSourcers(response.data.data);
      })
      .catch((e) => {
        console.log(e);
      });
  };

  const columns = [
    {
      name: "Date",
      selector: "jobDate",
      sortable: true,
      cell: (row: any) => (
        <span>{dateFormat(row.createdDate, "d mmmm, yyyy")}</span>
      ),
    },
    {
      name: "Full Name",
      selector: "fullName",
      sortable: true,
    },
    {
      name: "Phone Number",
      selector: "phoneNumber",
      sortable: true,
    },
    {
      name: "Email",
      selector: "email",
      sortable: true,
    },
    {
      name: "Job Title",
      selector: "jobTitle",
      sortable: true,
    },
    {
      name: "Links",
      selector: "",
      sortable: true,
      cell: (row: any) => (
        <a target="_blank" href={row.linkedInProfile}>
          <span className="table-link">in</span>
        </a>
      ),
    },
    {
      name: "Details",
      selector: "",
      sortable: true,
      cell: (row: any) => (
        <button
          className="btn btn-approve"
          onClick={() => history.push("/admin/sourcer-info/" + row.uuid)}
        >
          Show Details
        </button>
      ),
    },
  ];

  // const exportPDF = () => {
  // const unit = "pt";
  //   const size = "A4"; // Use A1, A2, A3 or A4
  //   const orientation = "portrait"; // portrait or landscape

  //   const marginLeft = 40;
  //   const doc = new jsPDF(orientation, unit, size);

  //   doc.setFontSize(15);

  //   const title = "";
  //   const headers = [["Date", "Full Name","Phone Number", 'Email', 'Job Title']];

  //   const data = Sourcers.map(elt=> [dateFormat(elt.createdDate, "d mmmm, yyyy"), elt.fullName, elt.phoneNumber, elt.email, elt.jobTitle,]);


  //   let content = {
  //     startY: 50,
  //     head: headers,
  //     body: data
  //   };

  //   doc.text(title, marginLeft, 40);
  //   doc.autoTable(content);
  //   doc.save("report.pdf")
  //  }
  return (
    <React.Fragment>
      <div className="listing-admin job-req">
        <div className="head-sec">
          <div className="blackHead">Sourcers</div>

          <div className="filter">
            <span
              id=""
              onClick={() =>
                GetAllSourcersByStatusId(userStatusConstant.Request)
              }
              className={
                userStatus == userStatusConstant.Request
                  ? "active-filter highlight-filter border-rad-L-6"
                  : "non-active-filter border-rad-L-6"
              }
            >
              Requests
            </span>
            <span
              id=""
              onClick={() =>
                GetAllSourcersByStatusId(userStatusConstant.Approved)
              }
              className={
                userStatus == userStatusConstant.Approved
                  ? "active-filter highlight-filter"
                  : "non-active-filter"
              }
            >
              Approved
            </span>
            <span
              id=""
              onClick={() =>
                GetAllSourcersByStatusId(userStatusConstant.Declined)
              }
              className={
                userStatus == userStatusConstant.Declined
                  ? "active-filter highlight-filter border-rad-R-6"
                  : "non-active-filter border-rad-R-6"
              }
            >
              Declined
            </span>
          </div>
        </div>

        {/* <button className='btn btn-approve' onClick={exportPDF}>Download as PDF</button> */}

        <div id='capture'>      


        <DataTableExtensions columns={columns} data={Sourcers}>
          <DataTable columns={columns} data={Sourcers} pagination={true} />
        </DataTableExtensions>
        </div>
        {/* <div className="table-responsive">
          <table className="table">
            <thead>
              <tr>
                <th>Date</th>
                <th>Fullname</th>
                <th>Phone Number</th>
                <th>Email</th>
                <th>Job Title</th>
                <th>Links</th>
                <th>Details</th>
              </tr>
            </thead>
            <tbody>
              {Sourcers.map((m) => {
                return (
                  <tr>
                    <td>{dateFormat(m.createdDate, "d mmmm, yyyy")}</td>
                    <td>{m.fullName}</td>
                    <td>{m.phoneNumber}</td>
                    <td>{m.email}</td>
                    <td>{m.jobTitle}</td>
                    <td>
                      <a target="_blank" href={m.linkedInProfile}>
                        <span className="table-link">in</span>
                      </a>
                    </td>
                    <td>
                      <Link to={"/admin/sourcer-info/" + m.uuid}>Details</Link>
                    </td>
                  </tr>
                );
              })}
            </tbody>
          </table>
        </div> */}
      </div>
    </React.Fragment>
  );
};

export default SourcerRequest;
