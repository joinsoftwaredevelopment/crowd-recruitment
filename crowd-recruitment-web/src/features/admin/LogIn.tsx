import React, { useState } from "react";
import { IUser } from "../../models/IUser";
import userService from "../../services/UserService";
import { useForm } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import * as Yup from "yup";
import Button from "@material-ui/core/Button";
import { useDispatch, useSelector } from "react-redux";
import { adminActions } from "../../actions/adminActions";
import dots from "../../../src/assets/dots.png";
import { useHistory } from "react-router-dom";

interface Props {
  language: string;
}

function LogIn(props: Props) {
  const history = useHistory();
  let currentContent: any;

  let content = {
    English: {
      Email: "Email",
      password: "Password",
      logIn: "Sign In",
    },
    Arabic: {
      Email: "البريد الإلكترونى",
      password: "كلمة المرور",
      logIn: "تسجيل الدخول",
    },
  };

  props.language === "Arabic"
    ? (currentContent = content.Arabic)
    : (currentContent = content.English);

  const initialUserState = {
    Email: "",
    Password: "",
    submitted: false,
  };

  const dispatch = useDispatch();

  const [user, setUser] = useState(initialUserState);

  const handleInputChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    debugger;
    const { name, value } = event.target;
    setUser({ ...user, [name]: value });
  };

  const newUser = () => {
    setUser(initialUserState);
  };

  // Validation
  // form validation rules
  const validationSchema = Yup.object().shape({
    Email: Yup.string().trim().required("Email is required").email(),

    Password: Yup.string().trim().required("Password is required"),
  });

  // functions to build form returned by useForm() hook
  const { register, handleSubmit, reset, errors } = useForm({
    resolver: yupResolver(validationSchema),
  });

  function onSubmit(data: IUser) {
    debugger;
    dispatch(adminActions.login(data));
  }

  return (
    <React.Fragment>
      <section className="login-section">
        <img className="img-dots" src={dots} />
        <div className="login-form-bg">
          <h1 onClick={() => history.push("/employerHome")} className="brand">
            CR
          </h1>
          <div className="top-login-nav">
            <div className="gen-tab emp gen-tab-active ad-bt">Admin</div>
          </div>
          <form
            className="auth-white-bg"
            onSubmit={handleSubmit(onSubmit)}
            onReset={reset}
          >
            <div className="form-group">
              <label>{currentContent.Email}</label>
              <input
                name="Email"
                id="Email"
                value={user.Email}
                type="text"
                ref={register}
                className={`form-control ${errors.Email ? "is-invalid" : ""}`}
                onChange={handleInputChange}
              />
              <div className="invalid-feedback">{errors.Email?.message}</div>
            </div>

            <div className="form-group">
              <label>{currentContent.password}</label>
              <input
                name="Password"
                id="Password"
                value={user.Password}
                type="password"
                ref={register}
                className={`form-control ${
                  errors.Password ? "is-invalid" : ""
                }`}
                onChange={handleInputChange}
              />
              <div className="invalid-feedback">{errors.Password?.message}</div>
            </div>

            {/* <a className='anchor' style={{color:'red'}}>Forgot password?</a> */}

            <div className="next-btn">
              <Button type="submit" variant="contained" color="primary">
                {currentContent.logIn}
              </Button>
            </div>
          </form>
        </div>
      </section>
    </React.Fragment>
  );
}

export default LogIn;
