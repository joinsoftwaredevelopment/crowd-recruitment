import {
  createStyles,
  Theme,
  withStyles,
  WithStyles,
} from "@material-ui/core/styles";
import DataTable, { createTheme } from "react-data-table-component";
import { IEmployerDetails } from "../../models/IEmployerDetails";
import employerService from "../../services/EmployerService";
import React, { useState, useEffect } from "react";
import { employerActions } from "../../actions/employerActions";

import IconButton from "@material-ui/core/IconButton";
import CloseIcon from "@material-ui/icons/Close";
import Typography from "@material-ui/core/Typography";

import Dialog from "@material-ui/core/Dialog";
import MuiDialogTitle from "@material-ui/core/DialogTitle";
import MuiDialogContent from "@material-ui/core/DialogContent";
import MuiDialogActions from "@material-ui/core/DialogActions";

import { date } from "yup/lib/locale";
import { userStatusConstant } from "../../constants/userStatusConstant";
import { IUserRequest } from "../../models/IUserRequest";
import questionMark from "../../../src/assets/question-mark.png";

import { useForm } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import * as Yup from "yup";
import decline from "../../../src/assets/decline.png";

import DataTableExtensions from "react-data-table-component-extensions";
import "react-data-table-component-extensions/dist/index.css";

const styles = (theme: Theme) =>
  createStyles({
    root: {
      margin: 0,
      padding: theme.spacing(2),
    },
    closeButton: {
      position: "absolute",
      right: theme.spacing(1),
      top: theme.spacing(1),
      color: theme.palette.grey[500],
    },
  });

export interface DialogTitleProps extends WithStyles<typeof styles> {
  id: string;
  children: React.ReactNode;
  onClose: () => void;
}

const DialogTitle = withStyles(styles)((props: DialogTitleProps) => {
  const { children, classes, onClose, ...other } = props;
  return (
    <MuiDialogTitle disableTypography className={classes.root} {...other}>
      <Typography variant="h6">{children}</Typography>
      {onClose ? (
        <IconButton
          aria-label="close"
          className={classes.closeButton}
          onClick={onClose}
        >
          <CloseIcon />
        </IconButton>
      ) : null}
    </MuiDialogTitle>
  );
});

const DialogContent = withStyles((theme: Theme) => ({
  root: {
    padding: theme.spacing(2),
  },
}))(MuiDialogContent);

const DialogActions = withStyles((theme: Theme) => ({
  root: {
    margin: 0,
    padding: theme.spacing(1),
  },
}))(MuiDialogActions);

const EmployerRequest = () => {
  const [userStatus, setSetUserStatus] = useState(1);
  const [open, setOpen] = useState(false);
  const [employers, setEmployers] = useState<IEmployerDetails[]>([]);
  const [reason, setReason] = useState<string>("");
  const [currentUuid, setcurrentUuid] = useState<string>("");
  const [rejectionReason, setRejectionReason] = React.useState("");
  const [openReason, setOpenReason] = useState(false);
  const [openPackage, setOpenPackage] = useState(false);

  useEffect(() => {
    console.log(userStatusConstant.Request);
    retrieveEmployers(userStatusConstant.Request);
  }, []);

  const GetAllEmployersByStatusId = (userStatusId: any) => {
    console.log(userStatusId);
    setSetUserStatus(userStatusId);
    retrieveEmployers(userStatusId);
  };

  var dateFormat = require("dateformat");

  const validationSchema = Yup.object().shape({
    rejectedReason: Yup.string().required("Rejection reason is required"),
  });

  const retrieveEmployers = (userStatusId: number) => {
    employerService
      .getAll(userStatusId)
      .then((response) => {
        debugger;
        console.log(response.data.data);
        setEmployers(response.data.data);
      })
      .catch((e) => {
        console.log(e);
      });
  };

  const handleInputChange = (event: React.ChangeEvent<any>) => {
    debugger;
    const { name, value } = event.target;
    setReason(value);
  };

  const acceptEmployer = (uuid: string) => {
    const currentEmployer: IUserRequest = {
      uuid: uuid,
      rejectedReason: "",
    };
    debugger;
    employerService
      .acceptEmployer(currentEmployer)
      .then((response) => {
        if (userStatus == userStatusConstant.Request) {
          setSetUserStatus(userStatusConstant.Request);
          retrieveEmployers(userStatusConstant.Request);
        } else if (userStatus == userStatusConstant.Approved) {
          setSetUserStatus(userStatusConstant.Approved);
          retrieveEmployers(userStatusConstant.Approved);
        } else {
          setSetUserStatus(userStatusConstant.Declined);
          retrieveEmployers(userStatusConstant.Declined);
        }
      })
      .catch((e) => {
        console.log(e);
      });
  };

  const declineEmployer = () => {
    const currentEmployer: IUserRequest = {
      uuid: currentUuid,
      rejectedReason: reason,
    };
    debugger;
    employerService
      .rejectEmployer(currentEmployer)
      .then((response) => {
        if (userStatus == userStatusConstant.Request) {
          setSetUserStatus(userStatusConstant.Request);
          retrieveEmployers(userStatusConstant.Request);
        } else if (userStatus == userStatusConstant.Approved) {
          setSetUserStatus(userStatusConstant.Approved);
          retrieveEmployers(userStatusConstant.Approved);
        } else {
          setSetUserStatus(userStatusConstant.Declined);
          retrieveEmployers(userStatusConstant.Declined);
        }

        handleClose();
        setReason("");
        setcurrentUuid("");
      })
      .catch((e) => {
        console.log(e);
      });
  };

  const { register, handleSubmit, reset, errors } = useForm({
    resolver: yupResolver(validationSchema),
  });

  function onSubmit() {
    declineEmployer();
  }

  const handleClose = () => {
    setOpen(false);
  };

  const handleClickOpen = (uuid: string) => {
    setcurrentUuid(uuid);
    setOpen(true);
  };

  const handleClickOpenReason = (reason: any) => {
    setOpenReason(true);
    setRejectionReason(reason);
  };

  const handleCloseReason = () => {
    setOpenReason(false);
  };

  const handleOpenPackage = () => {
    setOpenPackage(true);
  };

  const handleClosePackage = () => {
    setOpenPackage(false);
  };

  const columns = [
    {
      name: "Date",
      selector: "jobDate",
      sortable: true,
      cell: (row: any) => (
        <span>{dateFormat(row.createdDate, "d mmmm, yyyy")}</span>
      ),
    },
    {
      name: "Company Name",
      selector: "companyName",
      sortable: true,
    },
    {
      name: "Full Name",
      selector: "fullName",
      sortable: true,
    },
    {
      name: "Phone Number",
      selector: "phoneNumber",
      sortable: true,
    },
    {
      name: "Email",
      selector: "email",
      sortable: true,
    },
    {
      name: "Package",
      selector: "",
      sortable: false,
      cell: (row: any) => (
        <span onClick={handleOpenPackage} className="blue bold pointer">
          Golden Package
        </span>
      ),
    },
    {
      name: "Action",
      selector: "",
      sortable: false,
      minWidth: "200px",
      cell: (row: any) => (
        <span>
          <button
            style={{
              display:
                !(userStatus == userStatusConstant.Request) &&
                !(userStatus == userStatusConstant.Declined)
                  ? "none"
                  : "inline",
            }}
            className="btn btn-approve"
            onClick={() => acceptEmployer(row.uuid)}
          >
            Approve
          </button>
          <button
            style={{
              display:
                (!(userStatus == userStatusConstant.Request) &&
                  !(userStatus == userStatusConstant.Approved)) ||
                row.openedJobsCount != 0
                  ? "none"
                  : "inline",
            }}
            className="btn btn-decline"
            onClick={() => handleClickOpen(row.uuid)}
          >
            Decline
          </button>
          <button
            style={{
              display:
                userStatus == userStatusConstant.Declined ? "inline" : "none",
            }}
            className="btn btn-info"
            onClick={() => handleClickOpenReason(row.rejectedReason)}
          >
            View Reason
          </button>
        </span>
      ),
    },
  ];

  return (
    <React.Fragment>
      <div className="listing-admin job-req">
        <div className="head-sec">
          <div className="blackHead">Employers</div>

          <div className="filter">
            <span
              id=""
              onClick={() =>
                GetAllEmployersByStatusId(userStatusConstant.Request)
              }
              className={
                userStatus == userStatusConstant.Request
                  ? "active-filter highlight-filter border-rad-L-6"
                  : "non-active-filter border-rad-L-6"
              }
            >
              Requests
            </span>
            <span
              id=""
              onClick={() =>
                GetAllEmployersByStatusId(userStatusConstant.Approved)
              }
              className={
                userStatus == userStatusConstant.Approved
                  ? "active-filter highlight-filter"
                  : "non-active-filter"
              }
            >
              Approved
            </span>
            <span
              id=""
              onClick={() =>
                GetAllEmployersByStatusId(userStatusConstant.Declined)
              }
              className={
                userStatus == userStatusConstant.Declined
                  ? "active-filter highlight-filter border-rad-R-6"
                  : "non-active-filter border-rad-R-6"
              }
            >
              Declined
            </span>
          </div>
        </div>

        <DataTableExtensions columns={columns} data={employers}>
          <DataTable pagination title="" columns={columns} data={employers} />
        </DataTableExtensions>

        {/* <div className="table-responsive">
          <table className="table">
            <thead>
              <tr>
                <th>Date</th>
                <th>Company Name</th>
                <th>Full Name</th>
                <th>Phone Number</th>
                <th>Email</th>
                <th>Action</th>
              </tr>
            </thead>
            <tbody>
              {employers.map((m) => {
                return (
                  <tr key={m.uuid}>
                    <td>{dateFormat(m.createdDate, "d mmmm, yyyy")}</td>
                    <td>{m.companyName}</td>
                    <td>{m.fullName}</td>
                    <td>{m.phoneNumber}</td>
                    <td>{m.email}</td>

                    <td>
                      <button
                        style={{
                          display:
                            !(userStatus == userStatusConstant.Request) &&
                            !(userStatus == userStatusConstant.Declined)
                              ? "none"
                              : "inline",
                        }}
                        className="btn btn-approve"
                        onClick={() => acceptEmployer(m.uuid)}
                      >
                        Approve
                      </button>
                      <button
                        style={{
                          display:
                            !(userStatus == userStatusConstant.Request) &&
                            !(userStatus == userStatusConstant.Approved)
                              ? "none"
                              : "inline",
                        }}
                        className="btn btn-decline"
                        onClick={() => handleClickOpen(m.uuid)}
                      >
                        Decline
                      </button>
                      <button
                        style={{
                          display:
                            userStatus == userStatusConstant.Declined
                              ? "inline"
                              : "none",
                        }}
                        className="btn btn-info"
                        onClick={() => handleClickOpenReason(m.rejectedReason)}
                      >
                        View Reason
                      </button>
                    </td>
                  </tr>
                );
              })}
            </tbody>
          </table>
        </div> */}
      </div>

      <Dialog
        fullWidth={true}
        maxWidth={"sm"}
        onClose={handleClose}
        aria-labelledby="customized-dialog-title"
        open={open}
      >
        <DialogTitle id="customized-dialog-title3" onClose={handleClose}>
          Decline Employer
        </DialogTitle>
        <form onSubmit={handleSubmit(onSubmit)} onReset={reset} className="">
          <DialogContent className="text-center" dividers>
            <img style={{ marginTop: "20px" }} src={questionMark} />

            <div className="head-job-pop">
              <div className="blackHead">Type a reason to decline the job</div>
              <div className="shortBorder"></div>
            </div>

            <div className="form-group">
              <label>Enter your reason </label>
              <textarea
                name="rejectedReason"
                id="rejectedReason"
                value={reason}
                ref={register}
                rows={6}
                placeholder="Reason"
                className="form-control"
                onChange={handleInputChange}
              />
              <div className="invalid-feedback">
                {errors.rejectedReason?.message}
              </div>
            </div>

            <div className="text-center job-pop-btns">
              <button onClick={handleClose} className="btn btn-decline">
                Cancel
              </button>
              <button type="submit" className="btn btn-approve">
                Confirm
              </button>
            </div>
          </DialogContent>
        </form>
      </Dialog>

      <Dialog
        fullWidth={true}
        maxWidth={"sm"}
        onClose={handleCloseReason}
        aria-labelledby="customized-dialog-title"
        open={openReason}
      >
        <DialogTitle id="customized-dialog-title3" onClose={handleCloseReason}>
          Reason
        </DialogTitle>

        <DialogContent className="text-center" dividers>
          <img style={{ marginTop: "20px" }} src={decline} />

          <div className="head-job-pop">
            <div className="blackHead">
              We are sorry to inform you that <br />
              we had declined you
            </div>
            <div className="shortBorder"></div>
          </div>

          <p className="blurText">{rejectionReason}</p>

          <div className="text-center job-pop-btns">
            <button onClick={handleCloseReason} className="btn btn-approve">
              OK
            </button>
          </div>
        </DialogContent>
      </Dialog>

      <div className="packageModal">
        <Dialog
          fullWidth={true}
          maxWidth={"sm"}
          onClose={handleClosePackage}
          aria-labelledby="customized-dialog-title"
          open={openPackage}
          id="packageModal"
        >
          <DialogTitle
            id="customized-dialog-title3"
            onClose={handleClosePackage}
          >
            Package
          </DialogTitle>

          <DialogContent className="text-center" dividers>
            <div className="packageBox">
              {/* <i
              onClick={handleClosePackage}
              className='fa fa-times'></i> */}
              <div className="packageUpper">
                <div>Card Name</div>
                <div className="package-price">$10</div>
                <div>per month, paid yearly</div>
              </div>
              <div className="packageBox-body">
                <ul>
                  <li>Talent Screening</li>
                  <li>Test Screening</li>
                </ul>
              </div>
            </div>
          </DialogContent>
        </Dialog>
      </div>
    </React.Fragment>
  );
};

export default EmployerRequest;
