import React, { useEffect, useState } from "react";
import {
  createStyles,
  Theme,
  withStyles,
  WithStyles,
} from "@material-ui/core/styles";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import Grid from "@material-ui/core/Grid";
import thank from "../../../src/assets/thankful.png";
import cross from "../../../src/assets/cross.png";

import Dialog from "@material-ui/core/Dialog";
import MuiDialogTitle from "@material-ui/core/DialogTitle";
import MuiDialogContent from "@material-ui/core/DialogContent";
import MuiDialogActions from "@material-ui/core/DialogActions";

import IconButton from "@material-ui/core/IconButton";
import CloseIcon from "@material-ui/icons/Close";
import Typography from "@material-ui/core/Typography";
import { v4 as uuidv4 } from "uuid";
import {IPackage} from "../../models/IPackage";


import { EditorState, convertToRaw, ContentState } from "draft-js";
import Draft, {
  htmlToDraft,
  EmptyState,
  rawToDraft,
  draftToRaw,
  draftStateToHTML,
} from "react-wysiwyg-typescript";

import draftToHtml from "draftjs-to-html";
import PackageService from "../../services/PackageService";
import { useForm } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import * as Yup from "yup";

const styles = (theme: Theme) =>
  createStyles({
    root: {
      margin: 0,
      padding: theme.spacing(2),
    },
    closeButton: {
      position: "absolute",
      right: theme.spacing(1),
      top: theme.spacing(1),
      color: theme.palette.grey[500],
    },
  });

export interface DialogTitleProps extends WithStyles<typeof styles> {
  id: string;
  children: React.ReactNode;
  onClose: () => void;
}

const DialogTitle = withStyles(styles)((props: DialogTitleProps) => {
  const { children, classes, onClose, ...other } = props;
  return (
    <MuiDialogTitle disableTypography className={classes.root} {...other}>
      <Typography variant="h6">{children}</Typography>
      {onClose ? (
        <IconButton
          aria-label="close"
          className={classes.closeButton}
          onClick={onClose}
        >
          <CloseIcon />
        </IconButton>
      ) : null}
    </MuiDialogTitle>
  );
});

const DialogContent = withStyles((theme: Theme) => ({
  root: {
    padding: theme.spacing(2),
  },
}))(MuiDialogContent);

const DialogActions = withStyles((theme: Theme) => ({
  root: {
    margin: 0,
    padding: theme.spacing(1),
  },
}))(MuiDialogActions);

export default function PackagesListing() {
  const [addPackage, setAddPackage] = React.useState(false);
  const [editPackage, setEditPackage] = React.useState(false);
  const [openDelete, setopenDelete] = React.useState(false);

  
  
  const handleClose = () => {
    setAddPackage(false);
    setEditPackage(false);
    setopenDelete(false);
    setCurrentPackageId(0);
    GetAllPackages();
  };

  const handleOpen = (args: any,packageId: number) => {
    debugger;
    if(args == 'add'){
      setPricePackage (initialPackageState);
      setPackageList(initialPackageList.splice(1,2));
      setAddPackage(true);
    }
    
    else if(args == 'delete'){
      setopenDelete(true);
      setCurrentPackageId(packageId);
    }

    else{
      GetPackageById(packageId);
      setCurrentPackageId(packageId);
      setAddPackage(true);
    }

  };

  const initialPackageList = [
    {
      uuid: "",
      text: "",
      packageId: 0
    },
  ];

  const initialPackageState = {
    packageId: 0, 
    packageTitle : "",
    packagePrice : 0,
  };

  const [packageName, setPackageName] = React.useState("");
  const [packageShown, setPackageShown] = React.useState("none");
  const [packageList, setPackageList] = React.useState(initialPackageList);
  const [currentPackageId, setCurrentPackageId] = React.useState(0);
  const [pricePackage, setPricePackage] = useState(initialPackageState);

  const [allPricePackage, setAllPricePackage] = useState([]);

  const handleInputChange = (event: any) => {
    const { name, value } = event.target;
    setPricePackage({ ...pricePackage, [name]: value });
  };

  function handlePackageChange(event: any) {
    debugger;
    // track input field's state
    setPackageName(event.target.value);
    setPackageShown("none");
  }

  function handlePackageAdd() {
    debugger;
    // add item
    if (packageName != "") {
      const newList = packageList.concat({ text: packageName, uuid: uuidv4(),packageId:0 });

      setPackageList(newList);
      setPackageName("");
    setPackageShown("none");
    }
  }

  function DeletePackageSkill(id: string) {
    debugger;
    const newList = packageList.filter((m) => m.uuid != id);

    setPackageList(newList);
  }

  const [editorState, seteditorState] = useState(htmlToDraft(''));
  const handleDescDraftChange = (draftText: any) => {
    seteditorState(draftText);
    // SAVING HTML TO SERVER
    const convertedHtml = draftToHtml(
      convertToRaw(editorState.getCurrentContent())
    );
  };

  useEffect(() => {
    GetAllPackages();
  }, []);

  const GetAllPackages = () => {
    debugger;
    PackageService
      .GetAllPackages()
      .then((response) => {
        debugger;
        setAllPricePackage(response.data.data.packages);
        setPackageList(response.data.data.packagesFeatures);
      })
      .catch((e) => {
        console.log(e);
      });
  };

  const GetPackageById = (packageId: number) => {
    debugger;
    PackageService
      .GetPackageById(packageId)
      .then((response) => {
        debugger;
        setPricePackage(response.data.data);
        setPackageList(response.data.data.packagesFeatures);
      })
      .catch((e) => {
        console.log(e);
      });
  };

  const DeletePackage = () => {
    debugger;
    PackageService
      .DeletePackage(currentPackageId)
      .then((response) => {
        debugger;
        GetAllPackages();
        handleClose();
      })
      .catch((e) => {
        console.log(e);
      });
  };

  const savePackage = () => {
    debugger;
    var data: IPackage = {
      // if package id > 0 edit else save
      packageId: currentPackageId,
      packageTitle : pricePackage.packageTitle,
      packagePrice : pricePackage.packagePrice,
      packagesFeatures : packageList,
    };

    if (packageList.length > 0) {
      //setShowLoader(true);

      if(currentPackageId > 0){
        // Update package
        PackageService.EditPackage(data).then(
          (response) => {
            handleClose();
          },
          (error) => {
            alert("failed to add price package");
            //setShowLoader(false);
          }
        );
      }

      else{
        // Add new package
        PackageService.AddPackage(data).then(
          (response) => {
            handleClose();
          },
          (error) => {
            alert("failed to add price package");
            //setShowLoader(false);
          }
        );
      }

     
    } else {
      if (packageList.length == 0) {
        setPackageShown("inline");
      }
    }
  };


  // Validation
  const validationSchema = Yup.object().shape({
    packagePrice: Yup.number()
    .typeError('Package price must be a number')
    .positive('Package price must be greater than zero')
    .required('Package price is required'),

    packageTitle: Yup.string().required("Package title is required"),
  });

  // functions to build form returned by useForm() hook
  const { register, handleSubmit, reset, errors } = useForm({
    resolver: yupResolver(validationSchema),
  });

  function onSubmit(data: any) {
    debugger;
    savePackage();
  }

  return (
    <React.Fragment>
      <div className="packages">
      <div className="head-sec">
          <div className="blackHead pull-left">Packages</div>

      <div className='addNewPack'>
      <button
      onClick={() => handleOpen('add',0)}
      className="btn btn-decline"
    >
      <i className="fa fa-plus"></i>
      Add New
    </button>
      </div>
          </div>

          <div className='packages-body'>
              <div className='row'>

              {allPricePackage.map((item) => (
                        <div className='col-md-3'>
                        <div className='packageBox'>
                        <div className='packageUpper'>
                        <div>{item.packageTitle}</div>
                        <div className='package-price'>${item.packagePrice}</div>
                        <div>per month, paid yearly</div>
                        </div>
                        <div className='packageBox-body'>
                        <ul>
                          
                          {packageList.filter((temp) => temp.packageId == item.packageId
                        )
                        .map((k) => {
                          return (
                            <li>
                              {k.text}
                          </li>
                          );
                        })}
                        </ul>
                        </div>
                        </div>
          
                        <div className='packageBtns'>
                        <button
                className="btn btn-green"
                onClick={() => handleOpen('edit',item.packageId)}
              >
              Edit
              </button>
          
              <button
                className="btn btn-red"
                onClick={() => handleOpen('delete',item.packageId)}
              >
              Delete
              </button>
                        </div>
                        </div>
          
                     ))}
              
              </div>
          </div>


      </div>

      <Dialog
        fullWidth={true}
        maxWidth={"sm"}
        onClose={handleClose}
        aria-labelledby="customized-dialog-title"
        open={addPackage}
      >
        <DialogTitle id="customized-dialog-title2" onClose={handleClose}>
        Add a package
        </DialogTitle>

        <DialogContent className="text-center" dividers>
<form onSubmit={handleSubmit(onSubmit)} onReset={reset}>
<div className='row'>
        <div className='form-group col-md-6'>
        <label>Package Title</label>
        
        <input
                      name="packageTitle"
                      id="packageTitle"
                      value={pricePackage.packageTitle}
                      type="text"
                      min="0"
                      ref={register}
                      className={`form-control ${
                        errors.packageTitle ? "is-invalid" : ""
                      }`}
                      onChange={handleInputChange}
                    />
                    <div className="invalid-feedback">
                      {errors.packageTitle?.message}
                    </div>
        </div>

        <div className='form-group col-md-6'>
        <label>Package Price</label>
        <input
                      name="packagePrice"
                      id="packagePrice"
                      value={pricePackage.packagePrice}
                      type="Number"
                      min="0"
                      ref={register}
                      className={`form-control ${
                        errors.packagePrice ? "is-invalid" : ""
                      }`}
                      onChange={handleInputChange}
                    />
                    <div className="invalid-feedback">
                      {errors.packagePrice?.message}
                    </div>
        </div>

        <div className='form-group col-md-12'>
        {/* <label>Description</label>
        <Draft
        editorState={editorState}
        name=""
        id=""
        className='form-control'
        onEditorStateChange={(editorState) =>
        handleDescDraftChange(editorState)
        }
      /> */}
      <label>Package features</label>

      <input
                      className="form-control"
                      value={packageName}
                      onChange={handlePackageChange}
                      type="text"
                    />
                    <span onClick={handlePackageAdd}>
                      <i className="fa fa-plus add-tag"></i>
                    </span>

                    <ul className="tag-output">
                      {packageList.map((item) => (
                        <li key={item.uuid}>
                          {item.text}
                          <span>
                            <i
                              className="fa fa-times close-tag"
                              onClick={() => DeletePackageSkill(item.uuid)}
                            ></i>
                          </span>
                        </li>
                      ))}
                    </ul>

                    <div
                      className="invalid-feedback"
                      style={{ display: packageShown }}
                    >
                      Add at least one feature to the package
                    </div>
                 

        </div>

        </div>
          <div className="text-center job-pop-btns">
            <span onClick={handleClose} className="btn btn-decline">
              Cancel
            </span>
            <button
              type="submit"
              className="btn btn-approve"
            >
              Add
            </button>
          </div>
        
</form>
        
        </DialogContent>
      </Dialog>


      <Dialog
        fullWidth={true}
        maxWidth={"sm"}
        onClose={handleClose}
        aria-labelledby="customized-dialog-title"
        open={editPackage}
      >
        <DialogTitle id="customized-dialog-title2" onClose={handleClose}>
        Edit package
        </DialogTitle>

        <DialogContent className="text-center" dividers>

        <div className='row'>
        <div className='form-group col-md-6'>
        <label>Package Title</label>
        <input className='form-control'/>
        </div>

        <div className='form-group col-md-6'>
        <label>Package Price</label>
        <input className='form-control'/>
        </div>

        <div className='form-group col-md-12'>
        <label>Description</label>
        <Draft
        editorState={editorState}
        name="jobDescription"
        id="jobDescription"
        className='form-control'
        onEditorStateChange={(editorState) =>
        handleDescDraftChange(editorState)
        }
      />
        </div>

        </div>
          <div className="text-center job-pop-btns">
            <button onClick={handleClose} className="btn btn-decline">
              Cancel
            </button>
            <button
              type="button"
              onClick={handleClose}
              className="btn btn-approve"
            >
              Edit
            </button>
          </div>
        </DialogContent>
      </Dialog>



      <Dialog
      fullWidth={true}
      maxWidth={"sm"}
      onClose={handleClose}
      aria-labelledby="customized-dialog-title"
      open={openDelete}
    >
      <DialogTitle id="customized-dialog-title3" onClose={handleClose}>
         Confirmation
      </DialogTitle>
      <DialogContent className="text-center" dividers>
        <img style={{ marginTop: "20px" }} src={cross} />

        <div className="head-job-pop">
          <div className="blackHead">
            Are you sure you want to delete this package?
          </div>
          <div className="shortBorder"></div>
        </div>

        

        <div className="text-center job-pop-btns">
        <button onClick={handleClose} className="btn btn-decline">
       Cancel
          </button>
          <button onClick={() => DeletePackage()} className="btn btn-approve">
           Yes
          </button>
        </div>
      </DialogContent>
      
    </Dialog>

    </React.Fragment>
  );
}
