import { IEmployerDetails } from "../../models/IEmployerDetails";
import employerService from "../../services/EmployerService";
import React, { useState, useEffect } from "react";
import { employerActions } from "../../actions/employerActions";
import { date } from "yup/lib/locale";
import { history } from "../../helpers/history";
import { IAdminSourcersBalances } from "../../models/IAdminSourcersBalances";
import AdminService from "../../services/AdminService";
import DataTable, { createTheme } from "react-data-table-component";
import {
  createStyles,
  Theme,
  withStyles,
  WithStyles,
} from "@material-ui/core/styles";

import shape from "../../../src/assets/shape.png";
import footShape from "../../../src/assets/footShape.png";

import commission from "../../../src/assets/commission.png";
import questionMark from "../../../src/assets/question-mark.png";

import IconButton from "@material-ui/core/IconButton";
import CloseIcon from "@material-ui/icons/Close";
import Typography from "@material-ui/core/Typography";

import Dialog from "@material-ui/core/Dialog";
import MuiDialogTitle from "@material-ui/core/DialogTitle";
import MuiDialogContent from "@material-ui/core/DialogContent";
import MuiDialogActions from "@material-ui/core/DialogActions";

import DataTableExtensions from "react-data-table-component-extensions";
import "react-data-table-component-extensions/dist/index.css";

import arrowRight from "../../assets/arrow-icon.png";
import arrowPrev from "../../assets/arrow-prev.png";
import { WeekFilterConstant } from "../../constants/WeekFilterConstant";
import { ISourcerPaymentModel } from "../../models/ISourcerPaymentModel";
import html2canvas from "html2canvas";

declare const require: any;
const { jsPDF } = require("jspdf");
require("jspdf-autotable");

const styles = (theme: Theme) =>
  createStyles({
    root: {
      margin: 0,
      padding: theme.spacing(2),
    },
    closeButton: {
      position: "absolute",
      right: theme.spacing(1),
      top: theme.spacing(1),
      color: theme.palette.grey[500],
    },
  });

export interface DialogTitleProps extends WithStyles<typeof styles> {
  id: string;
  children: React.ReactNode;
  onClose: () => void;
}

const DialogTitle = withStyles(styles)((props: DialogTitleProps) => {
  const { children, classes, onClose, ...other } = props;
  return (
    <MuiDialogTitle disableTypography className={classes.root} {...other}>
      <Typography variant="h6">{children}</Typography>
      {onClose ? (
        <IconButton
          aria-label="close"
          className={classes.closeButton}
          onClick={onClose}
        >
          <CloseIcon />
        </IconButton>
      ) : null}
    </MuiDialogTitle>
  );
});

const DialogContent = withStyles((theme: Theme) => ({
  root: {
    padding: theme.spacing(2),
  },
}))(MuiDialogContent);

const DialogActions = withStyles((theme: Theme) => ({
  root: {
    margin: 0,
    padding: theme.spacing(1),
  },
}))(MuiDialogActions);

const SourcerBalance = () => {
  const [sourcerBalances, setAllsourcerBalances] = useState<
    IAdminSourcersBalances[]
  >([]);

  var dateFormat = require("dateformat");
  const [commission, setCommission] = useState(0);
  const [sourcerName, setSourcerName] = useState("");
  const [sourcerId, setSourcerId] = useState(0);

  const [today, setToday] = useState(
    new Date(
      new Date().setDate(new Date().getDate() + (6 - new Date().getDay()))
    )
  );
  const [start, setstart] = useState(
    new Date(today.getFullYear(), today.getMonth(), today.getDate())
  );
  const [end, setend] = useState(
    new Date(today.getFullYear(), today.getMonth(), today.getDate() + 7)
  );

  const [friday, setFriday] = useState(
    new Date(
      new Date().setDate(new Date().getDate() + (5 - new Date().getDay()))
    )
  );
  const [saturday, setSaturday] = useState(
    new Date(
      new Date().setDate(new Date().getDate() + (5 - new Date().getDay()) - 6)
    )
  );
  const currentDate = new Date(new Date().setHours(0, 0, 0, 0));

  // new Date().setDate(new Date().getDate()+(5 - (new Date().getDay())))
  // new Date(1615534674110).toLocaleDateString("en-US")

  const getPrevWeeks = (args: any) => {
    // if (args == "next") {
    //   setstart(new Date(end.getFullYear(), end.getMonth(), end.getDate()));
    //   setend(new Date(end.getFullYear(), end.getMonth(), end.getDate() + 7));
    // } else {
    //   setstart(new Date(end.getFullYear(), end.getMonth(), end.getDate() - 14));
    //   setend(new Date(end.getFullYear(), end.getMonth(), end.getDate() - 8));
    // }
    if (args == WeekFilterConstant.Next) {
      setSaturday(
        new Date(friday.getFullYear(), friday.getMonth(), friday.getDate() + 1)
      );
      setFriday(
        new Date(friday.getFullYear(), friday.getMonth(), friday.getDate() + 7)
      );

      console.log(saturday + " " + friday);
      getSourcerBalances(
        new Date(friday.getFullYear(), friday.getMonth(), friday.getDate() + 1),
        new Date(friday.getFullYear(), friday.getMonth(), friday.getDate() + 7)
      );
    } else {
      setSaturday(
        new Date(
          saturday.getFullYear(),
          saturday.getMonth(),
          saturday.getDate() - 7
        )
      );
      setFriday(
        new Date(friday.getFullYear(), friday.getMonth(), friday.getDate() - 7)
      );

      console.log(saturday + " " + friday);

      getSourcerBalances(
        new Date(
          saturday.getFullYear(),
          saturday.getMonth(),
          saturday.getDate() - 7
        ),
        new Date(friday.getFullYear(), friday.getMonth(), friday.getDate() - 7)
      );
    }
  };

  const [open, setOpen] = React.useState(false);
  const [
    sourcerPayment,
    setSourcerPayment,
  ] = React.useState<ISourcerPaymentModel>({
    endDate: friday,
    startDate: saturday,
    sourcerId: 0,
  });

  const handleClose = () => {
    setOpen(false);
    setCommission(0);
    setSourcerName("");
  };

  const handleOpen = (value: any, sId: any, sName: any) => {
    setOpen(true);
    setCommission(value);
    setSourcerName(sName);
    setSourcerId(sId);
    console.log(sId);
  };

  const drawSourcerBalances = sourcerBalances.map(
    (sourcer: IAdminSourcersBalances) => (
      <tr>
        <td>{sourcer.fullName}</td>
        <td>{sourcer.jobCount}</td>
        <td>{sourcer.submissionCount}</td>
        <td>
          <span className="balance-box">{sourcer.totalBalance}$</span>
        </td>
        <td>
          {sourcer.acceptanceRatio > 0 ? sourcer.acceptanceRatio + "%" : "0%"}
        </td>
      </tr>
    )
  );

  const getSourcerBalances = (sat: any, fri: any) => {
    debugger;
    console.log(currentDate);
    AdminService.sourcerBalancesByWeek(sat, fri)
      .then((response) => {
        debugger;
        setAllsourcerBalances(response.data.data);
      })
      .catch((e) => {
        console.log(e);
      });
  };

  const UpdateSourcerPayment = (sat: any, fri: any) => {
    debugger;
    sourcerPayment.startDate = saturday;
    sourcerPayment.endDate = friday;
    sourcerPayment.sourcerId = sourcerId;

    console.log(sat + " " + fri);
    AdminService.UpdateAdminSourcersBalance(sourcerPayment)
      .then((response) => {
        debugger;
        getSourcerBalances(saturday, friday);
        handleClose();
      })
      .catch((e) => {
        console.log(e);
      });
  };

  useEffect(() => {
    getSourcerBalances(saturday, friday);
  }, []);

  const columns = [
    {
      name: "Sourcer",
      selector: "fullName",
      sortable: true,
    },
    {
      name: "Job Applied",
      selector: "jobCount",
      sortable: true,
    },
    {
      name: "No. candidates",
      selector: "submissionCount",
      sortable: true,
    },
    {
      name: "Approved",
      selector: "approved",
      sortable: true,
      cell: (row: any) => (
        <span>{row.acceptanceCount > 0 ? row.acceptanceCount : "0"}</span>
      ),
    },

    {
      name: "Declined",
      selector: "declined",
      sortable: true,
      cell: (row: any) => (
        <span>{row.declinedCount > 0 ? row.declinedCount : "0"}</span>
      ),
    },
    {
      name: "Rate",
      selector: "rate",
      sortable: true,
      cell: (row: any) => (
        <span className="balance-box">
          {row.acceptanceRatio > 0 ? row.acceptanceRatio + "%" : "0%"}
        </span>
      ),
    },
    {
      name: "Balance",
      selector: "",
      sortable: true,
      cell: (row: any) => (
        <span className="balance-box">{row.totalBalance}$</span>
      ),
    },
    {
      name: "Action",
      selector: "",
      sortable: true,
      cell: (row: any) => {
        return row.isPaid == true ? (
          currentDate >= new Date(saturday.setHours(0, 0, 0, 0)) &&
          currentDate <= new Date(friday.setHours(0, 0, 0, 0)) ? (
            <button className="btn btn-warning">Pending</button>
          ) : (
            <button className="btn btn-decline">Paid</button>
          )
        ) : (
          <button
            onClick={() => {
              handleOpen(row.totalBalance, row.sourcerId, row.fullName);
            }}
            className="btn btn-success"
            disabled={row.totalBalance == "0.000"}
          >
            pay
          </button>
        );
      },
    },
  ];

  // const exportPDF = () => {
  //   // const input = document.getElementById("pdf-element");
  //   // document.getElementById("pdf-element").style.fontFamily  = "sans-serif";

  //   // const pdf = new jsPDF({ unit: "pt", format: "A4", userUnit: "pt" });
  //   // pdf.html(input, { html2canvas: { scale:0.58 } }).then(() => {
  //   //   pdf.save("test.pdf");
  //   // });

  //   const input = document.getElementById("pdf-element");
  //   html2canvas(input).then((canvas) => {
  //     const imgData = canvas.toDataURL("image/png");
  //     const pdf = new jsPDF({ unit: "pt", format: "A4", userUnit: "pt" });
  //     pdf.addImage(imgData, "JPEG", 0, 0, 600, 700, "", "NONE", 0);
  //     pdf.save("Sourcer Balances Report.pdf");
  //   });
  // };
  const exportPDF = () => {
    const unit = "pt";
    const size = "A4"; // Use A1, A2, A3 or A4
    const orientation = "portrait"; // portrait or landscape

    const marginLeft = 300;
    const doc = new jsPDF(orientation, unit, size);

    doc.setFontSize(15);
    var reportName = "Sourcer Balance";
    var startDate = saturday;
    var endDate = friday;

    const title =
      "" +
      reportName +
      "\n\n" +
      dateFormat(saturday, "dd-mm-yyyy") +
      " to " +
      dateFormat(friday, "dd-mm-yyyy");
    const headers = [
      [
        "Sourcer",
        "Job Applied",
        "No of Candidates",
        "Approved",
        "Declined",
        "Rate",
        "Balance",
      ],
    ];

    const data = sourcerBalances.map((elt) => [
      elt.fullName,
      elt.jobCount,
      elt.submissionCount,
      elt.acceptanceCount > 0 ? elt.acceptanceCount : "0",
      elt.declinedCount > 0 ? elt.declinedCount : "0",
      elt.acceptanceRatio > 0 ? elt.acceptanceRatio + "%" : "0%",
      elt.totalBalance + "$",
    ]);

    let content = {
      startY: 100,
      head: headers,
      body: data,
    };

    var options = {
      align: "center",
    };

    doc.text(title, marginLeft, 40, options);
    doc.autoTable(content);
    doc.save("report.pdf");
  };

  return (
    <React.Fragment>
      <div className="listing-admin job-req">
        {/* <div>Start :{start.toLocaleDateString()}</div>
      <div>End :{end.toLocaleDateString()}</div> */}
        <div className="head-sec">
          <div className="blackHead">Sourcer Balance</div>
        </div>

        <div className="weekFilter">
          <div className="text-center" style={{ paddingBottom: "30px 0" }}>
            <span className="soft-text">Today : </span>
            {new Date().toLocaleDateString()}
          </div>

          <div
            className="bold-head text-center"
            style={{ marginBottom: "30px" }}
          >
            <span
              className="weekText"
              onClick={() => getPrevWeeks(WeekFilterConstant.Previous)}
            >
              <img src={arrowPrev} />
              Previous Week
            </span>
            <span className="soft-text">Sat: </span>{" "}
            {saturday.toLocaleDateString()}
            <span style={{ margin: "0 20px" }}>to</span>
            <span className="soft-text">Fri: </span>{" "}
            {friday.toLocaleDateString()}
            <span
              className="weekText"
              onClick={() => getPrevWeeks(WeekFilterConstant.Next)}
            >
              Next Week
              <img src={arrowRight} />
            </span>
          </div>
          {/* <div className="btns"></div> */}

          <div className="btn-pdf">
            <button className="btn btn-approve" onClick={exportPDF}>
              Download as PDF
            </button>

            {/* <button className="btn btn-approve"
            onClick={() => {
              history.push("/invoice");
            }}
            >
              Download as PDF
            </button> */}
          </div>

          <div className="invoiceParent" id="pdf-element">
            {/* <div className="invoiceTop">
              <div className="row">
                <div className="col-md-8">
                  <div className="invoiceTopText">Crowd Recruitment</div>
                  <div className="invoiceTopText">
                    King Abdullah Rd, Al Hamra, 2nd Floor, Deem Plaza
                  </div>
                  <div className="invoiceTopText">
                    2nd Floor, Deem Plaza 6389
                  </div>
                  <div className="invoiceTopText">Riyadh 13216</div>
                  <div className="invoiceTopText">Phone: +966503230508</div>
                  <div className="invoiceTopText">
                    Email: sales@assisted.vip{" "}
                  </div>
                  <div className="invoiceTopText">Riyadh 13216</div>
                </div>
                <div className="col-md-4">
                  <div className="logoCR">CR</div>
                </div>
              </div>
            </div>

            <div className="invoiceSecond">
              <img className="secondFoot" src={shape} />
              <div className="row">
                <div className="col-md-4 text-center">
                  <div>
                    <div className="inHead">Start Date</div>
                    <div className="inSubHead">12/12/11</div>
                  </div>
                </div>

                <div className="col-md-4 text-center">
                  <div>
                    <div className="inHead">Edited to</div>
                    <div className="inSubHead">Testing</div>
                  </div>
                </div>

                <div className="col-md-4 text-center">
                  <div>
                    <div className="inHead">Administrator</div>
                    <div className="inSubHead">Ahmed Bashir</div>
                  </div>
                </div>
              </div>

              <div className="row">
                <div className="col-md-4 text-center">
                  <div>
                    <div className="inHead">End Date</div>
                    <div className="inSubHead">12/12/11</div>
                  </div>
                </div>

                <div className="col-md-4 text-center">
                  <div>
                    <div className="inHead">Total</div>
                    <div className="inSubHead">121</div>
                  </div>
                </div>

                <div className="col-md-4 text-center">
                  <div>
                    <div className="inHead">Phone</div>
                    <div className="inSubHead">2323232323</div>
                  </div>
                </div>
              </div>
            </div> */}

            {/* <div className="invoiceTable"> */}
            {/* <img className="tableFoot" src={footShape} /> */}
            <div className="table-responsive">
              <DataTableExtensions columns={columns} data={sourcerBalances}>
                <DataTable
                  columns={columns}
                  data={sourcerBalances}
                  pagination={true}
                />
              </DataTableExtensions>
              {/* <div className="totalInvoice">
                  <div className="totalRow blue bold">
                    <span className="textIn">Total</span>
                    <span className="float-right">0,SAR90</span>
                  </div>
                </div> */}
              {/* </div> */}
            </div>
          </div>
        </div>
      </div>

      <Dialog
        fullWidth={true}
        maxWidth={"sm"}
        onClose={handleClose}
        aria-labelledby="customized-dialog-title"
        open={open}
      >
        <DialogTitle id="customized-dialog-title2" onClose={handleClose}>
          Payment Confirmation
        </DialogTitle>

        <DialogContent className="text-center" dividers>
          <img style={{ marginTop: "20px" }} src={questionMark} />

          <div className="head-job-pop">
            <div className="blackHead">
              Are you sure You want to give {commission} $ commission for{" "}
              {sourcerName}
            </div>
            <div className="shortBorder"></div>
          </div>

          <div className="text-center job-pop-btns">
            <button onClick={handleClose} className="btn btn-decline">
              Cancel
            </button>
            <button
              type="button"
              onClick={() => {
                UpdateSourcerPayment(saturday, friday);
              }}
              className="btn btn-approve"
            >
              Confirm
            </button>
          </div>
        </DialogContent>
      </Dialog>
    </React.Fragment>
  );
};

export default SourcerBalance;
