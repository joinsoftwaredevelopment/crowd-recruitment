import React, { useState, useEffect } from "react";
import {
  createStyles,
  Theme,
  withStyles,
  WithStyles,
} from "@material-ui/core/styles";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import Grid from "@material-ui/core/Grid";
import thank from "../../../src/assets/thankful.png";
import decline from "../../../src/assets/decline.png";
import empIco from "../../../src/assets/emp-ico.png";
import Switch from "@material-ui/core/Switch";
import CircularProgress from "@material-ui/core/CircularProgress";
import { Link } from "react-router-dom";

import IconButton from "@material-ui/core/IconButton";
import CloseIcon from "@material-ui/icons/Close";
import Typography from "@material-ui/core/Typography";
import { IJobSourcers } from "../../models/IJobSourcers";
import AdminService from "../../services/AdminService";
import candIco from "../../../src/assets/cand-ico.png";
import { IJobCandidate } from "../../models/IJobCandidate";
import { useHistory } from "react-router-dom";
import JobService from "../../services/JobService";
import sourcerService from "../../services/SourcerService";
import { SubmissionStatusConstant } from "../../constants/SubmissionStatusConstant";

export default function ReviewSourcerCandidates(props: any) {
  const [jobSourcers, setAlljobSourcers] = useState<IJobSourcers[]>([]);
  const [allJobCandidates, setAllJobCandidates] = useState<IJobCandidate[]>([]);
  const history = useHistory();
  const [sourcerUuId, setSourcerUuId] = useState("");
  const [submissionStatus, setsubmissionStatus] = useState(2);
  const drawjobSourcers = jobSourcers.map((sourcer: IJobSourcers) => (
    <div className="cand-row">
      <div className="left">
        <div className="data-card-head">{sourcer.sourcerName}</div>
        <div className="data-card-light">{sourcer.sourcerJobTitle}</div>
      </div>
      <div
        className="right"
        onClick={() =>
          getAllJobCandidates(
            sourcer.sourcerUuId,
            SubmissionStatusConstant.Submitted
          )
        }
      >
        <div className="blue">{sourcer.submissionCount} submissions</div>
      </div>
    </div>
  ));

  const drawAllJobCandidates = allJobCandidates.map((job: IJobCandidate) => (
    <div className="col-md-6" key={job.rowNumber}>
      <div className="job-box">
        {/* <span className="red declined">Declined by employer</span> */}
        <span
          className={
            job.candidateSubmissionStatusId == 4 ||
            job.candidateSubmissionStatusId == 6
              ? "red declined"
              : "green declined"
          }
        >
          {job.candidateSubmissionStatusId == 2 ? "  Pending" : ""}
          {job.candidateSubmissionStatusId == 3 ? "  Approved" : ""}
          {job.candidateSubmissionStatusId == 4 ? "  Disqualified" : ""}
          {job.candidateSubmissionStatusId == 5 ? "  Approved By Employer" : ""}
          {job.candidateSubmissionStatusId == 6 ? "  Declined By Employer" : ""}
        </span>
        <div className="remain-img-box">
          <img src={candIco} />
        </div>

        <div className="data-card-head">{job.candidateName}</div>
        <div className="data-card-light">{job.cvName}</div>
        <div className="data-card-light">Job: {job.jobTitle}</div>
        {/* <div className="data-card-light">
          Status:
          {job.candidateSubmissionStatusId == 2 ? "  Pending" : ""}
          {job.candidateSubmissionStatusId == 3 ? "  Approved" : ""}
          {job.candidateSubmissionStatusId == 4 ? "  Disqualified" : ""}
        </div> */}
        <Link className="" to={"/admin/review-candidate/" + job.submissionUuId}>
          Show Details
        </Link>

        <div className="job-card-btm" style={{ display: "flow-root" }}>
          <span className="" style={{ float: "left" }}>
            <a target="_blank" href={job.linkedInProfile}>
              <span className="table-link">in</span>
            </a>
            {/* <span className="table-link">
              {" "}
              <i className="fa fa-phone"></i>{" "}
            </span>
            <span className="table-link">
              <i className="fa fa-envelope"></i>
            </span> */}
          </span>

          <span className="data-card-light cand-light-text">
            Sourced By : {job.sourcerName}
          </span>
        </div>
      </div>
    </div>
  ));

  const getAllJobCandidates = (
    sourcerUuId: string,
    submissionStatusId: number
  ) => {
    debugger;
    setSourcerUuId(sourcerUuId);
    setsubmissionStatus(submissionStatusId);
    sourcerService
      .GetSourcerCandidatesByStatus(
        sourcerUuId,
        submissionStatusId,
        props.match.params.id
      )
      .then((response) => {
        debugger;
        if (response.data.data != null) setAllJobCandidates(response.data.data);
      })
      .catch((e) => {
        console.log(e);
      });
  };

  const getjobSourcers = () => {
    debugger;
    AdminService.GetJobSourcers(props.match.params.id)
      .then((response) => {
        debugger;
        setAlljobSourcers(response.data.data);
      })
      .catch((e) => {
        console.log(e);
      });
  };

  useEffect(() => {
    getjobSourcers();
  }, []);

  return (
    <React.Fragment>
      <div className="emp-job">
        <div className="row">
          <div className="col-md-12">
            <div className="bread-text">
              <a className="lt-text" href={"/admin/job-requests"}>
                Job Requests
              </a>
              <span className="lt-text">
                <i className="fas fa-angle-right"></i>
              </span>
              <span className="lt-head">Sourcers Candidates</span>
            </div>

            <div className="job-bg" style={{ margin: "0" }}>
              <div className="head-sec">
                <div className="row">
                  <div className="col-md-4">
                    <div className="bread-text">
                      <span className="lt-head">Sourcer</span>
                    </div>
                  </div>

                  <div className="col-md-8">
                    <div className="bread-text">
                      <span className="lt-head">Candidates</span>
                    </div>

                    <div className="filter">
                      <span
                        onClick={() =>
                          getAllJobCandidates(
                            sourcerUuId,
                            SubmissionStatusConstant.Submitted
                          )
                        }
                        className={
                          submissionStatus == SubmissionStatusConstant.Submitted
                            ? "active-filter highlight-filter border-rad-L-6"
                            : "non-active-filter border-rad-L-6"
                        }
                      >
                        Pending
                      </span>
                      <span
                        onClick={() =>
                          getAllJobCandidates(
                            sourcerUuId,
                            SubmissionStatusConstant.Approved
                          )
                        }
                        className={
                          submissionStatus == SubmissionStatusConstant.Approved
                            ? "active-filter highlight-filter"
                            : "non-active-filter"
                        }
                      >
                        Approved
                      </span>
                      <span
                        onClick={() =>
                          getAllJobCandidates(
                            sourcerUuId,
                            SubmissionStatusConstant.Disqualified
                          )
                        }
                        className={
                          submissionStatus ==
                          SubmissionStatusConstant.Disqualified
                            ? "active-filter highlight-filter border-rad-R-6"
                            : "non-active-filter border-rad-R-6"
                        }
                      >
                        Disqualified
                      </span>
                    </div>
                  </div>
                </div>
              </div>

              <div className="row">
                <div className="col-md-4">
                  <div className="cand-box">
                    <div className="cand-head">Sourcers</div>
                    {drawjobSourcers}
                  </div>
                </div>
                <div className="col-md-8">
                  <div className="row empJB">{drawAllJobCandidates}</div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </React.Fragment>
  );
}
