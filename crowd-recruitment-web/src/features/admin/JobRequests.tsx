import React, { useState, useEffect } from "react";
import {
  createStyles,
  Theme,
  withStyles,
  WithStyles,
} from "@material-ui/core/styles";
import DataTable, { createTheme } from "react-data-table-component";
import { ISourcerDetails } from "../../models/ISourcerDetails";
import sourcerService from "../../services/SourcerService";
import { Link } from "react-router-dom";
import cross from "../../../src/assets/tbl-cross.png";
import tick from "../../../src/assets/tbl-tick.png";
import eye from "../../../src/assets/tbl-eye.png";
import Button from "@material-ui/core/Button";
import commission from "../../../src/assets/commission.png";
import questionMark from "../../../src/assets/question-mark.png";

import IconButton from "@material-ui/core/IconButton";
import CloseIcon from "@material-ui/icons/Close";
import Typography from "@material-ui/core/Typography";

import Dialog from "@material-ui/core/Dialog";
import MuiDialogTitle from "@material-ui/core/DialogTitle";
import MuiDialogContent from "@material-ui/core/DialogContent";
import MuiDialogActions from "@material-ui/core/DialogActions";

import commissionService from "../../services/CommissionService";
import { IAcceptJob } from "../../models/IAcceptJob";
import { IDeclineJob } from "../../models/IDeclineJob";
import JobService from "../../services/JobService";
// import { IJobDetails } from "../../models/IJobDetails";
// import { IJobRequestsDetails } from "../../models/IJobRequestsDetails";

import { useForm } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import * as Yup from "yup";
import { useHistory } from "react-router-dom";
import { jobStatusConstant } from "../../constants/jobStatusConstant";

import imgDecline from "../../../src/assets/decline.png";
import { userStatusConstant } from "../../constants/userStatusConstant";

import DataTableExtensions from "react-data-table-component-extensions";
import "react-data-table-component-extensions/dist/index.css";

import jsPDF from "jspdf";
import html2canvas from "html2canvas";
import { IJobRequestsDetails } from "../../models/IJobRequestsDetails";

const styles = (theme: Theme) =>
  createStyles({
    root: {
      margin: 0,
      padding: theme.spacing(2),
    },
    closeButton: {
      position: "absolute",
      right: theme.spacing(1),
      top: theme.spacing(1),
      color: theme.palette.grey[500],
    },
  });

export interface DialogTitleProps extends WithStyles<typeof styles> {
  id: string;
  children: React.ReactNode;
  onClose: () => void;
}

const DialogTitle = withStyles(styles)((props: DialogTitleProps) => {
  const { children, classes, onClose, ...other } = props;
  return (
    <MuiDialogTitle disableTypography className={classes.root} {...other}>
      <Typography variant="h6">{children}</Typography>
      {onClose ? (
        <IconButton
          aria-label="close"
          className={classes.closeButton}
          onClick={onClose}
        >
          <CloseIcon />
        </IconButton>
      ) : null}
    </MuiDialogTitle>
  );
});

const DialogContent = withStyles((theme: Theme) => ({
  root: {
    padding: theme.spacing(2),
  },
}))(MuiDialogContent);

const DialogActions = withStyles((theme: Theme) => ({
  root: {
    margin: 0,
    padding: theme.spacing(1),
  },
}))(MuiDialogActions);

const JobRequests = () => {
  // const classes = useStyles();
  const [active, setActive] = useState(true);
  const [openApprove, setOpenApprove] = React.useState(false);
  const [openSure, setOpenSure] = React.useState(false);
  const [openDecline, setOpenDecline] = React.useState(false);
  const [openDetails, setopenDetails] = React.useState(false);
  const [jobOpt, setJobOpt] = useState(1);
  const history = useHistory();
  const [jobDetails, setjobDetails] = useState<IJobRequestsDetails>({
    uuid: "",
    jobTitle: "",
    employmentTypeName: "",
    seniorityLevelName: "",
    companiesNotToSourceFrom: "",
    languageId: 2,
    jobLocation: "",
    companyName: "",
    mustHaveQualification: "",
    niceToHaveQualification: "",
    timetoHireId: 0,
    timetoHireName: "",
    experienceLevelName: "",
    description: "",
    companyIndustry: "",
    jobStatusId: 2,
    jobStatusName: "",
    requirements: "",
    noOfSubmissions: 0,
    jobDate: new Date(),
    peopleToHireCount: 0,
    findingJobDifficultyLevelId: 0,
    minSalary: 0,
    maxSalary: 0,
    firstMailSubject: "",
    firstMailDescription: "",
    findingJobDifficultyName: "",
    currencyName: "",
  });

  // get requests
  const [requests, setRequests] = useState<any[]>([]);

  // approve request
  const initialAcceptState = {
    uuid: "",
    numberOfSubmissions: 1,
    commissionVaue: 1,
  };

  const initialDeclineState = {
    uuid: "",
    rejectionReason: "",
  };

  const [approve, setApprove] = useState(initialAcceptState);
  const [decline, setDecline] = useState(initialDeclineState);

  const [userStatus, setSetUserStatus] = useState(2);
  const [openReason, setOpenReason] = useState(false);
  const [rejectionReason, setRejectionReason] = React.useState("");

  const [currentUuid, setCurrentUuid] = useState("");

  var data: IAcceptJob = {
    uuid: "",
    numberOfSubmissions: approve.numberOfSubmissions,
    commissionVaue: approve.commissionVaue,
  };

  const handleClickOpenReason = (reason: any) => {
    setOpenReason(true);
    setRejectionReason(reason);
  };

  const handleCloseReason = () => {
    setOpenReason(false);
  };

  var declineData: IDeclineJob = {
    uuid: "",
    rejectionReason: decline.rejectionReason,
  };

  const handleInputChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    debugger;
    const { name, value } = event.target;
    setApprove({ ...approve, [name]: value });
  };

  const GetJobOpt = (args: any) => {
    setJobOpt(args);
  };

  const handleDeclineInputChange = (event: React.ChangeEvent<any>) => {
    debugger;
    const { name, value } = event.target;
    setDecline({ ...decline, [name]: value });
  };

  const acceptJob = () => {
    debugger;
    approve.uuid = currentUuid;
    commissionService
      .AcceptJob(approve)
      .then((response) => {
        retrieveRequests(2);
        handleClose();
        approve.commissionVaue = 1;
        approve.numberOfSubmissions = 1;
      })
      .catch((e) => {
        console.log(e);
      });
  };

  // form validation rules
  const validationSchema = Yup.object().shape({
    rejectionReason: Yup.string().required("Rejection reason is required"),
    //numberOfSubmissions: Yup.string().required("Number of submissions reason is required"),
    //commissionVaue: Yup.string().required("Commission vaue is required")
  });

  const validationApproveSchema = Yup.object().shape({
    
    numberOfSubmissions: Yup.number()
    .typeError('Number of submissions must be a number')
    .positive('Number of submissions must be greater than zero')
    .required('Number of submissions is required'),

    commissionVaue: Yup.number()
    .typeError('Commission Vaue must be a number')
    .positive('Commission Vaue must be greater than zero')
    .required('Commission Vaue is required'),
  });

  var dateFormat = require("dateformat");

  // functions to build form returned by useForm() hook
  const { register, handleSubmit, reset, errors } = useForm({
    resolver: yupResolver(validationSchema),
  });

  const {
    register: register2,
    handleSubmit: handleSubmit2,
    reset: reset2,
    errors: errors2,
  } = useForm({
    resolver: yupResolver(validationApproveSchema),
    mode: "onBlur",
  });

  //function onSubmit(data:any) {
  //saveEmployee();
  //}

  function onSubmit(data: any) {
    // display form data on success
    //alert(data);
    debugger;
    DeclineJob();
  }

  function onSubmit2(data: any) {
    // display form data on success
    //alert(data);
  }

  const DeclineJob = () => {
    debugger;
    decline.uuid = currentUuid;
    commissionService
      .DeclineJob(decline)
      .then((response) => {
        retrieveRequests(2);
        decline.rejectionReason = "";
        handleClose();
      })
      .catch((e) => {
        console.log(e);
      });
  };

  // Get All
  useEffect(() => {
    retrieveRequests(2);
  }, []);

  const retrieveRequests = (statusId: number) => {
    debugger;

    commissionService
      .GetAllJobs(statusId)
      .then((response) => {
        debugger;
        setRequests(response.data.data);
        setSetUserStatus(statusId);
      })
      .catch((e) => {
        console.log(e);
      });
  };

  const getJobDetails = (uuid: string) => {
    debugger;
    JobService.getJobDetails(uuid)
      .then((response) => {
        debugger;
        setjobDetails(response.data.data);
      })
      .catch((e) => {
        console.log(e);
      });
  };

  const changeActive = (isActive: boolean) => {
    setActive(isActive);
    debugger;
    if (isActive == false) {
      retrieveRequests(3);
    } else {
      retrieveRequests(2);
    }
  };

  const handleClickOpen = (paramType: any, uuid: string) => {
    debugger;
    console.log(paramType);
    if (paramType == "tick") {
      setOpenApprove(true);
    }
    if (paramType == "sure") {
      if (
        errors2.commissionVaue != null ||
        errors2.numberOfSubmissions != null
      ) {
        setOpenSure(false);
        setOpenApprove(true);
      } else {
        debugger;
        setOpenApprove(false);
        setOpenSure(true);
      }
    }
    if (paramType == "cross") {
      setOpenDecline(true);
    }
    if (paramType == "eye") {
      setopenDetails(true);
      getJobDetails(uuid);
    }

    if (uuid != "") {
      setCurrentUuid(uuid);
    }
  };
  const handleClose = () => {
    setOpenApprove(false);
    setOpenSure(false);
    setOpenDecline(false);
    setopenDetails(false);
  };

  const columns = [
    {
      name: "Date",
      selector: "jobDate",
      sortable: true,
      cell: (row: any) => (
        <span>{dateFormat(row.jobDate, "d mmmm, yyyy")}</span>
      ),
    },
    {
      name: "Employer",
      selector: "employerName",
      sortable: true,
    },
    {
      name: "Job",
      selector: "jobTitle",
      sortable: true,
    },
    {
      name: "Location",
      selector: "jobLocation",
      sortable: true,
    },
    {
      name: "Action",
      minWidth: "230px",
      selector: "jobLocation",
      sortable: false,
      cell: (row: any) => (
        <span className="action-links-job">
          <img
            onClick={(event) => handleClickOpen("eye", row.uuid)}
            src={eye}
          />
          <img
            style={{
              display:
                userStatus == jobStatusConstant.Posted ||
                userStatus == jobStatusConstant.Declined
                  ? "inline"
                  : "none",
            }}
            onClick={(event) => handleClickOpen("tick", row.uuid)}
            src={tick}
          />
          <img
            style={{
              display:
                userStatus == jobStatusConstant.Posted ||
                userStatus == jobStatusConstant.Opened
                  ? "inline"
                  : "none",
            }}
            src={cross}
            onClick={(event) => handleClickOpen("cross", row.uuid)}
          />

          <button
            style={{
              display:
                userStatus == jobStatusConstant.Declined ? "inline" : "none",
            }}
            className="btn btn-info"
            onClick={() => handleClickOpenReason(row.rejectionReason)}
          >
            View Reason
          </button>

          <button
            className="btn btn-approve"
            style={{
              display:
                userStatus == jobStatusConstant.Opened ? "inline" : "none",
            }}
            onClick={() => history.push("/admin/review-sourcers/" + row.uuid)}
          >
            Candidates
          </button>
        </span>
      ),
    },
  ];
  return (
    <React.Fragment>
      <div className="listing-admin job-req">
        <div className="head-sec">
          <div className="blackHead">Job requests</div>

          <div className="filter">
            <span
              id=""
              onClick={() => retrieveRequests(2)}
              className={
                userStatus == jobStatusConstant.Posted
                  ? "active-filter highlight-filter border-rad-L-6"
                  : "non-active-filter border-rad-L-6"
              }
            >
              Pending
            </span>
            <span
              id=""
              onClick={() => retrieveRequests(3)}
              className={
                userStatus == jobStatusConstant.Opened
                  ? "active-filter highlight-filter"
                  : "non-active-filter"
              }
            >
              Approved
            </span>
            <span
              id=""
              onClick={() => retrieveRequests(4)}
              className={
                userStatus == jobStatusConstant.Declined
                  ? "active-filter highlight-filter border-rad-R-6"
                  : "non-active-filter border-rad-R-6"
              }
            >
              Declined
            </span>
          </div>
        </div>

        <div id="capture">
          <DataTableExtensions columns={columns} data={requests}>
            <DataTable pagination title="" columns={columns} data={requests} />
          </DataTableExtensions>
        </div>

        {/* <div className="table-responsive">
          <table className="table">
            <thead>
              <tr>
                <th>Date</th>
                <th>Employer</th>
                <th>Job</th>
                <th>Location</th>
                <th>Action</th>
              </tr>
            </thead>
            <tbody>
              {requests.map((m) => {
                return (
                  <tr>
                    <td>{dateFormat(m.jobDate, "d mmmm, yyyy")}</td>

                    <td>{m.employerName}</td>

                    <td>{m.jobTitle}</td>
                    <td>{m.jobLocation}</td>

                    <td className="action-links-job">
                      <img
                        onClick={(event) => handleClickOpen("eye", m.uuid)}
                        src={eye}
                      />
                      <img
                        style={{
                          display:
                            userStatus == jobStatusConstant.Posted ||
                            userStatus == jobStatusConstant.Declined
                              ? "inline"
                              : "none",
                        }}
                        onClick={(event) => handleClickOpen("tick", m.uuid)}
                        src={tick}
                      />
                      <img
                        style={{
                          display:
                            userStatus == jobStatusConstant.Posted ||
                            userStatus == jobStatusConstant.Opened
                              ? "inline"
                              : "none",
                        }}
                        src={cross}
                        onClick={(event) => handleClickOpen("cross", m.uuid)}
                      />

                      <button
                        style={{
                          display:
                            userStatus == jobStatusConstant.Declined
                              ? "inline"
                              : "none",
                        }}
                        className="btn btn-info"
                        onClick={() => handleClickOpenReason(m.rejectionReason)}
                      >
                        View Reason
                      </button>

                      <button
                        className="btn btn-approve"
                        style={{
                          display:
                            userStatus == jobStatusConstant.Opened
                              ? "inline"
                              : "none",
                        }}
                        onClick={() =>
                          history.push("/admin/review-sourcers/" + m.uuid)
                        }
                      >
                        Candidates
                      </button>
                    </td>
                  </tr>
                );
              })}
            </tbody>
          </table>
        </div> */}
      </div>

      <Dialog
        fullWidth={true}
        maxWidth={"sm"}
        onClose={handleClose}
        aria-labelledby="customized-dialog-title"
        open={openApprove}
      >
        <DialogTitle id="customized-dialog-title1" onClose={handleClose}>
          Commission
        </DialogTitle>
        <form className="" onSubmit={handleSubmit2(onSubmit2)}>
          <DialogContent className="text-center" dividers>
            <img style={{ marginTop: "20px" }} src={commission} />

            <div className="head-job-pop">
              <div className="blackHead">
                Enter the commission for each candidate
              </div>
              <div className="shortBorder"></div>
            </div>

            <div className="form-group">
              <label>Commission for candidate in dollars ($) </label>
              <input
                type="number"
                min="0"
                name="commissionVaue"
                id="commissionVaue"
                placeholder="write value here ..."
                className="form-control"
                value={approve.commissionVaue}
                onChange={handleInputChange}
                ref={register2}
              />
              <div className="invalid-feedback">
                {errors2.commissionVaue?.message}{" "}
              </div>
            </div>

            <div className="form-group">
              <label>Number of submissions </label>
              <input
                min="1"
                type="number"
                //min="1"
                name="numberOfSubmissions"
                id="numberOfSubmissions"
                placeholder="write number here ..."
                className="form-control"
                value={approve.numberOfSubmissions}
                onChange={handleInputChange}
                ref={register2}
              />
              <div className="invalid-feedback">
                {errors2.numberOfSubmissions?.message}{" "}
              </div>
            </div>

            <div className="text-center job-pop-btns">
              <button onClick={handleClose} className="btn btn-decline">
                Cancel
              </button>
              <button
                className="btn btn-approve"
                onClick={(event) => handleClickOpen("sure", "")}
              >
                Submit
              </button>
            </div>
          </DialogContent>
        </form>
      </Dialog>

      <Dialog
        fullWidth={true}
        maxWidth={"sm"}
        onClose={handleClose}
        aria-labelledby="customized-dialog-title"
        open={openSure}
      >
        <DialogTitle id="customized-dialog-title2" onClose={handleClose}>
          Commission
        </DialogTitle>

        <DialogContent className="text-center" dividers>
          <img style={{ marginTop: "20px" }} src={questionMark} />

          <div className="head-job-pop">
            <div className="blackHead">
              Are you sure You want to give {approve.commissionVaue} $
              commission for each candidate
            </div>
            <div className="shortBorder"></div>
          </div>

          <div className="text-center job-pop-btns">
            <button onClick={handleClose} className="btn btn-decline">
              Cancel
            </button>
            <button
              type="button"
              onClick={acceptJob}
              className="btn btn-approve"
            >
              Confirm
            </button>
          </div>
        </DialogContent>
      </Dialog>

      <Dialog
        fullWidth={true}
        maxWidth={"sm"}
        onClose={handleClose}
        aria-labelledby="customized-dialog-title"
        open={openDecline}
      >
        <DialogTitle id="customized-dialog-title3" onClose={handleClose}>
          Decline Job
        </DialogTitle>
        <form className="" onSubmit={handleSubmit(onSubmit)}>
          <DialogContent className="text-center" dividers>
            <img style={{ marginTop: "20px" }} src={questionMark} />

            <div className="head-job-pop">
              <div className="blackHead">Type a reason to decline the job</div>
              <div className="shortBorder"></div>
            </div>

            <div className="form-group">
              <label>Enter your reason </label>
              <textarea
                name="rejectionReason"
                id="rejectionReason"
                value={decline.rejectionReason}
                placeholder="Reason"
                rows={6}
                className="form-control"
                onChange={handleDeclineInputChange}
                ref={register}
              />
              <div className="invalid-feedback">
                {errors.rejectionReason?.message}
              </div>
            </div>

            <div className="text-center job-pop-btns">
              <button onClick={handleClose} className="btn btn-decline">
                Cancel
              </button>
              <button type="submit" className="btn btn-approve">
                Confirm
              </button>
            </div>
          </DialogContent>
        </form>
      </Dialog>

      <Dialog
        fullWidth={true}
        maxWidth={"lg"}
        onClose={handleClose}
        aria-labelledby="customized-dialog-title"
        open={openDetails}
      >
        <DialogTitle id="customized-dialog-title4" onClose={handleClose}>
          {jobDetails.jobTitle}
        </DialogTitle>

        <DialogContent dividers>
          <div className="jobOpts">
            <span
              className={
                jobOpt == 1
                  ? "jobOpts-row jobOpts-active border-L-4"
                  : "jobOpts-row border-L-4"
              }
              onClick={() => GetJobOpt(1)}
            >
              Essential Data
            </span>
            <span
              className={
                jobOpt == 2
                  ? "jobOpts-row jobOpts-active border-R-4"
                  : "jobOpts-row border-R-4"
              }
              onClick={() => GetJobOpt(2)}
            >
              Additional Data
            </span>
          </div>
          {jobOpt == 1 && (
            <div className="row">
              <div className="col-md-12">
                <div className="sub-info-box" style={{ boxShadow: "none" }}>
                  <div className="feature-set">
                    <div className="feature-set-row">
                      <div className="bold-head">
                        <span>
                          <i className="fa fa-clock"></i>
                        </span>
                        <span>Employment type</span>
                      </div>

                      <div className="text">
                        {jobDetails.employmentTypeName}
                      </div>
                    </div>

                    <div className="feature-set-row">
                      <div className="bold-head">
                        <span>
                          <i className="fa fa-clock"></i>
                        </span>
                        <span>Seniority level</span>
                      </div>

                      <div className="text">
                        {jobDetails.seniorityLevelName}
                      </div>
                    </div>

                    <div className="feature-set-row">
                      <div className="bold-head">
                        <span>
                          <i className="fa fa-clock"></i>
                        </span>
                        <span>Company name</span>
                      </div>

                      <div className="text">{jobDetails.companyName}</div>
                    </div>

                    <div className="feature-set-row">
                      <div className="bold-head">
                        <span>
                          <i className="fa fa-clock"></i>
                        </span>
                        <span>Location</span>
                      </div>

                      <div className="text">{jobDetails.jobLocation}</div>
                    </div>
                  </div>

                  <div className="feature-set-info">
                    <div className="sub-feature">
                      <div className="bold-head">Job Title</div>

                      <div className="light-text">{jobDetails.jobTitle}</div>
                    </div>
                  </div>

                  <div className="feature-set-info">
                    <div className="sub-feature">
                      <div className="bold-head">Company Industry</div>

                      <div className="light-text">
                        {jobDetails.companyIndustry}
                      </div>
                    </div>
                  </div>

                  <div className="feature-set-info">
                    <div className="sub-feature">
                      <div className="bold-head">Job Description</div>
                      <div className="appColor">
                        <div
                          dangerouslySetInnerHTML={{
                            __html: jobDetails.description,
                          }}
                        />
                      </div>
                    </div>
                  </div>

                  <div className="feature-set-info">
                    <div className="sub-feature">
                      <div className="bold-head">Requirements</div>

                      <div className="appColor">
                        <div
                          dangerouslySetInnerHTML={{
                            __html: jobDetails.requirements,
                          }}
                        />
                      </div>
                    </div>
                  </div>

                  <div className="feature-set-info">
                    <div className="sub-feature">
                      <div className="bold-head">Work experience</div>

                      <div className="light-text">
                        {jobDetails.experienceLevelName}
                      </div>
                    </div>
                  </div>

                  <div className="feature-set-info">
                    <div className="sub-feature">
                      <div className="bold-head">Must-have qualifications</div>

                      <div className="light-text">
                        {jobDetails.mustHaveQualification}
                      </div>
                    </div>
                  </div>

                  <div className="feature-set-info">
                    <div className="sub-feature">
                      <div className="bold-head">
                        Nice to have qualifications
                      </div>

                      <div className="light-text">
                        {jobDetails.niceToHaveQualification}
                      </div>
                    </div>
                  </div>

                  <div className="feature-set-info">
                    <div className="sub-feature">
                      <div className="bold-head">
                        Companies not to source from
                      </div>

                      <div className="light-text">
                        {jobDetails.companiesNotToSourceFrom}
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          )}

          {jobOpt == 2 && (
            <div className="sub-info-box" style={{ boxShadow: "none" }}>
              <div className="row">
                <div className="col-md-6">
                  {/* <div className="sub-feature"><div className="bold-head">What are your hiring needs for this job?</div><div className="light-text">{jobDetails.timetoHireName}</div></div> */}
                  {/* <div className="sub-feature">
                    <div className="bold-head">
                      When do you need to be hire by?
                    </div>
                    <div className="light-text">
                      {jobDetails.timetoHireName}
                    </div>
                  </div> */}
                  {/* <div className="sub-feature"><div className="bold-head">Are you sourcing this job outside of join right now?</div><div className="light-text">One time</div></div> */}
                  {/* <div className="sub-feature"><div className="bold-head">What are your hiring needs for this job?</div><div className="light-text">One time</div></div> */}
                  <div className="sub-feature">
                    <div className="bold-head">
                      How difficult would you say it is to find great profiles
                      for this job?
                    </div>
                    <div className="light-text">
                      {jobDetails.findingJobDifficultyName}
                    </div>
                  </div>

                  <div className="sub-feature">
                    <div className="bold-head">Currency</div>
                    <div className="light-text">{jobDetails.currencyName}</div>
                  </div>
                </div>
                <div className="col-md-6">
                  <div className="sub-feature">
                    <div className="bold-head">
                      How many people are you aiming to hire?
                    </div>
                    <div className="light-text">
                      {jobDetails.peopleToHireCount}
                    </div>
                  </div>
                  <div className="sub-feature">
                    <div className="bold-head">Min Salary</div>
                    <div className="light-text">{jobDetails.minSalary}</div>
                  </div>
                </div>
              </div>
              <div className="sep"></div>
              <div className="row">
                <div className="col-md-12">
                  <div className="sub-feature">
                    <div className="bold-head">First email subject?</div>
                    <div className="light-text">
                      {jobDetails.firstMailSubject}
                    </div>
                  </div>
                  <div className="sub-feature">
                    <div className="bold-head">First Email Description</div>
                    <div dangerouslySetInnerHTML={{ __html: jobDetails.firstMailDescription }} className="light-text">
                      
                    </div>

                  </div>
                </div>
                {/* <div className='col-md-6'>
          <div className="sub-feature"><div className="bold-head">Follow up 1 (if no reply, send after a delay of)?</div><div className="light-text">One time</div></div>
          </div> */}

                {/* <div className='col-md-6'>
          <div className="sub-feature"><div className="bold-head">Follow up 2 (if no reply, send after a delay of)?</div><div className="light-text">One time</div></div>
          </div> */}
              </div>
            </div>
          )}
        </DialogContent>
      </Dialog>

      <Dialog
        fullWidth={true}
        maxWidth={"sm"}
        onClose={handleCloseReason}
        aria-labelledby="customized-dialog-title"
        open={openReason}
      >
        <DialogTitle id="customized-dialog-title3" onClose={handleCloseReason}>
          Reason
        </DialogTitle>

        <DialogContent className="text-center" dividers>
          <img style={{ marginTop: "20px" }} src={imgDecline} />

          <div className="head-job-pop">
            <div className="blackHead">
              We are sorry to inform you that <br />
              we had declined you
            </div>
            <div className="shortBorder"></div>
          </div>

          <p className="blurText">{rejectionReason}</p>

          <div className="text-center job-pop-btns">
            <button onClick={handleCloseReason} className="btn btn-approve">
              OK
            </button>
          </div>
        </DialogContent>
      </Dialog>
    </React.Fragment>
  );
};

export default JobRequests;
