import DataTable, { createTheme } from 'react-data-table-component';
import React, { useEffect, useState } from "react";
import {
  createStyles,
  Theme,
  withStyles,
  WithStyles,
} from "@material-ui/core/styles";
import empIco from "../../../src/assets/job-board-icon.png";
import hot from "../../../src/assets/hot.png";
import { Link, useHistory } from "react-router-dom";

import Board from 'react-trello'

import { useDropzone } from "react-dropzone";

import Draft, {
  htmlToDraft,
  EmptyState,
  rawToDraft,
  draftToRaw,
  draftStateToHTML,
} from "react-wysiwyg-typescript";
import { stateToHTML } from "draft-js-export-html";

import { EditorState, convertToRaw, ContentState } from "draft-js";

import draftToHtml from "draftjs-to-html";

import IconButton from "@material-ui/core/IconButton";
import CloseIcon from "@material-ui/icons/Close";
import Typography from "@material-ui/core/Typography";

import Dialog from "@material-ui/core/Dialog";
import MuiDialogTitle from "@material-ui/core/DialogTitle";
import MuiDialogContent from "@material-ui/core/DialogContent";
import MuiDialogActions from "@material-ui/core/DialogActions";

const styles = (theme: Theme) =>
  createStyles({
    root: {
      margin: 0,
      padding: theme.spacing(2),
    },
    closeButton: {
      position: "absolute",
      right: theme.spacing(1),
      top: theme.spacing(1),
      color: theme.palette.grey[500],
    },
  });
;


export interface DialogTitleProps extends WithStyles<typeof styles> {
  id: string;
  children: React.ReactNode;
  onClose: () => void;
}

const DialogTitle = withStyles(styles)((props: DialogTitleProps) => {
  const { children, classes, onClose, ...other } = props;
  return (
    <MuiDialogTitle disableTypography className={classes.root} {...other}>
      <Typography variant="h6">{children}</Typography>
      {onClose ? (
        <IconButton
          aria-label="close"
          className={classes.closeButton}
          onClick={onClose}
        >
          <CloseIcon />
        </IconButton>
      ) : null}
    </MuiDialogTitle>
  );
});

const DialogContent = withStyles((theme: Theme) => ({
  root: {
    padding: theme.spacing(2),
  },
}))(MuiDialogContent);

const DialogActions = withStyles((theme: Theme) => ({
  root: {
    margin: 0,
    padding: theme.spacing(1),
  },
}))(MuiDialogActions);




export default function TestDoc() {

  const card = () => {
    alert('HURRAH')
  }

  const [uploaded, setUploaded] = useState(false);

  const { acceptedFiles, getRootProps, getInputProps } = useDropzone({
    accept: ".pdf",
  });

  const files = acceptedFiles.map((file: any) => (
    <li key={file.path}>
      {file.path} - {file.size} bytes
    </li>
  ));

  function handleChange(e: any) {
    const { name, value } = e.target;
  }

  // TO SET A DEFAULT HTML
  const html = "";
  const [editorState, seteditorState] = useState(htmlToDraft(html));
  
  const handleDraftChange = (draftText: any) => {
    seteditorState(draftText);
    // SAVING HTML TO SERVER
    const convertedHtml = draftToHtml(
      convertToRaw(editorState.getCurrentContent())
    );
    // const html = draftToHtml(draftText)
  };

  const [openSure, setOpenSure] = React.useState(false);

  const handleOpen = () => {
    setOpenSure(true);
  };

  const handleClose = () => {
    setOpenSure(false);
  };
  
  const [activeRadio, setactiveRadio] = React.useState('approved');
  const handleRadio = (args:any) => {
    setactiveRadio(args);
  };

  const [offerStatus, setofferStatus] = React.useState('accepted');
  const handleOffer = (args:any) => {
    setofferStatus(args);
  };

  const [hiredStatus, sethiredStatus] = React.useState('yes');
  const handleHired = (args:any) => {
    sethiredStatus(args);
  };

  const handleDragEnd = (cardId:any, sourceLaneId:any, targetLaneId:any, position:any, cardDetails:any) =>{
   console.log('cardId',cardId)
   if(sourceLaneId == 'lane2' && targetLaneId == 'lane1'){
     return false
   }
   if(sourceLaneId == 'lane3' && targetLaneId == 'lane2'){
    return false
  }
  if(sourceLaneId == 'lane4' && targetLaneId == 'lane3'){
    return false
  }
  if(sourceLaneId == 'lane5' && targetLaneId == 'lane4'){
    return false
  }
  if(sourceLaneId == 'lane5' && targetLaneId == 'lane3'){
    return false
  }
  if(sourceLaneId == 'lane5' && targetLaneId == 'lane2'){
    return false
  }
  if(sourceLaneId == 'lane5' && targetLaneId == 'lane1'){
    return false
  }
  if(sourceLaneId == 'lane4' && targetLaneId == 'lane2'){
    return false
  }
  if(sourceLaneId == 'lane4' && targetLaneId == 'lane1'){
    return false
  }
  if(sourceLaneId == 'lane2' && targetLaneId == 'lane4'){
    return false
  }
  if(sourceLaneId == 'lane2' && targetLaneId == 'lane5'){
    return false
  }
  if(sourceLaneId == 'lane3' && targetLaneId == 'lane1'){
    return false
  }
  if(sourceLaneId == 'lane3' && targetLaneId == 'lane5'){
    return false
  }
  if(sourceLaneId == 'lane1' && targetLaneId == 'lane3'){
    return false
  }
  if(sourceLaneId == 'lane1' && targetLaneId == 'lane4'){
    return false
  }
  if(sourceLaneId == 'lane1' && targetLaneId == 'lane5'){
    return false
  }
  }
  
  const cardsSourcing = [
    
    {id: 'Card1', title: '', description: <div className='job-box boardBox OrangeCard'>
    <div className='topNavBoard'>
    <div className='pull-left data-card-light'><i className='fal fa-calendar'></i>10/12/2021</div>
    <div className='pull-right'><img src={hot}/></div>
    </div>

    <div className='clearBoth flowRoot' style={{margin:'0px 0px 10px'}}>
    <div className='float-left' style={{marginRight:'10px'}}>
     <img src={empIco}/>
    </div>
    <div className='float-left'>
    <div className="data-card-head">
    John Willaims
    </div> 
    <div className="align-left">
    <Link
  to=''
  className="detail-link"
  >
    Show Details
  </Link>
    </div>
    </div>
    
    </div>
  <div className='cardStatus'>
  <ul>
    <span className='liS-orange'>
    <span className='cricle'></span>
    Sourcing</span>
  </ul>
  </div>
  </div>, draggable: true},
  ];
  
  const cardsApproval = [
    
    {id: 'Card22', title: '', description: <div className='job-box boardBox GreenCard'>
    <div className='topNavBoard'>
    <div className='pull-left data-card-light'><i className='fal fa-calendar'></i>10/12/2021</div>
    <div className='pull-right'><img src={hot}/></div>
    </div>

    <div className='clearBoth flowRoot' style={{margin:'0px 0px 10px'}}>
    <div className='float-left' style={{marginRight:'10px'}}>
     <img src={empIco}/>
    </div>
    <div className='float-left'>
    <div className="data-card-head">
    John Willaims
    </div> 
    <div className="align-left">
    <Link
  to=''
  className="detail-link"
  >
    Show Details
  </Link>
    </div>
    </div>
    
    </div>
  <div className='cardStatus bRadioBtn'>
  <div className='font-14'>CV approval</div>
  <div>
    <span
    className={
      activeRadio == 'approved'
        ? "bRadio approveRadio rActive"
        : "bRadio approveRadio"
    }
    onClick={() =>handleRadio('approved')}>
    <i className='fa fa-circle'></i>  
    Approved</span>
    <span
    className={
      activeRadio == 'disapproved'
        ? "bRadio disApproveRadio rActive"
        : "bRadio disApproveRadio"
    }
    onClick={() => handleRadio('disapproved')}>
    <i className='fa fa-circle'></i>  
    Disapproved</span>
  </div>
  </div>
  </div>, draggable: true},
    
  ]
  
  const cardsInterviews = [
    {id: 'Card13', title: '', description: <div className='job-box boardBox BlueCard'>
    <div className='topNavBoard'>
    <div className='pull-left data-card-light'><i className='fal fa-calendar'></i>10/12/2021</div>
    <div className='pull-right'><img src={hot}/></div>
    </div>

    <div className='clearBoth flowRoot' style={{margin:'0px 0px 10px'}}>
    <div className='float-left' style={{marginRight:'10px'}}>
     <img src={empIco}/>
    </div>
    <div className='float-left'>
    <div className="data-card-head">
    John Willaims
    </div> 
    <div className="align-left">
    <Link
  to=''
  className="detail-link"
  >
    Show Details
  </Link>
    </div>
    </div>
    
    </div>
  <div className='cardStatus'>
  <ul className='pull-left'>
    <span className='liS-blue'>
    <span className='cricle'></span>
    3 Interviews</span>
  </ul>
  <div className='pull-right'>
  <button onClick={handleOpen} className='btn btn-approve board-btn'>Set Another</button>
  </div>
  </div>
  </div>, draggable: true},
    
    {id: 'Card33', title: '', description: <div className='job-box boardBox GrayCard'>
    <div className='topNavBoard'>
    <div className='pull-left data-card-light'><i className='fal fa-calendar'></i>10/12/2021</div>
    <div className='pull-right'><img src={hot}/></div>
    </div>

    <div className='clearBoth flowRoot' style={{margin:'0px 0px 10px'}}>
    <div className='float-left' style={{marginRight:'10px'}}>
     <img src={empIco}/>
    </div>
    <div className='float-left'>
    <div className="data-card-head">
    John Willaims
    </div> 
    <div className="align-left">
    <Link
  to=''
  className="detail-link"
  >
    Show Details
  </Link>
    </div>
    </div>
    
    </div>
  <div className='cardStatus'>
  <ul className='pull-left'>
    <span className='liS-gray'>
    <span className='cricle'></span>
    0 Interviews</span>
  </ul>
  <div className='pull-right'>
  <button onClick={handleOpen} className='btn btn-approve board-btn'>Set Another</button>
  </div>
  </div>
  </div>, draggable: true},
    
  ]
  
  
  const cardsOffering =[
    {id: 'Card44', title: '', description: <div className='job-box boardBox GrayCard'>
    <div className='topNavBoard'>
    <div className='pull-left data-card-light'><i className='fal fa-calendar'></i>10/12/2021</div>
    <div className='pull-right'><img src={hot}/></div>
    </div>

    <div className='clearBoth flowRoot' style={{margin:'0px 0px 10px'}}>
    <div className='float-left' style={{marginRight:'10px'}}>
     <img src={empIco}/>
    </div>
    <div className='float-left'>
    <div className="data-card-head">
    John Willaims
    </div> 
    <div className="align-left">
    <Link
  to=''
  className="detail-link"
  >
    Show Details
  </Link>
    </div>
    </div>
    
    </div>
  <div className='cardStatus'>
  <ul className='pull-left'>
  </ul>
  <div className='pull-right'>
  <button onClick={handleOpen} className='btn btn-approve board-btn'>Set Offer</button>
  </div>
  </div>
  </div>, draggable: true},

{id: 'Card32', title: '', description: <div className='job-box boardBox GreenCard'>
<div className='topNavBoard'>
<div className='pull-left data-card-light'><i className='fal fa-calendar'></i>10/12/2021</div>
<div className='pull-right'><img src={hot}/></div>
</div>

<div className='clearBoth flowRoot' style={{margin:'0px 0px 10px'}}>
<div className='float-left' style={{marginRight:'10px'}}>
 <img src={empIco}/>
</div>
<div className='float-left'>
<div className="data-card-head">
John Willaims
</div> 
<div className="align-left">
<Link
to=''
className="detail-link"
>
Show Details
</Link>
</div>
</div>

</div>
<div className='cardStatus bRadioBtn'>
<div className='font-14'>Is offer accepted? </div>
<div>
<span
className={
  offerStatus == 'accepted'
    ? "bRadio approveRadio rActive"
    : "bRadio approveRadio"
}
onClick={() =>handleOffer('accepted')}>
<i className='fa fa-circle'></i>  
Accepted</span>
<span
className={
  offerStatus == 'refused'
    ? "bRadio disApproveRadio rActive"
    : "bRadio disApproveRadio"
}
onClick={() => handleOffer('refused')}>
<i className='fa fa-circle'></i>  
Refused</span>
</div>
</div>
</div>, draggable: true},
  ]
  const cardsHiring =[
    {id: 'Card55', title: '', description: <div className='job-box boardBox GreenCard'>
<div className='topNavBoard'>
<div className='pull-left data-card-light'><i className='fal fa-calendar'></i>10/12/2021</div>
<div className='pull-right'><img src={hot}/></div>
</div>

<div className='clearBoth flowRoot' style={{margin:'0px 0px 10px'}}>
<div className='float-left' style={{marginRight:'10px'}}>
 <img src={empIco}/>
</div>
<div className='float-left'>
<div className="data-card-head">
John Willaims
</div> 
<div className="align-left">
<Link
to=''
className="detail-link"
>
Show Details
</Link>
</div>
</div>

</div>
<div className='cardStatus bRadioBtn'>
<div className='font-14'>Is this candidate hired? </div>
<div>
<span
className={
  hiredStatus == 'yes'
    ? "bRadio approveRadio rActive"
    : "bRadio approveRadio"
}
onClick={() =>handleHired('yes')}>
<i className='fa fa-circle'></i>  
Yes</span>
<span
className={
  hiredStatus == 'no'
    ? "bRadio disApproveRadio rActive"
    : "bRadio disApproveRadio"
}
onClick={() => handleHired('no')}>
<i className='fa fa-circle'></i>  
No</span>
</div>
</div>
</div>, draggable: true},
  ]
  const data = {
    lanes: [
      {
        id: 'lane1',
        title: 'Sourcing',
        cards: cardsSourcing
      },
      {
        id: 'lane2',
        title: 'CV Approval',
        cards: cardsApproval,
        currentPage: 1,
      },
      {
        id: 'lane3',
        title: 'Interviews',
        cards: cardsInterviews,
      },
      {
        id: 'lane4',
        title: 'Offering',
        cards: cardsOffering,
      },
      {
        id: 'lane5',
        title: 'Hiring',
        cards: cardsHiring,
      }
    ]
  }
  


  return (
    <React.Fragment>

      <div className='newJobBoard'>
      <Board data={data}
       handleDragEnd={handleDragEnd}
       hideCardDeleteIcon={true}
      />

        <Dialog
        fullWidth={true}
        maxWidth={"sm"}
        onClose={handleClose}
        aria-labelledby="customized-dialog-title"
        open={openSure}
      >
        <DialogTitle id="customized-dialog-title2" onClose={handleClose}>
          Setting Interview
        </DialogTitle>

        <DialogContent className="text-center" dividers>
        

          <div className='form row'>
          <div className='form-group col-md-6'>
          <label>Choose Template</label>
          <select className='form-control'>
          </select>
          </div>

          <div className='form-group col-md-6'>
          <label>First Email Subject</label>
          <input className='form-control'/>
          </div>

          <div className="form-group col-md-12">
                    <label>First email description</label>
                    <Draft
                      editorState={editorState}
                      name="firstMailDescription"
                      id="firstMailDescription"
                      className='form-control'
                      onEditorStateChange={(editorState) =>
                      handleDraftChange(editorState)
                      }
                    />
                  </div>

                  <div className="form-group col-md-12">
                  <div
                      onChange={handleChange}
                      {...getRootProps({ className: "dropzone" })}
                    >
                      <input onChange={handleChange} {...getInputProps()} />
                      <div
                        className="file-drop"
                        style={{ marginBottom: "15px" }}
                      >
                        <div className="blackHead">
                          Drag and drop
                        </div>
                        <div>
                          Or click{" "}
                          <a className="anchor"> here to upload attachment</a>
                        </div>

                      </div>
                    </div>

                    <br />
                    <aside>
                      <h4>File</h4>
                      <ul>{files}</ul>
                    </aside>
                  </div>



          </div>

          <div className="text-center job-pop-btns">
            <button onClick={handleClose} className="btn btn-decline">
              Cancel
            </button>
            <button
              type="button"
              onClick={handleClose}
              className="btn btn-approve"
            >
              Confirm
            </button>
          </div>
        </DialogContent>
      </Dialog>
      </div>
    </React.Fragment>
  );
}
