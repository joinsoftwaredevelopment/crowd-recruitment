import React, { useState, useEffect } from "react";
import { createStyles, makeStyles, Theme } from "@material-ui/core/styles";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import Grid from "@material-ui/core/Grid";
import thank from "../../../src/assets/thankful.png";
import jobimg from "../../../src/assets/job-bg.png";
import { ISourcersBalances } from "../../models/ISourcersBalances";
import sourcerService from "../../services/SourcerService";
import { ISourcersBalancesSummary } from "../../models/ISourcersBalancesSummary";
import DataTable, { createTheme } from "react-data-table-component";

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {},
  })
);

export default function SourcerBalances() {
  const [sourcerBalances, setAllsourcerBalances] = useState<
    ISourcersBalances[]
  >([]);

  let user = JSON.parse(localStorage.getItem("user") || "{}");

  const [sourcerBalancesSummary, setAllsourcerBalancesSummary] =
    useState<ISourcersBalancesSummary>({
      approvedCount: 0,
      declinedCount: 0,
      sourcerUuid: "",
      totalBalance: 0,
      rate: 0,
    });

  const drawSourcerBalances = sourcerBalances.map(
    (sourcer: ISourcersBalances) => (
      <tr key={sourcer.candidateUuId}>
        <td>{sourcer.submissionDate}</td>
        <td>{sourcer.candidateName}</td>
        <td>{sourcer.jobTitle}</td>
        <td>{sourcer.employerName}</td>
        <td>
          <span className="balance-box">{sourcer.candidateCommission}$</span>
        </td>
      </tr>
    )
  );

  const getSourcerBalances = () => {
    debugger;
    sourcerService
      .getSourcerBalances(user.uuid)
      .then((response) => {
        debugger;
        setAllsourcerBalances(response.data.data.sourcersBalances);
        setAllsourcerBalancesSummary(
          response.data.data.sourcersBalancesSummary
        );
        debugger;
      })
      .catch((e) => {
        console.log(e);
      });
  };

  useEffect(() => {
    getSourcerBalances();
  }, []);

  const columns = [
    {
      name: "Date",
      selector: "submissionDate",
      sortable: true,
    },
    {
      name: "Candidate",
      selector: "candidateName",
      sortable: true,
    },
    {
      name: "Job Title",
      selector: "jobTitle",
      sortable: true,
    },
    {
      name: "Employer",
      selector: "employerName",
      sortable: true,
    },
    {
      name: "Bounty",
      selector: "",
      sortable: true,
      cell: (row: any) => (
        <span className="balance-box">{row.candidateCommission}$</span>
      ),
    },
  ];

  const classes = useStyles();

  return (
    <React.Fragment>
      {/* <div>
        <img src={jobimg} />
      </div> */}
      <div className="src-blnc">
        <div className="container">
          <div className="row">
            <div className="col-md-4">
              <div className="white-box">
                <div
                  className="bold-head text-center"
                  style={{ padding: "20px 0" }}
                >
                  2021/01/01 to 2021/12/31
                </div>
              </div>
            </div>
            <div className="col-md-6">
              <div className="white-box">
                <div className="row row-center text-center">
                  <div className="col-md-4">
                    <div className="bold-head clr-head">
                      {sourcerBalancesSummary.approvedCount}
                    </div>
                    <div className="bold-head">No of approved</div>
                  </div>
                  <div className="col-md-4 border-LR">
                    <div className="bold-head clr-head">
                      {sourcerBalancesSummary.declinedCount}
                    </div>
                    <div className="bold-head">No of declined</div>
                  </div>
                  <div className="col-md-4">
                    <div className="bold-head clr-head">
                      {sourcerBalancesSummary.totalBalance}$
                    </div>
                    <div className="bold-head">Total Bounty</div>
                  </div>
                </div>
              </div>
            </div>

            <div className="col-md-2">
              <div className="rate-box">
                <div className="bold-head-white">
                  {sourcerBalancesSummary.rate}%
                </div>
                <div>successful rate </div>
              </div>
            </div>
          </div>

          <div className="row">
            <div className="col-md-12">
              <div className="splitHead">Submissions</div>
              <div className="shortBorder"></div>

              <DataTable
                pagination
                title=""
                columns={columns}
                data={sourcerBalances}
              />
              {/* <div className="table-responsive">
                                <table className="table">
                                    <thead>
                                        <tr>
                                            <th>Date</th>
                                            <th>Candidate</th>
                                            <th>Job Title</th>
                                            <th>Employer</th>
                                            <th>Bounty</th>
                                        </tr>
                                    </thead>
                                    <tbody>{drawSourcerBalances}</tbody>
                                </table>
                            </div> */}
            </div>
          </div>
        </div>
      </div>
    </React.Fragment>
  );
}
