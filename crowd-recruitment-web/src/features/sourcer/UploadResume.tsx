import React, { useState } from "react";
import { createStyles, makeStyles, Theme } from "@material-ui/core/styles";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import Grid from "@material-ui/core/Grid";
import thank from "../../../src/assets/thankful.png";
import { useDropzone } from "react-dropzone";
import { RootStateOrAny, useDispatch, useSelector } from "react-redux";
import { yupResolver } from "@hookform/resolvers/yup";
import { useForm } from "react-hook-form";
import * as Yup from "yup";
// import {history} from '../../helpers/history'
import { history } from "../../helpers/history";

import { ISourcerCVModel } from "../../models/ISourcerCVModel";
import { sourcerActions } from "../../actions/sourcerActions";
import { CircularProgress } from "@material-ui/core";
import { CenterFocusStrongOutlined } from "@material-ui/icons";

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {},
  })
);

export default function UploadResume() {
  const classes = useStyles();
  const dispatch = useDispatch();

  const [uploaded, setUploaded] = useState(false);

  const { acceptedFiles, getRootProps, getInputProps } = useDropzone({
    accept: ".pdf",
  });

  const files = acceptedFiles.map((file: any) => (
    <li key={file.path}>
      {file.path} - {file.size} bytes
    </li>
  ));

  //inital sourcer state
  const [sourcerCV, setSourcerCV] = useState<ISourcerCVModel>({
    Uuid: "",
    JobUuId: "",
    CvName: "",
    LinkedInProfile: "",
    Email: "",
    Location: "",
    UploadedFile: acceptedFiles[0],
    candidateName: "",
    countryId: 189,
  });


  let currentContent: any;

  let content = {
    English: {
      UploadyourresumeandLinkedInprofile: "Upload your resume and LinkedIn profile",
      Draganddropresumehere: "Drag and drop resume here",
      Orclick: "Or click",
      heretouploadresume: "here to upload resume",
      PleaseuploadyourCV: "Please upload your CV",
      LinkedinProfile: "Linkedin Profile",
      Files: "Files",
      Finish: "Finish",
      Back: "Back",
      LinkedInProfileisrequired:"LinkedIn Profile is required",
      validLinkedInprofilelink:"Please enter a valid LinkedIn profile link",
    },
    Arabic: {
      UploadyourresumeandLinkedInprofile: "قم برفع سيرتك الذاتية و كتابة رابط لينكد ان الخاص بك",
      Draganddropresumehere: "يمكنك سحب سيرتك الذاتية هنا",
      Orclick: "قم قم بالضغط",
      heretouploadresume: "هنا لتحميلها",
      PleaseuploadyourCV: "من فضلك قم برفع السيرة الذاتية الخاصة بك",
      LinkedinProfile: "رابط لينكد ان الخاص بكم",
      Files: "الملفات",
      Finish: "إنهاء",
      Back: "الرجوع",
      LinkedInProfileisrequired:"رابط ملفك علي لينكد ان مطلوب",
      validLinkedInprofilelink:"ادخل عنوان ملفك الشخصي علي لينكد ان بشكل صحيح",
    },
  };

  localStorage.getItem("currentLanguage") === "Arabic"
  ? (currentContent = content.Arabic)
  : (currentContent = content.English);

  // form validation rules
  const validationRules = Yup.object().shape({
    LinkedInProfile: Yup.string()
      .required(currentContent.LinkedInProfileisrequired)
      .test("regex", currentContent.validLinkedInprofilelink, (val) => {
        let regExp = new RegExp("^https://[a-z]{2,3}.linkedin.com/.*$");
        console.log(regExp.test(val), regExp, val);
        return regExp.test(val);
      }),
    // CvName : Yup.string()
    //     .required('please upload your CV')
  });

  // functions to build form returned by useForm() hook
  const { register, handleSubmit, errors } = useForm<ISourcerCVModel>({
    resolver: yupResolver(validationRules),
  });

  function handleChange(e: any) {
    const { name, value } = e.target;
    // console.log(e.target);
    debugger;

    if (acceptedFiles.length != 0) {
      let file = acceptedFiles[0];
      setSourcerCV((sourcerCV) => ({
        ...sourcerCV,
        [name]: value,
        UploadedFile: file,
        CvName: file.name,
      }));
      setUploaded(true);
    } else if (name == "" && value != "") {
      setUploaded(true);
    } else {
      setSourcerCV((sourcerCV) => ({ ...sourcerCV, [name]: value }));
      setUploaded(false);
    }
  }

  function UploadCVChanged() {
    setUploaded(true);
    console.log(uploaded);
  }

  function onSubmit() {
    // display form data on success
    //alert(data);
    debugger;
    if (uploaded) {
      let file = acceptedFiles[0];
      console.log(file);
      const formData = new FormData();
      formData.append("CvName", file.name);
      formData.append("UploadedFile", file);
      formData.append("LinkedInProfile", sourcerCV.LinkedInProfile);

      dispatch(sourcerActions.SourcerUploadCV(formData));
    }
  }




  return (
    <React.Fragment>
      <div className="container">
        <div className="row">
          <div className="col-md-2"></div>
          <div className="col-md-8">
            <div className="auth-white-bg thankyou">
              <div className="blueHead" style={{ marginBottom: "20px" }}>
                {currentContent.UploadyourresumeandLinkedInprofile}
              </div>

              <form onSubmit={handleSubmit(onSubmit)} id="my-awesome-dropzone">
                <div className="row">
                  <div className="col-md-2"></div>
                  <div className="col-md-8">
                    <div
                      onChange={handleChange}
                      {...getRootProps({ className: "dropzone" })}
                    >
                      <input onChange={handleChange} {...getInputProps()} />
                      <div
                        className="file-drop"
                        style={{ marginBottom: "15px" }}
                      >
                        <div className="blackHead">
                        {currentContent.Draganddropresumehere}
                        </div>
                        <div>
                        {currentContent.Orclick}{" "}
                          <a className="anchor"> {currentContent.heretouploadresume}</a>
                        </div>
                        <div className="blackHead red">
                          {!uploaded ? currentContent.PleaseuploadyourCV : ""}
                        </div>
                        {/* <div className='blurText' style={{ marginBottom: '0px' }}>maximum file size: 2 MB  </div> */}
                      </div>
                    </div>

                    <br />
                    <aside>
                      <h4>{currentContent.Files}</h4>
                      <ul>{files}</ul>
                    </aside>

                    <div className="form-group">
                      <label>{currentContent.LinkedinProfile}</label>
                      <input
                        onChange={handleChange}
                        name="LinkedInProfile"
                        id="LinkedInProfile"
                        placeholder={currentContent.LinkedinProfile}
                        value={sourcerCV.LinkedInProfile}
                        type="text"
                        ref={register}
                        className={`form-control ${
                          errors.LinkedInProfile ? "is-invalid" : ""
                        }`}
                      />
                      <div className="invalid-feedback">
                        {errors.LinkedInProfile?.message}
                      </div>
                    </div>
                  </div>
                  <div className="col-md-2"></div>
                </div>
                <div className="next-btn">
                  <Button
                    style={{ marginRight: "20px" }}
                    type="submit"
                    onClick={() => {
                      history.push("/registration/sourcer-industry");
                    }}
                    variant="contained"
                    color="primary"
                  >
                    {currentContent.Back}
                  </Button>

                  <Button type="submit" variant="contained" color="primary">
                    {/* {sourcerCV.CvName != "" &&
                      sourcerCV.LinkedInProfile != "" && (
                        <span className="loading">
                          <CircularProgress />
                        </span>
                      )}{" "} */}
                    {currentContent.Finish}
                  </Button>
                </div>
              </form>
            </div>
          </div>
          <div className="col-md-2"></div>
        </div>
      </div>
    </React.Fragment>
  );
}
