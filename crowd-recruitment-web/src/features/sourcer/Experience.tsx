import React , {useState,useEffect} from 'react';
import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import Grid from '@material-ui/core/Grid';
import thank from '../../../src/assets/thankful.png'
import { RootStateOrAny, useDispatch, useSelector } from 'react-redux';

import {history} from '../../helpers/history'
import { ISourcerExperienceModel } from '../../models/ISourcerExperienceModel';
import { sourcerActions } from '../../actions';
import sourcerService from '../../services/SourcerService';

const Experience = () => {
const dispatch = useDispatch();
const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        root: {
        },
    }),
);

//inital sourcer state
const [sourcerExperience, setSourcerExperience] = useState<ISourcerExperienceModel>({
  Uuid: '',
  ExperienceTypeId : 0
});

const onSelectExperienceType = (experienceTypeId : number) => {
    debugger ;
    console.log(experienceTypeId);
    setSourcerExperience({...sourcerExperience,ExperienceTypeId: experienceTypeId});
    debugger;
     sourcerExperience.ExperienceTypeId = experienceTypeId;
     dispatch(sourcerActions.SourcerExperience(sourcerExperience));
}

//  useEffect(() => {
//    // take action when property in Object Changed
//    debugger;
//    if(sourcerExperience.ExperienceTypeId != 0)
//      dispatch(sourcerActions.SourcerExperience(sourcerExperience));
//  }, [sourcerExperience.ExperienceTypeId]);

useEffect(() => {
  retrieveJobExperience();
}, []);

const retrieveJobExperience = () => {
  sourcerService.retrieveJobExperience()
    .then((response) => {
      debugger;
      setSourcerExperience({...sourcerExperience,ExperienceTypeId: response.data.data});
    })
    .catch((e) => {
      console.log(e);
    });
};

let currentContent: any;

  let content = {
    English: {
      Experience: "Experience",
      Jobtitle: "Job title",
      Candidatedatabase: "Candidate database",
      Industries: "Industries",
      CVProfile: "CV & Profile",
      Yearsofexperience: "Years of experience",
    },
    Arabic: {
      Experience: "الخبرة",
      Jobtitle: "عنوان الوظيفة",
      Candidatedatabase: "معلومات المتقدم",
      Industries: "التخصص",
      CVProfile: "السيرة الذتية",
      Yearsofexperience: "سنوات الخبرة",
    },
  };

  localStorage.getItem("currentLanguage") === "Arabic"
    ? (currentContent = content.Arabic)
    : (currentContent = content.English);

 return (
        <React.Fragment>
            <div className='container'>
                <div className='row'>
                    <div className='col-md-2'></div>
                    <div className='col-md-8'>
                   <div className='auth-white-bg thankyou'>
                   <ul className='reg-nav'>
                <li>
                  <span className='fill'>
                  <i className='fa fa-check'></i>
                  </span>
                  <span className='high'>{currentContent.Experience}</span>
                </li>
                <li>
                  <span className='nofill'>
                  <i className='fa fa-check'></i>
                  </span>
                  <span className='nohigh'>{currentContent.Jobtitle}</span>
                </li>
                <li>
                  <span className='nofill'>
                  <i className='fa fa-check'></i>
                  </span>
                  <span className='nohigh'>{currentContent.Candidatedatabase} </span>
                </li>
                <li>
                  <span className='nofill'>
                  <i className='fa fa-check'></i>
                  </span>
                  <span className='nohigh'>{currentContent.Industries}</span>
                </li>
                <li>
                  <span className='nofill'>
                  <i className='fa fa-check'></i>
                  </span>
                  <span className='nohigh'>{currentContent.CVProfile}</span>
                </li>
              </ul>
                   <div className='blueHead'>
                   {currentContent.Yearsofexperience}
                   </div>

                   <div className='row'>
                   <div className='col-md-3'>
                   <div onClick={() =>onSelectExperienceType(1)}
                       className={
                        sourcerExperience.ExperienceTypeId == 1
                          ? "square-box sqActive"
                          : "square-box"
                      }
                   >
                   0 - 2
                   </div>
                   </div>
                   <div className='col-md-3'>
                   <div  onClick={() =>onSelectExperienceType(2)} 
                      className={
                        sourcerExperience.ExperienceTypeId == 2
                          ? "square-box sqActive"
                          : "square-box"
                      }
                   >
                   3 - 5
                   </div>
                   </div>
                   <div className='col-md-3'>
                   <div  onClick={() =>onSelectExperienceType(3)}
                      className={
                        sourcerExperience.ExperienceTypeId == 3
                          ? "square-box sqActive"
                          : "square-box"
                      }
                   >
                   6 - 10
                   </div>
                   </div>
                   <div className='col-md-3'>
                   <div  onClick={() =>onSelectExperienceType(4)} 
                      className={
                        sourcerExperience.ExperienceTypeId == 4
                          ? "square-box sqActive"
                          : "square-box"
                      }
                   >
                   10 +
                   </div>
                   </div>
                   </div>

                 {/* <div className='next-btn'>
                 <Button onClick={() => {history.push('/sourcer/Jobtitle')}} type="button" variant="contained" color="primary">
                 Next
                </Button>
                 </div> */}
                   </div>
                    </div>
                    <div className='col-md-2'></div>
                </div>
            </div>

        </React.Fragment>

    );
}

export default  Experience;