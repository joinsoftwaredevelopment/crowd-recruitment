import React, { useState, useEffect } from "react";
import { createStyles, makeStyles, Theme } from "@material-ui/core/styles";
import { IJobSummary } from "../../models/IJobSummary";
import JobService from "../../services/JobService";

import { Link, useHistory } from "react-router-dom";
import { blue } from "@material-ui/core/colors";

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {},
  })
);

const MyJobs = () => {
  const classes = useStyles();
  const history = useHistory();
  const [allJobs, setAllJobs] = useState<IJobSummary[]>([]);
  const [sourcerJobs, setSourcerJobs] = useState<IJobSummary[]>([]);
  const [active, setActive] = useState(true);
  let currentContent: any;
  let content = {
    English: {
      Active: "Active",
      Inactive: "Inactive",
      MyJobs: "My Jobs",
      ShowDetails: "Show details",
      OpenJobs:"Opened Jobs",
    },
    Arabic: {
      Active: "نشطة",
      Inactive: "غير نشطة",
      MyJobs: "وظائفي",
      ShowDetails: "اظهار التفاصيل",
      OpenJobs:"الوظائف المفتوحة",
    },
  };

  localStorage.getItem("currentLanguage") === "Arabic"
    ? (currentContent = content.Arabic)
    : (currentContent = content.English);

  const drawAllJobs = allJobs.map((job: IJobSummary) => (
    <div className="col-md-4" key={job.rowNumber}>
      <div className="job-box">
        <div className="remain-box">
          <div className="mega-number">{job.noOfSubmissions}</div>
          <div className="light-mini">remaining</div>
        </div>

        <div className="data-card-head">{job.jobTitle}</div>
        <div className="data-card-light">{job.jobLocation}</div>
        <div className="data-card-light">Owner : {job.employerName}</div>
        <div className="data-card-light-green" style={{ position: "relative" }}>
          <span className="green">Commission: {job.commission}$</span>
          <Link className="detail-link" to={"/sourcer/jobDetails/" + job.uuid}>
            {currentContent.ShowDetails}
          </Link>
        </div>

        <div className="job-card-btm">
          <div className="data-card-light"> {job.jobDate}</div>
        </div>
      </div>
    </div>
  ));

  const drawSourcerJobs = sourcerJobs.map((job: IJobSummary) => (
    <div className="col-md-4" key={job.rowNumber}>
      <div className="job-box">
        <div className="remain-box">
          <div className="mega-number">{job.remainingCount}</div>
          <div className="light-mini">remaining</div>
        </div>

        <div className="data-card-head">{job.jobTitle}</div>
        <div className="data-card-light">{job.jobLocation}</div>
        <div className="data-card-light">
          Owner : {job.employerFname} {job.employerLname}
        </div>
        <div className="data-card-light-green">
          <span style={{ color: "#0195ff" }}>Commission:{job.commission}$</span>
        </div>
        <div className="job-card-btm">
          <div className="data-card-light"> {job.jobDate}</div>
          <div className="data-card-light-green">
            <a href={"/sourcer/jobDetails/" + job.uuid}>{currentContent.ShowDetails}</a>
          </div>
        </div>
      </div>
    </div>
  ));

  useEffect(() => {
    getAllJobs();
    getAllSourcerJobs(true);
  }, []);

  const getAllJobs = () => {
    JobService.allJobsSummaries()
      .then((response) => {
        debugger;
        setAllJobs(response.data.data);
      })
      .catch((e) => {
        console.log(e);
      });
  };

  const getAllSourcerJobs = (isActive: boolean) => {
    JobService.sourcerJobsSummaries(isActive)
      .then((response) => {
        setSourcerJobs(response.data.data);
      })
      .catch((e) => {
        console.log(e);
      });
  };

  const changeActive = (isActive: boolean) => {
    setActive(isActive);
    getAllSourcerJobs(isActive);
  };

  

  return (
    <React.Fragment>
      <div className="container">
        <div className="row">
          <div className="col-md-12">
            <div className="job-bg">
              <div className="head-sec">
                <div className="mini-head">{currentContent.MyJobs}</div>

                <div className="filter">
                  <span
                    id="activeJobs"
                    onClick={() => changeActive(true)}
                    className={
                      active
                        ? "active-filter highlight-filter border-rad-L-6"
                        : "non-active-filter border-rad-L-6"
                    }
                  >
                   {currentContent.Active}
                  </span>
                  <span
                    id="inActiveJobs"
                    onClick={() => changeActive(false)}
                    className={
                      !active
                        ? "active-filter highlight-filter border-rad-R-6"
                        : "non-active-filter border-rad-R-6"
                    }
                  >
                    {currentContent.Inactive}
                  </span>
                </div>
              </div>

              <div className="row">{drawSourcerJobs}</div>
            </div>
          </div>
        </div>

        <div className="row">
          <div className="col-md-12">
            <div className="job-bg">
              <div className="head-sec">
                <div className="mini-head">{currentContent.OpenJobs}</div>
              </div>

              <div className="row">{drawAllJobs}</div>
            </div>
          </div>
        </div>
      </div>
    </React.Fragment>
  );
};

export default MyJobs;
