import React, { useEffect, useState } from "react";
import { createStyles, makeStyles, Theme } from "@material-ui/core/styles";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import Grid from "@material-ui/core/Grid";
import thank from "../../../src/assets/thankful.png";
import { RootStateOrAny, useDispatch, useSelector } from "react-redux";
import { yupResolver } from "@hookform/resolvers/yup";
import { useForm } from "react-hook-form";
import * as Yup from "yup";

import { history } from "../../helpers/history";
import { ISourcerJobModel } from "../../models/ISourcerJobModel";
import { sourcerActions } from "../../actions/sourcerActions";
import sourcerService from "../../services/SourcerService";

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {},
  })
);

const JobTitle = () => {
  const classes = useStyles();
  const dispatch = useDispatch();

  //inital sourcer state
  const [sourcerJobTitle, setSourcerJobTitle] = useState<ISourcerJobModel>({
    Uuid: "",
    JobTitle: "",
  });

  let currentContent: any;

  let content = {
    English: {
      Experience: "Experience",
      Jobtitle: "Job title",
      Candidatedatabase: "Candidate database",
      Industries: "Industries",
      CVProfile: "CV & Profile",
      CurrentJobTitle: "Current job title",
      Back: "Back",
      Next: "Next",
      JobTitleisrequired: "Job Title is required",
    },
    Arabic: {
      Experience: "الخبرة",
      Jobtitle: "عنوان الوظيفة",
      Candidatedatabase: "معلومات المتقدم",
      Industries: "التخصص",
      CVProfile: "السيرة الذتية",
      CurrentJobTitle: "لقب الوظيفة الحالية",
      Back: "رجوع",
      Next: "التالي",
      JobTitleisrequired: "لقب الوظيفة مطلوب",
    },
  };

  localStorage.getItem("currentLanguage") === "Arabic"
    ? (currentContent = content.Arabic)
    : (currentContent = content.English);
    
  // form validation rules
  const validationRules = Yup.object().shape({
    JobTitle: Yup.string().required(currentContent.JobTitleisrequired),
  });

  // functions to build form returned by useForm() hook
  const { register, handleSubmit, errors } = useForm<ISourcerJobModel>({
    resolver: yupResolver(validationRules),
  });

  function handleChange(e: any) {
    debugger;
    if (e.target != undefined) {
      const { name, value } = e.target;
      console.log(e.target);
      setSourcerJobTitle((user) => ({ ...sourcerJobTitle, [name]: value }));
    } else {
      setSourcerJobTitle((user) => ({
        ...sourcerJobTitle,
        [sourcerJobTitle.JobTitle]: e,
      }));
    }
  }

  function onSubmit() {
    // display form data on success
    //alert(data);
    debugger;
    dispatch(sourcerActions.SourcerJobTitle(sourcerJobTitle));
  }

  useEffect(() => {
    GetJobtitleName();
  }, []);

  const GetJobtitleName = () => {
    sourcerService
      .GetJobtitleName()
      .then((response) => {
        debugger;
        sourcerJobTitle.JobTitle = response.data.data;
        handleChange(response.data.data);
      })
      .catch((e) => {
        console.log(e);
      });
  };

  

  return (
    <React.Fragment>
      <div className="container">
        <div className="row">
          <div className="col-md-2"></div>
          <div className="col-md-8">
            <div className="auth-white-bg thankyou">
              <ul className="reg-nav">
                <li>
                  <span className="fill">
                    <i className="fa fa-check"></i>
                  </span>
                  <span className="high">{currentContent.Experience}</span>
                </li>
                <li>
                  <span className="fill">
                    <i className="fa fa-check"></i>
                  </span>
                  <span className="high">{currentContent.Jobtitle}</span>
                </li>
                <li>
                  <span className="nofill">
                    <i className="fa fa-check"></i>
                  </span>
                  <span className="nohigh">{currentContent.Candidatedatabase} </span>
                </li>
                <li>
                  <span className="nofill">
                    <i className="fa fa-check"></i>
                  </span>
                  <span className="nohigh">{currentContent.Industries}</span>
                </li>
                <li>
                  <span className="nofill">
                    <i className="fa fa-check"></i>
                  </span>
                  <span className="nohigh">{currentContent.CVProfile}</span>
                </li>
              </ul>
              <div className="blueHead">{currentContent.CurrentJobtitle}</div>

              <form onSubmit={handleSubmit(onSubmit)}>
                <div className="row">
                  <div className="col-md-2"></div>
                  <div className="col-md-8">
                    <div className="form-group">
                      <label>{currentContent.CurrentJobTitle}</label>
                      <input
                        onChange={handleChange}
                        name="JobTitle"
                        id="JobTitle"
                        placeholder={currentContent.CurrentJobTitle}
                        ref={register}
                        type="text"
                        value={sourcerJobTitle.JobTitle}
                        className={`form-control ${
                          errors.JobTitle ? "is-invalid" : ""
                        }`}
                      />
                      <div className="invalid-feedback">
                        {errors.JobTitle?.message}
                      </div>
                    </div>
                  </div>
                  <div className="col-md-2"></div>
                </div>
                <div className="next-btn">
                  <Button
                    style={{marginRight:'20px'}}
                    type="button"
                    variant="contained"
                    color="primary"
                    onClick={() => {
                      history.push("/registration/sourcer-experience");
                    }}
                  >
                    {currentContent.Back}
                  </Button>
                  <Button type="submit" variant="contained" color="primary">
                  {currentContent.Next}
                  </Button>
                </div>
              </form>
            </div>
          </div>
          <div className="col-md-2"></div>
        </div>
      </div>
    </React.Fragment>
  );
};

export default JobTitle;
