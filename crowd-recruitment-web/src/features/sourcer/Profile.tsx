import React, { useEffect, useState } from "react";
import { createStyles, makeStyles, Theme } from "@material-ui/core/styles";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import Grid from "@material-ui/core/Grid";
import thank from "../../../src/assets/thankful.png";
import { useForm, Controller } from "react-hook-form";

import PhoneInput from "react-phone-input-2";
import "react-phone-input-2/lib/style.css";

import { yupResolver } from "@hookform/resolvers/yup";
import { Dispatch } from "redux";
import { alertActions } from "../../actions/alertActions";
import { useDispatch } from "react-redux";
import * as Yup from "yup";
import { IResponseData } from '../../models/IResponseData';

// Models
import {ISourcerCareerInfo} from "../../models/ISourcerCareerInfo";
import {ISourcerBasicInfo} from "../../models/ISourcerBasicInfo";
import {IUpdatePassword} from "../../models/IUpdatePassword";
import {ISourcerCVModel} from "../../models/ISourcerCVModel";

// Services
import SourcerService from "../../services/SourcerService";
import UserService from "../../services/UserService";
import sourcerService from "../../services/SourcerService";

const pdfDoc = require("../../../src/assets/pdfDoc.pdf")

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {},
  })
);

export default function SourcerProfile(props: any) {

  const classes = useStyles();
  
  const [currentTab, setCurrentTab] = useState(1);
  const [selectedPackage, setSelectedPackage] = useState(1);
  
  const toggleTab = (args:any) => {
    setCurrentTab(args);
  };

  const selectPackage = (args:any) => {
    setSelectedPackage(args);
  };

  const dispatch = useDispatch();

  const handleBasicInfoInputChange = (event: any) => {
    const { name, value } = event.target;
    setBasicInfo({ ...basicInfo, [name]: value });
  };

  const handlePasswordInputChange = (event: any) => {
    debugger;
    const { name, value } = event.target;
    setUpdatePassword({ ...updatePassword, [name]: value });
  };

  const handleCareerInputChange = (event: any) => {
    debugger;
    const { name, value } = event.target;
    setCareerInfo({ ...careerInfo, [name]: value });
  };

  // form validation rules
  const validationSchema = Yup.object().shape({
    jobTitle: Yup.string().trim().required("Job title is required"),
    firstName: Yup.string().trim().required("First name is required"),
    lastName: Yup.string().trim().required("Last name is required"),
    // phoneNumber: Yup.string()
    //   .trim()
    //   .required("Phone number is required")
    //   .matches(/^[0-9]+$/,"Phone number Must be only digits"),
    email: Yup.string()
      .trim()
      .required("Email is required")
      .email("Email is invalid"),
  });
debugger;
  const validationPasswordSchema = Yup.object().shape({
    password: Yup.string()
      .trim()
      .min(8, "Password must be at least 8 digits")
      .required("Old password is required"),
    newPassword: Yup.string()
      .trim()
      .min(8, "Password must be at least 8 digits")
      .required("New password is required"),
      confirmedPassword: Yup.string()
     .oneOf([Yup.ref('newPassword'), null], 'Two passwords must match')
  });


  // functions to build form returned by useForm() hook
  const { register, handleSubmit, reset, errors, control } = useForm({
    resolver: yupResolver(validationSchema),
  });

  const {
    register: register2,
    handleSubmit: handleSubmit2,
    reset: reset2,
    errors: errors2,
  } = useForm({
    resolver: yupResolver(validationPasswordSchema),
    mode: "onBlur",
  });

  function onSubmit(data: any) {
    debugger;
    UpdateBasicInfo();
  }

  function onSubmit2(data: any) {
    debugger;
    UpdatePassword();
  }

  const initialBasicInfo: ISourcerBasicInfo = {
    jobTitle : "",
    firstName: "",
    lastName: "",
    phoneNumber: "",
    email: "",
    uuid: "",
    cvName: "",
    linkedInProfile:"",
  };

  const initialUpdatePassword: IUpdatePassword = {
    newPassword: "",
    password: "",
    uuid: "",
    confirmedPassword: "",
  };

  const initialCareerInfo: ISourcerCareerInfo = {
    hasAccessToDifferentWebsite: false,
    experienceTypeId: 1,
    uuid: "",
    industryId: 1,
  };

  const [basicInfo, setBasicInfo] = useState(initialBasicInfo);
  const [updatePassword, setUpdatePassword] = useState(initialUpdatePassword);
  const [careerInfo, setCareerInfo] = useState(initialCareerInfo);
  const [path, setPath] = useState("");
 
  useEffect(() => {
    GetBasicInfo();
    GetCareerInfo();
    //GetCvInfo();
  }, []);

  const GetBasicInfo = () => {
    debugger;
    SourcerService
      .GetBasicInfo(props.match.params.uuid)
      .then((response) => {
        debugger;
        setBasicInfo(response.data.data);
        basicInfo.cvName = response.data.data.cvName;
        openPdf(response.data.data.cvName);
      })
      .catch((e) => {
        console.log(e);
      });
  };

  const openPdf = (fileName: string) => {
    sourcerService
      .getPdf(fileName)
      .then((response) => {
        debugger;
        console.log(response.data);
        //Create a Blob from the PDF Stream
        const file = new Blob([response.data], { type: "application/pdf" });
        //Build a URL from the file
        const fileURL = URL.createObjectURL(file);
        setPath(fileURL);
        //Open the URL on new Window
        //window.open(fileURL);
      })
      .catch((e) => {
        console.log(e);
      });
  };

  const UpdateBasicInfo = () => {
    debugger;
    basicInfo.uuid = props.match.params.uuid;
    SourcerService
      .UpdateBasicInfo(basicInfo)
        .then(
          user => { 
            debugger;
            let responseDetails : IResponseData  =  user.data;
            dispatch(
              alertActions.success(responseDetails.message));
          },
          error => {
            debugger;
            let responseDetails : IResponseData  =  error.response.data
            dispatch(alertActions.error(responseDetails.message));
          }
      );
  };

  const UpdatePassword = () => {
    debugger;
    updatePassword.uuid = props.match.params.uuid;
    UserService
      .UpdatePassword(updatePassword)
        .then(
          user => { 
            debugger;
            let responseDetails : IResponseData  =  user.data;
            dispatch(
              alertActions.success(responseDetails.message));
          },
          error => {
            debugger;
            let responseDetails : IResponseData  =  error.response.data
            dispatch(alertActions.error(responseDetails.message));
          }
      );
  };

  const GetCareerInfo = () => {
    debugger;
    SourcerService
      .GetCareerInfo(props.match.params.uuid)
      .then((response) => {
        debugger;
        careerInfo.industryId = response.data.data.industryId;
        careerInfo.hasAccessToDifferentWebsite = response.data.data.hasAccessToDifferentWebsite;
        careerInfo.experienceTypeId = response.data.data.experienceTypeId;

        setCareerInfo(careerInfo);
      })
      .catch((e) => {
        console.log(e);
      });
  };


  const UpdateCareerInfo = () => {
    debugger;
    careerInfo.uuid = props.match.params.uuid;
    SourcerService
      .UpdateCareerInfo(careerInfo)
        .then(
          user => { 
            debugger;
            let responseDetails : IResponseData  =  user.data;
            dispatch(
              alertActions.success(responseDetails.message));
          },
          error => {
            debugger;
            let responseDetails : IResponseData  =  error.response.data
            dispatch(alertActions.error(responseDetails.message));
          }
      );
  };

  
  function handleOnChange(
    value: any,
    data: any,
    event: any,
    formattedValue: any
  ) {
    console.log("value", value);
    basicInfo.phoneNumber = value;
    setBasicInfo(basicInfo);
  }

  return (
    <React.Fragment>
      <div className="container profilePage">
      <div className='row'>
      <div className='col-md-3'>
      <div className='tabBox'>
      <div
      onClick={() => toggleTab(1)}
      className={
        currentTab == 1
          ? "tabList activeTab"
          : "tabList"
      }
      
      >Basic Info</div>
      <div
      onClick={() => toggleTab(2)}
      className={
        currentTab == 2
          ? "tabList activeTab"
          : "tabList"
      }
      >Change password</div>
      <div
      onClick={() => toggleTab(3)}
      className={
        currentTab == 3
          ? "tabList activeTab"
          : "tabList"
      }
      >Career Info  </div>

    <div
      onClick={() => toggleTab(4)}
      className={
        currentTab == 4
          ? "tabList activeTab"
          : "tabList"
      }
      >CV & Profile  </div>

      </div>
      </div>

      <div className='col-md-9'>
      <div className='tabContent'>
      <form
              onSubmit={handleSubmit(onSubmit)}
              onReset={reset}
            >
      {currentTab == 1 && 
      <div className='row'>
      <div className='form-group col-md-12'>
      <label>Job Title</label>
      <input 
         name="jobTitle"
         id="jobTitle"
         value={basicInfo.jobTitle}
         type="text"
         ref={register}
         className={`form-control ${
           errors.jobTitle ? "is-invalid" : ""
         }`}
         onChange={handleBasicInfoInputChange}
       />
       <div className="invalid-feedback">
         {errors.jobTitle?.message}
       </div>
      </div>

      <div className='form-group col-md-6'>
      <label>First Name</label>
      <input
                  name="firstName"
                  id="firstName"
                  value={basicInfo.firstName}
                  type="text"
                  ref={register}
                  className={`form-control ${
                    errors.firstName ? "is-invalid" : ""
                  }`}
                  onChange={handleBasicInfoInputChange}
                />
                <div className="invalid-feedback">
                  {errors.firstName?.message}
                </div>
      </div>

      <div className='form-group col-md-6'>
      <label>Last Name</label>
      <input
                  name="lastName"
                  id="lastName"
                  value={basicInfo.lastName}
                  type="text"
                  ref={register}
                  className={`form-control ${
                    errors.lastName ? "is-invalid" : ""
                  }`}
                  onChange={handleBasicInfoInputChange}
                />
                <div className="invalid-feedback">
                  {errors.lastName?.message}
                </div>
      </div>

      <div className='form-group col-md-6'>
      <label>Phone Number</label>

      <Controller
                  name="phoneNumber"
                  control={control}
                  defaultValue=""
                  render={({ name, onBlur, onChange, value }) => (
                    <PhoneInput
                      value={basicInfo.phoneNumber}
                      onBlur={onBlur}
                      onChange={handleOnChange}
                      country={"sa"}
                      inputProps={{
                        name: "phoneNumber",
                        id: "phoneNumber",
                        required: true,
                        autoFocus: true,
                      }}
                    />
                  )}
                />
                <div className="invalid-feedback">
                  {errors.phoneNumber?.message}
                </div>
  
      </div>

      <div className='form-group col-md-6'>
      <label>Email Address</label>
      <input
                  name="email"
                  id="email"
                  value={basicInfo.email}
                  type="text"
                  ref={register}
                  className={`form-control ${errors.email ? "is-invalid" : ""}`}
                  onChange={handleBasicInfoInputChange}
                />
                <div className="invalid-feedback">{errors.email?.message}</div>
      </div>
      
      <div className='col-md-12 pull-right'>
      <button className='btn btn-approve'>Save Changes</button>
      </div>
      </div>
      }
      </form>
      
      <form className="" onSubmit={handleSubmit2(onSubmit2)}>
{currentTab == 2 && 
      <div className='row'>
      <div className='form-group col-md-12'>
      <label>Old Password</label>
      <input type='password'
     name="password"
     id="password"
     value={updatePassword.password}
     ref={register2}
     className={`form-control ${
       errors2.password ? "is-invalid" : ""
     }`}
     onChange={handlePasswordInputChange}
   />
   <div className="invalid-feedback">{errors2.password?.message}</div>
      </div>

      <div className='form-group col-md-6'>
      <label>New Password</label>
      <input
      name="newPassword"
      id="newPassword"
      value={updatePassword.newPassword}
      type="password"
      ref={register2}
      className={`form-control ${
        errors2.newPassword ? "is-invalid" : ""
      }`}
      onChange={handlePasswordInputChange}
    />
    <div className="invalid-feedback">{errors2.newPassword?.message}</div>
      </div>

      <div className='form-group col-md-6'>
      <label>Confirm New Password</label>
      <input
      name="confirmedPassword"
      id="confirmedPassword"
      value={updatePassword.confirmedPassword}
      type="password"
      ref={register2}
      className={`form-control ${
        errors2.confirmedPassword ? "is-invalid" : ""
      }`}
      onChange={handlePasswordInputChange}
    />
    <div className="invalid-feedback">{errors2.confirmedPassword?.message}</div>
      </div>
      <div className='col-md-12 pull-right'>
      <button className='btn btn-approve'>Save Changes</button>
      </div>

      </div>
      
      }
</form>
    

      {currentTab == 3 && 
       <div className='row'>
 
       <div className='form-group col-md-6'>
       <label>Years of experience </label>
       <select
                      onChange={handleCareerInputChange}
                      className="form-control"
                      name="experienceTypeId"
                      id="experienceTypeId"
                      value={careerInfo.experienceTypeId}
                    >
                      <option value="1">0 - 2</option>
                      <option value="2">3 - 5</option>
                      <option value="3">6 - 10</option>
                      <option value="4">10 +</option>
                    </select>
       </div>

       <div className='form-group col-md-6'>
       <label>Do you have access to any of these? </label>
       <select
                      onChange={handleCareerInputChange}
                      className="form-control"
                      name="hasAccessToDifferentWebsite"
                      id="hasAccessToDifferentWebsite"
                      value={String(careerInfo.hasAccessToDifferentWebsite)}
                    >
                      <option value="1">Yes</option>

                      <option value="0">No</option>
                    </select>
       </div>

       <div className='form-group col-md-6'>
       <label>Which of these industries you more interested in </label>
       <select
                      onChange={handleCareerInputChange}
                      className="form-control"
                      name="industryId"
                      id="industryId"
                      value={careerInfo.industryId}
                    >
                      <option value="1">Business</option>
                      <option value="2">Tech</option>
                      <option value="3">Health care</option>
                      <option value="4">Admin & Sales</option>
                    </select>
       </div>
       <div className='col-md-12 pull-right'>
       <button 
       className='btn btn-approve'
       onClick={UpdateCareerInfo}
       >Save Changes</button>
       </div>
 
       </div>
      }
       {currentTab == 4 && 
          <div className='row'>
          <div className='form-group col-md-12'>
          <label>Linkedin Profile</label>
          <br/>
          <span>
                    <a target="_blank" href={basicInfo.linkedInProfile}>{basicInfo.linkedInProfile}</a>
                  </span>
          </div>

          <div className='form-group col-md-12 pull-right'>
         {/* <button style={{width:'100px'}} className='btn btn-red'>Delete CV</button>*/}
          </div> 


          <div className='form-group col-md-12'>
          <iframe src={basicInfo.cvName != null ? path : ""} width="100%" height="500px" />
          </div>

    
    
          {/* <div className='col-md-12 pull-right'>
          <button className='btn btn-approve'>Save Changes</button>
          </div> */}
    
          </div>
      
      }
      </div>
      </div>
      </div>

      </div>
    </React.Fragment>
  );
}
