import React, { useState, useEffect } from 'react';
import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import Grid from '@material-ui/core/Grid';
import jobimg from '../../../src/assets/job-bg.png'
import draftIco from '../../../src/assets/draftIco.png'
import { PDFViewer } from '@react-pdf/renderer';

// import PDFViewer from 'pdf-viewer-reactjs'
// import { Document, Page } from 'react-pdf';
// import 'pdf-viewer-reactjs-bulma-wrapped/css/main.css';
//import PDFViewer from 'pdf-viewer-reactjs-bulma-wrapped';

// Core viewer
// import { Viewer } from '@react-pdf-viewer/core';

// // Plugins
// import { defaultLayoutPlugin } from '@react-pdf-viewer/default-layout';

// // Import styles
// import '@react-pdf-viewer/core/lib/styles/index.css';
// import '@react-pdf-viewer/default-layout/lib/styles/index.css';

// // Create new plugin instance
// const defaultLayoutPluginInstance = defaultLayoutPlugin();

const pdfDoc = require("../../../src/assets/pdfDoc.pdf")

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        root: {
        },
    }),
);

export default function MyDrafts() {
    console.log('pdfDoc', pdfDoc.default);
    const classes = useStyles();

    const [numPages, setNumPages] = useState(null);
    const [pageNumber, setPageNumber] = useState(1);

    function onDocumentLoadSuccess(numPages: any) {
        setNumPages(numPages);
    }

    const [scroll, setScroll] = useState(false);
    useEffect(() => {
        window.addEventListener("scroll", () => {
            setScroll(window.scrollY > 50);
        });
    }, []);

    return (
        <React.Fragment>
            {/* <div>
                <img src={jobimg}/>
            </div> */}

            <div className={scroll ? "filters-inbox fixed-filter" : "filters-inbox bg-white"}>
                <span className='plain'>DETAILS</span>
                <span className='plain active'>0   MY DRAFTS</span>
                <span className='plain'>1   MY SUBMISSIONS</span>
             


            </div>
            <div className='container draft'>

                <div className='row'>

                    <div className='col-md-12'>
                        <div className="bread-text">
                            <span className="lt-text">My Jobs</span>
                            <span className="lt-text"><i className="fas fa-angle-right"></i></span>
                            <span className="lt-head">Drafts</span>


                        </div>
                    </div>

                </div>

                <div className='row'>
                    <div className='col-md-3'>
                        <div className='job-box'>
                            <div className='remain-img-box'>
                                <img src={draftIco} />
                            </div>

                            <div className="data-card-head">shrook cv</div>


                        </div>
                    </div>
                </div>

                <div className='row'>

                    <div className='col-md-12'>
                        <div className='sub-info-box'>
                            <div className='row'>
                                <div className='col-md-5'>
                                    <div className='bold-head'>
                                        Please fill the missing data
               <div className='shortBorder-head'>

                                        </div>
                                    </div>

                                    <div className='form-group'>
                                        <label>Profile Email</label>
                                        <input
                                            type="text"
                                            className='form-control'
                                        />
                                    </div>

                                    <div className='form-group'>
                                        <label>Phone Number</label>
                                        <input
                                            type="text"
                                            className='form-control'
                                        />
                                    </div>

                                    <div className='next-btn'>

                                        <Button type="submit" variant="contained" color="primary">
                                            Next
          </Button>
                                    </div>
                                </div>
                                <div className='col-md-7'>
                                    <iframe src={pdfDoc.default} width="100%" height="500px" />
                                    {/* <object data={pdfDoc} type="application/pdf" width="100%" height="100%">
      <p>Alternative text - include a link <a href="http://africau.edu/images/default/sample.pdf">to the PDF!</a></p>
  </object> */}

                                    {/* <Viewer
    fileUrl='https://arxiv.org/pdf/quant-ph/0410100.pdf'
    plugins={[
        // Register plugins
        defaultLayoutPluginInstance,
    ]}
/> */}
                                    {/* <PDFViewer
                document={{
                url: 'https://arxiv.org/pdf/quant-ph/0410100.pdf',
            }}
        /> */}

                                    {/* <Document
        file="https://arxiv.org/pdf/quant-ph/0410100.pdf"
        onLoadSuccess={onDocumentLoadSuccess}
      >
        <Page pageNumber={pageNumber} />
      </Document> */}
                                </div>
                            </div>
                        </div>
                    </div>

                </div>

                <div className='row sub-info'>
                    <div className='col-md-4'>
                        <div className='bold-head'>E-Commerce Manager</div>
                    </div>

                    <div className='col-md-8 sub-info-right'>
                        <span className='sub-row'>
                            <span className='bold-clr-head'>
                                5
                    </span>

                            <span className='bold-head'> remaining submissions</span>
                        </span>

                        <span className='sub-row'>
                            <span className='bold-clr-head'>
                                7 mins
                    </span>

                            <span className='bold-head'> to submit the next profile</span>
                        </span>

                        <span className='sub-row'>
                            <span className='bold-clr-head'>
                                +  Add submission
                    </span>


                        </span>

                    </div>
                </div>

                <div className='row'>
                    <div className='col-md-12'>
                        <div className='sub-info-box'>

                            <div className='feature-set'>
                                <div className='feature-set-row'>
                                    <div className='bold-head'>
                                        <span>
                                            <i className='fa fa-clock'></i>
                                        </span>
                                        <span>Employment type</span>
                                    </div>

                                    <div className='text'>
                                        Full time
                    </div>
                                </div>

                                <div className='feature-set-row'>
                                    <div className='bold-head'>
                                        <span>
                                            <i className='fa fa-clock'></i>
                                        </span>
                                        <span>Seniority level</span>
                                    </div>

                                    <div className='text'>
                                        Mid_senior
                    </div>
                                </div>

                                <div className='feature-set-row'>
                                    <div className='bold-head'>
                                        <span>
                                            <i className='fa fa-clock'></i>
                                        </span>
                                        <span>Company name</span>
                                    </div>

                                    <div className='text'>
                                        Join solutions
                    </div>
                                </div>

                                <div className='feature-set-row'>
                                    <div className='bold-head'>
                                        <span>
                                            <i className='fa fa-clock'></i>
                                        </span>
                                        <span>Location</span>
                                    </div>

                                    <div className='text'>
                                        On-Site - London, UK
                    </div>
                                </div>

                                <div className='feature-set-row'>
                                    <div className='bold-head'>
                                        <span>
                                            <i className='fa fa-clock'></i>
                                        </span>
                                        <span>Locations to source from</span>
                                    </div>

                                    <div className='text'>
                                        Within 70 miles of London, UK
                    </div>
                                </div>
                            </div>

                            <div className='feature-set-info'>
                                <div className='sub-feature'>
                                    <div className='bold-head'>
                                        Job function
                    </div>

                                    <div className='light-text'>
                                        Business Development - Marketing
                    </div>
                                </div>

                                <div className='sub-feature'>
                                    <div className='bold-head'>
                                        Company Industry
                    </div>

                                    <div className='light-text'>
                                        Computer Software - Information Technology and Services
                    </div>
                                </div>

                            </div>

                            <div className='feature-set-info'>
                                <div className='sub-feature'>
                                    <div className='bold-head'>
                                        Job Description
                    </div>

                                    <div className='light-text'>
                                        Reporting to the Division CDO, the Ecommerce Manager is responsible for driving the overall e commerce performance for the business division.  He/she is responsible to drive customer acquisition, retention and growth through digital marketing.  He/she is also responsible for driving a seamless customer/ consumer online experience across all channels.  The Ecommerce Manager will ensure that processes are effective while building relationship with key partners and retailers. He/she will work with the local markets to ensure that the strategy is adapted to local needs and that the brand presence and consumer experience is consistent
                    </div>
                                </div>

                            </div>

                            <div className='feature-set-info'>
                                <div className='sub-feature'>
                                    <div className='bold-head'>
                                        Requirements
                    </div>

                                    <div className='light-text'>
                                        Digital Savvy, ideally in the ecommerce environment
                                        Understand the omnichannel consumer journey
                                        Proven track record in driving ecommerce business growth and performance
                                        Passion and strong conviction to drive for performance
                                        Strong interpersonal and communication skills
                    </div>
                                </div>

                            </div>

                            <div className='feature-set-info'>
                                <div className='sub-feature'>
                                    <div className='bold-head'>
                                        Work experience
                    </div>

                                    <div className='light-text'>
                                        Minimum of 6 years and maximum of 15 years
                    </div>
                                </div>

                            </div>

                            <div className='feature-set-info'>
                                <div className='sub-feature'>
                                    <div className='bold-head'>
                                        Must-have qualifications
                    </div>

                                    <div className='light-text'>
                                        Digital Savvy, ideally in the ecommerce environment
                                        Understand the omnichannel consumer journey
                                        Proven track record in driving ecommerce business growth and performance
                    </div>
                                </div>

                            </div>

                            <div className='feature-set-info'>
                                <div className='sub-feature'>
                                    <div className='bold-head'>
                                        Companies not to source from
                    </div>

                                    <div className='light-text'>
                                        Join solutions
                    </div>
                                </div>

                            </div>

                            <div className='feature-set-info'>
                                <div className='sub-feature'>
                                    <div className='bold-head'>
                                        Search tips
                    </div>

                                    <div className='light-text'>
                                        Please review the client before sourcing: www.siemens.com
                                        Benchmark from previously contacted candidates
                    </div>
                                </div>

                            </div>


                        </div>
                    </div>
                </div>




            </div>

        </React.Fragment>

    );
}
