import React , {useState,useEffect} from 'react';
import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import Grid from '@material-ui/core/Grid';
import tick from '../../../src/assets/tick.png'
import cross from '../../../src/assets/cross.png'
import { RootStateOrAny, useDispatch, useSelector } from 'react-redux';

import {history} from '../../helpers/history'
import { ISourcerAccessWebSitesModel } from '../../models/ISourcerWebsiteModel';
import { sourcerActions } from '../../actions/sourcerActions';
import sourcerService from '../../services/SourcerService';

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        root: {
        },
    }),
);

const Access =() =>{

const dispatch = useDispatch();

//inital sourcer state
const [sourcerAccess, setSourcerAccess] = useState<ISourcerAccessWebSitesModel>({
    Uuid: '',
    HasAccessToDifferentWebsite : false
  });

const onSelectAccess = (accessWebsites : boolean) => {
  debugger ;
  console.log(accessWebsites);
  setSourcerAccess({ ...sourcerAccess ,HasAccessToDifferentWebsite: accessWebsites});
  debugger;
   sourcerAccess.HasAccessToDifferentWebsite = accessWebsites;
   dispatch(sourcerActions.SourcerAccessWebSites(sourcerAccess));
}

// useEffect(() => {
//   // take action when property in Object Changed
//   debugger;
//   if(sourcerAccess.HasAccessToDifferentWebsite != false)
//     dispatch(sourcerActions.SourcerAccessWebSites(sourcerAccess));
// }, [sourcerAccess.HasAccessToDifferentWebsite]);

useEffect(() => {
  GetSourcerhasAccessToWebsite();
}, []);

const GetSourcerhasAccessToWebsite = () => {
  sourcerService.GetSourcerhasAccessToWebsite()
    .then((response) => {
      debugger;
      setSourcerAccess({ ...sourcerAccess ,HasAccessToDifferentWebsite: response.data.data});
    })
    .catch((e) => {
      console.log(e);
    });
};

let currentContent: any;

  let content = {
    English: {
      Experience: "Experience",
      Jobtitle: "Job title",
      Candidatedatabase: "Candidate database",
      Industries: "Industries",
      CVProfile: "CV & Profile",
      DoYouHave: "Do you have access to any of these?",
      Linked : "( linkedin, bayt.com, naukri, etc... )",
      Back: "Back",
    },
    Arabic: {
      Experience: "الخبرة",
      Jobtitle: "عنوان الوظيفة",
      Candidatedatabase: "معلومات المتقدم",
      Industries: "التخصص",
      CVProfile: "السيرة الذتية",
      CurrentJobTitle: "لقب الوظيفة الحالية",
      Back: "رجوع",
      DoYouHave: "هل لديك وصول لأي من هذا ؟",
      Linked : "( linkedin, bayt.com, naukri, etc... )",
    },
  };

  localStorage.getItem("currentLanguage") === "Arabic"
  ? (currentContent = content.Arabic)
  : (currentContent = content.English);

return (
        <React.Fragment>
            <div className='container'>
                <div className='row'>
                    <div className='col-md-2'></div>
                    <div className='col-md-8'>
                   <div className='auth-white-bg thankyou access'>
                   <ul className='reg-nav'>
                <li>
                  <span className='fill'>
                  <i className='fa fa-check'></i>
                  </span>
                  <span className='high'>{currentContent.Experience}</span>
                </li>
                <li>
                  <span className='fill'>
                  <i className='fa fa-check'></i>
                  </span>
                  <span className='high'>{currentContent.Jobtitle}</span>
                </li>
                <li>
                  <span className='fill'>
                  <i className='fa fa-check'></i>
                  </span>
                  <span className='high'>{currentContent.Candidatedatabase} </span>
                </li>
                <li>
                  <span className='nofill'>
                  <i className='fa fa-check'></i>
                  </span>
                  <span className='nohigh'>{currentContent.Industries}</span>
                </li>
                <li>
                  <span className='nofill'>
                  <i className='fa fa-check'></i>
                  </span>
                  <span className='nohigh'>{currentContent.CVProfile}</span>
                </li>
              </ul>
                   <div className='blueHead'>
                   {currentContent.DoYouHave}
                   <div style={{fontSize:'18px'}}>
                   {currentContent.Linked}
                   </div>
                   </div>

                   <div className='row'>
                   <div className='col-md-3'>                  
                   </div>
                   <div className='col-md-3'>
                   <div onClick={() =>onSelectAccess(true)}
                    className={
                      sourcerAccess.HasAccessToDifferentWebsite
                        ? "square-box sqActive"
                        : "square-box"
                    }
                
                    >
                   <img style={{margin:'20px 0'}} src={tick}/>
                   </div>
                   </div>
                   <div className='col-md-3'>
                   <div onClick={() =>onSelectAccess(false)}
                    className={
                      sourcerAccess.HasAccessToDifferentWebsite
                        ? "square-box"
                        : "square-box sqActive"
                    }
                   >
                   <img  style={{margin:'20px 0'}} src={cross}/>
                   </div>
                   </div>
                   <div className='col-md-3'>
                   </div>
                   </div>

                 <div className='next-btn'>
                 {/* <Button onClick={() => {history.push('/sourcer/Industry')}}  type="button" variant="contained" color="primary">
                 Next
                </Button> */}
                 </div>

                 <div className='next-btn'>
                                <Button type="submit"
                                onClick={() => {
                                  history.push("/registration/sourcer-jobtitle");
                                }} variant="contained" color="primary">
                                    {currentContent.Back}
                                </Button>
                            </div>
                   </div>
                    </div>
                    
                    <div className='col-md-2'></div>
                    
                </div>
           
                
            </div>
            
        </React.Fragment>

    );
}

export default  Access