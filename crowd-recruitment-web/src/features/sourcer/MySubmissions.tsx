import React, { useState, useEffect } from "react";
import ReactAutocomplete from "react-autocomplete";
import Switch from "@material-ui/core/Switch";
import {
  createStyles,
  Theme,
  withStyles,
  WithStyles,
} from "@material-ui/core/styles";
import { Link } from "react-router-dom";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import Grid from "@material-ui/core/Grid";
import jobimg from "../../../src/assets/job-bg.png";
import decline from "../../../src/assets/decline.png";
import Dialog from "@material-ui/core/Dialog";
import MuiDialogTitle from "@material-ui/core/DialogTitle";
import MuiDialogContent from "@material-ui/core/DialogContent";
import MuiDialogActions from "@material-ui/core/DialogActions";
import IconButton from "@material-ui/core/IconButton";
import CloseIcon from "@material-ui/icons/Close";
import Typography from "@material-ui/core/Typography";
import { useDropzone } from "react-dropzone";
import JobService from "../../services/JobService";
import { IJobSubmission } from "../../models/IJobSubmission";
import { ISourcerCVModel } from "../../models/ISourcerCVModel";
import { yupResolver } from "@hookform/resolvers/yup";
import { useForm } from "react-hook-form";
import * as Yup from "yup";
import { IJobDetails } from "../../models/IJobDetails";
import candIco from "../../../src/assets/cand-ico.png";
import { useHistory } from "react-router-dom";
import { SubmissionStatusConstant } from "../../constants/SubmissionStatusConstant";
import Autocomplete from "react-autocomplete";
import { CircularProgress } from "@material-ui/core";
import Select from "react-select";
import { ISubmitJobCountries } from "../../models/ISubmitJobCountries";
import { jobStatusConstant } from "../../constants/jobStatusConstant";

const styles = (theme: Theme) =>
  createStyles({
    root: {
      margin: 0,
      padding: theme.spacing(2),
    },
    closeButton: {
      position: "absolute",
      right: theme.spacing(1),
      top: theme.spacing(1),
      color: theme.palette.grey[500],
    },
  });

export interface DialogTitleProps extends WithStyles<typeof styles> {
  id: string;
  children: React.ReactNode;
  onClose: () => void;
}

const DialogTitle = withStyles(styles)((props: DialogTitleProps) => {
  const { children, classes, onClose, ...other } = props;
  return (
    <MuiDialogTitle disableTypography className={classes.root} {...other}>
      <Typography variant="h6">{children}</Typography>
      {onClose ? (
        <IconButton
          aria-label="close"
          className={classes.closeButton}
          onClick={onClose}
        >
          <CloseIcon />
        </IconButton>
      ) : null}
    </MuiDialogTitle>
  );
});

const DialogContent = withStyles((theme: Theme) => ({
  root: {
    padding: theme.spacing(2),
  },
}))(MuiDialogContent);

const DialogActions = withStyles((theme: Theme) => ({
  root: {
    margin: 0,
    padding: theme.spacing(1),
  },
}))(MuiDialogActions);

export default function MySubmissions(props: any) {
  const history = useHistory();
  const [open, setOpen] = React.useState(false);
  const [rejectionReason, setRejectionReason] = useState("");
  const [openReason, setOpenReason] = React.useState(false);
  const [showLoader, setShowLoader] = useState(false);
  const [uploaded, setUploaded] = useState(false);

  const handleClickOpen = (paramType: any, reason: string) => {
    if (paramType == "add") {
      setOpen(true);
    } else {
      setOpenReason(true);
      setRejectionReason(reason);
    }
  };
  const handleClose = () => {
    setOpen(false);
    setOpenReason(false);
  };

  const { acceptedFiles, getRootProps, getInputProps } = useDropzone({
    accept: ".pdf",
  });

  const files = acceptedFiles.map((file: any) => (
    <li key={file.path}>
      {file.path} - {file.size} bytes
    </li>
  ));

  const [sourcerSubmissions, setAllsourcerSubmissions] = useState<
    IJobSubmission[]
  >([]);

  const drawSourcerSubmissions = sourcerSubmissions.map(
    (job: IJobSubmission) => (
      <div className="col-md-4" key={job.candidateName}>
        <div className="job-box">
          <span className="red declined none">Disqualified</span>
          <span
            className={
              job.rejectionReason == null ? "green declined" : "red declined"
            }
          >
            {job.candidateStatusName}
          </span>
          <div
            hidden={
              job.candidateSubmissionStatusId !=
              SubmissionStatusConstant.Disqualified
            }
            onClick={(event) => handleClickOpen("reason", job.rejectionReason)}
            className="red show-reason"
          >
            Show reason
          </div>
          <div className="remain-img-box">
            <img src={candIco} />
          </div>
          <div className="data-card-head">{job.candidateName}</div>
          <div className="data-card-head">{job.cvName}</div>
          <div className="data-card-light">{job.jobTitle}</div>
          {/* <div className="data-card-light">{job.jobLocationTranslation}</div> */}

          {/* <Link to="">Show Details</Link> */}
        </div>
      </div>
    )
  );

  //inital sourcer state
  const [uploadCV, setUploadCV] = useState<ISourcerCVModel>({
    Uuid: "",
    JobUuId: "",
    CvName: "",
    LinkedInProfile: "",
    Email: "",
    Location: "",
    UploadedFile: acceptedFiles[0],
    candidateName: "",
    countryId: 189,
  });
  const [isUploaded, setIsUploaded] = useState(false);
  const [jobDetails, setjobDetails] = useState<IJobDetails>({
    uuid: "",
    jobTitle: "",
    employmentTypeName: "",
    seniorityLevelName: "",
    companiesNotToSourceFrom: "",
    languageId: 2,
    jobLocation: "",
    companyName: "",
    mustHaveQualification: "",
    niceToHaveQualification: "",
    hiringNeeds: "",
    experienceLevelName: "",
    description: "",
    companyIndustry: "",
    jobStatusId: 3,
    jobStatusName: "",
    requirements: "",
    noOfSubmissions: 0,
    jobDate: new Date(),
  });

  const getSourcerSubmissions = () => {
    debugger;
    JobService.sourcerJobsSubmissions(props.match.params.id)
      .then((response) => {
        debugger;
        setAllsourcerSubmissions(response.data.data);
      })
      .catch((e) => {
        console.log(e);
      });
  };

  const getJobDetails = () => {
    debugger;
    JobService.getJobDetails(props.match.params.id)
      .then((response) => {
        debugger;
        setjobDetails(response.data.data);
      })
      .catch((e) => {
        console.log(e);
      });
  };

  useEffect(() => {
    getSourcerSubmissions();
    getJobDetails();
    getCountries();
  }, []);

  // form validation rules
  const validationRules = Yup.object().shape({
    candidateName: Yup.string().required("Candidate Name is required"),
    LinkedInProfile: Yup.string()
      .required("LinkedIn Profile is required")
      .test("regex", "Please enter a valid LinkedIn profile link ", (val) => {
        let regExp = new RegExp("^https://[a-z]{2,3}.linkedin.com/.*$");
        console.log(regExp.test(val), regExp, val);
        return regExp.test(val);
      }),
    Email: Yup.string().trim().required("Email is required"),
    // Location: Yup.string().required("Location is required"),
  });

  // functions to build form returned by useForm() hook
  const { register, handleSubmit, errors } = useForm<ISourcerCVModel>({
    resolver: yupResolver(validationRules),
  });

  const [currentCountries, setcurrentCountries] = useState<
    ISubmitJobCountries[]
  >([]);

  const [selectedCountry, setSelectedCountry] = useState<ISubmitJobCountries>({
    countryId: 189,
    countryName: "Saudi Arabia",
  });

  const getCountries = () => {
    debugger;
    JobService.getAllCountries()
      .then((response) => {
        debugger;
        setcurrentCountries(response.data.data);
      })
      .catch((e) => {
        console.log(e);
      });
  };

  function handleChange(e: any) {
    const { name, value } = e.target;
    // console.log(e.target);
    debugger;

    if (acceptedFiles.length != 0) {
      let file = acceptedFiles[0];
      setUploadCV((uploadCV) => ({
        ...uploadCV,
        [name]: value,
        UploadedFile: file,
        CvName: file.name,
      }));
      setUploaded(true);
    } else if (name == "" && value != "") {
      setUploaded(true);
    } else {
      setUploadCV((uploadCV) => ({ ...uploadCV, [name]: value }));
      setUploaded(false);
    }
  }

  function ClearForm() {
    setUploadCV({
      Uuid: "",
      JobUuId: "",
      CvName: "",
      LinkedInProfile: "",
      Email: "",
      Location: "",
      UploadedFile: acceptedFiles[0],
      candidateName: "",
      countryId: 189,
    });
    setUploaded(false);
    setShowLoader(false);
  }

  function UploadCVChanged() {
    setIsUploaded(true);
    console.log(isUploaded);
  }

  function onSubmit() {
    // display form data on success
    //alert(data);
    debugger;
    if (uploaded) {
      console.log(props.match.params.id);
      const formData = new FormData();
      formData.append("CvName", acceptedFiles[0].name);
      formData.append("UploadedFile", acceptedFiles[0]);
      formData.append("LinkedInProfile", uploadCV.LinkedInProfile);
      formData.append("Email", uploadCV.Email);
      formData.append("Location", uploadCV.Location);
      formData.append("JobUuId", props.match.params.id);
      formData.append("candidateName", uploadCV.candidateName);
      formData.append("LocationId", String(uploadCV.countryId));

      setShowLoader(true);
      debugger;
      JobService.addSourcerSubmission(formData)
        .then((response) => {
          debugger;
          setOpen(false);
          ClearForm();
          getSourcerSubmissions();
        })
        .catch((e) => {
          console.log(e);
          setShowLoader(false);
        });
    }
  }

  const [scroll, setScroll] = useState(false);
  useEffect(() => {
    window.addEventListener("scroll", () => {
      setScroll(window.scrollY > 50);
    });
  }, []);

  const [value, setSetvalue] = useState("");

  function handleChangeSelect(event: React.ChangeEvent<HTMLInputElement>) {
    uploadCV.Location = event.target.value;
    setSetvalue(event.target.value);
  }

  function handleAutoCompleteChange(selectedOption: any) {
    setSelectedCountry(selectedOption);
    uploadCV.countryId = selectedOption.countryId;
    setUploadCV(uploadCV);
  }

  function selectValue(value: any) {
    uploadCV.Location = value;
    setSetvalue(value);
  }

  return (
    <React.Fragment>
      {/* <div>
        <img src={jobimg} />
      </div> */}

      <div
        className={
          scroll ? "filters-inbox fixed-filter" : "filters-inbox bg-white"
        }
      >
        {/* <span className='plain'>0   MY DRAFTS</span> */}
        {/* <span className='plain active'>1   MY SUBMISSIONS</span> */}
        <span
          className="plain"
          onClick={() => history.push("/sourcer/jobDetails/" + jobDetails.uuid)}
        >
          DETAILS
        </span>
        <span
          className="plain active"
          onClick={() =>
            history.push("/sourcer/MySubmissions/" + jobDetails.uuid)
          }
        >
          MY SUBMISSIONS
        </span>
        {/* <span className='plain'>0   APPROVED</span>
                <span className='plain'>0   DISSQUALIFIED</span> */}
      </div>
      <div className="container">
        {/* <Button variant="outlined" color="primary" onClick={handleClickOpen}>
        Open dialog
      </Button> */}

        <div className="row">
          <div className="col-md-12">
            <div className="bread-text">
              <span className="lt-head">
                <a href="/sourcer/myJobs">My Jobs</a>
              </span>
              <span className="lt-text">
                <i className="fas fa-angle-right"></i>
              </span>
              <span className="lt-head">My Submissions</span>
            </div>
          </div>
        </div>

        <div className="row sub-info">
          <div className="col-md-4">
            <div className="bold-head">{jobDetails.jobTitle}</div>
          </div>

          <div className="col-md-8 sub-info-right">
            <span
              className="sub-row"
              onClick={(event) => handleClickOpen("add", "")}
            >
              <button
                disabled={
                  jobDetails.noOfSubmissions == sourcerSubmissions.length ||
                  jobDetails.jobStatusId == jobStatusConstant.Closed
                }
                className="btn btn-approve"
              >
                + Add submission
              </button>
            </span>
          </div>
        </div>

        <div className="row">{drawSourcerSubmissions}</div>
      </div>
      <Dialog
        fullWidth={true}
        maxWidth={"sm"}
        onClose={handleClose}
        aria-labelledby="customized-dialog-title"
        open={open}
      >
        <DialogTitle id="customized-dialog-title" onClose={handleClose}>
          Refer a candidate
        </DialogTitle>
        <form onSubmit={handleSubmit(onSubmit)} id="my-awesome-dropzone">
          <DialogContent dividers>
            <div className="col-md-12">
              <div
                onChange={handleChange}
                {...getRootProps({ className: "dropzone" })}
              >
                <input onChange={handleChange} {...getInputProps()} />
                <div className="file-drop" style={{ marginBottom: "15px" }}>
                  <div className="blackHead">Drag and drop resume here</div>
                  <div>
                    Or click <a className="anchor"> here to upload resume</a>
                  </div>
                  <div className="blackHead red">
                    {!uploaded ? "Please upload your CV" : ""}
                  </div>
                </div>
              </div>
              <aside>
                <h4>File</h4>
                <ul>{files}</ul>
              </aside>
            </div>

            <div className="form-group col-md-6">
              <label>Candidate Name</label>
              <input
                onChange={handleChange}
                type="text"
                name="candidateName"
                id="candidateName"
                placeholder="Candidate Name"
                value={uploadCV.candidateName}
                ref={register}
                className={`form-control ${
                  errors.candidateName ? "is-invalid" : ""
                }`}
              />
              <div className="invalid-feedback">
                {errors.candidateName?.message}
              </div>
            </div>

            <div className="form-group col-md-6">
              <label>Email</label>
              <input
                onChange={handleChange}
                type="email"
                name="Email"
                id="Email"
                placeholder="Email"
                value={uploadCV.Email}
                ref={register}
                className={`form-control ${errors.Email ? "is-invalid" : ""}`}
              />
              <div className="invalid-feedback">{errors.Email?.message}</div>
            </div>

            <div className="form-group col-md-12">
              <label>Linkedin Profile</label>
              <input
                onChange={handleChange}
                type="text"
                name="LinkedInProfile"
                id="LinkedInProfile"
                placeholder="LinkedIn Profile"
                value={uploadCV.LinkedInProfile}
                ref={register}
                className={`form-control ${
                  errors.LinkedInProfile ? "is-invalid" : ""
                }`}
              />
              <div className="invalid-feedback">
                {errors.LinkedInProfile?.message}
              </div>
            </div>

            <div className="form-group col-md-12">
              <label>Location</label>
              <Select
                menuPlacement="top"
                value={selectedCountry}
                onChange={handleAutoCompleteChange}
                options={currentCountries}
                getOptionLabel={(option) => option.countryName}
                getOptionValue={(option) => option.countryId.toString()}
              />
            </div>

            {/* <div className="form-group col-md-12">
              <label className="pull-left">Hot Candidate </label>
              <div className="pull-right">
                <Switch
                  id=""
                  key=""
                  checked={false}
                  color="primary"
                  name=""
                  inputProps={{ "aria-label": "primary checkbox" }}
                />
              </div>
            </div> */}
          </DialogContent>
          <DialogActions>
            <Button
              disabled={showLoader}
              type="submit"
              variant="contained"
              color="primary"
            >
              {showLoader && (
                <span className="loading">
                  <CircularProgress />
                </span>
              )}{" "}
              Submit
            </Button>
          </DialogActions>
        </form>
      </Dialog>

      <Dialog
        fullWidth={true}
        maxWidth={"sm"}
        onClose={handleClose}
        aria-labelledby="customized-dialog-title"
        open={openReason}
      >
        <DialogTitle id="customized-dialog-title3" onClose={handleClose}>
          Disqualifed CV
        </DialogTitle>

        <DialogContent className="text-center" dividers>
          <img style={{ marginTop: "20px" }} src={decline} />

          <div className="head-job-pop">
            <div className="blackHead">This CV is disqualified</div>
            <div className="shortBorder"></div>
          </div>

          <p className="blurText">
            {/* Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
            eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim
            ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut
            aliquip ex e */}
            {rejectionReason}
          </p>

          <div className="text-center job-pop-btns">
            <button onClick={handleClose} className="btn btn-approve">
              OK
            </button>
          </div>
        </DialogContent>
      </Dialog>
    </React.Fragment>
  );
}
