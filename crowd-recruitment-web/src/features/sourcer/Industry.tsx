import React ,{useState,useEffect} from 'react';
import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import Grid from '@material-ui/core/Grid';
import businessMan from '../../../src/assets/business-man.png'
import pm from '../../../src/assets/project-management.png'
import health from '../../../src/assets/healthcare.png'
import admin from '../../../src/assets/sales.png'
import { RootStateOrAny, useDispatch, useSelector } from 'react-redux';

import {history} from '../../helpers/history'
import { sourcerActions } from '../../actions/sourcerActions';
import { ISourcerIndustryModel } from '../../models/ISourcerIndustryModel';
import sourcerService from '../../services/SourcerService';

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        root: {
        },
    }),
);

const Industry = () =>{
  const classes = useStyles();
  const dispatch = useDispatch();

    //inital sourcer state
const [sourcerIndustry, setSourcerIndustry] = useState<ISourcerIndustryModel>({
  Uuid: '',
  IndustryId:0
});

const onSelectIndustry = (industryId : number) => {
    debugger ;
    console.log(industryId);
    setSourcerIndustry({...sourcerIndustry,IndustryId: industryId});
    debugger;
    sourcerIndustry.IndustryId = industryId;
    dispatch(sourcerActions.SourcerIndustry(sourcerIndustry));
}

// useEffect(() => {
//   // take action when property in Object Changed
//   debugger;
//   if(sourcerIndustry.IndustryId != 0)
//     dispatch(sourcerActions.SourcerIndustry(sourcerIndustry));
// }, [sourcerIndustry.IndustryId]);


useEffect(() => {
  GetSourcerIndustryId();
}, []);

const GetSourcerIndustryId = () => {
  sourcerService.GetSourcerIndustryId()
    .then((response) => {
      debugger;
      setSourcerIndustry({...sourcerIndustry,IndustryId: response.data.data});
    })
    .catch((e) => {
      console.log(e);
    });
};

let currentContent: any;

  let content = {
    English: {
      Experience: "Experience",
      Jobtitle: "Job title",
      Candidatedatabase: "Candidate database",
      Industries: "Industries",
      CVProfile: "CV & Profile",
      Whichoftheseindustries: "Which of these industries you more interested in?",
      Back: "Back",
    },
    Arabic: {
      Experience: "الخبرة",
      Jobtitle: "عنوان الوظيفة",
      Candidatedatabase: "معلومات المتقدم",
      Industries: "التخصص",
      CVProfile: "السيرة الذتية",
      CurrentJobTitle: "لقب الوظيفة الحالية",
      Back: "رجوع",
      DoYouHave: "هل لديك وصول لأي من هذا ؟",
      Linked : "( linkedin, bayt.com, naukri, etc... )",
      Whichoftheseindustries: "إي من هذه التخصصات تهتم بها أكثر",
    },
  };

  localStorage.getItem("currentLanguage") === "Arabic"
  ? (currentContent = content.Arabic)
  : (currentContent = content.English);



    return (
        <React.Fragment>
            <div className='container'>
                <div className='row'>
                    <div className='col-md-2'></div>
                    <div className='col-md-8'>
                   <div className='auth-white-bg thankyou access'>
                   <ul className='reg-nav'>
                <li>
                  <span className='fill'>
                  <i className='fa fa-check'></i>
                  </span>
                  <span className='high'>{currentContent.Experience}</span>
                </li>
                <li>
                  <span className='fill'>
                  <i className='fa fa-check'></i>
                  </span>
                  <span className='high'>{currentContent.Jobtitle}</span>
                </li>
                <li>
                  <span className='fill'>
                  <i className='fa fa-check'></i>
                  </span>
                  <span className='high'>{currentContent.Candidatedatabase} </span>
                </li>
                <li>
                  <span className='fill'>
                  <i className='fa fa-check'></i>
                  </span>
                  <span className='high'>{currentContent.Industries}</span>
                </li>
                <li>
                  <span className='nofill'>
                  <i className='fa fa-check'></i>
                  </span>
                  <span className='nohigh'>{currentContent.CVProfile}</span>
                </li>
              </ul>
                   <div className='blueHead'>
                   {currentContent.Whichoftheseindustries}
                   </div>

                   <div onClick={() =>onSelectIndustry(1)} className='row'>
                   <div className='col-md-3'>
                   <div         className={
                        sourcerIndustry.IndustryId == 1
                          ? "square-box sqActive"
                          : "square-box"
                      }>
                   <img style={{margin:'20px 0 10px 0'}} src={businessMan}/>
                   <div    className='img-text'>Business</div>
                   </div>
                   </div>
                   <div onClick={() =>onSelectIndustry(2)} className='col-md-3'>
                   <div         className={
                        sourcerIndustry.IndustryId == 2
                          ? "square-box sqActive"
                          : "square-box"
                      }>
                   <img style={{margin:'20px 0 10px 0'}} src={pm}/>
                   <div    className='img-text'>Tech</div>
                   </div>
                   </div>

                   <div onClick={() =>onSelectIndustry(3)} className='col-md-3'>
                   <div         className={
                        sourcerIndustry.IndustryId == 3
                          ? "square-box sqActive"
                          : "square-box"
                      }>
                   <img style={{margin:'20px 0 10px 0'}} src={health}/>
                   <div    className='img-text'>Health care</div>
                   </div>
                   </div>

                   <div onClick={() =>onSelectIndustry(4)}  className='col-md-3'>
                   <div         className={
                        sourcerIndustry.IndustryId == 4
                          ? "square-box sqActive"
                          : "square-box"
                      }>
                   <img style={{margin:'20px 0 10px 0'}} src={admin}/>
                   <div   className='img-text'>Admin & Sales</div>
                   </div>
                   </div>
               
                   </div>

                   <div className='next-btn'>
                                <Button type="submit"
                                onClick={() => {
                                  history.push("/registration/sourcer-access");
                                }} variant="contained" color="primary">
                                    {currentContent.Back}
                                </Button>
                            </div>

                 {/* <div className='next-btn'>
                 <Button   onClick={() => {history.push('/sourcer/UploadResume')}}  type="button" variant="contained" color="primary">
                 Next
                </Button>
                 </div> */}
                   </div>
                    </div>
                    <div className='col-md-2'></div>
                </div>
           
            </div>

        </React.Fragment>

    );
}

export default Industry