import React, { useState, useEffect } from "react";
import {
  createStyles,
  Theme,
  withStyles,
  WithStyles,
  makeStyles,
} from "@material-ui/core/styles";
import tick from "../../../src/assets/tick.png";

import IconButton from "@material-ui/core/IconButton";
import CloseIcon from "@material-ui/icons/Close";

import AppBar from "@material-ui/core/AppBar";
import Tabs from "@material-ui/core/Tabs";
import Tab from "@material-ui/core/Tab";
import Typography from "@material-ui/core/Typography";
import Box from "@material-ui/core/Box";
import sourcerService from "../../services/SourcerService";
import employerService from "../../services/EmployerService";
import { ISourcerDetails } from "../../models/ISourcerDetails";
import candIco from "../../../src/assets/cand-ico.png";
import JobService from "../../services/JobService";
import AdminService from "../../services/AdminService";
import { IJobCandidate } from "../../models/IJobCandidate";
import { IUserRequest } from "../../models/IUserRequest";

import commission from "../../../src/assets/commission.png";
import questionMark from "../../../src/assets/question-mark.png";

import Dialog from "@material-ui/core/Dialog";
import MuiDialogTitle from "@material-ui/core/DialogTitle";
import MuiDialogContent from "@material-ui/core/DialogContent";
import MuiDialogActions from "@material-ui/core/DialogActions";

import { useForm } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import * as Yup from "yup";
import { SubmissionStatusConstant } from "../../constants/SubmissionStatusConstant";
import { ICandidatStatusHistory } from "../../models/ICandidatStatusHistory";
import CandidateService from "../../services/CandidateService";
import { RootStateOrAny, useDispatch, useSelector } from "react-redux";

import { EditorState, convertToRaw, ContentState } from "draft-js";
import Draft, {
  htmlToDraft,
  EmptyState,
  rawToDraft,
  draftToRaw,
  draftStateToHTML,
} from "react-wysiwyg-typescript";

import draftToHtml from "draftjs-to-html";
import { IResponseData } from "../../models/IResponseData";
import { alertActions } from "../../actions";

const styles = (theme: Theme) =>
  createStyles({
    root: {
      margin: 0,
      padding: theme.spacing(2),
    },
    closeButton: {
      position: "absolute",
      right: theme.spacing(1),
      top: theme.spacing(1),
      color: theme.palette.grey[500],
    },
  });

export interface DialogTitleProps extends WithStyles<typeof styles> {
  id: string;
  children: React.ReactNode;
  onClose: () => void;
}

const DialogTitle = withStyles(styles)((props: DialogTitleProps) => {
  const { children, classes, onClose, ...other } = props;
  return (
    <MuiDialogTitle disableTypography className={classes.root} {...other}>
      <Typography variant="h6">{children}</Typography>
      {onClose ? (
        <IconButton
          aria-label="close"
          className={classes.closeButton}
          onClick={onClose}
        >
          <CloseIcon />
        </IconButton>
      ) : null}
    </MuiDialogTitle>
  );
});

const DialogContent = withStyles((theme: Theme) => ({
  root: {
    padding: theme.spacing(2),
  },
}))(MuiDialogContent);

const DialogActions = withStyles((theme: Theme) => ({
  root: {
    margin: 0,
    padding: theme.spacing(1),
  },
}))(MuiDialogActions);

interface TabPanelProps {
  children?: React.ReactNode;
  index: any;
  value: any;
}

function TabPanel(props: TabPanelProps) {
  const { children, value, index, ...other } = props;

  return (
    <div
      role="tabpanel"
      hidden={value !== index}
      id={`simple-tabpanel-${index}`}
      aria-labelledby={`simple-tab-${index}`}
      {...other}
    >
      {value === index && <Box p={3}>{children}</Box>}
    </div>
  );
}

function a11yProps(index: any) {
  return {
    id: `simple-tab-${index}`,
    "aria-controls": `simple-tabpanel-${index}`,
  };
}

const useStyles = makeStyles((theme: Theme) => ({
  root: {
    flexGrow: 1,
    backgroundColor: theme.palette.background.paper,
  },
}));

export default function Summary(props: any) {
  const [openApprove, setOpenApprove] = React.useState(false);
  const [openDecline, setOpenDecline] = React.useState(false);
  const [sendMessage, setSendMessage] = React.useState(false);
  const closeSendAlert = () => {
    setSendMessage(false);
  };

  const [isApprove, setIsApprove] = React.useState(false);
  const [isDeclined, setIsDeclined] = React.useState(false);
  const [disableSend, setDisableSend] = React.useState(false);

  const showSendAlert = () => {
    setSendMessage(true);
  };

  const [value, setValue] = React.useState(0);
  const [pdfDoc, setPdfDoc] = useState(
    require("../../../src/UploadCVs/resume_012.pdf")
  );

  const handleChange = (event: React.ChangeEvent<{}>, newValue: number) => {
    setValue(newValue);
  };

  const [reason, setReason] = useState<string>("");

  const handleInputChange = (event: React.ChangeEvent<any>) => {
    debugger;
    const { name, value } = event.target;
    setReason(value);
  };

  const acceptCandidate = () => {
    const currentCandidate: IUserRequest = {
      uuid: props.match.params.id,
      rejectedReason: "",
    };
    debugger;
    setIsApprove(true);
    employerService
      .acceptCandidate(currentCandidate)
      .then((response) => {
        handleClose();
        getJobCandidateByUuId();
        //props.history.push("/employer/dashboard");
        getCandidateStatusHistoryByUuId();
      })
      .catch((e) => {
        console.log(e);
        setIsApprove(false);
      });
  };

  const declineCandidate = () => {
    const currentCandidate: IUserRequest = {
      uuid: props.match.params.id,
      rejectedReason: reason,
    };
    debugger;
    setIsDeclined(true);
    employerService
      .rejectCandidate(currentCandidate)
      .then((response) => {
        handleClose();
        getJobCandidateByUuId();
        //props.history.push("/employer/dashboard");
        getCandidateStatusHistoryByUuId();
      })
      .catch((e) => {
        console.log(e);
        setIsDeclined(false);
      });
  };

  const dispatch = useDispatch();
  const SendMail = () => {
    currentCandidate.candidateUuId = String(props.match.params.id);
    debugger;

    AdminService.SendMail(currentCandidate).then(
      (user) => {
        debugger;
        let responseDetails: IResponseData = user.data;
        dispatch(alertActions.success(responseDetails.message));
      },
      (error) => {
        debugger;
        let responseDetails: IResponseData = error.response.data;
        dispatch(alertActions.error(responseDetails.message));
      }
    );
    //     .then((response) => {
    //       showSendAlert();
    //     })
    //     .catch((e) => {
    //       console.log(e);
    //       closeSendAlert();
    //     })
    // );
  };

  const validationSchema = Yup.object().shape({
    rejectedReason: Yup.string().required("Rejection reason is required"),
  });

  const { register, handleSubmit, reset, errors } = useForm({
    resolver: yupResolver(validationSchema),
  });

  function onSubmit() {
    declineCandidate();
  }

  var dateFormat = require("dateformat");

  const [currentCandidate, setCurrentCandidate] = useState<IJobCandidate>({
    candidateName: "",
    countryName: "",
    cvName: "",
    jobDate: "",
    jobId: 0,
    jobTitle: "",
    jobUuId: "",
    languageId: 2,
    linkedInProfile: "",
    rowNumber: 0,
    sourcerId: 0,
    sourcerName: "",
    submissionDate: "",
    submissionUuId: "",
    candidateSubmissionStatusId: 0,
    firstMailSubject: "",
    firstMailDescription: "",
    email: "",
    rejectionReason: "",
    candidateUuId: props.match.params.id,
    submissionStatusName: "",
    isCvApproved: false,
    isAcceptOffer: false,
    isHired: false,
    hasInterview: 0,
    jobStatusId: 0,
  });
  const [message, setMessage] = useState("");
  const [path, setPath] = useState("");

  const html = currentCandidate.firstMailDescription;
  const [editorState, seteditorState] = useState(htmlToDraft(html));

  const getJobCandidateByUuId = () => {
    JobService.getJobCandidateByUuId(props.match.params.id)
      .then((response) => {
        if (response.data.data.candidateSubmissionStatusId == 5) {
          setDisableSend(false);
        } else {
          setDisableSend(true);
        }

        debugger;
        seteditorState(htmlToDraft(response.data.data.firstMailDescription));
        setCurrentCandidate(response.data.data);
        currentCandidate.cvName = response.data.data.cvName;
        openPdf(response.data.data.cvName);
      })
      .catch((e) => {
        console.log(e);
      });
  };

  const openPdf = (fileName: string) => {
    sourcerService
      .getPdf(fileName)
      .then((response) => {
        debugger;
        console.log(response.data);
        //Create a Blob from the PDF Stream
        const file = new Blob([response.data], { type: "application/pdf" });
        //Build a URL from the file
        const fileURL = URL.createObjectURL(file);
        setPath(fileURL);
        //Open the URL on new Window
        //window.open(fileURL);
      })
      .catch((e) => {
        console.log(e);
      });
  };

  useEffect(() => {
    getJobCandidateByUuId();
    getCandidateStatusHistoryByUuId();
  }, [props.id]);

  const handleDraftChange = (draftText: any) => {
    seteditorState(draftText);
    // SAVING HTML TO SERVER

    const convertedHtml = draftToHtml(
      convertToRaw(editorState.getCurrentContent())
    );
    // const html = draftToHtml(draftText)
    currentCandidate.firstMailDescription = convertedHtml;
    setCurrentCandidate(currentCandidate);
  };

  const handleClickOpen = (paramType: any) => {
    debugger;
    console.log(paramType);
    if (paramType == "tick") {
      setOpenApprove(true);
    }
    if (paramType == "cross") {
      setOpenDecline(true);
    }
  };
  const handleClose = () => {
    setOpenApprove(false);
    setOpenDecline(false);
  };

  const [candidateStatusHistory, setAllCandidateStatusHistory] = useState<
    ICandidatStatusHistory[]
  >([]);

  const drawCandidateStatusHistories = candidateStatusHistory.map(
    (status: ICandidatStatusHistory) => (
      <li className="completed">
        <h5
          className="mt-0 mb-1"
          style={{
            color:
              // SubmissionStatusConstant.Disqualified ==
              //   status.candidateSubmissionStatusId ||
              // SubmissionStatusConstant.DeclinedByEmployer ==
              //   status.candidateSubmissionStatusId
              //   ? "red"
              //: "blue",
              "blue",
          }}
        >
          {status.statusName}{" "}
        </h5>
        <p className="text-muted">
          {" "}
          {dateFormat(status.createDate, "d mmmm, yyyy HH:MM TT")}
        </p>
      </li>
    )
  );

  const getCandidateStatusHistoryByUuId = () => {
    debugger;
    CandidateService.GetCandidateStatusHistory(props.match.params.id)
      .then((response) => {
        console.log(response.data);
        setAllCandidateStatusHistory(response.data.data);
      })
      .catch((e) => {
        console.log(e);
      });
  };

  let currentContent: any;

  let content = {
    English: {
      Candidates: "Candidates",
      CV: "CV",
      Contact: "Contact",
      Emailsubject: "Email subject",
      Emaildescription: "Email description",
      Send: "Send",
      ThisCVisdisqualified: "This candidate is disqualified",
    },
    Arabic: {
      Candidates: "المتقدمين",
      CV: "السيرة الذاتية",
      Contact: "التواصل",
      Emailsubject: "عنوان البريد",
      Emaildescription: "وصف البريد",
      Send: "إرسال",
      ThisCVisdisqualified: "هذا المتقدم غير مطابق للشروط",
    },
  };

  localStorage.getItem("currentLanguage") === "Arabic"
    ? (currentContent = content.Arabic)
    : (currentContent = content.English);

  return (
    <React.Fragment>
      <div className="container candidate-tabs">
        <div className="listing-admin">
          <div className="bread-text">
            <span className="lt-text">
              <a href={"/candidate/jobBoard/" + currentCandidate.jobUuId}>
                {currentContent.Candidates}
              </a>
            </span>
            <span className="lt-text">
              <i className="fas fa-angle-right"></i>
            </span>
            <span className="lt-head">{currentCandidate.candidateName}</span>
          </div>
          <div className="row">
            <div className="col-md-5">
              <div className="data-card data-info">
                <div className="data-img">
                  <img src={candIco} />
                </div>
                <div className="data-card-head">
                  {currentCandidate.candidateName}
                </div>
                <div className="data-card-subhead">
                  {currentCandidate.jobTitle}
                </div>
                <div className="data-card-light">
                  {currentCandidate.countryName}
                </div>

                <div className="data-links">
                  <span className="table-link">
                    <a href={currentCandidate.linkedInProfile}>in</a>
                  </span>
                  {/* <span className='table-link'> <i className='fa fa-phone'></i> </span>
          <span className='table-link'><i className='fa fa-envelope'></i></span> */}
                </div>

                <div className="track-order-list">
                  <ul className="list-unstyled">
                    {drawCandidateStatusHistories}
                    {/* <li className="completed">
                      <h5 className="mt-0 mb-1">Screening </h5>
                      <p className="text-muted"> </p>
                    </li>
                    <li>
                      <span className="active-dot dot"></span>
                      <h5 className="mt-0 mb-1">Offer</h5>
                      <p className="text-muted"></p>
                    </li>
                    <li>
                      <h5 className="mt-0 mb-1"> Hire</h5>
                      <p className="text-muted"></p>
                    </li> */}
                    {/* <li className="disq">
                      <h5 className="mt-0 mb-1" style={{ color: "red" }}>
                        Disqualified{" "}
                      </h5>
                      <p className="text-muted">Dec 2019 - 12:00 PM</p>
                    </li> */}
                  </ul>

                  {/* <div
                    hidden={
                      currentCandidate.candidateSubmissionStatusId !=
                        SubmissionStatusConstant.Disqualified &&
                      currentCandidate.candidateSubmissionStatusId !=
                        SubmissionStatusConstant.DeclinedByEmployer
                    }
                    className="red medium"
                  >
                    {currentContent.ThisCVisdisqualified}
                  </div>
                  <p>{currentCandidate.rejectionReason}</p> */}
                </div>
              </div>

              {/* <div className="data-btns text-center">
                <button
                  style={{
                    display:
                      currentCandidate.candidateSubmissionStatusId !=
                        SubmissionStatusConstant.ApprovedByEmployer &&
                      currentCandidate.candidateSubmissionStatusId !=
                        SubmissionStatusConstant.DeclinedByEmployer
                        ? "inline"
                        : "none",
                  }}
                  onClick={(event) => handleClickOpen("cross")}
                  className="btn btn-decline"
                >
                  Decline
                </button>
                <button
                  style={{
                    display:
                      currentCandidate.candidateSubmissionStatusId !=
                        SubmissionStatusConstant.ApprovedByEmployer &&
                      currentCandidate.candidateSubmissionStatusId !=
                        SubmissionStatusConstant.DeclinedByEmployer
                        ? "inline"
                        : "none",
                  }}
                  className="btn btn-approve"
                  onClick={(event) => handleClickOpen("tick")}
                >
                  Approve
                </button>
              </div> */}
            </div>
            <div className="col-md-7">
              <div className="data-card">
                <AppBar position="static">
                  <Tabs
                    value={value}
                    onChange={handleChange}
                    aria-label="simple tabs example"
                  >
                    <Tab label={currentContent.CV} {...a11yProps(0)} />
                    {/* <Tab label={currentContent.Contact} {...a11yProps(1)} /> */}
                    {/* <Tab label="Profile" {...a11yProps(2)} /> */}
                  </Tabs>
                </AppBar>
                <TabPanel value={value} index={0}>
                  {/* <iframe src={pdfDoc.default} width="100%" height="500px" /> */}
                  {/* <embed
                    src={
                      "https://crowd-recruitment.joinsolutions.app/UploadCVs/" +
                      currentCandidate.cvName
                    }
                    width="100%"
                    height="500px"
                  /> */}
                  <iframe src={path} width="100%" height="500px" />
                </TabPanel>
                <TabPanel value={value} index={1}>
                  <div className="row">
                    <div className="form-group col-md-12">
                      <label>{currentContent.Emailsubject}</label>
                      <input
                        name="FirstMailSubject"
                        className="form-control"
                        value={currentCandidate.firstMailSubject}
                      />
                    </div>

                    <div className="form-group col-md-12">
                      <label>{currentContent.Emaildescription}</label>
                      {/* Visit  this package to check it's event and state change to get the output */}
                      {/* https://www.npmjs.com/package/react-wysiwyg-typescript */}

                      <Draft
                        readOnly={true}
                        editorState={editorState}
                        name="firstMailDescription"
                        id="firstMailDescription"
                        // value={job.firstMailDescription}
                        ref={register}
                        className={`form-control ${
                          errors.firstMailDescription ? "is-invalid" : ""
                        }`}
                        // onEditorStateChange={(editorState) => { seteditorState(editorState) }}
                        onEditorStateChange={(editorState) =>
                          handleDraftChange(editorState)
                        }
                      />

                      {/* <textarea
                        name="FirstMailDescription"
                        className="form-control"
                        // style={{ height: 150 }}
                        rows={25}
                        value={currentCandidate.firstMailDescription}
                      /> */}
                    </div>

                    <div className="pull-right data-btns">
                      <button
                        type="button"
                        className="btn btn-approve"
                        onClick={SendMail}
                        disabled={disableSend}
                      >
                        {currentContent.Send}
                      </button>
                    </div>
                  </div>
                </TabPanel>
                <TabPanel value={value} index={2}>
                  <div className="feature-set-info">
                    <div className="sub-feature">
                      <div className="bold-head">Feedback Snapshot</div>

                      <div className="light-text">XYZ</div>
                    </div>
                  </div>

                  <div className="feature-set-info">
                    <div className="sub-feature">
                      <div className="bold-head">Experience</div>

                      <div className="mb5">
                        ( 0 years 7 months ) - KSA
                        <span className="light-text pull-right">
                          Jul 2019 - Present
                        </span>
                      </div>
                      <div className="mb5">
                        Project coordinator & Sales ( 7 years 0 months ) - KSA
                        <span className="light-text pull-right">
                          Jul 2019 - Present
                        </span>
                      </div>
                    </div>
                  </div>

                  <div className="feature-set-info">
                    <div className="sub-feature">
                      <div className="bold-head">Application form details</div>

                      <div className="mb5">
                        <b>Are you a Bachelor Holder?</b>
                        <span className="light-text pull-right">No</span>
                      </div>

                      <div className="mb5">
                        <b>Do you live in Riyadh?</b>
                        <span className="light-text pull-right">No</span>
                      </div>

                      <div className="mb5">
                        <b>Years of Experience (Entertainment Industry)</b>
                        <span className="light-text pull-right">No</span>
                      </div>

                      <div className="mb5">
                        <b>Are you a Saudi Arabian?</b>
                        <span className="light-text pull-right">No</span>
                      </div>
                    </div>
                  </div>

                  <div className="feature-set-info">
                    <div className="sub-feature">
                      <div className="bold-head">Personal Details</div>

                      <div className="mb5">
                        Gender:
                        <span className="light-text"> Jul 2019 - Present</span>
                      </div>
                      <div className="mb5">
                        Date of birth:
                        <span className="light-text"> Jul 2019 - Present</span>
                      </div>
                    </div>
                  </div>
                </TabPanel>
              </div>
            </div>
          </div>
        </div>
      </div>

      <Dialog
        fullWidth={true}
        maxWidth={"sm"}
        onClose={handleClose}
        aria-labelledby="customized-dialog-title"
        open={openDecline}
      >
        <DialogTitle id="customized-dialog-title3" onClose={handleClose}>
          Decline Candidate
        </DialogTitle>
        <form className="" onSubmit={handleSubmit(onSubmit)} onReset={reset}>
          <DialogContent className="text-center" dividers>
            <img style={{ marginTop: "20px" }} src={questionMark} />

            <div className="head-job-pop">
              <div className="blackHead">
                Type a reason to decline the candidate
              </div>
              <div className="shortBorder"></div>
            </div>

            <div className="form-group">
              <label>Enter your reason </label>
              <textarea
                name="rejectedReason"
                id="rejectedReason"
                value={reason}
                ref={register}
                rows={6}
                placeholder="Reason"
                className="form-control"
                onChange={handleInputChange}
              />
              <div className="invalid-feedback">
                {errors.rejectedReason?.message}
              </div>
            </div>

            <div className="text-center job-pop-btns">
              <button onClick={handleClose} className="btn btn-decline">
                Cancel
              </button>
              <button
                disabled={isDeclined}
                type="submit"
                className="btn btn-approve"
              >
                Confirm
              </button>
            </div>
          </DialogContent>
        </form>
      </Dialog>

      <Dialog
        fullWidth={true}
        maxWidth={"sm"}
        onClose={handleClose}
        aria-labelledby="customized-dialog-title"
        open={openApprove}
      >
        <DialogTitle id="customized-dialog-title2" onClose={handleClose}>
          Commission
        </DialogTitle>

        <DialogContent className="text-center" dividers>
          <img style={{ marginTop: "20px" }} src={questionMark} />

          <div className="head-job-pop">
            <div className="blackHead">
              Are you sure You want to accept that candidate
            </div>
            <div className="shortBorder"></div>
          </div>

          <div className="text-center job-pop-btns">
            <button onClick={handleClose} className="btn btn-decline">
              Cancel
            </button>
            <button
              disabled={isApprove}
              onClick={acceptCandidate}
              className="btn btn-approve"
            >
              Confirm
            </button>
          </div>
        </DialogContent>
      </Dialog>
    </React.Fragment>
  );
}
