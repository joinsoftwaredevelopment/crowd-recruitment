import React, { useState, useEffect } from "react";
import { createStyles, makeStyles, Theme } from "@material-ui/core/styles";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import Grid from "@material-ui/core/Grid";
import thank from "../../../src/assets/thankful.png";
import candIco from "../../../src/assets/cand-ico.png";
import { IJobCandidate } from "../../models/IJobCandidate";
import { useHistory } from "react-router-dom";
import JobService from "../../services/JobService";

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {},
  })
);

export default function Candidates(props: any) {
  const classes = useStyles();
  const history = useHistory();
  const [allJobCandidates, setAllJobCandidates] = useState<IJobCandidate[]>([]);

  let currentContent: any;

  let content = {
    English: {
      Candidates: "Candidates",
      MyJobs: "My Jobs",
      Details: "Details",
    },
    Arabic: {
      Details: "التفاصيل",
      Candidates: "المتقدمين",
      MyJobs: "وظائفي",
    },
  };

  localStorage.getItem("currentLanguage") === "Arabic"
    ? (currentContent = content.Arabic)
    : (currentContent = content.English);

  const drawAllJobCandidates = allJobCandidates.map((job: IJobCandidate) => (
    <div className="col-md-4" key={job.rowNumber}>
      <div className="job-box">
        <div className="remain-img-box">
          <img src={candIco} />
        </div>

        <div className="data-card-head">{job.candidateName}</div>
        <div className="data-card-head">{job.cvName}</div>
        <div className="data-card-light">{job.jobTitle}</div>
        <div className="data-card-light">Owner: {job.sourcerName}</div>
        <div className="data-card-light">
          Submission status : {job.submissionStatusName}
        </div>

        <div className="job-card-btm">
          <div className="data-card-light">
            <span className="table-link">
              <a href={job.linkedInProfile}>in</a>
            </span>
            {/* <span className='table-link'> <i className='fa fa-phone'></i> </span>
         <span className='table-link'><i className='fa fa-envelope'></i></span> */}
          </div>
          <div className="data-card-light-green">
            <a href={"/candidate/summary/" + job.submissionUuId}>
              {currentContent.Details}
            </a>{" "}
          </div>
        </div>
      </div>
    </div>
  ));

  useEffect(() => {
    getAllJobCandidates();
  }, []);

  const getAllJobCandidates = () => {
    debugger;
    JobService.getAcceptedJobCandidates(props.match.params.id)
      .then((response) => {
        debugger;
        setAllJobCandidates(response.data.data);
      })
      .catch((e) => {
        console.log(e);
      });
  };

  return (
    <React.Fragment>
      <div className="container emp-job candidate">
        <div className="row">
          <div className="col-md-12">
            <div className="top-head">
              <div className="bold-head">{currentContent.MyJobs}</div>
              {/* <button className='btn btn-decline'>
                <i className='fa fa-plus'></i>
                Add Job
                </button> */}
            </div>
            <div className="job-bg">
              <div className="head-sec">
                <div className="bread-text">
                  <span className="lt-head">
                    <a href="/employer/dashboard/0">{currentContent.MyJobs}</a>
                  </span>
                  <span className="lt-text">
                    <i className="fas fa-angle-right"></i>
                  </span>
                  <span className="lt-head">{currentContent.Candidates}</span>
                </div>
              </div>

              <div className="row">{drawAllJobCandidates}</div>
            </div>
          </div>
        </div>
      </div>
    </React.Fragment>
  );
}
