import React from "react";
import { createStyles, makeStyles, Theme } from "@material-ui/core/styles";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import arrowIcon from "../../assets/arrow-icon.png";
import split1 from "../../assets/5.png";
import blurNext from "../../assets/blur-next.png";
import sliderImg from "../../assets/profile.png";
import arrowRight from "../../assets/arrow-icon.png";
import arrowPrev from "../../assets/arrow-prev.png";

import src1 from "../../assets/src-1.png";
import src2 from "../../assets/src-2.png";
import src3 from "../../assets/src-3.png";
import src4 from "../../assets/src-4.png";

import dumSlider from "../../assets/dum-slider.png";
import split2 from "../../assets/7.png";
import split3 from "../../assets/6.png";
import split4 from "../../assets/8.png";
import circles from "../../assets/circles.png";
import quality from "../../assets/quality.png";
import fast from "../../assets/fast.png";
import lowcost from "../../assets/low-cost.png";
import { AppHeaderSourcerHome } from "../../components/appHeaderSourcerHome";
import AppFooter from "../../components/appFooter";
import { useHistory } from "react-router-dom";

import Slider from "react-slick";
import "slick-carousel/slick/slick-theme.css";
import "slick-carousel/slick/slick.css";

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {},
  })
);

export default function SourcerHomePage(props: any) {
  const { className, style, onClick } = props;
  const classes = useStyles();
  const history = useHistory();
  function SampleNextArrow(props: any) {
    const { className, style, onClick } = props;
    return (
      <div className={className} onClick={onClick}>
        <img className="sL-next" src={arrowRight} />
      </div>
    );
  }

  function SamplePrevArrow(props: any) {
    const { className, style, onClick } = props;
    return (
      <div className={className} onClick={onClick}>
        <img className="sL-prev" src={arrowPrev} />
      </div>
    );
  }

  //   function SampleNextArrow() {
  //     const { className, style, onClick } = props;
  //     return <div className={className}>
  //         <img className="sL-next" onClick={onClick} src={arrowRight} />
  //     </div>;
  //   }

  //   function SamplePrevArrow(props: any) {
  //     const { className, style, onClick } = props;
  //     return <div className={className}>
  //      <img className="sL-prev" onClick={onClick} src={arrowPrev} />
  //     </div>;

  //   }

  //   function SampleNextArrow2() {
  //     const { className, style, onClickN } = props;
  //     return <img className="sL-next" onClick={onClick} src={arrowRight} />;
  //   }

  //   function SamplePrevArrow2(props: any) {
  //     const { className, style, onClick } = props;
  //     return <img className="sL-prev" onClick={onClick} src={arrowPrev} />;
  //   }

  var settings = {
    className: "",
    centerMode: true,
    infinite: true,
    // centerPadding: "88px",
    slidesToShow: 1,
    speed: 500,
    focusOnSelect: true,
    nextArrow: <SampleNextArrow />,
    prevArrow: <SamplePrevArrow />,
  };

  var settings2 = {
    className: "",
    dots: true,
    centerMode: true,
    infinite: true,
    // centerPadding: "88px",
    slidesToShow: 1,
    speed: 500,
    focusOnSelect: true,
    nextArrow: <SampleNextArrow />,
    prevArrow: <SamplePrevArrow />,
  };

  return (
    <React.Fragment>
      <div className="layout-home sourcer-home">
        <AppHeaderSourcerHome />

        <section className="hero-section">
          <div className="container">
            <div className="row banner-text">
              <div className="blackHead">Earn Cash Sourcing Profiles!</div>
              <div className="shortBorder"></div>

              <p>
                You’re in Demand. Become a sourcer with Visage <br /> by
                sourcing quality profiles - no need to contact <br />{" "}
                candidates, we manage the rest.
              </p>

              <div className="blackSubHead">
                Start Earning Cash
                <span>
                  <img src={arrowIcon} />
                </span>
              </div>
            </div>
          </div>
        </section>

        <section className="features">
          <div className="container">
            <div className="row">
              <div className="col-md-4">
                <div className="srcFeature">
                  <img src={fast} />
                  <div className="featureHead">Earn Fast</div>
                  <p>
                    Unlike all the other commission-based platforms, Join pays
                    you per profile submitted on a weekly basis.
                  </p>
                </div>
              </div>

              <div className="col-md-4">
                <div className="srcFeature">
                  <img src={quality} />
                  <div className="featureHead">Simple profile submission</div>
                  <p>
                    Search online and upload candidate profiles. Nothing else is
                    required.
                  </p>
                </div>
              </div>

              <div className="col-md-4">
                <div className="srcFeature">
                  <img src={lowcost} />
                  <div className="featureHead">
                    Make money recruiting from home
                  </div>
                  <p>
                    Work whenever you want, wherever you want. All you need is
                    internet access.
                  </p>
                </div>
              </div>
            </div>
          </div>
        </section>

        <section className="howSlider">
          <div className="container">
            <div className="splitHead text-center p0">How It works</div>
            <div className="shortBorder"></div>
            <Slider {...settings2}>
             
                <div className="row">
                  <div className="col-md-6">
                    <img src={src1} />
                  </div>
                  <div className="col-md-6">
                    <div className="splitHead text-left">Sign Up</div>
                    <div className="shortBorder text-left mLR0"></div>
                    <p className="text-left">
                      Register to be a sourcer by filling out a user profile{" "}
                      <br /> and completing a trial.
                    </p>
                  </div>
                </div>
             
            
                <div className="row">
                  <div className="col-md-6">
                    <img src={src2} />
                  </div>
                  <div className="col-md-6">
                    <div className="splitHead text-left">Claim jobs</div>
                    <div className="shortBorder text-left mLR0"></div>
                    <p className="text-left">
                      Select the jobs you would like to work on according to
                      your availability. You choose the roles that you are most
                      comfortable sourcing for.
                    </p>
                  </div>
                </div>
            

         
                <div className="row">
                  <div className="col-md-6">
                    <img src={src3} />
                  </div>
                  <div className="col-md-6">
                    <div className="splitHead text-left">Search profiles</div>
                    <div className="shortBorder text-left mLR0"></div>
                    <p className="text-left">
                      Search for profiles from any and all professional
                      networking platforms you have access to. Don’t spend hours
                      connecting with candidates, just search the most relevant
                      profiles online and submit them to us, we take care of the
                      rest
                    </p>
                  </div>
                </div>
           

           
                <div className="row">
                  <div className="col-md-6">
                    <img src={src4} />
                  </div>
                  <div className="col-md-6">
                    <div className="splitHead text-left">Get Paid</div>
                    <div className="shortBorder text-left mLR0"></div>
                    <p className="text-left">
                      Receive weekly payments for all sourced profiles. With
                      Visage, you get paid per sourced profile. No need to wait
                      for a placement. Simply source great profiles and receive
                      weekly payments.
                    </p>
                  </div>
                </div>
          
            </Slider>
          </div>
        </section>

        <section className="blueSec">
          <img src={circles} />
          <div className="bluesecHead">Ready to start earning cash?</div>

          <div className="inputBlue">
            <button
              onClick={() => {
                history.push("/registration/sourcer-signup");
              }}
              className="btn getStart"
            >
              Get Started
            </button>
          </div>

          <div></div>
        </section>

        <section className="slider-sec">
          <div className="container">
            <div className="row">
              <div className="col-md-2"></div>
              <div className="col-md-8">
                <Slider {...settings}>
                  <div>
                    <img src={dumSlider} />
                    <p>
                      Lorem Ipsum is simply dummy text of the printing and
                      typesetting industry. Lorem Ipsum has been the industry's
                      standard dummy text ever since the 1500s, when an unknown
                      printer took a galley of type and scrambled
                    </p>
                    <div className="featureHead">John Smith</div>
                    <div>Art director</div>
                  </div>
                  <div>
                    <img src={dumSlider} />
                    <p>
                      Lorem Ipsum is simply dummy text of the printing and
                      typesetting industry. Lorem Ipsum has been the industry's
                      standard dummy text ever since the 1500s, when an unknown
                      printer took a galley of type and scrambled
                    </p>
                    <div className="featureHead">John Smith</div>
                    <div>Art director</div>
                  </div>
                </Slider>
              </div>
              <div className="col-md-2"></div>
            </div>
          </div>
        </section>
      </div>
      <AppFooter />
    </React.Fragment>
  );
}
