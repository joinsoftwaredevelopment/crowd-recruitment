import DataTable, { createTheme } from "react-data-table-component";
import React, { useEffect, useState } from "react";
import { AppHeader } from "../../components/appHeader";
import JobService from "../../services/JobService";
import { IJobCandidate } from "../../models/IJobCandidate";
import { history } from "../../helpers/history";

import DatePicker from "react-datepicker";

import "react-datepicker/dist/react-datepicker.css";

import {
  createStyles,
  Theme,
  withStyles,
  WithStyles,
} from "@material-ui/core/styles";
import empIco from "../../../src/assets/emp-ico.png";
import { Link, useHistory } from "react-router-dom";

import Switch from "@material-ui/core/Switch";

import Board from "react-trello";

import { useDropzone } from "react-dropzone";
import { hiringStagesConstants } from "../../constants/hiringStagesConstants";
import { IUpdateCandidateCvStatusModel } from "../../models/IUpdateCandidateCvStatusModel";
import { IUpdateHiringStageModel } from "../../models/IUpdateHiringStageModel";

import { IUpdateIsHiredModel } from "../../models/IUpdateIsHiredModel";

import { IUpdateIsAcceptOfferModel } from "../../models/IUpdateIsAcceptOfferModel";

import { IInterviewDetailsModel } from "../../models/IInterviewDetailsModel";

import { IInterviewsModel } from "../../models/IInterviewsModel";
import { IOfferModel } from "../../models/IOfferModel";

import Draft, {
  htmlToDraft,
  EmptyState,
  rawToDraft,
  draftToRaw,
  draftStateToHTML,
} from "react-wysiwyg-typescript";
import { stateToHTML } from "draft-js-export-html";

import { EditorState, convertToRaw, ContentState } from "draft-js";

import draftToHtml from "draftjs-to-html";

import IconButton from "@material-ui/core/IconButton";
import CloseIcon from "@material-ui/icons/Close";
import Typography from "@material-ui/core/Typography";

import Dialog from "@material-ui/core/Dialog";
import MuiDialogTitle from "@material-ui/core/DialogTitle";
import MuiDialogContent from "@material-ui/core/DialogContent";
import MuiDialogActions from "@material-ui/core/DialogActions";
import CandidateService from "../../services/CandidateService";
import { time } from "console";
import AdminLayout from "../../layouts/AdminLayout";
import axios from "axios";
import { Dispatch } from "redux";
import { alertActions } from "../../actions/alertActions";
import { useDispatch } from "react-redux";

const styles = (theme: Theme) =>
  createStyles({
    root: {
      margin: 0,
      padding: theme.spacing(2),
    },
    closeButton: {
      position: "absolute",
      right: theme.spacing(1),
      top: theme.spacing(1),
      color: theme.palette.grey[500],
    },
  });
export interface DialogTitleProps extends WithStyles<typeof styles> {
  id: string;
  children: React.ReactNode;
  onClose: () => void;
}

const DialogTitle = withStyles(styles)((props: DialogTitleProps) => {
  const { children, classes, onClose, ...other } = props;
  return (
    <MuiDialogTitle disableTypography className={classes.root} {...other}>
      <Typography variant="h6">{children}</Typography>
      {onClose ? (
        <IconButton
          aria-label="close"
          className={classes.closeButton}
          onClick={onClose}
        >
          <CloseIcon />
        </IconButton>
      ) : null}
    </MuiDialogTitle>
  );
});

const DialogContent = withStyles((theme: Theme) => ({
  root: {
    padding: theme.spacing(2),
  },
}))(MuiDialogContent);

const DialogActions = withStyles((theme: Theme) => ({
  root: {
    margin: 0,
    padding: theme.spacing(1),
  },
}))(MuiDialogActions);

export default function JobBoard(props: any) {
  const card = () => {
    // alert('HURRAH')
  };

  const initialAddInterviewDetails: IInterviewDetailsModel = {
    submissionUUid: "",
    interviewerId: 1,
    interviewComment: "",
    interviewTypeId: 1,
    interviewDate: new Date(),
    interviewTime: "12:00",
    duration: 1,
    templateId: 0,
    emailSubject: "",
    emailDescription: "",
    interviewerTypeId: 1,
    id: 0,
  };

  const initialOffer: IOfferModel = {
    submissionUuid: "",
    offerSubject: "",
    offerDescription: "",
    offerFile: null,
  };

  const [addInterviewDetails, setAddInterviewDetails] = useState(
    initialAddInterviewDetails
  );

  const [offer, setOffer] = useState(initialOffer);
  //const [hideMeetingSection, setHideMeetingSection] = useState("none");
  //const [meetingButtonText , setMeetingButtonText] = useState("Show meetings");

  const [showMeetingText, setShowMeetingText] = useState(true);

  const [interviewCommentError, setInterviewCommentError] = useState(false);

  const initialTitle = [
    {
      id: 0,
      emailTitleEn: "",
      emailtitleAr: "",
      emailbodyEn: "",
      emailbodyAr: "",
    },
  ];

  const [templates, setTemplates] = React.useState(initialTitle);
  const [offerTemplates, setOfferTemplates] = React.useState(initialTitle);
  const [value, setValue] = React.useState();
  const [descriptionError, setDescriptionError] = useState(false);
  const [offerDescriptionError, setOfferDescriptionError] = useState(false);
  const [fileError, setFileError] = useState(false);
  const [offerSubjectError, setOfferSubjectError] = useState(false);
  const [titleError, setTitleError] = useState(false);
  var dateFormat = require("dateformat");

  const [uploaded, setUploaded] = useState(false);

  const { acceptedFiles, getRootProps, getInputProps } = useDropzone({
    accept: ".pdf",
  });

  const files = acceptedFiles.map((file: any) => (
    <li key={file.path}>
      {file.path} - {file.size} bytes
    </li>
  ));

  function handleChange(e: any) {
    const { name, value } = e.target;
    setFileError(false);
  }

  function handleShowMeeting(submissionUuid: string) {
    if (showMeetingText == true) {
      document.getElementById(submissionUuid).style.display = "none";
      document.getElementById("txt" + submissionUuid).innerText =
        "Show interviews";
      //setHideMeetingSection("none");
      //setMeetingButtonText("Show meetings");
    } else {
      document.getElementById(submissionUuid).style.display = "inline";
      document.getElementById("txt" + submissionUuid).innerText =
        "Hide interviews";
      //setHideMeetingSection("inline");
      //setMeetingButtonText("Hide meetings");
    
    
    }

    setShowMeetingText(!showMeetingText);
  }

  const handleSetInterviewChange = (event: any) => {
    debugger;
    const { name, value } = event.target;
    setAddInterviewDetails({ ...addInterviewDetails, [name]: value });

    if (addInterviewDetails.emailSubject == "") {
      setTitleError(true);
    }
    if (addInterviewDetails.emailSubject != "") {
      setTitleError(false);
    }
  };

  const handleOfferChange = (event: any) => {
    debugger;
    const { name, value } = event.target;
    setOffer({ ...offer, [name]: value });

    if (offer.offerSubject == "") {
      setOfferSubjectError(true);
    }
    if (offer.offerSubject != "") {
      setOfferSubjectError(false);
    }
  };
  const handleSetInterviewCommentChange = (event: any) => {
    debugger;
    const { name, value } = event.target;
    setAddInterviewDetails({ ...addInterviewDetails, [name]: value });
    if (addInterviewDetails.interviewComment == "") {
      setInterviewCommentError(true);
    }
    if (addInterviewDetails.interviewComment != "") {
      setInterviewCommentError(false);
    }
  };

  // TO SET A DEFAULT HTML
  const html = "";
  const [editorState, seteditorState] = useState(htmlToDraft(html));
  const [recuirEditorState, setRecuirEditorState] = useState(htmlToDraft(html));

  const handleDraftChange = (draftText: any) => {
    seteditorState(draftText);
    // SAVING HTML TO SERVER
    const convertedHtml = draftToHtml(
      convertToRaw(editorState.getCurrentContent())
    );

    addInterviewDetails.emailDescription = convertedHtml;
    setAddInterviewDetails(addInterviewDetails);

    if (addInterviewDetails.emailDescription == "") {
      setDescriptionError(true);
    }
    if (addInterviewDetails.emailDescription != "") {
      setDescriptionError(false);
    }
    // const html = draftToHtml(draftText)
  };

  const handleRecuirDraftChange = (draftText: any) => {
    setRecuirEditorState(draftText);
    // SAVING HTML TO SERVER
    const convertedReqireHtml = draftToHtml(
      convertToRaw(recuirEditorState.getCurrentContent())
    );
    // const html = draftToHtml(draftText)
    offer.offerDescription = convertedReqireHtml;
    setOffer(offer);

    if (offer.offerDescription == "") {
      setOfferDescriptionError(true);
    }
    if (offer.offerDescription != "") {
      setOfferDescriptionError(false);
    }
  };

  const [openSure, setOpenSure] = React.useState(false);
  const [openOffer, setOpenOffer] = React.useState(false);
  const [modalOption, setmodalOption] = React.useState(1);

  const handleOpen = (args: any, submissionUuId: string) => {
    debugger;
    if (args == "hr") {
      // addInterviewDetails.interviewerTypeId = 1;
      // setAddInterviewDetails(addInterviewDetails);
      setOpenSure(true);
    }

    if (args == "tech") {
      addInterviewDetails.submissionUUid = submissionUuId;
      addInterviewDetails.interviewerTypeId = 1;
      setAddInterviewDetails(addInterviewDetails);
      setOpenSure(true);
    }
    if (args == "offer") {
      addInterviewDetails.submissionUUid = submissionUuId;
      setOpenOffer(true);
    }
  };

  const handleClose = () => {
    setOpenSure(false);
    setOpenOffer(false);
  };

  const modalOptionChange = (args: any) => {
    setmodalOption(args);
  };

  const [count, setCount] = useState("1");

  const handleChangeSwitch = (event: React.ChangeEvent<HTMLInputElement>) => {
    console.log(event.target.name, event.target.checked);
  };

  const handleChangeCvApprovalSwitch = (
    event: React.ChangeEvent<HTMLInputElement>
  ) => {
    debugger;
    console.log(event.target.name, event.target.checked);

    const acceptModel: IUpdateCandidateCvStatusModel = {
      isCvApproved: event.target.checked,
      submissionUuid: event.target.name,
    };

    CandidateService.UpdateCandidateCvStatus(acceptModel)
      .then((response) => {
        getAllJobCandidates();
      })
      .catch((e) => {
        console.log(e);
      });
  };

  const saveInterviewDetails = () => {
    debugger;

    if (
      addInterviewDetails.interviewComment == "" ||
      addInterviewDetails.emailSubject == "" ||
      addInterviewDetails.emailDescription == "" ||
      addInterviewDetails.emailDescription.includes("<p></p>") == true
    ) {
      if (addInterviewDetails.emailSubject == "") {
        setTitleError(true);
        modalOptionChange(2);
      }

      if (addInterviewDetails.emailSubject != "") {
        setTitleError(false);
      }

      if (addInterviewDetails.emailDescription == "") {
        setDescriptionError(true);
        modalOptionChange(2);
      }

      if (addInterviewDetails.emailDescription.includes("<p></p>") == true) {
        setDescriptionError(true);
        modalOptionChange(2);
      }

      if (
        addInterviewDetails.emailDescription != "" &&
        addInterviewDetails.emailDescription.includes("<p></p>") == false
      ) {
        setDescriptionError(false);
      }

      if (addInterviewDetails.interviewComment == "") {
        setInterviewCommentError(true);
        modalOptionChange(1);
      }

      if (addInterviewDetails.interviewComment != "") {
        setInterviewCommentError(false);
      }
    } else {
      addInterviewDetails.interviewTypeId = Number(count);
      addInterviewDetails.interviewDate = startDate;
      setInterviewCommentError(false);
      CandidateService.AddInterviewDetails(addInterviewDetails)
        .then((response) => {
          debugger;
          getAllJobCandidates();
          getAllInterviews();
          getAllTemplates();
          getAllOfferTemplates();
          handleClose();
          setAddInterviewDetails(initialAddInterviewDetails);
          seteditorState(htmlToDraft(""));
        })
        .catch((e) => {
          getAllInterviews();
          handleClose();
          setAddInterviewDetails(initialAddInterviewDetails);
          seteditorState(htmlToDraft(""));
        });
    }
  };

  const AddOffer = () => {
    debugger;
    if (
      offer.offerSubject == "" ||
      offer.offerDescription == "" ||
      acceptedFiles[0] == null
    ) {
      if (offer.offerSubject == "") {
        setOfferSubjectError(true);
      }

      if (offer.offerSubject != "") {
        setOfferSubjectError(false);
      }

      if (offer.offerDescription == "") {
        setOfferDescriptionError(true);
      }

      if (offer.offerDescription != "") {
        setOfferDescriptionError(false);
      }

      if (acceptedFiles[0] == null) {
        setFileError(true);
      }

      if (acceptedFiles[0] != null) {
        setFileError(false);
      }
    } else {
      offer.submissionUuid = addInterviewDetails.submissionUUid;
      // Validate then Assign file
      setOffer(offer);

      const formData = new FormData();
      formData.append("offerFile", acceptedFiles[0]);
      formData.append("submissionUuid", offer.submissionUuid);
      formData.append("offerSubject", offer.offerSubject);
      formData.append("offerDescription", offer.offerDescription);
      CandidateService.SendOffer(formData);

      handleClose();
    }
  };

  const DeleteMeeting = (interviewId: number, isCanceled: boolean) => {
    CandidateService.CancelInterview(interviewId, isCanceled)
      .then((response) => {
        debugger;
        //window.location.reload();
        // getAllJobCandidates();
        getAllInterviews();
        handleClose();
      })
      .catch((e) => {
        //window.location.reload();
        // getAllJobCandidates();
        getAllInterviews();
      });
  };

  const GetMeetingDetails = (interviewId: number, submissionUuId: string) => {
    CandidateService.GetMeetingDetails(interviewId)
      .then((response) => {
        debugger;
        addInterviewDetails.id = response.data.data.id;
        // addInterviewDetails.interviewComment = response.data.data.interviewComment
        setCount(response.data.data.interviewTypeId);
        addInterviewDetails.submissionUUid = submissionUuId;
        setAddInterviewDetails(addInterviewDetails);
        setAddInterviewDetails(response.data.data);
        seteditorState(htmlToDraft(response.data.data.emailDescription));
        handleOpen("hr", submissionUuId);
      })
      .catch((e) => {
        console.log(e);
      });
  };

  const handleChangeIsHired = (event: React.ChangeEvent<HTMLInputElement>) => {
    debugger;
    console.log(event.target.name, event.target.checked);

    const IsHiredModel: IUpdateIsHiredModel = {
      isHired: event.target.checked,
      submissionUuid: event.target.name,
    };

    CandidateService.UpdateIsHired(IsHiredModel)
      .then((response) => {
        getAllJobCandidates();
      })
      .catch((e) => {
        console.log(e);
      });
  };

  const handleChangeIsAcceptOffer = (
    event: React.ChangeEvent<HTMLInputElement>
  ) => {
    debugger;
    console.log(event.target.name, event.target.checked);

    const IsAcceptOfferModel: IUpdateIsAcceptOfferModel = {
      isOfferAccepted: event.target.checked,
      submissionUuid: event.target.name,
    };

    CandidateService.UpdateIsAcceptOffer(IsAcceptOfferModel)
      .then((response) => {
        getAllJobCandidates();
      })
      .catch((e) => {
        console.log(e);
      });
  };

  const dispatch = useDispatch();
  const [startDate, setStartDate] = useState(new Date());

  const handleDragEnd = (
    cardId: any,
    sourceLaneId: any,
    targetLaneId: any,
    position: any,
    cardDetails: any
  ) => {
    debugger;
    console.log("cardId", cardId);
    if (
      sourceLaneId == hiringStagesConstants.Approved_By_Employer &&
      targetLaneId == hiringStagesConstants.Approved
    ) {
      return false;
    }
    if (
      sourceLaneId == hiringStagesConstants.Interviews &&
      targetLaneId == hiringStagesConstants.Approved_By_Employer
    ) {
      return false;
    }
    if (
      sourceLaneId == hiringStagesConstants.Offer &&
      targetLaneId == hiringStagesConstants.Interviews
    ) {
      return false;
    }
    if (
      sourceLaneId == hiringStagesConstants.Hiring &&
      targetLaneId == hiringStagesConstants.Offer
    ) {
      return false;
    }
    if (
      sourceLaneId == hiringStagesConstants.Hiring &&
      targetLaneId == hiringStagesConstants.Interviews
    ) {
      return false;
    }
    if (
      sourceLaneId == hiringStagesConstants.Hiring &&
      targetLaneId == hiringStagesConstants.Approved_By_Employer
    ) {
      return false;
    }
    if (
      sourceLaneId == hiringStagesConstants.Hiring &&
      targetLaneId == hiringStagesConstants.Approved
    ) {
      return false;
    }
    if (
      sourceLaneId == hiringStagesConstants.Offer &&
      targetLaneId == hiringStagesConstants.Approved_By_Employer
    ) {
      return false;
    }
    if (
      sourceLaneId == hiringStagesConstants.Offer &&
      targetLaneId == hiringStagesConstants.Approved
    ) {
      return false;
    }
    if (
      sourceLaneId == hiringStagesConstants.Approved_By_Employer &&
      targetLaneId == hiringStagesConstants.Offer
    ) {
      return false;
    }
    if (
      sourceLaneId == hiringStagesConstants.Approved_By_Employer &&
      targetLaneId == hiringStagesConstants.Hiring
    ) {
      return false;
    }
    if (
      sourceLaneId == hiringStagesConstants.Interviews &&
      targetLaneId == hiringStagesConstants.Approved
    ) {
      return false;
    }
    if (
      sourceLaneId == hiringStagesConstants.Interviews &&
      targetLaneId == hiringStagesConstants.Hiring
    ) {
      return false;
    }
    if (
      sourceLaneId == hiringStagesConstants.Approved &&
      targetLaneId == hiringStagesConstants.Interviews
    ) {
      return false;
    }
    if (
      sourceLaneId == hiringStagesConstants.Approved &&
      targetLaneId == hiringStagesConstants.Offer
    ) {
      return false;
    }
    if (
      sourceLaneId == hiringStagesConstants.Approved &&
      targetLaneId == hiringStagesConstants.Hiring
    ) {
      return false;
    }

    debugger;
    const currentCardDetails = allJobCandidates.find(
      (m) => m.submissionUuId == cardId
    );

    //alert(currentCardDetails.jobStatusId)

    if (sourceLaneId == targetLaneId) {
      return false;
    }

    if (
      sourceLaneId == hiringStagesConstants.Approved_By_Employer &&
      currentCardDetails.isCvApproved != true
    ) {
      dispatch(
        alertActions.error("Approve Cv to enable dragging to next stage")
      );
      return false;
    }

    if (
      sourceLaneId == hiringStagesConstants.Offer &&
      currentCardDetails.isAcceptOffer != true
    ) {
      dispatch(
        alertActions.error(
          "Offer should be accepted to enable dragging to next stage"
        )
      );
      return false;
    }

    if (
      sourceLaneId == hiringStagesConstants.Interviews &&
      currentCardDetails.hasInterview <= 0
    ) {
      dispatch(
        alertActions.error(
          "Candidate should have at least one interview to enable dragging to next stage"
        )
      );
      return false;
    }

    addInterviewDetails.submissionUUid = cardId;
    setAddInterviewDetails(addInterviewDetails);

    const updateStageModel: IUpdateHiringStageModel = {
      submissionUuid: cardId,
      hiringStageId: targetLaneId,
    };

    CandidateService.UpdateHiringStageStatus(updateStageModel)
      .then((response) => {
        getAllJobCandidates();
      })
      .catch((e) => {
        console.log(e);
      });
  };

  const [allJobCandidates, setAllJobCandidates] = useState<IJobCandidate[]>([]);
  const [currentBoardClass, setCurrentBoardClass] = useState("");
  const [jobTitle, setJobTitle] = useState("");

  const [allInterviews, setAllInterviews] = useState<IInterviewsModel[]>([]);

  useEffect(() => {
    getAllJobCandidates();
    getAllInterviews();
    getAllTemplates();
    getAllOfferTemplates();
  }, []);

  const getAllJobCandidates = () => {
    debugger;
    JobService.getAcceptedJobCandidates(props.match.params.id)
      .then((response) => {
        debugger;
        setAllJobCandidates(response.data.data);
        setJobTitle(response.data.data[0].jobTitle);
        if (response.data.data[0].jobStatusId == 5) {
          setCurrentBoardClass("closed");
        }
      })
      .catch((e) => {
        console.log(e);
      });
  };

  const getAllInterviews = () => {
    debugger;
    CandidateService.GetAllInterviews()
      .then((response) => {
        debugger;
        setAllInterviews(response.data.data);
      })
      .catch((e) => {
        console.log(e);
      });
  };

  const getAllTemplates = () => {
    debugger;
    CandidateService.GetAllTemplates()
      .then((response) => {
        setTemplates(response.data.data);
      })
      .catch((e) => {
        console.log(e);
      });
  };

  const getAllOfferTemplates = () => {
    debugger;
    CandidateService.GetAllOfferTemplates()
      .then((response) => {
        setOfferTemplates(response.data.data);
      })
      .catch((e) => {
        console.log(e);
      });
  };

  const handleTemplateChange = (event: React.ChangeEvent<any>) => {
    const { name, value } = event.target;
    setValue({ ...value, [name]: value });
    debugger;
    if (value != "0") {
      const currentTemplate = templates.find((m) => m.id == value);
      addInterviewDetails.emailSubject = currentTemplate.emailTitleEn;
      addInterviewDetails.emailDescription = currentTemplate.emailbodyEn;
      addInterviewDetails.templateId = currentTemplate.id;
      seteditorState(htmlToDraft(currentTemplate.emailbodyEn));
    } else {
      addInterviewDetails.emailSubject = "";
      addInterviewDetails.emailDescription = "";
      seteditorState(htmlToDraft(""));
      addInterviewDetails.templateId = 0;
    }

    setAddInterviewDetails(addInterviewDetails);

    if (
      addInterviewDetails.emailSubject == "" ||
      addInterviewDetails.emailDescription == ""
    ) {
      if (addInterviewDetails.emailSubject == "") {
        setTitleError(true);
      }
      if (addInterviewDetails.emailSubject != "") {
        setTitleError(false);
      }
      if (addInterviewDetails.emailDescription == "") {
        setDescriptionError(true);
      }
      if (addInterviewDetails.emailDescription != "") {
        setDescriptionError(false);
      }
    } else {
      setTitleError(false);
      setDescriptionError(false);
    }
  };

  const handleOfferTemplateChange = (event: React.ChangeEvent<any>) => {
    const { name, value } = event.target;
    setValue({ ...value, [name]: value });
    debugger;
    if (value != "0") {
      const currentOfferTemplate = offerTemplates.find((m) => m.id == value);
      offer.offerSubject = currentOfferTemplate.emailTitleEn;
      offer.offerDescription = currentOfferTemplate.emailbodyEn;
      setRecuirEditorState(htmlToDraft(currentOfferTemplate.emailbodyEn));
    } else {
      offer.offerSubject = "";
      offer.offerDescription = "";
      setRecuirEditorState(htmlToDraft(""));
    }

    setOffer(offer);

    if (offer.offerSubject == "" || offer.offerDescription == "") {
      if (offer.offerSubject == "") {
        setOfferSubjectError(true);
      }
      if (offer.offerSubject != "") {
        setOfferSubjectError(false);
      }
      if (offer.offerDescription == "") {
        setOfferDescriptionError(true);
      }
      if (addInterviewDetails.emailDescription != "") {
        setOfferDescriptionError(false);
      }
    } else {
      setOfferSubjectError(false);
      setOfferDescriptionError(false);
    }
  };

  const cardsSourcing: any = [];

  allJobCandidates
    .filter(
      (m) => m.candidateSubmissionStatusId == hiringStagesConstants.Approved
    )
    .map((job: IJobCandidate) =>
      cardsSourcing.push({
        id: job.submissionUuId,
        title: "",
        description: (
          <div className="job-box boardBox">
            <div className="topNavBoard">
              <div className="pull-left data-card-light">
                <i className="fa fa-calendar"></i>
                {job.jobDate}
              </div>
              {/* <div className='pull-right data-card-light'><i className='fa fa-clock'></i>{job.jobDate}</div> */}
            </div>

            <div className="remain-img-box">
              <img src={empIco} />
            </div>
            <div onClick={card} className="data-card-head">
              {job.candidateName}
            </div>
            <Link
              to={"/candidate/summary/" + job.submissionUuId}
              className="detail-link"
            >
              Show Details
            </Link>
            <div className="cardStatus">
              <ul>
                <span className="liS-blue">
                  <span className="cricle"></span>
                  Sourcing
                </span>
              </ul>
            </div>
          </div>
        ),
        draggable: true,
      })
    );

  const cardsApproval: any = [];

  allJobCandidates
    .filter(
      (m) =>
        m.candidateSubmissionStatusId ==
        hiringStagesConstants.Approved_By_Employer
    )
    .map((job: IJobCandidate) =>
      cardsApproval.push({
        id: job.submissionUuId,
        title: "",
        description: (
          <div className="job-box boardBox">
            <div className="topNavBoard">
              <div className="pull-left data-card-light">
                <i className="fa fa-calendar"></i>
                <div className="pull-right data-card-light">{job.jobDate}</div>
              </div>
              {/* <div className='pull-right data-card-light'><i className='fa fa-clock'></i> 12:20 PM</div> */}
            </div>

            <div className="remain-img-box">
              <img src={empIco} />
            </div>
            <div onClick={card} className="data-card-head">
              {job.candidateName}
            </div>
            <Link
              to={"/candidate/summary/" + job.submissionUuId}
              className="detail-link"
            >
              Show Details
            </Link>
            <div className="cardStatus">
              <ul className="pull-left">
                {job.isCvApproved == false ? (
                  <span className="liS-red">
                    <span className="cricle"></span>CV Declined
                  </span>
                ) : job.isCvApproved == true ? (
                  <span className="liS-green">
                    <span className="cricle"></span>CV Approved
                  </span>
                ) : (
                  ""
                )}
              </ul>
              <div className="pull-right">
                <Switch
                  onChange={handleChangeCvApprovalSwitch}
                  color="primary"
                  name={job.submissionUuId}
                  inputProps={{ "aria-label": "primary checkbox" }}
                  checked={job.isCvApproved == true ? true : false}
                />
              </div>
            </div>
          </div>
        ),
      })
    );

  const cardsInterviews: any = [
    
  ];

  allJobCandidates
    .filter(
      (m) => m.candidateSubmissionStatusId == hiringStagesConstants.Interviews
    )
    .map((job: IJobCandidate) =>
      cardsInterviews.push({
        id: job.submissionUuId,
        title: "",
        description: (
          <div className="job-box boardBox">
            <div className="topNavBoard">
              <div className="pull-left data-card-light">
                <i className="fa fa-calendar"></i>
                {job.jobDate}
              </div>
              {/* <div className='pull-right data-card-light'><i className='fa fa-clock'></i> 12:20 PM</div> */}
            </div>

            <div className="remain-img-box">
              <img src={empIco} />
            </div>
            <div onClick={card} className="data-card-head">
              {job.candidateName}
            </div>
            <Link
              to={"/candidate/summary/" + job.submissionUuId}
              className="detail-link"
            >
              Show Details
            </Link>
            <div className="cardStatus">
              <ul>
                <div className="detail-link-btn">
                  <span onClick={() => handleOpen("tech", job.submissionUuId)}>
                    Add interview
                  </span>
                  <span
                    id={"txt" + job.submissionUuId}
                    style={{ float: "right", color: "#a5adb2" }}
                    onClick={() => handleShowMeeting(job.submissionUuId)}
                  >
                    Show interviews
                  </span>
                </div>

                <div id={job.submissionUuId} style={{ display: "none" }}>
                  {allInterviews
                    .filter(
                      (interview) =>
                        interview.submissionUuid == job.submissionUuId &&
                        interview.jobId == job.jobId
                    )
                    .map((item: IInterviewsModel) => (
                      <div>
                        <div className="liS-gray">
                          <span>
                            <span className="cricle"></span>
                            Meeting at
                          </span>
                        </div>
                        <div className="data-card-light font-13">
                          {dateFormat(item.interviewDate, "d mmmm, yyyy")} ,{" "}
                          {item.interviewTime}
                        </div>

                        {item.isEmailSent == true &&
                        item.isCanceled == false ? (
                          <div className="pull-right">
                            <span
                              className="red"
                              onClick={() => DeleteMeeting(item.id, true)}
                            >
                              Delete
                            </span>
                            <span
                              className="green"
                              onClick={() =>
                                GetMeetingDetails(item.id, item.submissionUuid)
                              }
                            >
                              Edit
                            </span>
                          </div>
                        ) : (
                          <span className="red">Interview is canceled</span>
                        )}

                        <hr />
                      </div>
                    ))}
                </div>
              </ul>
            </div>
          </div>
        ),
      })
    );

  const cardsOffering: any = [];
  allJobCandidates
    .filter((m) => m.candidateSubmissionStatusId == hiringStagesConstants.Offer)
    .map((job: IJobCandidate) =>
      cardsOffering.push({
        id: job.submissionUuId,
        title: "",
        description: (
          <div className="job-box boardBox">
            <div className="topNavBoard">
              <div className="pull-left data-card-light">
                <i className="fa fa-calendar"></i>
                {job.jobDate}
              </div>
              {/* <div className='pull-right data-card-light'><i className='fa fa-clock'></i> 12:20 PM</div> */}
            </div>

            <div className="remain-img-box">
              <img src={empIco} />
            </div>
            <div onClick={card} className="data-card-head">
              {job.candidateName}
            </div>
            <Link
              to={"/candidate/summary/" + job.submissionUuId}
              className="detail-link"
            >
              Show Details
            </Link>
            <div className="cardStatus">
              <ul className="pull-left">
                <div className="detail-link-btn">Is Offer Accepted ?</div>
                <div
                  onClick={() => handleOpen("offer", job.submissionUuId)}
                  className="detail-link-btn"
                >
                  Set an offer
                </div>
              </ul>

              <div className="pull-right">
                <Switch
                  onChange={handleChangeIsAcceptOffer}
                  color="primary"
                  name={job.submissionUuId}
                  inputProps={{ "aria-label": "primary checkbox" }}
                  checked={job.isAcceptOffer == true ? true : false}
                />
              </div>
            </div>
          </div>
        ),
      })
    );

  const cardsHiring: any = [];

  allJobCandidates
    .filter(
      (m) => m.candidateSubmissionStatusId == hiringStagesConstants.Hiring
    )
    .map((job: IJobCandidate) =>
      cardsHiring.push({
        id: job.submissionUuId,
        title: "",
        description: (
          <div className="job-box boardBox">
            <div className="topNavBoard">
              <div className="pull-left data-card-light">
                <i className="fa fa-calendar"></i>
                {job.jobDate}
              </div>
              {/* <div className='pull-right data-card-light'><i className='fa fa-clock'></i> 12:20 PM</div> */}
            </div>

            <div className="remain-img-box">
              <img src={empIco} />
            </div>
            <div onClick={card} className="data-card-head">
              {job.candidateName}
            </div>
            <Link
              to={"/candidate/summary/" + job.submissionUuId}
              className="detail-link"
            >
              Show Details
            </Link>
            <div className="cardStatus">
              <ul className="pull-left">
                {/* <span className='liS-red'>
    <span className='cricle'></span>
    Rejected</span> */}
                {job.isHired == false ? (
                  <span className="liS-red">
                    <span className="cricle"></span> Rejected
                  </span>
                ) : job.isHired == true ? (
                  <span className="liS-green">
                    <span className="cricle"></span>Hired
                  </span>
                ) : (
                  ""
                )}
              </ul>

              <div className="pull-right">
                <Switch
                  onChange={handleChangeIsHired}
                  color="primary"
                  name={job.submissionUuId}
                  inputProps={{ "aria-label": "primary checkbox" }}
                  checked={job.isHired == true ? true : false}
                />
              </div>
            </div>
          </div>
        ),
      })
    );

  const data = {
    lanes: [
      {
        id: hiringStagesConstants.Approved,
        title: "Sourcing",
        cards: cardsSourcing,
      },
      {
        id: hiringStagesConstants.Approved_By_Employer,
        title: "Cv Approval",
        cards: cardsApproval,
        currentPage: 1,
      },
      {
        id: hiringStagesConstants.Interviews,
        title: "Interviews",
        cards: cardsInterviews,
      },
      {
        id: hiringStagesConstants.Offer,
        title: "Offering",
        cards: cardsOffering,
      },
      {
        id: hiringStagesConstants.Hiring,
        title: "Hiring",
        cards: cardsHiring,
      },
    ],
  };

  return (
    <React.Fragment>
      <div className="layout-home">
        <AppHeader />

        <div className="boardContent">
          <div className="" style={{ padding: "20px 0" }}>
            <div style={{ paddingLeft: "15px" }}>
              <div className="bread-text" style={{ paddingBottom: "10px" }}>
                <span className="lt-text">Candidates</span>
                <span className="lt-text">
                  <i className="fas fa-angle-right"></i>
                </span>
                <span className="lt-head">Hiring Process</span>
              </div>

              <div className="bold-head">
                {jobTitle}
                {currentBoardClass == "closed" ? " ( Job is closed )" : ""}
              </div>
            </div>
          </div>

          <div>
            <Board
              data={data}
              className={currentBoardClass}
              handleDragEnd={handleDragEnd}
              hideCardDeleteIcon={true}
            />
          </div>
        </div>

        <Dialog
          fullWidth={true}
          maxWidth={"sm"}
          onClose={handleClose}
          aria-labelledby="customized-dialog-title"
          open={openSure}
        >
          <DialogTitle id="customized-dialog-title2" onClose={handleClose}>
            Setting Interview
          </DialogTitle>

          <DialogContent className="text-center" dividers>
            <div className="modalFilter">
              <span
                className={
                  modalOption == 1 ? "modalOption activeModal" : "modalOption"
                }
                onClick={() => modalOptionChange(1)}
              >
                Interview Details
              </span>
              <span
                className={
                  modalOption == 2 ? "modalOption activeModal" : "modalOption"
                }
                onClick={() => modalOptionChange(2)}
              >
                Interview Email
              </span>
            </div>

            {modalOption == 1 && (
              <div className="form row">
                <div className="col-md-6">
                  <div className="form-group">
                    <label>Interviewer Name</label>
                    <select
                      onChange={handleSetInterviewChange}
                      value={addInterviewDetails.interviewerId}
                      className="form-control"
                      name="interviewerId"
                      id="interviewerId"
                    >
                      <option value="1">Basheer</option>

                      <option value="2">Jomana</option>

                      <option value="3">Sara</option>
                      <option value="4">Laura</option>
                    </select>
                  
                  </div>

                  {/* <div className='form-group'>
          <label>Interviewer Name</label>
          <input className='form-control'/>
          </div> */}

                  <div className="form-group">
                    <label>Interview Comment</label>
                    <textarea
                      name="interviewComment"
                      id="interviewComment"
                      value={addInterviewDetails.interviewComment}
                      style={{ height: 170 }}
                      className="form-control"
                      onChange={handleSetInterviewCommentChange}
                    />
                    {interviewCommentError && (
                      <div className="invalid-feedback">
                        Interview comment is required
                      </div>
                    )}{" "}
                  </div>
                </div>

                <div className="col-md-6 modalCheck">
                  <div className="form-group">
                    <div>
                      <div
                        className={count == "1" ? "activeCheck check" : "check"}
                        id=""
                        onClick={() => setCount("1")}
                      >
                        <i className="far fa-circle"></i>
                        Telephone
                      </div>

                      <div
                        className={count == "2" ? "activeCheck check" : "check"}
                        id=""
                        onClick={() => setCount("2")}
                      >
                        <i className="far fa-circle"></i>
                        On site
                      </div>

                      <div
                        className={count == "3" ? "activeCheck check" : "check"}
                        id=""
                        onClick={() => setCount("3")}
                      >
                        <i className="far fa-circle"></i>
                        Web Conference
                      </div>
                    </div>
                  </div>

                  <div className="form-group">
                    <label>Interview Date</label>
                    <div>
                      <DatePicker
                        className="form-control w-100"
                        selected={startDate}
                        onChange={(date: any) => setStartDate(date)}
                      />
                    </div>
                  </div>

                  <div className="row">
                    <div className="form-group col-md-6">
                      <label>Time</label>
                      <input
                        onChange={handleSetInterviewChange}
                        type="time"
                        className="form-control"
                        value={addInterviewDetails.interviewTime}
                        name="interviewTime"
                        id="interviewTime"
                      />
                    </div>

                    <div className="form-group col-md-6">
                      <label>Duration</label>
                      <select
                        onChange={handleSetInterviewChange}
                        value={addInterviewDetails.duration}
                        className="form-control"
                        name="duration"
                        id="duration"
                      >
                        <option value="1">30 minutes</option>

                        <option value="2">1 hour</option>

                        <option value="3">2 hours</option>
                      </select>
                    </div>
                  </div>
                </div>
              </div>
            )}

            {modalOption == 2 && (
              <div className="form row">
                <div className="form-group col-md-6">
                  <label>Choose Template</label>
                  <select
                    value={addInterviewDetails.templateId}
                    className="form-control"
                    onChange={handleTemplateChange}
                  >
                    <option key="0" value="0">
                      {"You can choose from our templates"}
                    </option>

                    {templates.map((item) => (
                      <option key={item.id} value={item.id}>
                        {item.emailTitleEn}
                      </option>
                    ))}
                  </select>
                
                </div>

                <div className="form-group col-md-6">
                  <label>Email Subject</label>
                  <input
                    className="form-control"
                    name="emailSubject"
                    id="emailSubject"
                    value={addInterviewDetails.emailSubject}
                    type="text"
                    onChange={handleSetInterviewChange}
                  />
                  {titleError && (
                    <div className="invalid-feedback">
                      Email subject is required
                    </div>
                  )}{" "}
                </div>

                <div className="form-group col-md-12">
                  <label>Email description</label>
                  <Draft
                    editorState={editorState}
                    name="emailDescription"
                    id="emailDescription"
                    className="form-control"
                    onEditorStateChange={(editorState) =>
                      handleDraftChange(editorState)
                    }
                  />
                  {descriptionError && (
                    <div className="invalid-feedback">
                      Email description is required
                    </div>
                  )}{" "}
                </div>
              </div>
            )}

            <div className="text-center job-pop-btns">
              <button onClick={handleClose} className="btn btn-decline">
                Cancel
              </button>
              <button
                type="button"
                onClick={saveInterviewDetails}
                className="btn btn-approve"
              >
                Confirm
              </button>
            </div>
          </DialogContent>
        </Dialog>

        <Dialog
          fullWidth={true}
          maxWidth={"sm"}
          onClose={handleClose}
          aria-labelledby="customized-dialog-title"
          open={openOffer}
        >
          <DialogTitle id="customized-dialog-title2" onClose={handleClose}>
            Make Offer
          </DialogTitle>

          <DialogContent className="text-center" dividers>
            <div className="form row">
              <div className="form-group col-md-6">
                <label>Choose Template</label>
                <select
                  value="0"
                  className="form-control"
                  onChange={handleOfferTemplateChange}
                >
                  <option key="0" value="0">
                    {"You can choose from our templates"}
                  </option>

                  {offerTemplates.map((item) => (
                    <option key={item.id} value={item.id}>
                      {item.emailTitleEn}
                    </option>
                  ))}
                </select>
              </div>

              <div className="form-group col-md-6">
                <label>Email Subject</label>
                <input
                  className="form-control"
                  name="offerSubject"
                  id="offerSubject"
                  value={offer.offerSubject}
                  type="text"
                  onChange={handleOfferChange}
                />
                {offerSubjectError && (
                  <div className="invalid-feedback">
                    Email Subject is required
                  </div>
                )}{" "}
              </div>

              <div className="form-group col-md-12">
                <label>Email description</label>
                <Draft
                  editorState={recuirEditorState}
                  name="offerDescription"
                  id="offerDescription"
                  className="form-control"
                  onEditorStateChange={(recuirEditorState) =>
                    handleRecuirDraftChange(recuirEditorState)
                  }
                />
                {offerDescriptionError && (
                  <div className="invalid-feedback">
                    Email description is required
                  </div>
                )}{" "}
              </div>
            </div>

            <div className="form-group col-md-12">
              <div
                onChange={handleChange}
                {...getRootProps({ className: "dropzone" })}
              >
                <input onChange={handleChange} {...getInputProps()} />
                <div className="file-drop" style={{ marginBottom: "15px" }}>
                  <div className="blackHead">Drag and drop</div>
                  <div>
                    Or click{" "}
                    <a className="anchor"> here to upload attachment</a>
                  </div>
                </div>
              </div>

              <br />
              <aside>
                <h4>File</h4>
                <ul>{files}</ul>
                {fileError && (
                  <div className="invalid-feedback">Offer file is required</div>
                )}{" "}
              </aside>
            </div>

            <div className="text-center job-pop-btns">
              <button onClick={handleClose} className="btn btn-decline">
                Cancel
              </button>
              <button
                type="button"
                onClick={AddOffer}
                className="btn btn-approve"
              >
                Confirm
              </button>
            </div>
          </DialogContent>
        </Dialog>
      </div>
    </React.Fragment>
  );
}
