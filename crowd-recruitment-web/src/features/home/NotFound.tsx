import React from "react";
import { createStyles, makeStyles, Theme } from "@material-ui/core/styles";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import Grid from "@material-ui/core/Grid";
import noFound from "../../../src/assets/noFound.png";
import { useHistory } from "react-router-dom";

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {},
  })
);

export default function NotFound() {
  const classes = useStyles();
  const history = useHistory();

  return (
    <React.Fragment>
      <div className="container">
        <div className="row">
          <div className="col-md-12" style={{ margin: "3em 0" }}>
            <div className="col-md-6" style={{ paddingTop: "10em" }}>
              <div className="blackHead">Page Not Found</div>
              <p className="blurText">
                The resource you are looking for might have been <br /> removed,
                had its name changed, or is <br /> temporarily unavailable
              </p>

              <button
                onClick={() => {
                  history.push("/employerHome");
                }}
                className="btn btn-approve"
              >
                Back to home
              </button>
            </div>
            <div className="col-md-6">
              <img style={{ width: "100%" }} src={noFound} />
            </div>
          </div>
        </div>
      </div>
    </React.Fragment>
  );
}
