import React, { useEffect } from 'react'
import { createStyles, makeStyles, Theme } from "@material-ui/core/styles";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import Grid from "@material-ui/core/Grid";
import shape from "../../../src/assets/shape.png";
import footShape from "../../../src/assets/footShape.png";
// import { jsPDF } from "jspdf";

declare const require: any;
const { jsPDF } = require("jspdf");
require("jspdf-autotable");

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {},
  })
);

export default function Invoice() {

    useEffect(() => {
    //    window.print();

    const input = document.getElementById("pdf-element");
    const pdf = new jsPDF({ unit: "px", format: "letter", userUnit: "px" });
    pdf.html(input, { html2canvas: { scale:0.34 } }).then(() => {
      pdf.save("test.pdf");
    });

      }, [])
  const classes = useStyles();


  return (
    <React.Fragment>
        
        <div className='invoiceParent' id='pdf-element'>
        <div className='invoiceTop'>
        <div className='row'>
        <div className='col-md-8'>
        <div className="invoiceTopText">Crowd Recruitment</div>
            <div className="invoiceTopText">King Abdullah Rd, Al Hamra, 2nd Floor, Deem Plaza</div>
            <div className="invoiceTopText">2nd Floor, Deem Plaza 6389</div>
            <div className="invoiceTopText">Riyadh 13216</div>
            <div className="invoiceTopText">Phone: +966503230508</div>
            <div className="invoiceTopText">Email: sales@assisted.vip </div>
            <div className="invoiceTopText">Riyadh 13216</div>
        </div>
        <div className='col-md-4'>
        <div className='logoCR'>CR</div>
        </div>
        </div>

        </div>

        <div className='invoiceSecond'>
        <img className='secondFoot' src={shape}/>
        <div className='row'>
        <div className='col-md-4 text-center'>
        <div>
            <div className='inHead'>Start Date</div>
            <div className='inSubHead'>12/12/11</div>
        </div>
        </div>

        <div className='col-md-4 text-center'>
        <div>
            <div className='inHead'>Edited to</div>
            <div className='inSubHead'>Testing</div>
        </div>
        </div>

        <div className='col-md-4 text-center'>
        <div>
            <div className='inHead'>Administrator</div>
            <div className='inSubHead'>Ahmed Bashir</div>
        </div>
        </div>

        </div>


        <div className='row'>
        <div className='col-md-4 text-center'>
        <div>
            <div className='inHead'>End Date</div>
            <div className='inSubHead'>12/12/11</div>
        </div>
        </div>

        <div className='col-md-4 text-center'>
        <div>
            <div className='inHead'>Total</div>
            <div className='inSubHead'>121</div>
        </div>
        </div>

        <div className='col-md-4 text-center'>
        <div>
            <div className='inHead'>Phone</div>
            <div className='inSubHead'>2323232323</div>
        </div>
        </div>

        </div>
        </div>

        <div className='invoiceTable'>
        <img className='tableFoot' src={footShape}/>
         <div className="table-responsive">
                                <table className="table table-bordered">
                                    <thead>
                                        <tr>
                                            <th>Sourcer</th>
                                            <th>Job Applied</th>
                                            <th>No of Candidates</th>
                                            <th>Approved</th>
                                            <th>Declined</th>
                                            <th>Rate</th>
                                            <th>Balance</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                        <td>Ahmed</td>
                                        <td>1</td>
                                        <td>1</td>
                                        <td>1</td>
                                        <td>1</td>
                                        <td>50%</td>
                                        <td>4.66$</td>
                                        </tr>

                                        <tr>
                                        <td>Ahmed</td>
                                        <td>1</td>
                                        <td>1</td>
                                        <td>1</td>
                                        <td>1</td>
                                        <td>50%</td>
                                        <td>4.66$</td>
                                        </tr>
                                     
                                    </tbody>

                                    <tfoot>
                                    <tr>
                                        <td>Total</td>
                                        <td>1</td>
                                        <td>1</td>
                                        <td>1</td>
                                        <td>1</td>
                                        <td>50%</td>
                                        <td>4.66$</td>
                                        </tr>
                                    </tfoot>
                                </table>

                                <div className='totalInvoice'>
                                <div className='totalRow'>
                                <span className='textIn'>SubTotal</span>
                                <span className='float-right'>0,SAR90</span>
                                </div>

                                <div className='totalRow'>
                                <span className='textIn'>Tax</span>
                                <span className='float-right'>0,SAR90</span>
                                </div>

                                <div className='totalRow blue bold'>
                                <span className='textIn'>Total</span>
                                <span className='float-right'>0,SAR90</span>
                                </div>
                                </div>
                            </div>
        </div>
        </div>
 
    </React.Fragment>
  );
}
