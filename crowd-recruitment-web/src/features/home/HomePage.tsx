import React, { useEffect, useState } from "react";

import { createStyles, makeStyles, Theme } from "@material-ui/core/styles";
import PackageService from "../../services/PackageService";


import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import arrowIcon from "../../assets/arrow-icon.png";
import split1 from "../../assets/5.png";
import split2 from "../../assets/7.png";
import split3 from "../../assets/6.png";
import split4 from "../../assets/8.png";
import circles from "../../assets/circles.png";
import card1 from "../../assets/card-1.png";
import card2 from "../../assets/card-2.png";
import card3 from "../../assets/card-3.png";
import { AppHeader } from "../../components/appHeader";
import AppFooter from "../../components/appFooter";
import dumSlider from "../../assets/dum-slider.png";
import sliderImg from "../../assets/profile.png";
import arrowRight from "../../assets/arrow-icon.png";
import arrowPrev from "../../assets/arrow-prev.png";

import Slider from "react-slick";
import "slick-carousel/slick/slick-theme.css";
import "slick-carousel/slick/slick.css";

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {},
  })
);

export default function HomePage() {
  const classes = useStyles();
  const initialPackageList = [
    {
      uuid: "",
      text: "",
      packageId: 0
    },
  ];
  const [packageList, setPackageList] = React.useState(initialPackageList);
  const [allPricePackage, setAllPricePackage] = useState([]);

  useEffect(() => {
    GetAllPackages();
  }, []);

  const GetAllPackages = () => {
    debugger;
    PackageService
      .GetAllPackages()
      .then((response) => {
        debugger;
        setAllPricePackage(response.data.data.packages);
        setPackageList(response.data.data.packagesFeatures);
      })
      .catch((e) => {
        console.log(e);
      });
  };


  function SampleNextArrow(props:any) {
    const { className, style, onClick } = props;
    return (
      <div
        className={className}
        onClick={onClick}
      >
       <img className="sL-next" src={arrowRight} />
       </div>
    );
  }
  
  function SamplePrevArrow(props:any) {
    const { className, style, onClick } = props;
    return (
        <div
        className={className}
        onClick={onClick}
      >
       <img className="sL-prev" src={arrowPrev} />
       </div>
    );
  }

  var settings = {
    className: "",
    centerMode: true,
    infinite: true,
    // centerPadding: "88px",
    slidesToShow: 1,
    speed: 500,
    focusOnSelect: true,
    nextArrow: <SampleNextArrow />,
    prevArrow: <SamplePrevArrow />,
  };

  return (
    <React.Fragment>
      <div className="layout-home">
        <AppHeader />

        <section className="hero-section">
          <div className="container">
            <div className="row banner-text">
              <div className="blackHead">
                Impossible hiring goals?
                <br />
                We have the solution.
              </div>
              <div className="shortBorder"></div>

              <p>
                Our sourcing technology blends 4,000 Sourcers <br />
                and AI to find talent within hours
              </p>

              <div className="blackSubHead">
                Discover now and learn more
                <span>
                  <img src={arrowIcon} />
                </span>
              </div>
            </div>
          </div>
        </section>

        <section className="features">
          <div className="container">
            <div className="row">
              <div className="col-md-4">
                <div className="firstFeature">
                  <div className="featureHead">20x Faster</div>
                  <p>
                    We simplify candidate sourcing and outreach so you can focus
                    on what really matters - people
                  </p>
                  <img src={card1} />
                </div>
              </div>

              <div className="col-md-4">
                <div className="firstFeature secondFeature">
                  <div className="featureHead">50% Cheaper</div>
                  <p>
                    Our flexible sourcing solution is 50% cheaper than in-house
                    sourcing and can quickly scale up or down according to your
                    hiring needs
                  </p>
                  <img src={card2} />
                </div>
              </div>

              <div className="col-md-4">
                <div className="firstFeature thirdFeature">
                  <div className="featureHead">10x Higher Quality</div>
                  <p>
                    The unique combination of AI and a network of top sourcers
                    ensure you will find the best talent. Unlike sourcing
                    robots, We understand your unique requirements and delivers
                    10x higher profile acceptance rates
                  </p>
                  <img src={card3} />
                </div>
              </div>
            </div>
          </div>
        </section>

        <section className="splitCol firstSplit">
          <div className="container">
            <div className="row">
              <div className="col-md-5 info">
                <div className="splitHead">Easy Job Posting</div>
                <div className="shortBorder"></div>
                <p>
                  Define the role requirements , and submit. AI matches the role
                  to 5+ Top Sourcers with the most relevant expertise for
                  immediate sourcing. With a network of over 4,000 sourcers,
                  Visage can support searches for any function, domain, and
                  industry.
                </p>
              </div>
              <div className="col-md-2"></div>
              <div className="col-md-5">
                <img src={split1} />
              </div>
            </div>
          </div>
        </section>

        <section className="splitCol secondSplit">
          <div className="container">
            <div className="row">
              <div className="col-md-5">
                <img src={split2} />
              </div>
              <div className="col-md-2"></div>
              <div className="col-md-5 info">
                <div className="splitHead">Smart Sourcing</div>
                <div className="shortBorder"></div>
                <p>
                  Our Top Sourcers compete to submit their best-matching
                  candidates within 24h. They are incentivized based on the
                  quality of submission. They will leave no stone unturned to
                  identify potential candidates.
                </p>
              </div>
            </div>
          </div>
        </section>

        <section className="splitCol firstSplit">
          <div className="container">
            <div className="row">
              <div className="col-md-5 info">
                <div className="splitHead">Qualify</div>
                <div className="shortBorder"></div>
                <p>
                  AI expedites the matching process to identify top candidates.
                  Algorithms disqualify candidates based on structured data (job
                  history, title, skills, etc.). We take into account your
                  feedback (candidates accepted/rejected) using machine learning
                </p>
              </div>
              <div className="col-md-2"></div>
              <div className="col-md-5">
                <img src={split3} />
              </div>
            </div>
          </div>
        </section>

        <section className="splitCol secondSplit">
          <div className="container">
            <div className="row">
              <div className="col-md-5">
                <img src={split4} />
              </div>
              <div className="col-md-2"></div>
              <div className="col-md-5 info">
                <div className="splitHead">Engage</div>
                <div className="shortBorder"></div>
                <p>
                  Review candidate profiles and provide feedback to improve your
                  next batch of candidates. In one-click, engage your shortlist
                  with custom messages via email and SMS. See candidate
                  responses directly in your inbox
                </p>

                <div className="blackSubHead">
                  Request a demo
                  <span>
                    <img className="reqImg" src={arrowIcon} />
                  </span>
                </div>
              </div>
            </div>
          </div>
        </section>

        <section className='packagesSection'>
        <div className='container'>
        <div className='row'>
              {allPricePackage.map((item) => (
                        <div className='col-md-3'>
                        <div className='packageBox'>
                        <div className='packageUpper'>
                        <div>{item.packageTitle}</div>
                        <div className='package-price'>${item.packagePrice}</div>
                        <div>per month, paid yearly</div>
                        </div>
                        <div className='packageBox-body'>
                        <ul>
                          
                          {packageList.filter((temp) => temp.packageId == item.packageId
                        )
                        .map((k) => {
                          return (
                            <li>
                              {k.text}
                          </li>
                          );
                        })}
                        </ul>
                        </div>
                        </div>
          
                        
                        </div>
          
                     ))}
              
              </div>
        </div>
        </section>

        <section className="blueSec">
          <img src={circles} />
          <div className="bluesecHead">Ready to save time and resources?</div>

          <div className="inputBlue">
            <img className="reqImg" src={arrowIcon} />
            <input placeholder="Enter Your Email" className="form-control" />
          </div>

          <div></div>
        </section>

        <section className="slider-sec">
          <div className="container">
            <div className="row">
              <div className="col-md-2"></div>
              <div className="col-md-8">
                <Slider {...settings}>
                  <div>
                    <img src={dumSlider} />
                    <p>
                      Lorem Ipsum is simply dummy text of the printing and
                      typesetting industry. Lorem Ipsum has been the industry's
                      standard dummy text ever since the 1500s, when an unknown
                      printer took a galley of type and scrambled
                    </p>
                    <div className="featureHead">John Smith</div>
                    <div>Art director</div>
                  </div>
                  <div>
                    <img src={dumSlider} />
                    <p>
                      Lorem Ipsum is simply dummy text of the printing and
                      typesetting industry. Lorem Ipsum has been the industry's
                      standard dummy text ever since the 1500s, when an unknown
                      printer took a galley of type and scrambled
                    </p>
                    <div className="featureHead">John Smith</div>
                    <div>Art director</div>
                  </div>
                </Slider>
              </div>
              <div className="col-md-2"></div>
            </div>
          </div>
        </section>
      </div>
      <AppFooter />
    </React.Fragment>
  );
}
