import React from "react";
import { createStyles, makeStyles, Theme } from "@material-ui/core/styles";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import arrowIcon from "../../assets/arrow-icon.png";
import split1 from "../../assets/5.png";
import split2 from "../../assets/7.png";
import split3 from "../../assets/6.png";
import split4 from "../../assets/8.png";
import circles from "../../assets/circles.png";
import card1 from "../../assets/card-1.png";
import card2 from "../../assets/card-2.png";
import card3 from "../../assets/card-3.png";
import { AppHeaderArabic } from "../../components/appHeaderArabic";
import AppFooter from "../../components/appFooter";


import dumSlider from "../../assets/dum-slider.png";
import sliderImg from "../../assets/profile.png";
import arrowRight from "../../assets/arrow-icon.png";
import arrowPrev from "../../assets/arrow-prev.png";

import Slider from "react-slick";
import "slick-carousel/slick/slick-theme.css";
import "slick-carousel/slick/slick.css";


const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {},
  })
);

export default function HomePageArabic() {
  const classes = useStyles();

  function SampleNextArrow(props:any) {
    const { className, style, onClick } = props;
    return (
      <div
        className={className}
        onClick={onClick}
      >
       <img className="sL-next" src={arrowRight} />
       </div>
    );
  }
  
  function SamplePrevArrow(props:any) {
    const { className, style, onClick } = props;
    return (
        <div
        className={className}
        onClick={onClick}
      >
       <img className="sL-prev" src={arrowPrev} />
       </div>
    );
  }

  var settings = {
    className: "",
    centerMode: true,
    infinite: true,
    // centerPadding: "88px",
    slidesToShow: 1,
    speed: 500,
    focusOnSelect: true,
    nextArrow: <SampleNextArrow />,
    prevArrow: <SamplePrevArrow />,
  };

  return (
    <React.Fragment>
      <div className="layout-home">
        <AppHeaderArabic />

        <section className="hero-section">
          <div className="container">
            <div className="row banner-text">
              <div className="blackHead">
              هذا النص هو مثال لنص يمكن

                <br />
                أن يستبدل في؟
              </div>
              <div className="shortBorder"></div>

              <p>
               
                هذا النص هو مثال لنص يمكن أن يستبدل في نفس المساحة
              </p>

              <div className="blackSubHead">
              هذا النص هو مثال لنص يمكن أن يستبدل في
                <span>
                  <img src={arrowPrev} />
                </span>
              </div>
            </div>
          </div>
        </section>

        <section className="features">
          <div className="container">
            <div className="row">
              <div className="col-md-4">
                <div className="firstFeature">
                  <div className="featureHead">سرعه أكبر</div>
                  <p>
                  هذا النص هو مثال لنص يمكن أن يستبدل في نفس المساحة، لقد تم توليد هذا النص من مولد النص العربى، حيث يمكنك أن تولد مثل هذا النص أو العديد من النصوص الأخرى إضافة إلى زيادة عدد الحروف التى يولدها التطبيق.

                  </p>
                  <img src={card1} />
                </div>
              </div>

              <div className="col-md-4">
                <div className="firstFeature secondFeature">
                  <div className="featureHead">سعر أرخص</div>
                  <p>
                  هذا النص هو مثال لنص يمكن أن يستبدل في نفس المساحة، لقد تم توليد هذا النص من مولد النص العربى، حيث يمكنك أن تولد مثل هذا النص أو العديد من النصوص الأخرى إضافة إلى زيادة عدد الحروف التى يولدها التطبيق.

                  </p>
                  <img src={card2} />
                </div>
              </div>

              <div className="col-md-4">
                <div className="firstFeature thirdFeature">
                  <div className="featureHead">جودة أعلي</div>
                  <p>
                  هذا النص هو مثال لنص يمكن أن يستبدل في نفس المساحة، لقد تم توليد هذا النص من مولد النص العربى، حيث يمكنك أن تولد مثل هذا النص أو العديد من النصوص الأخرى إضافة إلى زيادة عدد الحروف التى يولدها التطبيق.

                  </p>
                  <img src={card3} />
                </div>
              </div>
            </div>
          </div>
        </section>

        <section className="splitCol firstSplit">
          <div className="container">
            <div className="row arabic-dir-right">
              <div className="col-md-5 info">
                <div className="splitHead">اضافه وظائف</div>
                <div className="shortBorder"></div>
                <p>
                هذا النص هو مثال لنص يمكن أن يستبدل في نفس المساحة، لقد تم توليد هذا النص من مولد النص العربى، حيث يمكنك أن تولد مثل هذا النص أو العديد من النصوص الأخرى إضافة إلى زيادة عدد الحروف التى يولدها التطبيق.

                </p>
              </div>
              <div className="col-md-2"></div>
              <div className="col-md-5">
                <img src={split1} />
              </div>
            </div>
          </div>
        </section>

        <section className="splitCol secondSplit">
          <div className="container">
            <div className="row arabic-dir-right">
              <div className="col-md-5">
                <img src={split2} />
              </div>
              <div className="col-md-2"></div>
              <div className="col-md-5 info">
                <div className="splitHead">التصنيف الذكي</div>
                <div className="shortBorder"></div>
                <p>
                هذا النص هو مثال لنص يمكن أن يستبدل في نفس المساحة، لقد تم توليد هذا النص من مولد النص العربى، حيث يمكنك أن تولد مثل هذا النص أو العديد من النصوص الأخرى إضافة إلى زيادة عدد الحروف التى يولدها التطبيق.
إذا كنت تحتاج إلى عدد أكبر من الفقرات يتيح لك مولد النص العربى 
                </p>
              </div>
            </div>
          </div>
        </section>

        <section className="splitCol firstSplit">
          <div className="container">
            <div className="row arabic-dir-right">
              <div className="col-md-5 info">
                <div className="splitHead">الترقية</div>
                <div className="shortBorder"></div>
                <p>
                هذا النص هو مثال لنص يمكن أن يستبدل في نفس المساحة، لقد تم توليد هذا النص من مولد النص العربى، حيث يمكنك أن تولد مثل هذا النص أو العديد من النصوص الأخرى إضافة إلى زيادة عدد الحروف التى يولدها التطبيق.
                إذا كنت تحتاج إلى عدد أكبر من الفقرات يتيح لك مولد النص العربى 
                </p>
              </div>
              <div className="col-md-2"></div>
              <div className="col-md-5">
                <img src={split3} />
              </div>
            </div>
          </div>
        </section>

        <section className="splitCol secondSplit">
          <div className="container">
            <div className="row arabic-dir-right">
              <div className="col-md-5">
                <img src={split4} />
              </div>
              <div className="col-md-2"></div>
              <div className="col-md-5 info">
                <div className="splitHead">التواصل</div>
                <div className="shortBorder"></div>
                <p>
                هذا النص هو مثال لنص يمكن أن يستبدل في نفس المساحة، لقد تم توليد هذا النص من مولد النص العربى، حيث يمكنك أن تولد مثل هذا النص أو العديد من النصوص الأخرى إضافة إلى زيادة عدد الحروف التى يولدها التطبيق.
                إذا كنت تحتاج إلى عدد أكبر من الفقرات يتيح لك مولد النص العربى 
                </p>

                <div className="blackSubHead">
                اطلب نسخة
                  <span>
                    <img className="reqImg" src={arrowPrev} />
                  </span>
                </div>
              </div>
            </div>
          </div>
        </section>

        <section className="blueSec">
          <img src={circles} />
          <div className="bluesecHead">هذا النص هو مثال لنص يمكن أن يستبدل؟</div>

          <div className="inputBlue">
            <img className="reqImg" src={arrowPrev} />
            <input placeholder="ادخل البريد الالكتروني" className="form-control" />
          </div>

          <div></div>
        </section>

        <section className="slider-sec">
          <div className="container">
            <div className="row">
              <div className="col-md-2"></div>
              <div className="col-md-8">
                <Slider {...settings}>
                  <div>
                    <img src={dumSlider} />
                    <p>
                    هذا النص هو مثال لنص يمكن أن يستبدل في نفس المساحة، لقد تم توليد هذا النص من مولد النص العربى، حيث يمكنك أن تولد مثل هذا النص أو العديد من النصوص الأخرى إضافة إلى زيادة عدد الحروف التى يولدها التطبيق
                    </p>
                    <div className="featureHead">John Smith</div>
                    <div>Art director</div>
                  </div>
                  <div>
                    <img src={dumSlider} />
                    <p>
                    هذا النص هو مثال لنص يمكن أن يستبدل في نفس المساحة، لقد تم توليد هذا النص من مولد النص العربى، حيث يمكنك أن تولد مثل هذا النص أو العديد من النصوص الأخرى إضافة إلى زيادة عدد الحروف التى يولدها التطبيق
                    </p>
                    <div className="featureHead">John Smith</div>
                    <div>Art director</div>
                  </div>
                </Slider>
              </div>
              <div className="col-md-2"></div>
            </div>
          </div>
        </section>
      </div>
      <AppFooter />
    </React.Fragment>
  );
}
