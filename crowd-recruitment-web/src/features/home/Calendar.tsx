import React, { useEffect } from "react";
import { createStyles, makeStyles, Theme } from "@material-ui/core/styles";
import { InlineWidget } from "react-calendly";

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {},
  })
);

export default function Calendar(props: any) {
  const classes = useStyles();

  return (
    <React.Fragment>
      <div className="App">
        <InlineWidget url="https://calendly.com/basheer-8/meeting-with-basheer?month=2021-03" />
      </div>
    </React.Fragment>
  );
}
