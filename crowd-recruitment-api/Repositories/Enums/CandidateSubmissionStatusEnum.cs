﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Repositories.Enums
{
    public enum CandidateSubmissionStatusEnum
    {
        Draft = 1,
        Pending = 2,
        Approved = 3,
        Disqualified = 4,
        Approved_By_Employer = 5,
        Interviews = 6,
        Offer = 7,	
        Hiring = 8
    }
}
