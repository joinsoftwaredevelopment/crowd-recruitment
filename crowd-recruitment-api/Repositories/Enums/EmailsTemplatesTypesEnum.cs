﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Repositories.Enums
{
    public enum EmailsTemplatesTypesEnum
    {
        SystemEmails = 1,
        JobsCandidates = 2,
        Interviews = 3,
        Offers = 4,
        Common = 5,
    }
}
