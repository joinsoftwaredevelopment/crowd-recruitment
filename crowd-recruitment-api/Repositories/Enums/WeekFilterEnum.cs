﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Repositories.Enums
{
    public enum WeekFilterEnum
    {
        Previous = 1,
        Next = 2
    }
}
