﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Repositories.Enums
{
    public enum EmailsTemplatesEnum
    {
        CRVerificationEmail = 1,
        CRSourcerAcceptanceEmail = 2,
        CREmployerAcceptanceEmail = 3,
        CRZoomMeetingEmail = 4 ,
        CRNewSourcer = 5 ,
        CRNewEmployer = 6,
        CRRejectionEmail = 7,
        Interview = 9,
        Offer = 10,
        Common = 11,
        ResetPassoword = 12,
    }
}
