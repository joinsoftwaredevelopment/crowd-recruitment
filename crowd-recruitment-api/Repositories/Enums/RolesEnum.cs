﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Repositories.Enums
{
    public enum RolesEnum
    {
        Admin = 1,
        Employer = 2,
        Sourcer = 3
    }
}
