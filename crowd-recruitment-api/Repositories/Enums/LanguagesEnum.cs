﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Repositories.Enums
{
    public enum LanguagesEnum
    {
        Arabic = 1,
        English = 2
    }
}
