﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Repositories.Enums
{
    public enum UserStatusEnum
    {
        Request = 1,
        Approved = 2,
        Decline = 3
    }
}
