﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Repositories.Enums
{
    public enum JobStatusEnum
    {
        Draft = 1,
        Posted = 2,
        Opened = 3,
        Declined = 4,
        Closed = 5
    }
}
