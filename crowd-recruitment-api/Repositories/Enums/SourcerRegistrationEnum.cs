﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Repositories.Enums
{
    public enum SourcerRegistrationEnum
    {
        Simple_Form_Step = 1 ,
        Experience_Step = 2 ,
        Sourcer_Title_Step = 3 ,
        Website_Step = 4 ,
        Industries_Step = 5,
        Attach_CV_Step = 6,
        Book_A_Meeting = 7
    }
}
