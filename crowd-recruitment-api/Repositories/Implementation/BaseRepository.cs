﻿using Data.Tables;
using Repositories.Common.Loggers;
using Repositories.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Repositories.Implementation
{
    public class BaseRepository<TEntity> : IBaseRepository<TEntity> where TEntity : class, new()
    {
        protected readonly recruitmentContext _recruitmentContext;

        public TEntity CurrentModel { get; set; }

        public BaseRepository(recruitmentContext recruitmentContext)
        {
            _recruitmentContext = recruitmentContext;
            CurrentModel = new TEntity();
        }

        public TEntity Add(TEntity entity)
        {
            if (entity == null)
            {
                throw new ArgumentNullException($"{nameof(AddAsync)} entity must not be null");
            }

            try
            {
                _recruitmentContext.Add(entity);
                _recruitmentContext.SaveChanges();
                CurrentModel = entity;
                return entity;
            }
            catch (Exception ex)
            {
                throw new Exception($"{nameof(entity)} could not be saved: {ex.Message}");
            }
        }

        public bool Update(TEntity entity)
        {
            if (entity == null)
            {
                throw new ArgumentNullException($"{nameof(UpdateAsync)} entity must not be null");
            }

            try
            {
                _recruitmentContext.Update(entity);
                CurrentModel = entity;
                return _recruitmentContext.SaveChanges() > 0;
            }
            catch (Exception ex)
            {
                throw new Exception($"{nameof(entity)} could not be updated: {ex.Message}");
            }
        }

        public bool Delete(string uUId)
        {
            if (uUId == null)
            {
                throw new ArgumentNullException($"{nameof(Delete)} entity must not be null");
            }

            try
            {
                //_recruitmentContext.Remove(entity);
                return _recruitmentContext.SaveChanges() > 0;
            }
            catch (Exception ex)
            {
                throw new Exception($"{nameof(Delete)} could not be saved: {ex.Message}");
            }
        }

        public async Task<TEntity> AddAsync(TEntity entity)
        {
            if (entity == null)
            {
                throw new ArgumentNullException($"{nameof(AddAsync)} entity must not be null");
            }

            try
            {
                await _recruitmentContext.AddAsync(entity);
                await _recruitmentContext.SaveChangesAsync();

                return entity;
            }
            catch (Exception ex)
            {
                throw new Exception($"{nameof(entity)} could not be saved: {ex.Message}");
            }
        }

        public async Task<TEntity> UpdateAsync(TEntity entity)
        {
            if (entity == null)
            {
                throw new ArgumentNullException($"{nameof(UpdateAsync)} entity must not be null");
            }

            try
            {
                _recruitmentContext.Update(entity);
                await _recruitmentContext.SaveChangesAsync();

                return entity;
            }
            catch (Exception ex)
            {
                throw new Exception($"{nameof(entity)} could not be updated: {ex.Message}");
            }
        }


        public TEntity GetById(long id)
        {
            try
            {
                return _recruitmentContext.Set<TEntity>().Find(id);
            }
            catch (Exception e)
            {
                // we can log exception here in exception table
                return null;
            }
        }
        public IQueryable<TEntity> GetAll()
        {
            try
            {
                return _recruitmentContext.Set<TEntity>().AsQueryable();
            }
            catch (Exception e)
            {
                // we can log exception here in exception table
                return null;
            }
        }
        public IQueryable<TEntity> FindBy(Expression<Func<TEntity, bool>> predicate)
        {
            try
            {
                return _recruitmentContext.Set<TEntity>().Where(predicate);
            }
            catch (Exception e)
            {
                // we can log exception here in exception table
                return null;
            }
        }

        public TEntity FindFirstBy(Expression<Func<TEntity, bool>> predicate)
        {
            try
            {
                return _recruitmentContext.Set<TEntity>().FirstOrDefault(predicate);
            }
            catch (Exception e)
            {
                // we can log exception here in exception table
                return null;
            }
        }

        public int LogException(Exception exception)
        {
            //return Logger.Instance.Log(exception);
            return 1;
        }


    }
}
