﻿using Data.Tables;
using Data.Views;
using Repositories.Enums;
using Repositories.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Repositories.Implementation
{
    public class SourcerProfileRepository : BaseRepository<SourcerDetail>, ISourcerProfileRepository
    {
        #region Fieids
        private readonly recruitmentContext _recruitmentContext;
        #endregion

        #region Constructors
        public SourcerProfileRepository(recruitmentContext context) : base(context)
        {
            _recruitmentContext = context;
        }
        #endregion

        #region Methods

        public bool UpdateBasicInfo(string uuid, string jobTitle, string fName, 
            string lname, string phone, string email)
        {
            try
            {
                var currentUser = _recruitmentContext.Users.FirstOrDefault(m=> m.Uuid == uuid);
                currentUser.FirstName = fName;
                currentUser.LastName = lname;
                currentUser.PhoneNumber = phone;
                currentUser.Email = email;

                _recruitmentContext.SaveChanges();
                
                var currentSourcer = _recruitmentContext.SourcerDetails
                       .FirstOrDefault(m => m.UserId == currentUser.Id);
                currentSourcer.JobTitle = jobTitle;
                _recruitmentContext.SaveChanges();
                 return true;

            }
            catch (System.Exception e)
            {
                LogException(e);
                return false;
            }
        }

        public bool UpdateCareerInfo(string uuid, int experienceTypeId,
                    bool hasAccessToDifferentWebsite, int industryId)
        {
            try
            {
                var currentUser = _recruitmentContext.Users.FirstOrDefault(m => m.Uuid == uuid);
            
                var currentSourcer = _recruitmentContext.SourcerDetails
                       .FirstOrDefault(m => m.UserId == currentUser.Id);
                currentSourcer.ExperienceTypeId = experienceTypeId;
                currentSourcer.HasAccessToDifferentWebsite = hasAccessToDifferentWebsite;
                currentSourcer.IndustryId = industryId;
                _recruitmentContext.SaveChanges();
                return true;

            }
            catch (System.Exception e)
            {
                LogException(e);
                return false;
            }
        }

        public VwSourcer GetBasicInfo(string uuid)
        {
            try
            {
                return _recruitmentContext.VwSourcers.
                     FirstOrDefault(m => m.Uuid == uuid);
            }
            catch (System.Exception e)
            {
                LogException(e);
                return null;
            }
        }

        #endregion
    }
}
