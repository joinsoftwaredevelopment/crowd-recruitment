﻿using Data.Tables;
using Microsoft.EntityFrameworkCore;
using Repositories.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Repositories.Implementation
{
    public class CredentialRepository : BaseRepository<Credential>, ICredentialRepository
    {
        #region Fieids
        private readonly recruitmentContext _recruitmentContext;
        #endregion

        #region Constructors
        public CredentialRepository(recruitmentContext context) : base(context)
        {
            _recruitmentContext = context ;
        }
        #endregion

        #region Methods

        public Credential GetCredentials()
        {
            try
            {
                return _recruitmentContext.Credentials.FirstOrDefault();
            }
            catch (System.Exception e)
            {
                LogException(e);
                return null;
            }
        }

        #endregion
    }

}
