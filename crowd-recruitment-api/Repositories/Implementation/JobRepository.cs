﻿using Data.Tables;
using Data.Views;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Query.SqlExpressions;
using Repositories.Enums;
using Repositories.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Repositories.Implementation
{
    public class JobRepository : BaseRepository<User>, IJobRepository
    {
        #region Fieids
        private readonly recruitmentContext _recruitmentContext;
        #endregion

        #region Constructors
        public JobRepository(recruitmentContext context) : base(context)
        {
            _recruitmentContext = context;
        }
        #endregion

        #region Methods
        public JobDetailsInfo GetJobDetailsInfo(string uuid, int? tempId)
        {
            //var seniorityLevelsTranslation = _recruitmentContext.SeniorityLevelTranslations
            //    .Where(m => m.LanguageId == 2);

            //var employmentTypeTranslations = _recruitmentContext.EmploymentTypeTranslations
            //    .Where(m => m.LanguageId == 2);

            //var functionTranslations = _recruitmentContext.JobFunctionTranslations
            //    .Where(m => m.LanguageId == 2);

            //var companyIndustryTranslations = _recruitmentContext.CompanyIndustryTranslations
            //    .Where(m => m.LanguageId == 2);

            //var jobLocationTranslations = _recruitmentContext.JobLocationTranslations
            //    .Where(m => m.LanguageId == 2);

            var CountriesTranslations = _recruitmentContext.CountriesTranslations
                .Where(m => m.LanguageId == 2).ToList();

            var currentJob = new PostJobCurentDetails();

            if (tempId > 0)
            {
                var job = _recruitmentContext.VwJobTemplates
                    .FirstOrDefault(m => m.JobTemplateId == tempId);
                currentJob.employerTypeId = (int)job.EmploymentTypeId;
                currentJob.industryId = (int)job.JobIndustryId;
                currentJob.jobDescription = job.Description;
                currentJob.jobNationalityId = job.JobLocationId;
                currentJob.jobTitle = job.JobTitle;
                currentJob.requirements = job.Requirements;
            }

            else if(uuid != "0")
            {
                var job = _recruitmentContext.Jobs.FirstOrDefault(m => m.Uuid == uuid);
                currentJob.employerTypeId = (int)job.EmployerTypeId;
                currentJob.industryId = (int)job.IndustryId;
                currentJob.jobDescription = job.JobDescription;
                currentJob.jobLocationId = job.JobLocationId;
                currentJob.jobNationalityId = (int)job.JobNationalityId;
                currentJob.jobTitle = job.JobTitle;
                currentJob.requirements = job.Requirements;
                currentJob.seniorityLevelId = (int)job.SeniorityLevelId;
                currentJob.uuId = job.Uuid;
            }
            JobDetailsInfo jobDetailsInfo = new JobDetailsInfo()
            {
                //seniorityLevels = seniorityLevelsTranslation,
                //employmentTypeTranslations = employmentTypeTranslations,
                //jobFunctionTranslations = functionTranslations,
                //companyIndustryTranslations = companyIndustryTranslations,
                //jobLocationTranslations = jobLocationTranslations,
                countriesTranslations = CountriesTranslations,
                job = currentJob
            };

            return jobDetailsInfo;
        }

        public JoObjectiveInfo GetJobInstructionsInfo(string uuid)
        {
            var currentJob = _recruitmentContext.Jobs.FirstOrDefault(m => m.Uuid == uuid);

            var mustQualifications = _recruitmentContext.EmployerMustHaveQualifications
                       .Where(m => m.JobId == currentJob.Id).ToList();
            var niceQualifications = _recruitmentContext.EmployerNiceHaveQualifications
                       .Where(m => m.JobId == currentJob.Id).ToList();

            var notToSourceQualifications = _recruitmentContext.CompaniesNotToSourceFroms
                       .Where(m => m.JobId == currentJob.Id).ToList();


            var joObjectiveInfo = new JoObjectiveInfo()
            {
                job = currentJob,
                mustQualifications = mustQualifications,
                niceQualifications = niceQualifications,
                notToSourceQualifications = notToSourceQualifications,
            };

            return joObjectiveInfo;
        }

        public JobEngagementInfo GetJobEngagementsInfo(string uuid)
        {
            var currentJob = _recruitmentContext.Jobs.FirstOrDefault(m => m.Uuid == uuid);

            var templates = _recruitmentContext.EmailsTemplates
                       .Where(m => m.EmailTemplateType == 2).ToList();

            var employerDetails = _recruitmentContext.EmployerDetails
                       .Where(m => m.UserId == currentJob.EmployerId).First();

            var jobEngagementInfo = new JobEngagementInfo()
            {
                job = currentJob,
                templates = templates,
                isMembershipActive = (employerDetails.PackageId != null && employerDetails.IsMembershipActive)
            };

            return jobEngagementInfo;
        }


        public IEnumerable<ExperienceLevelTranslation> GetJobInstructionsInfo()
        {
            var experienceLevelTranslation = _recruitmentContext.ExperienceLevelTranslations
                .Where(m => m.LanguageId == 2);

            return experienceLevelTranslation;
        }

        public Job GetJobByUuid(string uuid)
        {
            var job = _recruitmentContext.Jobs.FirstOrDefault(m => m.Uuid == uuid);
            return job;
        }

        public bool CreateJob(Job job)
        {
            try
            {
                job.JobStatusId = (int)JobStatusEnum.Draft;
                job.CurrencyId = 122;
                job.MinSalary = 0;
                job.MaxSalary = 0;
                _recruitmentContext.Jobs.Add(job);
                if (_recruitmentContext.SaveChanges() > 0)
                    return true;
                return false;
            }
            catch (System.Exception e)
            {
                LogException(e);
                return false;
            }
        }

        public bool UpdateJobInstructions(string uuid, int experienceLevelId,
                List<mustHaveQualification> mustHaveQualifications,
                List<niceHaveQualification> niceHaveQualification,
                List<notToSource> notToSource, int currencyId, int minSalary, int maxSalary
            )
        {
            var jobEntity = _recruitmentContext.Jobs.FirstOrDefault(U => U.Uuid == uuid);
            if (jobEntity != null)
            {
                jobEntity.ExperienceLevelId = experienceLevelId;
                jobEntity.CurrencyId = currencyId;
                jobEntity.MinSalary = minSalary;
                jobEntity.MaxSalary = maxSalary;
                //_recruitmentContext.SaveChanges();

                var mustQualification = new EmployerMustHaveQualification();
                var niceQualification = new EmployerNiceHaveQualification();
                var notSource = new CompaniesNotToSourceFrom();

                mustQualification.JobId = jobEntity.Id;
                niceQualification.JobId = jobEntity.Id;
                notSource.JobId = jobEntity.Id;


                // clean lists from database
                var must = _recruitmentContext.EmployerMustHaveQualifications.Where(m => m.JobId == jobEntity.Id);
                if (must != null)
                {
                    foreach (var item in must)
                    {
                        _recruitmentContext.EmployerMustHaveQualifications.Remove(item);
                    }
                }


                var nice = _recruitmentContext.EmployerNiceHaveQualifications.Where(m => m.JobId == jobEntity.Id);
                if (nice != null)
                {
                    foreach (var item in nice)
                    {
                        _recruitmentContext.EmployerNiceHaveQualifications.Remove(item);
                    }
                }

                var companies = _recruitmentContext.CompaniesNotToSourceFroms.Where(m => m.JobId == jobEntity.Id);
                if (companies != null)
                {
                    foreach (var item in companies)
                    {
                        _recruitmentContext.CompaniesNotToSourceFroms.Remove(item);
                    }
                }

                _recruitmentContext.SaveChanges();
                // MUST have
                foreach (var item in mustHaveQualifications)
                {
                    if (item.id != "")
                    {
                        // search with uuid if not exists, add new one

                        var checkExistence = _recruitmentContext.EmployerMustHaveQualifications.FirstOrDefault(m => m.Uuid == item.id);

                        if (checkExistence != null)
                        {
                            // delete then add
                            mustQualification.Id = 0;
                            mustQualification.Uuid = item.id;
                            mustQualification.Text = item.name;
                            _recruitmentContext.EmployerMustHaveQualifications.Add(mustQualification);
                            _recruitmentContext.SaveChanges();
                        }

                        else
                        {
                            // add
                            mustQualification.Id = 0;
                            mustQualification.Uuid = item.id;
                            mustQualification.Text = item.name;
                            _recruitmentContext.EmployerMustHaveQualifications.Add(mustQualification);
                            _recruitmentContext.SaveChanges();

                        }

                    }

                }

                // Nice to have
                foreach (var item in niceHaveQualification)
                {
                    if (item.id != "")
                    {
                        // search with uuid if not exists, add new one

                        var checkExistence = _recruitmentContext.EmployerNiceHaveQualifications.FirstOrDefault(m => m.Uuid == item.id);

                        if (checkExistence != null)
                        {
                            _recruitmentContext.EmployerNiceHaveQualifications.Remove(checkExistence);

                            niceQualification.Id = 0;
                            niceQualification.Uuid = item.id;
                            niceQualification.Text = item.name;
                            _recruitmentContext.EmployerNiceHaveQualifications.Add(niceQualification);
                            _recruitmentContext.SaveChanges();
                        }

                        else
                        {
                            niceQualification.Id = 0;
                            niceQualification.Uuid = item.id;
                            niceQualification.Text = item.name;
                            _recruitmentContext.EmployerNiceHaveQualifications.Add(niceQualification);
                            _recruitmentContext.SaveChanges();
                        }

                    }

                }


                // Not to source
                foreach (var item in notToSource)
                {
                    if (item.id != "")
                    {
                        // search with uuid if not exists, add new one
                        var checkExistence = _recruitmentContext.CompaniesNotToSourceFroms.FirstOrDefault(m => m.Uuid == item.id);

                        if (checkExistence != null)
                        {
                            _recruitmentContext.CompaniesNotToSourceFroms.Remove(checkExistence);
                            notSource.Id = 0;
                            notSource.Uuid = item.id;
                            notSource.Text = item.name;
                            _recruitmentContext.CompaniesNotToSourceFroms.Add(notSource);
                            _recruitmentContext.SaveChanges();
                        }

                        else
                        {
                            notSource.Id = 0;
                            notSource.Uuid = item.id;
                            notSource.Text = item.name;
                            _recruitmentContext.CompaniesNotToSourceFroms.Add(notSource);
                            _recruitmentContext.SaveChanges();
                        }

                    }

                }

                return true;
            }

            return false;
        }

        public bool UpdateJob(string uuId, int EmployerTypeId,
                int industryId, string JobDescription,
                int jobLocationId, int JobNationalityId,
                string JobTitle, int SeniorityLevelId, string requirements
                )
        {
            try
            {
                var jobEntity = _recruitmentContext.Jobs.FirstOrDefault(U => U.Uuid == uuId);
                if (jobEntity != null)
                {
                    jobEntity.EmployerTypeId = EmployerTypeId;
                    jobEntity.IndustryId = industryId;
                    jobEntity.JobDescription = JobDescription;
                    jobEntity.JobLocationId = jobLocationId;
                    jobEntity.JobNationalityId = JobNationalityId;
                    jobEntity.JobTitle = JobTitle;
                    jobEntity.SeniorityLevelId = SeniorityLevelId;
                    jobEntity.Requirements = requirements;
                    _recruitmentContext.SaveChanges();
                    return true;
                }

                return false;
            }
            catch (System.Exception e)
            {
                LogException(e);
                return false;
            }

        }

        public bool UpdateJobObjectives(string uuid, bool HiringNeeds,
                int PeopleToHireCount, int whenYouNeedToHire, bool sourcingOutsideOfJoin,
                string FindingJobDifficultyLevelId)
        {
            var jobEntity = _recruitmentContext.Jobs.FirstOrDefault(U => U.Uuid == uuid);
            if (jobEntity != null)
            {
                jobEntity.IsContinuousHire = HiringNeeds;
                jobEntity.PeopleToHireCount = PeopleToHireCount;
                jobEntity.TimetoHireId = whenYouNeedToHire;
                jobEntity.NumberOfComingDays = 1;
                jobEntity.SourcingOutsideOfJoin = sourcingOutsideOfJoin;
                jobEntity.FindingJobDifficultyLevelId = Convert.ToInt32(FindingJobDifficultyLevelId.Substring(0, 1));
                _recruitmentContext.SaveChanges();
                return true;
            }

            return false;
        }

        public bool UpdateJobEngagement(string uuid, string firstMailSubject,
                string firstMailDescription, int statusId, int engagementTemplateId)
        {
            var jobEntity = _recruitmentContext.Jobs.FirstOrDefault(U => U.Uuid == uuid);
            if (jobEntity != null)
            {
                jobEntity.FirstMailSubject = firstMailSubject;
                jobEntity.FirstMailDescription = firstMailDescription;
                jobEntity.JobStatusId = statusId;
                jobEntity.EngagementTemplateId = engagementTemplateId;

                _recruitmentContext.SaveChanges();
                return true;
            }

            return false;
        }

        public List<VwJob> GetJobSummaries(int languageId, int sourcerId)
        {
            try
            {
                var sourcerJobSummaries = _recruitmentContext.VwJobSubmissionsSummaries.Where(job => job.LanguageId == languageId &&
                                           job.SourcerId == sourcerId).ToList();

                var jobSummaries = (from j in _recruitmentContext.VwJobs
                                    join ed in _recruitmentContext.EmployerDetails
                                     on j.EmployerId equals ed.UserId
                                    where j.LanguageId == languageId && j.JobStatusId == (int)JobStatusEnum.Opened && ed.IsMembershipActive == true
                                    select j).ToList();
                
                jobSummaries = jobSummaries.Where(o => !sourcerJobSummaries.Any(oo => oo.JobId == o.JobId)).ToList();

                return jobSummaries;
            }
            catch (System.Exception e)
            {
                LogException(e);
                return null;
            }
        }

        public List<VwJob> GetJobSummariesByEmployer(int languageId, string employeruuid)
        {
            try
            {
                var jobSummaries = _recruitmentContext.VwJobs.Where(job => job.LanguageId == languageId
                && job.EmpoyerUuId == employeruuid && (job.JobStatusId != (int)JobStatusEnum.Closed) && (job.JobStatusId != (int)JobStatusEnum.Draft)).ToList();

                return jobSummaries.OrderByDescending(m=>m.JobDate).ToList();
            }
            catch (System.Exception e)
            {
                LogException(e);
                return null;
            }
        }

        public List<VwJob> GetAllJobs(int languageId, string employeruuid)
        {
            try
            {
                var jobSummaries = _recruitmentContext.VwJobs.Where(job => job.LanguageId == languageId
                && job.EmpoyerUuId == employeruuid).ToList();

                return jobSummaries.OrderByDescending(m => m.JobDate).ToList();
            }
            catch (System.Exception e)
            {
                LogException(e);
                return null;
            }
        }

        public List<VwJob> GetAllJobs(int statusId)
        {
            try
            {
                var jobSummaries = _recruitmentContext.VwJobs
                    .Where(job => job.LanguageId == 2 &&
                    job.JobStatusId == statusId).OrderByDescending(m => m.JobId)
                     .ToList();

                return jobSummaries;
            }
            catch (System.Exception e)
            {
                LogException(e);
                return null;
            }
        }

        public List<VwJob> GetJobDraftsByEmployer(int languageId, string employeruuid)
        {
            try
            {
                var jobSummaries = _recruitmentContext.VwJobs.Where(job => job.LanguageId == languageId
                && job.EmpoyerUuId == employeruuid && job.JobStatusId == 1).ToList();

                return jobSummaries;
            }
            catch (System.Exception e)
            {
                LogException(e);
                return null;
            }
        }

        public List<VwJob> GetJobClosedByEmployer(int languageId, string employeruuid)
        {
            try
            {
                var jobSummaries = _recruitmentContext.VwJobs.Where(job => job.LanguageId == languageId
                && job.EmpoyerUuId == employeruuid && job.JobStatusId == 5).ToList();

                return jobSummaries;
            }
            catch (System.Exception e)
            {
                LogException(e);
                return null;
            }
        }

        public bool CloseJob(string uuid)
        {
            var jobEntity = _recruitmentContext.Jobs.FirstOrDefault(U => U.Uuid == uuid);
            if (jobEntity != null)
            {
                jobEntity.JobStatusId = 5;
                _recruitmentContext.SaveChanges();
                return true;
            }

            return false;
        }

        public bool DeleteJob(string uuid)
        {
            var jobEntity = _recruitmentContext.Jobs.FirstOrDefault(U => U.Uuid == uuid);
            if (jobEntity != null)
            {
                // clean lists from database
                var must = _recruitmentContext.EmployerMustHaveQualifications
                    .Where(m => m.JobId == jobEntity.Id);
                if (must != null)
                {
                    foreach (var item in must)
                    {
                        _recruitmentContext.EmployerMustHaveQualifications.Remove(item);
                    }
                }


                var nice = _recruitmentContext.EmployerNiceHaveQualifications.Where(m => m.JobId == jobEntity.Id);
                if (nice != null)
                {
                    foreach (var item in nice)
                    {
                        _recruitmentContext.EmployerNiceHaveQualifications.Remove(item);
                    }
                }

                var companies = _recruitmentContext.CompaniesNotToSourceFroms.Where(m => m.JobId == jobEntity.Id);
                if (companies != null)
                {
                    foreach (var item in companies)
                    {
                        _recruitmentContext.CompaniesNotToSourceFroms.Remove(item);
                    }
                }

                _recruitmentContext.SaveChanges();

                _recruitmentContext.Jobs.Remove(jobEntity);
                _recruitmentContext.SaveChanges();

                return true;
            }

            return false;
        }


        public List<VwJobSubmissionsSummary> GetSourcerJobSummaries(int languageId, int sourcerId, bool isActive)
        {
            try
            {
                var jobSummaries = new List<VwJobSubmissionsSummary>();

                if (isActive)

                    jobSummaries = _recruitmentContext.VwJobSubmissionsSummaries.Where(job => job.LanguageId == languageId &&
                         job.SourcerId == sourcerId && job.JobStatusId != 5).ToList();
                else
                    jobSummaries = _recruitmentContext.VwJobSubmissionsSummaries.Where(job => job.LanguageId == languageId &&
                    job.SourcerId == sourcerId && job.JobStatusId == 5).ToList();

                jobSummaries = (from jobs in jobSummaries
                           join ed in _recruitmentContext.EmployerDetails
                           on jobs.EmployerId equals ed.UserId
                           where ed.IsMembershipActive == true
                           select jobs).ToList();

                return jobSummaries;

            }
            catch (System.Exception e)
            {
                LogException(e);
                return null;
            }
        }

        public List<VwSourcerSubmission> GetSourcerJobSubmissions(int languageId, int sourcerId, int jobId)
        {
            try
            {
                var jobSummaries = _recruitmentContext.VwSourcerSubmissions.Where(job => job.LanguageId == languageId &&
                job.SourcerId == sourcerId && job.JobId == jobId).ToList();

                return jobSummaries;
            }
            catch (System.Exception e)
            {
                LogException(e);
                return null;
            }
        }

        public Job GetJobBuUuId(string uuId)
        {
            try
            {
                var job = _recruitmentContext.Jobs.Where(o => o.Uuid == uuId).FirstOrDefault();
                return job;
            }
            catch (System.Exception e)
            {
                LogException(e);
                return null;
            }
        }

        public VwJob GetVwJobByUuId(string uuId, int languageId)
        {
            try
            {
                var job = _recruitmentContext.VwJobs.Where(o => o.Uuid == uuId && o.LanguageId == languageId).FirstOrDefault();
               
                var mustHaveQualificationList = string.Join("\r\n", _recruitmentContext.EmployerMustHaveQualifications.Where(o => o.JobId == job.JobId).Select(o => string.Join("\r\n", o.Text)));
                job.MustHaveQualification = mustHaveQualificationList;

                var niceToHaveQualification = string.Join("\r\n", _recruitmentContext.EmployerNiceHaveQualifications.Where(o => o.JobId == job.JobId).Select(o => string.Join("\r\n", o.Text)));
                job.NiceToHaveQualification = niceToHaveQualification;

                var companiesNotToSourceFrom = string.Join("\r\n", _recruitmentContext.CompaniesNotToSourceFroms.Where(o => o.JobId == job.JobId).Select(o => string.Join("\r\n", o.Text)));
                job.CompaniesNotToSourceFrom = companiesNotToSourceFrom;

                job.FindingJobDifficultyName = GetDifficultyLevelById((int)job.FindingJobDifficultyLevelId, 2);
                job.TimetoHireName = GettimetoHireNameById((int)job.TimetoHireId, 2);

                return job;
            }
            catch (System.Exception e)
            {
                LogException(e);
                return null;
            }
        }

        public JobCandidateSubmission AddSourcerSubmission(JobCandidateSubmission jobCandidateSubmission)
        {
            try
            {
                _recruitmentContext.JobCandidateSubmissions.Add(jobCandidateSubmission);
                _recruitmentContext.SaveChanges();
                return jobCandidateSubmission;
            }
            catch (System.Exception e)
            {
                LogException(e);
                return null;
            }
        }


        public bool AcceptJob(string uuid, int numberOfSubmissions, decimal commission)
        {
            try
            {
                var jobEntity = _recruitmentContext.Jobs.FirstOrDefault(U => U.Uuid == uuid);
                if (jobEntity != null)
                {
                    jobEntity.JobStatusId = 3;
                    jobEntity.NoOfSubmissions = numberOfSubmissions;
                    jobEntity.Commission = commission;

                    if (_recruitmentContext.SaveChanges() > 0)
                        return true;
                    return false;
                }

                else
                    return false;
            }
            catch (System.Exception e)
            {
                LogException(e);
                return false;
            }

        }

        public bool DeclineJob(string uuid, string rejectionReason)
        {
            var jobEntity = _recruitmentContext.Jobs.FirstOrDefault(U => U.Uuid == uuid);
            if (jobEntity != null)
            {
                jobEntity.JobStatusId = 4;
                jobEntity.Reason = rejectionReason;
                jobEntity.Commission = 0;

                if (_recruitmentContext.SaveChanges() > 0)
                    return true;
            }

            return false;
        }


        public List<CompanyIndustryTranslation> GetAllIndustries()
        {
            try
            {
                return _recruitmentContext.CompanyIndustryTranslations
                    .Where(m => m.LanguageId == 2).ToList();
            }
            catch (System.Exception e)
            {
                LogException(e);
                return null;
            }
        }

        public List<VwJobTemplate> GetTempByIndustryId(int industryId)
        {
            try
            {
                return _recruitmentContext.VwJobTemplates
                    .Where(m => m.LanguageId == 2 && m.JobIndustryId == industryId).ToList();
            }
            catch (System.Exception e)
            {
                LogException(e);
                return null;
            }
        }

        public VwJobTemplate GetJobTempById(int templateId)
        {
            try
            {
                var temp = _recruitmentContext.VwJobTemplates
                    .FirstOrDefault(m => m.JobTemplateId == templateId);

                var mustHaveQualificationList = string.Join("\r\n", _recruitmentContext.JobTemplateMustHaveQualifications.Where(o => o.JobTemplateId == temp.JobTemplateId).Select(o => string.Join("\r\n", o.Text)));
                temp.MustHaveQualification = mustHaveQualificationList;

                var niceToHaveQualification = string.Join("\r\n", _recruitmentContext.JobTemplateNiceHaveQualifications.Where(o => o.JobTemplateId == temp.JobTemplateId).Select(o => string.Join("\r\n", o.Text)));
                temp.NiceToHaveQualification = niceToHaveQualification;

                return temp;
            }
            catch (System.Exception e)
            {
                LogException(e);
                return null;
            }
        }

        public List<VwJobTemplate> GetAllJobTemps()
        {
            try
            {
                return _recruitmentContext.VwJobTemplates
                    .Where(m => m.LanguageId == 2).ToList();
            }
            catch (System.Exception e)
            {
                LogException(e);
                return null;
            }
        }

        public void InsertJobTemplateSkills(int jobTemplateId, int jobId)
        {
            var mustToHaveSkills = _recruitmentContext.JobTemplateMustHaveQualifications.Where(m => m.JobTemplateId == jobTemplateId);
            if (mustToHaveSkills != null)
            {
                foreach (var item in mustToHaveSkills)
                {

                    _recruitmentContext.EmployerMustHaveQualifications.Add(new EmployerMustHaveQualification()
                    {
                        JobId = jobId,
                        Uuid = Guid.NewGuid().ToString(),
                        Text = item.Text
                    });
                }
            }

            var niceToHaveSkills = _recruitmentContext.JobTemplateNiceHaveQualifications.Where(m => m.JobTemplateId == jobTemplateId);
            if (niceToHaveSkills != null)
            {
                foreach (var item in niceToHaveSkills)
                {
                    _recruitmentContext.EmployerNiceHaveQualifications.Add(new EmployerNiceHaveQualification()
                    {
                        JobId = jobId,
                        Uuid = Guid.NewGuid().ToString(),
                        Text = item.Text
                    });
                }
            }

            _recruitmentContext.SaveChanges();
        }

        public List<BasicModel> GetDifficultyLevel(int languageId)
        {
            List<BasicModel> lst = new List<BasicModel>();
            lst.Add(new BasicModel()
            {
                Id = 1,
                NameAr = "",
                NameEn = "Not difficult"
            });

            lst.Add(new BasicModel()
            {
                Id = 2,
                NameAr = "",
                NameEn = "Medium"
            });

            lst.Add(new BasicModel()
            {
                Id = 3,
                NameAr = "",
                NameEn = "Very Difficult"
            });

            return lst;
        }

        public List<BasicModel> GettimetoHireName(int languageId)
        {
            List<BasicModel> lst = new List<BasicModel>();
            lst.Add(new BasicModel()
            {
                Id = 1,
                NameAr = "",
                NameEn = "As soon as possible"
            });

            lst.Add(new BasicModel()
            {
                Id = 2,
                NameAr = "",
                NameEn = "In the comming days"
            });

            lst.Add(new BasicModel()
            {
                Id = 3,
                NameAr = "",
                NameEn = "No deadline"
            });

            return lst;
        }

        
        public string GetDifficultyLevelById(int id, int languageId)
        {
            var difficulty = GetDifficultyLevel(id).Find(m => m.Id == id);
            if (difficulty == null)
                return "";
            else
                return difficulty.NameEn;
        }
        public string GettimetoHireNameById(int id, int languageId)
        {
            var Time = GettimetoHireName(id).Find(m => m.Id == id);
            if (Time == null)
                return "";
            else
                return Time.NameEn;
        }

        #endregion
    }

}
