﻿using Data.Tables;
using Repositories.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Repositories.Implementation
{
    public class EmailTemplateRepository : BaseRepository<EmailsTemplate>, IEmailTemplateRepository
    {
        #region Fieids
        private readonly recruitmentContext _recruitmentContext;
        #endregion

        #region Constructors
        public EmailTemplateRepository(recruitmentContext context) : base(context)
        {
            _recruitmentContext = context;
        }
        #endregion

        #region Methods

        public EmailsTemplate GetEmailsTemplate(int templateId)
        {
            try
            {
                var currentTemplate = _recruitmentContext.EmailsTemplates.FirstOrDefault(U => U.Id == templateId);
                return currentTemplate;
            }
            catch (System.Exception e)
            {
                LogException(e);
                return null;
            }

        }

        public List<EmailsTemplate> GetJobEmailsTemplate()
        {
            try
            {
                var currentTemplate = _recruitmentContext.EmailsTemplates
                    .Where(m => m.EmailTemplateType == 2).ToList();
                return currentTemplate;
            }
            catch (System.Exception e)
            {
                LogException(e);
                return null;
            }

        }
        #endregion
    }
}
