﻿using Data.Tables;
using Repositories.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Repositories.Implementation
{
    public class EmployerPackageHistoryRepository : BaseRepository<EmployerPackageHistory>, IEmployerPackageHistoryRepository
    {
        #region Fieids
        private readonly recruitmentContext _recruitmentContext;
        #endregion


        #region Constructors
        public EmployerPackageHistoryRepository(recruitmentContext context) : base(context)
        {
            _recruitmentContext = context;
        }

        public IEnumerable<VwEmployerPackageHistory> GetAllPackagesHistory(int userId)
        {
            try
            {
                var employerPackageHistory = _recruitmentContext.VwEmployerPackageHistory.Where(P=>P.UserId == userId).ToList();
                return employerPackageHistory;
            }
            catch (System.Exception e)
            {
                LogException(e);
                return null;
            }
        }
        #endregion
    }
}
