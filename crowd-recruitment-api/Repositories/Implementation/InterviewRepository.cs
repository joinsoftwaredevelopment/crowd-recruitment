﻿using Data.Tables;
using Data.Views;
using Repositories.Enums;
using Repositories.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Repositories.Implementation
{
    public class InterviewRepository : BaseRepository<CandidatesInterview>, IInterviewRepositpry
    {
        #region Fieids
        private readonly recruitmentContext _recruitmentContext;
        #endregion

        #region Constructors
        public InterviewRepository(recruitmentContext context) : base(context)
        {
            _recruitmentContext = context;
        }
        #endregion

        #region Methods
        public int AddInterviewDetails(CandidatesInterview candidatesInterview,
            string employerUuid)
        {
            try
            {
                var userEntity = _recruitmentContext.Users.FirstOrDefault(U => U.Uuid == employerUuid);
                
                candidatesInterview.CreatedBy = userEntity.Id;

                candidatesInterview.IsEmailSent = true;
                candidatesInterview.IsCanceled = false;

                _recruitmentContext.CandidatesInterviews.Add(candidatesInterview);
                if (_recruitmentContext.SaveChanges() > 0)
                    return candidatesInterview.Id;

                return 0;
            }
            catch (System.Exception e)
            {
                LogException(e);
                return 0;
            }
        }

        public VwJobCandidate GetCandidateBySubmissionUuid(string submissionUuid)
        {
            var candidateInfo = _recruitmentContext.VwJobCandidates
                        .FirstOrDefault(m => m.SubmissionUuId == submissionUuid);
            return candidateInfo;
        }


        public int UpdateInterviewDetails(CandidatesInterview candidatesInterview)
        {
            try
            {
                var interviewEntity = _recruitmentContext.CandidatesInterviews
                    .FirstOrDefault(m => m.Id == candidatesInterview.Id);
                if (interviewEntity != null)
                {

                    interviewEntity.CandidateId = candidatesInterview.CandidateId;
                    interviewEntity.InterviewerId = candidatesInterview.InterviewerId;
                    interviewEntity.InterviewComment = candidatesInterview.InterviewComment;
                    interviewEntity.InterviewTypeId = candidatesInterview.InterviewTypeId;
                    interviewEntity.InterviewDate = candidatesInterview.InterviewDate;
                    interviewEntity.InterviewTime = candidatesInterview.InterviewTime;
                    interviewEntity.Duration = candidatesInterview.Duration;
                    interviewEntity.CreatedDate = candidatesInterview.CreatedDate;
                    interviewEntity.EmailSubject = candidatesInterview.EmailSubject;
                    interviewEntity.EmailDescription = candidatesInterview.EmailDescription;
                    interviewEntity.TemplateId = candidatesInterview.TemplateId;

                    if (_recruitmentContext.SaveChanges() > 0)
                        return candidatesInterview.Id;
                }

                return 0;
            }
            catch (System.Exception e)
            {
                LogException(e);
                return 0;
            }
        }

        public int AddInterviewEmails(CandidatesInterview candidatesInterview)
        {
            try
            {
                var interviewEntity = _recruitmentContext.CandidatesInterviews
                    .FirstOrDefault(m => m.Id == candidatesInterview.Id);
                if (interviewEntity != null)
                {

                    interviewEntity.TemplateId = candidatesInterview.TemplateId;
                    interviewEntity.EmailSubject = candidatesInterview.EmailSubject;
                    interviewEntity.EmailDescription = candidatesInterview.EmailDescription;
                    interviewEntity.IsEmailSent = candidatesInterview.IsEmailSent;
                    if (_recruitmentContext.SaveChanges() > 0)
                        return candidatesInterview.Id;
                }
     
                return 0;
            }
            catch (System.Exception e)
            {
                LogException(e);
                return 0;
            }
        }

        public int CancelInterview(int interviewId, bool isCanceled)
        {
            try
            {
                var interviewEntity = _recruitmentContext.CandidatesInterviews
                    .FirstOrDefault(m => m.Id == interviewId);
                if (interviewEntity != null)
                {
                    interviewEntity.IsCanceled = isCanceled;
                    if (_recruitmentContext.SaveChanges() > 0)
                        return interviewId;
                }

                return 0;
            }
            catch (System.Exception e)
            {
                LogException(e);
                return 0;
            }
        }

        
        public List<Interviewer> GetAllInterviewers(int languageId)
        {
            try
            {
                return _recruitmentContext.Interviewers.ToList();          
            }
            catch (System.Exception e)
            {
                LogException(e);
                return null;
            }
        }

        public VwInterviewDetail GetInterviewDetailsById(int interviewId)
        {
            try
            {
                return _recruitmentContext.VwInterviewDetails
                    .FirstOrDefault(m => m.Id == interviewId);
            }
            catch (System.Exception e)
            {
                LogException(e);
                return null;
            }
        }

        public List<VwCandidatesInterview> GetAllInterviews()
        {
            try
            {
                return _recruitmentContext.VwCandidatesInterviews.Where(m => m.IsCanceled == false).ToList();
            }
            catch (System.Exception e)
            {
                LogException(e);
                return null;
            }
        }

        public List<VwCandidatesInterview> GetAllInterviewsByUuid(string uuid)
        {
            try
            {
                return _recruitmentContext.VwCandidatesInterviews
                    .Where(m => m.SubmissionUuid == uuid && m.IsCanceled == false).ToList();
            }
            catch (System.Exception e)
            {
                LogException(e);
                return null;
            }
        }

        public VwInterviewEmail GetInterviewEmailsById(int interviewId)
        {
            try
            {
                return _recruitmentContext.VwInterviewEmails
                    .FirstOrDefault(m => m.Id == interviewId);
            }
            catch (System.Exception e)
            {
                LogException(e);
                return null;
            }
        }

        public List<EmailsTemplate> GetInterviewTemplates()
        {
            try
            {
                return _recruitmentContext.EmailsTemplates.
                    Where(m=> m.EmailTemplateType == (int)EmailsTemplatesTypesEnum.Interviews)
                    .ToList();
            }
            catch (System.Exception e)
            {
                LogException(e);
                return null;
            }
        }

        public List<EmailsTemplate> GetOfferTemplates()
        {
            try
            {
                return _recruitmentContext.EmailsTemplates.
                    Where(m => m.EmailTemplateType == (int)EmailsTemplatesTypesEnum.Offers)
                    .ToList();
            }
            catch (System.Exception e)
            {
                LogException(e);
                return null;
            }
        }

        
        public EmailsTemplate GetInterviewTempById(int templateId)
        {
            try
            {
                return _recruitmentContext.EmailsTemplates.
                    FirstOrDefault(m => m.EmailTemplateType == templateId);
            }
            catch (System.Exception e)
            {
                LogException(e);
                return null;
            }
        }

        
        public VwCandidatesInterview GetInterviewEmailDetails(int interviewId)
        {
            try
            {
                return _recruitmentContext.VwCandidatesInterviews.
                    FirstOrDefault(m => m.Id == interviewId);
            }
            catch (System.Exception e)
            {
                LogException(e);
                return null;
            }
        }

        #endregion
    }
}
