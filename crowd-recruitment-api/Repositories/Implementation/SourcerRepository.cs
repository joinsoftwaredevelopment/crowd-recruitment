﻿using Data.Tables;
using Data.Views;
using Microsoft.EntityFrameworkCore;
using Repositories.Enums;
using Repositories.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Repositories.Implementation
{
    public class SourcerRepository : BaseRepository<SourcerDetail>, ISourcerRepository
    {
        #region Fieids
        private readonly recruitmentContext _recruitmentContext;
        private readonly IUserRepository _userRepository;

        #endregion

        #region Constructors
        public SourcerRepository(recruitmentContext context, IUserRepository userRepository) : base(context)
        {
            _recruitmentContext = context ;
        }

        #endregion

        #region Methods

        public SourcerDetail GetSourcerByUuId(string uuid)
        {
            var userEntity = _recruitmentContext.Users.FirstOrDefault(U => U.Uuid == uuid);
            if (userEntity != null)
            {
                var sourcerEntity = _recruitmentContext.SourcerDetails.FirstOrDefault(U => U.UserId == userEntity.Id);
                return sourcerEntity;
            }

            return null;
        }

        public bool UpdateSourcerCV(string uuid, string cvName, string linkedInProfile)
        {
            var userEntity = _recruitmentContext.Users.FirstOrDefault(U => U.Uuid == uuid);
            if (userEntity != null)
            {
                var sourcerEntity = _recruitmentContext.SourcerDetails.FirstOrDefault(U => U.UserId == userEntity.Id);
                sourcerEntity.CvName = cvName;
                sourcerEntity.LinkedInProfile = linkedInProfile;
                var isUpdated = Update(sourcerEntity);

                userEntity.RegistrationStep = (int)SourcerRegistrationEnum.Attach_CV_Step;
                _recruitmentContext.Update(userEntity);
                _recruitmentContext.SaveChanges();

                return isUpdated;
            }

            return false;
        }
        public int GetSourcerExperienceId(string uuid)
        {
            var userEntity = _recruitmentContext.Users.FirstOrDefault(U => U.Uuid == uuid);
            if (userEntity != null)
            {
                var sourcerEntity = _recruitmentContext.SourcerDetails.FirstOrDefault(U => U.UserId == userEntity.Id);
                var experienceId = 0;
                if (sourcerEntity.ExperienceTypeId != null)
                    experienceId = (int)sourcerEntity.ExperienceTypeId;

                return experienceId;
            }

            return 0;
        }

        public bool UpdateSourcerExperience(string uuid, int? experienceLevelId)
        {
            var userEntity = _recruitmentContext.Users.FirstOrDefault(U => U.Uuid == uuid);
            if(userEntity != null)
            {
                var sourcerEntity = _recruitmentContext.SourcerDetails.FirstOrDefault(U => U.UserId == userEntity.Id);
                sourcerEntity.ExperienceTypeId = experienceLevelId;
                var isUpdated = Update(sourcerEntity);

                userEntity.RegistrationStep = (int)SourcerRegistrationEnum.Experience_Step;
                //_userRepository.Update(userEntity);
                _recruitmentContext.Update(userEntity);
                _recruitmentContext.SaveChanges();

                return isUpdated;
            }

            return false;
        }

        public int GetSourcerIndustryId(string uuid)
        {
            var userEntity = _recruitmentContext.Users.FirstOrDefault(U => U.Uuid == uuid);
            if (userEntity != null)
            {
                var sourcerEntity = _recruitmentContext.SourcerDetails.FirstOrDefault(U => U.UserId == userEntity.Id);
                var industryId = 0;
                if (sourcerEntity.IndustryId != null)
                    industryId = (int)sourcerEntity.IndustryId;

                return industryId;
            }

            return 0;
        }

        public bool UpdateSourcerIndustry(string uuid, int? industryId)
        {
            var userEntity = _recruitmentContext.Users.FirstOrDefault(U => U.Uuid == uuid);
            if (userEntity != null)
            {
                var sourcerEntity = _recruitmentContext.SourcerDetails.FirstOrDefault(U => U.UserId == userEntity.Id);
                sourcerEntity.IndustryId = industryId;
                var isUpdated = Update(sourcerEntity);

                userEntity.RegistrationStep = (int)SourcerRegistrationEnum.Industries_Step;
                _recruitmentContext.Update(userEntity);
                _recruitmentContext.SaveChanges();

                return isUpdated;
            }

            return false;
        }


        public string GetJobtitleName(string uuid)
        {
            var userEntity = _recruitmentContext.Users.FirstOrDefault(U => U.Uuid == uuid);
            if (userEntity != null)
            {
                var sourcerEntity = _recruitmentContext.SourcerDetails.FirstOrDefault(U => U.UserId == userEntity.Id);
                var jobTitle = "";
                if (sourcerEntity != null)
                    jobTitle = sourcerEntity.JobTitle;

                return jobTitle;
            }

            return "";
        }
        public bool UpdateSourcerJobtitle(string uuid, string jobTitle)
        {
            var userEntity = _recruitmentContext.Users.FirstOrDefault(U => U.Uuid == uuid);
            if (userEntity != null)
            {
                var sourcerEntity = _recruitmentContext.SourcerDetails.FirstOrDefault(U => U.UserId == userEntity.Id);
                sourcerEntity.JobTitle = jobTitle;
                var isUpdated = Update(sourcerEntity);

                userEntity.RegistrationStep = (int)SourcerRegistrationEnum.Sourcer_Title_Step;
                _recruitmentContext.Update(userEntity);
                _recruitmentContext.SaveChanges();

                return isUpdated;
            }

            return false;
        }


        public bool GetSourcerhasAccessToWebsite(string uuid)
        {
            var userEntity = _recruitmentContext.Users.FirstOrDefault(U => U.Uuid == uuid);
            if (userEntity != null)
            {
                var sourcerEntity = _recruitmentContext.SourcerDetails.FirstOrDefault(U => U.UserId == userEntity.Id);
                var hasAccessToWebsite = false;
                if (sourcerEntity.HasAccessToDifferentWebsite != null)
                    hasAccessToWebsite = (bool)sourcerEntity.HasAccessToDifferentWebsite;

                return hasAccessToWebsite;
            }

            return false;
        }
        public bool UpdateSourcerWebsites(string uuid, bool? hasAccessToDifferentWebsite)
        {
            var userEntity = _recruitmentContext.Users.FirstOrDefault(U => U.Uuid == uuid);
            if (userEntity != null)
            {
                var sourcerEntity = _recruitmentContext.SourcerDetails.FirstOrDefault(U => U.UserId == userEntity.Id);
                sourcerEntity.HasAccessToDifferentWebsite = hasAccessToDifferentWebsite;
                var isUpdated = Update(sourcerEntity);

                userEntity.RegistrationStep = (int)SourcerRegistrationEnum.Website_Step;
                _recruitmentContext.Update(userEntity);
                _recruitmentContext.SaveChanges();

                return isUpdated;
            }

            return false;
        }

        //// Sourcer acceptance
        public IEnumerable<VwSourcer> GetAllSourcers(int userStatusId)
        {
            try
            {
                var sourcers = _recruitmentContext.VwSourcers.Where(o=>o.UserStatusId == userStatusId)
                    .OrderByDescending(m=> m.SourcerId);
                return sourcers;
            }
            catch (System.Exception e)
            {
                LogException(e);
                return null;
            }
        }

        public Sourcer GetAllSourcerInfo(string uuid)
        {
            try
            {
                var currentUser = _recruitmentContext.Users.FirstOrDefault(m => m.Uuid == uuid);
                var currentSourcer = _recruitmentContext.SourcerDetails.FirstOrDefault(m => m.UserId == currentUser.Id);
                string experience = "";
                string industry = "";

                // assign experience
                switch (currentSourcer.ExperienceTypeId)
                {
                    case 1:
                        experience = "0 - 2" ;
                        break;
                    case 2:
                        experience = "3 - 5";
                        break;
                    case 3:
                        experience = "6 - 10";
                        break;
                    case 4:
                        experience = "10 +";
                        break;
                }

                // assign industry
                switch (currentSourcer.IndustryId)
                {
                    case 1:
                        industry = "Business";
                        break;
                    case 2:
                        industry = "Tech";
                        break;
                    case 3:
                        industry = "Health care";
                        break;
                    case 4:
                        industry = "Admin & Sales";
                        break;
                }

                Sourcer sourcer = new Sourcer()
                {
                    uuid = currentUser.Uuid,
                    date = currentUser.CreatedDate,
                    fullName = currentUser.FirstName + " " +currentUser.LastName,
                    phoneNumber = currentUser.PhoneNumber,
                    emailAddress = currentUser.Email,
                    experience = experience,
                    jobTitle = currentUser.Uuid,
                    haveAccessToDifferentWebsite = Convert.ToBoolean(currentSourcer.HasAccessToDifferentWebsite),
                    IndustryYouInterested = industry,
                    CvPath = currentSourcer.CvName,
                    LinkedInProfile = currentSourcer.LinkedInProfile,
                    RejectedReason = currentUser.RejectedReason,
                    UserStatusId = (int) currentUser.UserStatusId,
                    registerationStep = (int)currentUser.RegistrationStep
                };

                return sourcer;
            }
            catch (System.Exception e)
            {
                LogException(e);
                return null;
            }
        }


        public bool AcceptSourcer(string Uuid)
        {
            var userEntity = _recruitmentContext.Users.FirstOrDefault(U => U.Uuid == Uuid);
            if (userEntity != null)
            {
                userEntity.Accepted = true;
                userEntity.UserStatusId = (int)UserStatusEnum.Approved;
                userEntity.RejectedReason = null;

                return _recruitmentContext.SaveChanges() > 0;
            }

            return false;
        }

        public bool DeclineSourcer(string Uuid, string rejectedReason)
        {
            var userEntity = _recruitmentContext.Users.FirstOrDefault(U => U.Uuid == Uuid);
            if (userEntity != null)
            {
                userEntity.Accepted = false;
                userEntity.UserStatusId = (int)UserStatusEnum.Decline;
                userEntity.RejectedReason = rejectedReason;

                return _recruitmentContext.SaveChanges() > 0; 
            }

            return false;
        }

        public IEnumerable<VwSourcerBalance> GetSourcerBalances(string uuid)
        {
            try
            {
                var sourcerBalances = _recruitmentContext.VwSourcerBalances.Where(o=>o.SourcerUuid == uuid);
                return sourcerBalances;
            }
            catch (System.Exception e)
            {
                LogException(e);
                return null;
            }
        }

        public VwSourcerBalancesSummary GetSourcerBalancesSummary(string uuid)
        {
            try
            {
                var sourcerBalances = _recruitmentContext.VwSourcerBalancesSummaries.FirstOrDefault(o=>o.SourcerUuid == uuid);
                return sourcerBalances;
            }
            catch (System.Exception e)
            {
                LogException(e);
                return null;
            }
        }
        #endregion
    }

}
