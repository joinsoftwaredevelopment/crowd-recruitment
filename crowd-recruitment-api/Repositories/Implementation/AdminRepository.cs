﻿using Data.Tables;
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using Repositories.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Repositories.Implementation
{
    public class AdminRepository : BaseRepository<User>, IAdminRepository
    {
        #region Fieids
        private readonly recruitmentContext _recruitmentContext;
        #endregion

        #region Constructors
        public AdminRepository(recruitmentContext context) : base(context)
        {
            _recruitmentContext = context;
        }
        #endregion

        #region Methods

        public User LogIn(string email)
        {
            var currentUser = _recruitmentContext.Users.FirstOrDefault(U => U.RoleId == 1 && U.Email == email);
            return currentUser;
        }

        public IEnumerable<VwAdminSourcersBalance> GetVwAdminSourcersBalance()
        {
            try
            {
                var sourcers = _recruitmentContext.VwAdminSourcersBalances.ToList();
                return sourcers;
            }
            catch (System.Exception e)
            {
                LogException(e);
                return null;
            }
        }

        public IEnumerable<VwJobSourcer> GetJobSourcers(string jobUuId)
        {
            try
            {
                var sourcers = _recruitmentContext.VwJobSourcers.Where(o => o.JobUuId == jobUuId);
                return sourcers;
            }
            catch (System.Exception e)
            {
                LogException(e);
                return null;
            }
        }

        public IEnumerable<SourcerBalancesHistory> GetVwAdminSourcersBalance(DateTime startDate, DateTime endDate)
        {
            var sourcerBalancesHistory = new List<SourcerBalancesHistory>();

            var startDateParameter = new SqlParameter("@startDate", startDate);
            var endDateParameter = new SqlParameter("@endDate", endDate);

            var sourcerBalances = _recruitmentContext.VwAdminSourcersBalances.FromSqlRaw("exec GetSourcerBalancesByWeek {0} , {1}", startDateParameter, endDateParameter).ToList();

            var today = DateTime.Now;
            var weekTitle = string.Format("Week {0} - {1}", startDate.ToString("yyyy-MM-dd"), endDate.ToString("yyyy-MM-dd"));

            sourcerBalancesHistory = _recruitmentContext.SourcerBalancesHistories.ToList();

            if (today.Date >= startDate.Date && today.Date <= endDate.Date)
            {
                sourcerBalancesHistory = new List<SourcerBalancesHistory>();
                foreach (var sourcer in sourcerBalances)
                {
                    sourcerBalancesHistory.Add(new SourcerBalancesHistory()
                    {
                        TotalBalance = sourcer.TotalBalance.ToString(),
                        IsPaid = true,
                        JobCount = sourcer.JobCount ?? 0,
                        AcceptanceCount = sourcer.AcceptanceCount ?? 0,
                        SubmissionCount = sourcer.SubmissionCount ?? 0,
                        DeclinedCount = sourcer.DeclinedCount ?? 0,
                        AcceptanceRatio = sourcer.AcceptanceRatio.ToString(),
                        SourcerId = sourcer.SourcerId,
                        FullName = sourcer.FullName
                    });
                }
            }
            else if (!(today.Date >= startDate.Date && today.Date <= endDate.Date) && !sourcerBalancesHistory.Any(o => o.WeekTitle == weekTitle) && sourcerBalances.Count() > 0)
            {
                sourcerBalancesHistory = new List<SourcerBalancesHistory>();
                foreach (var balance in sourcerBalances)
                {
                    var sourcerBalance = new SourcerBalancesHistory()
                    {
                        TotalBalance = balance.TotalBalance.ToString(),
                        CreatedDate = DateTime.Now,
                        IsPaid = false,
                        JobCount = balance.JobCount.Value,
                        AcceptanceCount = balance.AcceptanceCount.Value,
                        SubmissionCount = balance.SubmissionCount.Value,
                        DeclinedCount = balance.DeclinedCount.Value,
                        AcceptanceRatio = balance.AcceptanceRatio.ToString(),
                        FullName = balance.FullName,
                        WeekTitle = weekTitle,
                        SourcerId = balance.SourcerId
                    };

                    sourcerBalancesHistory.Add(sourcerBalance);
                    _recruitmentContext.Add(sourcerBalance);

                }
                _recruitmentContext.SaveChanges();

            }
            else if (sourcerBalancesHistory.Any(o => o.WeekTitle == weekTitle))
            {
                sourcerBalancesHistory = _recruitmentContext.SourcerBalancesHistories.Where(o => o.WeekTitle == weekTitle).ToList();
            }
            else
            {
                sourcerBalancesHistory = new List<SourcerBalancesHistory>();
            }
            return sourcerBalancesHistory;
        }

        public bool UpdateAdminSourcersBalance(DateTime startDate, DateTime endDate, int sourcerId)
        {
            var weekTitle = string.Format("Week {0} - {1}", startDate.ToString("yyyy-MM-dd"), endDate.ToString("yyyy-MM-dd"));

            var sourcerPay = _recruitmentContext.SourcerBalancesHistories.FirstOrDefault(o => o.WeekTitle == weekTitle && o.SourcerId == sourcerId);
            sourcerPay.IsPaid = true;
            _recruitmentContext.Update(sourcerPay);

            return _recruitmentContext.SaveChanges() > 0;

        }

        public void UpdateEmployerPackages()
        {
            List<EmployerDetail> employerPackageList = _recruitmentContext.EmployerDetails.Where(o => o.PackageExpiryDate < DateTime.Now).ToList();
           
            foreach (EmployerDetail employerDetail in employerPackageList)
            {
                employerDetail.IsMembershipActive = false;
            }

            _recruitmentContext.SaveChanges();
        }

        #endregion
    }

}
