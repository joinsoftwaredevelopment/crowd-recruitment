﻿using Data.Tables;
using Data.Views;
using Repositories.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Repositories.Implementation
{
    public class UserRepository : BaseRepository<User>, IUserRepository
    {
        #region Fieids
        private readonly recruitmentContext _recruitmentContext;
        #endregion

        #region Constructors
        public UserRepository(recruitmentContext context) : base(context)
        {
            _recruitmentContext = context;
        }
        #endregion

        #region Methods

        public bool IsPhoneExistBefore(string phoneNumber, string uuId)
        {
            var users = new List<User>();

            if (string.IsNullOrEmpty(uuId)) // Insert Case
                users = _recruitmentContext.Users.Where(U => U.RoleId != 1 &&  U.PhoneNumber == phoneNumber && U.Active).ToList();
            else // Update Case
                users = _recruitmentContext.Users.Where(U => U.RoleId != 1 &&  U.PhoneNumber == phoneNumber && U.Active && U.Uuid != uuId).ToList();

            return users.Count() > 0;

        }

        public bool IsEmailExistBefore(string email, string uuId)
        {
            var users = new List<User>();

            if (string.IsNullOrEmpty(uuId)) // Insert Case
                users = _recruitmentContext.Users.Where(U => U.Email == email && U.Active).ToList();
            else // Update Case
                users = _recruitmentContext.Users.Where(U => U.Email == email && U.Active && U.Uuid != uuId).ToList();

            return users.Count() > 0;

        }

        public User LogIn(string email, int roleId)
        {
            var currentUser = _recruitmentContext.Users.FirstOrDefault(U => U.Email == email && U.RoleId == roleId);
            return currentUser;
        }

        public User GetUserByUuId(string uuId)
        {
            try
            {
                var currentUser = _recruitmentContext.Users.FirstOrDefault(U => U.Uuid == uuId);
                return currentUser;
            }
            catch (System.Exception e)
            {
                LogException(e);
                return null;
            }
        }

        public User GetUserById(int id)
        {
            try
            {
                var currentUser = _recruitmentContext.Users.FirstOrDefault(U => U.Id == id);
                return currentUser;
            }
            catch (System.Exception e)
            {
                LogException(e);
                return null;
            }
        }

        public User GetUserByEmail(string email)
        {
            try
            {
                var currentUser = _recruitmentContext.Users.FirstOrDefault(U => U.Email == email);
                return currentUser;
            }
            catch (System.Exception e)
            {
                LogException(e);
                return null;
            }
        }
        

        public bool BookMeeting(BookMeeting bookMeeting)
        {
            try
            {

               var currentUser = _recruitmentContext.Users
                    .FirstOrDefault(U => U.Uuid == bookMeeting.uuid);

                currentUser.MeetingPeriodId = Convert.ToInt32(bookMeeting.timeMettingId.Substring(0,1));
                currentUser.TimeZoneId = bookMeeting.timeZoneId;
                currentUser.MeetingTimeId = Convert.ToInt32(bookMeeting.timeMettingId.Substring(0, 1));
                currentUser.InterviewerId = bookMeeting.interviewerId;
                currentUser.MeetingDate = bookMeeting.day;
                _recruitmentContext.SaveChanges();

                return true;
            }
            catch (System.Exception e)
            {
                LogException(e);
                return false;
            }
        }

        public User GetAdminInfo()
        {
            try
            {
                var admin = _recruitmentContext.Users
                     .FirstOrDefault(U => U.RoleId == 1);

                return admin;
            }
            catch (System.Exception e)
            {
                LogException(e);
                return null;
            }
        }

        public Interviewer GetInterviewerById(int interviewerId)
        {
            try
            {
                var interviewer = _recruitmentContext.Interviewers
                     .FirstOrDefault(U => U.Id == interviewerId);

                return interviewer;
            }
            catch (System.Exception e)
            {
                LogException(e);
                return null;
            }
        }

        public List<VwCountry> GetAllCountries(int languageId)
        {
            try
            {
                var countries = _recruitmentContext.VwCountries.Where(U => U.LanguageId == languageId);

                return countries.ToList();
            }
            catch (System.Exception e)
            {
                LogException(e);
                return null;
            }
        }

        public List<VwCurrency> GetAllCurrencies(int languageId)
        {
            try
            {
                var countries = _recruitmentContext.VwCurrencies.Where(U => U.LanguageId == languageId);

                return countries.ToList();
            }
            catch (System.Exception e)
            {
                LogException(e);
                return null;
            }
        }

        public bool ResePassword(string password, string uuid)
        {
            try
            {
                var user = _recruitmentContext.Users.FirstOrDefault(U => U.Uuid == uuid);
                if (user != null)
                {
                    user.Password = password;
                    return _recruitmentContext.SaveChanges() > 0;
                }

                else
                    return false;
            }
            catch (System.Exception e)
            {
                LogException(e);
                return false;
            }
        }

        public User VerifyUserEmailByUuId(string uuId) {

            try
            {
                var user = _recruitmentContext.Users.FirstOrDefault(U => U.Uuid == uuId);
                if (user != null)
                {
                    user.IsEmailVerified = true;
                    _recruitmentContext.SaveChanges();
                    return user;
                }

                else
                    return null;
            }
            catch (System.Exception e)
            {
                LogException(e);
                return null;
            }
        }

        public bool ChangeEmail(string uuId, string Email)
        {
            try
            {
                var user = _recruitmentContext.Users.FirstOrDefault(U => U.Uuid == uuId);
                if (user != null)
                {
                    user.Email = Email;

                    return _recruitmentContext.SaveChanges() > 0;
                }

                else
                    return false;
            }
            catch (System.Exception e)
            {
                LogException(e);
                return false;
            }
        }
        public bool IsEmployerMembershipActive(int employeerId)
        {
            return _recruitmentContext.EmployerDetails.Where(p => p.UserId == employeerId).First().IsMembershipActive;
        }

        #endregion
    }

}
