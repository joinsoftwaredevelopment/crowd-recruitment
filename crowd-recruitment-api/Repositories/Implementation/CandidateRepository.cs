﻿using Data.Tables;
using Repositories.Enums;
using Repositories.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Repositories.Implementation
{
    public class CandidateRepository : BaseRepository<VwJobCandidate>, ICandidateRepository
    {
        #region Fieids
        private readonly recruitmentContext _recruitmentContext;
        #endregion

        #region Constructors
        public CandidateRepository(recruitmentContext context) : base(context)
        {
            _recruitmentContext = context;
        }
        #endregion

        #region Methods

        public List<VwJobCandidate> GetJobCandidates(int languageId, string jobUuid)
        {
            try
            {
                var jobCandidates = _recruitmentContext.VwJobCandidates.Where(o => o.JobUuId == jobUuid && o.LanguageId == languageId).ToList();
                return jobCandidates;
            }
            catch (System.Exception e)
            {
                LogException(e);
                return null;
            }
        }

        public List<VwJobCandidate> GetAcceptedJobCandidates(int languageId, string jobUuid)
        {
            try
            {
                var jobCandidates = _recruitmentContext.VwJobCandidates
                    .Where(o => o.JobUuId == jobUuid && o.LanguageId == languageId &&
                    (o.CandidateSubmissionStatusId >= (int)CandidateSubmissionStatusEnum.Approved ))
                    .ToList();
                return jobCandidates;
            }
            catch (System.Exception e)
            {
                LogException(e);
                return null;
            }
        }

        public VwJobCandidate GetJobCandidateByUuId(int languageId, string candidateUuid)
        {
            try
            {
                var jobCandidates = _recruitmentContext.VwJobCandidates.FirstOrDefault(o => o.SubmissionUuId == candidateUuid && o.LanguageId == languageId);
                return jobCandidates;
            }
            catch (System.Exception e)
            {
                LogException(e);
                return null;
            }
        }

        public List<VwJobCandidate> GetSourcerCandidateBySourcerUuId(int languageId, string sourcerUuId, string jobUuId)
        {
            try
            {
                var sourcer = _recruitmentContext.Users.FirstOrDefault(o => o.Uuid == sourcerUuId);
                var jobCandidates = _recruitmentContext.VwJobCandidates.Where(o => o.SourcerId == sourcer.Id && o.JobUuId == jobUuId && o.LanguageId == languageId).ToList();
                return jobCandidates;
            }
            catch (System.Exception e)
            {
                LogException(e);
                return null;
            }
        }

        public List<VwJobCandidate> GetSourcerCandidateBySubmissionStatusId(int languageId, string sourcerUuId, int submissionStatusId, string jobUuId)
        {
            try
            {
                var sourcer = _recruitmentContext.Users.FirstOrDefault(o => o.Uuid == sourcerUuId);
                var jobCandidates = new List<VwJobCandidate>();

                if (submissionStatusId == (int)CandidateSubmissionStatusEnum.Approved)
                {
                    jobCandidates =_recruitmentContext.VwJobCandidates
                        .Where(o => o.SourcerId == sourcer.Id 
                        && o.JobUuId == jobUuId && o.LanguageId == languageId 
                        && (o.CandidateSubmissionStatusId == submissionStatusId
                        || o.CandidateSubmissionStatusId == (int)CandidateSubmissionStatusEnum.Approved_By_Employer 
                        //|| o.CandidateSubmissionStatusId == (int)CandidateSubmissionStatusEnum.Declined_By_Employer
                        ) )
                        .ToList();
                }
                else
                {
                    jobCandidates = _recruitmentContext.VwJobCandidates.Where(o => o.SourcerId == sourcer.Id && o.JobUuId == jobUuId && o.LanguageId == languageId && o.CandidateSubmissionStatusId == submissionStatusId).ToList();

                }
                return jobCandidates;
            }
            catch (System.Exception e)
            {
                LogException(e);
                return null;
            }
        }


        public bool AcceptCandidate(string Uuid)
        {
            try
            {
                var candidateEntity = _recruitmentContext.JobCandidateSubmissions.FirstOrDefault(U => U.Uuid == Uuid);
                if (candidateEntity != null)
                {
                    candidateEntity.CandidateSubmissionStatusId = (int)CandidateSubmissionStatusEnum.Approved;

                    var job = _recruitmentContext.Jobs.FirstOrDefault(o => o.Id == candidateEntity.JobId);
                    candidateEntity.Commission = job.Commission;

                    return _recruitmentContext.SaveChanges() > 0;
                }

                return false;
            }
            catch (System.Exception e)
            {
                LogException(e);
                return false;
            }
        }

        public bool DeclineCandidate(string Uuid, string rejectedReason)
        {
            var candidateEntity = _recruitmentContext.JobCandidateSubmissions.FirstOrDefault(U => U.Uuid == Uuid);
            if (candidateEntity != null)
            {
                candidateEntity.CandidateSubmissionStatusId = (int)CandidateSubmissionStatusEnum.Disqualified;
                candidateEntity.RejectionReason = rejectedReason;
                candidateEntity.Commission = 0;

                return _recruitmentContext.SaveChanges() > 0;
            }

            return false;
        }

        public bool EmployerAcceptCandidate(string Uuid)
        {
            try
            {
                var candidateEntity = _recruitmentContext.JobCandidateSubmissions.FirstOrDefault(U => U.Uuid == Uuid);
                if (candidateEntity != null)
                {
                    candidateEntity.CandidateSubmissionStatusId = (int)CandidateSubmissionStatusEnum.Approved_By_Employer;

                    //var job = _recruitmentContext.Jobs.FirstOrDefault(o => o.Id == candidateEntity.JobId);
                    //candidateEntity.Commission = job.Commission;

                    return _recruitmentContext.SaveChanges() > 0;
                }

                return false;
            }
            catch (System.Exception e)
            {
                LogException(e);
                return false;
            }
        }

        public bool UpdateHiringStage(string submissionUuid, int hiringStageId)
        {
            try
            {
                var candidateEntity = _recruitmentContext.JobCandidateSubmissions.FirstOrDefault(U => U.Uuid == submissionUuid);
                if (candidateEntity != null)
                {
                    //if (hiringStageId == (int)CandidateSubmissionStatusEnum.Approved_By_Employer)
                    //    candidateEntity.IsCvApproved = true;

                    candidateEntity.CandidateSubmissionStatusId = hiringStageId;
                    candidateEntity.SubmissionDate = DateTime.Now;

                    return _recruitmentContext.SaveChanges() > 0;
                }

                return false;
            }
            catch (System.Exception e)
            {
                LogException(e);
                return false;
            }
        }

        public bool UpdateIsAcceptOffer(string submissionUuidUuid, bool isOfferAccepted)
        {
            try
            {
                var candidateEntity = _recruitmentContext.JobCandidateSubmissions.FirstOrDefault(U => U.Uuid == submissionUuidUuid);
                if (candidateEntity != null)
                {
                    candidateEntity.IsAcceptOffer = isOfferAccepted;

                    return _recruitmentContext.SaveChanges() > 0;
                }

                return false;
            }
            catch (System.Exception e)
            {
                LogException(e);
                return false;
            }
        }

        public bool UpdateIsHired(string submissionUuidUuid, bool isCandidateHired)
        {
            try
            {
                var candidateEntity = _recruitmentContext.JobCandidateSubmissions.FirstOrDefault(U => U.Uuid == submissionUuidUuid);
                if (candidateEntity != null)
                {
                    candidateEntity.IsHired = isCandidateHired;

                    return _recruitmentContext.SaveChanges() > 0;
                }

                return false;
            }
            catch (System.Exception e)
            {
                LogException(e);
                return false;
            }
        }

        public bool UpdateCandidateCvStatus(string submissionUuid, bool isCvApproved)
        {
            try
            {
                var candidateEntity = _recruitmentContext.JobCandidateSubmissions.FirstOrDefault(U => U.Uuid == submissionUuid);
                if (candidateEntity != null)
                {
                    candidateEntity.IsCvApproved = isCvApproved;
                    
                     return _recruitmentContext.SaveChanges() > 0;
                }

                return false;
            }
            catch (System.Exception e)
            {
                LogException(e);
                return false;
            }
        }

        public bool EmployerDeclineCandidate(string Uuid, string rejectedReason)
        {
            var candidateEntity = _recruitmentContext.JobCandidateSubmissions.FirstOrDefault(U => U.Uuid == Uuid);
            if (candidateEntity != null)
            {
                //candidateEntity.CandidateSubmissionStatusId = (int)CandidateSubmissionStatusEnum.Declined_By_Employer;
                candidateEntity.RejectionReason = rejectedReason;
                //candidateEntity.Commission = 0;

                return _recruitmentContext.SaveChanges() > 0;
            }

            return false;
        }

        public List<VwJobCandidateSubmissionHistory> GetCandidateStatusHistoryByUuId(int languageId, string candidateUuid)
        {
            try
            {
                var candidateHistory = _recruitmentContext.VwJobCandidateSubmissionHistories.Where(o=>o.CandidateUuId == candidateUuid && o.LanguageId  == languageId).ToList();
                return candidateHistory;
            }
            catch (System.Exception e)
            {
                LogException(e);
                return null;
            }
        }


        #endregion
    }
}
