﻿using Data.Tables;
using Data.Views;
using Repositories.Enums;
using Repositories.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Repositories.Implementation
{
    public class EmployerProfileRepository : BaseRepository<EmployerDetail>, IEmployerProfileRepository
    {
        #region Fieids
        private readonly recruitmentContext _recruitmentContext;
        #endregion

        #region Constructors
        public EmployerProfileRepository(recruitmentContext context) : base(context)
        {
            _recruitmentContext = context;
        }
        #endregion

        #region Methods

        public bool UpdateEmployerDetails(string uuid, string companyName, string fName, 
            string lname, string phone, string email)
        {
            try
            {
                var currentUser = _recruitmentContext.Users.FirstOrDefault(m=> m.Uuid == uuid);
                currentUser.FirstName = fName;
                currentUser.LastName = lname;
                currentUser.PhoneNumber = phone;
                currentUser.Email = email;

                _recruitmentContext.SaveChanges();
                
                var currentEmpoyer = _recruitmentContext.EmployerDetails
                       .FirstOrDefault(m => m.UserId == currentUser.Id);
                currentEmpoyer.CompanyName = companyName;
                _recruitmentContext.SaveChanges();
                 return true;

            }
            catch (System.Exception e)
            {
                LogException(e);
                return false;
            }
        }

        public VwEmployer GetEmployerDetails(string uuid)
        {
            try
            {
                return _recruitmentContext.VwEmployers.
                     FirstOrDefault(m => m.Uuid == uuid);
            }
            catch (System.Exception e)
            {
                LogException(e);
                return null;
            }
        }

        public bool UpdatePackage(string uuid, int packageId)
        {
            try
            {
                var currentUser = _recruitmentContext.Users.FirstOrDefault(m => m.Uuid == uuid);
              
                var currentEmpoyer = _recruitmentContext.EmployerDetails
                       .FirstOrDefault(m => m.UserId == currentUser.Id);
                currentEmpoyer.PackageId = packageId;
                currentEmpoyer.IsMembershipActive = true;
                currentEmpoyer.PackageExpiryDate= System.DateTime.Now.AddYears(1);

                //Insert data in employeer package history
                EmployerPackageHistory employerPackageHistory = new EmployerPackageHistory();
                employerPackageHistory.PackageId = packageId;
                employerPackageHistory.PackagePurchaseDate = DateTime.Now;
                employerPackageHistory.PackageExpiryDate = System.DateTime.Now.AddYears(1);
                employerPackageHistory.UserId = currentEmpoyer.UserId;

                _recruitmentContext.EmployerPackageHistory.Add(employerPackageHistory);

                _recruitmentContext.SaveChanges();

                return true;

            }
            catch (System.Exception e)
            {
                LogException(e);
                return false;
            }
        }

        #endregion
    }
}
