﻿using Data.Tables;
using Data.Views;
using Repositories.Enums;
using Repositories.Interfaces;
using System.Collections.Generic;
using System.Linq;

namespace Repositories.Implementation
{
    public class EmployerRepository : BaseRepository<EmployerDetail>, IEmployerRepository
    {
        #region Fieids
        private readonly recruitmentContext _recruitmentContext;
        #endregion

        #region Constructors
        public EmployerRepository(recruitmentContext context) : base(context)
        {
            _recruitmentContext = context;
        }
        #endregion

        #region Methods

        public EmployerDetails AddEmployer(EmployerDetails employerDetails)
        {
            try
            {
                _recruitmentContext.Users.Add(employerDetails.User);
                if (_recruitmentContext.SaveChanges() > 0)
                {
                    employerDetails.EmployerDetail.UserId = employerDetails.User.Id;
                    _recruitmentContext.EmployerDetails.Add(employerDetails.EmployerDetail);
                    if (_recruitmentContext.SaveChanges() > 0)
                        return employerDetails;
                }

                return null;
            }
            catch (System.Exception e)
            {
                LogException(e);
                return null;
            }
        }

        public IEnumerable<VwEmployer> GetAllEmployers(int userStatusId)
        {
            try
            {
                var employers = _recruitmentContext.VwEmployers
                    .Where(o=>o.UserStatusId == userStatusId)
                    .OrderByDescending(m => m.EmployerId);
                return employers;
            }
            catch (System.Exception e)
            {
                LogException(e);
                return null;
            }
        }

        public bool AcceptEmployer(string Uuid)
        {
            var userEntity = _recruitmentContext.Users.FirstOrDefault(U => U.Uuid == Uuid);
            if (userEntity != null)
            {
                userEntity.Accepted = true;
                userEntity.UserStatusId = (int)UserStatusEnum.Approved;
                userEntity.RejectedReason = null;

                return _recruitmentContext.SaveChanges() > 0;
            }

            return false;

        }

        public bool DeclineEmployer(string Uuid, string rejectedReason)
        {
            var userEntity = _recruitmentContext.Users.FirstOrDefault(U => U.Uuid == Uuid);
            if (userEntity != null)
            {
                userEntity.Accepted = false;
                userEntity.UserStatusId = (int)UserStatusEnum.Decline;
                userEntity.RejectedReason = rejectedReason;

                return _recruitmentContext.SaveChanges() > 0;
            }

            return false;
        }

        #endregion
    }
}
