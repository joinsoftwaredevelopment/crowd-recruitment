﻿using Data.Tables;
using Data.Views;
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using Repositories.Enums;
using Repositories.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Repositories.Implementation
{
    public class PackagesRepository : BaseRepository<Package>, IPackagesRepository
    {
        #region Fieids
        private readonly recruitmentContext _recruitmentContext;
        #endregion

        #region Constructors
        public PackagesRepository(recruitmentContext context) : base(context)
        {
            _recruitmentContext = context;
        }
        #endregion

        #region Methods
        public Package AddPackage(string title, decimal price, List<PackagesFeature> packagesFeatures)
        {
            try
            {
                Package package = new Package()
                {
                    PackageTitle = title,
                    PackagePrice = price,
                    PackageDescription = "No description yet"
                };
                _recruitmentContext.Packages.Add(package);
                _recruitmentContext.SaveChanges();

                foreach (var item in packagesFeatures)
                {
                    item.PackageId = package.PackageId;
                    _recruitmentContext.PackagesFeatures.Add(item);
                    _recruitmentContext.SaveChanges();
                }


                return package;
            }
            catch (System.Exception e)
            {
                LogException(e);
                return null;
            }
            
        }

        public PackageDetails GetPackageById(int packageId)
        {
            try
            {
                PackageDetails packageDetails = new PackageDetails();
                var package = _recruitmentContext.Packages.FirstOrDefault(m => m.PackageId == packageId);
                List<PackagesFeature> packagesFeature = new List<PackagesFeature>();
                packagesFeature = _recruitmentContext.PackagesFeatures.Where(m => m.PackageId == packageId).ToList();

                packageDetails.packageId = package.PackageId;
                packageDetails.packagePrice = package.PackagePrice;
                packageDetails.packageTitle = package.PackageTitle;
                packageDetails.packagesFeatures = packagesFeature;
                return packageDetails;
            }
            catch (System.Exception e)
            {
                LogException(e);
                return null;
            }
        }

        public Package EditPackage(int packageId,string title, decimal price, List<PackagesFeature> packagesFeatures)
        {
            try
            {
                var currentPackage = _recruitmentContext.Packages.
                    FirstOrDefault(m => m.PackageId == packageId);
                currentPackage.PackageTitle = title;
                currentPackage.PackagePrice = price;

                _recruitmentContext.SaveChanges();

                var currentFeatures = _recruitmentContext.PackagesFeatures.Where(m => m.PackageId == packageId);
                if (currentFeatures != null)
                {
                    foreach (var item in currentFeatures)
                    {
                        _recruitmentContext.PackagesFeatures.Remove(item);
                    }

                    _recruitmentContext.SaveChanges();
                }

                foreach (var item in packagesFeatures)
                {
                    item.PackageId = packageId;
                    item.Id = 0;
                    _recruitmentContext.PackagesFeatures.Add(item);
                    _recruitmentContext.SaveChanges();
                }

                return currentPackage;
            }
            catch (System.Exception e)
            {
                LogException(e);
                return null;
            }

        }

        public bool DeletePackage(int packageId)
        {
            try
            {
                
                var currentFeatures = _recruitmentContext.PackagesFeatures.Where(m => m.PackageId == packageId);
                if (currentFeatures != null)
                {
                    foreach (var item in currentFeatures)
                    {
                        _recruitmentContext.PackagesFeatures.Remove(item);
                    }

                    _recruitmentContext.SaveChanges();
                }

                var currentPackage = _recruitmentContext.Packages.
                    FirstOrDefault(m => m.PackageId == packageId);
                _recruitmentContext.Packages.Remove(currentPackage);

                _recruitmentContext.SaveChanges();

                return true;
            }
            catch (System.Exception e)
            {
                LogException(e);
                return false;
            }
        }

        public AllPackagesDetails GetAllPackages()
        {
            try
            {
                var packages = _recruitmentContext.Packages.ToList();
                var packagesFeatures = _recruitmentContext.PackagesFeatures.ToList();

                AllPackagesDetails allPackagesDetails = new AllPackagesDetails();
                allPackagesDetails.packages = packages;
                allPackagesDetails.packagesFeatures = packagesFeatures;

                return allPackagesDetails;
            }
            catch (System.Exception e)
            {
                LogException(e);
                return null;
            }
        }
        #endregion
    }

}
