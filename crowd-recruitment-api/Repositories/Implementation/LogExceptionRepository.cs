﻿using Data.Tables;
using Repositories.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace Repositories.Implementation
{
    public class LogExceptionRepository : BaseRepository<LogException> , ILogExceptionRepository
    {
        #region Fields

        private readonly recruitmentContext _recruitmentContext;

        #endregion

        #region Constructors

        public LogExceptionRepository(recruitmentContext context) : base(context)
        {
            _recruitmentContext = context;
        }

        #endregion
    }
}
