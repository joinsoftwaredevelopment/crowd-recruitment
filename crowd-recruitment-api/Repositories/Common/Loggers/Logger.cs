﻿using Data.Tables;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
namespace Repositories.Common.Loggers
{
    public class Logger
    {

        #region SingletonImplementation        
        private Logger()
        {

        }
        public static Logger Instance => Nested.Instance;
        private static class Nested
        {
            static Nested()
            {
                Instance = new Logger();
            }
            public static Logger Instance { get; private set; }
        }
        #endregion

        #region Methods
        public int Log(System.Exception exception)
        {
            var errorObject = new LogException();
            errorObject.AssemblyName = exception.TargetSite.Module.Assembly.FullName;
            errorObject.ObjectName = exception.TargetSite.DeclaringType.Name;
            errorObject.MethodName = exception.TargetSite.Name;
            errorObject.LogDate =DateTime.Now;
            errorObject.ErrorMessage = exception.Message;
            errorObject.InnerException = exception.InnerException?.Message;
            return LogException(errorObject);
        }

        public int LogMessage(string message)
        {
            var errorObject = new LogException();
            errorObject.AssemblyName = "test";
            errorObject.ObjectName = "test";
            errorObject.MethodName = "test";
            errorObject.LogDate = DateTime.Now;
            errorObject.ErrorMessage = message;
            errorObject.InnerException = message;
            return LogException(errorObject);
        }
        
        private int LogException(LogException errorObject)
        {
            using (var context = new recruitmentContext())
            {
                context.LogExceptions.Add(errorObject);
                var result = context.SaveChanges();
                return result != 0 ? result : 0;
            }
        }
        #endregion
    }
}
