﻿using Data.Tables;
using Data.Views;
using System;
using System.Collections.Generic;
using System.Text;

namespace Repositories.Interfaces
{
    public interface IUserRepository : IBaseRepository<User>
    {
        #region Methods
        bool IsPhoneExistBefore(string phoneNumber, string uuId);
        bool IsEmailExistBefore(string email, string uuId);
        User LogIn(string PhoneNumber, int roleId);
        User GetUserByUuId(string uuId);
        User GetUserById(int userId);
        User GetUserByEmail(string email);

        bool BookMeeting(BookMeeting bookMeeting);
        User GetAdminInfo();
        Interviewer GetInterviewerById(int interviewerId);

        List<VwCountry> GetAllCountries(int languageId);
        List<VwCurrency> GetAllCurrencies(int languageId);
        bool ResePassword(string password, string uuid);
        User VerifyUserEmailByUuId(string uuId);
        bool ChangeEmail(string uuId, string Email);

        bool IsEmployerMembershipActive(int employeerId);
        #endregion

    }

}
