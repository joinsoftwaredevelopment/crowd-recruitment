﻿using Data.Tables;
using Data.Views;
using System;
using System.Collections.Generic;
using System.Text;

namespace Repositories.Interfaces
{
    public interface IPackagesRepository : IBaseRepository<Package>
    {
        #region Methods
        Package AddPackage(string title, decimal price, List<PackagesFeature> packagesFeatures);
        PackageDetails GetPackageById(int packageId);
        Package EditPackage(int packageId,string title, decimal price, List<PackagesFeature> packagesFeatures);
        bool DeletePackage(int packageId);
        AllPackagesDetails GetAllPackages();
        #endregion

    }

}
