﻿using System;
using System.Collections.Generic;
using System.Text;
using Data.Tables;
using Data.Views;

namespace Repositories.Interfaces
{
    public interface ISourcerProfileRepository : IBaseRepository<SourcerDetail>
    {
        #region Methods
        bool UpdateBasicInfo(string uuid,string jobTitle, string fName, string lname,
            string phone, string email);
        bool UpdateCareerInfo(string uuid, int experienceTypeId,
                    bool hasAccessToDifferentWebsite, int industryId);

        VwSourcer GetBasicInfo(string uuid);

        #endregion
    }
}
