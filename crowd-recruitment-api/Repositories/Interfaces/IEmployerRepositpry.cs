﻿using System;
using System.Collections.Generic;
using System.Text;
using Data.Tables;
using Data.Views;

namespace Repositories.Interfaces
{
    public interface IEmployerRepository : IBaseRepository<EmployerDetail>
    {
        #region Methods
        EmployerDetails AddEmployer(EmployerDetails employerDetails );
        IEnumerable<VwEmployer> GetAllEmployers(int userStatusId);
        bool AcceptEmployer(string Uuid);
        bool DeclineEmployer(string Uuid, string rejectedReason);

        #endregion
    }
}
