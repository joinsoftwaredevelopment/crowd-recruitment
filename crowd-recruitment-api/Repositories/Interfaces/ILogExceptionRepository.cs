﻿using Data.Tables;
using System;
using System.Collections.Generic;
using System.Text;

namespace Repositories.Interfaces
{
    public interface ILogExceptionRepository : IBaseRepository<LogException>
    {

    }
}
