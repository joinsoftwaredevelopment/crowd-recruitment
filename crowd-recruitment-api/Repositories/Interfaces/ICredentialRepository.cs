﻿using Data.Tables;
using System;
using System.Collections.Generic;
using System.Text;

namespace Repositories.Interfaces
{
    public interface ICredentialRepository : IBaseRepository<Credential>
    {
        #region Methods
        Credential GetCredentials();

        #endregion

    }

}
