﻿using System;
using System.Collections.Generic;
using System.Text;
using Data.Tables;
using Data.Views;

namespace Repositories.Interfaces
{
    public interface IEmployerProfileRepository : IBaseRepository<EmployerDetail>
    {
        #region Methods
        bool UpdateEmployerDetails(string uuid,string companyName, string fName, string lname,
            string phone, string email);
        VwEmployer GetEmployerDetails(string uuid);
        bool UpdatePackage(string uuid, int packageId);

        #endregion
    }
}
