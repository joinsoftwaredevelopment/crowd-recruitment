﻿using System.Collections.Generic;
using Data.Tables;

namespace Repositories.Interfaces
{
    public interface IEmployerPackageHistoryRepository : IBaseRepository<EmployerPackageHistory>
    {
        #region Methods
        IEnumerable<VwEmployerPackageHistory> GetAllPackagesHistory(int userId);

        #endregion
    }
}
