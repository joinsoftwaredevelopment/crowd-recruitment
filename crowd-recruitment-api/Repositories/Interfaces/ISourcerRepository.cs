﻿using Data.Tables;
using Data.Views;
using System;
using System.Collections.Generic;
using System.Text;

namespace Repositories.Interfaces
{
    public interface ISourcerRepository : IBaseRepository<SourcerDetail>
    {
        #region Methods
         SourcerDetail GetSourcerByUuId(string uuid);
        int GetSourcerExperienceId(string uuid);
        bool UpdateSourcerExperience(string uuid, int? experienceLevelId);
        string GetJobtitleName(string uuid);
        bool UpdateSourcerJobtitle(string uuid, string jobTitle);
        bool GetSourcerhasAccessToWebsite(string uuid);
        bool UpdateSourcerWebsites(string uuid, bool? hasAccessToDifferentWebsite);
        int GetSourcerIndustryId(string uuid);
        bool UpdateSourcerIndustry(string uuid, int? industryId);
        bool UpdateSourcerCV(string uuid, string cvName , string linkedInProfile);

        // Soucer Acceptance
        IEnumerable<VwSourcer> GetAllSourcers(int userStatusId);
        Sourcer GetAllSourcerInfo(string uuid);
        bool AcceptSourcer(string Uuid);
        bool DeclineSourcer(string Uuid, string rejectedReason);
        IEnumerable<VwSourcerBalance> GetSourcerBalances(string uuid);
        VwSourcerBalancesSummary GetSourcerBalancesSummary(string uuid);

        #endregion
    }

}
