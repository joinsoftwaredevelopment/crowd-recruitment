﻿using Data.Tables;
using System;
using System.Collections.Generic;
using System.Text;

namespace Repositories.Interfaces
{
    public interface IAdminRepository : IBaseRepository<User>
    {
        #region Methods
        User LogIn(string PhoneNumber);
        IEnumerable<VwAdminSourcersBalance> GetVwAdminSourcersBalance();
        IEnumerable<VwJobSourcer> GetJobSourcers(string jobUuId);
        IEnumerable<SourcerBalancesHistory> GetVwAdminSourcersBalance(DateTime startDate, DateTime endDate);
        public bool UpdateAdminSourcersBalance(DateTime startDate, DateTime endDate, int sourcerId);

        public void UpdateEmployerPackages();
        #endregion

    }

}
