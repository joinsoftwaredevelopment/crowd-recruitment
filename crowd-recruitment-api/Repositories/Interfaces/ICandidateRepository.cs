﻿using Data.Tables;
using Data.Views;
using System;
using System.Collections.Generic;
using System.Text;

namespace Repositories.Interfaces
{
    public interface ICandidateRepository : IBaseRepository<VwJobCandidate>
    {
        #region Methods

        List<VwJobCandidate> GetJobCandidates(int languageId, string jobUuid);
        List<VwJobCandidate> GetAcceptedJobCandidates(int languageId, string jobUuid);
        VwJobCandidate GetJobCandidateByUuId(int languageId, string candidateUuid);
        List<VwJobCandidate> GetSourcerCandidateBySourcerUuId(int languageId, string sourcerUuId, string jobUuId);
        bool AcceptCandidate(string Uuid);
        bool UpdateHiringStage(string submissionUuid, int hiringStageId);
        bool UpdateIsAcceptOffer(string submissionUuidUuid, bool isOfferAccepted);
        bool UpdateIsHired(string submissionUuidUuid, bool isCandidateHired);
        bool UpdateCandidateCvStatus(string submissionUuid, bool isCvApproved);
        bool DeclineCandidate(string Uuid, string rejectedReason);

        bool EmployerAcceptCandidate(string Uuid);
        bool EmployerDeclineCandidate(string Uuid, string rejectedReason);

        List<VwJobCandidate> GetSourcerCandidateBySubmissionStatusId(int languageId, string sourcerUuId , int submissionStatusId, string jobUuId);

        List<VwJobCandidateSubmissionHistory> GetCandidateStatusHistoryByUuId(int languageId, string candidateUuid);

        #endregion
    }
}
