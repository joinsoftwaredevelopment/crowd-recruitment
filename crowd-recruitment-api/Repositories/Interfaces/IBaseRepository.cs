﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Repositories.Interfaces
{
    public interface IBaseRepository<TEntity> where TEntity : class
    {
        #region Fields

        TEntity CurrentModel { get; set; }

        #endregion

        #region Methods

        int LogException(Exception exception );

        TEntity Add(TEntity entity);
        bool Update(TEntity entity);
        bool Delete(string uUId);

        Task<TEntity> AddAsync(TEntity entity);
        Task<TEntity> UpdateAsync(TEntity entity);

        TEntity GetById(long id);
        IQueryable<TEntity> GetAll();
        IQueryable<TEntity> FindBy(Expression<Func<TEntity, bool>> predicate);
        TEntity FindFirstBy(Expression<Func<TEntity, bool>> predicate);

        #endregion
    }
}
