﻿using Data.Tables;
using Data.Views;
using System;
using System.Collections.Generic;
using System.Text;

namespace Repositories.Interfaces
{
    public interface IJobRepository : IBaseRepository<User>
    {
        #region Methods
        JobDetailsInfo GetJobDetailsInfo(string uuid,int? tempId);
        JoObjectiveInfo GetJobInstructionsInfo(string uuid);
        IEnumerable<ExperienceLevelTranslation> GetJobInstructionsInfo();
        JobEngagementInfo GetJobEngagementsInfo(string uuid);
        
        Job GetJobByUuid(string uuid);
        bool CreateJob(Job job);
        bool UpdateJob(string uuId, int EmployerTypeId,
                int industryId, string JobDescription,
                int jobLocationId, int JobNationalityId,
                string JobTitle, int SeniorityLevelId, string requirements
                );
        bool UpdateJobInstructions(string uuid,int experienceLevelId,
            List<mustHaveQualification> mustHaveQualifications,
                List<niceHaveQualification> niceHaveQualification,
                List<notToSource> notToSource, int currencyId, int minSalary, int maxSalary);

        bool UpdateJobObjectives(string uuid,bool HiringNeeds,
                int PeopleToHireCount, int whenYouNeedToHire, bool sourcingOutsideOfJoin,
                string FindingJobDifficultyLevelId);

        bool UpdateJobEngagement(string uuid,string firstMailSubject,
                string firstMailDescription, int statusId, int engagementTemplateId);

        List<VwJob> GetJobSummaries(int languageId, int sourcerId);
        List<VwJob> GetJobSummariesByEmployer(int languageId, string employeruuid);
        List<VwJob> GetAllJobs(int languageId, string employeruuid);
        List<VwJob> GetAllJobs(int statusId);
        List<VwJob> GetJobDraftsByEmployer(int languageId, string employeruuid);
        List<VwJob> GetJobClosedByEmployer(int languageId, string employeruuid);
        bool CloseJob(string uuid);
        bool DeleteJob(string uuid);
        List<VwJobSubmissionsSummary> GetSourcerJobSummaries(int languageId, int sourcerId, bool isActive);

        List<VwSourcerSubmission> GetSourcerJobSubmissions(int languageId, int sourcerId, int jobId);

        Job GetJobBuUuId(string uuId);

        VwJob GetVwJobByUuId(string uuId, int languageId);

        JobCandidateSubmission AddSourcerSubmission(JobCandidateSubmission jobCandidateSubmission);

        bool AcceptJob(string uuid, int numberOfSubmissions, decimal commission);
        bool DeclineJob(string uuid, string rejectionReason);
        List<CompanyIndustryTranslation> GetAllIndustries();
        List<VwJobTemplate> GetTempByIndustryId(int industryId);
        VwJobTemplate GetJobTempById(int templateId);
        List<VwJobTemplate> GetAllJobTemps();
        void InsertJobTemplateSkills(int jobTemplateId, int jobId);

        List<BasicModel> GetDifficultyLevel(int languageId);
        List<BasicModel> GettimetoHireName(int languageId);

        string GetDifficultyLevelById(int id , int languageId);
        string GettimetoHireNameById(int id , int languageId);

        #endregion
    }
}
