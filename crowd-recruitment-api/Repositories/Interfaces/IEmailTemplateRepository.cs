﻿using Data.Tables;
using System;
using System.Collections.Generic;
using System.Text;

namespace Repositories.Interfaces
{
    public interface IEmailTemplateRepository : IBaseRepository<EmailsTemplate>
    {
        EmailsTemplate GetEmailsTemplate(int templateId);
        List<EmailsTemplate> GetJobEmailsTemplate();
    }
}
