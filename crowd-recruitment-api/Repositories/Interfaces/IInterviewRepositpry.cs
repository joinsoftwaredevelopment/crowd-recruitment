﻿using System;
using System.Collections.Generic;
using System.Text;
using Data.Tables;
using Data.Views;

namespace Repositories.Interfaces
{
    public interface IInterviewRepositpry : IBaseRepository<CandidatesInterview>
    {
        #region Methods
        int AddInterviewDetails(CandidatesInterview candidatesInterview, string employerUuid);
        VwJobCandidate GetCandidateBySubmissionUuid(string submissionUuid);
        int UpdateInterviewDetails(CandidatesInterview candidatesInterview);
        int AddInterviewEmails(CandidatesInterview candidatesInterview);
        int CancelInterview(int interviewId,bool isCanceled);
        List<Interviewer> GetAllInterviewers(int languageId);
        VwInterviewDetail GetInterviewDetailsById(int interviewId);
        List<VwCandidatesInterview> GetAllInterviews();
        List<VwCandidatesInterview> GetAllInterviewsByUuid(string uuid);
        VwInterviewEmail GetInterviewEmailsById(int interviewId);
        List<EmailsTemplate> GetInterviewTemplates();
        List<EmailsTemplate> GetOfferTemplates();
        EmailsTemplate GetInterviewTempById(int templateId);
        VwCandidatesInterview GetInterviewEmailDetails(int interviewId);

        #endregion
    }
}
