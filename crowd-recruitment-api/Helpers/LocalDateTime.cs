﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;

namespace Helpers
{
    public static class LocalDateTime
    {
        public static string areaTimeZone = "Egypt Standard Time";

        public static DateTime GetDateTimeNow()
        {
            //Get Time Zone Info by Id to convert Current Time to it
            TimeZoneInfo timeZone = TimeZoneInfo.FindSystemTimeZoneById(areaTimeZone);

            //Get Current Date Time and Convert it to selected time zone
            //to get current time to server time
            DateTime dateTime = DateTime.Now;
            return TimeZoneInfo.ConvertTime(dateTime, timeZone);
        }

        public static DateTime GetServerDateTime(DateTime dateTime)
        {
            //Get Time Zone Info by Id to convert Current Time to it
            TimeZoneInfo timeZone = TimeZoneInfo.FindSystemTimeZoneById(areaTimeZone);

            //Convert dateTime to selected time zone
            //to get current time to server time
            return TimeZoneInfo.ConvertTime(dateTime, timeZone);
        }
    }

}
