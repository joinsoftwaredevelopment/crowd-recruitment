﻿using Microsoft.AspNetCore.Cryptography.KeyDerivation;
using PhoneNumbers;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Net.Mail;
using System.Security.Cryptography;
using System.Text;

namespace Helpers
{
    public static class GeneralFunctions
    {
        /// <summary>
        /// Given a single email address, return the email address if it is valid, or empty string if invalid.
        /// </summary>
        /// <param name="emailAddess"></param>
        /// <returns>Validated email address(</returns>  
        public static bool IsEmailValid(string emailaddress)
        {
            try
            {
                MailAddress mail = new MailAddress(emailaddress);
                return mail != null;
            }
            catch (FormatException)
            {
                return false;
            }
        }

        public static bool IsPhoneNumberValid(string phoneNumber)
        {
            try
            {
                var regionCode = RegionInfo.CurrentRegion.TwoLetterISORegionName;
                PhoneNumberUtil phoneNumberUtil = PhoneNumberUtil.GetInstance();
                var phone = phoneNumberUtil.Parse(phoneNumber, regionCode);
                bool isValid = phoneNumberUtil.IsValidNumber(phone); // returns true

                return isValid;
            }
            catch (Exception e)
            {
                return false;
            }
        }

        public static string HashPassword(string input)
        {
            var salt = GenerateSalt(16);

            // 500 (number of iterattions)
            var bytes = KeyDerivation.Pbkdf2(input, salt, KeyDerivationPrf.HMACSHA512, 500, 16);

            return $"{ Convert.ToBase64String(salt) }:{ Convert.ToBase64String(bytes) }";
        }

        private static byte[] GenerateSalt(int length)
        {
            var salt = new byte[length];

            using (var random = RandomNumberGenerator.Create())
            {
                random.GetBytes(salt);
            }

            return salt;
        }

        public static bool CheckMatch(string hash, string input)
        {
            try
            {
                var parts = hash.Split(':');

                var salt = Convert.FromBase64String(parts[0]);

                var bytes = KeyDerivation.Pbkdf2(input, salt, KeyDerivationPrf.HMACSHA512, 500, 16);

                return parts[1].Equals(Convert.ToBase64String(bytes));
            }
            catch
            {
                return false;
            }
        }
    }
}
