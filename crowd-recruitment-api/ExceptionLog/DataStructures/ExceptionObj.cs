﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.ExceptionLog.DataStructures
{
    public class ExceptionObj
    {
        #region Constructors        
        public ExceptionObj() { }
        public ExceptionObj(
            long Id, 
            DateTime logDate, 
            string assemblyName, 
            string objectName, 
            string methodName, 
            string innerException,
            string helpLink,
            string errorMessage, 
            Nullable<bool> @fixed, 
            Nullable<DateTime> fixDate)
        {
            this.Id = Id;
            this.logDate = logDate;
            this.assemblyName = assemblyName;
            this.objectName = objectName;
            this.methodName = methodName;
            this.innerException = innerException;
            this.helpLink = helpLink;
            this.errorMessage = errorMessage;
            this.@fixed = @fixed;
            this.fixDate = fixDate;
        }
        #endregion

        #region Properties        
        public long Id { get; set; }
        public System.DateTime logDate { get; set; }
        public string assemblyName { get; set; }
        public string objectName { get; set; }
        public string methodName { get; set; }
        public string innerException { get; set; }
        public string helpLink { get; set; }
        public string errorMessage { get; set; }
        public Nullable<bool> @fixed { get; set; }
        public Nullable<System.DateTime> fixDate { get; set; }
        #endregion
    }
}
