﻿
using Common.ExceptionLog.DataStructures;
using Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
namespace Common.ExceptionLog.Loggers
{
    public class Logger
    {
        #region SingletonImplementation        
        private Logger()
        {

        }
        public static Logger Instance => Nested.Instance;
        private static class Nested
        {
            static Nested()
            {
                Instance = new Logger();
            }
            public static Logger Instance { get; private set; }
        }
        #endregion

        #region Methods
        public int Log(System.Exception exception)
        {
            var errorObject = new ExceptionObj();
            errorObject.assemblyName = exception.TargetSite.Module.Assembly.FullName;
            errorObject.objectName = exception.TargetSite.DeclaringType.Name;
            errorObject.methodName = exception.TargetSite.Name;
            errorObject.logDate =DateTime.Now;
            errorObject.errorMessage = exception.Message;
            errorObject.helpLink = exception.HelpLink;
            errorObject.innerException = exception.InnerException?.Message;
            return LogException(errorObject);
        }

        public int LogMessage(string message)
        {
            var errorObject = new ExceptionObj();
            errorObject.assemblyName = "test";
            errorObject.objectName = "test";
            errorObject.methodName = "test";
            errorObject.logDate = DateTime.Now;
            errorObject.errorMessage = message;
            errorObject.innerException = message;
            return LogException(errorObject);
        }
        public void FixError(int Id)
        {
            using (var context = new RecruitmentContext())
            {
                context.Configuration.AutoDetectChangesEnabled = false;
                context.Configuration.LazyLoadingEnabled = true;
                context.SpFixException(Id);
            }
        }
        public List<ExceptionObj> GetLetestExceptions()
        {
            var errorList = new List<ExceptionObj>();
            using (var context = new diginEntities())
            {
                context.Configuration.AutoDetectChangesEnabled = false;
                context.Configuration.LazyLoadingEnabled = true;
                var errors = context.SpGetLatestExceptions();
                foreach (var err in errors)
                {
                    errorList.Add(
                     new ExceptionObj(err.id, err.logDate.Value,
                                 err.assemblyName,
                                 err.objectName,
                                 err.methodName,
                                 err.innerException,
                                 err.helpLink,
                                 err.errorMessage,
                                 err.@fixed,
                                 err.fixDate));
                }
            }
            return errorList;
        }
        private int LogException(ExceptionObj errorObject)
        {
            var exceptionId = 0;

            var err = errorObject;
            using (var context = new diginEntities())
            {
                context.Configuration.AutoDetectChangesEnabled = false;
                context.Configuration.LazyLoadingEnabled = true;
                exceptionId = context.SpLogException(err.logDate,
                                 err.assemblyName,
                                 err.objectName,
                                 err.methodName,
                                 err.innerException,
                                 err.helpLink,
                                 err.errorMessage,
                                 false,
                                 null);
            }

            return exceptionId;
        }
        #endregion
    }
}
