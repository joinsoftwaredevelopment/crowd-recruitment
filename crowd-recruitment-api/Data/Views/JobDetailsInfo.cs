﻿using Data.Tables;
using System;
using System.Collections.Generic;
using System.Text;

namespace Data.Views
{
    public class JobDetailsInfo
    {
        //public IEnumerable<SeniorityLevelTranslation> seniorityLevels { get; set; }
        //public IEnumerable<EmploymentTypeTranslation> employmentTypeTranslations { get; set; }
        //public IEnumerable<JobFunctionTranslation> jobFunctionTranslations { get; set; }
        //public IEnumerable<CompanyIndustryTranslation> companyIndustryTranslations { get; set; }
        //public IEnumerable<JobLocationTranslation> jobLocationTranslations { get; set; }
        public IEnumerable<CountriesTranslation> countriesTranslations { get; set; }
        public PostJobCurentDetails job { get; set; }
    }
}
