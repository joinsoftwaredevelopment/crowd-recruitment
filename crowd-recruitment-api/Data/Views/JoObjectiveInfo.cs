﻿using Data.Tables;
using System;
using System.Collections.Generic;
using System.Text;

namespace Data.Views
{
    public class JoObjectiveInfo
    {
        public IEnumerable<EmployerMustHaveQualification> mustQualifications { get; set; }
        public IEnumerable<EmployerNiceHaveQualification> niceQualifications { get; set; }
        public IEnumerable<CompaniesNotToSourceFrom> notToSourceQualifications { get; set; }
        public Job job { get; set; }
    }
}
