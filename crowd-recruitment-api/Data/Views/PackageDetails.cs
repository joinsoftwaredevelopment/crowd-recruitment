﻿using Data.Tables;
using System;
using System.Collections.Generic;
using System.Text;

namespace Data.Views
{
    public class PackageDetails
    {
        public int packageId { get; set; }
        public string packageTitle { get; set; }
        public decimal packagePrice { get; set; }
        public List<PackagesFeature> packagesFeatures { get; set; }
    }
}
