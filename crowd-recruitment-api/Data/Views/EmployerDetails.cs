﻿using Data.Tables;
using System;
using System.Collections.Generic;
using System.Text;

namespace Data.Views
{
    public class EmployerDetails
    {
        public EmployerDetails()
        {
            User = new User();
            EmployerDetail = new EmployerDetail();
        }
        public User User { get; set; }
        public EmployerDetail EmployerDetail { get; set; }
    }
}
