﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Data.Views
{
    public class Employer
    {
        public string uuid { get; set; }
        public DateTime date { get; set; }
        public string companyName { get; set; }
        public string fullName { get; set; }
        public string phoneNumber { get; set; }
        public string emailAddress { get; set; }
    }
}
