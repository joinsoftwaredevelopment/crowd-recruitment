﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Data.Views
{
    public class PostJobCurentDetails
    {
        public string uuId { get; set; }
        public string jobTitle { get; set; }
        public int seniorityLevelId { get; set; }
        public int employerTypeId { get; set; }
        public string jobDescription { get; set; }
        public int industryId { get; set; }
        public int jobLocationId { get; set; }
        public int jobNationalityId { get; set; }
        public string requirements { get; set; }
    }
}
