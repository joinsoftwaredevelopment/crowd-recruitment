﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Data.Views
{
    public class BookMeeting
    {
        public string uuid { get; set; }
        public string timePeriodId { get; set; }
        public int timeZoneId { get; set; }
        public string timeMettingId { get; set; }
        public int interviewerId { get; set; }
        public DateTime day { get; set; }
    }
}
