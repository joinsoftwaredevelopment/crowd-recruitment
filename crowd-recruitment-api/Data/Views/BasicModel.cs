﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Data.Views
{
    public  class BasicModel
    {
        public int Id { set; get; }
        public string  NameEn { set; get; }
        public string NameAr { set; get; }
    }
}
