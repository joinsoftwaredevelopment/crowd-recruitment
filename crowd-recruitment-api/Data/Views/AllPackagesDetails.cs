﻿using Data.Tables;
using System;
using System.Collections.Generic;
using System.Text;

namespace Data.Views
{
    public class AllPackagesDetails
    {
        public List<Package> packages { get; set; }
        public List<PackagesFeature> packagesFeatures { get; set; }
    }
}
