﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Data.Views
{
    public class CurrentEntity<TEntity>
    {
        //data
        public TEntity Entity { get; set; }
        //message
        public string ValidationMessage { get; set; }
        //code
        public int Code { get; set; }

    }
}
