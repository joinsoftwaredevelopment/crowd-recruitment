﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Data.Views
{
    public class Sourcer
    {
        public string uuid { get; set; }
        public DateTime date { get; set; }
        public string fullName { get; set; }
        public string phoneNumber { get; set; }
        public string emailAddress { get; set; }
        public string experience { get; set; }
        public string jobTitle { get; set; }
        public bool haveAccessToDifferentWebsite { get; set; }
        public string IndustryYouInterested { get; set; }
        public string CvPath { get; set; }
        public string LinkedInProfile { get; set; }
        public string RejectedReason { get; set; }
        public int UserStatusId { get; set; }
        public int registerationStep { get; set; }
    }
}
