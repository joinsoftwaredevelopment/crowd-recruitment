﻿using Data.Tables;
using System;
using System.Collections.Generic;
using System.Text;

namespace Data.Views
{
    public class JobEngagementInfo
    {
        public IEnumerable<EmailsTemplate> templates { get; set; }
        public Job job { get; set; }
        public bool isMembershipActive { get; set; } 
    }
}
