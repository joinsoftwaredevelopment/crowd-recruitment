﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Data.Tables
{
    public partial class VwCurrency
    {
        public int CurrencyId { get; set; }
        public string ShortName { get; set; }
        public string CurrencyName { get; set; }
        public int LanguageId { get; set; }
    }
}
