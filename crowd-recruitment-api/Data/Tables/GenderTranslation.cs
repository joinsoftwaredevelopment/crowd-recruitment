﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Data.Tables
{
    public partial class GenderTranslation
    {
        public int Id { get; set; }
        public string Translation { get; set; }
        public int? GenderId { get; set; }
        public int? LanguageId { get; set; }
    }
}
