﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Data.Tables
{
    public partial class VwJobSubmissionsSummary
    {
        public int JobId { get; set; }
        public string Uuid { get; set; }
        public int? JobStatusId { get; set; }
        public string JobTitle { get; set; }
        public bool IsActive { get; set; }
        public int? RemainingCount { get; set; }
        public int LanguageId { get; set; }
        public string JobLocationTranslation { get; set; }
        public int? SourcerId { get; set; }
        public int EmployerId { get; set; }
        public string EmployerUuid { get; set; }
        public string JobDate { get; set; }
        public string Description { get; set; }
        public string CompanyIndustry { get; set; }
        public string EmployerFname { get; set; }
        public string EmployerLname { get; set; }
        public string Reason { get; set; }
        public decimal? Commission { get; set; }
        public string JobStatusName { get; set; }
        public int? SubmissionCount { get; set; }
        public int? NoOfSubmissions { get; set; }
        public DateTime? CreatedDate { get; set; }
    }
}
