﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Data.Tables
{
    public partial class EmailsTemplatesType
    {
        public int Id { get; set; }
        public string NameAr { get; set; }
        public string NameEn { get; set; }
    }
}
