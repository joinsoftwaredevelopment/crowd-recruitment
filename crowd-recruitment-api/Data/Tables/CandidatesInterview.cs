﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Data.Tables
{
    public partial class CandidatesInterview
    {
        public int Id { get; set; }
        public int CandidateId { get; set; }
        public int InterviewerId { get; set; }
        public string InterviewComment { get; set; }
        public int InterviewTypeId { get; set; }
        public DateTime InterviewDate { get; set; }
        public TimeSpan InterviewTime { get; set; }
        public int Duration { get; set; }
        public int? TemplateId { get; set; }
        public string EmailSubject { get; set; }
        public string EmailDescription { get; set; }
        public bool IsCanceled { get; set; }
        public bool IsEmailSent { get; set; }
        public int CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public int? InterviewerTypeId { get; set; }
    }
}
