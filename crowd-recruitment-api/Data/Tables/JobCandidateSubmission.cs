﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Data.Tables
{
    public partial class JobCandidateSubmission
    {
        public int Id { get; set; }
        public string Uuid { get; set; }
        public int JobId { get; set; }
        public string CandidateName { get; set; }
        public string CvName { get; set; }
        public DateTime SubmissionDate { get; set; }
        public int SourcerId { get; set; }
        public int CandidateSubmissionStatusId { get; set; }
        public string LinkedInProfile { get; set; }
        public int? LocationId { get; set; }
        public bool IsDeleted { get; set; }
        public decimal? Commission { get; set; }
        public string RejectionReason { get; set; }
        public int? EmpSubmissionStatusId { get; set; }
        public string EmpReason { get; set; }
        public string Email { get; set; }
        public int? HiringStageId { get; set; }
        public bool? IsAcceptOffer { get; set; }
        public bool? IsHired { get; set; }
        public bool? IsCvApproved { get; set; }
    }
}
