﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Data.Tables
{
    public partial class JobLocationDetail
    {
        public int Id { get; set; }
        public int JobId { get; set; }
        public int? SourcerCityLocationId { get; set; }
        public int? SourcerCountryLocationId { get; set; }
        public int? RemoteTypeId { get; set; }
        public int? TimeZoneFromId { get; set; }
        public int? TimeZoneToId { get; set; }
        public string Other { get; set; }

        public virtual RemoteType RemoteType { get; set; }
    }
}
