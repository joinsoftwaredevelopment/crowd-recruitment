﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Data.Tables
{
    public partial class VwJobCandidateSubmissionHistory
    {
        public int CandidateSubmissionStatusId { get; set; }
        public string CandidateUuId { get; set; }
        public string StatusName { get; set; }
        public int LanguageId { get; set; }
        public DateTime CreateDate { get; set; }
        public string RejectionReason { get; set; }
    }
}
