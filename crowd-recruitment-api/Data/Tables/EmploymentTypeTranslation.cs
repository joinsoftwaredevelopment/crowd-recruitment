﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Data.Tables
{
    public partial class EmploymentTypeTranslation
    {
        public int Id { get; set; }
        public int EmploymentTypeId { get; set; }
        public string Translation { get; set; }
        public int LanguageId { get; set; }
    }
}
