﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Data.Tables
{
    public partial class Currency
    {
        public Currency()
        {
            CurrencyTranslations = new HashSet<CurrencyTranslation>();
        }

        public int Id { get; set; }
        public DateTime CreatedDate { get; set; }

        public virtual ICollection<CurrencyTranslation> CurrencyTranslations { get; set; }
    }
}
