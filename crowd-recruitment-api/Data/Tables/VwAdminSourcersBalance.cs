﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Data.Tables
{
    public partial class VwAdminSourcersBalance
    {
        public int SourcerId { get; set; }
        public string FullName { get; set; }
        public int? JobCount { get; set; }
        public int? SubmissionCount { get; set; }
        public decimal? TotalBalance { get; set; }
        public int? AcceptanceRatio { get; set; }
        public int? AcceptanceCount { get; set; }
        public int? DeclinedCount { get; set; }
        public bool? IsPaid { get; set; }
    }
}
