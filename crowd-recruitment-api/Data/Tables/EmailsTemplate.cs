﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Data.Tables
{
    public partial class EmailsTemplate
    {
        public int Id { get; set; }
        public string EmailTitleEn { get; set; }
        public string EmailTitleAr { get; set; }
        public string EmailbodyEn { get; set; }
        public string EmailbodyAr { get; set; }
        public int? EmailTemplateType { get; set; }
    }
}
