﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Data.Tables
{
    public partial class LanguagesTranslation
    {
        public int Id { get; set; }
        public int LanguageId { get; set; }
        public string Name { get; set; }
    }
}
