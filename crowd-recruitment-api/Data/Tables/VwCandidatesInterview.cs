﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Data.Tables
{
    public partial class VwCandidatesInterview
    {
        public int Id { get; set; }
        public string CandidateName { get; set; }
        public string CandidateEmail { get; set; }
        public DateTime InterviewDate { get; set; }
        public int Duration { get; set; }
        public string CompanyName { get; set; }
        public string InterviewerName { get; set; }
        public string EmailSubject { get; set; }
        public string EmailDescription { get; set; }
        public int JobId { get; set; }
        public string JobTitle { get; set; }
        public bool IsCanceled { get; set; }
        public bool IsEmailSent { get; set; }
        public TimeSpan InterviewTime { get; set; }
        public string SubmissionUuid { get; set; }
        public string InterviewComment { get; set; }
        public int InterviewerId { get; set; }
        public int CandidateId { get; set; }
        public int InterviewTypeId { get; set; }
        public int? TemplateId { get; set; }
        public int CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public int? InterviewerTypeId { get; set; }
    }
}
