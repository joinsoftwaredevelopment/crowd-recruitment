﻿using System;

#nullable disable

namespace Data.Tables
{
    public partial class EmployerPackageHistory
    {
        public int Id { get; set; }
        public int UserId { get; set; }
        public int PackageId { get; set; }
        public DateTime PackagePurchaseDate { get; set; }
        public DateTime PackageExpiryDate { get; set; }
        public virtual User User { get; set; }
        public virtual Package Package { get; set; }
    }
}
