﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Data.Tables
{
    public partial class VwEmployer
    {
        public int EmployerId { get; set; }
        public string Uuid { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string FullName { get; set; }
        public string PhoneNumber { get; set; }
        public string Email { get; set; }
        public bool? IsEmailVerified { get; set; }
        public string Password { get; set; }
        public int RoleId { get; set; }
        public int LanguageId { get; set; }
        public string CreatedDate { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public bool Active { get; set; }
        public bool? Accepted { get; set; }
        public DateTime? MeetingDate { get; set; }
        public int? MeetingPeriodId { get; set; }
        public int? RegistrationStep { get; set; }
        public int? InterviewerId { get; set; }
        public int? MeetingTimeId { get; set; }
        public int? TimeZoneId { get; set; }
        public int? UserStatusId { get; set; }
        public string CompanyName { get; set; }
        public string RejectedReason { get; set; }
        public int? OpenedJobsCount { get; set; }
        public int? PackageId { get; set; }

        public bool IsMembershipActive { get; set; }
    }
}
