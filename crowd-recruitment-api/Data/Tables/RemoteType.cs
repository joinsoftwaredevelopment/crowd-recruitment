﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Data.Tables
{
    public partial class RemoteType
    {
        public RemoteType()
        {
            JobLocationDetails = new HashSet<JobLocationDetail>();
        }

        public int Id { get; set; }
        public string Uuid { get; set; }
        public DateTime CreatedDate { get; set; }

        public virtual ICollection<JobLocationDetail> JobLocationDetails { get; set; }
    }
}
