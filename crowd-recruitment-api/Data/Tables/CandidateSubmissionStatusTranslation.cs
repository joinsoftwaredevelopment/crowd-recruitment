﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Data.Tables
{
    public partial class CandidateSubmissionStatusTranslation
    {
        public int Id { get; set; }
        public int CandidateSubmissionStatusId { get; set; }
        public string Translation { get; set; }
        public int LanguageId { get; set; }

        public virtual CandidateSubmissionStatus CandidateSubmissionStatus { get; set; }
    }
}
