﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Data.Tables
{
    public partial class Job
    {
        public int Id { get; set; }
        public string Uuid { get; set; }
        public int EmployerId { get; set; }
        public string JobTitle { get; set; }
        public string JobDescription { get; set; }
        public DateTime? JobDate { get; set; }
        public int? SeniorityLevelId { get; set; }
        public int? EmployerTypeId { get; set; }
        public int? JobNationalityId { get; set; }
        public int? ExperienceLevelId { get; set; }
        public int? JobStatusId { get; set; }
        public int? PeopleToHireCount { get; set; }
        public bool? IsContinuousHire { get; set; }
        public int? TimetoHireId { get; set; }
        public int? NumberOfComingDays { get; set; }
        public int? SourcingOusideYourCompanyId { get; set; }
        public int? FindingJobDifficultyLevelId { get; set; }
        public string FirstMailSubject { get; set; }
        public string FirstMailDescription { get; set; }
        public string FollowUp1Days { get; set; }
        public string Followup2Days { get; set; }
        public bool IsDeleted { get; set; }
        public int JobLocationId { get; set; }
        public int? OfficeLocationId { get; set; }
        public bool IsActive { get; set; }
        public string MustHaveQualification { get; set; }
        public string NiceToHaveQualification { get; set; }
        public string CompaniesNotToSourceFrom { get; set; }
        public string HiringNeeds { get; set; }
        public string WhenYouNeedToHire { get; set; }
        public bool? SourcingOutsideOfJoin { get; set; }
        public int? IndustryId { get; set; }
        public decimal? Commission { get; set; }
        public string Reason { get; set; }
        public int? NoOfSubmissions { get; set; }
        public string Requirements { get; set; }
        public decimal? MinSalary { get; set; }
        public decimal? MaxSalary { get; set; }
        public int? CurrencyId { get; set; }
        public int? EngagementTemplateId { get; set; }

        public virtual User Employer { get; set; }
        public virtual EmploymentType EmployerType { get; set; }
        public virtual ExperienceLevel ExperienceLevel { get; set; }
        public virtual FindingJobDifficultyLevel FindingJobDifficultyLevel { get; set; }
        public virtual JobLocation JobLocation { get; set; }
        public virtual Country JobNationality { get; set; }
        public virtual JobStatus JobStatus { get; set; }
        public virtual SeniorityLevel SeniorityLevel { get; set; }
    }
}
