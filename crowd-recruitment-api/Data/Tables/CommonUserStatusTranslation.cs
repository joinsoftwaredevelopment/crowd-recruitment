﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Data.Tables
{
    public partial class CommonUserStatusTranslation
    {
        public int Id { get; set; }
        public int UserStatusId { get; set; }
        public string Translation { get; set; }
        public int LanguageId { get; set; }
    }
}
