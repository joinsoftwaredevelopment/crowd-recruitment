﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Data.Tables
{
    public partial class HiringStagesTransaltion
    {
        public int Id { get; set; }
        public int? StageId { get; set; }
        public int? LanguageId { get; set; }
        public string Translation { get; set; }
    }
}
