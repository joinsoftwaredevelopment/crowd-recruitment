﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Data.Tables
{
    public partial class Package
    {
        public int PackageId { get; set; }
        public string PackageTitle { get; set; }
        public decimal PackagePrice { get; set; }
        public string PackageDescription { get; set; }
    }
}
