﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Data.Tables
{
    public partial class VwJob
    {
        public int JobId { get; set; }
        public string Uuid { get; set; }
        public string JobTitle { get; set; }
        public string EmploymentTypeName { get; set; }
        public string SeniorityLevelName { get; set; }
        public string CompaniesNotToSourceFrom { get; set; }
        public string JobLocation { get; set; }
        public string CompanyName { get; set; }
        public string MustHaveQualification { get; set; }
        public string NiceToHaveQualification { get; set; }
        public int EmployerId { get; set; }
        public string HiringNeeds { get; set; }
        public string ExperienceLevelName { get; set; }
        public int LanguageId { get; set; }
        public int? JobStatusId { get; set; }
        public string Description { get; set; }
        public string CompanyIndustry { get; set; }
        public string JobStatusName { get; set; }
        public string Requirements { get; set; }
        public int? NoOfSubmissions { get; set; }
        public string EmployerName { get; set; }
        public string RejectionReason { get; set; }
        public string JobDate { get; set; }
        public decimal? Commission { get; set; }
        public string EmpoyerUuId { get; set; }
        public bool IsActive { get; set; }
        public int CountryId { get; set; }
        public string CountryName { get; set; }
        public int? PeopleToHireCount { get; set; }
        public bool? IsContinuousHire { get; set; }
        public int? TimetoHireId { get; set; }
        public decimal? MinSalary { get; set; }
        public decimal? MaxSalary { get; set; }
        public string FirstMailSubject { get; set; }
        public string FirstMailDescription { get; set; }
        public int? FindingJobDifficultyLevelId { get; set; }
        public int? CurrencyId { get; set; }
        public string CurrencyName { get; set; }
        public string TimetoHireName { get; set; }
        public string FindingJobDifficultyName { get; set; }
        public int? SubmissonCount { get; set; }
    }
}
