﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Data.Tables
{
    public partial class VwCountry
    {
        public int CountryId { get; set; }
        public string CountryName { get; set; }
        public int LanguageId { get; set; }
    }
}
