﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Data.Tables
{
    public partial class SeniorityLevelTranslation
    {
        public int Id { get; set; }
        public int SeniorityLevelId { get; set; }
        public string Translation { get; set; }
        public int LanguageId { get; set; }
    }
}
