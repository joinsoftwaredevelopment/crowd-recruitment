﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Data.Tables
{
    public partial class CandidateSubmissionStatus
    {
        public CandidateSubmissionStatus()
        {
            CandidateSubmissionStatusTranslations = new HashSet<CandidateSubmissionStatusTranslation>();
        }

        public int Id { get; set; }
        public string Uuid { get; set; }
        public DateTime CreatedDate { get; set; }

        public virtual ICollection<CandidateSubmissionStatusTranslation> CandidateSubmissionStatusTranslations { get; set; }
    }
}
