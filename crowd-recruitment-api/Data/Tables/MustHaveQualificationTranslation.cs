﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Data.Tables
{
    public partial class MustHaveQualificationTranslation
    {
        public int Id { get; set; }
        public int MustHaveQualificationId { get; set; }
        public string Translation { get; set; }
        public int LanguageId { get; set; }
    }
}
