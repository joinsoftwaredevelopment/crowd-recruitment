﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Data.Tables
{
    public partial class VwInterviewEmail
    {
        public int Id { get; set; }
        public int? TemplateId { get; set; }
        public string EmailSubject { get; set; }
        public string EmailDescription { get; set; }
        public bool IsCanceled { get; set; }
        public bool IsEmailSent { get; set; }
    }
}
