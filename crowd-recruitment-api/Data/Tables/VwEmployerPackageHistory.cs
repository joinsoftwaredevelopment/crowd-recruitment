﻿using System;

#nullable disable

namespace Data.Tables
{
    public partial class VwEmployerPackageHistory
    {
        public int Id { get; set; }
        public DateTime PackagePurchaseDate { get; set; }
        public DateTime PackageExpiryDate { get; set; }
        public int PackageId { get; set; }
        public string PackageTitle { get; set; }
        public decimal PackagePrice { get; set; }
        public string PackageDescription { get; set; }
        public int UserId { get; set; }
    }
}
