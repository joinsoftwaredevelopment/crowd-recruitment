﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Data.Tables
{
    public partial class Interviewer
    {
        public int Id { get; set; }
        public string InterviewerName { get; set; }
        public string InterviewerImage { get; set; }
        public string ZoomLink { get; set; }
        public string Email { get; set; }
    }
}
