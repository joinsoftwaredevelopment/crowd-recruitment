﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Data.Tables
{
    public partial class Credential
    {
        public byte Id { get; set; }
        public string SmsApiAccountId { get; set; }
        public string SmsApiTokenKey { get; set; }
        public string SmsApiPhone { get; set; }
        public string NotificationEmail { get; set; }
        public string NotificationEmailPassword { get; set; }
        public string NotificationEmailIncomingServer { get; set; }
        public string NotificationEmailOutgoingServer { get; set; }
        public string NotificationEmailPort { get; set; }
        public string JwtAccountId { get; set; }
        public string JwtTokenKey { get; set; }
        public string JwtSecretKey { get; set; }
        public string JwtAudience { get; set; }
        public string JwtIssuer { get; set; }
        public string JwtExpriationTime { get; set; }
        public string GoogleApiAccountId { get; set; }
        public string GoogleApiTokenKey { get; set; }
        public string FacebookApiAccountId { get; set; }
        public string FacebookApiTokenKey { get; set; }
    }
}
