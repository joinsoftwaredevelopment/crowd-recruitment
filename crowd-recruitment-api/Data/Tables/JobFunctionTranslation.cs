﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Data.Tables
{
    public partial class JobFunctionTranslation
    {
        public int Id { get; set; }
        public int JobFunctionId { get; set; }
        public string Translation { get; set; }
        public int LanguageId { get; set; }
    }
}
