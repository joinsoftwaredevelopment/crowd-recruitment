﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Data.Tables
{
    public partial class JobFunction
    {
        public JobFunction()
        {
            EmployerJobFunctions = new HashSet<EmployerJobFunction>();
        }

        public int Id { get; set; }
        public string Uuid { get; set; }
        public DateTime CreatedDate { get; set; }

        public virtual ICollection<EmployerJobFunction> EmployerJobFunctions { get; set; }
    }
}
