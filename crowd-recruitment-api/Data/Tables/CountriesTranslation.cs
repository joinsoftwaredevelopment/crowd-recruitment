﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Data.Tables
{
    public partial class CountriesTranslation
    {
        public int CountryId { get; set; }
        public int LanguageId { get; set; }
        public string Name { get; set; }

        public virtual Country Country { get; set; }
    }
}
