﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Data.Tables
{
    public partial class EmployerDetail
    {
        public int Id { get; set; }
        public int UserId { get; set; }
        public string CompanyName { get; set; }
        public int? PackageId { get; set; }
        public bool IsMembershipActive { get; set; }
        public DateTime? PackageExpiryDate { get; set; }
        public virtual User User { get; set; }
    }
}
