﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Data.Tables
{
    public partial class VwJobSourcer
    {
        public string JobUuId { get; set; }
        public string JobTitle { get; set; }
        public string SourcerName { get; set; }
        public string SourcerJobTitle { get; set; }
        public string SourcerUuId { get; set; }
        public int? SubmissionCount { get; set; }
    }
}
