﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Data.Tables
{
    public partial class CurrencyTranslation
    {
        public int CurrencyId { get; set; }
        public int LanguageId { get; set; }
        public string Name { get; set; }
        public string ShortName { get; set; }

        public virtual Currency Currency { get; set; }
    }
}
