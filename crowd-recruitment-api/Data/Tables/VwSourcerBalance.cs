﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Data.Tables
{
    public partial class VwSourcerBalance
    {
        public string EmployerName { get; set; }
        public string CandidateName { get; set; }
        public string SubmissionDate { get; set; }
        public string JobTitle { get; set; }
        public decimal CandidateCommission { get; set; }
        public string SourcerUuid { get; set; }
        public string CandidateUuId { get; set; }
    }
}
