﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Data.Tables
{
    public partial class EmployerJobFunction
    {
        public int Id { get; set; }
        public string Uuid { get; set; }
        public int JobId { get; set; }
        public int JobFunctionId { get; set; }

        public virtual JobFunction JobFunction { get; set; }
    }
}
