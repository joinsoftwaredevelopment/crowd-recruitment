﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

#nullable disable

namespace Data.Tables
{
    public partial class recruitmentContext : DbContext
    {
        public recruitmentContext()
        {
        }

        public recruitmentContext(DbContextOptions<recruitmentContext> options)
            : base(options)
        {
        }

        public virtual DbSet<CandidateSubmissionStatus> CandidateSubmissionStatuses { get; set; }
        public virtual DbSet<CandidateSubmissionStatusTranslation> CandidateSubmissionStatusTranslations { get; set; }
        public virtual DbSet<CandidatesInterview> CandidatesInterviews { get; set; }
        public virtual DbSet<CommonUserStatus> CommonUserStatuses { get; set; }
        public virtual DbSet<CommonUserStatusTranslation> CommonUserStatusTranslations { get; set; }
        public virtual DbSet<CompaniesNotToSourceFrom> CompaniesNotToSourceFroms { get; set; }
        public virtual DbSet<CompanyIndustry> CompanyIndustries { get; set; }
        public virtual DbSet<CompanyIndustryTranslation> CompanyIndustryTranslations { get; set; }
        public virtual DbSet<CountriesTranslation> CountriesTranslations { get; set; }
        public virtual DbSet<Country> Countries { get; set; }
        public virtual DbSet<Credential> Credentials { get; set; }
        public virtual DbSet<Currency> Currencies { get; set; }
        public virtual DbSet<CurrencyTranslation> CurrencyTranslations { get; set; }
        public virtual DbSet<EmailsTemplate> EmailsTemplates { get; set; }
        public virtual DbSet<EmailsTemplatesKeyword> EmailsTemplatesKeywords { get; set; }
        public virtual DbSet<EmailsTemplatesType> EmailsTemplatesTypes { get; set; }
        public virtual DbSet<EmployerCompanyIndustry> EmployerCompanyIndustries { get; set; }
        public virtual DbSet<EmployerDetail> EmployerDetails { get; set; }
        public virtual DbSet<EmployerJobFunction> EmployerJobFunctions { get; set; }
        public virtual DbSet<EmployerMustHaveQualification> EmployerMustHaveQualifications { get; set; }
        public virtual DbSet<EmployerNiceHaveQualification> EmployerNiceHaveQualifications { get; set; }
        public virtual DbSet<EmploymentType> EmploymentTypes { get; set; }
        public virtual DbSet<EmploymentTypeTranslation> EmploymentTypeTranslations { get; set; }
        public virtual DbSet<ExperienceLevel> ExperienceLevels { get; set; }
        public virtual DbSet<ExperienceLevelTranslation> ExperienceLevelTranslations { get; set; }
        public virtual DbSet<FindingJobDifficultyLevel> FindingJobDifficultyLevels { get; set; }
        public virtual DbSet<FindingJobDifficultyLevelTranslation> FindingJobDifficultyLevelTranslations { get; set; }
        public virtual DbSet<Gender> Genders { get; set; }
        public virtual DbSet<GenderTranslation> GenderTranslations { get; set; }
        public virtual DbSet<HiringStage> HiringStages { get; set; }
        public virtual DbSet<HiringStagesTransaltion> HiringStagesTransaltions { get; set; }
        public virtual DbSet<Interviewer> Interviewers { get; set; }
        public virtual DbSet<Job> Jobs { get; set; }
        public virtual DbSet<JobCandidateSubmission> JobCandidateSubmissions { get; set; }
        public virtual DbSet<JobCandidateSubmissionHistory> JobCandidateSubmissionHistories { get; set; }
        public virtual DbSet<JobFunction> JobFunctions { get; set; }
        public virtual DbSet<JobFunctionTranslation> JobFunctionTranslations { get; set; }
        public virtual DbSet<JobHistory> JobHistories { get; set; }
        public virtual DbSet<JobLocation> JobLocations { get; set; }
        public virtual DbSet<JobLocationDetail> JobLocationDetails { get; set; }
        public virtual DbSet<JobLocationTranslation> JobLocationTranslations { get; set; }
        public virtual DbSet<JobStatus> JobStatuses { get; set; }
        public virtual DbSet<JobStatusTranslation> JobStatusTranslations { get; set; }
        public virtual DbSet<JobTemplate> JobTemplates { get; set; }
        public virtual DbSet<JobTemplateMustHaveQualification> JobTemplateMustHaveQualifications { get; set; }
        public virtual DbSet<JobTemplateNiceHaveQualification> JobTemplateNiceHaveQualifications { get; set; }
        public virtual DbSet<Language> Languages { get; set; }
        public virtual DbSet<LanguagesTranslation> LanguagesTranslations { get; set; }
        public virtual DbSet<LogException> LogExceptions { get; set; }
        public virtual DbSet<MustHaveQualification> MustHaveQualifications { get; set; }
        public virtual DbSet<MustHaveQualificationTranslation> MustHaveQualificationTranslations { get; set; }
        public virtual DbSet<NiceHaveQualification> NiceHaveQualifications { get; set; }
        public virtual DbSet<NiceHaveQualificationTranslation> NiceHaveQualificationTranslations { get; set; }
        public virtual DbSet<Package> Packages { get; set; }
        public virtual DbSet<PackagesFeature> PackagesFeatures { get; set; }
        public virtual DbSet<RemoteType> RemoteTypes { get; set; }
        public virtual DbSet<RemoteTypeTranslation> RemoteTypeTranslations { get; set; }
        public virtual DbSet<Role> Roles { get; set; }
        public virtual DbSet<RolesTranslation> RolesTranslations { get; set; }
        public virtual DbSet<SeniorityLevel> SeniorityLevels { get; set; }
        public virtual DbSet<SeniorityLevelTranslation> SeniorityLevelTranslations { get; set; }
        public virtual DbSet<SourcerBalancesHistory> SourcerBalancesHistories { get; set; }
        public virtual DbSet<SourcerDetail> SourcerDetails { get; set; }
        public virtual DbSet<TimetoHire> TimetoHires { get; set; }
        public virtual DbSet<TimetoHireTranslation> TimetoHireTranslations { get; set; }
        public virtual DbSet<User> Users { get; set; }
        public virtual DbSet<EmployerPackageHistory> EmployerPackageHistory { get; set; }
        public virtual DbSet<VwAdminSourcersBalance> VwAdminSourcersBalances { get; set; }
        public virtual DbSet<VwCandidatesInterview> VwCandidatesInterviews { get; set; }
        public virtual DbSet<VwCountry> VwCountries { get; set; }
        public virtual DbSet<VwCurrency> VwCurrencies { get; set; }
        public virtual DbSet<VwEmployer> VwEmployers { get; set; }
        public virtual DbSet<VwInterviewDetail> VwInterviewDetails { get; set; }
        public virtual DbSet<VwInterviewEmail> VwInterviewEmails { get; set; }
        public virtual DbSet<VwJob> VwJobs { get; set; }
        public virtual DbSet<VwJobCandidate> VwJobCandidates { get; set; }
        public virtual DbSet<VwJobCandidateSubmissionHistory> VwJobCandidateSubmissionHistories { get; set; }
        public virtual DbSet<VwJobSourcer> VwJobSourcers { get; set; }
        public virtual DbSet<VwJobSubmissionsSummary> VwJobSubmissionsSummaries { get; set; }
        public virtual DbSet<VwJobTemplate> VwJobTemplates { get; set; }
        public virtual DbSet<VwSourcer> VwSourcers { get; set; }
        public virtual DbSet<VwSourcerBalance> VwSourcerBalances { get; set; }
        public virtual DbSet<VwSourcerBalancesSummary> VwSourcerBalancesSummaries { get; set; }
        public virtual DbSet<VwSourcerSubmission> VwSourcerSubmissions { get; set; }
        public virtual DbSet<VwEmployerPackageHistory> VwEmployerPackageHistory { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. You can avoid scaffolding the connection string by using the Name= syntax to read it from configuration - see https://go.microsoft.com/fwlink/?linkid=2131148. For more guidance on storing connection strings, see http://go.microsoft.com/fwlink/?LinkId=723263.
                optionsBuilder.UseSqlServer("Server=.;Database=recruitment;Trusted_Connection=True;");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasAnnotation("Relational:Collation", "SQL_Latin1_General_CP1_CI_AS");

            modelBuilder.Entity<CandidateSubmissionStatus>(entity =>
            {
                entity.ToTable("CandidateSubmissionStatus", "Employer");

                entity.HasIndex(e => e.Uuid, "UQ__Candidat__65A475E69BA1FE46")
                    .IsUnique();

                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.Uuid)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasColumnName("UUID");
            });

            modelBuilder.Entity<CandidateSubmissionStatusTranslation>(entity =>
            {
                entity.ToTable("CandidateSubmissionStatusTranslation", "Employer");

                entity.Property(e => e.LanguageId).HasColumnName("languageId");

                entity.Property(e => e.Translation)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.HasOne(d => d.CandidateSubmissionStatus)
                    .WithMany(p => p.CandidateSubmissionStatusTranslations)
                    .HasForeignKey(d => d.CandidateSubmissionStatusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_CandidateSubmissionStatusTranslation_CandidateSubmissionStatus");
            });

            modelBuilder.Entity<CandidatesInterview>(entity =>
            {
                entity.ToTable("CandidatesInterviews", "Employer");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.CandidateId).HasColumnName("candidateId");

                entity.Property(e => e.CreatedBy).HasColumnName("createdBy");

                entity.Property(e => e.CreatedDate)
                    .HasColumnType("datetime")
                    .HasColumnName("createdDate");

                entity.Property(e => e.Duration).HasColumnName("duration");

                entity.Property(e => e.EmailDescription)
                    .HasMaxLength(1000)
                    .HasColumnName("emailDescription");

                entity.Property(e => e.EmailSubject)
                    .HasMaxLength(100)
                    .HasColumnName("emailSubject");

                entity.Property(e => e.InterviewComment)
                    .IsRequired()
                    .HasMaxLength(500)
                    .HasColumnName("interviewComment");

                entity.Property(e => e.InterviewDate)
                    .HasColumnType("date")
                    .HasColumnName("interviewDate");

                entity.Property(e => e.InterviewTime).HasColumnName("interviewTime");

                entity.Property(e => e.InterviewTypeId).HasColumnName("interviewTypeId");

                entity.Property(e => e.InterviewerId).HasColumnName("interviewerId");

                entity.Property(e => e.InterviewerTypeId).HasColumnName("interviewerTypeId");

                entity.Property(e => e.IsCanceled).HasColumnName("isCanceled");

                entity.Property(e => e.IsEmailSent).HasColumnName("isEmailSent");

                entity.Property(e => e.TemplateId).HasColumnName("templateId");
            });

            modelBuilder.Entity<CommonUserStatus>(entity =>
            {
                entity.ToTable("Common.UserStatus");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.CreatedDate)
                    .HasColumnType("date")
                    .HasColumnName("createdDate");
            });

            modelBuilder.Entity<CommonUserStatusTranslation>(entity =>
            {
                entity.ToTable("Common.UserStatusTranslation");

                entity.Property(e => e.Id)
                    .ValueGeneratedNever()
                    .HasColumnName("id");

                entity.Property(e => e.LanguageId).HasColumnName("languageId");

                entity.Property(e => e.Translation)
                    .IsRequired()
                    .HasMaxLength(50)
                    .HasColumnName("translation");

                entity.Property(e => e.UserStatusId).HasColumnName("userStatusId");
            });

            modelBuilder.Entity<CompaniesNotToSourceFrom>(entity =>
            {
                entity.ToTable("CompaniesNotToSourceFrom", "Employer");

                entity.HasIndex(e => e.Uuid, "UQ__Companie__65A475E6A2C7912A")
                    .IsUnique();

                entity.HasIndex(e => e.Uuid, "UQ__Companie__65A475E6BAD86678")
                    .IsUnique();

                entity.Property(e => e.Text)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.Uuid)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasColumnName("UUID");
            });

            modelBuilder.Entity<CompanyIndustry>(entity =>
            {
                entity.ToTable("CompanyIndustry", "Employer");

                entity.HasIndex(e => e.Uuid, "UQ__CompanyI__65A475E62AB9AA8A")
                    .IsUnique();

                entity.HasIndex(e => e.Uuid, "UQ__CompanyI__65A475E6A5B733E0")
                    .IsUnique();

                entity.HasIndex(e => e.Uuid, "UQ__CompanyI__65A475E6B68932D9")
                    .IsUnique();

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.Uuid)
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasColumnName("UUID");
            });

            modelBuilder.Entity<CompanyIndustryTranslation>(entity =>
            {
                entity.ToTable("CompanyIndustryTranslation", "Employer");

                entity.Property(e => e.LanguageId).HasColumnName("languageId");

                entity.Property(e => e.Translation)
                    .IsRequired()
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<CountriesTranslation>(entity =>
            {
                entity.HasKey(e => new { e.CountryId, e.LanguageId })
                    .HasName("PK_CountriesDescription");

                entity.ToTable("CountriesTranslation", "Common");

                entity.Property(e => e.CountryId).HasColumnName("countryId");

                entity.Property(e => e.LanguageId).HasColumnName("languageId");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasColumnName("name");

                entity.HasOne(d => d.Country)
                    .WithMany(p => p.CountriesTranslations)
                    .HasForeignKey(d => d.CountryId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_CountriesDescription_Countries");
            });

            modelBuilder.Entity<Country>(entity =>
            {
                entity.ToTable("Countries", "Common");

                entity.Property(e => e.Id)
                    .ValueGeneratedNever()
                    .HasColumnName("id");

                entity.Property(e => e.CreatedDate)
                    .HasColumnType("datetime")
                    .HasColumnName("createdDate");

                entity.Property(e => e.Currency)
                    .HasMaxLength(4)
                    .HasColumnName("currency");

                entity.Property(e => e.Iso2)
                    .IsRequired()
                    .HasMaxLength(2)
                    .IsUnicode(false)
                    .HasColumnName("iso2");

                entity.Property(e => e.Iso3)
                    .IsRequired()
                    .HasMaxLength(3)
                    .IsUnicode(false)
                    .HasColumnName("iso3");

                entity.Property(e => e.Phonecode)
                    .HasMaxLength(20)
                    .IsUnicode(false)
                    .HasColumnName("phonecode");
            });

            modelBuilder.Entity<Credential>(entity =>
            {
                entity.ToTable("Credentials", "Common");

                entity.Property(e => e.FacebookApiAccountId)
                    .HasMaxLength(100)
                    .IsUnicode(false)
                    .HasColumnName("facebookApiAccountId");

                entity.Property(e => e.FacebookApiTokenKey)
                    .HasMaxLength(100)
                    .IsUnicode(false)
                    .HasColumnName("facebookApiTokenKey");

                entity.Property(e => e.GoogleApiAccountId)
                    .HasMaxLength(100)
                    .IsUnicode(false)
                    .HasColumnName("googleApiAccountId");

                entity.Property(e => e.GoogleApiTokenKey)
                    .HasMaxLength(100)
                    .IsUnicode(false)
                    .HasColumnName("googleApiTokenKey");

                entity.Property(e => e.JwtAccountId)
                    .HasMaxLength(100)
                    .IsUnicode(false)
                    .HasColumnName("jwtAccountId");

                entity.Property(e => e.JwtAudience)
                    .HasMaxLength(500)
                    .IsUnicode(false)
                    .HasColumnName("jwtAudience");

                entity.Property(e => e.JwtExpriationTime)
                    .HasMaxLength(500)
                    .IsUnicode(false)
                    .HasColumnName("jwtExpriationTime");

                entity.Property(e => e.JwtIssuer)
                    .HasMaxLength(500)
                    .IsUnicode(false)
                    .HasColumnName("jwtIssuer");

                entity.Property(e => e.JwtSecretKey)
                    .HasMaxLength(500)
                    .IsUnicode(false)
                    .HasColumnName("jwtSecretKey");

                entity.Property(e => e.JwtTokenKey)
                    .HasMaxLength(500)
                    .IsUnicode(false)
                    .HasColumnName("jwtTokenKey");

                entity.Property(e => e.NotificationEmail)
                    .HasMaxLength(200)
                    .IsUnicode(false)
                    .HasColumnName("notificationEmail");

                entity.Property(e => e.NotificationEmailIncomingServer)
                    .HasMaxLength(200)
                    .IsUnicode(false)
                    .HasColumnName("notificationEmailIncomingServer");

                entity.Property(e => e.NotificationEmailOutgoingServer)
                    .HasMaxLength(200)
                    .IsUnicode(false)
                    .HasColumnName("notificationEmailOutgoingServer");

                entity.Property(e => e.NotificationEmailPassword)
                    .HasMaxLength(200)
                    .IsUnicode(false)
                    .HasColumnName("notificationEmailPassword");

                entity.Property(e => e.NotificationEmailPort)
                    .HasMaxLength(200)
                    .IsUnicode(false)
                    .HasColumnName("notificationEmailPort");

                entity.Property(e => e.SmsApiAccountId)
                    .HasMaxLength(300)
                    .IsUnicode(false)
                    .HasColumnName("smsApiAccountId");

                entity.Property(e => e.SmsApiPhone)
                    .HasMaxLength(300)
                    .IsUnicode(false)
                    .HasColumnName("smsApiPhone");

                entity.Property(e => e.SmsApiTokenKey)
                    .HasMaxLength(300)
                    .IsUnicode(false)
                    .HasColumnName("smsApiTokenKey");
            });

            modelBuilder.Entity<Currency>(entity =>
            {
                entity.ToTable("Currencies", "Common");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.CreatedDate)
                    .HasColumnType("datetime")
                    .HasColumnName("createdDate");
            });

            modelBuilder.Entity<CurrencyTranslation>(entity =>
            {
                entity.HasKey(e => new { e.CurrencyId, e.LanguageId })
                    .HasName("PK_CurrencyDescription");

                entity.ToTable("CurrencyTranslation", "Common");

                entity.Property(e => e.CurrencyId).HasColumnName("currencyId");

                entity.Property(e => e.LanguageId).HasColumnName("languageId");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasColumnName("name");

                entity.Property(e => e.ShortName)
                    .IsRequired()
                    .HasMaxLength(5)
                    .IsUnicode(false)
                    .HasColumnName("shortName");

                entity.HasOne(d => d.Currency)
                    .WithMany(p => p.CurrencyTranslations)
                    .HasForeignKey(d => d.CurrencyId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_CurrencyTranslation_Currencies");
            });

            modelBuilder.Entity<EmailsTemplate>(entity =>
            {
                entity.ToTable("EmailsTemplates", "Common");

                entity.Property(e => e.Id)
                    .ValueGeneratedNever()
                    .HasColumnName("id");

                entity.Property(e => e.EmailTemplateType).HasColumnName("emailTemplateType");

                entity.Property(e => e.EmailTitleAr)
                    .IsRequired()
                    .HasMaxLength(200)
                    .HasColumnName("emailTitleAr");

                entity.Property(e => e.EmailTitleEn)
                    .IsRequired()
                    .HasMaxLength(200)
                    .HasColumnName("emailTitleEN");

                entity.Property(e => e.EmailbodyAr)
                    .IsRequired()
                    .HasColumnName("emailbodyAr");

                entity.Property(e => e.EmailbodyEn)
                    .IsRequired()
                    .HasColumnName("emailbodyEn");
            });

            modelBuilder.Entity<EmailsTemplatesKeyword>(entity =>
            {
                entity.ToTable("EmailsTemplatesKeywords", "Common");

                entity.Property(e => e.Keyword)
                    .IsRequired()
                    .HasMaxLength(100);
            });

            modelBuilder.Entity<EmailsTemplatesType>(entity =>
            {
                entity.ToTable("EmailsTemplatesTypes", "Common");

                entity.Property(e => e.Id)
                    .ValueGeneratedNever()
                    .HasColumnName("id");

                entity.Property(e => e.NameAr)
                    .IsRequired()
                    .HasMaxLength(100)
                    .HasColumnName("nameAr");

                entity.Property(e => e.NameEn)
                    .IsRequired()
                    .HasMaxLength(100)
                    .HasColumnName("nameEn");
            });

            modelBuilder.Entity<EmployerCompanyIndustry>(entity =>
            {
                entity.ToTable("EmployerCompanyIndustry", "Employer");

                entity.HasIndex(e => e.Uuid, "UQ__Employer__65A475E6DB8C2FDC")
                    .IsUnique();

                entity.Property(e => e.Uuid)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasColumnName("UUID");
            });

            modelBuilder.Entity<EmployerDetail>(entity =>
            {
                entity.ToTable("EmployerDetails", "Employer");

                entity.Property(e => e.CompanyName)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.PackageId).HasColumnName("packageId");

                entity.HasOne(d => d.User)
                    .WithMany(p => p.EmployerDetails)
                    .HasForeignKey(d => d.UserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_EmployerDetails_Users");
            });

            modelBuilder.Entity<EmployerJobFunction>(entity =>
            {
                entity.ToTable("EmployerJobFunction", "Employer");

                entity.HasIndex(e => e.Uuid, "UQ__Employer__65A475E633129B97")
                    .IsUnique();

                entity.Property(e => e.Uuid)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasColumnName("UUID");

                entity.HasOne(d => d.JobFunction)
                    .WithMany(p => p.EmployerJobFunctions)
                    .HasForeignKey(d => d.JobFunctionId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_JobFunction_Job");
            });

            modelBuilder.Entity<EmployerMustHaveQualification>(entity =>
            {
                entity.ToTable("EmployerMustHaveQualification", "Employer");

                entity.HasIndex(e => e.Uuid, "UQ__Employer__65A475E60C6C1AB4")
                    .IsUnique();

                entity.Property(e => e.Text)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.Uuid)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasColumnName("UUID");
            });

            modelBuilder.Entity<EmployerNiceHaveQualification>(entity =>
            {
                entity.ToTable("EmployerNiceHaveQualification", "Employer");

                entity.HasIndex(e => e.Uuid, "UQ__Employer__65A475E63C33C567")
                    .IsUnique();

                entity.Property(e => e.Text)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.Uuid)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasColumnName("UUID");
            });

            modelBuilder.Entity<EmploymentType>(entity =>
            {
                entity.ToTable("EmploymentType", "Employer");

                entity.HasIndex(e => e.Uuid, "UQ__Employme__65A475E61FDDD1D0")
                    .IsUnique();

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.Uuid)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasColumnName("UUID");
            });

            modelBuilder.Entity<EmploymentTypeTranslation>(entity =>
            {
                entity.ToTable("EmploymentTypeTranslation", "Employer");

                entity.Property(e => e.LanguageId).HasColumnName("languageId");

                entity.Property(e => e.Translation)
                    .IsRequired()
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<ExperienceLevel>(entity =>
            {
                entity.ToTable("ExperienceLevel", "Employer");

                entity.HasIndex(e => e.Uuid, "UQ__Experien__65A475E6FFFCC814")
                    .IsUnique();

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.Uuid)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasColumnName("UUID");
            });

            modelBuilder.Entity<ExperienceLevelTranslation>(entity =>
            {
                entity.ToTable("ExperienceLevelTranslation", "Employer");

                entity.Property(e => e.LanguageId).HasColumnName("languageId");

                entity.Property(e => e.Translation)
                    .IsRequired()
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<FindingJobDifficultyLevel>(entity =>
            {
                entity.ToTable("FindingJobDifficultyLevel", "Employer");

                entity.HasIndex(e => e.Uuid, "UQ__FindingJ__65A475E6A4A7FD62")
                    .IsUnique();

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.Uuid)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasColumnName("UUID");
            });

            modelBuilder.Entity<FindingJobDifficultyLevelTranslation>(entity =>
            {
                entity.ToTable("FindingJobDifficultyLevelTranslation", "Employer");

                entity.Property(e => e.LanguageId).HasColumnName("languageId");

                entity.Property(e => e.Translation)
                    .IsRequired()
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<Gender>(entity =>
            {
                entity.ToTable("Gender", "Common");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.CreatedDate)
                    .HasColumnType("datetime")
                    .HasColumnName("createdDate");
            });

            modelBuilder.Entity<GenderTranslation>(entity =>
            {
                entity.ToTable("GenderTranslation", "Common");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.GenderId).HasColumnName("genderId");

                entity.Property(e => e.LanguageId).HasColumnName("languageId");

                entity.Property(e => e.Translation)
                    .HasMaxLength(50)
                    .HasColumnName("translation");
            });

            modelBuilder.Entity<HiringStage>(entity =>
            {
                entity.ToTable("HiringStages", "Employer");

                entity.Property(e => e.Id)
                    .ValueGeneratedNever()
                    .HasColumnName("id");

                entity.Property(e => e.CreatedDate)
                    .HasColumnType("datetime")
                    .HasColumnName("createdDate");
            });

            modelBuilder.Entity<HiringStagesTransaltion>(entity =>
            {
                entity.ToTable("HiringStagesTransaltion", "Employer");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.LanguageId).HasColumnName("languageId");

                entity.Property(e => e.StageId).HasColumnName("stageId");

                entity.Property(e => e.Translation)
                    .HasMaxLength(50)
                    .HasColumnName("translation");
            });

            modelBuilder.Entity<Interviewer>(entity =>
            {
                entity.ToTable("Interviewers", "Common");

                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.Email)
                    .HasMaxLength(200)
                    .HasColumnName("email");

                entity.Property(e => e.InterviewerImage)
                    .IsRequired()
                    .HasMaxLength(200);

                entity.Property(e => e.InterviewerName)
                    .IsRequired()
                    .HasMaxLength(200);

                entity.Property(e => e.ZoomLink).HasColumnName("zoomLink");
            });

            modelBuilder.Entity<Job>(entity =>
            {
                entity.ToTable("Job", "Job");

                entity.HasIndex(e => e.Uuid, "UQ__Job__65A475E6C4103BAF")
                    .IsUnique();

                entity.Property(e => e.Commission)
                    .HasColumnType("decimal(10, 3)")
                    .HasColumnName("commission");

                entity.Property(e => e.CompaniesNotToSourceFrom)
                    .HasMaxLength(500)
                    .HasColumnName("companiesNotToSourceFrom");

                entity.Property(e => e.EngagementTemplateId).HasColumnName("engagementTemplateId");

                entity.Property(e => e.FirstMailDescription).HasMaxLength(1000);

                entity.Property(e => e.FirstMailSubject).HasMaxLength(50);

                entity.Property(e => e.FollowUp1Days)
                    .HasMaxLength(10)
                    .IsFixedLength(true);

                entity.Property(e => e.Followup2Days)
                    .HasMaxLength(10)
                    .IsFixedLength(true);

                entity.Property(e => e.HiringNeeds).HasMaxLength(50);

                entity.Property(e => e.IndustryId).HasColumnName("industryId");

                entity.Property(e => e.JobDate).HasColumnType("datetime");

                entity.Property(e => e.JobTitle).HasMaxLength(50);

                entity.Property(e => e.MaxSalary).HasColumnType("decimal(10, 3)");

                entity.Property(e => e.MinSalary).HasColumnType("decimal(10, 3)");

                entity.Property(e => e.MustHaveQualification)
                    .HasMaxLength(500)
                    .HasColumnName("mustHaveQualification");

                entity.Property(e => e.NiceToHaveQualification)
                    .HasMaxLength(500)
                    .HasColumnName("niceToHaveQualification");

                entity.Property(e => e.NoOfSubmissions)
                    .HasColumnName("noOfSubmissions")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.Reason)
                    .HasMaxLength(500)
                    .HasColumnName("reason");

                entity.Property(e => e.Requirements)
                    .HasMaxLength(1000)
                    .HasColumnName("requirements");

                entity.Property(e => e.SourcingOutsideOfJoin).HasColumnName("sourcingOutsideOfJoin");

                entity.Property(e => e.Uuid)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasColumnName("UUID");

                entity.Property(e => e.WhenYouNeedToHire)
                    .HasMaxLength(50)
                    .HasColumnName("whenYouNeedToHire");

                entity.HasOne(d => d.Employer)
                    .WithMany(p => p.Jobs)
                    .HasForeignKey(d => d.EmployerId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Job_Users");

                entity.HasOne(d => d.EmployerType)
                    .WithMany(p => p.Jobs)
                    .HasForeignKey(d => d.EmployerTypeId)
                    .HasConstraintName("FK_Job_EmploymentType");

                entity.HasOne(d => d.ExperienceLevel)
                    .WithMany(p => p.Jobs)
                    .HasForeignKey(d => d.ExperienceLevelId)
                    .HasConstraintName("FK_Job_ExperienceLevel");

                entity.HasOne(d => d.FindingJobDifficultyLevel)
                    .WithMany(p => p.Jobs)
                    .HasForeignKey(d => d.FindingJobDifficultyLevelId)
                    .HasConstraintName("FK_Job_FindingJobDifficultyLevel");

                entity.HasOne(d => d.JobLocation)
                    .WithMany(p => p.Jobs)
                    .HasForeignKey(d => d.JobLocationId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Job_JobLocation");

                entity.HasOne(d => d.JobNationality)
                    .WithMany(p => p.Jobs)
                    .HasForeignKey(d => d.JobNationalityId)
                    .HasConstraintName("FK_Job_Countries");

                entity.HasOne(d => d.JobStatus)
                    .WithMany(p => p.Jobs)
                    .HasForeignKey(d => d.JobStatusId)
                    .HasConstraintName("FK_Job_JobStatus");

                entity.HasOne(d => d.SeniorityLevel)
                    .WithMany(p => p.Jobs)
                    .HasForeignKey(d => d.SeniorityLevelId)
                    .HasConstraintName("FK_Job_SeniorityLevel");
            });

            modelBuilder.Entity<JobCandidateSubmission>(entity =>
            {
                entity.ToTable("JobCandidateSubmission", "Sourcer");

                entity.HasIndex(e => e.Uuid, "UQ__JobCandi__65A475E6BD9808BB")
                    .IsUnique();

                entity.Property(e => e.CandidateName)
                    .HasMaxLength(150)
                    .HasColumnName("candidateName");

                entity.Property(e => e.Commission)
                    .HasColumnType("decimal(10, 3)")
                    .HasColumnName("commission");

                entity.Property(e => e.CvName)
                    .IsRequired()
                    .HasMaxLength(50)
                    .HasColumnName("CV_Name");

                entity.Property(e => e.Email)
                    .HasMaxLength(50)
                    .HasColumnName("email");

                entity.Property(e => e.EmpReason).HasMaxLength(500);

                entity.Property(e => e.HiringStageId).HasColumnName("hiringStageId");

                entity.Property(e => e.IsAcceptOffer).HasColumnName("isAcceptOffer");

                entity.Property(e => e.IsCvApproved).HasColumnName("isCvApproved");

                entity.Property(e => e.IsHired).HasColumnName("isHired");

                entity.Property(e => e.LinkedInProfile)
                    .HasMaxLength(300)
                    .HasColumnName("linkedInProfile");

                entity.Property(e => e.LocationId).HasColumnName("locationId");

                entity.Property(e => e.RejectionReason)
                    .HasMaxLength(500)
                    .HasColumnName("rejectionReason");

                entity.Property(e => e.SubmissionDate).HasColumnType("datetime");

                entity.Property(e => e.Uuid)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasColumnName("UUID");
            });

            modelBuilder.Entity<JobCandidateSubmissionHistory>(entity =>
            {
                entity.ToTable("JobCandidateSubmissionHistory", "Sourcer");

                entity.Property(e => e.Comment).HasMaxLength(50);

                entity.Property(e => e.CreateDate)
                    .HasColumnType("datetime")
                    .HasColumnName("createDate");
            });

            modelBuilder.Entity<JobFunction>(entity =>
            {
                entity.ToTable("JobFunction", "Job");

                entity.HasIndex(e => e.Uuid, "UQ__JobFunct__65A475E6865A7D32")
                    .IsUnique();

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.Uuid)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasColumnName("UUID");
            });

            modelBuilder.Entity<JobFunctionTranslation>(entity =>
            {
                entity.ToTable("JobFunctionTranslation", "Job");

                entity.Property(e => e.LanguageId).HasColumnName("languageId");

                entity.Property(e => e.Translation)
                    .IsRequired()
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<JobHistory>(entity =>
            {
                entity.ToTable("JobHistory", "Job");

                entity.Property(e => e.Comment).HasMaxLength(50);

                entity.Property(e => e.CreateDate)
                    .HasColumnType("datetime")
                    .HasColumnName("createDate");
            });

            modelBuilder.Entity<JobLocation>(entity =>
            {
                entity.ToTable("JobLocation", "Job");

                entity.HasIndex(e => e.Uuid, "UQ__JobLocat__65A475E6EF201177")
                    .IsUnique();

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.Uuid)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasColumnName("UUID");
            });

            modelBuilder.Entity<JobLocationDetail>(entity =>
            {
                entity.ToTable("JobLocationDetails", "Job");

                entity.Property(e => e.Other).HasMaxLength(50);

                entity.Property(e => e.SourcerCityLocationId).HasColumnName("SourcerCityLocationID");

                entity.Property(e => e.SourcerCountryLocationId).HasColumnName("SourcerCountryLocationID");

                entity.HasOne(d => d.RemoteType)
                    .WithMany(p => p.JobLocationDetails)
                    .HasForeignKey(d => d.RemoteTypeId)
                    .HasConstraintName("FK_JobLocationDetails_JobLocationDetails");
            });

            modelBuilder.Entity<JobLocationTranslation>(entity =>
            {
                entity.ToTable("JobLocationTranslation", "Job");

                entity.Property(e => e.LanguageId).HasColumnName("languageId");

                entity.Property(e => e.Translation)
                    .IsRequired()
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<JobStatus>(entity =>
            {
                entity.ToTable("JobStatus", "Job");

                entity.HasIndex(e => e.Uuid, "UQ__JobStatu__65A475E65AC539A1")
                    .IsUnique();

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.Uuid)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasColumnName("UUID");
            });

            modelBuilder.Entity<JobStatusTranslation>(entity =>
            {
                entity.ToTable("JobStatusTranslation", "Job");

                entity.Property(e => e.LanguageId).HasColumnName("languageId");

                entity.Property(e => e.Translation)
                    .IsRequired()
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<JobTemplate>(entity =>
            {
                entity.ToTable("JobTemplates", "Job");

                entity.Property(e => e.Id)
                    .ValueGeneratedNever()
                    .HasColumnName("id");

                entity.Property(e => e.EmploymentTypeId).HasColumnName("employmentTypeId");

                entity.Property(e => e.JobDescription)
                    .IsRequired()
                    .HasColumnName("jobDescription");

                entity.Property(e => e.JobIndustryId).HasColumnName("jobIndustryId");

                entity.Property(e => e.JobLocationId).HasColumnName("jobLocationId");

                entity.Property(e => e.JobRequirements)
                    .IsRequired()
                    .HasColumnName("jobRequirements");

                entity.Property(e => e.JobTitle)
                    .IsRequired()
                    .HasMaxLength(100)
                    .HasColumnName("jobTitle");

                entity.Property(e => e.LanguageId).HasColumnName("languageId");
            });

            modelBuilder.Entity<JobTemplateMustHaveQualification>(entity =>
            {
                entity.ToTable("JobTemplateMustHaveQualification", "Job");

                entity.Property(e => e.Text)
                    .IsRequired()
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<JobTemplateNiceHaveQualification>(entity =>
            {
                entity.ToTable("JobTemplateNiceHaveQualification", "Job");

                entity.Property(e => e.Text)
                    .IsRequired()
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<Language>(entity =>
            {
                entity.ToTable("Languages", "Common");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasColumnName("name");
            });

            modelBuilder.Entity<LanguagesTranslation>(entity =>
            {
                entity.ToTable("LanguagesTranslation", "Common");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.LanguageId).HasColumnName("languageId");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(50)
                    .HasColumnName("name");
            });

            modelBuilder.Entity<LogException>(entity =>
            {
                entity.ToTable("LogExceptions", "Logs");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.AssemblyName)
                    .HasMaxLength(100)
                    .IsUnicode(false)
                    .HasColumnName("assemblyName");

                entity.Property(e => e.ErrorMessage)
                    .HasMaxLength(500)
                    .IsUnicode(false)
                    .HasColumnName("errorMessage");

                entity.Property(e => e.InnerException)
                    .HasMaxLength(500)
                    .IsUnicode(false)
                    .HasColumnName("innerException");

                entity.Property(e => e.LogDate)
                    .HasColumnType("datetime")
                    .HasColumnName("logDate");

                entity.Property(e => e.MethodName)
                    .HasMaxLength(100)
                    .IsUnicode(false)
                    .HasColumnName("methodName");

                entity.Property(e => e.ObjectName)
                    .HasMaxLength(100)
                    .IsUnicode(false)
                    .HasColumnName("objectName");
            });

            modelBuilder.Entity<MustHaveQualification>(entity =>
            {
                entity.ToTable("MustHaveQualification", "Employer");

                entity.HasIndex(e => e.Uuid, "UQ__MustHave__65A475E69D625CD6")
                    .IsUnique();

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.Uuid)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasColumnName("UUID");
            });

            modelBuilder.Entity<MustHaveQualificationTranslation>(entity =>
            {
                entity.ToTable("MustHaveQualificationTranslation", "Employer");

                entity.Property(e => e.LanguageId).HasColumnName("languageId");

                entity.Property(e => e.Translation)
                    .IsRequired()
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<NiceHaveQualification>(entity =>
            {
                entity.ToTable("NiceHaveQualification", "Employer");

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.Uuid)
                    .IsRequired()
                    .HasMaxLength(50)
                    .HasColumnName("UUID");
            });

            modelBuilder.Entity<NiceHaveQualificationTranslation>(entity =>
            {
                entity.ToTable("NiceHaveQualificationTranslation", "Employer");

                entity.Property(e => e.LanguageId).HasColumnName("languageId");

                entity.Property(e => e.Translation)
                    .IsRequired()
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<Package>(entity =>
            {
                entity.ToTable("Packages", "Common");

                entity.Property(e => e.PackageId).HasColumnName("packageId");

                entity.Property(e => e.PackageDescription)
                    .IsRequired()
                    .HasColumnName("packageDescription");

                entity.Property(e => e.PackagePrice)
                    .HasColumnType("decimal(10, 3)")
                    .HasColumnName("packagePrice");

                entity.Property(e => e.PackageTitle)
                    .IsRequired()
                    .HasMaxLength(200)
                    .HasColumnName("packageTitle");
            });

            modelBuilder.Entity<PackagesFeature>(entity =>
            {
                entity.ToTable("PackagesFeatures", "Common");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.PackageId).HasColumnName("packageId");

                entity.Property(e => e.Text)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.Uuid)
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasColumnName("UUID");
            });

            modelBuilder.Entity<RemoteType>(entity =>
            {
                entity.ToTable("RemoteType", "Employer");

                entity.HasIndex(e => e.Uuid, "UQ__RemoteType__65A475E6FFFCC814")
                    .IsUnique();

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.Uuid)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasColumnName("UUID");
            });

            modelBuilder.Entity<RemoteTypeTranslation>(entity =>
            {
                entity.ToTable("RemoteTypeTranslation", "Employer");

                entity.Property(e => e.LanguageId).HasColumnName("languageId");

                entity.Property(e => e.Translation)
                    .IsRequired()
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<Role>(entity =>
            {
                entity.ToTable("Roles", "Common");

                entity.HasIndex(e => e.Uuid, "UQ__Roles__65A475E6F8764799")
                    .IsUnique();

                entity.Property(e => e.CreatedDate)
                    .IsRequired()
                    .HasMaxLength(10)
                    .IsFixedLength(true);

                entity.Property(e => e.Uuid)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasColumnName("UUID");
            });

            modelBuilder.Entity<RolesTranslation>(entity =>
            {
                entity.ToTable("RolesTranslation", "Common");

                entity.Property(e => e.LanguageId).HasColumnName("languageId");

                entity.Property(e => e.Translation)
                    .IsRequired()
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<SeniorityLevel>(entity =>
            {
                entity.ToTable("SeniorityLevel", "Employer");

                entity.HasIndex(e => e.Uuid, "UQ__Seniorit__65A475E6324A735D")
                    .IsUnique();

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.Uuid)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasColumnName("UUID");
            });

            modelBuilder.Entity<SeniorityLevelTranslation>(entity =>
            {
                entity.ToTable("SeniorityLevelTranslation", "Employer");

                entity.Property(e => e.LanguageId).HasColumnName("languageId");

                entity.Property(e => e.Translation)
                    .IsRequired()
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<SourcerBalancesHistory>(entity =>
            {
                entity.ToTable("SourcerBalancesHistory", "Job");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.AcceptanceRatio)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.CreatedDate)
                    .HasColumnType("datetime")
                    .HasColumnName("createdDate");

                entity.Property(e => e.FullName)
                    .IsRequired()
                    .HasMaxLength(200);

                entity.Property(e => e.IsPaid).HasColumnName("isPaid");

                entity.Property(e => e.SourcerId).HasColumnName("sourcerId");

                entity.Property(e => e.TotalBalance)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.WeekTitle)
                    .IsRequired()
                    .HasMaxLength(200)
                    .HasColumnName("weekTitle");
            });

            modelBuilder.Entity<SourcerDetail>(entity =>
            {
                entity.ToTable("SourcerDetails", "Sourcer");

                entity.Property(e => e.CvName).HasMaxLength(50);

                entity.Property(e => e.JobTitle).HasMaxLength(50);

                entity.Property(e => e.LinkedInProfile).HasMaxLength(100);

                entity.HasOne(d => d.User)
                    .WithMany(p => p.SourcerDetails)
                    .HasForeignKey(d => d.UserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_SourcerDetails_Users");
            });

            modelBuilder.Entity<EmployerPackageHistory>(entity =>
            {
                entity.ToTable("EmployerPackageHistory", "Employer");

                //entity.HasOne(d => d.User)
                //    .WithMany(p => p.e)
                //    .HasForeignKey(d => d.UserId)
                //    .OnDelete(DeleteBehavior.ClientSetNull)
                //    .HasConstraintName("FK_SourcerDetails_Users");
            });

            modelBuilder.Entity<TimetoHire>(entity =>
            {
                entity.ToTable("TimetoHire", "Employer");

                entity.HasIndex(e => e.Uuid, "UQ__TimetoHi__65A475E6F0CA6786")
                    .IsUnique();

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.Uuid)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasColumnName("UUID");
            });

            modelBuilder.Entity<TimetoHireTranslation>(entity =>
            {
                entity.ToTable("TimetoHireTranslation", "Employer");

                entity.Property(e => e.LanguageId).HasColumnName("languageId");

                entity.Property(e => e.Translation)
                    .IsRequired()
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<User>(entity =>
            {
                entity.ToTable("Users", "Common");

                entity.HasIndex(e => e.Uuid, "UQ__Users__65A475E6ACDE64C5")
                    .IsUnique();

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.Email)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.FirstName)
                    .IsRequired()
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.LastName)
                    .IsRequired()
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.MeetingDate).HasColumnType("datetime");

                entity.Property(e => e.MeetingTimeId).HasColumnName("meetingTimeId");

                entity.Property(e => e.Password)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.PhoneNumber)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.RejectedReason)
                    .HasMaxLength(500)
                    .HasColumnName("rejectedReason");

                entity.Property(e => e.TimeZoneId).HasColumnName("timeZoneId");

                entity.Property(e => e.UpdatedDate).HasColumnType("datetime");

                entity.Property(e => e.UserStatusId).HasColumnName("userStatusId");

                entity.Property(e => e.Uuid)
                    .IsRequired()
                    .HasMaxLength(50)
                    .HasColumnName("UUID");
            });

            modelBuilder.Entity<VwAdminSourcersBalance>(entity =>
            {
                entity.HasNoKey();

                entity.ToView("VwAdminSourcersBalances");

                entity.Property(e => e.AcceptanceCount).HasColumnName("acceptanceCount");

                entity.Property(e => e.AcceptanceRatio).HasColumnName("acceptanceRatio");

                entity.Property(e => e.DeclinedCount).HasColumnName("declinedCount");

                entity.Property(e => e.FullName)
                    .IsRequired()
                    .HasMaxLength(61)
                    .IsUnicode(false)
                    .HasColumnName("fullName");

                entity.Property(e => e.IsPaid).HasColumnName("isPaid");

                entity.Property(e => e.JobCount).HasColumnName("jobCount");

                entity.Property(e => e.SourcerId).HasColumnName("sourcerId");

                entity.Property(e => e.SubmissionCount).HasColumnName("submissionCount");

                entity.Property(e => e.TotalBalance)
                    .HasColumnType("decimal(38, 3)")
                    .HasColumnName("totalBalance");
            });

            modelBuilder.Entity<VwCandidatesInterview>(entity =>
            {
                entity.HasNoKey();

                entity.ToView("VwCandidatesInterviews");

                entity.Property(e => e.CandidateEmail)
                    .HasMaxLength(50)
                    .HasColumnName("candidateEmail");

                entity.Property(e => e.CandidateId).HasColumnName("candidateId");

                entity.Property(e => e.CandidateName)
                    .HasMaxLength(150)
                    .HasColumnName("candidateName");

                entity.Property(e => e.CompanyName)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.CreatedBy).HasColumnName("createdBy");

                entity.Property(e => e.CreatedDate)
                    .HasColumnType("datetime")
                    .HasColumnName("createdDate");

                entity.Property(e => e.Duration).HasColumnName("duration");

                entity.Property(e => e.EmailDescription)
                    .HasMaxLength(1000)
                    .HasColumnName("emailDescription");

                entity.Property(e => e.EmailSubject)
                    .HasMaxLength(100)
                    .HasColumnName("emailSubject");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.InterviewComment)
                    .IsRequired()
                    .HasMaxLength(500)
                    .HasColumnName("interviewComment");

                entity.Property(e => e.InterviewDate)
                    .HasColumnType("date")
                    .HasColumnName("interviewDate");

                entity.Property(e => e.InterviewTime).HasColumnName("interviewTime");

                entity.Property(e => e.InterviewTypeId).HasColumnName("interviewTypeId");

                entity.Property(e => e.InterviewerId).HasColumnName("interviewerId");

                entity.Property(e => e.InterviewerName)
                    .IsRequired()
                    .HasMaxLength(200);

                entity.Property(e => e.InterviewerTypeId).HasColumnName("interviewerTypeId");

                entity.Property(e => e.IsCanceled).HasColumnName("isCanceled");

                entity.Property(e => e.IsEmailSent).HasColumnName("isEmailSent");

                entity.Property(e => e.JobTitle).HasMaxLength(50);

                entity.Property(e => e.SubmissionUuid)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasColumnName("submissionUuid");

                entity.Property(e => e.TemplateId).HasColumnName("templateId");
            });

            modelBuilder.Entity<VwCountry>(entity =>
            {
                entity.HasNoKey();

                entity.ToView("VwCountries");

                entity.Property(e => e.CountryId).HasColumnName("countryId");

                entity.Property(e => e.CountryName)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasColumnName("countryName");

                entity.Property(e => e.LanguageId).HasColumnName("languageId");
            });

            modelBuilder.Entity<VwCurrency>(entity =>
            {
                entity.HasNoKey();

                entity.ToView("VwCurrencies");

                entity.Property(e => e.CurrencyId).HasColumnName("currencyId");

                entity.Property(e => e.CurrencyName)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasColumnName("currencyName");

                entity.Property(e => e.LanguageId).HasColumnName("languageId");

                entity.Property(e => e.ShortName)
                    .IsRequired()
                    .HasMaxLength(5)
                    .IsUnicode(false)
                    .HasColumnName("shortName");
            });

            modelBuilder.Entity<VwEmployer>(entity =>
            {
                entity.HasNoKey();

                entity.ToView("VwEmployers");

                entity.Property(e => e.CompanyName)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.CreatedDate)
                    .HasMaxLength(30)
                    .HasColumnName("createdDate");

                entity.Property(e => e.Email)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.EmployerId).HasColumnName("employerId");

                entity.Property(e => e.FirstName)
                    .IsRequired()
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.FullName)
                    .IsRequired()
                    .HasMaxLength(61)
                    .IsUnicode(false)
                    .HasColumnName("fullName");

                entity.Property(e => e.LastName)
                    .IsRequired()
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.MeetingDate).HasColumnType("datetime");

                entity.Property(e => e.MeetingTimeId).HasColumnName("meetingTimeId");

                entity.Property(e => e.OpenedJobsCount).HasColumnName("openedJobsCount");

                entity.Property(e => e.PackageId).HasColumnName("packageId");

                entity.Property(e => e.Password)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.PhoneNumber)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.RejectedReason)
                    .HasMaxLength(500)
                    .HasColumnName("rejectedReason");

                entity.Property(e => e.TimeZoneId).HasColumnName("timeZoneId");

                entity.Property(e => e.UpdatedDate).HasColumnType("datetime");

                entity.Property(e => e.UserStatusId).HasColumnName("userStatusId");

                entity.Property(e => e.Uuid)
                    .IsRequired()
                    .HasMaxLength(50)
                    .HasColumnName("UUID");
            });

            modelBuilder.Entity<VwEmployerPackageHistory>(entity =>
            {
                entity.Property(e => e.PackageId).HasColumnName("packageId");

                entity.Property(e => e.PackageTitle).HasColumnType("packageTitle");

                entity.Property(e => e.PackagePrice).HasColumnName("packagePrice");

                entity.Property(e => e.PackageDescription).HasColumnName("packageDescription");
            });

            modelBuilder.Entity<VwInterviewDetail>(entity =>
            {
                entity.HasNoKey();

                entity.ToView("VwInterviewDetails");

                entity.Property(e => e.CreatedBy).HasColumnName("createdBy");

                entity.Property(e => e.CreatedDate)
                    .HasColumnType("datetime")
                    .HasColumnName("createdDate");

                entity.Property(e => e.Duration).HasColumnName("duration");

                entity.Property(e => e.EmailDescription)
                    .HasMaxLength(1000)
                    .HasColumnName("emailDescription");

                entity.Property(e => e.EmailSubject)
                    .HasMaxLength(100)
                    .HasColumnName("emailSubject");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.InterviewComment)
                    .IsRequired()
                    .HasMaxLength(500)
                    .HasColumnName("interviewComment");

                entity.Property(e => e.InterviewDate)
                    .HasColumnType("date")
                    .HasColumnName("interviewDate");

                entity.Property(e => e.InterviewTime).HasColumnName("interviewTime");

                entity.Property(e => e.InterviewTypeId).HasColumnName("interviewTypeId");

                entity.Property(e => e.InterviewerId).HasColumnName("interviewerId");

                entity.Property(e => e.InterviewerTypeId).HasColumnName("interviewerTypeId");

                entity.Property(e => e.IsCanceled).HasColumnName("isCanceled");

                entity.Property(e => e.IsEmailSent).HasColumnName("isEmailSent");

                entity.Property(e => e.SubmissionUuid)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasColumnName("submissionUUid");

                entity.Property(e => e.TemplateId).HasColumnName("templateId");
            });

            modelBuilder.Entity<VwInterviewEmail>(entity =>
            {
                entity.HasNoKey();

                entity.ToView("VwInterviewEmails");

                entity.Property(e => e.EmailDescription)
                    .HasMaxLength(1000)
                    .HasColumnName("emailDescription");

                entity.Property(e => e.EmailSubject)
                    .HasMaxLength(100)
                    .HasColumnName("emailSubject");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.IsCanceled).HasColumnName("isCanceled");

                entity.Property(e => e.IsEmailSent).HasColumnName("isEmailSent");

                entity.Property(e => e.TemplateId).HasColumnName("templateId");
            });

            modelBuilder.Entity<VwJob>(entity =>
            {
                entity.HasNoKey();

                entity.ToView("VwJobs");

                entity.Property(e => e.Commission)
                    .HasColumnType("decimal(10, 3)")
                    .HasColumnName("commission");

                entity.Property(e => e.CompaniesNotToSourceFrom)
                    .HasMaxLength(500)
                    .HasColumnName("companiesNotToSourceFrom");

                entity.Property(e => e.CompanyIndustry)
                    .IsRequired()
                    .HasMaxLength(50)
                    .HasColumnName("companyIndustry");

                entity.Property(e => e.CompanyName)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.CountryId).HasColumnName("countryId");

                entity.Property(e => e.CountryName)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasColumnName("countryName");

                entity.Property(e => e.CurrencyName)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasColumnName("currencyName");

                entity.Property(e => e.Description).HasColumnName("description");

                entity.Property(e => e.EmployerId).HasColumnName("employerId");

                entity.Property(e => e.EmployerName)
                    .IsRequired()
                    .HasMaxLength(61)
                    .IsUnicode(false)
                    .HasColumnName("employerName");

                entity.Property(e => e.EmploymentTypeName)
                    .IsRequired()
                    .HasMaxLength(50)
                    .HasColumnName("employmentTypeName");

                entity.Property(e => e.EmpoyerUuId)
                    .IsRequired()
                    .HasMaxLength(50)
                    .HasColumnName("empoyerUuId");

                entity.Property(e => e.ExperienceLevelName).HasMaxLength(50);

                entity.Property(e => e.FindingJobDifficultyName)
                    .IsRequired()
                    .HasMaxLength(1)
                    .IsUnicode(false)
                    .HasColumnName("findingJobDifficultyName");

                entity.Property(e => e.FirstMailDescription).HasMaxLength(1000);

                entity.Property(e => e.FirstMailSubject).HasMaxLength(50);

                entity.Property(e => e.HiringNeeds).HasMaxLength(50);

                entity.Property(e => e.JobDate)
                    .HasMaxLength(30)
                    .HasColumnName("jobDate");

                entity.Property(e => e.JobId).HasColumnName("jobId");

                entity.Property(e => e.JobLocation)
                    .IsRequired()
                    .HasMaxLength(50)
                    .HasColumnName("jobLocation");

                entity.Property(e => e.JobStatusName)
                    .IsRequired()
                    .HasMaxLength(50)
                    .HasColumnName("jobStatusName");

                entity.Property(e => e.JobTitle).HasMaxLength(50);

                entity.Property(e => e.LanguageId).HasColumnName("languageId");

                entity.Property(e => e.MaxSalary).HasColumnType("decimal(10, 3)");

                entity.Property(e => e.MinSalary).HasColumnType("decimal(10, 3)");

                entity.Property(e => e.MustHaveQualification)
                    .HasMaxLength(500)
                    .HasColumnName("mustHaveQualification");

                entity.Property(e => e.NiceToHaveQualification)
                    .HasMaxLength(500)
                    .HasColumnName("niceToHaveQualification");

                entity.Property(e => e.NoOfSubmissions).HasColumnName("noOfSubmissions");

                entity.Property(e => e.RejectionReason)
                    .HasMaxLength(500)
                    .HasColumnName("rejectionReason");

                entity.Property(e => e.Requirements)
                    .HasMaxLength(1000)
                    .HasColumnName("requirements");

                entity.Property(e => e.SeniorityLevelName)
                    .IsRequired()
                    .HasMaxLength(50)
                    .HasColumnName("seniorityLevelName");

                entity.Property(e => e.SubmissonCount).HasColumnName("submissonCount");

                entity.Property(e => e.TimetoHireName)
                    .IsRequired()
                    .HasMaxLength(1)
                    .IsUnicode(false)
                    .HasColumnName("timetoHireName");

                entity.Property(e => e.Uuid)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasColumnName("UUID");
            });

            modelBuilder.Entity<VwJobCandidate>(entity =>
            {
                entity.HasNoKey();

                entity.ToView("VwJobCandidates");

                entity.Property(e => e.CandidateName)
                    .HasMaxLength(150)
                    .HasColumnName("candidateName");

                entity.Property(e => e.CountryName)
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasColumnName("countryName");

                entity.Property(e => e.CvName)
                    .IsRequired()
                    .HasMaxLength(50)
                    .HasColumnName("cvName");

                entity.Property(e => e.Email)
                    .HasMaxLength(50)
                    .HasColumnName("email");

                entity.Property(e => e.FirstMailDescription)
                    .HasMaxLength(1000)
                    .HasColumnName("firstMailDescription");

                entity.Property(e => e.FirstMailSubject)
                    .HasMaxLength(50)
                    .HasColumnName("firstMailSubject");

                entity.Property(e => e.HiringStageId).HasColumnName("hiringStageId");

                entity.Property(e => e.IsAcceptOffer).HasColumnName("isAcceptOffer");

                entity.Property(e => e.IsCvApproved).HasColumnName("isCvApproved");

                entity.Property(e => e.IsHired).HasColumnName("isHired");

                entity.Property(e => e.JobDate)
                    .HasMaxLength(30)
                    .HasColumnName("jobDate");

                entity.Property(e => e.JobId).HasColumnName("jobId");

                entity.Property(e => e.JobTitle)
                    .HasMaxLength(50)
                    .HasColumnName("jobTitle");

                entity.Property(e => e.JobUuId)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasColumnName("jobUuId");

                entity.Property(e => e.LanguageId).HasColumnName("languageId");

                entity.Property(e => e.LinkedInProfile)
                    .HasMaxLength(300)
                    .HasColumnName("linkedInProfile");

                entity.Property(e => e.RejectionReason)
                    .HasMaxLength(500)
                    .HasColumnName("rejectionReason");

                entity.Property(e => e.RowNumber).HasColumnName("rowNumber");

                entity.Property(e => e.SourcerId).HasColumnName("sourcerId");

                entity.Property(e => e.SourcerName)
                    .IsRequired()
                    .HasMaxLength(61)
                    .IsUnicode(false)
                    .HasColumnName("sourcerName");

                entity.Property(e => e.SubmissionDate)
                    .HasMaxLength(30)
                    .HasColumnName("submissionDate");

                entity.Property(e => e.SubmissionId).HasColumnName("submissionId");

                entity.Property(e => e.SubmissionStatusName)
                    .IsRequired()
                    .HasMaxLength(50)
                    .HasColumnName("submissionStatusName");

                entity.Property(e => e.SubmissionUuId)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasColumnName("submissionUuId");
            });

            modelBuilder.Entity<VwJobCandidateSubmissionHistory>(entity =>
            {
                entity.HasNoKey();

                entity.ToView("VwJobCandidateSubmissionHistories");

                entity.Property(e => e.CandidateSubmissionStatusId).HasColumnName("candidateSubmissionStatusId");

                entity.Property(e => e.CandidateUuId)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasColumnName("candidateUuId");

                entity.Property(e => e.CreateDate)
                    .HasColumnType("datetime")
                    .HasColumnName("createDate");

                entity.Property(e => e.LanguageId).HasColumnName("languageId");

                entity.Property(e => e.RejectionReason)
                    .HasMaxLength(500)
                    .HasColumnName("rejectionReason");

                entity.Property(e => e.StatusName)
                    .IsRequired()
                    .HasMaxLength(50)
                    .HasColumnName("statusName");
            });

            modelBuilder.Entity<VwJobSourcer>(entity =>
            {
                entity.HasNoKey();

                entity.ToView("VwJobSourcers");

                entity.Property(e => e.JobTitle)
                    .HasMaxLength(50)
                    .HasColumnName("jobTitle");

                entity.Property(e => e.JobUuId)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasColumnName("jobUuId");

                entity.Property(e => e.SourcerJobTitle)
                    .HasMaxLength(50)
                    .HasColumnName("sourcerJobTitle");

                entity.Property(e => e.SourcerName)
                    .IsRequired()
                    .HasMaxLength(61)
                    .IsUnicode(false)
                    .HasColumnName("sourcerName");

                entity.Property(e => e.SourcerUuId)
                    .IsRequired()
                    .HasMaxLength(50)
                    .HasColumnName("sourcerUuId");

                entity.Property(e => e.SubmissionCount).HasColumnName("submissionCount");
            });

            modelBuilder.Entity<VwJobSubmissionsSummary>(entity =>
            {
                entity.HasNoKey();

                entity.ToView("VwJobSubmissionsSummary");

                entity.Property(e => e.Commission)
                    .HasColumnType("decimal(10, 3)")
                    .HasColumnName("commission");

                entity.Property(e => e.CompanyIndustry)
                    .IsRequired()
                    .HasMaxLength(50)
                    .HasColumnName("companyIndustry");

                entity.Property(e => e.CreatedDate)
                    .HasColumnType("datetime")
                    .HasColumnName("createdDate");

                entity.Property(e => e.Description).HasColumnName("description");

                entity.Property(e => e.EmployerFname)
                    .IsRequired()
                    .HasMaxLength(30)
                    .IsUnicode(false)
                    .HasColumnName("employerFName");

                entity.Property(e => e.EmployerLname)
                    .IsRequired()
                    .HasMaxLength(30)
                    .IsUnicode(false)
                    .HasColumnName("employerLName");

                entity.Property(e => e.EmployerUuid)
                    .IsRequired()
                    .HasMaxLength(50)
                    .HasColumnName("employerUUId");

                entity.Property(e => e.JobDate)
                    .HasMaxLength(30)
                    .HasColumnName("jobDate");

                entity.Property(e => e.JobId).HasColumnName("jobId");

                entity.Property(e => e.JobLocationTranslation)
                    .IsRequired()
                    .HasMaxLength(50)
                    .HasColumnName("jobLocationTranslation");

                entity.Property(e => e.JobStatusName)
                    .IsRequired()
                    .HasMaxLength(50)
                    .HasColumnName("jobStatusName");

                entity.Property(e => e.JobTitle).HasMaxLength(50);

                entity.Property(e => e.LanguageId).HasColumnName("languageId");

                entity.Property(e => e.NoOfSubmissions).HasColumnName("noOfSubmissions");

                entity.Property(e => e.Reason)
                    .HasMaxLength(500)
                    .HasColumnName("reason");

                entity.Property(e => e.RemainingCount).HasColumnName("remainingCount");

                entity.Property(e => e.SubmissionCount).HasColumnName("submissionCount");

                entity.Property(e => e.Uuid)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasColumnName("UUID");
            });

            modelBuilder.Entity<VwJobTemplate>(entity =>
            {
                entity.HasNoKey();

                entity.ToView("VwJobTemplates");

                entity.Property(e => e.Description)
                    .IsRequired()
                    .HasColumnName("description");

                entity.Property(e => e.EmploymentTypeId).HasColumnName("employmentTypeId");

                entity.Property(e => e.EmploymentTypeName)
                    .IsRequired()
                    .HasMaxLength(50)
                    .HasColumnName("employmentTypeName");

                entity.Property(e => e.JobIndustryId).HasColumnName("jobIndustryId");

                entity.Property(e => e.JobLocation)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasColumnName("jobLocation");

                entity.Property(e => e.JobLocationId).HasColumnName("jobLocationId");

                entity.Property(e => e.JobTemplateId).HasColumnName("jobTemplateId");

                entity.Property(e => e.JobTitle)
                    .IsRequired()
                    .HasMaxLength(100)
                    .HasColumnName("jobTitle");

                entity.Property(e => e.LanguageId).HasColumnName("languageId");

                entity.Property(e => e.MustHaveQualification)
                    .IsRequired()
                    .HasMaxLength(1)
                    .IsUnicode(false)
                    .HasColumnName("mustHaveQualification");

                entity.Property(e => e.NiceToHaveQualification)
                    .IsRequired()
                    .HasMaxLength(1)
                    .IsUnicode(false)
                    .HasColumnName("niceToHaveQualification");

                entity.Property(e => e.Requirements)
                    .IsRequired()
                    .HasColumnName("requirements");
            });

            modelBuilder.Entity<VwSourcer>(entity =>
            {
                entity.HasNoKey();

                entity.ToView("VwSourcers");

                entity.Property(e => e.CreatedDate)
                    .HasMaxLength(30)
                    .HasColumnName("createdDate");

                entity.Property(e => e.CvName).HasMaxLength(50);

                entity.Property(e => e.Email)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.FirstName)
                    .IsRequired()
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.FullName)
                    .IsRequired()
                    .HasMaxLength(61)
                    .IsUnicode(false)
                    .HasColumnName("fullName");

                entity.Property(e => e.JobTitle).HasMaxLength(50);

                entity.Property(e => e.LastName)
                    .IsRequired()
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.LinkedInProfile).HasMaxLength(100);

                entity.Property(e => e.MeetingDate).HasColumnType("datetime");

                entity.Property(e => e.MeetingTimeId).HasColumnName("meetingTimeId");

                entity.Property(e => e.Password)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.PhoneNumber)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.RejectedReason)
                    .HasMaxLength(500)
                    .HasColumnName("rejectedReason");

                entity.Property(e => e.SourcerId).HasColumnName("sourcerId");

                entity.Property(e => e.TimeZoneId).HasColumnName("timeZoneId");

                entity.Property(e => e.UpdatedDate).HasColumnType("datetime");

                entity.Property(e => e.UserStatusId).HasColumnName("userStatusId");

                entity.Property(e => e.Uuid)
                    .IsRequired()
                    .HasMaxLength(50)
                    .HasColumnName("UUID");
            });

            modelBuilder.Entity<VwSourcerBalance>(entity =>
            {
                entity.HasNoKey();

                entity.ToView("VwSourcerBalances");

                entity.Property(e => e.CandidateCommission)
                    .HasColumnType("decimal(10, 3)")
                    .HasColumnName("candidateCommission");

                entity.Property(e => e.CandidateName)
                    .HasMaxLength(150)
                    .HasColumnName("candidateName");

                entity.Property(e => e.CandidateUuId)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasColumnName("candidateUuId");

                entity.Property(e => e.EmployerName)
                    .IsRequired()
                    .HasMaxLength(61)
                    .IsUnicode(false)
                    .HasColumnName("employerName");

                entity.Property(e => e.JobTitle).HasMaxLength(50);

                entity.Property(e => e.SourcerUuid)
                    .IsRequired()
                    .HasMaxLength(50)
                    .HasColumnName("sourcerUuid");

                entity.Property(e => e.SubmissionDate)
                    .HasMaxLength(30)
                    .HasColumnName("submissionDate");
            });

            modelBuilder.Entity<VwSourcerBalancesSummary>(entity =>
            {
                entity.HasNoKey();

                entity.ToView("VwSourcerBalancesSummary");

                entity.Property(e => e.ApprovedCount).HasColumnName("approvedCount");

                entity.Property(e => e.DeclinedCount).HasColumnName("declinedCount");

                entity.Property(e => e.Rate).HasColumnName("rate");

                entity.Property(e => e.SourcerUuid)
                    .IsRequired()
                    .HasMaxLength(50)
                    .HasColumnName("sourcerUuid");

                entity.Property(e => e.TotalBalance)
                    .HasColumnType("decimal(38, 3)")
                    .HasColumnName("totalBalance");
            });

            modelBuilder.Entity<VwSourcerSubmission>(entity =>
            {
                entity.HasNoKey();

                entity.ToView("VwSourcerSubmissions");

                entity.Property(e => e.CandidateName)
                    .HasMaxLength(150)
                    .HasColumnName("candidateName");

                entity.Property(e => e.CandidateStatusName)
                    .IsRequired()
                    .HasMaxLength(50)
                    .HasColumnName("candidateStatusName");

                entity.Property(e => e.CvName)
                    .IsRequired()
                    .HasMaxLength(50)
                    .HasColumnName("cvName");

                entity.Property(e => e.EmployerId).HasColumnName("employerId");

                entity.Property(e => e.EmployerUuid)
                    .IsRequired()
                    .HasMaxLength(50)
                    .HasColumnName("employerUUId");

                entity.Property(e => e.IsActive).HasColumnName("isActive");

                entity.Property(e => e.JobId).HasColumnName("jobId");

                entity.Property(e => e.JobLocationTranslation)
                    .IsRequired()
                    .HasMaxLength(50)
                    .HasColumnName("jobLocationTranslation");

                entity.Property(e => e.JobTitle)
                    .HasMaxLength(50)
                    .HasColumnName("jobTitle");

                entity.Property(e => e.LanguageId).HasColumnName("languageId");

                entity.Property(e => e.RejectionReason)
                    .HasMaxLength(500)
                    .HasColumnName("rejectionReason");

                entity.Property(e => e.RowNumber).HasColumnName("rowNumber");

                entity.Property(e => e.SourcerId).HasColumnName("sourcerId");

                entity.Property(e => e.UuId)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasColumnName("uuId");
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
