﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Data.Tables
{
    public partial class VwInterviewDetail
    {
        public int Id { get; set; }
        public int InterviewerId { get; set; }
        public string InterviewComment { get; set; }
        public int InterviewTypeId { get; set; }
        public DateTime InterviewDate { get; set; }
        public TimeSpan InterviewTime { get; set; }
        public int Duration { get; set; }
        public bool IsCanceled { get; set; }
        public bool IsEmailSent { get; set; }
        public int CreatedBy { get; set; }
        public int? TemplateId { get; set; }
        public string EmailSubject { get; set; }
        public string EmailDescription { get; set; }
        public int? InterviewerTypeId { get; set; }
        public DateTime CreatedDate { get; set; }
        public string SubmissionUuid { get; set; }
    }
}
