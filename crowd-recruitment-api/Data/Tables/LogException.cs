﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Data.Tables
{
    public partial class LogException
    {
        public long Id { get; set; }
        public DateTime? LogDate { get; set; }
        public string AssemblyName { get; set; }
        public string ObjectName { get; set; }
        public string MethodName { get; set; }
        public string InnerException { get; set; }
        public string ErrorMessage { get; set; }
    }
}
