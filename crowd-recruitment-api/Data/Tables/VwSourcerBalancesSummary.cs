﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Data.Tables
{
    public partial class VwSourcerBalancesSummary
    {
        public string SourcerUuid { get; set; }
        public int? SourcerId { get; set; }
        public int? ApprovedCount { get; set; }
        public int? DeclinedCount { get; set; }
        public decimal? TotalBalance { get; set; }
        public int? Rate { get; set; }
    }
}
