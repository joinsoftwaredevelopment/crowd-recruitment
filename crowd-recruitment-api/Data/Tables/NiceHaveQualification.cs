﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Data.Tables
{
    public partial class NiceHaveQualification
    {
        public int Id { get; set; }
        public string Uuid { get; set; }
        public DateTime CreatedDate { get; set; }
    }
}
