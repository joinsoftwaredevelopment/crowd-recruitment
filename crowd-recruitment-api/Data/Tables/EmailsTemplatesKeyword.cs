﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Data.Tables
{
    public partial class EmailsTemplatesKeyword
    {
        public int Id { get; set; }
        public string Keyword { get; set; }
    }
}
