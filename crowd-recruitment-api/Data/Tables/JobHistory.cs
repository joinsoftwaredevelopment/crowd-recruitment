﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Data.Tables
{
    public partial class JobHistory
    {
        public int Id { get; set; }
        public int JobStatusId { get; set; }
        public string Comment { get; set; }
        public int JobId { get; set; }
        public DateTime CreateDate { get; set; }
    }
}
