﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Data.Tables
{
    public partial class PackagesFeature
    {
        public int Id { get; set; }
        public string Uuid { get; set; }
        public string Text { get; set; }
        public int PackageId { get; set; }
    }
}
