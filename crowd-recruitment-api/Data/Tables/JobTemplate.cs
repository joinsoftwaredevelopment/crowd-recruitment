﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Data.Tables
{
    public partial class JobTemplate
    {
        public int Id { get; set; }
        public int JobIndustryId { get; set; }
        public string JobTitle { get; set; }
        public string JobDescription { get; set; }
        public string JobRequirements { get; set; }
        public int JobLocationId { get; set; }
        public int? EmploymentTypeId { get; set; }
        public int LanguageId { get; set; }
    }
}
