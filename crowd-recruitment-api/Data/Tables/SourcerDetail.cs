﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Data.Tables
{
    public partial class SourcerDetail
    {
        public int Id { get; set; }
        public int UserId { get; set; }
        public int? ExperienceTypeId { get; set; }
        public string JobTitle { get; set; }
        public bool? HasAccessToDifferentWebsite { get; set; }
        public int? IndustryId { get; set; }
        public string CvName { get; set; }
        public string LinkedInProfile { get; set; }

        public virtual User User { get; set; }
    }
}
