﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Data.Tables
{
    public partial class Country
    {
        public Country()
        {
            CountriesTranslations = new HashSet<CountriesTranslation>();
            Jobs = new HashSet<Job>();
        }

        public int Id { get; set; }
        public DateTime CreatedDate { get; set; }
        public string Iso3 { get; set; }
        public string Iso2 { get; set; }
        public string Phonecode { get; set; }
        public string Currency { get; set; }

        public virtual ICollection<CountriesTranslation> CountriesTranslations { get; set; }
        public virtual ICollection<Job> Jobs { get; set; }
    }
}
