﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Data.Tables
{
    public partial class JobCandidateSubmissionHistory
    {
        public int Id { get; set; }
        public int CandidateSubmissionStatusId { get; set; }
        public string Comment { get; set; }
        public int JobCandidatesubmissionId { get; set; }
        public DateTime CreateDate { get; set; }
    }
}
