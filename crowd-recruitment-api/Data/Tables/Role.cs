﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Data.Tables
{
    public partial class Role
    {
        public int Id { get; set; }
        public string Uuid { get; set; }
        public string CreatedDate { get; set; }
    }
}
