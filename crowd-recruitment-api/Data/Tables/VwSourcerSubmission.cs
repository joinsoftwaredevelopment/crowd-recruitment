﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Data.Tables
{
    public partial class VwSourcerSubmission
    {
        public long? RowNumber { get; set; }
        public int JobId { get; set; }
        public string UuId { get; set; }
        public string JobTitle { get; set; }
        public bool IsActive { get; set; }
        public int LanguageId { get; set; }
        public string JobLocationTranslation { get; set; }
        public int SourcerId { get; set; }
        public string CvName { get; set; }
        public int EmployerId { get; set; }
        public string EmployerUuid { get; set; }
        public string CandidateName { get; set; }
        public string RejectionReason { get; set; }
        public int CandidateSubmissionStatusId { get; set; }
        public string CandidateStatusName { get; set; }
    }
}
