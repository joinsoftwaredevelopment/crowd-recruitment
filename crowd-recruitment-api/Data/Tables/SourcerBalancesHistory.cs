﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Data.Tables
{
    public partial class SourcerBalancesHistory
    {
        public int Id { get; set; }
        public DateTime CreatedDate { get; set; }
        public string WeekTitle { get; set; }
        public int SourcerId { get; set; }
        public string FullName { get; set; }
        public int JobCount { get; set; }
        public int SubmissionCount { get; set; }
        public int AcceptanceCount { get; set; }
        public int DeclinedCount { get; set; }
        public string AcceptanceRatio { get; set; }
        public string TotalBalance { get; set; }
        public bool IsPaid { get; set; }
    }
}
