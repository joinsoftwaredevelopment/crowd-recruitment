﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Data.Tables
{
    public partial class JobTemplateMustHaveQualification
    {
        public int Id { get; set; }
        public int JobTemplateId { get; set; }
        public string Text { get; set; }
    }
}
