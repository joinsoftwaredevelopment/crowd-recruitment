﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Data.Tables
{
    public partial class VwJobTemplate
    {
        public int JobTemplateId { get; set; }
        public string JobTitle { get; set; }
        public string Description { get; set; }
        public string Requirements { get; set; }
        public int JobLocationId { get; set; }
        public string JobLocation { get; set; }
        public int? EmploymentTypeId { get; set; }
        public string EmploymentTypeName { get; set; }
        public int LanguageId { get; set; }
        public int JobIndustryId { get; set; }
        public string NiceToHaveQualification { get; set; }
        public string MustHaveQualification { get; set; }
    }
}
