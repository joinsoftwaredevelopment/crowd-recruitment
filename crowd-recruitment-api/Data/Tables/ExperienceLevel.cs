﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Data.Tables
{
    public partial class ExperienceLevel
    {
        public ExperienceLevel()
        {
            Jobs = new HashSet<Job>();
        }

        public int Id { get; set; }
        public string Uuid { get; set; }
        public DateTime CreatedDate { get; set; }

        public virtual ICollection<Job> Jobs { get; set; }
    }
}
