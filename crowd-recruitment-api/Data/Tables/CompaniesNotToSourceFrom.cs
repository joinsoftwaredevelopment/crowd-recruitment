﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Data.Tables
{
    public partial class CompaniesNotToSourceFrom
    {
        public int Id { get; set; }
        public string Uuid { get; set; }
        public int JobId { get; set; }
        public string Text { get; set; }
    }
}
