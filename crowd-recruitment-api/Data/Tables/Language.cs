﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Data.Tables
{
    public partial class Language
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
