﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Data.Tables
{
    public partial class VwJobCandidate
    {
        public long? RowNumber { get; set; }
        public int JobId { get; set; }
        public string JobUuId { get; set; }
        public string JobTitle { get; set; }
        public int? JobStatusId { get; set; }
        public string FirstMailSubject { get; set; }
        public string FirstMailDescription { get; set; }
        public string JobDate { get; set; }
        public int SourcerId { get; set; }
        public bool? IsCvApproved { get; set; }
        public string SourcerName { get; set; }
        public string SubmissionUuId { get; set; }
        public int SubmissionId { get; set; }
        public string CandidateName { get; set; }
        public string CvName { get; set; }
        public int? HiringStageId { get; set; }
        public bool? IsAcceptOffer { get; set; }
        public bool? IsHired { get; set; }
        public string LinkedInProfile { get; set; }
        public string Email { get; set; }
        public string CountryName { get; set; }
        public string SubmissionDate { get; set; }
        public int? LanguageId { get; set; }
        public string RejectionReason { get; set; }
        public int CandidateSubmissionStatusId { get; set; }
        public string SubmissionStatusName { get; set; }
        public int? HasInterview { get; set; }
    }
}
