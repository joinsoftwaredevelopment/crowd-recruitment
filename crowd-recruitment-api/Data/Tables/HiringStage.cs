﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Data.Tables
{
    public partial class HiringStage
    {
        public int Id { get; set; }
        public DateTime? CreatedDate { get; set; }
    }
}
