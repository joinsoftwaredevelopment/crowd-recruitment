
ALTER TABLE Job.Job
ADD MinSalary decimal(10, 3);

ALTER TABLE Job.Job
ADD MaxSalary decimal(10, 3);

ALTER TABLE Job.Job
ADD CurrencyId int;