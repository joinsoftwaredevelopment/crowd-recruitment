USE [recruitment]
GO

/****** Object:  View [dbo].[VwAdminSourcersBalances]    Script Date: 2021-02-17 8:44:14 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




ALTER VIEW [dbo].[VwAdminSourcersBalances]
AS
Select (FirstName + ' ' + LastName) as fullName ,
  (Select Count(*) from Job.Job where UserId = Users.Id) as jobCount ,
  (Select Count(*) from Sourcer.JobCandidateSubmission where JobCandidateSubmission.SourcerId = Users.Id) as submissionCount,
  (Select ISNULL(SUM(ISNULL(commission,0)),0) from Sourcer.JobCandidateSubmission where JobCandidateSubmission.SourcerId = Users.Id) as totalBalance
from Common.Users
Join Sourcer.SourcerDetails ON Users.Id = SourcerDetails.UserId
GO


