USE [recruitment]
GO

/****** Object:  Table [Employer].[HiringStages]    Script Date: 25-Mar-21 1:14:55 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [Employer].[HiringStages](
	[id] [int] NOT NULL,
	[createdDate] [datetime] NULL,
 CONSTRAINT [PK_HiringStages] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO


