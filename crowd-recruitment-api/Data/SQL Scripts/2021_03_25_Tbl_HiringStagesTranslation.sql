USE [recruitment]
GO

/****** Object:  Table [Employer].[HiringStagesTransaltion]    Script Date: 25-Mar-21 1:15:02 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [Employer].[HiringStagesTransaltion](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[stageId] [int] NULL,
	[languageId] [int] NULL,
	[translation] [nvarchar](50) NULL,
 CONSTRAINT [PK_HiringStagesTransaltion] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO


