USE [recruitment]
GO

/****** Object:  Table [Common].[Packages]    Script Date: 2021-04-20 12:28:40 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [Common].[Packages](
	[packageId] [int] IDENTITY(1,1) NOT NULL,
	[packageTitle] [nvarchar](200) NOT NULL,
	[packagePrice] [decimal](10, 3) NOT NULL,
	[packageDescription] [nvarchar](max) NOT NULL,
 CONSTRAINT [PK_Packages] PRIMARY KEY CLUSTERED 
(
	[packageId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO


