USE [recruitment]
GO

/****** Object:  Table [Job].[JobTemplates]    Script Date: 2021-03-08 11:44:19 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [Job].[JobTemplates](
	[id] [int] NOT NULL,
	[jobIndustryId] [int] NOT NULL,
	[jobTitle] [nvarchar](100) NOT NULL,
	[jobDescription] [nvarchar](max) NOT NULL,
	[jobRequirements] [nvarchar](max) NOT NULL,
	[jobLocationId] [int] NOT NULL,
	[employmentTypeId] [int] NULL,
	[languageId] [int] NOT NULL,
 CONSTRAINT [PK_JobTemplates] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO


