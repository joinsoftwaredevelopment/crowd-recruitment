USE [recruitment]
GO
/****** Object:  Trigger [Sourcer].[ChangeSubmissionStatus]    Script Date: 2021-04-20 2:37:28 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER TRIGGER [Sourcer].[ChangeSubmissionStatus]
   ON  [Sourcer].[JobCandidateSubmission]
   AFTER INSERT,UPDATE
AS 
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for trigger here

    Declare @submissionId int = (Select Id from inserted)
	Declare @submissionStatusId int =  (Select CandidateSubmissionStatusId from [Sourcer].[JobCandidateSubmission] where [JobCandidateSubmission].Id =@submissionId )
	Declare @insertedCandidateSubmissionStatusId int = (Select top 1 CandidateSubmissionStatusId from [Sourcer].[JobCandidateSubmissionHistory] where 
	 (CandidateSubmissionStatusId = @submissionStatusId) AND [JobCandidateSubmissionHistory].JobCandidatesubmissionId =@submissionId order by id desc )

	IF((@insertedCandidateSubmissionStatusId != @submissionStatusId) OR (@insertedCandidateSubmissionStatusId IS NULL))
	INSERT INTO [Sourcer].[JobCandidateSubmissionHistory]
           ([CandidateSubmissionStatusId]
           ,[Comment]
           ,[JobCandidatesubmissionId]
           ,[createDate])
     VALUES
           (@submissionStatusId
           ,NULL
           ,@submissionId
           ,Convert(datetime,(SELECT GETUTCDATE() AT TIME ZONE 'UTC' AT TIME ZONE 'Egypt Standard Time') ))


END
