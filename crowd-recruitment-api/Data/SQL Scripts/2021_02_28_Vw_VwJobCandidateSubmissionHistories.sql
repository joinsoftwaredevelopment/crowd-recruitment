USE [recruitment]
GO

/****** Object:  View [dbo].[VwJobCandidateSubmissionHistories]    Script Date: 2021-02-28 4:15:15 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


Create VIEW [dbo].[VwJobCandidateSubmissionHistories]
AS
SELECT        [CandidateSubmissionStatus].Id as candidateSubmissionStatusId , 
[JobCandidateSubmission].UUID as candidateUuId , [Translation] as statusName ,  [languageId] ,
[JobCandidateSubmissionHistory].createDate
FROM            [Sourcer].[JobCandidateSubmissionHistory] INNER JOIN
                [Employer].[CandidateSubmissionStatus]
			    ON [CandidateSubmissionStatus].Id = [JobCandidateSubmissionHistory].CandidateSubmissionStatusId
				INNER JOIN [Employer].[CandidateSubmissionStatusTranslation] ON  
				[CandidateSubmissionStatusTranslation].CandidateSubmissionStatusId=[CandidateSubmissionStatus].Id 
				INNER JOIN [Sourcer].[JobCandidateSubmission] ON [JobCandidateSubmission].Id = [JobCandidateSubmissionHistory].JobCandidatesubmissionId
GO


