﻿USE [recruitment]
GO

INSERT INTO [Employer].[CandidateSubmissionStatus]
           ([UUID]
           ,[CreatedDate])
     VALUES
           (5
           ,'2021-02-25')
GO

INSERT INTO [Employer].[CandidateSubmissionStatus]
           ([UUID]
           ,[CreatedDate])
     VALUES
           (6
           ,'2021-02-25')
GO

--==========================================================================================

INSERT INTO [Employer].[CandidateSubmissionStatusTranslation]
           ([CandidateSubmissionStatusId]
           ,[Translation]
           ,[languageId])
     VALUES
           (5
           ,N'Approved By Employer'
           ,2)
GO

INSERT INTO [Employer].[CandidateSubmissionStatusTranslation]
           ([CandidateSubmissionStatusId]
           ,[Translation]
           ,[languageId])
     VALUES
           (5
           ,N'تمت الموافقة من مقدم الوظيفة'
           ,1)
GO

INSERT INTO [Employer].[CandidateSubmissionStatusTranslation]
           ([CandidateSubmissionStatusId]
           ,[Translation]
           ,[languageId])
     VALUES
           (6
           ,N'Declined By Employer'
           ,2)
GO

INSERT INTO [Employer].[CandidateSubmissionStatusTranslation]
           ([CandidateSubmissionStatusId]
           ,[Translation]
           ,[languageId])
     VALUES
           (6
           ,N'تمت الرفض من مقدم الوظيفة'
           ,1)
GO


