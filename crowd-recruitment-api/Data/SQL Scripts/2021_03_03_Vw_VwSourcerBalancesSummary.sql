USE [recruitment]
GO

/****** Object:  View [dbo].[VwSourcerBalancesSummary]    Script Date: 2021-03-03 11:41:59 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


ALTER VIEW [dbo].[VwSourcerBalancesSummary]
AS
SELECT        Common.Users.UUID AS sourcerUuid, JobCandidateSubmission_1.SourcerId,
                             (SELECT        COUNT(*) AS approvedCount
                               FROM            Sourcer.JobCandidateSubmission
                               WHERE        (SourcerId = Common.Users.Id) AND (CandidateSubmissionStatusId = 3 Or CandidateSubmissionStatusId = 5 )) AS approvedCount,
                             (SELECT        COUNT(*) AS declinedCount
                               FROM            Sourcer.JobCandidateSubmission AS JobCandidateSubmission_2
                               WHERE        (SourcerId = Common.Users.Id) AND (CandidateSubmissionStatusId = 4 OR CandidateSubmissionStatusId = 6 )) AS declinedCount,
                             (SELECT        ISNULL(SUM(ISNULL(commission, 0)), 0) AS totalBalance
                               FROM            Sourcer.JobCandidateSubmission AS JobCandidateSubmission_3
                               WHERE        (SourcerId = Common.Users.Id) AND (CandidateSubmissionStatusId = 3 OR CandidateSubmissionStatusId = 5)) AS totalBalance,
                             (SELECT        COUNT(*) AS approvedCount
                               FROM            Sourcer.JobCandidateSubmission AS JobCandidateSubmission_4
                               WHERE        (SourcerId = Common.Users.Id) AND (CandidateSubmissionStatusId = 3 or CandidateSubmissionStatusId = 5  )) * 100 / COUNT(*) AS rate
FROM             Common.Users Left JOIN
                 Sourcer.JobCandidateSubmission AS JobCandidateSubmission_1  ON Common.Users.Id = JobCandidateSubmission_1.SourcerId
GROUP BY Common.Users.UUID, JobCandidateSubmission_1.SourcerId, Common.Users.Id
GO


