USE [recruitment]
GO

/****** Object:  Trigger [Sourcer].[ChangeSubmissionStatus]    Script Date: 2021-02-28 3:38:53 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER TRIGGER [Sourcer].[ChangeSubmissionStatus]
   ON  [Sourcer].[JobCandidateSubmission]
   AFTER INSERT,UPDATE
AS 
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for trigger here

    Declare @submissionId int = (Select Id from inserted)
	Declare @submissionStatusId int = (Select CandidateSubmissionStatusId from inserted)
	Declare @empSubmissionStatusId int = (Select EmpSubmissionStatusId from inserted)

	Declare @IsEmpStatusAdded int = (Select CandidateSubmissionStatusId from [JobCandidateSubmissionHistory] where CandidateSubmissionStatusId = @empSubmissionStatusId AND JobCandidatesubmissionId = @submissionId )

	if(@IsEmpStatusAdded is null AND  @empSubmissionStatusId is null)

	INSERT INTO [Sourcer].[JobCandidateSubmissionHistory]
           ([CandidateSubmissionStatusId]
           ,[Comment]
           ,[JobCandidatesubmissionId]
           ,[createDate])
     VALUES
           (@submissionStatusId
           ,NULL
           ,@submissionId
           ,GETDATE())

    else

	INSERT INTO [Sourcer].[JobCandidateSubmissionHistory]
           ([CandidateSubmissionStatusId]
           ,[Comment]
           ,[JobCandidatesubmissionId]
           ,[createDate])
     VALUES
           (@empSubmissionStatusId
           ,NULL
           ,@submissionId
           ,GETDATE())

END
GO


