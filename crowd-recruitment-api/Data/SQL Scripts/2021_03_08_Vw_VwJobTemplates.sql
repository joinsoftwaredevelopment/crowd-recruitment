USE [recruitment]
GO

/****** Object:  View [dbo].[VwJobTemplates]    Script Date: 2021-03-08 12:12:35 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


Create VIEW [dbo].[VwJobTemplates]
AS
SELECT           Job.JobTemplates.id as jobTemplateId , jobTitle , jobDescription , jobRequirements ,Job.JobTemplates.jobLocationId , JobLocationTranslation.Translation as locationName,
                Job.JobTemplates.employmentTypeId , EmploymentTypeTranslation.Translation as employmentTypeName , Job.JobTemplates.languageId , jobIndustryId
FROM            Job.JobTemplates INNER JOIN
                         Employer.EmploymentType ON Job.JobTemplates.employmentTypeId = Employer.EmploymentType.Id INNER JOIN
                         Employer.EmploymentTypeTranslation ON Employer.EmploymentType.Id = Employer.EmploymentTypeTranslation.EmploymentTypeId INNER JOIN
                         Common.Languages ON Common.Languages.id = Employer.EmploymentTypeTranslation.languageId INNER JOIN
                         Job.JobLocation ON Job.JobLocation.Id = Job.JobTemplates.jobLocationId INNER JOIN
                         Job.JobLocationTranslation ON Job.JobLocationTranslation.JobLocationId = Job.JobLocation.Id AND Job.JobLocationTranslation.languageId = Common.Languages.id INNER JOIN
                         Employer.CompanyIndustry ON Job.JobTemplates.jobIndustryId = Employer.CompanyIndustry.Id INNER JOIN
                         Employer.CompanyIndustryTranslation ON Employer.CompanyIndustryTranslation.CompanyIndustryId = Employer.CompanyIndustry.Id AND 
                         Employer.CompanyIndustryTranslation.languageId = Job.JobLocationTranslation.languageId
GO


