USE [recruitment]
GO

/****** Object:  View [dbo].[VwSourcerBalancesSummary]    Script Date: 2021-02-16 9:23:01 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[VwSourcerBalancesSummary]
AS
SELECT        Common.Users.UUID AS sourcerUuid, JobCandidateSubmission_1.SourcerId AS sourcerId,
                             (SELECT        COUNT(*) AS approvedCount
                               FROM            Sourcer.JobCandidateSubmission
                               WHERE        (SourcerId = Common.Users.Id) AND (CandidateSubmissionStatusId = 3)) AS approvedCount,
                             (SELECT        COUNT(*) AS declinedCount
                               FROM            Sourcer.JobCandidateSubmission AS JobCandidateSubmission_2
                               WHERE        (SourcerId = Common.Users.Id) AND (CandidateSubmissionStatusId = 4)) AS declinedCount,
							 (SELECT        SUM(ISNULL(JobCandidateSubmission_3.commission,0)) AS declinedCount
                               FROM            Sourcer.JobCandidateSubmission AS JobCandidateSubmission_3
                               WHERE        (SourcerId = Common.Users.Id) AND (CandidateSubmissionStatusId = 3)) AS totalBalance
FROM            Sourcer.JobCandidateSubmission AS JobCandidateSubmission_1 INNER JOIN
                         Common.Users ON Common.Users.Id = JobCandidateSubmission_1.SourcerId
GROUP BY Common.Users.UUID, JobCandidateSubmission_1.SourcerId, Common.Users.Id
GO



