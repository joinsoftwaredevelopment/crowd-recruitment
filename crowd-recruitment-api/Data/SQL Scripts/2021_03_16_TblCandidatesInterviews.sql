USE [recruitment]
GO

/****** Object:  Table [Employer].[CandidatesInterviews]    Script Date: 16-Mar-21 3:02:10 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [Employer].[CandidatesInterviews](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[candidateId] [int] NOT NULL,
	[interviewerId] [int] NOT NULL,
	[interviewComment] [nvarchar](500) NOT NULL,
	[interviewTypeId] [int] NOT NULL,
	[interviewerTypeId] [int] NULL,
	[interviewDate] [date] NOT NULL,
	[interviewTime] [time](7) NOT NULL,
	[duration] [int] NOT NULL,
	[templateId] [int] NULL,
	[emailSubject] [nvarchar](100) NULL,
	[emailDescription] [nvarchar](1000) NULL,
	[isCanceled] [bit] NOT NULL,
	[isEmailSent] [bit] NOT NULL,
	[createdBy] [int] NOT NULL,
	[createdDate] [datetime] NOT NULL,
 CONSTRAINT [PK_CandidatesInterviews] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO


