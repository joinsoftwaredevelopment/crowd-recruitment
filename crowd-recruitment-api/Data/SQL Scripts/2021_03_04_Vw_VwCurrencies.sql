USE [recruitment]
GO

/****** Object:  View [dbo].[VwCountries]    Script Date: 2021-03-04 3:10:57 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

Create VIEW [dbo].[VwCurrencies]
AS
SELECT        Common.CurrencyTranslation.currencyId, Common.CurrencyTranslation.shortName AS shortName, Common.CurrencyTranslation.name AS currencyName, Common.CurrencyTranslation.languageId
FROM            Common.Currencies INNER JOIN
                         Common.CurrencyTranslation ON Common.Currencies.id = Common.CurrencyTranslation.currencyId
GO


