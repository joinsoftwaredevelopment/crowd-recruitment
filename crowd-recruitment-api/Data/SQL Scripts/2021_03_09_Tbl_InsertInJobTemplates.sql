USE [recruitment]
GO
INSERT [Job].[JobTemplates] ([id], [jobIndustryId], [jobTitle], [jobDescription], [jobRequirements], [jobLocationId], [employmentTypeId], [languageId]) VALUES (1, 1, N'Data Analyst / Data Scientist
', N'Job role:

As a data analyst, you will be responsible for compiling actionable insights from data and assisting program, sales and marketing managers build data-driven processes. Your role will involve driving initiatives to optimize for operational excellence and revenue.


Responsibilities:

Ensure that data flows smoothly from source to destination so that it can be processed
Utilize strong database skills to work with large, complex data sets to extract insights
Filter and cleanse unstructured (or ambiguous) data into usable data sets that can be analyzed to extract insights and improve business processes
Identify new internal and external data sources to support analytics initiatives and work with appropriate partners to absorb the data into new or existing data infrastructure
Build tools for automating repetitive asks so that bandwidth can be freed for analytics
Collaborate with program managers and business analysts  to help them come up with actionable, high-impact insights across product lines and functions
Work closely with top management to prioritize information and analytic needs', N'Bachelors or Masters in a quantitative field (such as Engineering, Statistics, Math, Economics, or Computer Science with Modeling/Data Science), preferably with work experience of over [X] years.
Ability to program in any high level language is required. Familiarity with R and statistical packages are preferred.
Proven problem solving and debugging skills.
Familiar with database technologies and tools (SQL/R/SAS/JMP etc.), data warehousing, transformation and processing. Work experience with real data for customer insights, business and market analysis will be advantageous.
Experience with text analytics, data mining and social media analytics.
Statistical knowledge in standard techniques: Logistic Regression, Classification models, Cluster Analysis, Neural Networks, Random Forests, Ensembles, etc.', 189, 1, 1)
INSERT [Job].[JobTemplates] ([id], [jobIndustryId], [jobTitle], [jobDescription], [jobRequirements], [jobLocationId], [employmentTypeId], [languageId]) VALUES (2, 1, N'Data Analyst / Data Scientist
', N'Job role:

As a data analyst, you will be responsible for compiling actionable insights from data and assisting program, sales and marketing managers build data-driven processes. Your role will involve driving initiatives to optimize for operational excellence and revenue.


Responsibilities:

Ensure that data flows smoothly from source to destination so that it can be processed
Utilize strong database skills to work with large, complex data sets to extract insights
Filter and cleanse unstructured (or ambiguous) data into usable data sets that can be analyzed to extract insights and improve business processes
Identify new internal and external data sources to support analytics initiatives and work with appropriate partners to absorb the data into new or existing data infrastructure
Build tools for automating repetitive asks so that bandwidth can be freed for analytics
Collaborate with program managers and business analysts  to help them come up with actionable, high-impact insights across product lines and functions
Work closely with top management to prioritize information and analytic needs', N'Bachelors or Masters in a quantitative field (such as Engineering, Statistics, Math, Economics, or Computer Science with Modeling/Data Science), preferably with work experience of over [X] years.
Ability to program in any high level language is required. Familiarity with R and statistical packages are preferred.
Proven problem solving and debugging skills.
Familiar with database technologies and tools (SQL/R/SAS/JMP etc.), data warehousing, transformation and processing. Work experience with real data for customer insights, business and market analysis will be advantageous.
Experience with text analytics, data mining and social media analytics.
Statistical knowledge in standard techniques: Logistic Regression, Classification models, Cluster Analysis, Neural Networks, Random Forests, Ensembles, etc.', 189, 1, 2)
INSERT [Job].[JobTemplates] ([id], [jobIndustryId], [jobTitle], [jobDescription], [jobRequirements], [jobLocationId], [employmentTypeId], [languageId]) VALUES (3, 1, N'Business Development Representative
', N'Responsibilities:

Qualify leads from marketing campaigns as sales opportunities
Contact potential clients via phone and emails
Identify client needs and suggest appropriate products/services
Customize product solutions to increase customer satisfaction
Build long-term trusting relationships with clients
Proactively seek new business opportunities in the market
Set up meetings or calls between (prospective) clients and Account Executives
Build reports to present to the manager.
Stay up-to-date with new products/services and new pricing/payment plans
', N'Requirements:

Proven work experience as a Business Development Representative, Sales Account Executive or similar role
Hands-on experience with multiple sales techniques (eg: inbound, outbound, field sales)
Track record of achieving sales quotas
Experience with CRM software.
Fluency in MS Excel.
Understanding of sales performance metrics
Excellent communication and negotiation skills
Ability to deliver engaging presentations
BSc degree in Marketing, Business Administration or relevant field.', 189, 1, 1)
INSERT [Job].[JobTemplates] ([id], [jobIndustryId], [jobTitle], [jobDescription], [jobRequirements], [jobLocationId], [employmentTypeId], [languageId]) VALUES (4, 1, N'Business Development Representative
', N'Responsibilities:

Qualify leads from marketing campaigns as sales opportunities
Contact potential clients via phone and emails
Identify client needs and suggest appropriate products/services
Customize product solutions to increase customer satisfaction
Build long-term trusting relationships with clients
Proactively seek new business opportunities in the market
Set up meetings or calls between (prospective) clients and Account Executives
Build reports to present to the manager.
Stay up-to-date with new products/services and new pricing/payment plans
', N'Requirements:

Proven work experience as a Business Development Representative, Sales Account Executive or similar role
Hands-on experience with multiple sales techniques (eg: inbound, outbound, field sales)
Track record of achieving sales quotas
Experience with CRM software.
Fluency in MS Excel.
Understanding of sales performance metrics
Excellent communication and negotiation skills
Ability to deliver engaging presentations
BSc degree in Marketing, Business Administration or relevant field.', 189, 1, 2)
INSERT [Job].[JobTemplates] ([id], [jobIndustryId], [jobTitle], [jobDescription], [jobRequirements], [jobLocationId], [employmentTypeId], [languageId]) VALUES (5, 2, N'Software Developer / Software Engineer
', N' Responsibilities:

Familiar with the software development life cycle (SDLC) from analysis to deployment.
Comply with coding standards and technical design.
Believes in systematic approach to developing the system through clear documentation (flowcharts, layouts, & etc) of functionality, address every use case through creative solutions.
Adapts structured coding styles for easy review, testing and maintainability of the code.
Integrate the developed functionality and/or component into a fully functional system.
Ensure unit and integration level verification plan are in place and adheres to great quality of code at all time.
Active participate in troubleshooting, debugging and updating current live system.
Verify user feedback in making system more stable and easy.
Work closely with analysts, designers and other peer developers.
Preparing technical training documents for onboarding new engineers.', N'Requirements:

Bachelor�s degree in computer science or equivalent practical experience.
2+ years of experience as Software Engineer or Software Developer or in a relevant role.
Understanding of OOPS concepts, Persistence, Threading.
Proficient in Java, C++, Ruby on Rails or other programming languages.
Hands-on with SQL or NoSQL database.
Competent with developing web apps in popular web frameworks (ASP .Net, JQuery, Apache Wicket, JavaServer Faces (JSF) & Spring MVC etc,).
Knowledgeable in data structures and algorithms.
Experience with open-source projects.
Experience in designing interactive applications.
A background in Engineering with sound oral and written communication skills.
Prior experience with AWS, Azure, Google or Openstack will be a plus.', 189, 1, 1)
INSERT [Job].[JobTemplates] ([id], [jobIndustryId], [jobTitle], [jobDescription], [jobRequirements], [jobLocationId], [employmentTypeId], [languageId]) VALUES (6, 2, N'Software Developer / Software Engineer', N' Responsibilities:

Familiar with the software development life cycle (SDLC) from analysis to deployment.
Comply with coding standards and technical design.
Believes in systematic approach to developing the system through clear documentation (flowcharts, layouts, & etc) of functionality, address every use case through creative solutions.
Adapts structured coding styles for easy review, testing and maintainability of the code.
Integrate the developed functionality and/or component into a fully functional system.
Ensure unit and integration level verification plan are in place and adheres to great quality of code at all time.
Active participate in troubleshooting, debugging and updating current live system.
Verify user feedback in making system more stable and easy.
Work closely with analysts, designers and other peer developers.
Preparing technical training documents for onboarding new engineers.', N'Requirements:

Bachelor�s degree in computer science or equivalent practical experience.
2+ years of experience as Software Engineer or Software Developer or in a relevant role.
Understanding of OOPS concepts, Persistence, Threading.
Proficient in Java, C++, Ruby on Rails or other programming languages.
Hands-on with SQL or NoSQL database.
Competent with developing web apps in popular web frameworks (ASP .Net, JQuery, Apache Wicket, JavaServer Faces (JSF) & Spring MVC etc,).
Knowledgeable in data structures and algorithms.
Experience with open-source projects.
Experience in designing interactive applications.
A background in Engineering with sound oral and written communication skills.
Prior experience with AWS, Azure, Google or Openstack will be a plus.', 189, 1, 2)
INSERT [Job].[JobTemplates] ([id], [jobIndustryId], [jobTitle], [jobDescription], [jobRequirements], [jobLocationId], [employmentTypeId], [languageId]) VALUES (7, 2, N'Front End Developer
', N'Responsibilities:

Work closely with design, product management and development teams to create elegant, usable, responsive and interactive interfaces across multiple devices.
Turning UI/UX designs into prototypes, creating awesome interactions from designs, writing reusable content modules and maintainability of the code.
Implement UI development principles to ensure that the product client-side serves at scale.
Review and optimize the app usage by monitoring key metrics and rectifying the issues proactively.
An ability to perform well in a fast-paced environment and bring in optimal flow for rapidly changing design/ technology.', N'Requirements:

3 to 5 years of relevant work experience as a web developer, UI developer, JavaScript expert or frontend engineer
Sound knowledge in HTML and CSS
Familiar with UI layouts, SASS, bootstrap and the CSS GRID system
Proficient with JavaScript frameworks such as jQuery, Angular et al
Experience debugging using popular JavaScript-based tools like Chrome Developer Console
Passionate to create good design and usability
A team player with good communication skills
Knowledge of Ember & Ruby will be a plus', 189, 1, 1)
INSERT [Job].[JobTemplates] ([id], [jobIndustryId], [jobTitle], [jobDescription], [jobRequirements], [jobLocationId], [employmentTypeId], [languageId]) VALUES (8, 2, N'Front End Developer
', N'Responsibilities:

Work closely with design, product management and development teams to create elegant, usable, responsive and interactive interfaces across multiple devices.
Turning UI/UX designs into prototypes, creating awesome interactions from designs, writing reusable content modules and maintainability of the code.
Implement UI development principles to ensure that the product client-side serves at scale.
Review and optimize the app usage by monitoring key metrics and rectifying the issues proactively.
An ability to perform well in a fast-paced environment and bring in optimal flow for rapidly changing design/ technology.', N'Requirements:

3 to 5 years of relevant work experience as a web developer, UI developer, JavaScript expert or frontend engineer
Sound knowledge in HTML and CSS
Familiar with UI layouts, SASS, bootstrap and the CSS GRID system
Proficient with JavaScript frameworks such as jQuery, Angular et al
Experience debugging using popular JavaScript-based tools like Chrome Developer Console
Passionate to create good design and usability
A team player with good communication skills
Knowledge of Ember & Ruby will be a plus', 189, 1, 2)
INSERT [Job].[JobTemplates] ([id], [jobIndustryId], [jobTitle], [jobDescription], [jobRequirements], [jobLocationId], [employmentTypeId], [languageId]) VALUES (9, 3, N'Medical Doctor', N'The duties of a medical doctor include consulting with patients and determining skin ailments, prescribing medication, undertaking skin therapy treatment, or performing non-intrusive surgery. 

Your tasks will be : 

Assist in Medical and dermatological treatments 
Perform customer service and sales duties  
Complete training will be offered to the candidate. ', N'Job Requirements
Experience is a MUST!
Pharmacy Graduate
People skills: An ability to interact well with others, and to be pleasant and friendly even under trying circumstances, can be invaluable.
Think outside the box: Creativity and a willingness to adapt to new trends can be important.
Be a good listener: People like to talk about themselves when they have time on their hands, such as when they''re sitting still while you tend to them. You''ll want to be able to provide appropriate feedback.
Physical stamina: You''ll spend a lot of time on your feet.
Tidiness: This doesn''t mean just your work station. Personal tidiness is very important as well. Remember, you''re an example of your own work.', 189, 1, 1)
INSERT [Job].[JobTemplates] ([id], [jobIndustryId], [jobTitle], [jobDescription], [jobRequirements], [jobLocationId], [employmentTypeId], [languageId]) VALUES (10, 3, N'Medical Doctor', N'The duties of a medical doctor include consulting with patients and determining skin ailments, prescribing medication, undertaking skin therapy treatment, or performing non-intrusive surgery. 

Your tasks will be : 

Assist in Medical and dermatological treatments 
Perform customer service and sales duties  
Complete training will be offered to the candidate. ', N'Job Requirements
Experience is a MUST!
Pharmacy Graduate
People skills: An ability to interact well with others, and to be pleasant and friendly even under trying circumstances, can be invaluable.
Think outside the box: Creativity and a willingness to adapt to new trends can be important.
Be a good listener: People like to talk about themselves when they have time on their hands, such as when they''re sitting still while you tend to them. You''ll want to be able to provide appropriate feedback.
Physical stamina: You''ll spend a lot of time on your feet.
Tidiness: This doesn''t mean just your work station. Personal tidiness is very important as well. Remember, you''re an example of your own work.', 189, 1, 2)
INSERT [Job].[JobTemplates] ([id], [jobIndustryId], [jobTitle], [jobDescription], [jobRequirements], [jobLocationId], [employmentTypeId], [languageId]) VALUES (11, 3, N'Primary Care Physician / Doctor', N'Primary Care Physician (PCP) is a doctor who devotes most of his / her practice to family or general medicine.

PCP serves as the gatekeeper who controls a patient''s access to care.
Sends patients to see a specialist or to another health-care facility for treatment or additional health-care services.
Authorizes all referrals to other providers.
PCP''s duties are not limited to medical checkups and referrals to specialists.
Provide a cost-effective service without affecting the quality.
Provide medical statistics for each account portrait the number of chronic diseases.', N'Job Requirements
A diploma or master''s in internal medicine is required.
Age from 38 -49 years
This is a full time job working hours 6 hours daily for 6 days', 189, 1, 1)
INSERT [Job].[JobTemplates] ([id], [jobIndustryId], [jobTitle], [jobDescription], [jobRequirements], [jobLocationId], [employmentTypeId], [languageId]) VALUES (12, 3, N'Primary Care Physician / Doctor', N'Primary Care Physician (PCP) is a doctor who devotes most of his / her practice to family or general medicine.

PCP serves as the gatekeeper who controls a patient''s access to care.
Sends patients to see a specialist or to another health-care facility for treatment or additional health-care services.
Authorizes all referrals to other providers.
PCP''s duties are not limited to medical checkups and referrals to specialists.
Provide a cost-effective service without affecting the quality.
Provide medical statistics for each account portrait the number of chronic diseases.', N'Job Requirements
A diploma or master''s in internal medicine is required.
Age from 38 -49 years
This is a full time job working hours 6 hours daily for 6 days', 189, 1, 2)
INSERT [Job].[JobTemplates] ([id], [jobIndustryId], [jobTitle], [jobDescription], [jobRequirements], [jobLocationId], [employmentTypeId], [languageId]) VALUES (13, 4, N'Admin Assistant- Social Media', N'Job Description
We are looking for an admin professional with Experience in Social Media to join our team.
As a Social Media Specialist, you will be responsible for developing and implementing the Social Media strategy for our clients in order to increase their online presence and improve their marketing and sales efforts.
You will be required to generate organic leads, and following up with prospective clients.
As an Admin Professional, you will be responsible for helping with responding to emails and scheduling appointments with any other Admin duties.
The Assistant will:
Help their manager with his daily tasks.
Schedule appointments and add them calendars.
Other appropriate duties as prescribed', N'Job Requirements
1-2 years of experience as a Social Media Specialist or similar role
Excellent written and spoken English.
Excellent reading, writing, and speaking skills, with Experience in English content writing.
Good computer skills, including word processing and spreadsheets
An eye for detail and precision
Punctuality, in this job, if you are not early you are late
Must have Experience with English Content writing.
Working hours will be from 03 pm till 12 pm, or from 06 pm till 03 am from Monday-Friday', 189, 1, 1)
INSERT [Job].[JobTemplates] ([id], [jobIndustryId], [jobTitle], [jobDescription], [jobRequirements], [jobLocationId], [employmentTypeId], [languageId]) VALUES (14, 4, N'Admin Assistant- Social Media', N'Job Description
We are looking for an admin professional with Experience in Social Media to join our team.
As a Social Media Specialist, you will be responsible for developing and implementing the Social Media strategy for our clients in order to increase their online presence and improve their marketing and sales efforts.
You will be required to generate organic leads, and following up with prospective clients.
As an Admin Professional, you will be responsible for helping with responding to emails and scheduling appointments with any other Admin duties.
The Assistant will:
Help their manager with his daily tasks.
Schedule appointments and add them calendars.
Other appropriate duties as prescribed', N'Job Requirements
1-2 years of experience as a Social Media Specialist or similar role
Excellent written and spoken English.
Excellent reading, writing, and speaking skills, with Experience in English content writing.
Good computer skills, including word processing and spreadsheets
An eye for detail and precision
Punctuality, in this job, if you are not early you are late
Must have Experience with English Content writing.
Working hours will be from 03 pm till 12 pm, or from 06 pm till 03 am from Monday-Friday', 189, 1, 2)
INSERT [Job].[JobTemplates] ([id], [jobIndustryId], [jobTitle], [jobDescription], [jobRequirements], [jobLocationId], [employmentTypeId], [languageId]) VALUES (15, 4, N'Admin Assistant', N'Answer and direct phone calls
Organize and schedule appointments
Plan meetings and take detailed minutes
Write and distribute email, correspondence memos, letters, faxes and forms
Assist in the preparation of regularly scheduled reports
Update and maintain office policies and procedures
Order office supplies 
Maintain contact lists
Submit and reconcile expense reports
Provide general support to visitors
Act as the point of contact for internal and external clients
Liaise with executive and senior administrative assistants to handle requests and queries from senior managers', N'Bachelor�s degree in a related field.
2+ years� experience as an administrative assistant, or office admin assistant
Knowledge of office management systems and procedures
Working knowledge of office equipment, like printers and fax machines
Proficiency in MS Office (MS Excel and MS PowerPoint, in particular)
Excellent time management skills and the ability to prioritize work
Attention to detail and problem solving skills
Excellent written and verbal communication skills
Strong organizational skills with the ability to multi-task', 189, 1, 1)
INSERT [Job].[JobTemplates] ([id], [jobIndustryId], [jobTitle], [jobDescription], [jobRequirements], [jobLocationId], [employmentTypeId], [languageId]) VALUES (16, 4, N'Admin Assistant', N'Answer and direct phone calls
Organize and schedule appointments
Plan meetings and take detailed minutes
Write and distribute email, correspondence memos, letters, faxes and forms
Assist in the preparation of regularly scheduled reports
Update and maintain office policies and procedures
Order office supplies 
Maintain contact lists
Submit and reconcile expense reports
Provide general support to visitors
Act as the point of contact for internal and external clients
Liaise with executive and senior administrative assistants to handle requests and queries from senior managers', N'Bachelor�s degree in a related field.
2+ years� experience as an administrative assistant, or office admin assistant
Knowledge of office management systems and procedures
Working knowledge of office equipment, like printers and fax machines
Proficiency in MS Office (MS Excel and MS PowerPoint, in particular)
Excellent time management skills and the ability to prioritize work
Attention to detail and problem solving skills
Excellent written and verbal communication skills
Strong organizational skills with the ability to multi-task', 189, 1, 2)
