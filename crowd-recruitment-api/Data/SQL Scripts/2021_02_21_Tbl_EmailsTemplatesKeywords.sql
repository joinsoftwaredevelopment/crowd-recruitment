USE [recruitment]
GO

/****** Object:  Table [Common].[EmailsTemplatesKeywords]    Script Date: 2021-02-21 12:13:10 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [Common].[EmailsTemplatesKeywords](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Keyword] [nvarchar](100) NOT NULL,
 CONSTRAINT [PK_TemplatesKeywords] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO


