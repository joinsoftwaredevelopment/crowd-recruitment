USE [recruitment]
GO

/****** Object:  View [dbo].[VwCandidatesInterviews]    Script Date: 16-Mar-21 3:08:37 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[VwCandidatesInterviews]
AS
SELECT        Employer.CandidatesInterviews.id, Sourcer.JobCandidateSubmission.candidateName, Sourcer.JobCandidateSubmission.email AS candidateEmail, Employer.CandidatesInterviews.interviewDate, 
                         Employer.CandidatesInterviews.duration, Employer.EmployerDetails.CompanyName, Common.Interviewers.InterviewerName, Employer.CandidatesInterviews.emailSubject, 
                         Employer.CandidatesInterviews.emailDescription
FROM            Employer.CandidatesInterviews INNER JOIN
                         Sourcer.JobCandidateSubmission ON Employer.CandidatesInterviews.candidateId = Sourcer.JobCandidateSubmission.Id INNER JOIN
                         Common.Users ON Common.Users.Id = Employer.CandidatesInterviews.createdBy INNER JOIN
                         Employer.EmployerDetails ON Common.Users.Id = Employer.EmployerDetails.UserId INNER JOIN
                         Common.Interviewers ON Employer.CandidatesInterviews.interviewerId = Common.Interviewers.Id
GO

EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[52] 4[9] 2[15] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "CandidatesInterviews (Employer)"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 347
               Right = 229
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "JobCandidateSubmission (Sourcer)"
            Begin Extent = 
               Top = 28
               Left = 390
               Bottom = 336
               Right = 636
            End
            DisplayFlags = 280
            TopColumn = 2
         End
         Begin Table = "Users (Common)"
            Begin Extent = 
               Top = 6
               Left = 674
               Bottom = 136
               Right = 851
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "EmployerDetails (Employer)"
            Begin Extent = 
               Top = 6
               Left = 889
               Bottom = 119
               Right = 1062
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "Interviewers (Common)"
            Begin Extent = 
               Top = 120
               Left = 889
               Bottom = 250
               Right = 1069
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 9
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane =' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VwCandidatesInterviews'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane2', @value=N' 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VwCandidatesInterviews'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=2 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VwCandidatesInterviews'
GO


