USE [recruitment]
GO

/****** Object:  View [dbo].[VwJobCandidates]    Script Date: 2021-02-16 4:29:26 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO





ALTER VIEW [dbo].[VwJobCandidates]
AS
SELECT        ROW_NUMBER() OVER ( ORDER BY Job.Job.Id ) AS 'rowNumber' ,
         Job.Job.Id AS jobId, Job.Job.UUID AS jobUuId, Job.Job.JobTitle AS jobTitle, Cast(CAST(Job.Job.JobDate AS date ) as nvarchar) AS jobDate, Sourcer.JobCandidateSubmission.SourcerId AS sourcerId, 
                         Common.Users.FirstName + ' ' + Common.Users.LastName AS sourcerName, Sourcer.JobCandidateSubmission.UUID AS submissionUuId, Sourcer.JobCandidateSubmission.candidateName, 
                         Sourcer.JobCandidateSubmission.CV_Name AS cvName, Sourcer.JobCandidateSubmission.linkedInProfile, Common.CountriesTranslation.name AS countryName, 
                         CONVERT(nvarchar, JobCandidateSubmission.SubmissionDate, 107) AS submissionDate, Common.CountriesTranslation.languageId ,
						 rejectionReason , CandidateSubmissionStatusId
FROM            Sourcer.JobCandidateSubmission INNER JOIN
                         Job.Job ON Sourcer.JobCandidateSubmission.JobId = Job.Job.Id INNER JOIN
                         Common.Users ON Common.Users.Id = Sourcer.JobCandidateSubmission.SourcerId LEFT OUTER JOIN
                         Common.Countries ON Common.Countries.id = Sourcer.JobCandidateSubmission.locationId LEFT OUTER JOIN
                         Common.CountriesTranslation ON Common.Countries.id = Common.CountriesTranslation.countryId
GO


