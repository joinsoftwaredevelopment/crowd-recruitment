USE [recruitment]
GO

/****** Object:  View [dbo].[VwJobCandidateSubmissionHistories]    Script Date: 2021-03-01 11:28:08 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



ALTER VIEW [dbo].[VwJobCandidateSubmissionHistories]
AS
SELECT        [CandidateSubmissionStatus].Id as candidateSubmissionStatusId , 
[JobCandidateSubmission].UUID as candidateUuId , [Translation] as statusName ,  [languageId] ,
[JobCandidateSubmissionHistory].createDate , [JobCandidateSubmission].rejectionReason 
FROM            [Sourcer].[JobCandidateSubmissionHistory] INNER JOIN
                [Employer].[CandidateSubmissionStatus]
			    ON [CandidateSubmissionStatus].Id = [JobCandidateSubmissionHistory].CandidateSubmissionStatusId
				INNER JOIN [Employer].[CandidateSubmissionStatusTranslation] ON  
				[CandidateSubmissionStatusTranslation].CandidateSubmissionStatusId=[CandidateSubmissionStatus].Id 
				INNER JOIN [Sourcer].[JobCandidateSubmission] ON [JobCandidateSubmission].Id = [JobCandidateSubmissionHistory].JobCandidatesubmissionId
GO


