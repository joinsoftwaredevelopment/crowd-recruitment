USE [recruitment]
GO

/****** Object:  View [dbo].[VwJobSubmissionsSummary]    Script Date: 2021-02-16 4:47:47 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO





/*GROUP BY Job.Job.Id, Job.Job.UUID, Job.Job.JobStatusId, Job.Job.IsActive, Job.JobLocationTranslation.languageId, Sourcer.JobCandidateSubmission.SourcerId, EmployerId, Common.Users.UUID*/
ALTER VIEW [dbo].[VwJobSubmissionsSummary]
AS
SELECT DISTINCT Job.Job.Id AS jobId, Job.Job.UUID, Job.Job.JobStatusId as jobStatusId, Job.Job.JobTitle, Job.Job.IsActive,
                             (SELECT       (ISNULL(Job.noOfSubmissions,0) - COUNT(*)) AS submissionCount
                               FROM            Sourcer.JobCandidateSubmission
                               WHERE        (JobId = Job.Job.Id)) AS submissionCount, Job.JobLocationTranslation.languageId, Job.JobLocationTranslation.Translation AS jobLocationTranslation, Sourcer.JobCandidateSubmission.SourcerId, 
                         Job.Job.EmployerId, Common.Users.UUID AS employerUUId,  CONVERT(nvarchar, Common.Users.CreatedDate, 107) AS jobDate, Job.Job.JobDescription AS description, 
                         Employer.CompanyIndustryTranslation.Translation AS companyIndustry , Common.Users.FirstName as employerFName , Common.Users.LastName as employerLName,
						 Job.reason , job.commission
FROM            Job.Job INNER JOIN
                         Job.JobStatus ON Job.JobStatus.Id = Job.Job.JobStatusId LEFT OUTER JOIN
                         Sourcer.JobCandidateSubmission ON Sourcer.JobCandidateSubmission.JobId = Job.Job.Id INNER JOIN
                         Job.JobLocation ON Job.JobLocation.Id = Job.Job.JobLocationId INNER JOIN
                         Job.JobLocationTranslation ON Job.JobLocation.Id = Job.JobLocationTranslation.JobLocationId INNER JOIN
                         Common.Users ON Common.Users.Id = Job.Job.EmployerId INNER JOIN
                         Employer.CompanyIndustry ON Job.Job.industryId = Employer.CompanyIndustry.Id INNER JOIN
                         Employer.CompanyIndustryTranslation ON Employer.CompanyIndustryTranslation.CompanyIndustryId = Employer.CompanyIndustry.Id AND 
                         Employer.CompanyIndustryTranslation.languageId = Job.JobLocationTranslation.languageId
GO


