/****** Object:  View [dbo].[VwEmployerPackageHistory]    Script Date: 29-07-2021 10:40:45 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[VwEmployerPackageHistory]
AS
SELECT 
	EPH.Id,EPH.PackagePurchaseDate,EPH.PackageExpiryDate,EPH.PackageId,
	P.PackageTitle,P.packagePrice,P.packageDescription,EPH.UserId FROM [Employer].[EmployerPackageHistory] EPH
	LEFT JOIN [Common].[Packages] P on P.PackageId= EPH.PackageId

GO


