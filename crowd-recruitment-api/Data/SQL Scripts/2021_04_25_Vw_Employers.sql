USE [recruitment]
GO

/****** Object:  View [dbo].[VwEmployers]    Script Date: 25-Apr-21 2:47:53 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

ALTER VIEW [dbo].[VwEmployers]
AS
SELECT        TOP (100) PERCENT Common.Users.Id AS employerId, Common.Users.UUID, Common.Users.FirstName, Common.Users.LastName, Common.Users.FirstName + ' ' + Common.Users.LastName AS fullName, 
                         Common.Users.PhoneNumber, Common.Users.Email, Common.Users.IsEmailVerified, Common.Users.Password, Common.Users.RoleId, Common.Users.LanguageId, CONVERT(nvarchar, 
                         Common.Users.CreatedDate, 107) AS createdDate, Common.Users.UpdatedDate, Common.Users.Active, Common.Users.Accepted, Common.Users.MeetingDate, Common.Users.MeetingPeriodId, 
                         Common.Users.RegistrationStep, Common.Users.InterviewerId, Common.Users.meetingTimeId, Common.Users.timeZoneId, Common.Users.userStatusId, Employer.EmployerDetails.CompanyName, 
                         Common.Users.rejectedReason,
                             (SELECT        COUNT(*) AS Expr1
                               FROM            Job.Job
                               WHERE        (JobStatusId = 3) AND (EmployerId = Common.Users.Id)) AS openedJobsCount, Employer.EmployerDetails.packageId
FROM            Common.Users INNER JOIN
                         Employer.EmployerDetails ON Common.Users.Id = Employer.EmployerDetails.UserId
ORDER BY employerId DESC
GO


