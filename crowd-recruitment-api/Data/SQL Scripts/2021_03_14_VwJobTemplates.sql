USE [recruitment]
GO

/****** Object:  View [dbo].[VwJobTemplates]    Script Date: 2021-03-14 11:02:23 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




ALTER VIEW [dbo].[VwJobTemplates]
AS
SELECT           Job.JobTemplates.id as jobTemplateId , jobTitle , Job.JobTemplates.jobDescription as description , Job.JobTemplates.jobRequirements  as requirements,Job.JobTemplates.jobLocationId , Common.CountriesTranslation.name as jobLocation,
                Job.JobTemplates.employmentTypeId , EmploymentTypeTranslation.Translation as employmentTypeName , 
				Job.JobTemplates.languageId , jobIndustryId , '' as niceToHaveQualification , '' as mustHaveQualification
FROM            Job.JobTemplates INNER JOIN
                         Employer.EmploymentType ON Job.JobTemplates.employmentTypeId = Employer.EmploymentType.Id INNER JOIN
                         Employer.EmploymentTypeTranslation ON Employer.EmploymentType.Id = Employer.EmploymentTypeTranslation.EmploymentTypeId 
						 AND Employer.EmploymentTypeTranslation.languageId = Job.JobTemplates.languageId INNER JOIN
                         Common.Countries ON Common.Countries.Id = Job.JobTemplates.jobLocationId INNER JOIN
                         Common.CountriesTranslation ON Common.CountriesTranslation.countryId = Common.Countries.Id
						  AND Common.CountriesTranslation.languageId =Job.JobTemplates.languageId INNER JOIN
                         Employer.CompanyIndustry ON Job.JobTemplates.jobIndustryId = Employer.CompanyIndustry.Id INNER JOIN
                         Employer.CompanyIndustryTranslation ON Employer.CompanyIndustryTranslation.CompanyIndustryId = Employer.CompanyIndustry.Id AND 
                         Employer.CompanyIndustryTranslation.languageId = Job.JobTemplates.languageId
GO


