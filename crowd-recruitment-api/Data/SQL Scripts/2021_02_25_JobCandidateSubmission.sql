USE [recruitment]
GO

/****** Object:  Table [Sourcer].[JobCandidateSubmission]    Script Date: 25-Feb-21 1:37:19 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [Sourcer].[JobCandidateSubmission](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[UUID] [varchar](50) NOT NULL,
	[JobId] [int] NOT NULL,
	[candidateName] [nvarchar](150) NULL,
	[CV_Name] [nvarchar](50) NOT NULL,
	[SubmissionDate] [datetime] NOT NULL,
	[SourcerId] [int] NOT NULL,
	[CandidateSubmissionStatusId] [int] NOT NULL,
	[linkedInProfile] [nvarchar](300) NULL,
	[locationId] [int] NULL,
	[IsDeleted] [bit] NOT NULL,
	[commission] [decimal](10, 3) NULL,
	[rejectionReason] [nvarchar](500) NULL,
	[EmpSubmissionStatusId] [int] NULL,
	[EmpReason] [nvarchar](500) NULL,
	[email] [nvarchar](150) NULL,
 CONSTRAINT [PK_JobCandidateSubmission] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
 CONSTRAINT [UQ__JobCandi__65A475E6BD9808BB] UNIQUE NONCLUSTERED 
(
	[UUID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO


