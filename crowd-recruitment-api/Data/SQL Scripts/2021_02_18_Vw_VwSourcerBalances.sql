USE [recruitment]
GO

/****** Object:  View [dbo].[VwSourcerBalances]    Script Date: 2021-02-18 3:49:28 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



ALTER VIEW [dbo].[VwSourcerBalances]
AS
SELECT        employers.FirstName + ' ' + employers.LastName AS employerName, 
Sourcer.JobCandidateSubmission.candidateName,
 CONVERT(nvarchar, Sourcer.JobCandidateSubmission.SubmissionDate, 107) 
                         AS submissionDate, Job.Job.JobTitle, 
						 ISNULL(Sourcer.JobCandidateSubmission.commission,0) AS candidateCommission,
						 sourcers.UUID as sourcerUuid ,
						 Sourcer.JobCandidateSubmission.UUID as candidateUuId
FROM            Job.Job INNER JOIN
                         Common.Users as employers ON employers.Id = Job.Job.EmployerId INNER JOIN
                         Sourcer.JobCandidateSubmission ON Job.Job.Id = Sourcer.JobCandidateSubmission.JobId
						 INNER JOIN Common.Users as sourcers ON sourcers.Id = JobCandidateSubmission.SourcerId
GO


