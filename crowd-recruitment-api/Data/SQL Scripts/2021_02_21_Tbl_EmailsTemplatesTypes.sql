﻿
USE [recruitment]
GO
/****** Object:  Table [Common].[EmailsTemplatesTypes]    Script Date: 2021-02-21 3:07:13 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [Common].[EmailsTemplatesTypes](
	[id] [int] NOT NULL,
	[nameAr] [nvarchar](100) NOT NULL,
	[nameEn] [nvarchar](100) NOT NULL,
 CONSTRAINT [PK_TemplatesTypes] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
INSERT [Common].[EmailsTemplatesTypes] ([id], [nameAr], [nameEn]) VALUES (1, N'البريد الإلكترونى للنظام', N'System Emails')
INSERT [Common].[EmailsTemplatesTypes] ([id], [nameAr], [nameEn]) VALUES (2, N'نماذج البريد الالكترونى للوظائف', N'Jobs Candidates Templates')
