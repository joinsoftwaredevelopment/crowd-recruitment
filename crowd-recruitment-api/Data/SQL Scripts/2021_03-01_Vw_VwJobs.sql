USE [recruitment]
GO

/****** Object:  View [dbo].[VwJobs]    Script Date: 2021-03-01 2:10:04 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO










ALTER VIEW [dbo].[VwJobs]
AS
SELECT        Job.Job.Id AS jobId,Job.Job.UUID AS uuid, Job.Job.JobTitle AS jobTitle, Employer.EmploymentTypeTranslation.Translation AS employmentTypeName, 
              Employer.SeniorityLevelTranslation.Translation AS seniorityLevelName, 
              Job.Job.companiesNotToSourceFrom, Job.JobLocationTranslation.Translation AS jobLocation, 
			  Employer.EmployerDetails.CompanyName AS companyName, Job.Job.mustHaveQualification, 
			  Job.Job.niceToHaveQualification, Employer.EmployerDetails.UserId as employerId ,
              Job.Job.HiringNeeds, Employer.ExperienceLevelTranslation.Translation AS ExperienceLevelName , 
			  Common.Languages.id as languageId ,Job.JobStatusId as jobStatusId,
			  Job.Job.JobDescription as description , (CompanyIndustryTranslation.Translation) as companyIndustry , 
			  Job.JobStatusTranslation.Translation as jobStatusName, Job.Job.requirements , noOfSubmissions ,
			  (FirstName + ' ' + LastName) as employerName , Job.Job .reason as rejectionReason
FROM          Job.Job INNER JOIN
                         Employer.EmploymentType ON Job.Job.EmployerTypeId = Employer.EmploymentType.Id INNER JOIN
                         Employer.EmploymentTypeTranslation ON Employer.EmploymentType.Id = Employer.EmploymentTypeTranslation.EmploymentTypeId INNER JOIN
                         Common.Languages ON Common.Languages.id = Employer.EmploymentTypeTranslation.languageId INNER JOIN
                         Employer.SeniorityLevel ON Employer.SeniorityLevel.Id = Job.Job.SeniorityLevelId INNER JOIN
                         Employer.SeniorityLevelTranslation ON Employer.SeniorityLevelTranslation.SeniorityLevelId = Employer.SeniorityLevel.Id AND Employer.SeniorityLevelTranslation.languageId = Common.Languages.id INNER JOIN
                         Job.JobLocation ON Job.JobLocation.Id = Job.Job.JobLocationId INNER JOIN
                         Job.JobLocationTranslation ON Job.JobLocationTranslation.JobLocationId = Job.JobLocation.Id AND Job.JobLocationTranslation.languageId = Common.Languages.id INNER JOIN
                         Job.JobStatus ON Job.JobStatus.Id = Job.Job.JobStatusId INNER JOIN
                         Job.JobStatusTranslation ON Job.JobStatusTranslation.JobStatusId = Job.JobStatus.Id AND Job.JobStatusTranslation.languageId = Common.Languages.id INNER JOIN
						 Common.Users ON Common.Users.Id = Job.Job.EmployerId INNER JOIN
                         Employer.EmployerDetails ON Employer.EmployerDetails.UserId = Common.Users.Id INNER JOIN
                         Employer.ExperienceLevel ON Employer.ExperienceLevel.Id = Job.Job.ExperienceLevelId INNER JOIN
                         Employer.ExperienceLevelTranslation ON Employer.ExperienceLevelTranslation.ExperienceLevelId = Employer.ExperienceLevel.Id 
						 AND Employer.ExperienceLevelTranslation.languageId = Common.Languages.id
						 INNER JOIN Employer.CompanyIndustry ON Job.industryId = CompanyIndustry.Id
						 INNER JOIN Employer.CompanyIndustryTranslation ON CompanyIndustryTranslation.CompanyIndustryId = CompanyIndustry.Id
						 AND CompanyIndustryTranslation.languageId = JobLocationTranslation.languageId 
GO


