USE [recruitment]
GO
INSERT [Common].[EmailsTemplates] ([id], [emailTitleEN], [emailTitleAr], [emailbodyEn], [emailbodyAr], [emailTemplateType]) VALUES (1, N'CR- Verification', N'تفعيل حساب ال CR', N'<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="x-apple-disable-message-reformatting">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

    <title>CR - Verfication</title>
    <style type="text/css">
    
        .primaryColor {
            color: #0195ff !important;
        }

        .primaryBorderBColor {
            border-bottom-color: #0195ff !important;
        }

        .primaryBackgroundColor {
            background-color: #0195ff !important;
        }

        .secondryBackgroundColor {
            background-color: #fef5e2 !important;
        }

        a {
            text-decoration: none;
            outline: none;
        }

        @media (max-width: 649px) {
            .o_col-full {
                max-width: 100% !important;
            }

            .o_col-half {
                max-width: 50% !important;
            }

            .o_hide-lg {
                display: inline-block !important;
                font-size: inherit !important;
                max-height: none !important;
                line-height: inherit !important;
                overflow: visible !important;
                width: auto !important;
                visibility: visible !important;
            }

            .o_hide-xs, .o_hide-xs.o_col_i {
                display: none !important;
                font-size: 0 !important;
                max-height: 0 !important;
                width: 0 !important;
                line-height: 0 !important;
                overflow: hidden !important;
                visibility: hidden !important;
                height: 0 !important;
            }

            .o_xs-center {
                text-align: center !important;
            }

            .o_xs-left {
                text-align: left !important;
            }

            .o_xs-right {
                text-align: left !important;
            }

            table.o_xs-left {
                margin-left: 0 !important;
                margin-right: auto !important;
                float: none !important;
            }

            table.o_xs-right {
                margin-left: auto !important;
                margin-right: 0 !important;
                float: none !important;
            }

            table.o_xs-center {
                margin-left: auto !important;
                margin-right: auto !important;
                float: none !important;
            }

            h1.o_heading {
                font-size: 32px !important;
                line-height: 41px !important;
            }

            h2.o_heading {
                font-size: 26px !important;
                line-height: 37px !important;
            }

            h3.o_heading {
                font-size: 20px !important;
                line-height: 30px !important;
            }

            .o_xs-py-md {
                padding-top: 24px !important;
                padding-bottom: 24px !important;
            }

            .o_xs-pt-xs {
                padding-top: 8px !important;
            }

            .o_xs-pb-xs {
                padding-bottom: 8px !important;
            }
        }

        @media screen {
            @font-face {
                font-family: ''Roboto'';
                font-style: normal;
                font-weight: 400;
                src: local("Roboto"), local("Roboto-Regular"), url(https://fonts.gstatic.com/s/roboto/v18/KFOmCnqEu92Fr1Mu7GxKOzY.woff2) format("woff2");
                unicode-range: U+0100-024F, U+0259, U+1E00-1EFF, U+2020, U+20A0-20AB, U+20AD-20CF, U+2113, U+2C60-2C7F, U+A720-A7FF;
            }

            @font-face {
                font-family: ''Roboto'';
                font-style: normal;
                font-weight: 400;
                src: local("Roboto"), local("Roboto-Regular"), url(https://fonts.gstatic.com/s/roboto/v18/KFOmCnqEu92Fr1Mu4mxK.woff2) format("woff2");
                unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF, U+FFFD;
            }

            @font-face {
                font-family: ''Roboto'';
                font-style: normal;
                font-weight: 700;
                src: local("Roboto Bold"), local("Roboto-Bold"), url(https://fonts.gstatic.com/s/roboto/v18/KFOlCnqEu92Fr1MmWUlfChc4EsA.woff2) format("woff2");
                unicode-range: U+0100-024F, U+0259, U+1E00-1EFF, U+2020, U+20A0-20AB, U+20AD-20CF, U+2113, U+2C60-2C7F, U+A720-A7FF;
            }

            @font-face {
                font-family: ''Roboto'';
                font-style: normal;
                font-weight: 700;
                src: local("Roboto Bold"), local("Roboto-Bold"), url(https://fonts.gstatic.com/s/roboto/v18/KFOlCnqEu92Fr1MmWUlfBBc4.woff2) format("woff2");
                unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF, U+FFFD;
            }

            .o_sans, .o_heading {
                font-family: "Roboto", sans-serif !important;
            }

            .o_heading, strong, b {
                font-weight: 700 !important;
            }

            a[x-apple-data-detectors] {
                color: inherit !important;
                text-decoration: none !important;
            }
        }
    </style>
    <!--[if mso]>
    <style>
      table { border-collapse: collapse; }
      .o_col { float: left; }
    </style>
    <xml>
      <o:OfficeDocumentSettings>
        <o:PixelsPerInch>96</o:PixelsPerInch>
      </o:OfficeDocumentSettings>
    </xml>
    <![endif]-->
</head>
<body class="o_body o_bg-white" style="width: 100%;margin: 0px;padding: 0px;-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;background-color: #ffffff;">
        <style type="text/css">
    
        .primaryColor {
            color: #0195ff !important;
        }

        .primaryBorderBColor {
            border-bottom-color: #0195ff !important;
        }

        .primaryBackgroundColor {
            background-color: #0195ff !important;
        }

        .secondryBackgroundColor {
            background-color: #fef5e2 !important;
        }

        a {
            text-decoration: none;
            outline: none;
        }

        @media (max-width: 649px) {
            .o_col-full {
                max-width: 100% !important;
            }

            .o_col-half {
                max-width: 50% !important;
            }

            .o_hide-lg {
                display: inline-block !important;
                font-size: inherit !important;
                max-height: none !important;
                line-height: inherit !important;
                overflow: visible !important;
                width: auto !important;
                visibility: visible !important;
            }

            .o_hide-xs, .o_hide-xs.o_col_i {
                display: none !important;
                font-size: 0 !important;
                max-height: 0 !important;
                width: 0 !important;
                line-height: 0 !important;
                overflow: hidden !important;
                visibility: hidden !important;
                height: 0 !important;
            }

            .o_xs-center {
                text-align: center !important;
            }

            .o_xs-left {
                text-align: left !important;
            }

            .o_xs-right {
                text-align: left !important;
            }

            table.o_xs-left {
                margin-left: 0 !important;
                margin-right: auto !important;
                float: none !important;
            }

            table.o_xs-right {
                margin-left: auto !important;
                margin-right: 0 !important;
                float: none !important;
            }

            table.o_xs-center {
                margin-left: auto !important;
                margin-right: auto !important;
                float: none !important;
            }

            h1.o_heading {
                font-size: 32px !important;
                line-height: 41px !important;
            }

            h2.o_heading {
                font-size: 26px !important;
                line-height: 37px !important;
            }

            h3.o_heading {
                font-size: 20px !important;
                line-height: 30px !important;
            }

            .o_xs-py-md {
                padding-top: 24px !important;
                padding-bottom: 24px !important;
            }

            .o_xs-pt-xs {
                padding-top: 8px !important;
            }

            .o_xs-pb-xs {
                padding-bottom: 8px !important;
            }
        }

        @media screen {
            @font-face {


            .o_sans, .o_heading {
               font-family: "Cairo" !important;
            }

            .o_heading, strong, b {
                font-weight: 700 !important;
            }

            a[x-apple-data-detectors] {
                color: inherit !important;
                text-decoration: none !important;
            }
        }
    </style>
    <!-- preview-text -->
    <!-- <table width="100%" cellspacing="0" cellpadding="0" border="0" role="presentation">
        <tbody>
            <tr>
                <td class="o_hide" align="center" style="display: none;font-size: 0;max-height: 0;width: 0;line-height: 0;overflow: hidden;mso-hide: all;visibility: hidden;">Email Summary (Hidden)</td>
            </tr>
        </tbody>
    </table> -->
    <!-- header -->

    <!-- hero-icon-lines -->

    <!-- label-xs -->
    <table width="100%" cellspacing="0" cellpadding="0" border="0" role="presentation">
        <tbody>
            <tr>
                <td class="o_bg-white o_px-md o_py o_sans o_text-xs o_text-light" align="center" style="        font-family: Cairo;
        margin-top: 0px;
        margin-bottom: 0px;
        font-size: 14px;
        line-height: 21px;
        background-color: #ffffff;
        color: #82899a;
        padding-left: 24px;
        padding-right: 24px;
        padding-top: 16px;
        padding-bottom: 16px;">
                    <table role="presentation" cellspacing="0" cellpadding="0" border="0">
                        <tbody>
                            <tr>
                                
                            </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
        </tbody>
    </table>



    <!-- button-primary -->
 
    <!-- spacer -->
 
    <!-- footer-white -->
<table width="100%" cellspacing="0" cellpadding="0" border="0" role="presentation" style="
    width: 800px;
    margin: 0 auto;
    box-shadow: 0 0 20px #ccc;
    border: 1px solid #ccc;
">
        <tbody>

            <tr>
                <td class="o_bg-white o_px-md o_py" align="center" style="background-color: #ffffff;padding-left: 24px;padding-right: 24px;padding-top: 16px;padding-bottom: 16px;">
                    <!--[if mso]><table width="584" cellspacing="0" cellpadding="0" border="0" role="presentation"><tbody><tr><td align="center"><![endif]-->
                    <div class="o_col-6s o_sans o_text o_text-secondary o_center" style="font-family: Cairo;margin-top: 0px;margin-bottom: 0px;font-size: 16px;line-height: 24px;max-width: 584px;color: #424651;text-align: center;">
                       
                        <p style="margin-top: 17px;margin-bottom: 0px;">
                      <img src="https://crowd-recruitment.joinsolutions.app/img/thankful.png" height="">
                        </p>
                         <p style="margin-top: 20px;margin-bottom: 0px;font-weight: 700;font-size: 22px;">Welcome [USERNAME].</p>
                        <span style="
    height: 3px;
    width: 100px;
    display: inline-block;
    background: #0195ff;
"></span>
                    </div>
                    <!--[if mso]></td></tr></table><![endif]-->
                </td>
            </tr>


                            <tr>
                <td class="o_bg-white o_px-md o_py" align="center" style="background-color: #ffffff;padding-left: 24px;padding-right: 24px;padding-top: 16px;padding-bottom: 16px;">
                    <!--[if mso]><table width="584" cellspacing="0" cellpadding="0" border="0" role="presentation"><tbody><tr><td align="center"><![endif]-->
                    <div class="o_col-6s o_sans o_text o_text-secondary o_center" style="font-family: Cairo;margin-top: 0px;margin-bottom: 0px;font-size: 16px;line-height: 24px;max-width: 584px;color: #424651;text-align: center;">
                        <p style="margin-top: 0px;margin-bottom: 0px;">
             Thank you for applying with us, Now You can verify your email by clicking the button below

                        </p>

                  <a class="o_text-white" href="[ACTIVATIONLINK]" style="text-decoration: none;outline: none;color: #ffffff;display: block;padding: 12px 24px;mso-text-raise: 3px;background: #0195ff;margin: 15px 115px;">Book a Meeting</a>

                <span style="
    height: 1px;
    width: 600px;
    display: inline-block;
    background: #ccc;
    margin-top: 0px;
"></span>
                    </div>
                    <!--[if mso]></td></tr></table><![endif]-->
                </td>
            </tr>



            <tr>
                <td class="o_bg-white o_px-md o_py-lg o_bt-light" align="center" style="background-color: #ffffff;padding-left: 24px;padding-right: 24px;padding-top: 0px;padding-bottom: 0px;">
                    <!--[if mso]><table width="584" cellspacing="0" cellpadding="0" border="0" role="presentation"><tbody><tr><td align="center"><![endif]-->
                    <div class="o_col-6s o_sans o_text-xs o_text-light" style="font-family: Cairo;margin-top: 0px;margin-bottom: 0px;font-size: 14px;line-height: 21px;max-width: 584px;color: #82899a;">
                      <p style="
    color: #40a3e7;
    font-size: 45px;
    margin: 15px
">CR</p>
                        <p class="o_mb" style="margin-top: 0px;margin-bottom: 16px;">©2020 Join Solutions</p>
                        <p class="o_mb" style="margin-top: 0px;margin-bottom: 16px;">
                            3556 King Abdullah Rd,<br>
                            Al Riyadh 13216, KSA
                        </p>
                        <p style="margin-top: 0px;margin-bottom: 0px;">
                            <a class="o_text-light o_underline" href="https://join.sa/" style="text-decoration: underline;outline: none;color: #82899a;">Help Center</a> <span class="o_hide-xs">&nbsp; • &nbsp;</span><br class="o_hide-lg" style="display: none;font-size: 0;max-height: 0;width: 0;line-height: 0;overflow: hidden;mso-hide: all;visibility: hidden;">
                            <a class="o_text-light o_underline" href="https://join.sa/" style="text-decoration: underline;outline: none;color: #82899a;">Preferences</a>
                            </a> <span class="o_hide-xs">&nbsp; • &nbsp;</span><br class="o_hide-lg" style="display: none;font-size: 0;max-height: 0;width: 0;line-height: 0;overflow: hidden;mso-hide: all;visibility: hidden;">

                             <a class="o_text-light o_underline" href="https://join.sa/" style="text-decoration: underline;outline: none;color: #82899a;">Unsubscribe</a>
                        </p>


<p style="
    padding-top: 20px;
">
                            <a href="">
                                <i style="
    color: #82899a;
    font-size: 36px;
    padding: 0 10px;
    cursor: pointer;
" class="fa fa-instagram"></i>
                            </a>
                              <a href="">
                                <i style="
    color: #82899a;
    font-size: 36px;
    padding: 0 10px;
    cursor: pointer;
" class="fa fa-facebook"></i>
                            </a>
                              <a href="">
                                <i style="
    color: #82899a;
    font-size: 36px;
    padding: 0 10px;
    cursor: pointer;
" class="fa fa-twitter"></i>
                            </a>
                        </p>
 
                    </div>
                    <!--[if mso]></td></tr></table><![endif]-->
                   
                </td>
            </tr>
        </tbody>
    </table>
</body>
</html>
', N'Arabic', 1)
INSERT [Common].[EmailsTemplates] ([id], [emailTitleEN], [emailTitleAr], [emailbodyEn], [emailbodyAr], [emailTemplateType]) VALUES (2, N'CR-Sourcer  Acceptance', N'تم قبولك فى ال CR', N'<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="x-apple-disable-message-reformatting">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

    <title>CR - Request Acceptance</title>
    <style type="text/css">
    
        .primaryColor {
            color: #0195ff !important;
        }

        .primaryBorderBColor {
            border-bottom-color: #0195ff !important;
        }

        .primaryBackgroundColor {
            background-color: #0195ff !important;
        }

        .secondryBackgroundColor {
            background-color: #fef5e2 !important;
        }

        a {
            text-decoration: none;
            outline: none;
        }

        @media (max-width: 649px) {
            .o_col-full {
                max-width: 100% !important;
            }

            .o_col-half {
                max-width: 50% !important;
            }

            .o_hide-lg {
                display: inline-block !important;
                font-size: inherit !important;
                max-height: none !important;
                line-height: inherit !important;
                overflow: visible !important;
                width: auto !important;
                visibility: visible !important;
            }

            .o_hide-xs, .o_hide-xs.o_col_i {
                display: none !important;
                font-size: 0 !important;
                max-height: 0 !important;
                width: 0 !important;
                line-height: 0 !important;
                overflow: hidden !important;
                visibility: hidden !important;
                height: 0 !important;
            }

            .o_xs-center {
                text-align: center !important;
            }

            .o_xs-left {
                text-align: left !important;
            }

            .o_xs-right {
                text-align: left !important;
            }

            table.o_xs-left {
                margin-left: 0 !important;
                margin-right: auto !important;
                float: none !important;
            }

            table.o_xs-right {
                margin-left: auto !important;
                margin-right: 0 !important;
                float: none !important;
            }

            table.o_xs-center {
                margin-left: auto !important;
                margin-right: auto !important;
                float: none !important;
            }

            h1.o_heading {
                font-size: 32px !important;
                line-height: 41px !important;
            }

            h2.o_heading {
                font-size: 26px !important;
                line-height: 37px !important;
            }

            h3.o_heading {
                font-size: 20px !important;
                line-height: 30px !important;
            }

            .o_xs-py-md {
                padding-top: 24px !important;
                padding-bottom: 24px !important;
            }

            .o_xs-pt-xs {
                padding-top: 8px !important;
            }

            .o_xs-pb-xs {
                padding-bottom: 8px !important;
            }
        }

        @media screen {
            @font-face {
                font-family: ''Roboto'';
                font-style: normal;
                font-weight: 400;
                src: local("Roboto"), local("Roboto-Regular"), url(https://fonts.gstatic.com/s/roboto/v18/KFOmCnqEu92Fr1Mu7GxKOzY.woff2) format("woff2");
                unicode-range: U+0100-024F, U+0259, U+1E00-1EFF, U+2020, U+20A0-20AB, U+20AD-20CF, U+2113, U+2C60-2C7F, U+A720-A7FF;
            }

            @font-face {
                font-family: ''Roboto'';
                font-style: normal;
                font-weight: 400;
                src: local("Roboto"), local("Roboto-Regular"), url(https://fonts.gstatic.com/s/roboto/v18/KFOmCnqEu92Fr1Mu4mxK.woff2) format("woff2");
                unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF, U+FFFD;
            }

            @font-face {
                font-family: ''Roboto'';
                font-style: normal;
                font-weight: 700;
                src: local("Roboto Bold"), local("Roboto-Bold"), url(https://fonts.gstatic.com/s/roboto/v18/KFOlCnqEu92Fr1MmWUlfChc4EsA.woff2) format("woff2");
                unicode-range: U+0100-024F, U+0259, U+1E00-1EFF, U+2020, U+20A0-20AB, U+20AD-20CF, U+2113, U+2C60-2C7F, U+A720-A7FF;
            }

            @font-face {
                font-family: ''Roboto'';
                font-style: normal;
                font-weight: 700;
                src: local("Roboto Bold"), local("Roboto-Bold"), url(https://fonts.gstatic.com/s/roboto/v18/KFOlCnqEu92Fr1MmWUlfBBc4.woff2) format("woff2");
                unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF, U+FFFD;
            }

            .o_sans, .o_heading {
                font-family: "Roboto", sans-serif !important;
            }

            .o_heading, strong, b {
                font-weight: 700 !important;
            }

            a[x-apple-data-detectors] {
                color: inherit !important;
                text-decoration: none !important;
            }
        }
    </style>
    <!--[if mso]>
    <style>
      table { border-collapse: collapse; }
      .o_col { float: left; }
    </style>
    <xml>
      <o:OfficeDocumentSettings>
        <o:PixelsPerInch>96</o:PixelsPerInch>
      </o:OfficeDocumentSettings>
    </xml>
    <![endif]-->
</head>
<body class="o_body o_bg-white" style="width: 100%;margin: 0px;padding: 0px;-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;background-color: #ffffff;">
        <style type="text/css">
    
        .primaryColor {
            color: #0195ff !important;
        }

        .primaryBorderBColor {
            border-bottom-color: #0195ff !important;
        }

        .primaryBackgroundColor {
            background-color: #0195ff !important;
        }

        .secondryBackgroundColor {
            background-color: #fef5e2 !important;
        }

        a {
            text-decoration: none;
            outline: none;
        }

        @media (max-width: 649px) {
            .o_col-full {
                max-width: 100% !important;
            }

            .o_col-half {
                max-width: 50% !important;
            }

            .o_hide-lg {
                display: inline-block !important;
                font-size: inherit !important;
                max-height: none !important;
                line-height: inherit !important;
                overflow: visible !important;
                width: auto !important;
                visibility: visible !important;
            }

            .o_hide-xs, .o_hide-xs.o_col_i {
                display: none !important;
                font-size: 0 !important;
                max-height: 0 !important;
                width: 0 !important;
                line-height: 0 !important;
                overflow: hidden !important;
                visibility: hidden !important;
                height: 0 !important;
            }

            .o_xs-center {
                text-align: center !important;
            }

            .o_xs-left {
                text-align: left !important;
            }

            .o_xs-right {
                text-align: left !important;
            }

            table.o_xs-left {
                margin-left: 0 !important;
                margin-right: auto !important;
                float: none !important;
            }

            table.o_xs-right {
                margin-left: auto !important;
                margin-right: 0 !important;
                float: none !important;
            }

            table.o_xs-center {
                margin-left: auto !important;
                margin-right: auto !important;
                float: none !important;
            }

            h1.o_heading {
                font-size: 32px !important;
                line-height: 41px !important;
            }

            h2.o_heading {
                font-size: 26px !important;
                line-height: 37px !important;
            }

            h3.o_heading {
                font-size: 20px !important;
                line-height: 30px !important;
            }

            .o_xs-py-md {
                padding-top: 24px !important;
                padding-bottom: 24px !important;
            }

            .o_xs-pt-xs {
                padding-top: 8px !important;
            }

            .o_xs-pb-xs {
                padding-bottom: 8px !important;
            }
        }

        @media screen {
            @font-face {


            .o_sans, .o_heading {
               font-family: "Cairo" !important;
            }

            .o_heading, strong, b {
                font-weight: 700 !important;
            }

            a[x-apple-data-detectors] {
                color: inherit !important;
                text-decoration: none !important;
            }
        }
    </style>
    <!-- preview-text -->
    <!-- <table width="100%" cellspacing="0" cellpadding="0" border="0" role="presentation">
        <tbody>
            <tr>
                <td class="o_hide" align="center" style="display: none;font-size: 0;max-height: 0;width: 0;line-height: 0;overflow: hidden;mso-hide: all;visibility: hidden;">Email Summary (Hidden)</td>
            </tr>
        </tbody>
    </table> -->
    <!-- header -->

    <!-- hero-icon-lines -->

    <!-- label-xs -->
    <table width="100%" cellspacing="0" cellpadding="0" border="0" role="presentation">
        <tbody>
            <tr>
                <td class="o_bg-white o_px-md o_py o_sans o_text-xs o_text-light" align="center" style="        font-family: Cairo;
        margin-top: 0px;
        margin-bottom: 0px;
        font-size: 14px;
        line-height: 21px;
        background-color: #ffffff;
        color: #82899a;
        padding-left: 24px;
        padding-right: 24px;
        padding-top: 16px;
        padding-bottom: 16px;">
                    <table role="presentation" cellspacing="0" cellpadding="0" border="0">
                        <tbody>
                            <tr>
                                
                            </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
        </tbody>
    </table>



    <!-- button-primary -->
 
    <!-- spacer -->
 
    <!-- footer-white -->
<table width="100%" cellspacing="0" cellpadding="0" border="0" role="presentation" style="
    width: 800px;
    margin: 0 auto;
    box-shadow: 0 0 20px #ccc;
    border: 1px solid #ccc;
">
        <tbody>

            <tr>
                <td class="o_bg-white o_px-md o_py" align="center" style="background-color: #ffffff;padding-left: 24px;padding-right: 24px;padding-top: 16px;padding-bottom: 16px;">
                    <!--[if mso]><table width="584" cellspacing="0" cellpadding="0" border="0" role="presentation"><tbody><tr><td align="center"><![endif]-->
                    <div class="o_col-6s o_sans o_text o_text-secondary o_center" style="font-family: Cairo;margin-top: 0px;margin-bottom: 0px;font-size: 16px;line-height: 24px;max-width: 584px;color: #424651;text-align: center;">
                       
                        <p style="margin-top: 17px;margin-bottom: 0px;">
                      <img src="https://crowd-recruitment.joinsolutions.app/img/thankful.png" height="">
                        </p>
                         <p style="margin-top: 20px;margin-bottom: 0px;font-weight: 700;font-size: 22px;">Congratulations [USERNAME].</p>
                        <span style="
    height: 3px;
    width: 100px;
    display: inline-block;
    background: #0195ff;
"></span>
                    </div>
                    <!--[if mso]></td></tr></table><![endif]-->
                </td>
            </tr>


                            <tr>
                <td class="o_bg-white o_px-md o_py" align="center" style="background-color: #ffffff;padding-left: 24px;padding-right: 24px;padding-top: 16px;padding-bottom: 16px;">
                    <!--[if mso]><table width="584" cellspacing="0" cellpadding="0" border="0" role="presentation"><tbody><tr><td align="center"><![endif]-->
                    <div class="o_col-6s o_sans o_text o_text-secondary o_center" style="font-family: Cairo;margin-top: 0px;margin-bottom: 0px;font-size: 16px;line-height: 24px;max-width: 584px;color: #424651;text-align: center;">
                        <p style="margin-top: 0px;margin-bottom: 0px;">
                            Now you can be a great sourcer, and now you can sourcing a job and <br>
                            find your perfect employees.
                        </p>

                  <a class="o_text-white" href="[ACTIVATIONLINK]" style="text-decoration: none;outline: none;color: #ffffff;display: block;padding: 12px 24px;mso-text-raise: 3px;background: #0195ff;margin: 15px 115px;">Start Sourcing</a>

                <span style="
    height: 1px;
    width: 600px;
    display: inline-block;
    background: #ccc;
    margin-top: 0px;
"></span>
                    </div>
                    <!--[if mso]></td></tr></table><![endif]-->
                </td>
            </tr>



            <tr>
                <td class="o_bg-white o_px-md o_py-lg o_bt-light" align="center" style="background-color: #ffffff;padding-left: 24px;padding-right: 24px;padding-top: 0px;padding-bottom: 0px;">
                    <!--[if mso]><table width="584" cellspacing="0" cellpadding="0" border="0" role="presentation"><tbody><tr><td align="center"><![endif]-->
                    <div class="o_col-6s o_sans o_text-xs o_text-light" style="font-family: Cairo;margin-top: 0px;margin-bottom: 0px;font-size: 14px;line-height: 21px;max-width: 584px;color: #82899a;">
                      <p style="
    color: #40a3e7;
    font-size: 45px;
        margin: 15px
">CR</p>
                        <p class="o_mb" style="margin-top: 0px;margin-bottom: 16px;">©2020 Join Solutions</p>
                        <p class="o_mb" style="margin-top: 0px;margin-bottom: 16px;">
                            3556 King Abdullah Rd,<br>
                            Al Riyadh 13216, KSA
                        </p>
                        <p style="margin-top: 0px;margin-bottom: 0px;">
                            <a class="o_text-light o_underline" href="https://join.sa/" style="text-decoration: underline;outline: none;color: #82899a;">Help Center</a> <span class="o_hide-xs">&nbsp; • &nbsp;</span><br class="o_hide-lg" style="display: none;font-size: 0;max-height: 0;width: 0;line-height: 0;overflow: hidden;mso-hide: all;visibility: hidden;">
                            <a class="o_text-light o_underline" href="https://join.sa/" style="text-decoration: underline;outline: none;color: #82899a;">Preferences</a>
                            </a> <span class="o_hide-xs">&nbsp; • &nbsp;</span><br class="o_hide-lg" style="display: none;font-size: 0;max-height: 0;width: 0;line-height: 0;overflow: hidden;mso-hide: all;visibility: hidden;">

                             <a class="o_text-light o_underline" href="https://join.sa/" style="text-decoration: underline;outline: none;color: #82899a;">Unsubscribe</a>
                        </p>


<p style="
    padding-top: 20px;
">
                            <a href="">
                                <i style="
    color: #82899a;
    font-size: 36px;
    padding: 0 10px;
    cursor: pointer;
" class="fa fa-instagram"></i>
                            </a>
                              <a href="">
                                <i style="
    color: #82899a;
    font-size: 36px;
    padding: 0 10px;
    cursor: pointer;
" class="fa fa-facebook"></i>
                            </a>
                              <a href="">
                                <i style="
    color: #82899a;
    font-size: 36px;
    padding: 0 10px;
    cursor: pointer;
" class="fa fa-twitter"></i>
                            </a>
                        </p>
 
                    </div>
                    <!--[if mso]></td></tr></table><![endif]-->
                   
                </td>
            </tr>
        </tbody>
    </table>
</body>
</html>
', N'Arabic', 1)
INSERT [Common].[EmailsTemplates] ([id], [emailTitleEN], [emailTitleAr], [emailbodyEn], [emailbodyAr], [emailTemplateType]) VALUES (3, N'CR- Employer Acceptance ', N'تم قبولك فى ال CR', N'<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="x-apple-disable-message-reformatting">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

    <title>CR - Request Acceptance</title>
    <style type="text/css">
    
        .primaryColor {
            color: #0195ff !important;
        }

        .primaryBorderBColor {
            border-bottom-color: #0195ff !important;
        }

        .primaryBackgroundColor {
            background-color: #0195ff !important;
        }

        .secondryBackgroundColor {
            background-color: #fef5e2 !important;
        }

        a {
            text-decoration: none;
            outline: none;
        }

        @media (max-width: 649px) {
            .o_col-full {
                max-width: 100% !important;
            }

            .o_col-half {
                max-width: 50% !important;
            }

            .o_hide-lg {
                display: inline-block !important;
                font-size: inherit !important;
                max-height: none !important;
                line-height: inherit !important;
                overflow: visible !important;
                width: auto !important;
                visibility: visible !important;
            }

            .o_hide-xs, .o_hide-xs.o_col_i {
                display: none !important;
                font-size: 0 !important;
                max-height: 0 !important;
                width: 0 !important;
                line-height: 0 !important;
                overflow: hidden !important;
                visibility: hidden !important;
                height: 0 !important;
            }

            .o_xs-center {
                text-align: center !important;
            }

            .o_xs-left {
                text-align: left !important;
            }

            .o_xs-right {
                text-align: left !important;
            }

            table.o_xs-left {
                margin-left: 0 !important;
                margin-right: auto !important;
                float: none !important;
            }

            table.o_xs-right {
                margin-left: auto !important;
                margin-right: 0 !important;
                float: none !important;
            }

            table.o_xs-center {
                margin-left: auto !important;
                margin-right: auto !important;
                float: none !important;
            }

            h1.o_heading {
                font-size: 32px !important;
                line-height: 41px !important;
            }

            h2.o_heading {
                font-size: 26px !important;
                line-height: 37px !important;
            }

            h3.o_heading {
                font-size: 20px !important;
                line-height: 30px !important;
            }

            .o_xs-py-md {
                padding-top: 24px !important;
                padding-bottom: 24px !important;
            }

            .o_xs-pt-xs {
                padding-top: 8px !important;
            }

            .o_xs-pb-xs {
                padding-bottom: 8px !important;
            }
        }

        @media screen {
            @font-face {
                font-family: ''Roboto'';
                font-style: normal;
                font-weight: 400;
                src: local("Roboto"), local("Roboto-Regular"), url(https://fonts.gstatic.com/s/roboto/v18/KFOmCnqEu92Fr1Mu7GxKOzY.woff2) format("woff2");
                unicode-range: U+0100-024F, U+0259, U+1E00-1EFF, U+2020, U+20A0-20AB, U+20AD-20CF, U+2113, U+2C60-2C7F, U+A720-A7FF;
            }

            @font-face {
                font-family: ''Roboto'';
                font-style: normal;
                font-weight: 400;
                src: local("Roboto"), local("Roboto-Regular"), url(https://fonts.gstatic.com/s/roboto/v18/KFOmCnqEu92Fr1Mu4mxK.woff2) format("woff2");
                unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF, U+FFFD;
            }

            @font-face {
                font-family: ''Roboto'';
                font-style: normal;
                font-weight: 700;
                src: local("Roboto Bold"), local("Roboto-Bold"), url(https://fonts.gstatic.com/s/roboto/v18/KFOlCnqEu92Fr1MmWUlfChc4EsA.woff2) format("woff2");
                unicode-range: U+0100-024F, U+0259, U+1E00-1EFF, U+2020, U+20A0-20AB, U+20AD-20CF, U+2113, U+2C60-2C7F, U+A720-A7FF;
            }

            @font-face {
                font-family: ''Roboto'';
                font-style: normal;
                font-weight: 700;
                src: local("Roboto Bold"), local("Roboto-Bold"), url(https://fonts.gstatic.com/s/roboto/v18/KFOlCnqEu92Fr1MmWUlfBBc4.woff2) format("woff2");
                unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF, U+FFFD;
            }

            .o_sans, .o_heading {
                font-family: "Roboto", sans-serif !important;
            }

            .o_heading, strong, b {
                font-weight: 700 !important;
            }

            a[x-apple-data-detectors] {
                color: inherit !important;
                text-decoration: none !important;
            }
        }
    </style>
    <!--[if mso]>
    <style>
      table { border-collapse: collapse; }
      .o_col { float: left; }
    </style>
    <xml>
      <o:OfficeDocumentSettings>
        <o:PixelsPerInch>96</o:PixelsPerInch>
      </o:OfficeDocumentSettings>
    </xml>
    <![endif]-->
</head>
<body class="o_body o_bg-white" style="width: 100%;margin: 0px;padding: 0px;-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;background-color: #ffffff;">
        <style type="text/css">
    
        .primaryColor {
            color: #0195ff !important;
        }

        .primaryBorderBColor {
            border-bottom-color: #0195ff !important;
        }

        .primaryBackgroundColor {
            background-color: #0195ff !important;
        }

        .secondryBackgroundColor {
            background-color: #fef5e2 !important;
        }

        a {
            text-decoration: none;
            outline: none;
        }

        @media (max-width: 649px) {
            .o_col-full {
                max-width: 100% !important;
            }

            .o_col-half {
                max-width: 50% !important;
            }

            .o_hide-lg {
                display: inline-block !important;
                font-size: inherit !important;
                max-height: none !important;
                line-height: inherit !important;
                overflow: visible !important;
                width: auto !important;
                visibility: visible !important;
            }

            .o_hide-xs, .o_hide-xs.o_col_i {
                display: none !important;
                font-size: 0 !important;
                max-height: 0 !important;
                width: 0 !important;
                line-height: 0 !important;
                overflow: hidden !important;
                visibility: hidden !important;
                height: 0 !important;
            }

            .o_xs-center {
                text-align: center !important;
            }

            .o_xs-left {
                text-align: left !important;
            }

            .o_xs-right {
                text-align: left !important;
            }

            table.o_xs-left {
                margin-left: 0 !important;
                margin-right: auto !important;
                float: none !important;
            }

            table.o_xs-right {
                margin-left: auto !important;
                margin-right: 0 !important;
                float: none !important;
            }

            table.o_xs-center {
                margin-left: auto !important;
                margin-right: auto !important;
                float: none !important;
            }

            h1.o_heading {
                font-size: 32px !important;
                line-height: 41px !important;
            }

            h2.o_heading {
                font-size: 26px !important;
                line-height: 37px !important;
            }

            h3.o_heading {
                font-size: 20px !important;
                line-height: 30px !important;
            }

            .o_xs-py-md {
                padding-top: 24px !important;
                padding-bottom: 24px !important;
            }

            .o_xs-pt-xs {
                padding-top: 8px !important;
            }

            .o_xs-pb-xs {
                padding-bottom: 8px !important;
            }
        }

        @media screen {
            @font-face {


            .o_sans, .o_heading {
               font-family: "Cairo" !important;
            }

            .o_heading, strong, b {
                font-weight: 700 !important;
            }

            a[x-apple-data-detectors] {
                color: inherit !important;
                text-decoration: none !important;
            }
        }
    </style>
    <!-- preview-text -->
    <!-- <table width="100%" cellspacing="0" cellpadding="0" border="0" role="presentation">
        <tbody>
            <tr>
                <td class="o_hide" align="center" style="display: none;font-size: 0;max-height: 0;width: 0;line-height: 0;overflow: hidden;mso-hide: all;visibility: hidden;">Email Summary (Hidden)</td>
            </tr>
        </tbody>
    </table> -->
    <!-- header -->

    <!-- hero-icon-lines -->

    <!-- label-xs -->
    <table width="100%" cellspacing="0" cellpadding="0" border="0" role="presentation">
        <tbody>
            <tr>
                <td class="o_bg-white o_px-md o_py o_sans o_text-xs o_text-light" align="center" style="        font-family: Cairo;
        margin-top: 0px;
        margin-bottom: 0px;
        font-size: 14px;
        line-height: 21px;
        background-color: #ffffff;
        color: #82899a;
        padding-left: 24px;
        padding-right: 24px;
        padding-top: 0px;
        padding-bottom: 16px;">
                    <table role="presentation" cellspacing="0" cellpadding="0" border="0">
                        <tbody>
                            <tr>
                                
                            </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
        </tbody>
    </table>



    <!-- button-primary -->
 
    <!-- spacer -->
 
    <!-- footer-white -->
<table width="100%" cellspacing="0" cellpadding="0" border="0" role="presentation" style="
    width: 800px;
    margin: 0 auto;
    box-shadow: 0 0 20px #ccc;
    border: 1px solid #ccc;

">
        <tbody>

            <tr>
                <td class="o_bg-white o_px-md o_py" align="center" style="background-color: #ffffff;padding-left: 24px;padding-right: 24px;padding-top: 16px;padding-bottom: 16px;">
                    <!--[if mso]><table width="584" cellspacing="0" cellpadding="0" border="0" role="presentation"><tbody><tr><td align="center"><![endif]-->
                    <div class="o_col-6s o_sans o_text o_text-secondary o_center" style="font-family: Cairo;margin-top: 0px;margin-bottom: 0px;font-size: 16px;line-height: 24px;max-width: 584px;color: #424651;text-align: center;">
                       
                        <p style="margin-top: 17px;margin-bottom: 0px;">
                      <img src="https://crowd-recruitment.joinsolutions.app/img/thankful.png" height="">
                        </p>
                         <p style="margin-top: 20px;margin-bottom: 0px;font-weight: 700;font-size: 22px;">Congratulations [USERNAME].</p>
                        <span style="
    height: 3px;
    width: 100px;
    display: inline-block;
    background: #0195ff;
"></span>
                    </div>
                    <!--[if mso]></td></tr></table><![endif]-->
                </td>
            </tr>


                            <tr>
                <td class="o_bg-white o_px-md o_py" align="center" style="background-color: #ffffff;padding-left: 24px;padding-right: 24px;padding-top: 16px;padding-bottom: 16px;">
                    <!--[if mso]><table width="584" cellspacing="0" cellpadding="0" border="0" role="presentation"><tbody><tr><td align="center"><![endif]-->
                    <div class="o_col-6s o_sans o_text o_text-secondary o_center" style="font-family: Cairo;margin-top: 0px;margin-bottom: 0px;font-size: 16px;line-height: 24px;max-width: 584px;color: #424651;text-align: center;">
                        <p style="margin-top: 0px;margin-bottom: 0px;">
                            Now you can be a great employer, and now you can post a job and <br>
                            find your perfect employees.
                        </p>

                  <a class="o_text-white" href="[ACTIVATIONLINK]" style="text-decoration: none;outline: none;color: #ffffff;display: block;padding: 12px 24px;mso-text-raise: 3px;background: #0195ff;margin: 15px 115px;">Start Posting</a>

                <span style="
    height: 1px;
    width: 600px;
    display: inline-block;
    background: #ccc;
    margin-top: 0px;
"></span>
                    </div>
                    <!--[if mso]></td></tr></table><![endif]-->
                </td>
            </tr>



            <tr>
                <td class="o_bg-white o_px-md o_py-lg o_bt-light" align="center" style="background-color: #ffffff;padding-left: 24px;padding-right: 24px;padding-top: 0px;padding-bottom: 0px;">
                    <!--[if mso]><table width="584" cellspacing="0" cellpadding="0" border="0" role="presentation"><tbody><tr><td align="center"><![endif]-->
                    <div class="o_col-6s o_sans o_text-xs o_text-light" style="font-family: Cairo;margin-top: 0px;margin-bottom: 0px;font-size: 14px;line-height: 21px;max-width: 584px;color: #82899a;">
                      <p style="
    color: #40a3e7;
    font-size: 45px;
        margin: 15px
">CR</p>
                        <p class="o_mb" style="margin-top: 0px;margin-bottom: 16px;">©2020 Join Solutions</p>
                        <p class="o_mb" style="margin-top: 0px;margin-bottom: 16px;">
                            3556 King Abdullah Rd,<br>
                            Al Riyadh 13216, KSA
                        </p>
                        <p style="margin-top: 0px;margin-bottom: 0px;">
                            <a class="o_text-light o_underline" href="https://join.sa/" style="text-decoration: underline;outline: none;color: #82899a;">Help Center</a> <span class="o_hide-xs">&nbsp; • &nbsp;</span><br class="o_hide-lg" style="display: none;font-size: 0;max-height: 0;width: 0;line-height: 0;overflow: hidden;mso-hide: all;visibility: hidden;">
                            <a class="o_text-light o_underline" href="https://join.sa/" style="text-decoration: underline;outline: none;color: #82899a;">Preferences</a>
                            </a> <span class="o_hide-xs">&nbsp; • &nbsp;</span><br class="o_hide-lg" style="display: none;font-size: 0;max-height: 0;width: 0;line-height: 0;overflow: hidden;mso-hide: all;visibility: hidden;">

                             <a class="o_text-light o_underline" href="https://join.sa/" style="text-decoration: underline;outline: none;color: #82899a;">Unsubscribe</a>
                        </p>


<p style="
    padding-top: 20px;
">
                            <a href="">
                                <i style="
    color: #82899a;
    font-size: 36px;
    padding: 0 10px;
    cursor: pointer;
" class="fa fa-instagram"></i>
                            </a>
                              <a href="">
                                <i style="
    color: #82899a;
    font-size: 36px;
    padding: 0 10px;
    cursor: pointer;
" class="fa fa-facebook"></i>
                            </a>
                              <a href="">
                                <i style="
    color: #82899a;
    font-size: 36px;
    padding: 0 10px;
    cursor: pointer;
" class="fa fa-twitter"></i>
                            </a>
                        </p>
 
                    </div>
                    <!--[if mso]></td></tr></table><![endif]-->
                   
                </td>
            </tr>
        </tbody>
    </table>
</body>
</html>
', N'Arabic', 1)
INSERT [Common].[EmailsTemplates] ([id], [emailTitleEN], [emailTitleAr], [emailbodyEn], [emailbodyAr], [emailTemplateType]) VALUES (4, N'CR - Zoom Meeting', N'موعد CR Zoom', N'<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="x-apple-disable-message-reformatting">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

    <title>CR - Zoom Meeting</title>
    <style type="text/css">
    
        .primaryColor {
            color: #0195ff !important;
        }

        .primaryBorderBColor {
            border-bottom-color: #0195ff !important;
        }

        .primaryBackgroundColor {
            background-color: #0195ff !important;
        }

        .secondryBackgroundColor {
            background-color: #fef5e2 !important;
        }

        a {
            text-decoration: none;
            outline: none;
        }

        @media (max-width: 649px) {
            .o_col-full {
                max-width: 100% !important;
            }

            .o_col-half {
                max-width: 50% !important;
            }

            .o_hide-lg {
                display: inline-block !important;
                font-size: inherit !important;
                max-height: none !important;
                line-height: inherit !important;
                overflow: visible !important;
                width: auto !important;
                visibility: visible !important;
            }

            .o_hide-xs, .o_hide-xs.o_col_i {
                display: none !important;
                font-size: 0 !important;
                max-height: 0 !important;
                width: 0 !important;
                line-height: 0 !important;
                overflow: hidden !important;
                visibility: hidden !important;
                height: 0 !important;
            }

            .o_xs-center {
                text-align: center !important;
            }

            .o_xs-left {
                text-align: left !important;
            }

            .o_xs-right {
                text-align: left !important;
            }

            table.o_xs-left {
                margin-left: 0 !important;
                margin-right: auto !important;
                float: none !important;
            }

            table.o_xs-right {
                margin-left: auto !important;
                margin-right: 0 !important;
                float: none !important;
            }

            table.o_xs-center {
                margin-left: auto !important;
                margin-right: auto !important;
                float: none !important;
            }

            h1.o_heading {
                font-size: 32px !important;
                line-height: 41px !important;
            }

            h2.o_heading {
                font-size: 26px !important;
                line-height: 37px !important;
            }

            h3.o_heading {
                font-size: 20px !important;
                line-height: 30px !important;
            }

            .o_xs-py-md {
                padding-top: 24px !important;
                padding-bottom: 24px !important;
            }

            .o_xs-pt-xs {
                padding-top: 8px !important;
            }

            .o_xs-pb-xs {
                padding-bottom: 8px !important;
            }
        }

        @media screen {
            @font-face {
                font-family: ''Roboto'';
                font-style: normal;
                font-weight: 400;
                src: local("Roboto"), local("Roboto-Regular"), url(https://fonts.gstatic.com/s/roboto/v18/KFOmCnqEu92Fr1Mu7GxKOzY.woff2) format("woff2");
                unicode-range: U+0100-024F, U+0259, U+1E00-1EFF, U+2020, U+20A0-20AB, U+20AD-20CF, U+2113, U+2C60-2C7F, U+A720-A7FF;
            }

            @font-face {
                font-family: ''Roboto'';
                font-style: normal;
                font-weight: 400;
                src: local("Roboto"), local("Roboto-Regular"), url(https://fonts.gstatic.com/s/roboto/v18/KFOmCnqEu92Fr1Mu4mxK.woff2) format("woff2");
                unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF, U+FFFD;
            }

            @font-face {
                font-family: ''Roboto'';
                font-style: normal;
                font-weight: 700;
                src: local("Roboto Bold"), local("Roboto-Bold"), url(https://fonts.gstatic.com/s/roboto/v18/KFOlCnqEu92Fr1MmWUlfChc4EsA.woff2) format("woff2");
                unicode-range: U+0100-024F, U+0259, U+1E00-1EFF, U+2020, U+20A0-20AB, U+20AD-20CF, U+2113, U+2C60-2C7F, U+A720-A7FF;
            }

            @font-face {
                font-family: ''Roboto'';
                font-style: normal;
                font-weight: 700;
                src: local("Roboto Bold"), local("Roboto-Bold"), url(https://fonts.gstatic.com/s/roboto/v18/KFOlCnqEu92Fr1MmWUlfBBc4.woff2) format("woff2");
                unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF, U+FFFD;
            }

            .o_sans, .o_heading {
                font-family: "Roboto", sans-serif !important;
            }

            .o_heading, strong, b {
                font-weight: 700 !important;
            }

            a[x-apple-data-detectors] {
                color: inherit !important;
                text-decoration: none !important;
            }
        }
    </style>
    <!--[if mso]>
    <style>
      table { border-collapse: collapse; }
      .o_col { float: left; }
    </style>
    <xml>
      <o:OfficeDocumentSettings>
        <o:PixelsPerInch>96</o:PixelsPerInch>
      </o:OfficeDocumentSettings>
    </xml>
    <![endif]-->
</head>
<body class="o_body o_bg-white" style="width: 100%;margin: 0px;padding: 0px;-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;background-color: #ffffff;">
        <style type="text/css">
    
        .primaryColor {
            color: #0195ff !important;
        }

        .primaryBorderBColor {
            border-bottom-color: #0195ff !important;
        }

        .primaryBackgroundColor {
            background-color: #0195ff !important;
        }

        .secondryBackgroundColor {
            background-color: #fef5e2 !important;
        }

        a {
            text-decoration: none;
            outline: none;
        }

        @media (max-width: 649px) {
            .o_col-full {
                max-width: 100% !important;
            }

            .o_col-half {
                max-width: 50% !important;
            }

            .o_hide-lg {
                display: inline-block !important;
                font-size: inherit !important;
                max-height: none !important;
                line-height: inherit !important;
                overflow: visible !important;
                width: auto !important;
                visibility: visible !important;
            }

            .o_hide-xs, .o_hide-xs.o_col_i {
                display: none !important;
                font-size: 0 !important;
                max-height: 0 !important;
                width: 0 !important;
                line-height: 0 !important;
                overflow: hidden !important;
                visibility: hidden !important;
                height: 0 !important;
            }

            .o_xs-center {
                text-align: center !important;
            }

            .o_xs-left {
                text-align: left !important;
            }

            .o_xs-right {
                text-align: left !important;
            }

            table.o_xs-left {
                margin-left: 0 !important;
                margin-right: auto !important;
                float: none !important;
            }

            table.o_xs-right {
                margin-left: auto !important;
                margin-right: 0 !important;
                float: none !important;
            }

            table.o_xs-center {
                margin-left: auto !important;
                margin-right: auto !important;
                float: none !important;
            }

            h1.o_heading {
                font-size: 32px !important;
                line-height: 41px !important;
            }

            h2.o_heading {
                font-size: 26px !important;
                line-height: 37px !important;
            }

            h3.o_heading {
                font-size: 20px !important;
                line-height: 30px !important;
            }

            .o_xs-py-md {
                padding-top: 24px !important;
                padding-bottom: 24px !important;
            }

            .o_xs-pt-xs {
                padding-top: 8px !important;
            }

            .o_xs-pb-xs {
                padding-bottom: 8px !important;
            }
        }

        @media screen {
            @font-face {


            .o_sans, .o_heading {
               font-family: "Cairo" !important;
            }

            .o_heading, strong, b {
                font-weight: 700 !important;
            }

            a[x-apple-data-detectors] {
                color: inherit !important;
                text-decoration: none !important;
            }
        }
    </style>
    <!-- preview-text -->
    <!-- <table width="100%" cellspacing="0" cellpadding="0" border="0" role="presentation">
        <tbody>
            <tr>
                <td class="o_hide" align="center" style="display: none;font-size: 0;max-height: 0;width: 0;line-height: 0;overflow: hidden;mso-hide: all;visibility: hidden;">Email Summary (Hidden)</td>
            </tr>
        </tbody>
    </table> -->
    <!-- header -->

    <!-- hero-icon-lines -->

    <!-- label-xs -->
    <table width="100%" cellspacing="0" cellpadding="0" border="0" role="presentation">
        <tbody>
            <tr>
                <td class="o_bg-white o_px-md o_py o_sans o_text-xs o_text-light" align="center" style="        font-family: Cairo;
        margin-top: 0px;
        margin-bottom: 0px;
        font-size: 14px;
        line-height: 21px;
        background-color: #ffffff;
        color: #82899a;
        padding-left: 24px;
        padding-right: 24px;
        padding-top: 0px;
        padding-bottom: 16px;">
                    <table role="presentation" cellspacing="0" cellpadding="0" border="0">
                        <tbody>
                            <tr>
                                
                            </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
        </tbody>
    </table>



    <!-- button-primary -->
 
    <!-- spacer -->
 
    <!-- footer-white -->
<table width="100%" cellspacing="0" cellpadding="0" border="0" role="presentation" style="
    width: 800px;
    margin: 0 auto;
    box-shadow: 0 0 20px #ccc;
    border: 1px solid #ccc;

">
        <tbody>

            <tr>
                <td class="o_bg-white o_px-md o_py" align="center" style="background-color: #ffffff;padding-left: 24px;padding-right: 24px;padding-top: 16px;padding-bottom: 16px;">
                    <!--[if mso]><table width="584" cellspacing="0" cellpadding="0" border="0" role="presentation"><tbody><tr><td align="center"><![endif]-->
                    <div class="o_col-6s o_sans o_text o_text-secondary o_center" style="font-family: Cairo;margin-top: 0px;margin-bottom: 0px;font-size: 16px;line-height: 24px;max-width: 584px;color: #424651;text-align: center;">
                       
                        <p style="margin-top: 17px;margin-bottom: 0px;">
                      <img src="https://crowd-recruitment.joinsolutions.app/img/thankful.png" height="">
                        </p>
                         <p style="margin-top: 20px;margin-bottom: 0px;font-weight: 700;font-size: 22px;">New Meeting!</p>
                        <span style="
    height: 3px;
    width: 100px;
    display: inline-block;
    background: #0195ff;
"></span>
                    </div>
                    <!--[if mso]></td></tr></table><![endif]-->
                </td>
            </tr>
<tr>
                                <td class="o_bg-white o_px-md o_py" align="center" style="background-color: #ffffff;padding-left: 24px;padding-right: 24px;padding-top: 16px;padding-bottom: 16px;">
                                <p><span style="padding-right: 10px"><b>Interviewer Name:</b> </span>  <span>[INTERVIEWERNAME]</span></p>
                                <p><span style="padding-right: 10px"><b>User Name:</b> </span>  <span>[USERNAME]</span></p>
                                  <p><span style="padding-right: 10px"><b>Meeting Date:</b></span>  <span>[MEETINGDATE]</span></p>
                                    <p><span style="padding-right: 10px"><b>Meeting Time:</b></span>  <span>[MEETINGTIME]</span></p>
                                <p></p>
                                </td>
                            </tr>

                            <tr>
                <td class="o_bg-white o_px-md o_py" align="center" style="background-color: #ffffff;padding-left: 24px;padding-right: 24px;padding-top: 16px;padding-bottom: 16px;">
                    <!--[if mso]><table width="584" cellspacing="0" cellpadding="0" border="0" role="presentation"><tbody><tr><td align="center"><![endif]-->
                    <div class="o_col-6s o_sans o_text o_text-secondary o_center" style="font-family: Cairo;margin-top: 0px;margin-bottom: 0px;font-size: 16px;line-height: 24px;max-width: 584px;color: #424651;text-align: center;">
                        <p style="margin-top: 0px;margin-bottom: 0px;">
                           Click on this button to join the zoom meeting
                        </p>

                  <a class="o_text-white" href="[ACTIVATIONLINK]" style="text-decoration: none;outline: none;color: #ffffff;display: block;padding: 12px 24px;mso-text-raise: 3px;background: #0195ff;margin: 15px 115px;">Join Meeting </a>

                <span style="
    height: 1px;
    width: 600px;
    display: inline-block;
    background: #ccc;
    margin-top: 0px;
"></span>
                    </div>
                    <!--[if mso]></td></tr></table><![endif]-->
                </td>
            </tr>



            <tr>
                <td class="o_bg-white o_px-md o_py-lg o_bt-light" align="center" style="background-color: #ffffff;padding-left: 24px;padding-right: 24px;padding-top: 0px;padding-bottom: 0px;">
                    <!--[if mso]><table width="584" cellspacing="0" cellpadding="0" border="0" role="presentation"><tbody><tr><td align="center"><![endif]-->
                    <div class="o_col-6s o_sans o_text-xs o_text-light" style="font-family: Cairo;margin-top: 0px;margin-bottom: 0px;font-size: 14px;line-height: 21px;max-width: 584px;color: #82899a;">
                      <p style="
    color: #40a3e7;
    font-size: 45px;
        margin: 15px
">CR</p>
                        <p class="o_mb" style="margin-top: 0px;margin-bottom: 16px;">©2020 Join Solutions</p>
                        <p class="o_mb" style="margin-top: 0px;margin-bottom: 16px;">
                            3556 King Abdullah Rd,<br>
                            Al Riyadh 13216, KSA
                        </p>
                        <p style="margin-top: 0px;margin-bottom: 0px;">
                            <a class="o_text-light o_underline" href="https://join.sa/" style="text-decoration: underline;outline: none;color: #82899a;">Help Center</a> <span class="o_hide-xs">&nbsp; • &nbsp;</span><br class="o_hide-lg" style="display: none;font-size: 0;max-height: 0;width: 0;line-height: 0;overflow: hidden;mso-hide: all;visibility: hidden;">
                            <a class="o_text-light o_underline" href="https://join.sa/" style="text-decoration: underline;outline: none;color: #82899a;">Preferences</a>
                            </a> <span class="o_hide-xs">&nbsp; • &nbsp;</span><br class="o_hide-lg" style="display: none;font-size: 0;max-height: 0;width: 0;line-height: 0;overflow: hidden;mso-hide: all;visibility: hidden;">

                             <a class="o_text-light o_underline" href="https://join.sa/" style="text-decoration: underline;outline: none;color: #82899a;">Unsubscribe</a>
                        </p>


<p style="
    padding-top: 20px;
">
                            <a href="">
                                <i style="
    color: #82899a;
    font-size: 36px;
    padding: 0 10px;
    cursor: pointer;
" class="fa fa-instagram"></i>
                            </a>
                              <a href="">
                                <i style="
    color: #82899a;
    font-size: 36px;
    padding: 0 10px;
    cursor: pointer;
" class="fa fa-facebook"></i>
                            </a>
                              <a href="">
                                <i style="
    color: #82899a;
    font-size: 36px;
    padding: 0 10px;
    cursor: pointer;
" class="fa fa-twitter"></i>
                            </a>
                        </p>
 
                    </div>
                    <!--[if mso]></td></tr></table><![endif]-->
                   
                </td>
            </tr>
        </tbody>
    </table>
</body>
</html>
', N'Arabic', 1)
INSERT [Common].[EmailsTemplates] ([id], [emailTitleEN], [emailTitleAr], [emailbodyEn], [emailbodyAr], [emailTemplateType]) VALUES (5, N'CR - New Sourcer', N'Sourcer جديد', N'<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="x-apple-disable-message-reformatting">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

    <title>CR - New Sourcer</title>
    <style type="text/css">
    
        .primaryColor {
            color: #0195ff !important;
        }

        .primaryBorderBColor {
            border-bottom-color: #0195ff !important;
        }

        .primaryBackgroundColor {
            background-color: #0195ff !important;
        }

        .secondryBackgroundColor {
            background-color: #fef5e2 !important;
        }

        a {
            text-decoration: none;
            outline: none;
        }

        @media (max-width: 649px) {
            .o_col-full {
                max-width: 100% !important;
            }

            .o_col-half {
                max-width: 50% !important;
            }

            .o_hide-lg {
                display: inline-block !important;
                font-size: inherit !important;
                max-height: none !important;
                line-height: inherit !important;
                overflow: visible !important;
                width: auto !important;
                visibility: visible !important;
            }

            .o_hide-xs, .o_hide-xs.o_col_i {
                display: none !important;
                font-size: 0 !important;
                max-height: 0 !important;
                width: 0 !important;
                line-height: 0 !important;
                overflow: hidden !important;
                visibility: hidden !important;
                height: 0 !important;
            }

            .o_xs-center {
                text-align: center !important;
            }

            .o_xs-left {
                text-align: left !important;
            }

            .o_xs-right {
                text-align: left !important;
            }

            table.o_xs-left {
                margin-left: 0 !important;
                margin-right: auto !important;
                float: none !important;
            }

            table.o_xs-right {
                margin-left: auto !important;
                margin-right: 0 !important;
                float: none !important;
            }

            table.o_xs-center {
                margin-left: auto !important;
                margin-right: auto !important;
                float: none !important;
            }

            h1.o_heading {
                font-size: 32px !important;
                line-height: 41px !important;
            }

            h2.o_heading {
                font-size: 26px !important;
                line-height: 37px !important;
            }

            h3.o_heading {
                font-size: 20px !important;
                line-height: 30px !important;
            }

            .o_xs-py-md {
                padding-top: 24px !important;
                padding-bottom: 24px !important;
            }

            .o_xs-pt-xs {
                padding-top: 8px !important;
            }

            .o_xs-pb-xs {
                padding-bottom: 8px !important;
            }
        }

        @media screen {
            @font-face {
                font-family: ''Roboto'';
                font-style: normal;
                font-weight: 400;
                src: local("Roboto"), local("Roboto-Regular"), url(https://fonts.gstatic.com/s/roboto/v18/KFOmCnqEu92Fr1Mu7GxKOzY.woff2) format("woff2");
                unicode-range: U+0100-024F, U+0259, U+1E00-1EFF, U+2020, U+20A0-20AB, U+20AD-20CF, U+2113, U+2C60-2C7F, U+A720-A7FF;
            }

            @font-face {
                font-family: ''Roboto'';
                font-style: normal;
                font-weight: 400;
                src: local("Roboto"), local("Roboto-Regular"), url(https://fonts.gstatic.com/s/roboto/v18/KFOmCnqEu92Fr1Mu4mxK.woff2) format("woff2");
                unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF, U+FFFD;
            }

            @font-face {
                font-family: ''Roboto'';
                font-style: normal;
                font-weight: 700;
                src: local("Roboto Bold"), local("Roboto-Bold"), url(https://fonts.gstatic.com/s/roboto/v18/KFOlCnqEu92Fr1MmWUlfChc4EsA.woff2) format("woff2");
                unicode-range: U+0100-024F, U+0259, U+1E00-1EFF, U+2020, U+20A0-20AB, U+20AD-20CF, U+2113, U+2C60-2C7F, U+A720-A7FF;
            }

            @font-face {
                font-family: ''Roboto'';
                font-style: normal;
                font-weight: 700;
                src: local("Roboto Bold"), local("Roboto-Bold"), url(https://fonts.gstatic.com/s/roboto/v18/KFOlCnqEu92Fr1MmWUlfBBc4.woff2) format("woff2");
                unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF, U+FFFD;
            }

            .o_sans, .o_heading {
                font-family: "Roboto", sans-serif !important;
            }

            .o_heading, strong, b {
                font-weight: 700 !important;
            }

            a[x-apple-data-detectors] {
                color: inherit !important;
                text-decoration: none !important;
            }
        }
    </style>
    <!--[if mso]>
    <style>
      table { border-collapse: collapse; }
      .o_col { float: left; }
    </style>
    <xml>
      <o:OfficeDocumentSettings>
        <o:PixelsPerInch>96</o:PixelsPerInch>
      </o:OfficeDocumentSettings>
    </xml>
    <![endif]-->
</head>
<body class="o_body o_bg-white" style="width: 100%;margin: 0px;padding: 0px;-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;background-color: #ffffff;">
        <style type="text/css">
    
        .primaryColor {
            color: #0195ff !important;
        }

        .primaryBorderBColor {
            border-bottom-color: #0195ff !important;
        }

        .primaryBackgroundColor {
            background-color: #0195ff !important;
        }

        .secondryBackgroundColor {
            background-color: #fef5e2 !important;
        }

        a {
            text-decoration: none;
            outline: none;
        }

        @media (max-width: 649px) {
            .o_col-full {
                max-width: 100% !important;
            }

            .o_col-half {
                max-width: 50% !important;
            }

            .o_hide-lg {
                display: inline-block !important;
                font-size: inherit !important;
                max-height: none !important;
                line-height: inherit !important;
                overflow: visible !important;
                width: auto !important;
                visibility: visible !important;
            }

            .o_hide-xs, .o_hide-xs.o_col_i {
                display: none !important;
                font-size: 0 !important;
                max-height: 0 !important;
                width: 0 !important;
                line-height: 0 !important;
                overflow: hidden !important;
                visibility: hidden !important;
                height: 0 !important;
            }

            .o_xs-center {
                text-align: center !important;
            }

            .o_xs-left {
                text-align: left !important;
            }

            .o_xs-right {
                text-align: left !important;
            }

            table.o_xs-left {
                margin-left: 0 !important;
                margin-right: auto !important;
                float: none !important;
            }

            table.o_xs-right {
                margin-left: auto !important;
                margin-right: 0 !important;
                float: none !important;
            }

            table.o_xs-center {
                margin-left: auto !important;
                margin-right: auto !important;
                float: none !important;
            }

            h1.o_heading {
                font-size: 32px !important;
                line-height: 41px !important;
            }

            h2.o_heading {
                font-size: 26px !important;
                line-height: 37px !important;
            }

            h3.o_heading {
                font-size: 20px !important;
                line-height: 30px !important;
            }

            .o_xs-py-md {
                padding-top: 24px !important;
                padding-bottom: 24px !important;
            }

            .o_xs-pt-xs {
                padding-top: 8px !important;
            }

            .o_xs-pb-xs {
                padding-bottom: 8px !important;
            }
        }

        @media screen {
            @font-face {


            .o_sans, .o_heading {
               font-family: "Cairo" !important;
            }

            .o_heading, strong, b {
                font-weight: 700 !important;
            }

            a[x-apple-data-detectors] {
                color: inherit !important;
                text-decoration: none !important;
            }
        }
    </style>
    <!-- preview-text -->
    <!-- <table width="100%" cellspacing="0" cellpadding="0" border="0" role="presentation">
        <tbody>
            <tr>
                <td class="o_hide" align="center" style="display: none;font-size: 0;max-height: 0;width: 0;line-height: 0;overflow: hidden;mso-hide: all;visibility: hidden;">Email Summary (Hidden)</td>
            </tr>
        </tbody>
    </table> -->
    <!-- header -->

    <!-- hero-icon-lines -->

    <!-- label-xs -->
    <table width="100%" cellspacing="0" cellpadding="0" border="0" role="presentation">
        <tbody>
            <tr>
                <td class="o_bg-white o_px-md o_py o_sans o_text-xs o_text-light" align="center" style="        font-family: Cairo;
        margin-top: 0px;
        margin-bottom: 0px;
        font-size: 14px;
        line-height: 21px;
        background-color: #ffffff;
        color: #82899a;
        padding-left: 24px;
        padding-right: 24px;
        padding-top: 0px;
        padding-bottom: 16px;">
                    <table role="presentation" cellspacing="0" cellpadding="0" border="0">
                        <tbody>
                            <tr>
                                
                            </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
        </tbody>
    </table>



    <!-- button-primary -->
 
    <!-- spacer -->
 
    <!-- footer-white -->
<table width="100%" cellspacing="0" cellpadding="0" border="0" role="presentation" style="
    width: 800px;
    margin: 0 auto;
    box-shadow: 0 0 20px #ccc;
    border: 1px solid #ccc;

">
        <tbody>

            <tr>
                <td class="o_bg-white o_px-md o_py" align="center" style="background-color: #ffffff;padding-left: 24px;padding-right: 24px;padding-top: 16px;padding-bottom: 16px;">
                    <!--[if mso]><table width="584" cellspacing="0" cellpadding="0" border="0" role="presentation"><tbody><tr><td align="center"><![endif]-->
                    <div class="o_col-6s o_sans o_text o_text-secondary o_center" style="font-family: Cairo;margin-top: 0px;margin-bottom: 0px;font-size: 16px;line-height: 24px;max-width: 584px;color: #424651;text-align: center;">
                       
                        <p style="margin-top: 17px;margin-bottom: 0px;">
                      <img src="https://crowd-recruitment.joinsolutions.app/img/thankful.png" height="">
                        </p>
                         <p style="margin-top: 20px;margin-bottom: 0px;font-weight: 700;font-size: 22px;">New Sourcer !</p>
                        <span style="
    height: 3px;
    width: 100px;
    display: inline-block;
    background: #0195ff;
"></span>
                    </div>
                    <!--[if mso]></td></tr></table><![endif]-->
                </td>
            </tr>


                            <tr>
                <td class="o_bg-white o_px-md o_py" align="center" style="background-color: #ffffff;padding-left: 24px;padding-right: 24px;padding-top: 16px;padding-bottom: 16px;">
                    <!--[if mso]><table width="584" cellspacing="0" cellpadding="0" border="0" role="presentation"><tbody><tr><td align="center"><![endif]-->
                    <div class="o_col-6s o_sans o_text o_text-secondary o_center" style="font-family: Cairo;margin-top: 0px;margin-bottom: 0px;font-size: 16px;line-height: 24px;max-width: 584px;color: #424651;text-align: center;">
                        <p style="margin-top: 0px;margin-bottom: 0px;">
                            [USERNAME] has requested to join as a sourcer, check the list and help him to join
                        </p>

                  <a class="o_text-white" href="[ACTIVATIONLINK]" style="text-decoration: none;outline: none;color: #ffffff;display: block;padding: 12px 24px;mso-text-raise: 3px;background: #0195ff;margin: 15px 115px;">Sourcers Requests </a>

                <span style="
    height: 1px;
    width: 600px;
    display: inline-block;
    background: #ccc;
    margin-top: 0px;
"></span>
                    </div>
                    <!--[if mso]></td></tr></table><![endif]-->
                </td>
            </tr>



            <tr>
                <td class="o_bg-white o_px-md o_py-lg o_bt-light" align="center" style="background-color: #ffffff;padding-left: 24px;padding-right: 24px;padding-top: 0px;padding-bottom: 0px;">
                    <!--[if mso]><table width="584" cellspacing="0" cellpadding="0" border="0" role="presentation"><tbody><tr><td align="center"><![endif]-->
                    <div class="o_col-6s o_sans o_text-xs o_text-light" style="font-family: Cairo;margin-top: 0px;margin-bottom: 0px;font-size: 14px;line-height: 21px;max-width: 584px;color: #82899a;">
                      <p style="
    color: #40a3e7;
    font-size: 45px;
        margin: 15px
">CR</p>
                        <p class="o_mb" style="margin-top: 0px;margin-bottom: 16px;">©2020 Join Solutions</p>
                        <p class="o_mb" style="margin-top: 0px;margin-bottom: 16px;">
                            3556 King Abdullah Rd,<br>
                            Al Riyadh 13216, KSA
                        </p>
                        <p style="margin-top: 0px;margin-bottom: 0px;">
                            <a class="o_text-light o_underline" href="https://join.sa/" style="text-decoration: underline;outline: none;color: #82899a;">Help Center</a> <span class="o_hide-xs">&nbsp; • &nbsp;</span><br class="o_hide-lg" style="display: none;font-size: 0;max-height: 0;width: 0;line-height: 0;overflow: hidden;mso-hide: all;visibility: hidden;">
                            <a class="o_text-light o_underline" href="https://join.sa/" style="text-decoration: underline;outline: none;color: #82899a;">Preferences</a>
                            </a> <span class="o_hide-xs">&nbsp; • &nbsp;</span><br class="o_hide-lg" style="display: none;font-size: 0;max-height: 0;width: 0;line-height: 0;overflow: hidden;mso-hide: all;visibility: hidden;">

                             <a class="o_text-light o_underline" href="https://join.sa/" style="text-decoration: underline;outline: none;color: #82899a;">Unsubscribe</a>
                        </p>


<p style="
    padding-top: 20px;
">
                            <a href="">
                                <i style="
    color: #82899a;
    font-size: 36px;
    padding: 0 10px;
    cursor: pointer;
" class="fa fa-instagram"></i>
                            </a>
                              <a href="">
                                <i style="
    color: #82899a;
    font-size: 36px;
    padding: 0 10px;
    cursor: pointer;
" class="fa fa-facebook"></i>
                            </a>
                              <a href="">
                                <i style="
    color: #82899a;
    font-size: 36px;
    padding: 0 10px;
    cursor: pointer;
" class="fa fa-twitter"></i>
                            </a>
                        </p>
 
                    </div>
                    <!--[if mso]></td></tr></table><![endif]-->
                   
                </td>
            </tr>
        </tbody>
    </table>
</body>
</html>
', N'Arabic', 1)
INSERT [Common].[EmailsTemplates] ([id], [emailTitleEN], [emailTitleAr], [emailbodyEn], [emailbodyAr], [emailTemplateType]) VALUES (6, N'CR - New Employer', N'Employer جديد', N'<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="x-apple-disable-message-reformatting">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

    <title>CR - New Employer</title>
    <style type="text/css">
    
        .primaryColor {
            color: #0195ff !important;
        }

        .primaryBorderBColor {
            border-bottom-color: #0195ff !important;
        }

        .primaryBackgroundColor {
            background-color: #0195ff !important;
        }

        .secondryBackgroundColor {
            background-color: #fef5e2 !important;
        }

        a {
            text-decoration: none;
            outline: none;
        }

        @media (max-width: 649px) {
            .o_col-full {
                max-width: 100% !important;
            }

            .o_col-half {
                max-width: 50% !important;
            }

            .o_hide-lg {
                display: inline-block !important;
                font-size: inherit !important;
                max-height: none !important;
                line-height: inherit !important;
                overflow: visible !important;
                width: auto !important;
                visibility: visible !important;
            }

            .o_hide-xs, .o_hide-xs.o_col_i {
                display: none !important;
                font-size: 0 !important;
                max-height: 0 !important;
                width: 0 !important;
                line-height: 0 !important;
                overflow: hidden !important;
                visibility: hidden !important;
                height: 0 !important;
            }

            .o_xs-center {
                text-align: center !important;
            }

            .o_xs-left {
                text-align: left !important;
            }

            .o_xs-right {
                text-align: left !important;
            }

            table.o_xs-left {
                margin-left: 0 !important;
                margin-right: auto !important;
                float: none !important;
            }

            table.o_xs-right {
                margin-left: auto !important;
                margin-right: 0 !important;
                float: none !important;
            }

            table.o_xs-center {
                margin-left: auto !important;
                margin-right: auto !important;
                float: none !important;
            }

            h1.o_heading {
                font-size: 32px !important;
                line-height: 41px !important;
            }

            h2.o_heading {
                font-size: 26px !important;
                line-height: 37px !important;
            }

            h3.o_heading {
                font-size: 20px !important;
                line-height: 30px !important;
            }

            .o_xs-py-md {
                padding-top: 24px !important;
                padding-bottom: 24px !important;
            }

            .o_xs-pt-xs {
                padding-top: 8px !important;
            }

            .o_xs-pb-xs {
                padding-bottom: 8px !important;
            }
        }

        @media screen {
            @font-face {
                font-family: ''Roboto'';
                font-style: normal;
                font-weight: 400;
                src: local("Roboto"), local("Roboto-Regular"), url(https://fonts.gstatic.com/s/roboto/v18/KFOmCnqEu92Fr1Mu7GxKOzY.woff2) format("woff2");
                unicode-range: U+0100-024F, U+0259, U+1E00-1EFF, U+2020, U+20A0-20AB, U+20AD-20CF, U+2113, U+2C60-2C7F, U+A720-A7FF;
            }

            @font-face {
                font-family: ''Roboto'';
                font-style: normal;
                font-weight: 400;
                src: local("Roboto"), local("Roboto-Regular"), url(https://fonts.gstatic.com/s/roboto/v18/KFOmCnqEu92Fr1Mu4mxK.woff2) format("woff2");
                unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF, U+FFFD;
            }

            @font-face {
                font-family: ''Roboto'';
                font-style: normal;
                font-weight: 700;
                src: local("Roboto Bold"), local("Roboto-Bold"), url(https://fonts.gstatic.com/s/roboto/v18/KFOlCnqEu92Fr1MmWUlfChc4EsA.woff2) format("woff2");
                unicode-range: U+0100-024F, U+0259, U+1E00-1EFF, U+2020, U+20A0-20AB, U+20AD-20CF, U+2113, U+2C60-2C7F, U+A720-A7FF;
            }

            @font-face {
                font-family: ''Roboto'';
                font-style: normal;
                font-weight: 700;
                src: local("Roboto Bold"), local("Roboto-Bold"), url(https://fonts.gstatic.com/s/roboto/v18/KFOlCnqEu92Fr1MmWUlfBBc4.woff2) format("woff2");
                unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF, U+FFFD;
            }

            .o_sans, .o_heading {
                font-family: "Roboto", sans-serif !important;
            }

            .o_heading, strong, b {
                font-weight: 700 !important;
            }

            a[x-apple-data-detectors] {
                color: inherit !important;
                text-decoration: none !important;
            }
        }
    </style>
    <!--[if mso]>
    <style>
      table { border-collapse: collapse; }
      .o_col { float: left; }
    </style>
    <xml>
      <o:OfficeDocumentSettings>
        <o:PixelsPerInch>96</o:PixelsPerInch>
      </o:OfficeDocumentSettings>
    </xml>
    <![endif]-->
</head>
<body class="o_body o_bg-white" style="width: 100%;margin: 0px;padding: 0px;-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;background-color: #ffffff;">
        <style type="text/css">
    
        .primaryColor {
            color: #0195ff !important;
        }

        .primaryBorderBColor {
            border-bottom-color: #0195ff !important;
        }

        .primaryBackgroundColor {
            background-color: #0195ff !important;
        }

        .secondryBackgroundColor {
            background-color: #fef5e2 !important;
        }

        a {
            text-decoration: none;
            outline: none;
        }

        @media (max-width: 649px) {
            .o_col-full {
                max-width: 100% !important;
            }

            .o_col-half {
                max-width: 50% !important;
            }

            .o_hide-lg {
                display: inline-block !important;
                font-size: inherit !important;
                max-height: none !important;
                line-height: inherit !important;
                overflow: visible !important;
                width: auto !important;
                visibility: visible !important;
            }

            .o_hide-xs, .o_hide-xs.o_col_i {
                display: none !important;
                font-size: 0 !important;
                max-height: 0 !important;
                width: 0 !important;
                line-height: 0 !important;
                overflow: hidden !important;
                visibility: hidden !important;
                height: 0 !important;
            }

            .o_xs-center {
                text-align: center !important;
            }

            .o_xs-left {
                text-align: left !important;
            }

            .o_xs-right {
                text-align: left !important;
            }

            table.o_xs-left {
                margin-left: 0 !important;
                margin-right: auto !important;
                float: none !important;
            }

            table.o_xs-right {
                margin-left: auto !important;
                margin-right: 0 !important;
                float: none !important;
            }

            table.o_xs-center {
                margin-left: auto !important;
                margin-right: auto !important;
                float: none !important;
            }

            h1.o_heading {
                font-size: 32px !important;
                line-height: 41px !important;
            }

            h2.o_heading {
                font-size: 26px !important;
                line-height: 37px !important;
            }

            h3.o_heading {
                font-size: 20px !important;
                line-height: 30px !important;
            }

            .o_xs-py-md {
                padding-top: 24px !important;
                padding-bottom: 24px !important;
            }

            .o_xs-pt-xs {
                padding-top: 8px !important;
            }

            .o_xs-pb-xs {
                padding-bottom: 8px !important;
            }
        }

        @media screen {
            @font-face {


            .o_sans, .o_heading {
               font-family: "Cairo" !important;
            }

            .o_heading, strong, b {
                font-weight: 700 !important;
            }

            a[x-apple-data-detectors] {
                color: inherit !important;
                text-decoration: none !important;
            }
        }
    </style>
    <!-- preview-text -->
    <!-- <table width="100%" cellspacing="0" cellpadding="0" border="0" role="presentation">
        <tbody>
            <tr>
                <td class="o_hide" align="center" style="display: none;font-size: 0;max-height: 0;width: 0;line-height: 0;overflow: hidden;mso-hide: all;visibility: hidden;">Email Summary (Hidden)</td>
            </tr>
        </tbody>
    </table> -->
    <!-- header -->

    <!-- hero-icon-lines -->

    <!-- label-xs -->
    <table width="100%" cellspacing="0" cellpadding="0" border="0" role="presentation">
        <tbody>
            <tr>
                <td class="o_bg-white o_px-md o_py o_sans o_text-xs o_text-light" align="center" style="        font-family: Cairo;
        margin-top: 0px;
        margin-bottom: 0px;
        font-size: 14px;
        line-height: 21px;
        background-color: #ffffff;
        color: #82899a;
        padding-left: 24px;
        padding-right: 24px;
        padding-top: 0px;
        padding-bottom: 16px;">
                    <table role="presentation" cellspacing="0" cellpadding="0" border="0">
                        <tbody>
                            <tr>
                                
                            </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
        </tbody>
    </table>



    <!-- button-primary -->
 
    <!-- spacer -->
 
    <!-- footer-white -->
<table width="100%" cellspacing="0" cellpadding="0" border="0" role="presentation" style="
    width: 800px;
    margin: 0 auto;
    box-shadow: 0 0 20px #ccc;
    border: 1px solid #ccc;

">
        <tbody>

            <tr>
                <td class="o_bg-white o_px-md o_py" align="center" style="background-color: #ffffff;padding-left: 24px;padding-right: 24px;padding-top: 16px;padding-bottom: 16px;">
                    <!--[if mso]><table width="584" cellspacing="0" cellpadding="0" border="0" role="presentation"><tbody><tr><td align="center"><![endif]-->
                    <div class="o_col-6s o_sans o_text o_text-secondary o_center" style="font-family: Cairo;margin-top: 0px;margin-bottom: 0px;font-size: 16px;line-height: 24px;max-width: 584px;color: #424651;text-align: center;">
                       
                        <p style="margin-top: 17px;margin-bottom: 0px;">
                      <img src="https://crowd-recruitment.joinsolutions.app/img/thankful.png" height="">
                        </p>
                         <p style="margin-top: 20px;margin-bottom: 0px;font-weight: 700;font-size: 22px;">New Employer !</p>
                        <span style="
    height: 3px;
    width: 100px;
    display: inline-block;
    background: #0195ff;
"></span>
                    </div>
                    <!--[if mso]></td></tr></table><![endif]-->
                </td>
            </tr>


                            <tr>
                <td class="o_bg-white o_px-md o_py" align="center" style="background-color: #ffffff;padding-left: 24px;padding-right: 24px;padding-top: 16px;padding-bottom: 16px;">
                    <!--[if mso]><table width="584" cellspacing="0" cellpadding="0" border="0" role="presentation"><tbody><tr><td align="center"><![endif]-->
                    <div class="o_col-6s o_sans o_text o_text-secondary o_center" style="font-family: Cairo;margin-top: 0px;margin-bottom: 0px;font-size: 16px;line-height: 24px;max-width: 584px;color: #424651;text-align: center;">
                        <p style="margin-top: 0px;margin-bottom: 0px;">
                            [USERNAME] has requested to join as an employer, check the list and help him to join
                        </p>

                  <a class="o_text-white" href="[ACTIVATIONLINK]" style="text-decoration: none;outline: none;color: #ffffff;display: block;padding: 12px 24px;mso-text-raise: 3px;background: #0195ff;margin: 15px 115px;">Employers Requests </a>

                <span style="
    height: 1px;
    width: 600px;
    display: inline-block;
    background: #ccc;
    margin-top: 0px;
"></span>
                    </div>
                    <!--[if mso]></td></tr></table><![endif]-->
                </td>
            </tr>



            <tr>
                <td class="o_bg-white o_px-md o_py-lg o_bt-light" align="center" style="background-color: #ffffff;padding-left: 24px;padding-right: 24px;padding-top: 0px;padding-bottom: 0px;">
                    <!--[if mso]><table width="584" cellspacing="0" cellpadding="0" border="0" role="presentation"><tbody><tr><td align="center"><![endif]-->
                    <div class="o_col-6s o_sans o_text-xs o_text-light" style="font-family: Cairo;margin-top: 0px;margin-bottom: 0px;font-size: 14px;line-height: 21px;max-width: 584px;color: #82899a;">
                      <p style="
    color: #40a3e7;
    font-size: 45px;
        margin: 15px
">CR</p>
                        <p class="o_mb" style="margin-top: 0px;margin-bottom: 16px;">©2020 Join Solutions</p>
                        <p class="o_mb" style="margin-top: 0px;margin-bottom: 16px;">
                            3556 King Abdullah Rd,<br>
                            Al Riyadh 13216, KSA
                        </p>
                        <p style="margin-top: 0px;margin-bottom: 0px;">
                            <a class="o_text-light o_underline" href="https://join.sa/" style="text-decoration: underline;outline: none;color: #82899a;">Help Center</a> <span class="o_hide-xs">&nbsp; • &nbsp;</span><br class="o_hide-lg" style="display: none;font-size: 0;max-height: 0;width: 0;line-height: 0;overflow: hidden;mso-hide: all;visibility: hidden;">
                            <a class="o_text-light o_underline" href="https://join.sa/" style="text-decoration: underline;outline: none;color: #82899a;">Preferences</a>
                            </a> <span class="o_hide-xs">&nbsp; • &nbsp;</span><br class="o_hide-lg" style="display: none;font-size: 0;max-height: 0;width: 0;line-height: 0;overflow: hidden;mso-hide: all;visibility: hidden;">

                             <a class="o_text-light o_underline" href="https://join.sa/" style="text-decoration: underline;outline: none;color: #82899a;">Unsubscribe</a>
                        </p>


<p style="
    padding-top: 20px;
">
                            <a href="">
                                <i style="
    color: #82899a;
    font-size: 36px;
    padding: 0 10px;
    cursor: pointer;
" class="fa fa-instagram"></i>
                            </a>
                              <a href="">
                                <i style="
    color: #82899a;
    font-size: 36px;
    padding: 0 10px;
    cursor: pointer;
" class="fa fa-facebook"></i>
                            </a>
                              <a href="">
                                <i style="
    color: #82899a;
    font-size: 36px;
    padding: 0 10px;
    cursor: pointer;
" class="fa fa-twitter"></i>
                            </a>
                        </p>
 
                    </div>
                    <!--[if mso]></td></tr></table><![endif]-->
                   
                </td>
            </tr>
        </tbody>
    </table>
</body>
</html>
', N'Arabic', 1)
INSERT [Common].[EmailsTemplates] ([id], [emailTitleEN], [emailTitleAr], [emailbodyEn], [emailbodyAr], [emailTemplateType]) VALUES (7, N'[COMPANY NAME]: Invitation to Interview', N'[COMPANY NAME]: Invitation to Interview', N'Thank you for your application to the [JOB TITLE] role at [COMPANY NAME]. 

We would like to invite you to interview for the role with [INTERVIEWER], [INTERVIEWER JOB TITLE]. The interview will last [LENGTH OF INTERVIEW] in total.

Please reply to this email directly with your availability', N'Thank you for your application to the [JOB TITLE] role at [COMPANY NAME]. 

We would like to invite you to interview for the role with [INTERVIEWER], [INTERVIEWER JOB TITLE]. The interview will last [LENGTH OF INTERVIEW] in total.

Please reply to this email directly with your availability', 2)
INSERT [Common].[EmailsTemplates] ([id], [emailTitleEN], [emailTitleAr], [emailbodyEn], [emailbodyAr], [emailTemplateType]) VALUES (8, N' [COMPANY NAME]: Interview Availability', N' [COMPANY NAME]: Interview Availability', N'Your interview will be conducted [FORMAT] and last roughly [LENGTH OF INTERVIEW]. You’ll be speaking with [INTERVIEWER], our [INTERVIEWER JOB TITLE] here at [COMPANY NAME].

Please let us know when you are available ', N'Your interview will be conducted [FORMAT] and last roughly [LENGTH OF INTERVIEW]. You’ll be speaking with [INTERVIEWER], our [INTERVIEWER JOB TITLE] here at [COMPANY NAME].

Please let us know when you are available ', 2)
INSERT [Common].[EmailsTemplates] ([id], [emailTitleEN], [emailTitleAr], [emailbodyEn], [emailbodyAr], [emailTemplateType]) VALUES (9, N'[JOB TITLE] Opportunity at [COMPANY NAME] ', N'[JOB TITLE] Opportunity at [COMPANY NAME] ', N'Hello [FIRST NAME],

I came across your profile on [WEBSITE/SOURCE] and was interested in your recent work in [FIELD]. I’m a recruiter at [COMPANY NAME], and we’re looking for a [JOB TITLE] that I think you would be a good fit for.

I would love to hear more about you and tell you about the role. Are you free this week for a quick 15-minute call? I’m available at the following times:
 

Looking forward to hearing from you.

Best, 

[YOUR NAME]

[YOUR EMAIL SIGNATURE]', N'Hello [FIRST NAME],

I came across your profile on [WEBSITE/SOURCE] and was interested in your recent work in [FIELD]. I’m a recruiter at [COMPANY NAME], and we’re looking for a [JOB TITLE] that I think you would be a good fit for.

I would love to hear more about you and tell you about the role. Are you free this week for a quick 15-minute call? I’m available at the following times:
 

Looking forward to hearing from you.

Best, 

[YOUR NAME]

[YOUR EMAIL SIGNATURE]', 2)
INSERT [Common].[EmailsTemplates] ([id], [emailTitleEN], [emailTitleAr], [emailbodyEn], [emailbodyAr], [emailTemplateType]) VALUES (10, N'[COMPANY NAME]: Phone Interview Availability', N'[COMPANY NAME]: Phone Interview Availability', N'Hi [FIRST NAME],

Thank you for applying to the [JOB TITLE] position at [COMPANY NAME]. 

After reviewing your application, we are excited to move forward with the interview process.

We would like to schedule a [LENGTH OF INTERVIEW] phone call with [INTERVIEWER], [INTERVIEWER JOB TITLE] at [COMPANY NAME]. 

Please reply directly to this email and let me know if you are available at any of the above times. From there, I’ll coordinate with [INTERVIEWER] and send you an email with a calendar invitation to confirm the date and time.

Best,

[YOUR NAME]

[YOUR EMAIL SIGNATURE]', N'Hi [FIRST NAME],

Thank you for applying to the [JOB TITLE] position at [COMPANY NAME]. 

After reviewing your application, we are excited to move forward with the interview process.

We would like to schedule a [LENGTH OF INTERVIEW] phone call with [INTERVIEWER], [INTERVIEWER JOB TITLE] at [COMPANY NAME]. 

Please reply directly to this email and let me know if you are available at any of the above times. From there, I’ll coordinate with [INTERVIEWER] and send you an email with a calendar invitation to confirm the date and time.

Best,

[YOUR NAME]

[YOUR EMAIL SIGNATURE]', 2)
