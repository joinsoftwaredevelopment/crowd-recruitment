USE [recruitment]
GO

/****** Object:  Table [Common].[PackagesFeatures]    Script Date: 21-Apr-21 2:15:37 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [Common].[PackagesFeatures](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[UUID] [varchar](50) NULL,
	[Text] [nvarchar](50) NOT NULL,
	[packageId] [int] NOT NULL,
 CONSTRAINT [PK_PackagesFeatures] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO


