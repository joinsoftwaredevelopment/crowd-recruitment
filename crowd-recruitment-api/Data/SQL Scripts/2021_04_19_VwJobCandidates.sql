USE [recruitment]
GO

/****** Object:  View [dbo].[VwJobCandidates]    Script Date: 19-Apr-21 4:04:04 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

ALTER VIEW [dbo].[VwJobCandidates]
AS
SELECT        ROW_NUMBER() OVER (ORDER BY Job.Job.Id) AS 'rowNumber', Job.Job.Id AS jobId,
 Job.Job.UUID AS jobUuId, Job.Job.JobTitle AS jobTitle, Job.Job.JobStatusId AS JobStatusId, Job.Job.FirstMailSubject AS firstMailSubject, 
Job.Job.FirstMailDescription AS firstMailDescription, Cast(CAST(Job.Job.JobDate AS date) AS nvarchar) AS jobDate, Sourcer.JobCandidateSubmission.SourcerId AS sourcerId, Sourcer.JobCandidateSubmission.isCvApproved, 
Common.Users.FirstName + ' ' + Common.Users.LastName AS sourcerName, Sourcer.JobCandidateSubmission.UUID AS submissionUuId, Sourcer.JobCandidateSubmission.id AS submissionId, 
Sourcer.JobCandidateSubmission.candidateName, Sourcer.JobCandidateSubmission.CV_Name AS cvName, Sourcer.JobCandidateSubmission.hiringStageId AS hiringStageId, 
Sourcer.JobCandidateSubmission.isAcceptOffer AS isAcceptOffer, Sourcer.JobCandidateSubmission.isHired AS isHired, Sourcer.JobCandidateSubmission.linkedInProfile, Sourcer.JobCandidateSubmission.email, 
Common.CountriesTranslation.name AS countryName, CONVERT(nvarchar, JobCandidateSubmission.SubmissionDate, 107) AS submissionDate, Common.CountriesTranslation.languageId, rejectionReason, 
JobCandidateSubmission.CandidateSubmissionStatusId, CandidateSubmissionStatusTranslation.Translation AS submissionStatusName,
    (SELECT        Count(*)
      FROM            Employer.CandidatesInterviews
      WHERE        candidateId = JobCandidateSubmission.Id AND CandidatesInterviews.isCanceled = 0) AS HasInterview
FROM            Sourcer.JobCandidateSubmission INNER JOIN
                         Employer.CandidateSubmissionStatus ON CandidateSubmissionStatus.Id = JobCandidateSubmission.CandidateSubmissionStatusId INNER JOIN
                         Job.Job ON Sourcer.JobCandidateSubmission.JobId = Job.Job.Id INNER JOIN
                         Common.Users ON Common.Users.Id = Sourcer.JobCandidateSubmission.SourcerId LEFT OUTER JOIN
                         Common.Countries ON Common.Countries.id = Sourcer.JobCandidateSubmission.locationId LEFT OUTER JOIN
                         Common.CountriesTranslation ON Common.Countries.id = Common.CountriesTranslation.countryId INNER JOIN
                         Employer.CandidateSubmissionStatusTranslation ON CandidateSubmissionStatusTranslation.CandidateSubmissionStatusId = CandidateSubmissionStatus.Id AND 
                         CandidateSubmissionStatusTranslation.languageId = CountriesTranslation.languageId
ORDER BY JobCandidateSubmission.SubmissionDate DESC OFFSET 0 ROWS
GO


