USE [recruitment]
GO

/****** Object:  View [dbo].[VwSourcers]    Script Date: 2021-03-07 6:51:17 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


ALTER VIEW [dbo].[VwSourcers]
AS
SELECT        TOP (100) PERCENT Common.Users.Id as sourcerId ,Common.Users.UUID, Common.Users.FirstName, Common.Users.LastName, Common.Users.FirstName + ' ' + Common.Users.LastName AS fullName, Common.Users.PhoneNumber, 
                         Common.Users.Email, Common.Users.IsEmailVerified, Common.Users.Password, Common.Users.RoleId, Common.Users.LanguageId, CONVERT(nvarchar, Common.Users.CreatedDate, 107) AS createdDate, 
                         Common.Users.UpdatedDate, Common.Users.Active, Common.Users.Accepted, Common.Users.MeetingDate, Common.Users.MeetingPeriodId, Common.Users.RegistrationStep, Common.Users.InterviewerId, 
                         Common.Users.meetingTimeId, Common.Users.timeZoneId, Common.Users.userStatusId, Sourcer.SourcerDetails.ExperienceTypeId, Sourcer.SourcerDetails.JobTitle, 
                         Sourcer.SourcerDetails.HasAccessToDifferentWebsite, Sourcer.SourcerDetails.IndustryId, Sourcer.SourcerDetails.CvName, Sourcer.SourcerDetails.LinkedInProfile, Common.Users.rejectedReason
FROM            Common.Users INNER JOIN
                         Sourcer.SourcerDetails ON Common.Users.Id = Sourcer.SourcerDetails.UserId
ORDER BY Common.Users.Id DESC
GO


