USE [recruitment]
GO

/****** Object:  Table [Job].[SourcerBalancesHistory]    Script Date: 2021-03-19 6:02:24 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [Job].[SourcerBalancesHistory](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[createdDate] [datetime] NOT NULL,
	[weekTitle] [nvarchar](200) NOT NULL,
	[sourcerId] [int] NOT NULL,
	[FullName] [nvarchar](200) NOT NULL,
	[JobCount] [int] NOT NULL,
	[SubmissionCount] [int] NOT NULL,
	[AcceptanceCount] [int] NOT NULL,
	[DeclinedCount] [int] NOT NULL,
	[AcceptanceRatio] [nvarchar](50) NOT NULL,
	[TotalBalance] [nvarchar](50) NOT NULL,
	[isPaid] [bit] NOT NULL,
 CONSTRAINT [PK_SourcerBalancesHistory] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO


