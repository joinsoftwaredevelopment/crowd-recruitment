USE [recruitment]
GO

/****** Object:  View [dbo].[VwJobCandidates]    Script Date: 28-Feb-21 3:01:42 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

ALTER VIEW [dbo].[VwJobCandidates]
AS
SELECT        ROW_NUMBER() OVER (ORDER BY Job.Job.Id) AS 'rowNumber', Job.Job.Id AS jobId, Job.Job.UUID AS jobUuId, Job.Job.JobTitle AS jobTitle, Job.Job.FirstMailSubject AS firstMailSubject, 
Job.Job.FirstMailDescription AS firstMailDescription, Cast(CAST(Job.Job.JobDate AS date) AS nvarchar) AS jobDate, Sourcer.JobCandidateSubmission.SourcerId AS sourcerId, 
Common.Users.FirstName + ' ' + Common.Users.LastName AS sourcerName, Sourcer.JobCandidateSubmission.UUID AS submissionUuId, Sourcer.JobCandidateSubmission.candidateName, 
Sourcer.JobCandidateSubmission.CV_Name AS cvName, Sourcer.JobCandidateSubmission.linkedInProfile, Sourcer.JobCandidateSubmission.email, Common.CountriesTranslation.name AS countryName, CONVERT(nvarchar, 
JobCandidateSubmission.SubmissionDate, 107) AS submissionDate, Common.CountriesTranslation.languageId, rejectionReason, CandidateSubmissionStatusId
FROM            Sourcer.JobCandidateSubmission INNER JOIN
                         Job.Job ON Sourcer.JobCandidateSubmission.JobId = Job.Job.Id INNER JOIN
                         Common.Users ON Common.Users.Id = Sourcer.JobCandidateSubmission.SourcerId LEFT OUTER JOIN
                         Common.Countries ON Common.Countries.id = Sourcer.JobCandidateSubmission.locationId LEFT OUTER JOIN
                         Common.CountriesTranslation ON Common.Countries.id = Common.CountriesTranslation.countryId
GO


