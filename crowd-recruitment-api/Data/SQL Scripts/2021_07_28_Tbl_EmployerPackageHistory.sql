Create Table Employer.EmployerPackageHistory
(
Id INT PRIMARY KEY IDENTITY(1,1),
UserId INT NOT NULL,
PackageId INT NOT NULL,
PackagePurchaseDate DATETIME NOT NULL,
PackageExpiryDate DATETIME NOT NULL,
FOREIGN KEY (UserId) REFERENCES Common.Users(Id),
FOREIGN KEY (PackageId) REFERENCES Common.Packages(packageId)
)

drop table Employer.EmployerPackageHistory