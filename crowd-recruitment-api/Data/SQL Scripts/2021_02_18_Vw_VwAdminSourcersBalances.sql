USE [recruitment]
GO

/****** Object:  View [dbo].[VwAdminSourcersBalances]    Script Date: 2021-02-18 5:00:39 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO








ALTER VIEW [dbo].[VwAdminSourcersBalances]
AS
Select Distinct (FirstName + ' ' + LastName) as fullName ,
  (Select Count(*) from (Select Distinct JobId from Sourcer.JobCandidateSubmission JCS  where JCS.SourcerId = Users.Id )as sourcerJobs)  as jobCount ,
  (Select Count(*) from Sourcer.JobCandidateSubmission where JobCandidateSubmission.SourcerId = Users.Id) as submissionCount,
  (Select ISNULL(SUM(ISNULL(commission,0)),0) from Sourcer.JobCandidateSubmission where JobCandidateSubmission.SourcerId = Users.Id) as totalBalance
from Common.Users
Join Sourcer.SourcerDetails ON Users.Id = SourcerDetails.UserId
Left Join Sourcer.JobCandidateSubmission ON JobCandidateSubmission.SourcerId = Users.Id
GO


