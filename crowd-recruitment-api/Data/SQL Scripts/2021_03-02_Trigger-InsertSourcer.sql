USE [recruitment]
GO

/****** Object:  Trigger [Common].[InsertSourcer]    Script Date: 2021-03-02 10:26:01 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE TRIGGER [Common].[InsertSourcer]
   ON  [Common].[Users]
   AFTER INSERT
AS 
BEGIN

	SET NOCOUNT ON;

	Declare @userId int = (Select Id from inserted)
    Declare @roleId int = (Select RoleId from inserted)

	IF(@roleId = 3)
	    Insert INTO Sourcer.SourcerDetails (UserId) Values (@userId)

END
GO

ALTER TABLE [Common].[Users] ENABLE TRIGGER [InsertSourcer]
GO


