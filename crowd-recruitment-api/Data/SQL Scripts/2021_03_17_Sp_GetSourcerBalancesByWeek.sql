USE [recruitment]
GO
/****** Object:  StoredProcedure [dbo].[GetSourcerBalancesByWeek]    Script Date: 2021-03-16 11:01:51 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
Alter PROCEDURE [dbo].[GetSourcerBalancesByWeek]
	-- Add the parameters for the stored procedure here
	@startDate dateTime, 
	@endDate dateTime
AS
BEGIN
	SET NOCOUNT ON;
    -- Insert statements for procedure here
	SELECT DISTINCT Common.Users.Id as sourcerId ,Common.Users.FirstName + ' ' + Common.Users.LastName AS fullName,
                             (SELECT        COUNT(*) AS jobCount
                               FROM            (SELECT DISTINCT JobId
                                                         FROM            Sourcer.JobCandidateSubmission AS JCS
                                                         WHERE        (SourcerId = Common.Users.Id AND (  CONVERT(DATE,JCS.SubmissionDate) >= CONVERT(DATE,@startDate)  AND  CONVERT(DATE,JCS.SubmissionDate) <= CONVERT(DATE,  @endDate)))) AS sourcerJobs) AS jobCount,
                             (SELECT        COUNT(*) AS submissionCount
                               FROM            Sourcer.JobCandidateSubmission
                               WHERE        (SourcerId = Common.Users.Id AND (  CONVERT(DATE,JobCandidateSubmission.SubmissionDate) >= CONVERT(DATE,@startDate)  AND  CONVERT(DATE,JobCandidateSubmission.SubmissionDate) <= CONVERT(DATE,  @endDate)))) AS submissionCount,
                             (SELECT        ISNULL(SUM(ISNULL(commission, 0)), 0) AS Expr1
                               FROM            Sourcer.JobCandidateSubmission AS JCS
                               WHERE        (SourcerId = Common.Users.Id AND (  CONVERT(DATE,JCS.SubmissionDate) >= CONVERT(DATE,@startDate)  AND CONVERT(DATE,JCS.SubmissionDate) <= CONVERT(DATE,  @endDate)))) AS totalBalance,
                             (SELECT        COUNT(*) AS Expr1
                               FROM            Sourcer.JobCandidateSubmission AS JCS
                               WHERE        (SourcerId = Common.Users.Id AND (  CONVERT(DATE,JCS.SubmissionDate) >= CONVERT(DATE,@startDate)  AND CONVERT(DATE,JCS.SubmissionDate) <= CONVERT(DATE,  @endDate))) AND (CandidateSubmissionStatusId = 3 OR CandidateSubmissionStatusId = 5 OR CandidateSubmissionStatusId = 6 )) * 100 /
                             (SELECT        NULLIF (COUNT(*), 0) AS Expr1
                               FROM            Sourcer.JobCandidateSubmission AS JCS
                               WHERE        (SourcerId = Common.Users.Id AND (  CONVERT(DATE,JCS.SubmissionDate) >= CONVERT(DATE,@startDate)  AND CONVERT(DATE,JCS.SubmissionDate) <= CONVERT(DATE,  @endDate)))) AS acceptanceRatio,
                             (SELECT        COUNT(*) AS Expr1
                               FROM            Sourcer.JobCandidateSubmission AS JCS
                               WHERE        (SourcerId = Common.Users.Id AND (  CONVERT(DATE,JCS.SubmissionDate) >= CONVERT(DATE,@startDate)  AND CONVERT(DATE,JCS.SubmissionDate) <= CONVERT(DATE,  @endDate))) AND (CandidateSubmissionStatusId = 3 OR CandidateSubmissionStatusId = 5 OR CandidateSubmissionStatusId = 6 )) AS acceptanceCount,
                             (SELECT        COUNT(*) AS declinedCount
                               FROM            Sourcer.JobCandidateSubmission AS JCS
                               WHERE        (SourcerId = Common.Users.Id AND (  CONVERT(DATE,JCS.SubmissionDate) >= CONVERT(DATE,@startDate)  AND CONVERT(DATE,JCS.SubmissionDate) <= CONVERT(DATE,  CONVERT(DATE,  @endDate)))) AND (CandidateSubmissionStatusId = 4 )) AS declinedCount,
							    (CAST(0 as bit)) as isPaid
FROM            Common.Users INNER JOIN
                         Sourcer.SourcerDetails ON Common.Users.Id = Sourcer.SourcerDetails.UserId LEFT OUTER JOIN
                         Sourcer.JobCandidateSubmission AS JobCandidateSubmission_1 ON JobCandidateSubmission_1.SourcerId = Common.Users.Id
						 Where  Common.Users.userStatusId = 2 AND (  CONVERT(DATE,JobCandidateSubmission_1.SubmissionDate) >= CONVERT(DATE,@startDate)  AND  CONVERT(DATE,JobCandidateSubmission_1.SubmissionDate) <= CONVERT(DATE,  @endDate))

END
