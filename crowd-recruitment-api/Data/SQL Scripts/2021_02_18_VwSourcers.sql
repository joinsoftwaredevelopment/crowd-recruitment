USE [recruitment]
GO

/****** Object:  View [dbo].[VwSourcers]    Script Date: 2021-02-18 12:57:47 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO





ALTER VIEW [dbo].[VwSourcers]
AS
SELECT        Common.Users.UUID AS uuid, Common.Users.FirstName AS firstName, Common.Users.LastName AS lastName, (FirstName + ' ' + LastName) as fullName, Common.Users.PhoneNumber AS phoneNumber, Common.Users.Email AS email, 
                         Common.Users.IsEmailVerified AS isEmailVerified, Common.Users.Password AS password, Common.Users.RoleId AS roleId, Common.Users.LanguageId AS languageId, CONVERT(nvarchar, Common.Users.CreatedDate, 107) AS createdDate, 
                         Common.Users.UpdatedDate AS updatedDate, Common.Users.Active AS active, Common.Users.Accepted AS accepted, Common.Users.MeetingDate, Common.Users.MeetingPeriodId AS meetingPeriodId, 
                         Common.Users.RegistrationStep AS registrationStep, Common.Users.InterviewerId AS interviewerId, Common.Users.meetingTimeId, Common.Users.timeZoneId, Common.Users.userStatusId, 
                         Sourcer.SourcerDetails.ExperienceTypeId AS experienceTypeId, Sourcer.SourcerDetails.JobTitle AS jobTitle, Sourcer.SourcerDetails.HasAccessToDifferentWebsite AS hasAccessToDifferentWebsite, 
                         Sourcer.SourcerDetails.IndustryId AS industryId, Sourcer.SourcerDetails.CvName AS cvName, Sourcer.SourcerDetails.LinkedInProfile AS linkedInProfile ,rejectedReason
FROM            Common.Users INNER JOIN
                         Sourcer.SourcerDetails ON Common.Users.Id = Sourcer.SourcerDetails.UserId
GO


