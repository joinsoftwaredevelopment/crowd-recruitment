USE [recruitment]
GO

/****** Object:  View [dbo].[VwInterviewDetails]    Script Date: 14-Apr-21 3:02:39 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

ALTER VIEW [dbo].[VwInterviewDetails]
AS
SELECT        Employer.CandidatesInterviews.id, Employer.CandidatesInterviews.interviewerId, Employer.CandidatesInterviews.interviewComment, Employer.CandidatesInterviews.interviewTypeId, 
                         Employer.CandidatesInterviews.interviewDate, Employer.CandidatesInterviews.interviewTime, Employer.CandidatesInterviews.duration, Employer.CandidatesInterviews.isCanceled, 
                         Employer.CandidatesInterviews.isEmailSent, Employer.CandidatesInterviews.createdBy, Employer.CandidatesInterviews.templateId, Employer.CandidatesInterviews.emailSubject, 
                         Employer.CandidatesInterviews.emailDescription, Employer.CandidatesInterviews.interviewerTypeId, Employer.CandidatesInterviews.createdDate, 
                         Sourcer.JobCandidateSubmission.UUID AS submissionUUid
FROM            Employer.CandidatesInterviews INNER JOIN
                         Sourcer.JobCandidateSubmission ON Employer.CandidatesInterviews.candidateId = Sourcer.JobCandidateSubmission.Id
GO


