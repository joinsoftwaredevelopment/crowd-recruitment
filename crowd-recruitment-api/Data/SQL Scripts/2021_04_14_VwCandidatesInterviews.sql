USE [recruitment]
GO

/****** Object:  View [dbo].[VwCandidatesInterviews]    Script Date: 14-Apr-21 12:22:17 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

ALTER VIEW [dbo].[VwCandidatesInterviews]
AS
SELECT        Employer.CandidatesInterviews.id, Sourcer.JobCandidateSubmission.candidateName, Sourcer.JobCandidateSubmission.email AS candidateEmail, Employer.CandidatesInterviews.interviewDate, 
                         Employer.CandidatesInterviews.duration, Employer.EmployerDetails.CompanyName, Common.Interviewers.InterviewerName, Employer.CandidatesInterviews.emailSubject, 
                         Employer.CandidatesInterviews.emailDescription, Sourcer.JobCandidateSubmission.JobId, Job.Job.JobTitle, Employer.CandidatesInterviews.isCanceled, Employer.CandidatesInterviews.isEmailSent, 
                         Employer.CandidatesInterviews.interviewTime, Sourcer.JobCandidateSubmission.UUID AS submissionUuid, Employer.CandidatesInterviews.interviewComment, Employer.CandidatesInterviews.interviewerId, 
                         Employer.CandidatesInterviews.candidateId, Employer.CandidatesInterviews.interviewTypeId, Employer.CandidatesInterviews.templateId, Employer.CandidatesInterviews.createdBy, 
                         Employer.CandidatesInterviews.createdDate, Employer.CandidatesInterviews.interviewerTypeId
FROM            Employer.CandidatesInterviews INNER JOIN
                         Sourcer.JobCandidateSubmission ON Employer.CandidatesInterviews.candidateId = Sourcer.JobCandidateSubmission.Id INNER JOIN
                         Common.Users ON Common.Users.Id = Employer.CandidatesInterviews.createdBy INNER JOIN
                         Employer.EmployerDetails ON Common.Users.Id = Employer.EmployerDetails.UserId INNER JOIN
                         Common.Interviewers ON Employer.CandidatesInterviews.interviewerId = Common.Interviewers.Id INNER JOIN
                         Job.Job ON Sourcer.JobCandidateSubmission.JobId = Job.Job.Id
GO


