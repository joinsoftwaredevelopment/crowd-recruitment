
use [recruitment]

ALTER TABLE [Sourcer].[JobCandidateSubmission]
ADD EmpSubmissionStatusId int;

ALTER TABLE [Sourcer].[JobCandidateSubmission]
ADD EmpReason nvarchar(500);