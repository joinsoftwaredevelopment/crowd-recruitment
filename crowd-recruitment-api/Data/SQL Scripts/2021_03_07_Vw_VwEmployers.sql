USE [recruitment]
GO

/****** Object:  View [dbo].[VwEmployers]    Script Date: 2021-03-07 6:43:20 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



ALTER VIEW [dbo].[VwEmployers]
AS
SELECT        TOP (100) PERCENT Common.Users.Id as employerId , Common.Users.UUID, Common.Users.FirstName, Common.Users.LastName, Common.Users.FirstName + ' ' + Common.Users.LastName AS fullName, Common.Users.PhoneNumber, 
                         Common.Users.Email, Common.Users.IsEmailVerified, Common.Users.Password, Common.Users.RoleId, Common.Users.LanguageId, CONVERT(nvarchar, Common.Users.CreatedDate, 107) AS createdDate, 
                         Common.Users.UpdatedDate, Common.Users.Active, Common.Users.Accepted, Common.Users.MeetingDate, Common.Users.MeetingPeriodId, Common.Users.RegistrationStep, Common.Users.InterviewerId, 
                         Common.Users.meetingTimeId, Common.Users.timeZoneId, Common.Users.userStatusId, Employer.EmployerDetails.CompanyName, Common.Users.rejectedReason,
						 (Select COUNT(*) from Job.Job where JobStatusId = 3 aND EmployerId = Common.Users.Id ) as openedJobsCount
FROM            Common.Users INNER JOIN
                         Employer.EmployerDetails ON Common.Users.Id = Employer.EmployerDetails.UserId
ORDER BY Common.Users.Id DESC
GO


