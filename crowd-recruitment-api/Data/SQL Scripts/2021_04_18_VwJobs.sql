USE [recruitment]
GO

/****** Object:  View [dbo].[VwJobs]    Script Date: 2021-04-18 3:53:36 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


ALTER VIEW [dbo].[VwJobs]
AS
SELECT        TOP (100) PERCENT Job.Job.Id AS jobId, Job.Job.UUID, Job.Job.JobTitle, Employer.EmploymentTypeTranslation.Translation AS employmentTypeName, 
                         Employer.SeniorityLevelTranslation.Translation AS seniorityLevelName, Job.Job.companiesNotToSourceFrom, Job.JobLocationTranslation.Translation AS jobLocation, Employer.EmployerDetails.CompanyName, 
                         Job.Job.mustHaveQualification, Job.Job.niceToHaveQualification, Employer.EmployerDetails.UserId AS employerId, Job.Job.HiringNeeds, 
                         Employer.ExperienceLevelTranslation.Translation AS ExperienceLevelName, Common.Languages.id AS languageId, Job.Job.JobStatusId, Job.Job.JobDescription AS description, 
                         Employer.CompanyIndustryTranslation.Translation AS companyIndustry, Job.JobStatusTranslation.Translation AS jobStatusName, Job.Job.requirements, Job.Job.noOfSubmissions, 
                         Common.Users.FirstName + ' ' + Common.Users.LastName AS employerName, Job.Job.reason AS rejectionReason, CONVERT(nvarchar, Job.Job.JobDate, 107) AS jobDate, Job.Job.commission, 
                         Common.Users.UUID AS empoyerUuId, Job.Job.IsActive, Common.CountriesTranslation.countryId, Common.CountriesTranslation.name AS countryName, Job.Job.PeopleToHireCount, Job.Job.IsContinuousHire, 
                         Job.Job.TimetoHireId, Job.Job.MinSalary, Job.Job.MaxSalary, Job.Job.FirstMailSubject, Job.Job.FirstMailDescription, Job.Job.FindingJobDifficultyLevelId, Job.Job.CurrencyId, 
                         Common.CurrencyTranslation.name AS currencyName, '' AS timetoHireName, '' AS findingJobDifficultyName,
						 (Select Count(*) from Sourcer.JobCandidateSubmission where JobId = Job.Id) as submissonCount
FROM            Job.Job INNER JOIN
                         Employer.EmploymentType ON Job.Job.EmployerTypeId = Employer.EmploymentType.Id INNER JOIN
                         Employer.EmploymentTypeTranslation ON Employer.EmploymentType.Id = Employer.EmploymentTypeTranslation.EmploymentTypeId INNER JOIN
                         Common.Languages ON Common.Languages.id = Employer.EmploymentTypeTranslation.languageId INNER JOIN
                         Employer.SeniorityLevel ON Employer.SeniorityLevel.Id = Job.Job.SeniorityLevelId INNER JOIN
                         Employer.SeniorityLevelTranslation ON Employer.SeniorityLevelTranslation.SeniorityLevelId = Employer.SeniorityLevel.Id AND 
                         Employer.SeniorityLevelTranslation.languageId = Common.Languages.id INNER JOIN
                         Job.JobLocation ON Job.JobLocation.Id = Job.Job.JobLocationId INNER JOIN
                         Job.JobLocationTranslation ON Job.JobLocationTranslation.JobLocationId = Job.JobLocation.Id AND Job.JobLocationTranslation.languageId = Common.Languages.id INNER JOIN
                         Job.JobStatus ON Job.JobStatus.Id = Job.Job.JobStatusId INNER JOIN
                         Job.JobStatusTranslation ON Job.JobStatusTranslation.JobStatusId = Job.JobStatus.Id AND Job.JobStatusTranslation.languageId = Common.Languages.id INNER JOIN
                         Common.Users ON Common.Users.Id = Job.Job.EmployerId INNER JOIN
                         Employer.EmployerDetails ON Employer.EmployerDetails.UserId = Common.Users.Id LEFT OUTER JOIN
                         Employer.ExperienceLevel ON Employer.ExperienceLevel.Id = Job.Job.ExperienceLevelId LEFT OUTER JOIN
                         Employer.ExperienceLevelTranslation ON Employer.ExperienceLevelTranslation.ExperienceLevelId = Employer.ExperienceLevel.Id AND 
                         Employer.ExperienceLevelTranslation.languageId = Common.Languages.id INNER JOIN
                         Employer.CompanyIndustry ON Job.Job.industryId = Employer.CompanyIndustry.Id INNER JOIN
                         Employer.CompanyIndustryTranslation ON Employer.CompanyIndustryTranslation.CompanyIndustryId = Employer.CompanyIndustry.Id AND 
                         Employer.CompanyIndustryTranslation.languageId = Job.JobLocationTranslation.languageId INNER JOIN
                         Common.Countries ON Job.Job.JobNationalityId = Common.Countries.id AND Job.Job.JobNationalityId = Common.Countries.id INNER JOIN
                         Common.CountriesTranslation ON Common.Countries.id = Common.CountriesTranslation.countryId AND Common.Countries.id = Common.CountriesTranslation.countryId AND 
                         Common.Countries.id = Common.CountriesTranslation.countryId AND Common.Countries.id = Common.CountriesTranslation.countryId AND Common.Countries.id = Common.CountriesTranslation.countryId AND 
                         Common.Countries.id = Common.CountriesTranslation.countryId AND Common.Countries.id = Common.CountriesTranslation.countryId AND Common.Countries.id = Common.CountriesTranslation.countryId AND 
                         Common.Countries.id = Common.CountriesTranslation.countryId AND Common.Countries.id = Common.CountriesTranslation.countryId AND Common.Countries.id = Common.CountriesTranslation.countryId AND 
                         Common.Countries.id = Common.CountriesTranslation.countryId AND Common.Countries.id = Common.CountriesTranslation.countryId AND Common.Countries.id = Common.CountriesTranslation.countryId AND 
                         Common.CountriesTranslation.languageId = Common.Languages.id INNER JOIN
                         Common.CurrencyTranslation ON Job.Job.CurrencyId = Common.CurrencyTranslation.currencyId AND Common.CurrencyTranslation.languageId = Common.Languages.id
ORDER BY Job.JobDate DESC
GO


