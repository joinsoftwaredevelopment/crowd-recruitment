USE [recruitment]
GO

/****** Object:  View [dbo].[VwJobSourcers]    Script Date: 2021-02-17 12:08:32 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

Create VIEW [dbo].[VwJobSourcers]
AS
SELECT DISTINCT Job.Job.UUID AS jobUuId, Job.Job.JobTitle AS jobTitle, 
sourcers.FirstName + ' ' + sourcers.LastName AS sourcerName,
 Sourcer.SourcerDetails.JobTitle AS sourcerJobTitle, sourcers.UUID AS sourcerUuId ,
 (Select Count(*) as submissionCount  from Sourcer.JobCandidateSubmission where SourcerId = sourcers.Id ) as submissionCount
FROM            Job.Job INNER JOIN
                         Sourcer.JobCandidateSubmission ON Sourcer.JobCandidateSubmission.JobId = Job.Job.Id INNER JOIN
                         Common.Users AS sourcers ON Sourcer.JobCandidateSubmission.SourcerId = sourcers.Id INNER JOIN
                         Sourcer.SourcerDetails ON Sourcer.SourcerDetails.UserId = sourcers.Id
GO


