USE [recruitment]
GO

/****** Object:  View [dbo].[VwAdminSourcersBalances]    Script Date: 21-Feb-21 1:59:11 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

ALTER VIEW [dbo].[VwAdminSourcersBalances]
AS
SELECT DISTINCT Common.Users.FirstName + ' ' + Common.Users.LastName AS fullName,
                             (SELECT        COUNT(*) AS Expr1
                               FROM            (SELECT DISTINCT JobId
                                                         FROM            Sourcer.JobCandidateSubmission AS JCS
                                                         WHERE        (SourcerId = Common.Users.Id)) AS sourcerJobs) AS jobCount,
                             (SELECT        COUNT(*) AS Expr1
                               FROM            Sourcer.JobCandidateSubmission
                               WHERE        (SourcerId = Common.Users.Id)) AS submissionCount,
                             (SELECT        ISNULL(SUM(ISNULL(commission, 0)), 0) AS Expr1
                               FROM            Sourcer.JobCandidateSubmission AS JobCandidateSubmission_4
                               WHERE        (SourcerId = Common.Users.Id)) AS totalBalance,
                             (SELECT        COUNT(*) AS Expr1
                               FROM            Sourcer.JobCandidateSubmission AS JobCandidateSubmission_3
                               WHERE        (SourcerId = Common.Users.Id) AND (CandidateSubmissionStatusId = 3)) * 100 /
                             (SELECT        NULLIF (COUNT(*), 0) AS Expr1
                               FROM            Sourcer.JobCandidateSubmission AS JobCandidateSubmission_2
                               WHERE        (SourcerId = Common.Users.Id)) AS acceptanceRatio
FROM            Common.Users INNER JOIN
                         Sourcer.SourcerDetails ON Common.Users.Id = Sourcer.SourcerDetails.UserId LEFT OUTER JOIN
                         Sourcer.JobCandidateSubmission AS JobCandidateSubmission_1 ON JobCandidateSubmission_1.SourcerId = Common.Users.Id
GO


