USE [recruitment]
GO
/****** Object:  Table [Job].[JobTemplateMustHaveQualification]    Script Date: 2021-03-10 2:08:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [Job].[JobTemplateMustHaveQualification](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[JobTemplateId] [int] NOT NULL,
	[Text] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_EmployerMustHaveQualification] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [Job].[JobTemplateNiceHaveQualification]    Script Date: 2021-03-10 2:08:19 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [Job].[JobTemplateNiceHaveQualification](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[JobTemplateId] [int] NOT NULL,
	[Text] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_EmployerNiceHaveQualification] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [Job].[JobTemplateMustHaveQualification] ON 

INSERT [Job].[JobTemplateMustHaveQualification] ([Id], [JobTemplateId], [Text]) VALUES (1, 2, N'HTML')
INSERT [Job].[JobTemplateMustHaveQualification] ([Id], [JobTemplateId], [Text]) VALUES (2, 2, N'CSS')
INSERT [Job].[JobTemplateMustHaveQualification] ([Id], [JobTemplateId], [Text]) VALUES (3, 2, N'Javascript')
INSERT [Job].[JobTemplateMustHaveQualification] ([Id], [JobTemplateId], [Text]) VALUES (4, 2, N'Jquery')
INSERT [Job].[JobTemplateMustHaveQualification] ([Id], [JobTemplateId], [Text]) VALUES (5, 2, N'OOP')
INSERT [Job].[JobTemplateMustHaveQualification] ([Id], [JobTemplateId], [Text]) VALUES (6, 2, N'SQL')
INSERT [Job].[JobTemplateMustHaveQualification] ([Id], [JobTemplateId], [Text]) VALUES (7, 2, N'ASP.Net')
INSERT [Job].[JobTemplateMustHaveQualification] ([Id], [JobTemplateId], [Text]) VALUES (8, 2, N'MVC')
INSERT [Job].[JobTemplateMustHaveQualification] ([Id], [JobTemplateId], [Text]) VALUES (9, 2, N'ASP.NET Core')
INSERT [Job].[JobTemplateMustHaveQualification] ([Id], [JobTemplateId], [Text]) VALUES (10, 2, N'C#')
INSERT [Job].[JobTemplateMustHaveQualification] ([Id], [JobTemplateId], [Text]) VALUES (11, 4, N'HTML')
INSERT [Job].[JobTemplateMustHaveQualification] ([Id], [JobTemplateId], [Text]) VALUES (12, 4, N'CSS')
INSERT [Job].[JobTemplateMustHaveQualification] ([Id], [JobTemplateId], [Text]) VALUES (13, 4, N'Javascript')
INSERT [Job].[JobTemplateMustHaveQualification] ([Id], [JobTemplateId], [Text]) VALUES (14, 4, N'Jquery')
INSERT [Job].[JobTemplateMustHaveQualification] ([Id], [JobTemplateId], [Text]) VALUES (15, 4, N'OOP')
INSERT [Job].[JobTemplateMustHaveQualification] ([Id], [JobTemplateId], [Text]) VALUES (16, 4, N'SQL')
INSERT [Job].[JobTemplateMustHaveQualification] ([Id], [JobTemplateId], [Text]) VALUES (17, 4, N'ASP.Net')
INSERT [Job].[JobTemplateMustHaveQualification] ([Id], [JobTemplateId], [Text]) VALUES (18, 4, N'MVC')
INSERT [Job].[JobTemplateMustHaveQualification] ([Id], [JobTemplateId], [Text]) VALUES (19, 4, N'ASP.NET Core')
INSERT [Job].[JobTemplateMustHaveQualification] ([Id], [JobTemplateId], [Text]) VALUES (20, 4, N'C#')
INSERT [Job].[JobTemplateMustHaveQualification] ([Id], [JobTemplateId], [Text]) VALUES (21, 6, N'HTML')
INSERT [Job].[JobTemplateMustHaveQualification] ([Id], [JobTemplateId], [Text]) VALUES (22, 6, N'CSS')
INSERT [Job].[JobTemplateMustHaveQualification] ([Id], [JobTemplateId], [Text]) VALUES (23, 6, N'Javascript')
INSERT [Job].[JobTemplateMustHaveQualification] ([Id], [JobTemplateId], [Text]) VALUES (24, 6, N'Jquery')
INSERT [Job].[JobTemplateMustHaveQualification] ([Id], [JobTemplateId], [Text]) VALUES (25, 6, N'OOP')
INSERT [Job].[JobTemplateMustHaveQualification] ([Id], [JobTemplateId], [Text]) VALUES (26, 6, N'SQL')
INSERT [Job].[JobTemplateMustHaveQualification] ([Id], [JobTemplateId], [Text]) VALUES (27, 6, N'ASP.Net')
INSERT [Job].[JobTemplateMustHaveQualification] ([Id], [JobTemplateId], [Text]) VALUES (28, 6, N'MVC')
INSERT [Job].[JobTemplateMustHaveQualification] ([Id], [JobTemplateId], [Text]) VALUES (29, 6, N'ASP.NET Core')
INSERT [Job].[JobTemplateMustHaveQualification] ([Id], [JobTemplateId], [Text]) VALUES (30, 6, N'C#')
INSERT [Job].[JobTemplateMustHaveQualification] ([Id], [JobTemplateId], [Text]) VALUES (31, 8, N'HTML')
INSERT [Job].[JobTemplateMustHaveQualification] ([Id], [JobTemplateId], [Text]) VALUES (32, 8, N'CSS')
INSERT [Job].[JobTemplateMustHaveQualification] ([Id], [JobTemplateId], [Text]) VALUES (33, 8, N'Javascript')
INSERT [Job].[JobTemplateMustHaveQualification] ([Id], [JobTemplateId], [Text]) VALUES (34, 8, N'Jquery')
INSERT [Job].[JobTemplateMustHaveQualification] ([Id], [JobTemplateId], [Text]) VALUES (35, 8, N'OOP')
INSERT [Job].[JobTemplateMustHaveQualification] ([Id], [JobTemplateId], [Text]) VALUES (36, 8, N'SQL')
INSERT [Job].[JobTemplateMustHaveQualification] ([Id], [JobTemplateId], [Text]) VALUES (37, 8, N'ASP.Net')
INSERT [Job].[JobTemplateMustHaveQualification] ([Id], [JobTemplateId], [Text]) VALUES (38, 8, N'MVC')
INSERT [Job].[JobTemplateMustHaveQualification] ([Id], [JobTemplateId], [Text]) VALUES (39, 8, N'ASP.NET Core')
INSERT [Job].[JobTemplateMustHaveQualification] ([Id], [JobTemplateId], [Text]) VALUES (40, 8, N'C#')
INSERT [Job].[JobTemplateMustHaveQualification] ([Id], [JobTemplateId], [Text]) VALUES (41, 10, N'HTML')
INSERT [Job].[JobTemplateMustHaveQualification] ([Id], [JobTemplateId], [Text]) VALUES (42, 10, N'CSS')
INSERT [Job].[JobTemplateMustHaveQualification] ([Id], [JobTemplateId], [Text]) VALUES (43, 10, N'Javascript')
INSERT [Job].[JobTemplateMustHaveQualification] ([Id], [JobTemplateId], [Text]) VALUES (44, 10, N'Jquery')
INSERT [Job].[JobTemplateMustHaveQualification] ([Id], [JobTemplateId], [Text]) VALUES (45, 10, N'OOP')
INSERT [Job].[JobTemplateMustHaveQualification] ([Id], [JobTemplateId], [Text]) VALUES (46, 10, N'SQL')
INSERT [Job].[JobTemplateMustHaveQualification] ([Id], [JobTemplateId], [Text]) VALUES (47, 10, N'ASP.Net')
INSERT [Job].[JobTemplateMustHaveQualification] ([Id], [JobTemplateId], [Text]) VALUES (48, 10, N'MVC')
INSERT [Job].[JobTemplateMustHaveQualification] ([Id], [JobTemplateId], [Text]) VALUES (49, 10, N'ASP.NET Core')
INSERT [Job].[JobTemplateMustHaveQualification] ([Id], [JobTemplateId], [Text]) VALUES (50, 10, N'C#')
INSERT [Job].[JobTemplateMustHaveQualification] ([Id], [JobTemplateId], [Text]) VALUES (51, 12, N'HTML')
INSERT [Job].[JobTemplateMustHaveQualification] ([Id], [JobTemplateId], [Text]) VALUES (52, 12, N'CSS')
INSERT [Job].[JobTemplateMustHaveQualification] ([Id], [JobTemplateId], [Text]) VALUES (53, 12, N'Javascript')
INSERT [Job].[JobTemplateMustHaveQualification] ([Id], [JobTemplateId], [Text]) VALUES (54, 12, N'Jquery')
INSERT [Job].[JobTemplateMustHaveQualification] ([Id], [JobTemplateId], [Text]) VALUES (55, 12, N'OOP')
INSERT [Job].[JobTemplateMustHaveQualification] ([Id], [JobTemplateId], [Text]) VALUES (56, 12, N'SQL')
INSERT [Job].[JobTemplateMustHaveQualification] ([Id], [JobTemplateId], [Text]) VALUES (57, 12, N'ASP.Net')
INSERT [Job].[JobTemplateMustHaveQualification] ([Id], [JobTemplateId], [Text]) VALUES (58, 12, N'MVC')
INSERT [Job].[JobTemplateMustHaveQualification] ([Id], [JobTemplateId], [Text]) VALUES (59, 12, N'ASP.NET Core')
INSERT [Job].[JobTemplateMustHaveQualification] ([Id], [JobTemplateId], [Text]) VALUES (60, 12, N'C#')
INSERT [Job].[JobTemplateMustHaveQualification] ([Id], [JobTemplateId], [Text]) VALUES (61, 14, N'HTML')
INSERT [Job].[JobTemplateMustHaveQualification] ([Id], [JobTemplateId], [Text]) VALUES (62, 14, N'CSS')
INSERT [Job].[JobTemplateMustHaveQualification] ([Id], [JobTemplateId], [Text]) VALUES (63, 14, N'Javascript')
INSERT [Job].[JobTemplateMustHaveQualification] ([Id], [JobTemplateId], [Text]) VALUES (64, 14, N'Jquery')
INSERT [Job].[JobTemplateMustHaveQualification] ([Id], [JobTemplateId], [Text]) VALUES (65, 14, N'OOP')
INSERT [Job].[JobTemplateMustHaveQualification] ([Id], [JobTemplateId], [Text]) VALUES (66, 14, N'SQL')
INSERT [Job].[JobTemplateMustHaveQualification] ([Id], [JobTemplateId], [Text]) VALUES (67, 14, N'ASP.Net')
INSERT [Job].[JobTemplateMustHaveQualification] ([Id], [JobTemplateId], [Text]) VALUES (68, 14, N'MVC')
INSERT [Job].[JobTemplateMustHaveQualification] ([Id], [JobTemplateId], [Text]) VALUES (69, 14, N'ASP.NET Core')
INSERT [Job].[JobTemplateMustHaveQualification] ([Id], [JobTemplateId], [Text]) VALUES (70, 14, N'C#')
INSERT [Job].[JobTemplateMustHaveQualification] ([Id], [JobTemplateId], [Text]) VALUES (71, 16, N'HTML')
INSERT [Job].[JobTemplateMustHaveQualification] ([Id], [JobTemplateId], [Text]) VALUES (72, 16, N'CSS')
INSERT [Job].[JobTemplateMustHaveQualification] ([Id], [JobTemplateId], [Text]) VALUES (73, 16, N'Javascript')
INSERT [Job].[JobTemplateMustHaveQualification] ([Id], [JobTemplateId], [Text]) VALUES (74, 16, N'Jquery')
INSERT [Job].[JobTemplateMustHaveQualification] ([Id], [JobTemplateId], [Text]) VALUES (75, 16, N'OOP')
INSERT [Job].[JobTemplateMustHaveQualification] ([Id], [JobTemplateId], [Text]) VALUES (76, 16, N'SQL')
INSERT [Job].[JobTemplateMustHaveQualification] ([Id], [JobTemplateId], [Text]) VALUES (77, 16, N'ASP.Net')
INSERT [Job].[JobTemplateMustHaveQualification] ([Id], [JobTemplateId], [Text]) VALUES (78, 16, N'MVC')
INSERT [Job].[JobTemplateMustHaveQualification] ([Id], [JobTemplateId], [Text]) VALUES (79, 16, N'ASP.NET Core')
INSERT [Job].[JobTemplateMustHaveQualification] ([Id], [JobTemplateId], [Text]) VALUES (80, 16, N'C#')
SET IDENTITY_INSERT [Job].[JobTemplateMustHaveQualification] OFF
SET IDENTITY_INSERT [Job].[JobTemplateNiceHaveQualification] ON 

INSERT [Job].[JobTemplateNiceHaveQualification] ([Id], [JobTemplateId], [Text]) VALUES (1, 2, N'React')
INSERT [Job].[JobTemplateNiceHaveQualification] ([Id], [JobTemplateId], [Text]) VALUES (2, 2, N'Angular')
INSERT [Job].[JobTemplateNiceHaveQualification] ([Id], [JobTemplateId], [Text]) VALUES (3, 2, N'DevExpress')
INSERT [Job].[JobTemplateNiceHaveQualification] ([Id], [JobTemplateId], [Text]) VALUES (4, 2, N'Telerik')
INSERT [Job].[JobTemplateNiceHaveQualification] ([Id], [JobTemplateId], [Text]) VALUES (5, 2, N'Node Js')
INSERT [Job].[JobTemplateNiceHaveQualification] ([Id], [JobTemplateId], [Text]) VALUES (6, 2, N'Typescript')
INSERT [Job].[JobTemplateNiceHaveQualification] ([Id], [JobTemplateId], [Text]) VALUES (7, 2, N'Material UI')
INSERT [Job].[JobTemplateNiceHaveQualification] ([Id], [JobTemplateId], [Text]) VALUES (8, 4, N'React')
INSERT [Job].[JobTemplateNiceHaveQualification] ([Id], [JobTemplateId], [Text]) VALUES (9, 4, N'Angular')
INSERT [Job].[JobTemplateNiceHaveQualification] ([Id], [JobTemplateId], [Text]) VALUES (10, 4, N'DevExpress')
INSERT [Job].[JobTemplateNiceHaveQualification] ([Id], [JobTemplateId], [Text]) VALUES (11, 4, N'Telerik')
INSERT [Job].[JobTemplateNiceHaveQualification] ([Id], [JobTemplateId], [Text]) VALUES (12, 4, N'Node Js')
INSERT [Job].[JobTemplateNiceHaveQualification] ([Id], [JobTemplateId], [Text]) VALUES (13, 4, N'Typescript')
INSERT [Job].[JobTemplateNiceHaveQualification] ([Id], [JobTemplateId], [Text]) VALUES (14, 4, N'Material UI')
INSERT [Job].[JobTemplateNiceHaveQualification] ([Id], [JobTemplateId], [Text]) VALUES (15, 6, N'React')
INSERT [Job].[JobTemplateNiceHaveQualification] ([Id], [JobTemplateId], [Text]) VALUES (16, 6, N'Angular')
INSERT [Job].[JobTemplateNiceHaveQualification] ([Id], [JobTemplateId], [Text]) VALUES (17, 6, N'DevExpress')
INSERT [Job].[JobTemplateNiceHaveQualification] ([Id], [JobTemplateId], [Text]) VALUES (18, 6, N'Telerik')
INSERT [Job].[JobTemplateNiceHaveQualification] ([Id], [JobTemplateId], [Text]) VALUES (19, 6, N'Node Js')
INSERT [Job].[JobTemplateNiceHaveQualification] ([Id], [JobTemplateId], [Text]) VALUES (20, 6, N'Typescript')
INSERT [Job].[JobTemplateNiceHaveQualification] ([Id], [JobTemplateId], [Text]) VALUES (21, 6, N'Material UI')
INSERT [Job].[JobTemplateNiceHaveQualification] ([Id], [JobTemplateId], [Text]) VALUES (22, 8, N'React')
INSERT [Job].[JobTemplateNiceHaveQualification] ([Id], [JobTemplateId], [Text]) VALUES (23, 8, N'Angular')
INSERT [Job].[JobTemplateNiceHaveQualification] ([Id], [JobTemplateId], [Text]) VALUES (24, 8, N'DevExpress')
INSERT [Job].[JobTemplateNiceHaveQualification] ([Id], [JobTemplateId], [Text]) VALUES (25, 8, N'Telerik')
INSERT [Job].[JobTemplateNiceHaveQualification] ([Id], [JobTemplateId], [Text]) VALUES (26, 8, N'Node Js')
INSERT [Job].[JobTemplateNiceHaveQualification] ([Id], [JobTemplateId], [Text]) VALUES (27, 8, N'Typescript')
INSERT [Job].[JobTemplateNiceHaveQualification] ([Id], [JobTemplateId], [Text]) VALUES (28, 8, N'Material UI')
INSERT [Job].[JobTemplateNiceHaveQualification] ([Id], [JobTemplateId], [Text]) VALUES (29, 10, N'React')
INSERT [Job].[JobTemplateNiceHaveQualification] ([Id], [JobTemplateId], [Text]) VALUES (30, 10, N'Angular')
INSERT [Job].[JobTemplateNiceHaveQualification] ([Id], [JobTemplateId], [Text]) VALUES (31, 10, N'DevExpress')
INSERT [Job].[JobTemplateNiceHaveQualification] ([Id], [JobTemplateId], [Text]) VALUES (32, 10, N'Telerik')
INSERT [Job].[JobTemplateNiceHaveQualification] ([Id], [JobTemplateId], [Text]) VALUES (33, 10, N'Node Js')
INSERT [Job].[JobTemplateNiceHaveQualification] ([Id], [JobTemplateId], [Text]) VALUES (34, 10, N'Typescript')
INSERT [Job].[JobTemplateNiceHaveQualification] ([Id], [JobTemplateId], [Text]) VALUES (35, 10, N'Material UI')
INSERT [Job].[JobTemplateNiceHaveQualification] ([Id], [JobTemplateId], [Text]) VALUES (36, 12, N'React')
INSERT [Job].[JobTemplateNiceHaveQualification] ([Id], [JobTemplateId], [Text]) VALUES (37, 12, N'Angular')
INSERT [Job].[JobTemplateNiceHaveQualification] ([Id], [JobTemplateId], [Text]) VALUES (38, 12, N'DevExpress')
INSERT [Job].[JobTemplateNiceHaveQualification] ([Id], [JobTemplateId], [Text]) VALUES (39, 12, N'Telerik')
INSERT [Job].[JobTemplateNiceHaveQualification] ([Id], [JobTemplateId], [Text]) VALUES (40, 12, N'Node Js')
INSERT [Job].[JobTemplateNiceHaveQualification] ([Id], [JobTemplateId], [Text]) VALUES (41, 12, N'Typescript')
INSERT [Job].[JobTemplateNiceHaveQualification] ([Id], [JobTemplateId], [Text]) VALUES (42, 12, N'Material UI')
INSERT [Job].[JobTemplateNiceHaveQualification] ([Id], [JobTemplateId], [Text]) VALUES (43, 14, N'React')
INSERT [Job].[JobTemplateNiceHaveQualification] ([Id], [JobTemplateId], [Text]) VALUES (44, 14, N'Angular')
INSERT [Job].[JobTemplateNiceHaveQualification] ([Id], [JobTemplateId], [Text]) VALUES (45, 14, N'DevExpress')
INSERT [Job].[JobTemplateNiceHaveQualification] ([Id], [JobTemplateId], [Text]) VALUES (46, 14, N'Telerik')
INSERT [Job].[JobTemplateNiceHaveQualification] ([Id], [JobTemplateId], [Text]) VALUES (47, 14, N'Node Js')
INSERT [Job].[JobTemplateNiceHaveQualification] ([Id], [JobTemplateId], [Text]) VALUES (48, 14, N'Typescript')
INSERT [Job].[JobTemplateNiceHaveQualification] ([Id], [JobTemplateId], [Text]) VALUES (49, 14, N'Material UI')
INSERT [Job].[JobTemplateNiceHaveQualification] ([Id], [JobTemplateId], [Text]) VALUES (50, 16, N'React')
INSERT [Job].[JobTemplateNiceHaveQualification] ([Id], [JobTemplateId], [Text]) VALUES (51, 16, N'Angular')
INSERT [Job].[JobTemplateNiceHaveQualification] ([Id], [JobTemplateId], [Text]) VALUES (52, 16, N'DevExpress')
INSERT [Job].[JobTemplateNiceHaveQualification] ([Id], [JobTemplateId], [Text]) VALUES (53, 16, N'Telerik')
INSERT [Job].[JobTemplateNiceHaveQualification] ([Id], [JobTemplateId], [Text]) VALUES (54, 16, N'Node Js')
INSERT [Job].[JobTemplateNiceHaveQualification] ([Id], [JobTemplateId], [Text]) VALUES (55, 16, N'Typescript')
INSERT [Job].[JobTemplateNiceHaveQualification] ([Id], [JobTemplateId], [Text]) VALUES (56, 16, N'Material UI')
SET IDENTITY_INSERT [Job].[JobTemplateNiceHaveQualification] OFF
