USE [recruitment]
GO

/****** Object:  View [dbo].[VwSourcerSubmissions]    Script Date: 2021-03-01 2:03:34 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO









ALTER VIEW [dbo].[VwSourcerSubmissions]
AS
SELECT        ROW_NUMBER() OVER ( ORDER BY Job.Job.Id ) AS 'rowNumber' ,
              Job.Job.Id AS jobId, Job.Job.UUID as uuId, Job.Job.JobTitle AS jobTitle, 
			  Job.Job.IsActive as isActive, 
			  Job.JobLocationTranslation.languageId, Job.JobLocationTranslation.Translation
              AS jobLocationTranslation, Sourcer.JobCandidateSubmission.SourcerId as sourcerId,
			  CV_Name as cvName , EmployerId as employerId , Common.Users.UUID as employerUUId,
			  candidateName , rejectionReason , JobCandidateSubmission.CandidateSubmissionStatusId , CandidateSubmissionStatusTranslation.Translation as candidateStatusName
FROM          Job.Job INNER JOIN
              Job.JobStatus ON Job.JobStatus.Id = Job.Job.JobStatusId  INNER JOIN
              Sourcer.JobCandidateSubmission ON Sourcer.JobCandidateSubmission.JobId = Job.Job.Id INNER JOIN
              Job.JobLocation ON Job.JobLocation.Id = Job.Job.JobLocationId INNER JOIN
              Job.JobLocationTranslation ON Job.JobLocation.Id = Job.JobLocationTranslation.JobLocationId
			  INNER JOIN Common.Users ON Common.Users.Id = Job.Job.EmployerId 
			  Inner Join Employer.CandidateSubmissionStatusTranslation ON 
			  CandidateSubmissionStatusTranslation.CandidateSubmissionStatusId = JobCandidateSubmission.CandidateSubmissionStatusId
			  and CandidateSubmissionStatusTranslation.languageId = JobLocationTranslation.languageId
GO


