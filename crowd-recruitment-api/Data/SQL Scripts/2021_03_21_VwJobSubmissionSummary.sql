USE [recruitment]
GO

/****** Object:  View [dbo].[VwJobSubmissionsSummary]    Script Date: 21-Mar-21 12:57:42 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/*GROUP BY Job.Job.Id, Job.Job.UUID, Job.Job.JobStatusId, Job.Job.IsActive, Job.JobLocationTranslation.languageId, Sourcer.JobCandidateSubmission.SourcerId, EmployerId, Common.Users.UUID*/
ALTER VIEW [dbo].[VwJobSubmissionsSummary]
AS
SELECT DISTINCT Job.Job.Id AS jobId, Job.Job.UUID, Job.Job.JobStatusId, Job.Job.JobTitle, Job.Job.IsActive,
                             (SELECT        ISNULL(Job.Job.noOfSubmissions, 0) - COUNT(*) AS submissionCount
                               FROM            Sourcer.JobCandidateSubmission
                               WHERE        (JobId = Job.Job.Id) AND (SourcerId = JCS.SourcerId)) AS remainingCount, Job.JobLocationTranslation.languageId, Job.JobLocationTranslation.Translation AS jobLocationTranslation, JCS.SourcerId, 
                         Job.Job.EmployerId, Common.Users.UUID AS employerUUId, CONVERT(nvarchar, Common.Users.CreatedDate, 107) AS jobDate, Job.Job.JobDescription AS description, 
                         Employer.CompanyIndustryTranslation.Translation AS companyIndustry, Common.Users.FirstName AS employerFName, Common.Users.LastName AS employerLName, Job.Job.reason, Job.Job.commission, 
                         Job.JobStatusTranslation.Translation AS jobStatusName,
                             (SELECT        ISNULL(COUNT(*), 0) AS submissionCount
                               FROM            Sourcer.JobCandidateSubmission AS JobCandidateSubmission_1
                               WHERE        (JobId = Job.Job.Id) AND (SourcerId = Common.Users.Id)) AS submissionCount, Job.Job.noOfSubmissions, Job.Job.JobDate AS createdDate
FROM            Job.Job INNER JOIN
                         Job.JobStatus ON Job.JobStatus.Id = Job.Job.JobStatusId LEFT OUTER JOIN
                         Sourcer.JobCandidateSubmission AS JCS ON JCS.JobId = Job.Job.Id INNER JOIN
                         Job.JobLocation ON Job.JobLocation.Id = Job.Job.JobLocationId INNER JOIN
                         Job.JobLocationTranslation ON Job.JobLocation.Id = Job.JobLocationTranslation.JobLocationId INNER JOIN
                         Job.JobStatusTranslation ON Job.JobStatusTranslation.JobStatusId = Job.JobStatus.Id AND Job.JobStatusTranslation.languageId = Job.JobLocationTranslation.languageId INNER JOIN
                         Common.Users ON Common.Users.Id = Job.Job.EmployerId INNER JOIN
                         Employer.CompanyIndustry ON Job.Job.industryId = Employer.CompanyIndustry.Id INNER JOIN
                         Employer.CompanyIndustryTranslation ON Employer.CompanyIndustryTranslation.CompanyIndustryId = Employer.CompanyIndustry.Id AND 
                         Employer.CompanyIndustryTranslation.languageId = Job.JobLocationTranslation.languageId


						 Order By Job.Job.Id DESC OFFSET 0 ROWS
