USE [recruitment]
GO

/****** Object:  View [dbo].[VwCandidatesInterviews]    Script Date: 13-Apr-21 2:25:08 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

ALTER VIEW [dbo].[VwCandidatesInterviews]
AS
SELECT        Employer.CandidatesInterviews.id, Sourcer.JobCandidateSubmission.candidateName, Sourcer.JobCandidateSubmission.email AS candidateEmail, Employer.CandidatesInterviews.interviewDate, 
                         Employer.CandidatesInterviews.duration, Employer.EmployerDetails.CompanyName, Common.Interviewers.InterviewerName, Employer.CandidatesInterviews.emailSubject, 
                         Employer.CandidatesInterviews.emailDescription, Sourcer.JobCandidateSubmission.JobId, Job.Job.Id AS Expr1, Job.Job.JobTitle, Employer.CandidatesInterviews.isCanceled, 
                         Employer.CandidatesInterviews.isEmailSent, Employer.CandidatesInterviews.interviewTime, Sourcer.JobCandidateSubmission.UUID AS submissionUuid
FROM            Employer.CandidatesInterviews INNER JOIN
                         Sourcer.JobCandidateSubmission ON Employer.CandidatesInterviews.candidateId = Sourcer.JobCandidateSubmission.Id INNER JOIN
                         Common.Users ON Common.Users.Id = Employer.CandidatesInterviews.createdBy INNER JOIN
                         Employer.EmployerDetails ON Common.Users.Id = Employer.EmployerDetails.UserId INNER JOIN
                         Common.Interviewers ON Employer.CandidatesInterviews.interviewerId = Common.Interviewers.Id INNER JOIN
                         Job.Job ON Sourcer.JobCandidateSubmission.JobId = Job.Job.Id
GO


