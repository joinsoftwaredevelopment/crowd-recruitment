﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services.Common.Jwt.Claims
{
    public class BaseClaim
    {
        public BaseClaim()
        {
            OtherClaims = new Dictionary<string, object>();
        }
        public Dictionary<string, object> OtherClaims { get; set; }
    }
}
