﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services.Common.Jwt.Claims
{
    public class UserClaim : BaseClaim
    {
        public string UuId { get; set; }
        public int UserRole { get; set; }
        public string UserName { get; set; }
        public string UserEmail { get; set; }
        public int Language { get; set; }
        public string ImageName { get; set; }
        public string PhoneNumer { get; set; }
        public int NotificationsCount { get; set; }
    }
}
