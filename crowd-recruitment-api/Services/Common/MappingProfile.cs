﻿using AutoMapper;
using Data.Tables;
using Data.Views;
using Services.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace Services.Common
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            // Add as many of these lines as you need to map your objects
            CreateMap<SourcerViewModel, User>();
            CreateMap<InterviewDetailsViewModel, User>();

            CreateMap<SourcerDetail, SourcerExperienceViewModel>()
            //.ForMember(q => q.ExperienceTypeId, option => option.MapFrom(q => q.ExperienceTypeId))
            .ForPath(dest => dest.Uuid, opts => opts.MapFrom(src => src.User.Uuid));

            CreateMap<SourcerDetail, SourcerJobViewModel>()
            //.ForMember(q => q.JobTitle, option => option.MapFrom(q => q.JobTitle))
            .ForPath(dest => dest.Uuid, opts => opts.MapFrom(src => src.User.Uuid));

            CreateMap<SourcerDetail, SourcerIndustryViewModel>()
            //.ForMember(q => q.IndustryId, option => option.MapFrom(q => q.IndustryId))
            .ForPath(dest => dest.Uuid, opts => opts.MapFrom(src => src.User.Uuid));

            CreateMap<SourcerDetail, SourcerCVViewModel>()
            .ForPath(dest => dest.Uuid, opts => opts.MapFrom(src => src.User.Uuid));


            CreateMap<LogInViewModel, User>();
            CreateMap<PostJobDetails, Job>();
            CreateMap<PostJobInstructions, Job>();
            CreateMap<PostJobEngagment, Job>();
            CreateMap<EmployerDetailsViewModel, EmployerDetails>()
            .ForMember(dest => dest.User,
                         input => input.MapFrom(i => new User
                         {
                             FirstName = i.FirstName,
                             LastName = i.LastName,
                             PhoneNumber = i.PhoneNumber,
                             Email = i.Email,
                             Password = i.Password
                         }))
            .ForMember(dest => dest.EmployerDetail,
                         input => input.MapFrom(i => new EmployerDetail { CompanyName = i.CompanyName }));
           

        }
    }
}
