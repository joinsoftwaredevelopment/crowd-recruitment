﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Services.Interfaces
{
    public interface ICipherService
    {
        string Encrypt(string input);
        string Decrypt(string cipherText);
    }
}
