﻿using Data.Tables;
using Data.Views;
using Services.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace Services.Interfaces
{
    public interface ICandidateService : IBaseService<VwJobCandidate, JobCandidateViewModel>
    {
        CurrentResponse GetJobCandidates(int languageId , string jobUuid);
        CurrentResponse GetAcceptedJobCandidates(int languageId, string jobUuid);
        CurrentResponse GetJobCandidateByUuId(int languageId, string candidateUuid);
        CurrentResponse GetSourcerCandidateBySourcerUuId(int languageId, string sourcerUuId , string jobUuId);
        CurrentResponse AcceptCandidate(string uuid);
        CurrentResponse DeclineCandidate(UserRequestViewModel userRequestViewModel);
        CurrentResponse EmployerAcceptCandidate(string uuid);
        CurrentResponse UpdateHiringStage(UpdateHiringStageViewModel updateHiringStageViewModel);
        CurrentResponse UpdateIsAcceptOffer(UpdateIsAcceptOfferViewModel updateIsAcceptOfferViewModel);
        CurrentResponse UpdateIsHired(UpdateIsHiredViewModel updateIsHiredViewModel);
        CurrentResponse UpdateCandidateCvStatus(UpdateCandidateCvStatusViewModel updateCandidateCvStatusViewModel);

        CurrentResponse EmployerDeclineCandidate(UserRequestViewModel userRequestViewModel);
        CurrentResponse GetSourcerCandidateBySubmissionStatusId(int languageId, string sourcerUuId, int submissionStatusId, string jobUuId);
        CurrentResponse GetCandidateStatusHistoryByUuId(int languageId, string candidateUuid);

    }
}
