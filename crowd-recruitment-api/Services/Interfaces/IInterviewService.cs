﻿using System;
using System.Collections.Generic;
using System.Text;
using Data.Tables;
using Services.ViewModels;

namespace Services.Interfaces
{
    public interface IInterviewService : IBaseService<InterviewDetailsViewModel, InterviewDetailsViewModel> 
    {
        #region Methods
        CurrentResponse AddInterviewDetails(InterviewDetailsViewModel interviewDetailsViewModel);
        CurrentResponse SendOffer(OfferViewModel offerViewModel);
        CurrentResponse AddInterviewEmails(InterviewEmailViewModel interviewEmailViewModel);
        CurrentResponse CancelInterview(int interviewId, bool isCanceled);
        CurrentResponse GetAllInterviewers(int languageId);
        CurrentResponse GetInterviewDetailsById(int interviewId);
        CurrentResponse GetInterviewEmailsById(int interviewId);
        CurrentResponse GetAllInterviews();
        CurrentResponse GetAllInterviewsByUuid(string uuid);
        CurrentResponse GetInterviewTypes(int languageId);
        CurrentResponse GetDurationTimes(int languageId);
        CurrentResponse GetInterviewTemplates();
        CurrentResponse GetOfferTemplates();
        CurrentResponse GetInterviewTempById(int templateId);
        #endregion
    }
}
