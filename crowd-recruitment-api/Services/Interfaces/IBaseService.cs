﻿using Services.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;

namespace Services.Interfaces
{
    public interface IBaseService<TEntity,TModel> where  TEntity : class where TModel : class
    {
        //int LogException(Exception exception);

        #region Methods

        int LogException(Exception exception);

        CurrentResponse CreateResponse(object data, int statusCode, string message);

        #endregion
    }
}
