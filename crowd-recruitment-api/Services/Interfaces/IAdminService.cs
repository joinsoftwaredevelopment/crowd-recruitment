﻿using Services.ViewModels;
using System;

namespace Services.Interfaces
{
    public interface IAdminService : IBaseService<LogInViewModel, LogInViewModel>
    {
        CurrentResponse LogIn(LogInViewModel logInViewModel);
        CurrentResponse GetVwAdminSourcersBalance();
        CurrentResponse GetJobSourcers(string jobUuId);
        CurrentResponse GetVwAdminSourcersBalanceByWeek(DateTime startDate, DateTime endDate);
        CurrentResponse UpdateAdminSourcersBalance(SourcerPaymentModel sourcerPayment);
        void UpdateEmployerPackages();
    }
}
