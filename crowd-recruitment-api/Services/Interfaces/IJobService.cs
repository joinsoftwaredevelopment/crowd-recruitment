﻿using Data.Tables;
using Data.Views;
using Services.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace Services.Interfaces
{
    public interface IJobService : IBaseService<User, JobDetailsInfo>
    {
        CurrentResponse GetJobDetailsInfo(string uuid, int? tempId);
        CurrentResponse GetJobInstructionsInfo(string uuid);
        CurrentResponse GetJobInstructionsInfo();
        CurrentResponse GetJobEngagementsInfo(string uuid);

        CurrentResponse GetJobByUuid(string uuid);
        CurrentResponse CreateJob(PostJobDetails postJobViewModel);
        CurrentResponse UpdateJob(PostJobDetails postJobViewModel);
        CurrentResponse UpdateJobInstructions(PostJobInstructions postJobViewModel);
        CurrentResponse UpdateJobObjectives(PostJobObjectives postJobViewModel);
        CurrentResponse UpdateJobEngagement(PostJobEngagment postJobViewModel);
        CurrentResponse GetJobSummaries(int languageId,string uuId);
        CurrentResponse GetJobSummariesByEmployer(int languageId,string uuId);
        CurrentResponse GetAllJobs(int languageId,string uuId);
        CurrentResponse GetAllJobs(int statusId);
        CurrentResponse GetJobDraftsByEmployer(int languageId,string uuId);
        CurrentResponse GetJobClosedByEmployer(int languageId, string employeruuid);
        CurrentResponse CloseJob(CloseJob closeJob);
        CurrentResponse DeleteJob(string uuid);
        CurrentResponse GetSourcerJobSummaries(int languageId, string uuId, bool isActive);
        CurrentResponse GetSourcerJobSubmissions(int languageId, string sourcerUuId, string jobUuId);
        CurrentResponse AddSourcerSubmission(SourcerCVViewModel jobCandidateSubmission);
        CurrentResponse GetVwJobByUuId(string uuId, int languageId);
        CurrentResponse AcceptJob(string uuid, int numberOfSubmissions, decimal commission);
        CurrentResponse DeclineJob(string uuid, string rejectionReason);
        CurrentResponse GetAllIndustries();
        CurrentResponse GetTempByIndustryId(int industryId);
        CurrentResponse GetJobTempById(int templateId);
        CurrentResponse GetAllJobTemps();

        bool IsEmployerMembershipActive(string jobUuid);

    }
}
