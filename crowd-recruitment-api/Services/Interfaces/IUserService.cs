﻿using Data.Views;
using Services.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace Services.Interfaces
{
    public interface IUserService : IBaseService<LogInViewModel, LogInViewModel>
    {
        CurrentResponse LogIn(LogInViewModel logInViewModel);
        CurrentResponse GetUserByUuId(string uuId);
        CurrentResponse BookMeeting(BookMeeting bookMeeting);
        CurrentResponse GetAdminInfo();
        BookMeetingViewModel GetMeetingInfo(string uuId);
        BasicModel GetTimePeriod(int timePeriodId);
        CurrentResponse GetAllCountries(int languageId);
        CurrentResponse GetAllCurrencies(int languageId);
        CurrentResponse ResePassword(ResetPasswordViewModel resetPasswordViewModel);
        CurrentResponse SendResetEmail(string email);
        CurrentResponse UpdatePassword(UpdatePasswordViewModel updatePasswordViewModel);
        CurrentResponse VerifyUserEmailByUuId(string uuId);

        CurrentResponse ResendEmail(string uuId, int languageId);

        CurrentResponse ChangeEmail(string uuId, string Email, int languageId);

        bool IsEmployerMembershipActive(int employeerId);
    }
}
