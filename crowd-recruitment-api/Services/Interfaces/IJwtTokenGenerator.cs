﻿using Services.Common.Jwt.Claims;
using System;
using System.Collections.Generic;
using System.Text;

namespace Services.Interfaces
{
    public interface IJwtTokenGenerator
    {
        //string GenerateToken(UserClaim userClaim);
        UserClaim ValidateToken(string jwtToken);
        string generateJwtToken(UserClaim userClaim);
    }
}
