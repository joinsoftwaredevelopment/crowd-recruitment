﻿using System;
using System.Collections.Generic;
using System.Text;
using Data.Tables;
using Services.ViewModels;

namespace Services.Interfaces
{
    public interface ISourcerProfileService : IBaseService<SourcerDetail, SourcerDetail> 
    {
        #region Methods
        CurrentResponse UpdateBasicInfo(SourcerBasicViewModel sourcerBasicViewModel);
        CurrentResponse UpdateCareerInfo(UpdateCareerInfoViewModel updateCareerInfoViewModel);

        CurrentResponse GetBasicInfo(string uuid);
        #endregion
    }
}
