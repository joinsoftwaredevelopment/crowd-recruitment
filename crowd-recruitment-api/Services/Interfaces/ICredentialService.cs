﻿using Data.Tables;
using Services.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace Services.Interfaces
{
    public interface ICredentialService //: IBaseService<Credential>
    {
        #region Methods
        JwtApiContract GetJwtCredentials();
        #endregion

    }

}
