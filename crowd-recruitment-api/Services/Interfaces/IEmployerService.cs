﻿using System;
using System.Collections.Generic;
using System.Text;
using Data.Tables;
using Services.ViewModels;

namespace Services.Interfaces
{
    public interface IEmployerService : IBaseService<EmployerDetailsViewModel, EmployerDetailsViewModel> 
    {
        #region Methods
        CurrentResponse AddEmployer(EmployerDetailsViewModel employerDetailsViewModel);
        CurrentResponse GetAllEmployers(int userStatusId);
        CurrentResponse AcceptEmployer(string Uuid);
        CurrentResponse DeclineEmployer(UserRequestViewModel userRequestViewModel);
        #endregion
    }
}
