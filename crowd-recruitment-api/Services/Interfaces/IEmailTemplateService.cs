﻿using Data.Tables;
using Microsoft.AspNetCore.Http;
using Services.ViewModels;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;

namespace Services.Interfaces
{
    public interface IEmailTemplateService : IBaseService<EmailsTemplate, EmailsTemplate>
    {
        bool SendCRVerificationEmail(int languageId, string userName, string mailReceiver, string webAppLink, string uuId);
        bool SendCRSourcerAcceptanceEmail(int languageId, string userName, string mailReceiver, string webAppLink);
        bool SendCREmployerAcceptanceEmail(int languageId, string userName, string mailReceiver, string webAppLink);
        bool SendCRZoomEmail(int languageId, string userName, string mailReceiver, string webAppLink , string zoomLink , string interviewer , string meetingDate , string meetingTime);
        bool SendCRNewSourcerEmail(int languageId, string userName, string mailReceiver, string webAppLink);
        bool SendCRNewEmployerEmail(int languageId, string userName, string mailReceiver, string webAppLink);
        List<EmailsTemplate> GetJobEmailsTemplate();
        bool SendCRRejectionEmail(int languageId, string userName , string reason, string mailReceiver, string webAppLink);
        CurrentResponse SendJobEmail(int languageId , SendCandidateMailViewModel sendCandidateMailViewModel);
        bool SendInterviewEmail(string candidateName, string companyName,
            DateTime interviewDate, TimeSpan time , string interviewDuration, string interviewerName,
            int languageId, string mailReceiver, string Description, string title,string jobTitle);
        bool SendOfferEmail(string offerSubject,string offerDescription, IFormFile offerFile,
                    string CandidateName,string Email,string JobTitle,int languageId);
        bool SendResetTemplate(string email, string resetLink, int languageId);
    }
}
