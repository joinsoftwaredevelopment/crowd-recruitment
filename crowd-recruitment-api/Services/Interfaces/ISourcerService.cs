﻿using Data.Tables;
using Services.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace Services.Interfaces
{
    public interface ISourcerService : IBaseService<SourcerDetail,SourcerViewModel>
    {
        SourcerViewModel GetSourcerByUuId(string uuid);
        CurrentResponse CreateSourcer(SourcerViewModel entity);
        CurrentResponse GetSourcerExperienceId(string uuid);
        CurrentResponse UpdateSourcerExperience(SourcerExperienceViewModel model);
        CurrentResponse GetJobtitleName(string uuid);
        CurrentResponse UpdateSourcerJobtitle(SourcerJobViewModel model);
        CurrentResponse GetSourcerhasAccessToWebsite(string uuid);
        CurrentResponse UpdateSourcerWebsites(SourcerWebsiteViewModel model);
        CurrentResponse GetSourcerIndustryId(string uuid);
        CurrentResponse UpdateSourcerIndustry(SourcerIndustryViewModel model);
        CurrentResponse UpdateSourcerCV(SourcerCVViewModel model);

        // Sourcer acceptance
        CurrentResponse GetAllSourcers(int userStatusId);
        CurrentResponse GetAllSourcerInfo(string uuid);
        CurrentResponse AcceptSourcer(string uuid);
        CurrentResponse DeclineSourcer(UserRequestViewModel userRequestViewModel);
        CurrentResponse GetSourcerBalances(string sourcerUuid);
        //CurrentResponse GetSourcerBalancesSummary(string sourcerUuid);

    }
}
