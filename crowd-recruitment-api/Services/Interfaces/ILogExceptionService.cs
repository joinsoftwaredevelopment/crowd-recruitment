﻿using Data.Tables;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Services.Interfaces
{
    public interface ILogExceptionService : IBaseService<LogException, LogException>
    {
    }
}
