﻿using Services.ViewModels;

namespace Services.Interfaces
{
    public interface IEmployerPackageHistoryService : IBaseService<EmployerPackageHistoryViewModel, EmployerPackageHistoryViewModel> 
    {
        #region Methods
        CurrentResponse GetAllPackagesHistory(string uuId);
        #endregion
    }
}
