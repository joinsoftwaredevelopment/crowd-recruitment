﻿using Services.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace Services.Interfaces
{
    public interface IPackagesService : IBaseService<AddPackageViewModel, AddPackageViewModel>
    {
        CurrentResponse AddPackage(AddPackageViewModel addPackageViewModel);
        CurrentResponse GetPackageById(int packageId);
        CurrentResponse EditPackage(AddPackageViewModel addPackageViewModel);
        CurrentResponse DeletePackage(int packageId);
        CurrentResponse GetAllPackages();
    }
}
