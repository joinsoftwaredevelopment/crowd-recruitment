﻿using System;
using System.Collections.Generic;
using System.Text;
using Data.Tables;
using Services.ViewModels;

namespace Services.Interfaces
{
    public interface IEmployerProfileService : IBaseService<EmployerDetailsViewModel, EmployerDetailsViewModel> 
    {
        #region Methods
        CurrentResponse UpdateEmployerDetails(UpdateEmployerDetailsViewModel updateEmployerDetailsViewModel);
        CurrentResponse GetEmployerDetails(string uuid);
        CurrentResponse UpdatePackage(UpdatePackageViewModel updatePackageViewModel);
        #endregion
    }
}
