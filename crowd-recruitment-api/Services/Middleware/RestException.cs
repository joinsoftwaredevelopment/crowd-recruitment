﻿using Services.ViewModels;
using System;
using System.Collections.Generic;
using System.Net;
using System.Text;

namespace Services.Middleware
{
    public class RestException : Exception
    {
        public RestException(int code, object errors = null)
        {
            Code = code;
            Errors = errors;
        }

        public RestException(CurrentResponse response)
        {
            Code = response.status;
            Errors = response.message;
        }

        public int Code { get; }
        public object Errors { get; }
    }

}
