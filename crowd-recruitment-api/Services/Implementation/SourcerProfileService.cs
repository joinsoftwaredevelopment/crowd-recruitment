﻿using System;
using System.Collections.Generic;
using System.Text;
using AutoMapper;
using Data.Tables;
using Data.Views;
using Repositories.Interfaces;
using Repositories.Enums;
using Services.Interfaces;
using Services.ViewModels;
using Helpers;
using System.Net;
using Microsoft.Extensions.Options;

namespace Services.Implementation
{
    public class SourcerProfileService : BaseService<SourcerDetail, SourcerViewModel>, ISourcerProfileService
    {
        #region Fields
        private readonly ISourcerProfileRepository _sourcerProfileRepository;
        private readonly IMapper _mapper;
        private readonly IUserRepository _userRepository;
        #endregion

        #region Constructors
        public SourcerProfileService(ISourcerProfileRepository sourcerProfileRepository,
            IMapper mapper, IUserRepository userRepository) : base(sourcerProfileRepository)
        {
            _sourcerProfileRepository = sourcerProfileRepository;
            _mapper = mapper;
            _userRepository = userRepository;
        }

        #endregion

        #region Methods
        public CurrentResponse UpdateBasicInfo(SourcerBasicViewModel sourcerBasicViewModel)
        {
            try
            {
                //Validation 
                if (!sourcerBasicViewModel.phoneNumber.Contains('+'))
                    sourcerBasicViewModel.phoneNumber = string.Format("+{0}", sourcerBasicViewModel.phoneNumber);
                if (string.IsNullOrEmpty(sourcerBasicViewModel.jobTitle))
                {
                    _currentResponse.message = "Job title is required";
                    _currentResponse.status = (int)HttpStatusCode.BadRequest;
                    return _currentResponse;
                }

                if (string.IsNullOrEmpty(sourcerBasicViewModel.firstName))
                {
                    _currentResponse.message = "First Name is required";
                    _currentResponse.status = (int)HttpStatusCode.BadRequest;
                    return _currentResponse;
                }

                if (string.IsNullOrEmpty(sourcerBasicViewModel.lastName))
                {
                    _currentResponse.message = "Last Name is required";
                    _currentResponse.status = (int)HttpStatusCode.BadRequest;
                    return _currentResponse;
                }

                sourcerBasicViewModel.email = sourcerBasicViewModel.email.Trim();
                var isEmailValid = GeneralFunctions.IsEmailValid(sourcerBasicViewModel.email);
                if (!isEmailValid)
                {
                    _currentResponse.message = "Email address is invalid";
                    _currentResponse.status = (int)HttpStatusCode.BadRequest;
                    return _currentResponse;
                }

                var isPhoneValid = GeneralFunctions.IsPhoneNumberValid(sourcerBasicViewModel.phoneNumber);
                if (!isPhoneValid)
                {
                    _currentResponse.message = "Phone number is invalid";
                    _currentResponse.status = (int)HttpStatusCode.BadRequest;
                    return _currentResponse;
                }

           
                var currentUser = _userRepository.GetUserByUuId(sourcerBasicViewModel.uuid);

                var isPhoneExist = _userRepository.IsPhoneExistBefore(sourcerBasicViewModel.phoneNumber, null);
                if (isPhoneExist && sourcerBasicViewModel.phoneNumber != currentUser.PhoneNumber)
                {
                    CreateResponse(null, (int)HttpStatusCode.BadRequest, "Phone number is already exist, choose another one");

                    return _currentResponse;
                }

                var isEmailExist = _userRepository.IsEmailExistBefore(sourcerBasicViewModel.email, null);
                if (isEmailExist && sourcerBasicViewModel.email != currentUser.Email)
                {
                    CreateResponse(null, (int)HttpStatusCode.BadRequest, "Email is already exist, choose another one");

                    return _currentResponse;
                }


                if (_sourcerProfileRepository.
                    UpdateBasicInfo(sourcerBasicViewModel.uuid, sourcerBasicViewModel.jobTitle,
                    sourcerBasicViewModel.firstName, sourcerBasicViewModel.lastName, sourcerBasicViewModel.phoneNumber,
                    sourcerBasicViewModel.email))
                {
                    CreateResponse(null, (int)HttpStatusCode.OK, "Sourcer details has been updated successfully");
                    return _currentResponse;
                }

                CreateResponse(null, (int)HttpStatusCode.BadRequest, "Failed to update sourcer details");
                return _currentResponse;
            }
            catch (System.Exception e)
            {
                LogException(e);

                _currentResponse.message = "An error occurred while updating your data";
                _currentResponse.status = (int)HttpStatusCode.BadRequest;

                return _currentResponse;
            }
        }

        public CurrentResponse UpdateCareerInfo(UpdateCareerInfoViewModel updateCareerInfoViewModel)
        {
            try
            {
                
                if (_sourcerProfileRepository.
                    UpdateCareerInfo(updateCareerInfoViewModel.uuid, updateCareerInfoViewModel.experienceTypeId,
                    updateCareerInfoViewModel.hasAccessToDifferentWebsite, updateCareerInfoViewModel.industryId))
                {
                    CreateResponse(null, (int)HttpStatusCode.OK, "Sourcer career info has been updated successfully");
                    return _currentResponse;
                }

                CreateResponse(null, (int)HttpStatusCode.BadRequest, "Failed to update sourcer career info ");
                return _currentResponse;
            }
            catch (System.Exception e)
            {
                LogException(e);

                _currentResponse.message = "An error occurred while updating your data";
                _currentResponse.status = (int)HttpStatusCode.BadRequest;

                return _currentResponse;
            }
        }


        public CurrentResponse GetBasicInfo(string uuid)
        {
            var employerDetails = _sourcerProfileRepository.GetBasicInfo(uuid);
            if (employerDetails != null)
                CreateResponse(employerDetails, (int)HttpStatusCode.OK, "Sourcer details has been retrieved successfully");

            else
                CreateResponse(null, (int)HttpStatusCode.BadRequest, "Failed to retrieve sourcer details");

            return _currentResponse;
        }

        #endregion


    }
}
