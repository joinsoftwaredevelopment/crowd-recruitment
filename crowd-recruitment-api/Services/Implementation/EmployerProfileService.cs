﻿using System;
using System.Collections.Generic;
using System.Text;
using AutoMapper;
using Data.Tables;
using Data.Views;
using Repositories.Interfaces;
using Repositories.Enums;
using Services.Interfaces;
using Services.ViewModels;
using Helpers;
using System.Net;
using Microsoft.Extensions.Options;

namespace Services.Implementation
{
    public class EmployerProfileService : BaseService<EmployerDetail, EmployerDetailsViewModel>, IEmployerProfileService
    {
        #region Fields
        private readonly IEmployerProfileRepository _employerProfileRepository;
        private readonly IMapper _mapper;
        private readonly IUserRepository _userRepository;
        #endregion

        #region Constructors
        public EmployerProfileService(IEmployerProfileRepository employerProfileRepository,
            IMapper mapper, IUserRepository userRepository) : base(employerProfileRepository)
        {
            _employerProfileRepository = employerProfileRepository;
            _mapper = mapper;
            _userRepository = userRepository;
        }

        #endregion

        #region Methods
        public CurrentResponse UpdateEmployerDetails(UpdateEmployerDetailsViewModel updateEmployerDetailsViewModel)
        {
            try
            {
                //Validation 
                if (!updateEmployerDetailsViewModel.phoneNumber.Contains('+'))
                    updateEmployerDetailsViewModel.phoneNumber = string.Format("+{0}", updateEmployerDetailsViewModel.phoneNumber);
                if (string.IsNullOrEmpty(updateEmployerDetailsViewModel.companyName))
                {
                    _currentResponse.message = "CompanyName Name is required";
                    _currentResponse.status = (int)HttpStatusCode.BadRequest;
                    return _currentResponse;
                }

                if (string.IsNullOrEmpty(updateEmployerDetailsViewModel.firstName))
                {
                    _currentResponse.message = "First Name is required";
                    _currentResponse.status = (int)HttpStatusCode.BadRequest;
                    return _currentResponse;
                }

                if (string.IsNullOrEmpty(updateEmployerDetailsViewModel.lastName))
                {
                    _currentResponse.message = "Last Name is required";
                    _currentResponse.status = (int)HttpStatusCode.BadRequest;
                    return _currentResponse;
                }

                updateEmployerDetailsViewModel.email = updateEmployerDetailsViewModel.email.Trim();
                var isEmailValid = GeneralFunctions.IsEmailValid(updateEmployerDetailsViewModel.email);
                if (!isEmailValid)
                {
                    _currentResponse.message = "Email address is invalid";
                    _currentResponse.status = (int)HttpStatusCode.BadRequest;
                    return _currentResponse;
                }

                var isPhoneValid = GeneralFunctions.IsPhoneNumberValid(updateEmployerDetailsViewModel.phoneNumber);
                if (!isPhoneValid)
                {
                    _currentResponse.message = "Phone number is invalid";
                    _currentResponse.status = (int)HttpStatusCode.BadRequest;
                    return _currentResponse;
                }

           
                var currentUser = _userRepository.GetUserByUuId(updateEmployerDetailsViewModel.uuid);

                var isPhoneExist = _userRepository.IsPhoneExistBefore(updateEmployerDetailsViewModel.phoneNumber, null);
                if (isPhoneExist && updateEmployerDetailsViewModel.phoneNumber != currentUser.PhoneNumber)
                {
                    CreateResponse(null, (int)HttpStatusCode.BadRequest, "Phone number is already exist, choose another one");

                    return _currentResponse;
                }

                var isEmailExist = _userRepository.IsEmailExistBefore(updateEmployerDetailsViewModel.email, null);
                if (isEmailExist && updateEmployerDetailsViewModel.email != currentUser.Email)
                {
                    CreateResponse(null, (int)HttpStatusCode.BadRequest, "Email is already exist, choose another one");

                    return _currentResponse;
                }


                if (_employerProfileRepository.
                    UpdateEmployerDetails(updateEmployerDetailsViewModel.uuid, updateEmployerDetailsViewModel.companyName,
                    updateEmployerDetailsViewModel.firstName, updateEmployerDetailsViewModel.lastName, updateEmployerDetailsViewModel.phoneNumber,
                    updateEmployerDetailsViewModel.email))
                {
                    CreateResponse(null, (int)HttpStatusCode.OK, "Employer details has been updated successfully");
                    return _currentResponse;
                }

                CreateResponse(null, (int)HttpStatusCode.BadRequest, "Failed to update employer details");
                return _currentResponse;
            }
            catch (System.Exception e)
            {
                LogException(e);

                _currentResponse.message = "An error occurred while added your data";
                _currentResponse.status = (int)HttpStatusCode.BadRequest;

                return _currentResponse;
            }
        }

        public CurrentResponse GetEmployerDetails(string uuid)
        {
            var employerDetails = _employerProfileRepository.GetEmployerDetails(uuid);
            if (employerDetails != null)
                CreateResponse(employerDetails, (int)HttpStatusCode.OK, "Employer details has been retrieved successfully");

            else
                CreateResponse(null, (int)HttpStatusCode.BadRequest, "Failed to retrieve employer details");

            return _currentResponse;
        }

        public CurrentResponse UpdatePackage(UpdatePackageViewModel updatePackageViewModel)
        {
            if (_employerProfileRepository.UpdatePackage(updatePackageViewModel.uuid,updatePackageViewModel.packageId))
                CreateResponse(null, (int)HttpStatusCode.OK, "Employer packages has been updated successfully");

            else
                CreateResponse(null, (int)HttpStatusCode.BadRequest, "Failed to update employer packages");

            return _currentResponse;
        }

        #endregion


    }
}
