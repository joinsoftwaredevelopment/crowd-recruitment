﻿using AutoMapper;
using Data.Tables;
using Helpers;
using Repositories.Interfaces;
using Services.Common.Jwt.Claims;
using Services.Interfaces;
using Services.ViewModels;
using System;
using System.Collections.Generic;
using System.Net;
using System.Text;

namespace Services.Implementation
{
    public class PackagesService : BaseService<Package, AddPackageViewModel>, IPackagesService
    {
        #region Fields
        private readonly IPackagesRepository _packagesRepository;
        private readonly IMapper _mapper;
        #endregion

        #region Constructors
        public PackagesService(IMapper mapper
            , IPackagesRepository packagesRepository) : base(packagesRepository)
        {
            _packagesRepository = packagesRepository;
            _mapper = mapper;
        }

        #endregion

        #region Methods
        public CurrentResponse AddPackage(AddPackageViewModel addPackageViewModel)
        {
            var package = _packagesRepository.AddPackage(addPackageViewModel.packageTitle,
                addPackageViewModel.packagePrice, addPackageViewModel.packagesFeatures);
            if (package != null)
                CreateResponse(package, (int)HttpStatusCode.OK, "Package has been added successfully");

            else
                CreateResponse(null, (int)HttpStatusCode.BadRequest, "Failed to add package");

            return _currentResponse;
        }
        
        public CurrentResponse GetPackageById(int packageId)
        {
            var package = _packagesRepository.GetPackageById(packageId);
            if (package != null)
                CreateResponse(package, (int)HttpStatusCode.OK, "Package has been retrieved successfully");

            else
                CreateResponse(null, (int)HttpStatusCode.BadRequest, "Failed to retrieve package");

            return _currentResponse;
        }

        public CurrentResponse EditPackage(AddPackageViewModel addPackageViewModel)
        {
            var package = _packagesRepository.EditPackage(addPackageViewModel.packageId,addPackageViewModel.packageTitle,
                addPackageViewModel.packagePrice, addPackageViewModel.packagesFeatures);
            if (package != null)
                CreateResponse(package, (int)HttpStatusCode.OK, "Package has been edited successfully");

            else
                CreateResponse(null, (int)HttpStatusCode.BadRequest, "Failed to edit package");

            return _currentResponse;
        }

        public CurrentResponse DeletePackage(int packageId)
        {
            bool isDeleted = _packagesRepository.DeletePackage(packageId);
            if (isDeleted)
                CreateResponse(null, (int)HttpStatusCode.OK, "Package has been deleted successfully");

            else
                CreateResponse(null, (int)HttpStatusCode.BadRequest, "Failed to delete package");

            return _currentResponse;
        }
        public CurrentResponse GetAllPackages()
        {
            var packages = _packagesRepository.GetAllPackages();
            if (packages != null)
                CreateResponse(packages, (int)HttpStatusCode.OK, "Packages has been retrieved successfully");

            else
                CreateResponse(null, (int)HttpStatusCode.BadRequest, "Failed to retrieve packages");

            return _currentResponse;
        }
        
        #endregion
    }
}
