﻿using AutoMapper;
using Data.Tables;
using Data.Views;
using Repositories.Interfaces;
using Repositories.Enums;
using Services.Interfaces;
using Services.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;

namespace Services.Implementation
{
    public class JobService : BaseService<User, JobDetailsInfo>, IJobService
    {
        #region Fields
        private readonly IJobRepository _jobRepository;
        private readonly IUserRepository _userRepository;
        private readonly IMapper _mapper;
        #endregion

        #region Constructors

        public JobService(IJobRepository jobRepository,
            IMapper mapper, IUserRepository userRepository) : base(jobRepository)
        {
            _jobRepository = jobRepository;
            _mapper = mapper;
            _userRepository = userRepository;
        }
        #endregion

        #region Methods
        public CurrentResponse GetJobDetailsInfo(string uuid, int? tempId)
        {


            var job = _jobRepository.GetJobDetailsInfo(uuid, tempId);
            if (job != null)
                // GetJobInfo from template
                return CreateResponse(job, (int)HttpStatusCode.OK, "Employer details has been got successfully");

            else
                return CreateResponse(null, (int)HttpStatusCode.BadRequest, "Failed to get employers details");

        }

        public CurrentResponse GetJobInstructionsInfo(string uuid)
        {
            var JobDetailsInfo = _jobRepository.GetJobInstructionsInfo(uuid);

            List<MustHaveSkillsVM> mustHaveSkillsVM = new List<MustHaveSkillsVM>();

            foreach (var item in JobDetailsInfo.mustQualifications)
            {
                MustHaveSkillsVM mustHaveSkills = new MustHaveSkillsVM();
                mustHaveSkills.id = item.Uuid;
                mustHaveSkills.name = item.Text;
                mustHaveSkillsVM.Add(mustHaveSkills);
            }


            List<NiceHaveSkillsVM> niceHaveSkillsVM = new List<NiceHaveSkillsVM>();
            foreach (var item in JobDetailsInfo.niceQualifications)
            {
                NiceHaveSkillsVM niceHaveSkills = new NiceHaveSkillsVM();
                niceHaveSkills.id = item.Uuid;
                niceHaveSkills.name = item.Text;
                niceHaveSkillsVM.Add(niceHaveSkills);
            }

            List<NotToSourceVM> notToSourceVM = new List<NotToSourceVM>();
            foreach (var item in JobDetailsInfo.notToSourceQualifications)
            {
                NotToSourceVM notToSource = new NotToSourceVM();
                notToSource.id = item.Uuid;
                notToSource.name = item.Text;
                notToSourceVM.Add(notToSource);
            }

            InstructionsInfoVM instructionsInfoVM = new InstructionsInfoVM()
            {
                job = JobDetailsInfo.job,
                mustHaveSkillsVM = mustHaveSkillsVM,
                niceHaveSkillsVM = niceHaveSkillsVM,
                notToSourceVM = notToSourceVM,
            };


            if (JobDetailsInfo != null)
                CreateResponse(instructionsInfoVM, (int)HttpStatusCode.OK, "Job details has been got successfully");

            else
                CreateResponse(null, (int)HttpStatusCode.BadRequest, "Failed to get Job details");

            return _currentResponse;
        }

        public CurrentResponse GetJobInstructionsInfo()
        {
            var JobInstructionsInfo = _jobRepository.GetJobInstructionsInfo();
            if (JobInstructionsInfo != null)
                CreateResponse(JobInstructionsInfo, (int)HttpStatusCode.OK, "Employer details has been got successfully");

            else
                CreateResponse(null, (int)HttpStatusCode.BadRequest, "Failed to get employers details");

            return _currentResponse;
        }

        public CurrentResponse GetJobEngagementsInfo(string uuid)
        {
            var JobObjectivesInfo = _jobRepository.GetJobEngagementsInfo(uuid);
            if (JobObjectivesInfo != null)
                CreateResponse(JobObjectivesInfo, (int)HttpStatusCode.OK, "Employer details has been reteieved successfully");

            else
                CreateResponse(null, (int)HttpStatusCode.BadRequest, "Failed to get employers details");

            return _currentResponse;
        }

        public CurrentResponse GetJobByUuid(string uuid)
        {
            var JobInstructionsInfo = _jobRepository.GetJobByUuid(uuid);
            JobInstructionsInfo.WhenYouNeedToHire = JobInstructionsInfo.TimetoHireId.ToString();
            if (JobInstructionsInfo != null)
                CreateResponse(JobInstructionsInfo, (int)HttpStatusCode.OK, "Job details has been got successfully");

            else
                CreateResponse(null, (int)HttpStatusCode.BadRequest, "Failed to get job details");

            return _currentResponse;
        }

        public CurrentResponse CreateJob(PostJobDetails postJobViewModel)
        {
            try
            {
                var user = _userRepository.GetUserByUuId(postJobViewModel.uuId);

                var entity = _mapper.Map<Job>(postJobViewModel);

                entity.Uuid = Guid.NewGuid().ToString();
                entity.EmployerId = user.Id;
                entity.JobDate = DateTime.Now;
                entity.IsDeleted = false;
                entity.IndustryId = postJobViewModel.industryId;
                entity.Requirements = postJobViewModel.requirements;
                entity.IsActive = true;
                entity.JobStatusId = 1;  // needs an enum

                // adding
                _jobRepository.CreateJob(entity);

                //if job added from template insert skill from template
                if (postJobViewModel.jobTemplateId != 0)
                {
                    _jobRepository.InsertJobTemplateSkills(postJobViewModel.jobTemplateId, entity.Id);
                }

                CreateResponse(new { uuid = entity.Uuid }, (int)HttpStatusCode.OK, "your data has been added successfully");

                return _currentResponse;
            }
            catch (System.Exception e)
            {
                LogException(e);

                CreateResponse(null, (int)HttpStatusCode.BadRequest, "An error occurred while added your data");

                return _currentResponse;

            }
        }


        public CurrentResponse UpdateJob(PostJobDetails postJobViewModel)
        {
            var isUpdated = _jobRepository.UpdateJob(postJobViewModel.uuId, postJobViewModel.EmployerTypeId,
                postJobViewModel.industryId, postJobViewModel.JobDescription,
                postJobViewModel.jobLocationId, postJobViewModel.JobNationalityId,
                postJobViewModel.JobTitle, postJobViewModel.SeniorityLevelId, postJobViewModel.requirements
                );
            if (isUpdated)
                CreateResponse(new { uuid = postJobViewModel.uuId }, (int)HttpStatusCode.OK, "Job details is updated successfully");
            else
                CreateResponse(null, (int)HttpStatusCode.BadRequest, "Failed to update Job instructions");

            return _currentResponse;
        }


        public CurrentResponse CloseJob(CloseJob closeJob)
        {
            var isUpdated = _jobRepository.CloseJob(closeJob.uuId);
            if (isUpdated)
                CreateResponse(new { uuid = closeJob.uuId }, (int)HttpStatusCode.OK, "Job is closed successfully");
            else
                CreateResponse(null, (int)HttpStatusCode.BadRequest, "Failed to close job");

            return _currentResponse;
        }

        public CurrentResponse DeleteJob(string uuid)
        {
            var isUpdated = _jobRepository.DeleteJob(uuid);
            if (isUpdated)
                CreateResponse(new { uuid = uuid }, (int)HttpStatusCode.OK, "Job is deleted successfully");
            else
                CreateResponse(null, (int)HttpStatusCode.BadRequest, "Failed to delete job");

            return _currentResponse;
        }

        public CurrentResponse UpdateJobInstructions(PostJobInstructions postJobViewModel)
        {
            var isUpdated = _jobRepository.UpdateJobInstructions(postJobViewModel.uuid,
                postJobViewModel.experienceLevelId,
                postJobViewModel.mustHaveSkills, postJobViewModel.niceHaveQualification,
                postJobViewModel.notToSource, postJobViewModel.currencyId,
                postJobViewModel.minSalary, postJobViewModel.maxSalary
                );
            if (isUpdated)
                CreateResponse(new { uuid = postJobViewModel.uuid }, (int)HttpStatusCode.OK, "Job instructions is updated successfully");
            else
                CreateResponse(null, (int)HttpStatusCode.BadRequest, "Failed to update Job instructions");

            return _currentResponse;
        }

        public CurrentResponse UpdateJobObjectives(PostJobObjectives postJobViewModel)
        {
            var isUpdated = _jobRepository.UpdateJobObjectives(postJobViewModel.UUID, postJobViewModel.isContinuousHire,
                postJobViewModel.peopleToHireCount, postJobViewModel.whenYouNeedToHire, postJobViewModel.sourcingOutsideOfJoin,
                postJobViewModel.findingJobDifficultyLevelId

                );
            if (isUpdated)
                CreateResponse(new { uuid = postJobViewModel.UUID }, (int)HttpStatusCode.OK, "Job objectives is updated successfully");
            else
                CreateResponse(null, (int)HttpStatusCode.BadRequest, "Failed to update Job objectives");

            return _currentResponse;
        }

        public CurrentResponse UpdateJobEngagement(PostJobEngagment postJobViewModel)
        {
            var isUpdated = _jobRepository.UpdateJobEngagement(postJobViewModel.uuid, postJobViewModel.firstMailSubject,
                postJobViewModel.firstMailDescription, postJobViewModel.statusId,
                postJobViewModel.engagementTemplateId
                );
            if (isUpdated)
                CreateResponse(new { uuid = postJobViewModel.uuid }, (int)HttpStatusCode.OK, "Job engagement is updated successfully");
            else
                CreateResponse(null, (int)HttpStatusCode.BadRequest, "Failed to update Job engagement");

            return _currentResponse;
        }

        public CurrentResponse GetJobSummaries(int languageId, string uuId)
        {
            try
            {
                var user = _userRepository.GetUserByUuId(uuId);

                var jobSummaries = _jobRepository.GetJobSummaries(languageId, user.Id).Where(o => o.JobStatusId != (int)JobStatusEnum.Closed && o.JobStatusId != (int)JobStatusEnum.Draft);

                CreateResponse(jobSummaries, (int)HttpStatusCode.OK, "data is retrieved successfully");

                return _currentResponse;
            }
            catch (System.Exception e)
            {
                LogException(e);
                return null;
            }
        }

        public CurrentResponse GetJobSummariesByEmployer(int languageId, string uuId)
        {
            try
            {

                var jobSummaries = _jobRepository.GetJobSummariesByEmployer(languageId, uuId);

                CreateResponse(jobSummaries, (int)HttpStatusCode.OK, "data is retrieved successfully");

                return _currentResponse;
            }
            catch (System.Exception e)
            {
                LogException(e);
                return null;
            }
        }

        public CurrentResponse GetAllJobs(int languageId, string uuId)
        {
            try
            {

                var jobSummaries = _jobRepository.GetAllJobs(languageId, uuId);

                CreateResponse(jobSummaries, (int)HttpStatusCode.OK, "data is retrieved successfully");

                return _currentResponse;
            }
            catch (System.Exception e)
            {
                LogException(e);
                return null;
            }
        }

        public CurrentResponse GetAllJobs(int statusId)
        {
            try
            {

                var jobSummaries = _jobRepository.GetAllJobs(statusId);

                CreateResponse(jobSummaries, (int)HttpStatusCode.OK, "data is retrieved successfully");

                return _currentResponse;
            }
            catch (System.Exception e)
            {
                LogException(e);
                return null;
            }
        }


        public CurrentResponse GetJobDraftsByEmployer(int languageId, string uuId)
        {
            try
            {

                var jobSummaries = _jobRepository.GetJobDraftsByEmployer(languageId, uuId);

                CreateResponse(jobSummaries, (int)HttpStatusCode.OK, "data is retrieved successfully");

                return _currentResponse;
            }
            catch (System.Exception e)
            {
                LogException(e);
                return null;
            }
        }

        public CurrentResponse GetJobClosedByEmployer(int languageId, string employeruuid)
        {
            try
            {

                var jobSummaries = _jobRepository.GetJobClosedByEmployer(languageId, employeruuid);

                CreateResponse(jobSummaries, (int)HttpStatusCode.OK, "data is retrieved successfully");

                return _currentResponse;
            }
            catch (System.Exception e)
            {
                LogException(e);
                return null;
            }
        }


        public CurrentResponse GetSourcerJobSummaries(int languageId, string uuId, bool isActive)
        {
            try
            {

                var user = _userRepository.GetUserByUuId(uuId);

                var jobSummaries = _jobRepository.GetSourcerJobSummaries(languageId, user.Id, isActive);

                CreateResponse(jobSummaries, (int)HttpStatusCode.OK, "data is retrieved successfully");

                return _currentResponse;
            }
            catch (System.Exception e)
            {
                LogException(e);
                return null;
            }
        }

        public CurrentResponse GetSourcerJobSubmissions(int languageId, string sourcerUuId, string jobUuId)
        {
            try
            {
                var user = _userRepository.GetUserByUuId(sourcerUuId);
                var job = _jobRepository.GetJobBuUuId(jobUuId);
                var sourcerJobSubmissions = _jobRepository.GetSourcerJobSubmissions(languageId, user.Id, job.Id);

                CreateResponse(sourcerJobSubmissions, (int)HttpStatusCode.OK, "data is retrieved successfully");

                return _currentResponse;
            }
            catch (System.Exception e)
            {
                LogException(e);
                return null;
            }
        }

        public CurrentResponse AddSourcerSubmission(SourcerCVViewModel model)
        {
            try
            {
                var user = _userRepository.GetUserByUuId(model.Uuid);
                var job = _jobRepository.GetJobBuUuId(model.JobUuId);

                var jobCandidateSubmission = new JobCandidateSubmission()
                {
                    CandidateSubmissionStatusId = (int)CandidateSubmissionStatusEnum.Pending,
                    CvName = model.CvName,
                    JobId = job.Id,
                    SourcerId = user.Id,
                    IsDeleted = false,
                    SubmissionDate = DateTime.Now,
                    Uuid = Guid.NewGuid().ToString(),
                    CandidateName = model.candidateName,
                    LinkedInProfile = model.LinkedInProfile,
                    LocationId = model.LocationId,
                    Email = model.Email
                };

                _jobRepository.AddSourcerSubmission(jobCandidateSubmission);

                CreateResponse(jobCandidateSubmission, (int)HttpStatusCode.OK, "Job Submission is added Successfully");

                return _currentResponse;
            }
            catch (System.Exception e)
            {
                LogException(e);
                return null;
            }
        }

        public CurrentResponse GetVwJobByUuId(string uuId, int languageId)
        {
            try
            {
                var job = _jobRepository.GetVwJobByUuId(uuId, languageId);
                bool isMembershipActive = _userRepository.IsEmployerMembershipActive(job.EmployerId);

                if (isMembershipActive)
                {
                    CreateResponse(job, (int)HttpStatusCode.OK, "data is retrieved successfully");
                }
                else
                {
                    CreateResponse(null, (int)HttpStatusCode.BadRequest, "data is not retrieved successfully");
                }

                return _currentResponse;
            }
            catch (System.Exception e)
            {
                LogException(e);
                return null;
            }
        }

        public CurrentResponse AcceptJob(string uuid, int numberOfSubmissions, decimal commission)
        {
            try
            {
                var job = _jobRepository.AcceptJob(uuid, numberOfSubmissions, commission);

                CreateResponse(job, (int)HttpStatusCode.OK, "data is updated successfully");

                return _currentResponse;
            }
            catch (System.Exception e)
            {
                LogException(e);
                return null;
            }
        }

        public CurrentResponse DeclineJob(string uuid, string rejectionReason)
        {
            try
            {
                var job = _jobRepository.DeclineJob(uuid, rejectionReason);

                CreateResponse(job, (int)HttpStatusCode.OK, "data is updated successfully");

                return _currentResponse;
            }
            catch (System.Exception e)
            {
                LogException(e);
                return null;
            }
        }

        public CurrentResponse GetAllIndustries()
        {
            try
            {
                var job = _jobRepository.GetAllIndustries();

                CreateResponse(job, (int)HttpStatusCode.OK, "data is reteieved successfully");

                return _currentResponse;
            }
            catch (System.Exception e)
            {
                LogException(e);
                return null;
            }
        }

        public CurrentResponse GetTempByIndustryId(int industryId)
        {
            try
            {
                var job = _jobRepository.GetTempByIndustryId(industryId);

                CreateResponse(job, (int)HttpStatusCode.OK, "data is reteieved successfully");

                return _currentResponse;
            }
            catch (System.Exception e)
            {
                LogException(e);
                return null;
            }
        }

        public CurrentResponse GetJobTempById(int templateId)
        {
            try
            {
                var job = _jobRepository.GetJobTempById(templateId);

                CreateResponse(job, (int)HttpStatusCode.OK, "data is reteieved successfully");

                return _currentResponse;
            }
            catch (System.Exception e)
            {
                LogException(e);
                return null;
            }
        }

        public CurrentResponse GetAllJobTemps()
        {
            try
            {
                var job = _jobRepository.GetAllJobTemps();

                CreateResponse(job, (int)HttpStatusCode.OK, "data is reteieved successfully");

                return _currentResponse;
            }
            catch (System.Exception e)
            {
                LogException(e);
                return null;
            }
        }

        public bool IsEmployerMembershipActive(string jobUuid)
        {
            Job job = _jobRepository.GetJobBuUuId(jobUuid);
            return _userRepository.IsEmployerMembershipActive(job.EmployerId);
        }

        #endregion
    }
}
