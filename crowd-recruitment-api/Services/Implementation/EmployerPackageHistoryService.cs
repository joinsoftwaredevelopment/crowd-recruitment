﻿using Data.Tables;
using Repositories.Interfaces;
using Services.Interfaces;
using Services.ViewModels;
using System.Net;

namespace Services.Implementation
{
    public class EmployerPackageHistoryService : BaseService<EmployerPackageHistory, EmployerPackageHistoryViewModel>, IEmployerPackageHistoryService
    {
        #region Fields
        private readonly IEmployerPackageHistoryRepository _employerPackageHistoryRepository;
        private readonly IUserRepository _userRepository;
        #endregion

        #region Constructors
        public EmployerPackageHistoryService(IEmployerPackageHistoryRepository employerPackageHistoryRepository, IUserRepository userRepository
         ) : base(employerPackageHistoryRepository)
        {
            _employerPackageHistoryRepository = employerPackageHistoryRepository;
            _userRepository = userRepository;
        }

        #endregion

        #region Methods

        public CurrentResponse GetAllPackagesHistory(string uuId)
        {
            var user = _userRepository.GetUserByUuId(uuId);
            var employers = _employerPackageHistoryRepository.GetAllPackagesHistory(user.Id);
            if (employers != null)
                CreateResponse(employers , (int)HttpStatusCode.OK, "Employer packages history has been got successfully");
            
            else
                CreateResponse(null, (int)HttpStatusCode.BadRequest, "Failed to get packages history");
            
            return _currentResponse;
        }
        #endregion
    }
}
