﻿using AutoMapper;
using Data.Tables;
using Repositories.Interfaces;
using Services.Interfaces;
using Services.ViewModels;
using System;
using System.Collections.Generic;
using System.Net;
using System.Text;

namespace Services.Implementation
{
    public class CandidateService : BaseService<VwJobCandidate, JobCandidateViewModel>, ICandidateService
    {
        #region Fields
        private readonly ICandidateRepository _candidateRepository;
        private readonly IMapper _mapper;
        private readonly IJobRepository _jobRepository;
        private readonly IUserRepository _userRepository;
        #endregion

        #region Constructors

        public CandidateService(ICandidateRepository candidateRepository,
            IMapper mapper, IJobRepository jobRepository, IUserRepository userRepository) : base(candidateRepository)
        {
            _candidateRepository = candidateRepository;
            _mapper = mapper;
            _jobRepository = jobRepository;
            _userRepository = userRepository;
        }
        #endregion

        #region Email Template Keywords

        private const string _userName = "[USERNAME]";
        private const string _activationLink = "[ACTIVATIONLINK]";
        private const string _interviewName = "[INTERVIEWERNAME]";
        private const string _meetingDate = "[MEETINGDATE]";
        private const string _meetingTime = "[MEETINGTIME]";
        private const string _rejectionReason = "[REJECTIONREASON]";
        private const string _firstName = "[FIRST NAME]";
        private const string _websiteSourcer = "[WEBSITE/SOURCE]";
        private const string _fieldType = "[FIELD]";
        private const string _companyName = "[COMPANY NAME]";
        private const string _jobTitle = "[JOB TITLE]";
        private const string _yourName = "[YOUR NAME]";
        private const string _yourEmailSignature = "[YOUR EMAIL SIGNATURE]";

        #endregion

        #region Methods

        public CurrentResponse GetJobCandidates(int languageId, string jobUuid)
        {
            try
            {
                var jobCandidates = _candidateRepository.GetJobCandidates(languageId, jobUuid);

                CreateResponse(jobCandidates, (int)HttpStatusCode.OK, "data is retrieved successfully");

                return _currentResponse;
            }
            catch (System.Exception e)
            {
                LogException(e);
                return null;
            }
        }

        public CurrentResponse GetAcceptedJobCandidates(int languageId, string jobUuid)
        {
            try
            {
                var jobCandidates = _candidateRepository.GetAcceptedJobCandidates(languageId, jobUuid);

                CreateResponse(jobCandidates, (int)HttpStatusCode.OK, "data is retrieved successfully");

                return _currentResponse;
            }
            catch (System.Exception e)
            {
                LogException(e);
                return null;
            }
        }

        public CurrentResponse GetJobCandidateByUuId(int languageId, string candidateUuid)
        {
            try
            {
                var jobCandidate = _candidateRepository.GetJobCandidateByUuId(languageId, candidateUuid);

                var candidate = _candidateRepository.GetJobCandidateByUuId(languageId, candidateUuid);
                var sourcer = _userRepository.GetUserById(candidate.SourcerId);
                var job = _jobRepository.GetVwJobByUuId(candidate.JobUuId, languageId);
                var employer = _userRepository.GetUserById(job.EmployerId);

                jobCandidate.FirstMailSubject = jobCandidate.FirstMailSubject.Replace(_jobTitle, job.JobTitle);
                jobCandidate.FirstMailSubject = jobCandidate.FirstMailSubject.Replace(_companyName, job.CompanyName);

                jobCandidate.FirstMailDescription = jobCandidate.FirstMailDescription.Replace(_firstName, candidate.CandidateName);
                jobCandidate.FirstMailDescription = jobCandidate.FirstMailDescription.Replace(_websiteSourcer, "Recruitment Website");
                jobCandidate.FirstMailDescription = jobCandidate.FirstMailDescription.Replace(_fieldType, job.CompanyIndustry);
                jobCandidate.FirstMailDescription = jobCandidate.FirstMailDescription.Replace(_companyName, job.CompanyName);
                jobCandidate.FirstMailDescription = jobCandidate.FirstMailDescription.Replace(_jobTitle, job.JobTitle);
                jobCandidate.FirstMailDescription = jobCandidate.FirstMailDescription.Replace(_yourName, job.EmployerName);
                jobCandidate.FirstMailDescription = jobCandidate.FirstMailDescription.Replace(_yourEmailSignature, employer.Email);

                CreateResponse(jobCandidate, (int)HttpStatusCode.OK, "data is retrieved successfully");

                return _currentResponse;
            }
            catch (System.Exception e)
            {
                LogException(e);
                return null;
            }
        }

        public CurrentResponse GetSourcerCandidateBySourcerUuId(int languageId, string sourcerUuId, string jobUuId)
        {
            try
            {
                var sourcerCandidates = _candidateRepository.GetSourcerCandidateBySourcerUuId(languageId,sourcerUuId, jobUuId);
                
                CreateResponse(sourcerCandidates, (int)HttpStatusCode.OK, "data is retrieved successfully");

                return _currentResponse;
            }
            catch (System.Exception e)
            {
                LogException(e);
                return null;
            }
        }

        public CurrentResponse GetSourcerCandidateBySubmissionStatusId(int languageId, string sourcerUuId, int submissionStatusId, string jobUuId)
        {
            try
            {
                var sourcerCandidates = _candidateRepository.GetSourcerCandidateBySubmissionStatusId(languageId, sourcerUuId, submissionStatusId, jobUuId);

                CreateResponse(sourcerCandidates, (int)HttpStatusCode.OK, "data is retrieved successfully");

                return _currentResponse;
            }
            catch (System.Exception e)
            {
                LogException(e);
                return null;
            }
        }

        public CurrentResponse AcceptCandidate(string uuid)
        {
            if (string.IsNullOrEmpty(uuid))
            {
                CreateResponse(false, (int)HttpStatusCode.BadRequest, "Uuid is required");
                return _currentResponse;
            }

            var isAccepted = _candidateRepository.AcceptCandidate(uuid);

            if (isAccepted)
            {
                // send acceptance mail
                //MailsManager.SendMail(user.Email, "Recruitment - Acceptance email"
                //    , "Congratulations! you have been accepted to join recruitment as a sourcer");

                //var sourcer = _userRepository.GetUserByUuId(uuid);
                //var userName = string.Format("{0} {1}", sourcer.FirstName, sourcer.LastName);
                //_emailTemplateService.SendCRSourcerAcceptanceEmail(2, userName, sourcer.Email, _appSettings.WebAppLink);

                CreateResponse(isAccepted, (int)HttpStatusCode.OK, "Candidate has been accepted successfully");
            }

            else
                CreateResponse(false, (int)HttpStatusCode.BadRequest, "Failed to accept Candidate");

            return _currentResponse;
        }

        public CurrentResponse UpdateHiringStage(UpdateHiringStageViewModel updateHiringStageViewModel)
        {
     

            var isChanged = _candidateRepository.
                UpdateHiringStage(updateHiringStageViewModel.submissionUuid,
                updateHiringStageViewModel.hiringStageId);

            if (isChanged)
                CreateResponse(isChanged, (int)HttpStatusCode.OK, "Candidate has been changed successfully");
          

            else
                CreateResponse(false, (int)HttpStatusCode.BadRequest, "Failed to changed Candidate");

            return _currentResponse;
        }

        public CurrentResponse UpdateIsAcceptOffer(UpdateIsAcceptOfferViewModel updateIsAcceptOfferViewModel)
        {


            var isChanged = _candidateRepository.
                UpdateIsAcceptOffer(updateIsAcceptOfferViewModel.submissionUuid,
                updateIsAcceptOfferViewModel.isOfferAccepted
                );

            if (isChanged)
                CreateResponse(isChanged, (int)HttpStatusCode.OK, "Candidate has been changed successfully");


            else
                CreateResponse(false, (int)HttpStatusCode.BadRequest, "Failed to changed Candidate");

            return _currentResponse;
        }


        public CurrentResponse UpdateIsHired(UpdateIsHiredViewModel updateIsHiredViewModel)
        {


            var isChanged = _candidateRepository.
                UpdateIsHired(updateIsHiredViewModel.submissionUuid,updateIsHiredViewModel.isHired);

            if (isChanged)
                CreateResponse(isChanged, (int)HttpStatusCode.OK, "Candidate has been changed successfully");


            else
                CreateResponse(false, (int)HttpStatusCode.BadRequest, "Failed to changed Candidate");

            return _currentResponse;
        }

        public CurrentResponse UpdateCandidateCvStatus(UpdateCandidateCvStatusViewModel updateCandidateCvStatusViewModel)
        {


            var isChanged = _candidateRepository.
                UpdateCandidateCvStatus(updateCandidateCvStatusViewModel.submissionUuid, updateCandidateCvStatusViewModel.isCvApproved);

            if (isChanged)
                CreateResponse(isChanged, (int)HttpStatusCode.OK, "Candidate has been changed successfully");


            else
                CreateResponse(false, (int)HttpStatusCode.BadRequest, "Failed to changed Candidate");

            return _currentResponse;
        }

        
        public CurrentResponse DeclineCandidate(UserRequestViewModel userRequestViewModel)
        {
            if (string.IsNullOrEmpty(userRequestViewModel.uuid))
            {
                CreateResponse(false, (int)HttpStatusCode.BadRequest, "Uuid is required");
                return _currentResponse;
            }

            var isDeclined = _candidateRepository.DeclineCandidate(userRequestViewModel.uuid, userRequestViewModel.rejectedReason);

            if (isDeclined)
            {
                //var sourcer = _userRepository.GetUserByUuId(userRequestViewModel.uuid);
                //var userName = string.Format("{0} {1}", sourcer.FirstName, sourcer.LastName);
                //_emailTemplateService.SendCRSourcerAcceptanceEmail(2, userName, sourcer.Email, _appSettings.WebAppLink);

                CreateResponse(isDeclined, (int)HttpStatusCode.OK, "Candidate has been declined ");
            }

            else
                CreateResponse(false, (int)HttpStatusCode.BadRequest, "Failed to decline Candidate");

            return _currentResponse;
        }

        public CurrentResponse EmployerAcceptCandidate(string uuid)
        {
            if (string.IsNullOrEmpty(uuid))
            {
                CreateResponse(false, (int)HttpStatusCode.BadRequest, "Uuid is required");
                return _currentResponse;
            }

            var isAccepted = _candidateRepository.EmployerAcceptCandidate(uuid);

            if (isAccepted)
            {
                // send acceptance mail
                //MailsManager.SendMail(user.Email, "Recruitment - Acceptance email"
                //    , "Congratulations! you have been accepted to join recruitment as a sourcer");

                //var sourcer = _userRepository.GetUserByUuId(uuid);
                //var userName = string.Format("{0} {1}", sourcer.FirstName, sourcer.LastName);
                //_emailTemplateService.SendCRSourcerAcceptanceEmail(2, userName, sourcer.Email, _appSettings.WebAppLink);

                CreateResponse(isAccepted, (int)HttpStatusCode.OK, "Candidate has been accepted by Employer successfully");
            }

            else
                CreateResponse(false, (int)HttpStatusCode.BadRequest, "Failed to accept Candidate");

            return _currentResponse;
        }

        public CurrentResponse EmployerDeclineCandidate(UserRequestViewModel userRequestViewModel)
        {
            if (string.IsNullOrEmpty(userRequestViewModel.uuid))
            {
                CreateResponse(false, (int)HttpStatusCode.BadRequest, "Uuid is required");
                return _currentResponse;
            }

            var isDeclined = _candidateRepository.EmployerDeclineCandidate(userRequestViewModel.uuid, userRequestViewModel.rejectedReason);

            if (isDeclined)
            {
                //var sourcer = _userRepository.GetUserByUuId(userRequestViewModel.uuid);
                //var userName = string.Format("{0} {1}", sourcer.FirstName, sourcer.LastName);
                //_emailTemplateService.SendCRSourcerAcceptanceEmail(2, userName, sourcer.Email, _appSettings.WebAppLink);

                CreateResponse(isDeclined, (int)HttpStatusCode.OK, "Candidate has been declined By Employer ");
            }

            else
                CreateResponse(false, (int)HttpStatusCode.BadRequest, "Failed to decline Candidate");

            return _currentResponse;
        }

        public CurrentResponse GetCandidateStatusHistoryByUuId(int languageId, string candidateUuid)
        {
            try
            {
                var sourcerCandidates = _candidateRepository.GetCandidateStatusHistoryByUuId(languageId,candidateUuid);

                CreateResponse(sourcerCandidates, (int)HttpStatusCode.OK, "data is retrieved successfully");

                return _currentResponse;
            }
            catch (System.Exception e)
            {
                LogException(e);
                return null;
            }
        }

        #endregion
    }
}
