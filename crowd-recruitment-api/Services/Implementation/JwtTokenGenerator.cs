﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using microsoftIdentityModel = Microsoft.IdentityModel.Tokens;
using jwtIdentityModel = System.IdentityModel.Tokens.Jwt;
using Exception = System.Exception;
using Microsoft.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Reflection;
using Newtonsoft.Json;
using Services.Interfaces;
using Repositories.Common.Loggers;
using Services.ViewModels;
using Services.Common.Jwt.Claims;

namespace Services.Implementation
{
    public class JwtTokenGenerator : IJwtTokenGenerator
    {
        #region Fields
        //private JwtApiContract _jwtApiContract;
        private readonly ICredentialService _credentialService;
        #endregion

        #region Constructors

        public JwtTokenGenerator(ICredentialService credentialService)
        {
            _credentialService = credentialService;
            //_jwtApiContract = _credentialService.GetJwtCredentials();
        }

        #endregion

        #region Methods
        //public  string GenerateToken(UserClaim userClaim)
        //{
        //    try
        //    {
        //        if (userClaim == null)
        //            return null;

        //        //Get secret key for signing creditionals
        //        var secretKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes("croud-recruitment-key|"));

        //        //Get secret key for encryption creditionals
        //        //var encryptedKey = GetEncryptedKey(_jwtApiContract.SecretKey);

        //        //Fill security token descriptor object with all data needed for token creation
        //        var securityTokenDescriptor = new SecurityTokenDescriptor()
        //        {
        //            // Issuer of the JWT 
        //            Issuer = "https://www.join.sa/",

        //            // Recipient for which the JWT is intended
        //            Audience = "https://www.join.sa/",

        //            // Time at which the JWT was issued; can be used to determine age of the JWT
        //            IssuedAt =  DateTime.Now,

        //            // Subject of the JWT (the user)
        //            Subject = new ClaimsIdentity(new[]
        //                   {
        //                    new Claim("UuId", userClaim.UuId.ToString()),
        //                    new Claim("UserName", userClaim.UserName??string.Empty),
        //                    new Claim("UserEmail", userClaim.UserEmail??string.Empty),
        //                    new Claim("UserRole", userClaim.UserRole.ToString()),
        //                    new Claim("Language", userClaim.Language.ToString()),
        //                    new Claim("ImageName", userClaim.ImageName??string.Empty),
        //                    new Claim("PayLoad",JsonConvert.SerializeObject(userClaim.OtherClaims))
        //                }),

        //            // Time before which the JWT must not be accepted for processing
        //            NotBefore = null,

        //            Claims = userClaim.OtherClaims,

        //            // Time after which the JWT expires
        //            Expires = DateTime.Now.Add(new TimeSpan(1, 0, 0)),

        //            //Signing credentials using decrypted secret key
        //            SigningCredentials = new SigningCredentials(secretKey, SecurityAlgorithms.HmacSha256Signature),

        //            //Encrypting credentials using secret key
        //            //EncryptingCredentials = new EncryptingCredentials(encryptedKey, SecurityAlgorithms.Aes256KW, SecurityAlgorithms.Aes256CbcHmacSha512),
        //        };

        //        var token_Handler = new JwtSecurityTokenHandler();
        //        var securityToken = token_Handler.CreateToken(securityTokenDescriptor);
        //        //Logger.Instance.LogMessage("test");
        //        var token = token_Handler.WriteToken(securityToken);
        //        //Logger.Instance.LogMessage(token);
        //        return token;
        //    }
        //    catch (Exception e)
        //    {
        //        Logger.Instance.Log(e);
        //    }
        //    return null;
        //}
        public UserClaim ValidateToken(string jwtToken)
        {
            try
            {
                if (string.IsNullOrEmpty(jwtToken))
                    return null;

                //Set validate paramters with secret key and encrypted key that used in token creation
                TokenValidationParameters validationParameters = new TokenValidationParameters()
                {
                    ValidIssuer = "http://issuer.com",
                    ValidAudience = "http://mysite.com",
                    IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes("croud-recruitment-key|")),
                    //TokenDecryptionKey = GetEncryptedKey(_jwtApiContract != null ? _jwtApiContract.SecretKey : string.Empty),
                };

                JwtSecurityTokenHandler securityTokenHandler = new JwtSecurityTokenHandler();
                SecurityToken securityToken = new JwtSecurityToken(jwtToken);

                //Validate token and get payload Data
                ClaimsPrincipal claimsPrincipal = securityTokenHandler.ValidateToken(jwtToken, validationParameters, out securityToken);
                var payloadData = ((JwtSecurityToken)securityToken).Payload.Claims;

                //Set payload data to User Claim
                var uuId = payloadData.FirstOrDefault(p => p.Type == "UuId");
                var userName = payloadData.FirstOrDefault(p => p.Type == "UserName");
                var userEmail = payloadData.FirstOrDefault(p => p.Type == "UserEmail");
                var userRole = payloadData.FirstOrDefault(p => p.Type == "UserRole");
                var language = payloadData.FirstOrDefault(p => p.Type == "Language");
                var imageName = payloadData.FirstOrDefault(p => p.Type == "ImageName");
                var otherClaims = payloadData.FirstOrDefault(p => p.Type == "PayLoad");


                var userClaim = new UserClaim()
                {
                    UuId = uuId != null ? uuId.Value : string.Empty,
                    UserName = userName != null ? userName.Value : string.Empty,
                    UserEmail = userEmail != null ? userEmail.Value : string.Empty,
                    UserRole = userRole != null ? Convert.ToInt32(userRole.Value) : 0,
                    Language = language != null ? Convert.ToInt32(language.Value) : 1,
                    ImageName = imageName != null ? imageName.Value : string.Empty,
                    OtherClaims = otherClaims != null ? JsonConvert.DeserializeObject<Dictionary<string, object>>(otherClaims.Value) : null
                };

                return userClaim;

            }
            catch (Exception e)
            {
                Logger.Instance.Log(e);
            }
            return null;
        }

        public string generateJwtToken(UserClaim userClaim)
        {
            // generate token that is valid for 7 days
            var tokenHandler = new JwtSecurityTokenHandler();
            var key = Encoding.ASCII.GetBytes("croud-recruitment-key|");
            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new[] { new Claim("UuId", userClaim.UuId.ToString()),
                            new Claim("UserName", userClaim.UserName??string.Empty),
                            new Claim("UserEmail", userClaim.UserEmail??string.Empty),
                            new Claim("UserRole", userClaim.UserRole.ToString()),
                            new Claim("Language", userClaim.Language.ToString()),
                            new Claim("ImageName", userClaim.ImageName??string.Empty)}),
                Expires = DateTime.UtcNow.AddDays(7),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
            };
            var token = tokenHandler.CreateToken(tokenDescriptor);
            return tokenHandler.WriteToken(token);
        }

        private SymmetricSecurityKey GetEncryptedKey(string secretKey)
        {
            try
            {
                var encryptKeyBytes = Encoding.UTF32.GetBytes(secretKey);
                // Note that the encryptedKey should have 256 / 8 length: to make encryption run Successfully              
                byte[] ecKey = new byte[256 / 8];
                Array.Copy(encryptKeyBytes, ecKey, 256 / 8);
                return new SymmetricSecurityKey(ecKey);
            }
            catch (Exception e)
            {
                Logger.Instance.Log(e);
            }
            return null;
        }
        #endregion
    }
}
