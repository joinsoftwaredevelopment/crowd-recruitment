﻿using System;
using System.Collections.Generic;
using System.Text;
using AutoMapper;
using Data.Tables;
using Data.Views;
using Repositories.Interfaces;
using Repositories.Enums;
using Services.Interfaces;
using Services.ViewModels;
using Helpers;
using System.Net;
using Microsoft.Extensions.Options;

namespace Services.Implementation
{
    public class InterviewService : BaseService<CandidatesInterview, CandidatesInterview>, IInterviewService
    {
        #region Fields
        private readonly IInterviewRepositpry  _interviewRepository;
        private readonly IMapper _mapper;
        private readonly IEmailTemplateService _emailTemplateService;
        #endregion

        #region Constructors
        public InterviewService(IInterviewRepositpry interviewRepository,IMapper mapper
            ,IEmailTemplateService emailTemplateService) : base(interviewRepository)
        {
            _interviewRepository = interviewRepository;
            _mapper = mapper;
            _emailTemplateService = emailTemplateService;
        }

        #endregion

        #region Methods
        public CurrentResponse AddInterviewDetails(InterviewDetailsViewModel model)
        {
            try
            {
                var candidateInfo = _interviewRepository.GetCandidateBySubmissionUuid(model.submissionUUid);

                var entity = new CandidatesInterview();
                entity.CandidateId = candidateInfo.SubmissionId;
                entity.InterviewerId = model.interviewerId;
                entity.InterviewComment = model.interviewComment;
                entity.InterviewTypeId = model.interviewTypeId;
                entity.InterviewerTypeId = model.interviewerTypeId;
                entity.InterviewDate = model.interviewDate;
                entity.InterviewTime = model.interviewTime;
                entity.Duration = model.duration;
                entity.CreatedDate = DateTime.Now;
                entity.TemplateId = model.templateId;
                entity.EmailSubject = model.emailSubject;
                entity.EmailDescription = model.emailDescription;

                int interViewId = 0;

                // check if interview id > 0 update interview else add
                if (model.id > 0)
                {
                    entity.Id = model.id;
                    interViewId = _interviewRepository.UpdateInterviewDetails(entity);

                    // send email with new edit
                    //var emailDetails = GetInterviewEmailDetails(interViewId);
                    //_emailTemplateService.SendInterviewEmail(emailDetails.candidateName,
                    //    emailDetails.companyName, emailDetails.interviewDate, emailDetails.interviewDuration,
                    //    emailDetails.interviewerName, 2, emailDetails.candidateEmail,
                    //    emailDetails.description, emailDetails.subject,emailDetails.jobTitle);

                }

                else
                    interViewId = _interviewRepository.AddInterviewDetails(entity, model.employerUuid);


                if (interViewId > 0)
                {
                    var emailDetails = GetInterviewEmailDetails(interViewId);

                    switch (emailDetails.interviewDuration)
                    {
                        case "1":
                            emailDetails.interviewDuration = "30 minures";
                            break;
                        case "2":
                            emailDetails.interviewDuration = "1 hour";
                            break;
                        case "3":
                            emailDetails.interviewDuration = "2 hours";
                            break;
                        default:
                            emailDetails.interviewDuration = "30 minures";
                            break;
                    }

                    _emailTemplateService.SendInterviewEmail(emailDetails.candidateName,
                        emailDetails.companyName, emailDetails.interviewDate,emailDetails.time, emailDetails.interviewDuration,
                        emailDetails.interviewerName, 2, emailDetails.candidateEmail,
                        emailDetails.description, emailDetails.subject,emailDetails.jobTitle);

                    CreateResponse(new { interViewId = interViewId }, (int)HttpStatusCode.OK,
                        "Interview details has been added successfully");
                }
                
                else
                    CreateResponse(null, (int)HttpStatusCode.BadRequest,
                        "Failed to add Interview details");



                return _currentResponse;
            }
            catch (System.Exception e)
            {
                LogException(e);

                CreateResponse(null, (int)HttpStatusCode.BadRequest, "An error occurred while added your data");

                return _currentResponse;
            }
        }

        public CurrentResponse SendOffer(OfferViewModel offerViewModel)
        {
            try
            {
                var candidateInfo = _interviewRepository.GetCandidateBySubmissionUuid(offerViewModel.submissionUuid);

                //_emailTemplateService.SendInterviewEmail();
                _emailTemplateService.SendOfferEmail(offerViewModel.offerSubject,
                    offerViewModel.offerDescription,offerViewModel.offerFile,candidateInfo.CandidateName,
                    candidateInfo.Email,candidateInfo.JobTitle,2);


                CreateResponse(null, (int)HttpStatusCode.OK,
                        "Offer has been added successfully");
              
                return _currentResponse;
            }
            catch (System.Exception e)
            {
                LogException(e);

                CreateResponse(null, (int)HttpStatusCode.BadRequest, "An error occurred while added your data");

                return _currentResponse;
            }
        }
        public InterviewEmailDetailsViewModel GetInterviewEmailDetails(int interviewId)
        {
            var interviewDetails = _interviewRepository.GetInterviewEmailDetails(interviewId);
            InterviewEmailDetailsViewModel interviewEmailDetails = new InterviewEmailDetailsViewModel()
            {
                interviewDate = interviewDetails.InterviewDate.Date,
                time = interviewDetails.InterviewTime,
                interviewDuration = interviewDetails.Duration.ToString(),
                companyName = interviewDetails.CompanyName,
                candidateEmail = interviewDetails.CandidateEmail,
                candidateName = interviewDetails.CandidateName,
                interviewerName = interviewDetails.InterviewerName,
                subject = interviewDetails.EmailSubject,
                description = interviewDetails.EmailDescription,
                jobTitle = interviewDetails.JobTitle

            };

            return interviewEmailDetails;
        }

        public CurrentResponse AddInterviewEmails(InterviewEmailViewModel model)
        {
            try
            {
                var entity = _mapper.Map<CandidatesInterview>(model);

                entity.Id = model.interviewId;
                entity.TemplateId = model.templateId;
                entity.EmailSubject = model.emailSubject;
                entity.EmailDescription = model.emailDescription;
                entity.IsEmailSent = true;

                int interViewId = _interviewRepository.AddInterviewEmails(entity);

                if (interViewId > 0)
                {
                    // Getting interview details

                    //sending interview email
                    var emailDetails = GetInterviewEmailDetails(model.interviewId);
                    _emailTemplateService.SendInterviewEmail(emailDetails.candidateName,
                        emailDetails.companyName, emailDetails.interviewDate,emailDetails.time, emailDetails.interviewDuration,
                        emailDetails.interviewerName, 2, emailDetails.candidateEmail,
                        emailDetails.description,emailDetails.subject, emailDetails.jobTitle);

                    CreateResponse(new { interViewId = interViewId }, (int)HttpStatusCode.OK,
                        "Interview emails has been added successfully");
                }

                else
                    CreateResponse(null, (int)HttpStatusCode.BadRequest,
                        "Failed to add Interview emails");



                return _currentResponse;
            }
            catch (System.Exception e)
            {
                LogException(e);

                CreateResponse(null, (int)HttpStatusCode.BadRequest, "An error occurred while added your data");

                return _currentResponse;
            }
        }
        
        public CurrentResponse CancelInterview(int interviewId, bool isCanceled)
        {
            try
            {
                int interViewId = _interviewRepository.CancelInterview(interviewId,isCanceled);

                if (interViewId > 0)
                {
                    CreateResponse(new { interViewId = interViewId }, (int)HttpStatusCode.OK,
                        "Interview has been canceled successfully");
                }

                else
                    CreateResponse(null, (int)HttpStatusCode.BadRequest,
                        "Failed to cancel Interview");



                return _currentResponse;
            }
            catch (System.Exception e)
            {
                LogException(e);

                CreateResponse(null, (int)HttpStatusCode.BadRequest, "An error occurred while added your data");

                return _currentResponse;
            }
        }

        public CurrentResponse GetAllInterviewers(int languageId)
        {
            try
            {
                var interviewers = _interviewRepository.GetAllInterviewers(languageId);

                if (interviewers != null)
                    CreateResponse(interviewers , (int)HttpStatusCode.OK,
                        "Data has been retrieved successfully");

                else
                    CreateResponse(null, (int)HttpStatusCode.BadRequest,
                        "Failed to retrieve data");



                return _currentResponse;
            }
            catch (System.Exception e)
            {
                LogException(e);

                CreateResponse(null, (int)HttpStatusCode.BadRequest, "Failed to retrieve data");

                return _currentResponse;
            }
        }

        public CurrentResponse GetAllInterviews()
        {
            try
            {
                var interviewDetails = _interviewRepository.GetAllInterviews();

                if (interviewDetails != null)
                    CreateResponse(interviewDetails, (int)HttpStatusCode.OK,
                        "Interviews details has been retrieved successfully");

                else
                    CreateResponse(null, (int)HttpStatusCode.BadRequest,
                        "Failed to retrieve interviews details");



                return _currentResponse;
            }
            catch (System.Exception e)
            {
                LogException(e);

                CreateResponse(null, (int)HttpStatusCode.BadRequest, "Failed to retrieve interview details");

                return _currentResponse;
            }
        }

        public CurrentResponse GetAllInterviewsByUuid(string uuid)
        {
            try
            {
                var interviewDetails = _interviewRepository.GetAllInterviewsByUuid(uuid);

                if (interviewDetails != null)
                    CreateResponse(interviewDetails, (int)HttpStatusCode.OK,
                        "Interviews details has been retrieved successfully");

                else
                    CreateResponse(null, (int)HttpStatusCode.BadRequest,
                        "Failed to retrieve interviews details");



                return _currentResponse;
            }
            catch (System.Exception e)
            {
                LogException(e);

                CreateResponse(null, (int)HttpStatusCode.BadRequest, "Failed to retrieve interview details");

                return _currentResponse;
            }
        }
       
        public CurrentResponse GetInterviewDetailsById(int interviewId)
        {
            try
            {
                var interviewDetails = _interviewRepository.GetInterviewDetailsById(interviewId);

                if (interviewDetails != null)
                    CreateResponse(interviewDetails, (int)HttpStatusCode.OK,
                        "Interview details has been retrieved successfully");

                else
                    CreateResponse(null, (int)HttpStatusCode.BadRequest,
                        "Failed to retrieve interview details");



                return _currentResponse;
            }
            catch (System.Exception e)
            {
                LogException(e);

                CreateResponse(null, (int)HttpStatusCode.BadRequest, "Failed to retrieve interview details");

                return _currentResponse;
            }
        }

        public CurrentResponse GetInterviewEmailsById(int interviewId)
        {
            try
            {
                var interviewEmails = _interviewRepository.GetInterviewEmailsById(interviewId);

                if (interviewEmails != null)
                    CreateResponse(interviewEmails, (int)HttpStatusCode.OK,
                        "Interview emails has been retrieved successfully");

                else
                    CreateResponse(null, (int)HttpStatusCode.BadRequest,
                        "Failed to retrieve interview emails");



                return _currentResponse;
            }
            catch (System.Exception e)
            {
                LogException(e);

                CreateResponse(null, (int)HttpStatusCode.BadRequest, "Failed to retrieve interview emails");

                return _currentResponse;
            }
        }

        public List<BasicViewModel> GetAllInterviewTypes()
        {
            List<BasicViewModel> lst = new List<BasicViewModel>();

            lst.Add(new BasicViewModel()
            {
                id = 1,
                name = "Telephone",
                languageId = 2
            });

            lst.Add(new BasicViewModel()
            {
                id = 1,
                name = "مكالمة تليفونية",
                languageId = 1
            });

            lst.Add(new BasicViewModel()
            {
                id = 2,
                name = "On site",
                languageId = 2
            });

            lst.Add(new BasicViewModel()
            {
                id = 2,
                name = "في المكتب",
                languageId = 1
            });


            lst.Add(new BasicViewModel()
            {
                id = 3,
                name = "Web conference",
                languageId = 2
            });

            lst.Add(new BasicViewModel()
            {
                id = 3,
                name = "عبر الانترنت",
                languageId = 1
            });


            return lst;
        }

        public CurrentResponse GetInterviewTypes(int languageId)
        {
            // search by languge id
            var types = GetAllInterviewTypes().Find(m => m.languageId == languageId);
            CreateResponse(types, (int)HttpStatusCode.OK,
                "Interview types has been retrieved successfully");
            return _currentResponse;
        }

        public List<BasicViewModel> GetAllDurationTimes()
        {
            List<BasicViewModel> lst = new List<BasicViewModel>();

            lst.Add(new BasicViewModel()
            {
                id = 1,
                name = "30 minutes",
                languageId = 2
            });

            lst.Add(new BasicViewModel()
            {
                id = 1,
                name = "نصف ساعة",
                languageId = 1
            });

            lst.Add(new BasicViewModel()
            {
                id = 2,
                name = "1 hour",
                languageId = 2
            });

            lst.Add(new BasicViewModel()
            {
                id = 2,
                name = "ساعة",
                languageId = 1
            });


            lst.Add(new BasicViewModel()
            {
                id = 3,
                name = "2 hours",
                languageId = 2
            });

            lst.Add(new BasicViewModel()
            {
                id = 3,
                name = "ساعتان",
                languageId = 1
            });


            return lst;
        }

        public CurrentResponse GetDurationTimes(int languageId)
        {
            // search by languge id
            var types = GetAllDurationTimes().Find(m => m.languageId == languageId);
            CreateResponse(types, (int)HttpStatusCode.OK, "");
            return _currentResponse;
        }

        public CurrentResponse GetInterviewTemplates()
        {
            try
            {
                var templates = _interviewRepository.GetInterviewTemplates();

                if (templates != null)
                    CreateResponse(templates, (int)HttpStatusCode.OK,
                        "Interview templates has been retrieved successfully");

                else
                    CreateResponse(null, (int)HttpStatusCode.BadRequest,
                        "Failed to retrieve interview templates");



                return _currentResponse;
            }
            catch (System.Exception e)
            {
                LogException(e);

                CreateResponse(null, (int)HttpStatusCode.BadRequest, "Failed to retrieve interview templates");

                return _currentResponse;
            }
        }
        
        public CurrentResponse GetOfferTemplates()
        {
            try
            {
                var templates = _interviewRepository.GetOfferTemplates();

                if (templates != null)
                    CreateResponse(templates, (int)HttpStatusCode.OK,
                        "Offer templates has been retrieved successfully");

                else
                    CreateResponse(null, (int)HttpStatusCode.BadRequest,
                        "Failed to retrieve Offer templates");



                return _currentResponse;
            }
            catch (System.Exception e)
            {
                LogException(e);

                CreateResponse(null, (int)HttpStatusCode.BadRequest, "Failed to retrieve interview templates");

                return _currentResponse;
            }
        }
        public CurrentResponse GetInterviewTempById(int templateId)
        {
            try
            {
                var template = _interviewRepository.GetInterviewTempById(templateId);

                if (template != null)
                    CreateResponse(template, (int)HttpStatusCode.OK,
                        "Interview template has been retrieved successfully");

                else
                    CreateResponse(null, (int)HttpStatusCode.BadRequest,
                        "Failed to retrieve interview template");



                return _currentResponse;
            }
            catch (System.Exception e)
            {
                LogException(e);

                CreateResponse(null, (int)HttpStatusCode.BadRequest, "Failed to retrieve interview template");

                return _currentResponse;
            }
        }


        #endregion

    }
}
