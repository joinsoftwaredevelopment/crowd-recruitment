﻿using AutoMapper;
using Data.Tables;
using Helpers;
using Microsoft.Extensions.Options;
using Repositories.Enums;
using Repositories.Interfaces;
using Services.Common.Jwt.Claims;
using Services.Interfaces;
using Services.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;

namespace Services.Implementation
{
    public class SourcerService : BaseService<SourcerDetail, SourcerViewModel>, ISourcerService
    {
        #region Fields
        private readonly ISourcerRepository _sourcerRepository;
        private readonly IUserRepository _userRepository;
        private readonly IMapper _mapper;
        private readonly IJwtTokenGenerator _jwtTokenGenerator;
        private readonly IEmailTemplateService _emailTemplateService;
        private readonly AppSettings _appSettings;
        #endregion

        #region Constructors

        public SourcerService(ISourcerRepository sourcerRepository, IUserRepository userRepository,
            IMapper mapper, IJwtTokenGenerator jwtTokenGenerator, IEmailTemplateService emailTemplateService, IOptions<AppSettings> appSettings) : base(sourcerRepository)
        {
            _sourcerRepository = sourcerRepository;
            _userRepository = userRepository;
            _mapper = mapper;
            _jwtTokenGenerator = jwtTokenGenerator;
            _emailTemplateService = emailTemplateService;
            _appSettings = appSettings.Value;
        }


        #endregion

        #region Methods
        public SourcerViewModel GetSourcerByUuId(string uuid)
        {
            try
            {
                var sourcer = _sourcerRepository.GetSourcerByUuId(uuid);
                var sourcerViewModel = _mapper.Map<SourcerCVViewModel>(sourcer);

                return null;
            }
            catch (Exception e)
            {
                return null;
            }
        }

        public CurrentResponse CreateSourcer(SourcerViewModel model)
        {
            try
            {
                //Validation 
                if(!model.PhoneNumber.Contains('+'))
                model.PhoneNumber = string.Format("+{0}", model.PhoneNumber);

                var isPhoneValid = GeneralFunctions.IsPhoneNumberValid(model.PhoneNumber);
                if (!isPhoneValid)
                {
                    CreateResponse(null, (int)HttpStatusCode.BadRequest, "Phone number is invalid");

                    return _currentResponse;
                }

                model.Email = model.Email.Trim();
                var isEmailValid = GeneralFunctions.IsEmailValid(model.Email);
                if (!isEmailValid)
                {
                    CreateResponse(null, (int)HttpStatusCode.BadRequest, "Email address is invalid");

                    return _currentResponse;
                }

                if (string.IsNullOrEmpty(model.Password))
                {

                    CreateResponse(null, (int)HttpStatusCode.BadRequest, "Password is required");

                    return _currentResponse;
                }

                var isPhoneExist = _userRepository.IsPhoneExistBefore(model.PhoneNumber, null);
                if (isPhoneExist)
                {
                    CreateResponse(null, (int)HttpStatusCode.BadRequest, "Phone number is already exist please , login with your account");

                    return _currentResponse;
                }

                var isEmailExist = _userRepository.IsEmailExistBefore(model.Email, null);
                if (isEmailExist)
                {
                    CreateResponse(null, (int)HttpStatusCode.BadRequest, "Email is already exist please , login with your account ");

                    return _currentResponse;
                }

                var entity = _mapper.Map<User>(model);
                entity.Uuid = Guid.NewGuid().ToString();
                entity.LanguageId = (int)LanguagesEnum.English;
                entity.RoleId = (int)RolesEnum.Sourcer;
                entity.Active = true;
                entity.CreatedDate = DateTime.Now;
                entity.Password = GeneralFunctions.HashPassword(entity.Password);
                entity.Accepted = false;
                entity.UserStatusId = (int)UserStatusEnum.Request;
                entity.RegistrationStep = (int)SourcerRegistrationEnum.Simple_Form_Step;
                _userRepository.Add(entity);

                //Generate jwt Token                
                UserClaim userClaim = new UserClaim()
                {
                    UuId = entity.Uuid,
                    UserRole = entity.RoleId,
                    UserName = string.Format("{0} {1}", entity.FirstName, entity.LastName),
                    UserEmail = entity.Email,
                    PhoneNumer = entity.PhoneNumber,
                    Language = entity.LanguageId,
                };
                var token = _jwtTokenGenerator.generateJwtToken(userClaim);


                CreateResponse(new { uuid = entity.Uuid , token, userRole = entity.RoleId, userName = userClaim.UserName, userRegistrationStep = entity.RegistrationStep }, (int)HttpStatusCode.OK, "your data has been added successfully");

                return _currentResponse;
            }
            catch (System.Exception e)
            {
                LogException(e);

                CreateResponse(null, (int)HttpStatusCode.BadRequest, "An error occurred while added your data");

                return _currentResponse;
            }
        }

        public CurrentResponse UpdateSourcerCV(SourcerCVViewModel model)
        {
            var isUpdated = _sourcerRepository.UpdateSourcerCV(model.Uuid, model.CvName, model.LinkedInProfile);
            if (isUpdated)
            {
                var sourcer = _userRepository.GetUserByUuId(model.Uuid);
                var userName = string.Format("{0} {1}",sourcer.FirstName , sourcer.LastName);

                //send email to sourcer
                _emailTemplateService.SendCRVerificationEmail(2, userName,sourcer.Email, _appSettings.WebAppLink, model.Uuid);

                //send email to admin
                var admin = _userRepository.GetAdminInfo();
                _emailTemplateService.SendCRNewSourcerEmail(2, userName, admin.Email, _appSettings.WebAppLink);


                CreateResponse(new { uuid = _sourcerRepository.CurrentModel.User.Uuid, userRegistrationStep = sourcer.RegistrationStep }, (int)HttpStatusCode.OK, "Sourcer CV is updated successfully");
            }
            else
                CreateResponse(null, (int)HttpStatusCode.BadRequest, "Failed to update Sourcer CV");
            
            return _currentResponse;
        }


        public CurrentResponse GetSourcerExperienceId(string uuid)
        {
            var experienceId = _sourcerRepository.GetSourcerExperienceId(uuid);

            return CreateResponse(experienceId, (int)HttpStatusCode.OK,
                "data has been retrieved successfully");        
        }
        public CurrentResponse UpdateSourcerExperience(SourcerExperienceViewModel model)
        {
            var isUpdated = _sourcerRepository.UpdateSourcerExperience(model.Uuid, model.ExperienceTypeId);
            if (isUpdated)
            {
                var entity = _userRepository. GetUserByUuId(model.Uuid);
                CreateResponse(new { uuid = _sourcerRepository.CurrentModel.User.Uuid, userRegistrationStep = entity.RegistrationStep }, (int)HttpStatusCode.OK, "Sourcer Experience is updated successfully");
            }
            else
                CreateResponse(null, (int)HttpStatusCode.BadRequest, "Failed to update Sourcer Experience");
            
            return _currentResponse;
        }


        public CurrentResponse GetSourcerIndustryId(string uuid)
        {
            var industryId = _sourcerRepository.GetSourcerIndustryId(uuid);

            return CreateResponse(industryId, (int)HttpStatusCode.OK,
                "data has been retrieved successfully");
        }

        public CurrentResponse UpdateSourcerIndustry(SourcerIndustryViewModel model)
        {
            var isUpdated = _sourcerRepository.UpdateSourcerIndustry(model.Uuid, model.IndustryId);
            if (isUpdated)
            {
                var entity = _userRepository.GetUserByUuId(model.Uuid);
                CreateResponse(new { uuid = _sourcerRepository.CurrentModel.User.Uuid, userRegistrationStep = entity.RegistrationStep }, (int)HttpStatusCode.OK, "Sourcer Industry is updated successfully");
            }
            else
                CreateResponse(null, (int)HttpStatusCode.BadRequest, "Failed to update Sourcer Industry");
            
            
            return _currentResponse;
        }


        public CurrentResponse GetJobtitleName(string uuid)
        {
            var titleName = _sourcerRepository.GetJobtitleName(uuid);

            return CreateResponse(titleName, (int)HttpStatusCode.OK,
                "data has been retrieved successfully");
        }
        public CurrentResponse UpdateSourcerJobtitle(SourcerJobViewModel model)
        {
            var isUpdated = _sourcerRepository.UpdateSourcerJobtitle(model.Uuid, model.JobTitle);
            if (isUpdated)
            {
                var entity = _userRepository.GetUserByUuId(model.Uuid);
                CreateResponse(new { uuid = _sourcerRepository.CurrentModel.User.Uuid, userRegistrationStep = entity.RegistrationStep }, (int)HttpStatusCode.OK, "Sourcer Job is updated successfully");
            }
            else
            {
                CreateResponse(null, (int)HttpStatusCode.BadRequest, "Failed to update Sourcer Job");
            }
            return _currentResponse;
        }

        public CurrentResponse GetSourcerhasAccessToWebsite(string uuid)
        {
            var hasAccess = _sourcerRepository.GetSourcerhasAccessToWebsite(uuid);

            return CreateResponse(hasAccess, (int)HttpStatusCode.OK,
                "data has been retrieved successfully");
        }
        public CurrentResponse UpdateSourcerWebsites(SourcerWebsiteViewModel model)
        {
            var isUpdated = _sourcerRepository.UpdateSourcerWebsites(model.Uuid, model.HasAccessToDifferentWebsite);
            if (isUpdated)
            {
                var entity = _userRepository.GetUserByUuId(model.Uuid);
                CreateResponse(new { uuid = _sourcerRepository.CurrentModel.User.Uuid, userRegistrationStep = entity.RegistrationStep }, (int)HttpStatusCode.OK, "Sourcer Website is updated successfully");
            }
            else
                CreateResponse(null, (int)HttpStatusCode.BadRequest, "Failed to update Sourcer Website");
            
            return _currentResponse;
        }

        // Sourcer acceptance
        public CurrentResponse GetAllSourcers(int userStatusId)
        {
            var sourcers = _sourcerRepository.GetAllSourcers(userStatusId);
            if (sourcers != null)
                CreateResponse(sourcers, (int)HttpStatusCode.OK, "Data is retrieved successfully");

            else
                CreateResponse(null, (int)HttpStatusCode.BadRequest, "Failed to retrieve data");

            return _currentResponse;
        }

        public CurrentResponse GetAllSourcerInfo(string uuid)
        {
            if (string.IsNullOrEmpty(uuid))
            {
                _currentResponse.message = "Uuid is required";
                _currentResponse.status = (int)HttpStatusCode.BadRequest;
                return _currentResponse;
            }

            var sourcer = _sourcerRepository.GetAllSourcerInfo(uuid);
            if (sourcer != null)
                CreateResponse(sourcer, (int)HttpStatusCode.OK, "Sourcer details has been got successfully");

            else
                CreateResponse(null, (int)HttpStatusCode.BadRequest, "Failed to get sourcer details");

            return _currentResponse;
        }

        public CurrentResponse AcceptSourcer(string uuid)
        {
            if (string.IsNullOrEmpty(uuid))
            {
                CreateResponse(false, (int)HttpStatusCode.BadRequest, "Uuid is required");
                return _currentResponse;
            }

            var isAccepted = _sourcerRepository.AcceptSourcer(uuid);

            if (isAccepted)
            {
                var sourcer = _userRepository.GetUserByUuId(uuid);
                var userName = string.Format("{0} {1}", sourcer.FirstName, sourcer.LastName);

                // send acceptance mail

                _emailTemplateService.SendCRSourcerAcceptanceEmail(2, userName, sourcer.Email, _appSettings.WebAppLink);

                CreateResponse(isAccepted, (int)HttpStatusCode.OK, "Sourcer has been accepted successfully");
            }

            else
                CreateResponse(false, (int)HttpStatusCode.BadRequest, "Failed to accept sourcer");

            return _currentResponse;
        }

        public CurrentResponse DeclineSourcer(UserRequestViewModel userRequestViewModel)
        {
            if (string.IsNullOrEmpty(userRequestViewModel.uuid))
            {
                CreateResponse(false, (int)HttpStatusCode.BadRequest, "Uuid is required");
                return _currentResponse;
            }

            var isDeclined = _sourcerRepository.DeclineSourcer(userRequestViewModel.uuid,userRequestViewModel.rejectedReason);

            if (isDeclined)
            {
                var sourcer = _userRepository.GetUserByUuId(userRequestViewModel.uuid);
                var userName = string.Format("{0} {1}", sourcer.FirstName, sourcer.LastName);
                _emailTemplateService.SendCRRejectionEmail(2, userName, sourcer.RejectedReason, sourcer.Email, _appSettings.WebAppLink);

                CreateResponse(isDeclined, (int)HttpStatusCode.OK, "Sourcer has been declined ");
            }

            else
                CreateResponse(false, (int)HttpStatusCode.BadRequest, "Failed to decline sourcer");

            return _currentResponse;
        }

        public CurrentResponse GetSourcerBalances(string sourcerUuid)
        {
            try
            {
                var sourcersBalances = _sourcerRepository.GetSourcerBalances(sourcerUuid);
                var sourcersBalancesSummary = _sourcerRepository.GetSourcerBalancesSummary(sourcerUuid);

                CreateResponse(new { sourcersBalances , sourcersBalancesSummary }, (int)HttpStatusCode.OK, "data is retreived Successfully!");

                return _currentResponse;
            }
            catch (System.Exception e)
            {
                LogException(e);
                return null;
            }
        }

        //public CurrentResponse GetSourcerBalancesSummary(string sourcerUuid)
        //{
        //    try
        //    {
        //        var sourcers = _sourcerRepository.GetSourcerBalancesSummary(sourcerUuid);

        //        CreateResponse(sourcers, (int)HttpStatusCode.OK, "data is retreived Successfully!");

        //        return _currentResponse;
        //    }
        //    catch (System.Exception e)
        //    {
        //        LogException(e);
        //        return null;
        //    }
        //}

        #endregion
    }
}
