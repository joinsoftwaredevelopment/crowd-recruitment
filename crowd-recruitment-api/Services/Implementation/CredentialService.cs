﻿using Data.Tables;
using Helpers.Encryptors;
using Repositories.Interfaces;
using Services.Interfaces;
using Services.ViewModels;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Linq.Expressions;
using System.Text;

namespace Services.Implementation
{
    public class CredentialService : BaseService<Credential,JwtApiContract>, ICredentialService
    {
        #region Fields
        private readonly ICredentialRepository _credentialRepository;
        private readonly ICipherService _cipherService;
        #endregion

        #region Constructors
        public CredentialService(ICredentialRepository credentialRepository, ICipherService cipherService) : base(credentialRepository)
        {
            _credentialRepository = credentialRepository;
            _cipherService = cipherService;
        }

        #endregion

        #region Methods
        public JwtApiContract GetJwtCredentials()
        {
            try
            {
                var jwtCredentials = _credentialRepository.GetCredentials();

                if (jwtCredentials == null)
                    return null;

                //Encrypt data
                //var jwtissuer = _cipherService.Encrypt("https://www.join.sa/");
                //Console.WriteLine("jwtissuer=================|" + jwtissuer);

                //var jwtAudence = _cipherService.Encrypt("https://www.join.sa/");
                //Console.WriteLine("jwtAudence=================|" + jwtAudence);

                //var jwtExpiration = _cipherService.Encrypt("5");
                //Console.WriteLine("jwtExpiration=================|" + jwtExpiration);

                //var jwtSecretKey = _cipherService.Encrypt("croud-recruitment-key|");
                //Console.WriteLine("jwtSecretKey=================" + jwtSecretKey);

                var jwt = new JwtApiContract();

                //jwt.Audience = _cipherService.Decrypt(jwtCredentials.JwtAudience);
                //jwt.Issuer = _cipherService.Decrypt(jwtCredentials.JwtIssuer);
                //jwt.ExpriationTime = TimeSpan.Parse(_cipherService.Decrypt(jwtCredentials.JwtExpriationTime), CultureInfo.InvariantCulture);
                //jwt.SecretKey = _cipherService.Decrypt(jwtCredentials.JwtSecretKey);
                //jwt.EncryptKey = jwtCredentials.JwtSecretKey;

                jwt.Audience = "https://www.join.sa/";
                jwt.Issuer = "https://www.join.sa/";
                jwt.ExpriationTime = new TimeSpan(1,0,0);
                jwt.SecretKey = "croud-recruitment-key|";
                jwt.EncryptKey = jwtCredentials.JwtSecretKey;

                return jwt;
                //    new JwtApiContract
                //{
                //    Audience = _cipherService.Decrypt(jwtCredentials.JwtAudience),
                //    Issuer = _cipherService.Decrypt(jwtCredentials.JwtIssuer),
                //    //ExpriationTime = TimeSpan.Parse(_cipherService.Decrypt(jwtCredentials.JwtExpriationTime), CultureInfo.InvariantCulture),
                //    SecretKey = _cipherService.Decrypt(jwtCredentials.JwtSecretKey),
                //    EncryptKey = jwtCredentials.JwtSecretKey
                //};
            }
            catch (System.Exception e)
            {
                LogException(e);
                return null;
            }
        }


        #endregion
    }

}
