﻿using Repositories.Interfaces;
using Repositories.Enums;
using Services.Interfaces;
using Services.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;

namespace Services.Implementation
{
    public class BaseService<TEntity,TModel> : IBaseService<TEntity,TModel> where TEntity : class where TModel : class, new()
    {

        #region Fields

        private readonly IBaseRepository<TEntity> _baseRepository;
        public CurrentResponse _currentResponse;


        #endregion

        #region Constructors

        public BaseService(IBaseRepository<TEntity> baseRepository)
        {
            _baseRepository = baseRepository;
            _currentResponse = new CurrentResponse();
        }
         
        #endregion


        #region Methods

        public int LogException(Exception exception)
        {
            return _baseRepository.LogException(exception);
        }

        public CurrentResponse CreateResponse(object data, int statusCode, string message)
        {
            _currentResponse.data = data;
            _currentResponse.status = statusCode;
            _currentResponse.message = message;

            return _currentResponse;
        }

        #endregion

    }
}
