﻿using AutoMapper;
using Data.Tables;
using Data.Views;
using Helpers;
using Microsoft.Extensions.Options;
using Repositories.Enums;
using Repositories.Interfaces;
using Services.Common.Jwt.Claims;
using Services.Interfaces;
using Services.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;

namespace Services.Implementation
{
    public class UserService : BaseService<User, LogInViewModel>, IUserService
    {
        #region Fields
        private readonly IUserRepository _userRepository;
        private readonly IMapper _mapper;
        private readonly IJwtTokenGenerator _jwtTokenGenerator;
        private readonly IEmailTemplateService _emailTemplateService;
        private readonly AppSettings _appSettings;

        #endregion

        #region Constructors
        public UserService(IMapper mapper, IUserRepository userRepository,
            IJwtTokenGenerator jwtTokenGenerator, IEmailTemplateService emailTemplateService, IOptions<AppSettings> appSettings) : base(userRepository)
        {
            _userRepository = userRepository;
            _mapper = mapper;
            _jwtTokenGenerator = jwtTokenGenerator;
            _emailTemplateService = emailTemplateService;
            _appSettings = appSettings.Value;
        }

        #endregion

        #region Methods
        public CurrentResponse LogIn(LogInViewModel logInViewModel)
        {
            try
            {
                //Validation 
                if (string.IsNullOrEmpty(logInViewModel.Email))
                {
                    _currentResponse.message = "Email is required";
                    _currentResponse.status = (int)HttpStatusCode.BadRequest;
                    return _currentResponse;
                }

                if (string.IsNullOrEmpty(logInViewModel.Password))
                {
                    _currentResponse.message = "Password is required";
                    _currentResponse.status = (int)HttpStatusCode.BadRequest;
                    return _currentResponse;
                }


                var entity = _mapper.Map<User>(logInViewModel);

                var currentUser = _userRepository.LogIn(entity.Email.Trim(), entity.RoleId);

                if (currentUser != null)
                {
                    // validate password
                    if (GeneralFunctions.CheckMatch(currentUser.Password, entity.Password))
                    {
                        if (currentUser.IsEmailVerified != true)
                        {
                            return CreateResponse(null, (int)HttpStatusCode.BadRequest, "Your Email isn't verified  , please verify your email first .");
                        }

                        if (currentUser.Accepted != true)
                        {
                            if ((currentUser.RegistrationStep == (int)SourcerRegistrationEnum.Attach_CV_Step && currentUser.RoleId == (int)RolesEnum.Sourcer))
                            {
                                CreateResponse(null, (int)HttpStatusCode.BadRequest, "Admin is reviewing your application now , you will recieve an email when you are approved!");
                            }
                            else
                            {
                                //Generate jwt Token                
                                UserClaim userClaim = new UserClaim()
                                {
                                    UuId = currentUser.Uuid,
                                    UserRole = currentUser.RoleId,
                                    UserName = string.Format("{0} {1}", currentUser.FirstName, currentUser.LastName),
                                    UserEmail = currentUser.Email,
                                    PhoneNumer = currentUser.PhoneNumber,
                                    Language = currentUser.LanguageId,
                                };
                                var token = _jwtTokenGenerator.generateJwtToken(userClaim);

                                CreateResponse(new { uuid = currentUser.Uuid, token, userRole = currentUser.RoleId, userName = userClaim.UserName, userRegistrationStep = currentUser.RegistrationStep }, (int)HttpStatusCode.OK, "You can access your account now");

                            }

                            return _currentResponse;
                        }
                        // validate password
                        if (GeneralFunctions.CheckMatch(currentUser.Password, entity.Password))
                        {
                            // make response view model
                            //_currentResponse.data = new UserResponse { PhoneNumber = entity.PhoneNumber };
                            //_currentResponse.message = "User name and password is correct";
                            //_currentResponse.status = (int)HttpStatusCode.OK;

                            //Generate jwt Token                
                            UserClaim userClaim = new UserClaim()
                            {
                                UuId = currentUser.Uuid,
                                UserRole = currentUser.RoleId,
                                UserName = string.Format("{0} {1}", currentUser.FirstName, currentUser.LastName),
                                UserEmail = currentUser.Email,
                                PhoneNumer = currentUser.PhoneNumber,
                                Language = currentUser.LanguageId,
                            };

                            var tokenString = _jwtTokenGenerator.generateJwtToken(userClaim);
                            //var tokenString = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJVdUlkIjoiOTgwZjYyYzktZWVlOC00MmUwLWEzMWYtYTNkYmY2Y2E0NDgxIiwiVXNlck5hbWUiOiJaZFNkIGFzZGFzZCIsIlVzZXJFbWFpbCI6Im5haGxhMzRAam9pbi5zYSIsIlVzZXJSb2xlIjoiMyIsIkxhbmd1YWdlIjoiMiIsIkltYWdlTmFtZSI6IiIsIlBheUxvYWQiOiJ7fSIsIm5iZiI6MTYxMjAyODAyOCwiZXhwIjoxNjEyNDYwMDI4LCJpYXQiOjE2MTIwMjgwMjgsImlzcyI6Imh0dHBzOi8vd3d3LmpvaW4uc2EvIiwiYXVkIjoiaHR0cHM6Ly93d3cuam9pbi5zYS8ifQ.BhR7G723Ps-WEuINN9EeDzJnXniLX_-2gduOfhYfEsQ";

                            CreateResponse(new { uuid = currentUser.Uuid, token = tokenString, userRole = currentUser.RoleId, userName = userClaim.UserName, userRegistrationStep = currentUser.RegistrationStep }, (int)HttpStatusCode.OK, "You can access your data Now!");

                        }

                        else
                        {
                            _currentResponse.message = "Invalid email or password";
                            _currentResponse.status = (int)HttpStatusCode.BadRequest;
                        }
                    }

                    else
                    {
                        _currentResponse.message = "Invalid email or password";
                        _currentResponse.status = (int)HttpStatusCode.BadRequest;
                    }

                }



                else
                {
                    _currentResponse.message = "Invalid email or password";
                    _currentResponse.status = (int)HttpStatusCode.BadRequest;
                }

                return _currentResponse;
            }
            catch (System.Exception e)
            {
                LogException(e);

                _currentResponse.message = "An error occurred while logging in";
                _currentResponse.status = (int)HttpStatusCode.BadRequest;

                return _currentResponse;
            }
        }

        public CurrentResponse GetUserByUuId(string uuId)
        {
            try
            {
                var user = _userRepository.GetUserByUuId(uuId);

                CreateResponse(new UserViewModel { uuId = user.Uuid, Email = user.Email }, (int)HttpStatusCode.OK, "your data has been retrieved successfully");

                return _currentResponse;
            }
            catch (System.Exception e)
            {
                LogException(e);

                CreateResponse(null, (int)HttpStatusCode.BadRequest, "An error occurred while Get User Info");

                return _currentResponse;
            }
        }

        public CurrentResponse BookMeeting(BookMeeting bookMeeting)
        {
            try
            {
                var isAdded = _userRepository.BookMeeting(bookMeeting);

                if (isAdded)
                {
                    CreateResponse(null, (int)HttpStatusCode.OK, "your data has been added successfully");

                    var user = _userRepository.GetUserByUuId(bookMeeting.uuid);
                    var userName = string.Format("{0} {1}", user.FirstName, user.LastName);

                    var meetingdetails = GetMeetingInfo(bookMeeting.uuid);
                    //send email to user
                    _emailTemplateService.SendCRZoomEmail(2, userName, user.Email, string.Empty, meetingdetails.zoomLink, meetingdetails.interviewerName, meetingdetails.mettingDate, meetingdetails.timePeriod);

                    //send email to interviewer
                    _emailTemplateService.SendCRZoomEmail(2, userName, meetingdetails.email, string.Empty, meetingdetails.zoomLink, meetingdetails.interviewerName, meetingdetails.mettingDate, meetingdetails.timePeriod);
                }
                else
                    CreateResponse(null, (int)HttpStatusCode.BadRequest, "An error occurred while adding your data");

                return _currentResponse;
            }
            catch (System.Exception e)
            {
                LogException(e);

                CreateResponse(null, (int)HttpStatusCode.BadRequest, "An error occurred while Get User Info");

                return _currentResponse;
            }
        }

        public CurrentResponse GetAdminInfo()
        {
            try
            {
                var admin = _userRepository.GetAdminInfo();

                CreateResponse(admin, (int)HttpStatusCode.OK, "your data has been retrieved successfully");

                return _currentResponse;
            }
            catch (System.Exception e)
            {
                LogException(e);

                CreateResponse(null, (int)HttpStatusCode.BadRequest, "An error occurred while Get User Info");

                return _currentResponse;
            }
        }

        public BookMeetingViewModel GetMeetingInfo(string uuId)
        {
            var user = _userRepository.GetUserByUuId(uuId);
            var interviewer = _userRepository.GetInterviewerById(Convert.ToInt32(user.InterviewerId));
            var timePeriod = GetTimePeriod(Convert.ToInt32(user.MeetingTimeId));
            var bookAMeeting = new BookMeetingViewModel()
            {
                interviewerName = interviewer != null ? interviewer.InterviewerName : string.Empty,
                mettingDate = user.MeetingDate.Value.Date.ToShortDateString(),
                timePeriod = timePeriod != null ? timePeriod.NameEn : string.Empty,
                zoomLink = interviewer != null ? interviewer.ZoomLink : string.Empty,
                email = interviewer != null ? interviewer.Email : string.Empty,
                userName = string.Format("{0} {1}", user.FirstName, user.LastName)
            };

            return bookAMeeting;
        }

        public BasicModel GetTimePeriod(int timePeriodId)
        {
            var timePeriodList = new List<BasicModel>();
            timePeriodList.Add(new BasicModel() { Id = 1, NameEn = "09:00 AM" });
            timePeriodList.Add(new BasicModel() { Id = 2, NameEn = "10:00 AM" });
            timePeriodList.Add(new BasicModel() { Id = 3, NameEn = "11:00 AM" });
            timePeriodList.Add(new BasicModel() { Id = 4, NameEn = "12:00 PM" });
            timePeriodList.Add(new BasicModel() { Id = 5, NameEn = "01:00 PM" });

            return timePeriodList.FirstOrDefault(o => o.Id == timePeriodId);
        }

        public CurrentResponse GetAllCountries(int languageId)
        {
            try
            {
                var countries = _userRepository.GetAllCountries(languageId);

                CreateResponse(countries, (int)HttpStatusCode.OK, "your data has been retrieved successfully");

                return _currentResponse;
            }
            catch (System.Exception e)
            {
                LogException(e);

                CreateResponse(null, (int)HttpStatusCode.BadRequest, "An error occurred while Get User Info");

                return _currentResponse;
            }
        }

        public CurrentResponse GetAllCurrencies(int languageId)
        {
            try
            {
                var currencies = _userRepository.GetAllCurrencies(languageId);

                CreateResponse(currencies, (int)HttpStatusCode.OK, "your data has been retrieved successfully");

                return _currentResponse;
            }
            catch (System.Exception e)
            {
                LogException(e);

                CreateResponse(null, (int)HttpStatusCode.BadRequest, "An error occurred while Get User Info");

                return _currentResponse;
            }
        }

        public CurrentResponse SendResetEmail(string email)
        {
            try
            {
                var userDetails = _userRepository.GetUserByEmail(email);
                if (userDetails != null)
                {
                    string appLink = _appSettings.WebAppLink;
                    string resetLink = appLink + "employercreatepassword/" + userDetails.Uuid;
                    // send Email

                    if (_emailTemplateService.SendResetTemplate(email, resetLink, (int)LanguagesEnum.English))
                        CreateResponse(userDetails, (int)HttpStatusCode.OK, "Email has been sent successfully! visit your email to reset your password");
                    else
                        CreateResponse(null, (int)HttpStatusCode.BadRequest, "Failed to send reset email");


                }

                else
                {
                    CreateResponse(null, (int)HttpStatusCode.BadRequest, "No account related to that email, add another email or register a new account");
                }


                return _currentResponse;
            }
            catch (System.Exception e)
            {
                LogException(e);

                CreateResponse(null, (int)HttpStatusCode.BadRequest, "failed to send email");

                return _currentResponse;
            }
        }

        public CurrentResponse ResePassword(ResetPasswordViewModel resetPasswordViewModel)
        {
            try
            {
                var fullHashedPass = GeneralFunctions.HashPassword(resetPasswordViewModel.Password);
                if (string.IsNullOrEmpty(fullHashedPass))
                {
                    CreateResponse(null, (int)HttpStatusCode.BadRequest, "Failed to reset password");
                }

                if (_userRepository.ResePassword(fullHashedPass, resetPasswordViewModel.Uuid))
                    CreateResponse(null, (int)HttpStatusCode.OK, "your password has been reset successfully");

                else
                    CreateResponse(null, (int)HttpStatusCode.BadRequest, "Failed to reset password");


                return _currentResponse;
            }
            catch (System.Exception e)
            {
                LogException(e);

                CreateResponse(null, (int)HttpStatusCode.BadRequest, "Failed to reset password");

                return _currentResponse;
            }
        }

        public CurrentResponse UpdatePassword(UpdatePasswordViewModel updatePasswordViewModel)
        {
            try
            {
                if (updatePasswordViewModel.password == updatePasswordViewModel.newPassword)
                {
                    CreateResponse(null, (int)HttpStatusCode.BadRequest, "New password should be different than old password");
                    return _currentResponse;
                }
                var currentUser = _userRepository.GetUserByUuId(updatePasswordViewModel.uuid);


                if (currentUser != null)
                {
                    if (GeneralFunctions.CheckMatch(currentUser.Password, updatePasswordViewModel.password))
                    {
                        var fullHashedPass = GeneralFunctions.HashPassword(updatePasswordViewModel.newPassword);
                        if (string.IsNullOrEmpty(fullHashedPass))
                        {
                            _currentResponse.message = "Failed to hash password";
                            _currentResponse.status = (int)HttpStatusCode.BadRequest;

                            return _currentResponse;
                        }

                        updatePasswordViewModel.newPassword = fullHashedPass;
                        if (_userRepository.ResePassword(updatePasswordViewModel.newPassword, updatePasswordViewModel.uuid))
                        {
                            CreateResponse(null, (int)HttpStatusCode.OK, "Your password has been updated successfully");
                            return _currentResponse;
                        }

                        else
                        {
                            CreateResponse(null, (int)HttpStatusCode.BadRequest, "failed to update password");
                            return _currentResponse;
                        }
                    }

                    else
                    {
                        CreateResponse(null, (int)HttpStatusCode.BadRequest, "Old password is incorrect");
                        return _currentResponse;
                    }
                }

                else
                    CreateResponse(null, (int)HttpStatusCode.BadRequest, "You don't have access to change password");


                return _currentResponse;
            }
            catch (System.Exception e)
            {
                LogException(e);

                CreateResponse(null, (int)HttpStatusCode.BadRequest, "Failed to update password");

                return _currentResponse;
            }
        }

        public CurrentResponse VerifyUserEmailByUuId(string uuId)
        {
            try
            {
                var currentUser = _userRepository.VerifyUserEmailByUuId(uuId);

                CreateResponse(new { email = currentUser.Email }, (int)HttpStatusCode.OK, "You are registered successfully");

                return _currentResponse;
            }
            catch (System.Exception e)
            {
                LogException(e);

                CreateResponse(null, (int)HttpStatusCode.BadRequest, "Failed to register");

                return _currentResponse;
            }
        }

        public CurrentResponse ResendEmail(string uuId, int languageId)
        {
            var currentUser = _userRepository.GetUserByUuId(uuId);
            var userName = string.Format("{0} {1}", currentUser.FirstName, currentUser.LastName);
            _emailTemplateService.SendCRVerificationEmail(languageId, userName, currentUser.Email, _appSettings.WebAppLink, uuId);

            return CreateResponse(null, (int)HttpStatusCode.OK, "Email is sent Successfully");

        }

        public CurrentResponse ChangeEmail(string uuId, string email, int languageId)
        {
            if (!_userRepository.IsEmailExistBefore(email, uuId))
            {
                var isChanged = _userRepository.ChangeEmail(uuId, email);
                if (isChanged)
                {
                    ResendEmail(uuId, languageId);
                    return CreateResponse(null, (int)HttpStatusCode.OK, "Email is changed Successfully");
                }
                else
                    return CreateResponse(null, (int)HttpStatusCode.BadRequest, "Failed to change Email");

            }
            else
                return CreateResponse(null, (int)HttpStatusCode.BadRequest, "This email is already exist , try to login with this email");

        }

        public bool IsEmployerMembershipActive(int employeerId)
        {
            return _userRepository.IsEmployerMembershipActive(employeerId);
        }

        #endregion
    }
}
