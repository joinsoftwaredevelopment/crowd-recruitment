﻿using System;
using System.Collections.Generic;
using System.Text;
using AutoMapper;
using Data.Tables;
using Data.Views;
using Repositories.Interfaces;
using Repositories.Enums;
using Services.Interfaces;
using Services.ViewModels;
using Helpers;
using System.Net;
using Microsoft.Extensions.Options;

namespace Services.Implementation
{
    public class EmployerService : BaseService<EmployerDetail, EmployerDetailsViewModel>, IEmployerService
    {
        #region Fields
        private readonly IEmployerRepository _employerRepository;
        private readonly IUserRepository _userRepository;
        private readonly IMapper _mapper;
        private readonly IEmailTemplateService _emailTemplateService;
        private readonly AppSettings _appSettings;
        #endregion

        #region Constructors
        public EmployerService(IEmployerRepository employerRepository, IMapper mapper
            , IUserRepository userRepository, IEmailTemplateService emailTemplateService, IOptions<AppSettings> appSettings) : base(employerRepository)
        {
            _employerRepository = employerRepository;
            _mapper = mapper;
            _userRepository = userRepository;
            _emailTemplateService = emailTemplateService;
            _appSettings = appSettings.Value;
        }

        #endregion

        #region Methods
        public CurrentResponse AddEmployer(EmployerDetailsViewModel employerDetailsViewModel)
        {
            try
            {
                //Validation 
                if (!employerDetailsViewModel.PhoneNumber.Contains('+'))
                    employerDetailsViewModel.PhoneNumber = string.Format("+{0}",employerDetailsViewModel.PhoneNumber);
                if (string.IsNullOrEmpty(employerDetailsViewModel.CompanyName))
                {
                    _currentResponse.message = "CompanyName Name is required";
                    _currentResponse.status = (int)HttpStatusCode.BadRequest;
                    return _currentResponse;
                }

                if (string.IsNullOrEmpty(employerDetailsViewModel.FirstName))
                {
                    _currentResponse.message = "First Name is required";
                    _currentResponse.status = (int)HttpStatusCode.BadRequest;
                    return _currentResponse;
                }

                if (string.IsNullOrEmpty(employerDetailsViewModel.LastName))
                {
                    _currentResponse.message = "Last Name is required";
                    _currentResponse.status = (int)HttpStatusCode.BadRequest;
                    return _currentResponse;
                }

                employerDetailsViewModel.Email = employerDetailsViewModel.Email.Trim();
                var isEmailValid = GeneralFunctions.IsEmailValid(employerDetailsViewModel.Email);
                if (!isEmailValid)
                {
                    _currentResponse.message = "Email address is invalid";
                    _currentResponse.status = (int)HttpStatusCode.BadRequest;
                    return _currentResponse;
                }

                var isPhoneValid = GeneralFunctions.IsPhoneNumberValid(employerDetailsViewModel.PhoneNumber);
                if (!isPhoneValid)
                {
                    _currentResponse.message = "Phone number is invalid";
                    _currentResponse.status = (int)HttpStatusCode.BadRequest;
                    return _currentResponse;
                }

                if (string.IsNullOrEmpty(employerDetailsViewModel.Password))
                {
                    _currentResponse.message = "Password is required";
                    _currentResponse.status = (int)HttpStatusCode.BadRequest;
                    return _currentResponse;
                }

                if (_userRepository.IsPhoneExistBefore(employerDetailsViewModel.PhoneNumber, null))
                {
                    _currentResponse.message = "Phone is exists! choose another one.";
                    _currentResponse.status = (int)HttpStatusCode.BadRequest;
                    return _currentResponse;
                }

                if (_userRepository.IsEmailExistBefore(employerDetailsViewModel.Email, null))
                {
                    _currentResponse.message = "Email address is exists! choose another one.";
                    _currentResponse.status = (int)HttpStatusCode.BadRequest;
                    return _currentResponse;
                }

                var entity = _mapper.Map<EmployerDetails>(employerDetailsViewModel);

                entity.User.Uuid = Guid.NewGuid().ToString();
                entity.User.Active = true;
                entity.User.Accepted = false;
                entity.User.CreatedDate = DateTime.Now;
                entity.User.IsEmailVerified = false;
                entity.User.RoleId = (int)RolesEnum.Employer;
                entity.User.LanguageId = (int) LanguagesEnum.English;
                entity.User.UserStatusId = (int)UserStatusEnum.Request;
                entity.User.RegistrationStep = (int)SourcerRegistrationEnum.Simple_Form_Step;

                // encrypt password here
                var fullHashedPass = GeneralFunctions.HashPassword(entity.User.Password);
                if (string.IsNullOrEmpty(fullHashedPass))
                {
                    _currentResponse.message = "Failed to hash password";
                    _currentResponse.status = (int)HttpStatusCode.BadRequest;

                    return _currentResponse;
                }

                entity.User.Password = fullHashedPass;
                
                // adding
                _employerRepository.AddEmployer(entity);


                // make response view model
                _currentResponse.data = new { UuId = entity.User.Uuid , email = entity.User.Email };
                _currentResponse.message ="your data has been added successfully";
                _currentResponse.status = (int)HttpStatusCode.OK;

                //sending verfication email
                var employer = _userRepository.GetUserByUuId(entity.User.Uuid);
                var userName = string.Format("{0} {1}", employer.FirstName, employer.LastName);

                //send email to employer
                _emailTemplateService.SendCRVerificationEmail(2, userName, employer.Email,_appSettings.WebAppLink, entity.User.Uuid);

                //send email to admin
                var admin = _userRepository.GetAdminInfo();
                _emailTemplateService.SendCRNewEmployerEmail(2, userName, admin.Email, _appSettings.WebAppLink);

                return _currentResponse;
            }
            catch (System.Exception e)
            {
                LogException(e);

                _currentResponse.message = "An error occurred while added your data";
                _currentResponse.status = (int)HttpStatusCode.BadRequest;

                return _currentResponse;
            }
        }

        public CurrentResponse GetAllEmployers(int userStatusId)
        {
            var employers = _employerRepository.GetAllEmployers(userStatusId);
            if (employers != null)
                CreateResponse(employers , (int)HttpStatusCode.OK, "Employer details has been got successfully");
            
            else
                CreateResponse(null, (int)HttpStatusCode.BadRequest, "Failed to get employers details");
            
            return _currentResponse;
        }

        public CurrentResponse AcceptEmployer(string Uuid)
        {
            if (string.IsNullOrEmpty(Uuid))
            {
                CreateResponse(false, (int)HttpStatusCode.BadRequest, "Uuid is required");
                return _currentResponse;
            }

            var user = _employerRepository.AcceptEmployer(Uuid);
            if (user != null)
            {
                // send acceptance mail
                //MailsManager.SendMail(user.Email, "Recruitment - Acceptance email"
                //    , "Congratulations! you have been accepted to join recruitment as an employer");

                var employer = _userRepository.GetUserByUuId(Uuid);
                var userName = string.Format("{0} {1}", employer.FirstName, employer.LastName);
                _emailTemplateService.SendCREmployerAcceptanceEmail(2, userName, employer.Email, _appSettings.WebAppLink);

                CreateResponse(user, (int)HttpStatusCode.OK, "Employer has been accepted successfully");
            }

            else
                CreateResponse(null, (int)HttpStatusCode.BadRequest, "Failed to accept employer");
           
            return _currentResponse;
        }

        public CurrentResponse DeclineEmployer(UserRequestViewModel userRequestViewModel)
        {
            if (string.IsNullOrEmpty(userRequestViewModel.uuid))
            {
                CreateResponse(false, (int)HttpStatusCode.BadRequest, "Uuid is required");
                return _currentResponse;
            }

            var isDeclined = _employerRepository.DeclineEmployer(userRequestViewModel.uuid,userRequestViewModel.rejectedReason);

            if (isDeclined)
            {
                var sourcer = _userRepository.GetUserByUuId(userRequestViewModel.uuid);
                var userName = string.Format("{0} {1}", sourcer.FirstName, sourcer.LastName);
                _emailTemplateService.SendCRRejectionEmail(2, userName,  sourcer.RejectedReason, sourcer.Email, _appSettings.WebAppLink);

                CreateResponse(isDeclined, (int)HttpStatusCode.OK, "Sourcer has been declined ");
            }
            else
                CreateResponse(false, (int)HttpStatusCode.BadRequest, "Failed to decline sourcer");

            return _currentResponse;
        }

        #endregion
    }
}
