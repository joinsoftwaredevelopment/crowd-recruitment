﻿using AutoMapper;
using Data.Tables;
using Helpers;
using Repositories.Interfaces;
using Services.Common.Jwt.Claims;
using Services.Interfaces;
using Services.ViewModels;
using System;
using System.Net;

namespace Services.Implementation
{
    public class AdminService : BaseService<User, LogInViewModel>, IAdminService
    {
        #region Fields
        private readonly IAdminRepository _adminRepository;
        private readonly IMapper _mapper;
        private readonly IEmailTemplateService _emailTemplateService;
        private readonly IJwtTokenGenerator _jwtTokenGenerator;
        #endregion

        #region Constructors
        public AdminService(IMapper mapper
            , IAdminRepository adminRepository, IEmailTemplateService emailTemplateService
            , IJwtTokenGenerator jwtTokenGenerator) : base(adminRepository)
        {
            _adminRepository = adminRepository;
            _mapper = mapper;
            _emailTemplateService = emailTemplateService;
            _jwtTokenGenerator = jwtTokenGenerator;
        }

        #endregion

        #region Methods
        public CurrentResponse LogIn(LogInViewModel logInViewModel)
        {
            try
            {
                //Validation 
                if (string.IsNullOrEmpty(logInViewModel.Email))
                {
                    _currentResponse.message = "Email is required";
                    _currentResponse.status = (int)HttpStatusCode.BadRequest;
                    return _currentResponse;
                }

                if (string.IsNullOrEmpty(logInViewModel.Password))
                {
                    _currentResponse.message = "Password is required";
                    _currentResponse.status = (int)HttpStatusCode.BadRequest;
                    return _currentResponse;
                }


                var entity = _mapper.Map<User>(logInViewModel);

                var currentUser = _adminRepository.LogIn(entity.Email.Trim());

                if (currentUser != null)
                {
                    // validate password
                    if (GeneralFunctions.CheckMatch(currentUser.Password, entity.Password))
                    {
                        // make response view model
                        _currentResponse.data = new UserResponse { PhoneNumber = entity.PhoneNumber };
                        _currentResponse.message = "User name and password is correct";
                        _currentResponse.status = (int)HttpStatusCode.OK;

                        //Generate jwt Token                
                        UserClaim userClaim = new UserClaim()
                        {
                            UuId = currentUser.Uuid,
                            UserRole = currentUser.RoleId,
                            UserName = string.Format("{0} {1}", currentUser.FirstName, currentUser.LastName),
                            UserEmail = currentUser.Email,
                            PhoneNumer = currentUser.PhoneNumber,
                            Language = currentUser.LanguageId,
                        };

                        var tokenString = _jwtTokenGenerator.generateJwtToken(userClaim);

                        CreateResponse(new { uuid = currentUser.Uuid, token = tokenString, userRole = currentUser.RoleId, userName = userClaim.UserName }, (int)HttpStatusCode.OK, "You can access your data Now!");

                    }

                    else
                    {
                        _currentResponse.message = "Invalid email or password";
                        _currentResponse.status = (int)HttpStatusCode.BadRequest;
                    }
                }

                else
                {
                    _currentResponse.message = "Invalid email or password";
                    _currentResponse.status = (int)HttpStatusCode.BadRequest;
                }

                return _currentResponse;
            }
            catch (System.Exception e)
            {
                LogException(e);

                _currentResponse.message = "An error occurred while logging in";
                _currentResponse.status = (int)HttpStatusCode.BadRequest;

                return _currentResponse;
            }
        }

        public CurrentResponse GetVwAdminSourcersBalance()
        {
            try
            {
                var sourcers = _adminRepository.GetVwAdminSourcersBalance();

                CreateResponse(sourcers, (int)HttpStatusCode.OK, "data is retreived Successfully!");

                return _currentResponse;
            }
            catch (System.Exception e)
            {
                LogException(e);
                return null;
            }
        }

        public CurrentResponse GetJobSourcers(string jobUuId)
        {
            try
            {
                var sourcers = _adminRepository.GetJobSourcers(jobUuId);

                CreateResponse(sourcers, (int)HttpStatusCode.OK, "data is retreived Successfully!");

                return _currentResponse;
            }
            catch (System.Exception e)
            {
                LogException(e);
                return null;
            }
        }


        CurrentResponse IAdminService.GetVwAdminSourcersBalanceByWeek(DateTime startDate, DateTime endDate)
        {
            try
            {
                startDate = LocalDateTime.GetServerDateTime(startDate);
                endDate = LocalDateTime.GetServerDateTime(endDate);

                var sourcers = _adminRepository.GetVwAdminSourcersBalance(startDate , endDate);

                CreateResponse(sourcers, (int)HttpStatusCode.OK, "data is retreived Successfully!");

                return _currentResponse;
            }
            catch (System.Exception e)
            {
                LogException(e);
                return null;
            }
        }

        CurrentResponse IAdminService.UpdateAdminSourcersBalance(SourcerPaymentModel sourcerPayment)
        {
            try
            {
                sourcerPayment.startDate = LocalDateTime.GetServerDateTime(sourcerPayment.startDate);
                sourcerPayment.endDate = LocalDateTime.GetServerDateTime(sourcerPayment.endDate);

                var sourcers = _adminRepository.UpdateAdminSourcersBalance(sourcerPayment.startDate, sourcerPayment.endDate, sourcerPayment.sourcerId);

                CreateResponse(sourcers, (int)HttpStatusCode.OK, "data is updated Successfully!");

                return _currentResponse;
            }
            catch (System.Exception e)
            {
                LogException(e);
                return null;
            }
        }

        public void UpdateEmployerPackages()
        {
            try
            {
                _adminRepository.UpdateEmployerPackages();
            }
            catch (Exception e)
            {
                LogException(e);
            }
        }

        #endregion
    }
}
