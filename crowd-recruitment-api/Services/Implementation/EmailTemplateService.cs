﻿using Data.Tables;
using Repositories.Interfaces;
using Repositories.Enums;
using Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Mail;
using System.Text;
using Services.ViewModels;
using Microsoft.AspNetCore.Http;
using System.IO;

namespace Services.Implementation
{
    public class EmailTemplateService : BaseService<EmailsTemplate, EmailsTemplate>, IEmailTemplateService
    {
        #region Fields
        private readonly IEmailTemplateRepository _emailTemplateRepository;
        private readonly ICredentialRepository _credentialRepository;
        private readonly ICandidateRepository _candidateRepository;
        private readonly IJobRepository _jobRepository;
        private readonly IUserRepository _userRepository;

        #endregion

        #region Email Template Keywords

        private const string _userName = "[USERNAME]";
        private const string _activationLink = "[ACTIVATIONLINK]";
        private const string _interviewName = "[INTERVIEWERNAME]";
        private const string _meetingDate = "[MEETINGDATE]";
        private const string _meetingTime = "[MEETINGTIME]";
        private const string _rejectionReason = "[REJECTIONREASON]";
        private const string _firstName = "[FIRST NAME]";
        private const string _websiteSourcer = "[WEBSITE/SOURCE]";
        private const string _fieldType = "[FIELD]";
        private const string _companyName = "[COMPANY NAME]";
        private const string _jobTitle = "[JOB TITLE]";
        private const string _yourName = "[YOUR NAME]";
        private const string _yourEmailSignature = "[YOUR EMAIL SIGNATURE]";
        private const string _emailBody = "[EMAILBODY]";
        

        // Interview Keywords
        private const string _interviewCandidateName = "[Candidate NAME]";
        private const string _interviewCompanyName = "[COMPANY NAME]";
        private const string _interviewDate = "[Interview Date]";
        private const string _interviewTime = "[Interview Time]";
        
        private const string _interviewDuration = "[INTERVIEW DURATION]";
        private const string _interviewInterviewerName = "[INTERVIEWER NAME]";

        #endregion

        #region Constructors
        public EmailTemplateService(IEmailTemplateRepository emailTemplateRepository, ICredentialRepository credentialRepository,
            ICandidateRepository candidateRepository, IJobRepository jobRepository, IUserRepository userRepository) : base(emailTemplateRepository)
        {
            _emailTemplateRepository = emailTemplateRepository;
            _credentialRepository = credentialRepository;
            _candidateRepository = candidateRepository;
            _jobRepository = jobRepository;
            _userRepository = userRepository;
        }

        #endregion

        #region Methods

        public bool SendCRVerificationEmail(int languageId, string userName, string mailReceiver, string webAppLink , string uuId)
        {
            try
            {
                var emailTemplate = _emailTemplateRepository.GetEmailsTemplate((int)EmailsTemplatesEnum.CRVerificationEmail);

                StringBuilder emailBody = languageId == (int)LanguagesEnum.English ?
                    new StringBuilder(emailTemplate.EmailbodyEn) :
                    new StringBuilder(emailTemplate.EmailbodyAr);

                emailBody = emailBody.Replace(_userName, userName);
                //emailBody = emailBody.Replace(_activationLink, string.Format("{0}calendar/{1}", webAppLink,uuId));
                emailBody = emailBody.Replace(_activationLink, string.Format("{0}emailverified/{1}", webAppLink,uuId));

                string emailTitle = languageId == (int)LanguagesEnum.English ?
                    emailTemplate.EmailTitleEn : emailTemplate.EmailTitleAr;

                return SendEmail(emailTitle, emailBody.ToString(), mailReceiver);
            }
            catch (System.Exception e)
            {
                LogException(e);
                return false;
            }
        }

        public bool SendCRSourcerAcceptanceEmail(int languageId, string userName, string mailReceiver, string webAppLink)
        {
            try
            {
                var emailTemplate = _emailTemplateRepository.GetEmailsTemplate((int)EmailsTemplatesEnum.CRSourcerAcceptanceEmail);

                StringBuilder emailBody = languageId == (int)LanguagesEnum.English ?
                    new StringBuilder(emailTemplate.EmailbodyEn) :
                    new StringBuilder(emailTemplate.EmailbodyAr);

                emailBody = emailBody.Replace(_userName, userName);
                emailBody = emailBody.Replace(_activationLink,string.Format("{0}login",webAppLink));

                string emailTitle = languageId == (int)LanguagesEnum.English ?
                    emailTemplate.EmailTitleEn : emailTemplate.EmailTitleAr;

                return SendEmail(emailTitle, emailBody.ToString(), mailReceiver);
            }
            catch (System.Exception e)
            {
                LogException(e);
                return false;
            }
        }

        public bool SendCREmployerAcceptanceEmail(int languageId, string userName, string mailReceiver, string webAppLink)
        {
            try
            {
                var emailTemplate = _emailTemplateRepository.GetEmailsTemplate((int)EmailsTemplatesEnum.CREmployerAcceptanceEmail);

                StringBuilder emailBody = languageId == (int)LanguagesEnum.English ?
                    new StringBuilder(emailTemplate.EmailbodyEn) :
                    new StringBuilder(emailTemplate.EmailbodyAr);

                emailBody = emailBody.Replace(_userName, userName);
                emailBody = emailBody.Replace(_activationLink, string.Format("{0}login", webAppLink));

                string emailTitle = languageId == (int)LanguagesEnum.English ?
                    emailTemplate.EmailTitleEn : emailTemplate.EmailTitleAr;

                return SendEmail(emailTitle, emailBody.ToString(), mailReceiver);
            }
            catch (System.Exception e)
            {
                LogException(e);
                return false;
            }
        }

        public bool SendCRZoomEmail(int languageId, string userName, string mailReceiver, string webAppLink, string zoomLink, string interviewer, string meetingDate, string meetingTime)
        {
            try
            {
                var emailTemplate = _emailTemplateRepository.GetEmailsTemplate((int)EmailsTemplatesEnum.CRZoomMeetingEmail);

                StringBuilder emailBody = languageId == (int)LanguagesEnum.English ?
                    new StringBuilder(emailTemplate.EmailbodyEn) :
                    new StringBuilder(emailTemplate.EmailbodyAr);

                //emailBody = emailBody.Replace(_userName, userName);
                emailBody = emailBody.Replace(_activationLink, zoomLink);
                emailBody = emailBody.Replace(_userName, userName);
                emailBody = emailBody.Replace(_interviewName, interviewer);
                emailBody = emailBody.Replace(_meetingDate, meetingDate);
                emailBody = emailBody.Replace(_meetingTime, meetingTime);

                string emailTitle = languageId == (int)LanguagesEnum.English ?
                    emailTemplate.EmailTitleEn : emailTemplate.EmailTitleAr;

                return SendEmail(emailTitle, emailBody.ToString(), mailReceiver);
            }
            catch (System.Exception e)
            {
                LogException(e);
                return false;
            }
        }
        public bool SendCRNewSourcerEmail(int languageId, string userName, string mailReceiver, string webAppLink)
        {
            try
            {
                var emailTemplate = _emailTemplateRepository.GetEmailsTemplate((int)EmailsTemplatesEnum.CRNewSourcer);

                StringBuilder emailBody = languageId == (int)LanguagesEnum.English ?
                    new StringBuilder(emailTemplate.EmailbodyEn) :
                    new StringBuilder(emailTemplate.EmailbodyAr);

                emailBody = emailBody.Replace(_userName, userName);
                emailBody = emailBody.Replace(_activationLink, string.Format("{0}admin/sourcer-request", webAppLink));

                string emailTitle = languageId == (int)LanguagesEnum.English ?
                    emailTemplate.EmailTitleEn : emailTemplate.EmailTitleAr;

                return SendEmail(emailTitle, emailBody.ToString(), mailReceiver);
            }
            catch (System.Exception e)
            {
                LogException(e);
                return false;
            }
        }
        public bool SendCRNewEmployerEmail(int languageId, string userName, string mailReceiver, string webAppLink)
        {
            try
            {
                var emailTemplate = _emailTemplateRepository.GetEmailsTemplate((int)EmailsTemplatesEnum.CRNewEmployer);

                StringBuilder emailBody = languageId == (int)LanguagesEnum.English ?
                    new StringBuilder(emailTemplate.EmailbodyEn) :
                    new StringBuilder(emailTemplate.EmailbodyAr);

                emailBody = emailBody.Replace(_userName, userName);
                emailBody = emailBody.Replace(_activationLink, string.Format("{0}admin/employer-request", webAppLink));

                string emailTitle = languageId == (int)LanguagesEnum.English ?
                    emailTemplate.EmailTitleEn : emailTemplate.EmailTitleAr;

                return SendEmail(emailTitle, emailBody.ToString(), mailReceiver);
            }
            catch (System.Exception e)
            {
                LogException(e);
                return false;
            }
        }

        public bool SendInterviewEmail(string candidateName, string companyName, 
            DateTime interviewDate,TimeSpan time, string interviewDuration, string interviewerName,
            int languageId, string mailReceiver,string Description,string title, string jobTitle)
        {
            try
            {
                //var emailTemplate = _emailTemplateRepository.GetEmailsTemplate((int)EmailsTemplatesEnum.Interview);

                StringBuilder emailBody = languageId == (int)LanguagesEnum.English ?
                    new StringBuilder(Description) :
                    new StringBuilder(Description);

                emailBody = emailBody.Replace(_interviewCandidateName, candidateName);
                emailBody = emailBody.Replace(_interviewCompanyName, companyName);
                emailBody = emailBody.Replace(_interviewDate, interviewDate.ToString());
                emailBody = emailBody.Replace(_interviewDuration, interviewDuration);
                emailBody = emailBody.Replace(_interviewInterviewerName, interviewerName);
                emailBody = emailBody.Replace(_interviewTime, time.ToString());


                string emailTitle = languageId == (int)LanguagesEnum.English ?
                    title : title;

                emailTitle = emailTitle.Replace(_interviewCompanyName, companyName);
                emailTitle = emailTitle.Replace(_jobTitle, jobTitle);


                return SendCommonEmail(emailTitle, emailBody.ToString(), mailReceiver, languageId);
            }
            catch (System.Exception e)
            {
                LogException(e);
                return false;
            }
        }

        public bool SendOfferEmail(string offerSubject, string offerDescription, IFormFile offerFile,
                    string CandidateName, string Email, string JobTitle,int languageId)
        {
            try
            {
                //var emailTemplate = _emailTemplateRepository.GetEmailsTemplate((int)EmailsTemplatesEnum.Interview);

                StringBuilder emailBody = languageId == (int)LanguagesEnum.English ?
                    new StringBuilder(offerDescription) :
                    new StringBuilder(offerDescription);

                emailBody = emailBody.Replace(_interviewCandidateName, CandidateName);
                emailBody = emailBody.Replace(_jobTitle, JobTitle);

                string emailTitle = languageId == (int)LanguagesEnum.English ?
                    offerSubject : offerSubject;

                emailTitle = emailTitle.Replace(_jobTitle, JobTitle);


                return SendNewOfferEmail(emailTitle, emailBody.ToString(), Email, offerFile,(int)LanguagesEnum.English);
            }
            catch (System.Exception e)
            {
                LogException(e);
                return false;
            }
        }
        public bool SendResetTemplate(string email, string resetLink,int languageId)
        {
            try
            {
                var emailTemplate = _emailTemplateRepository.GetEmailsTemplate((int)EmailsTemplatesEnum.ResetPassoword);

                StringBuilder emailBody = languageId == (int)LanguagesEnum.English ?
                    new StringBuilder(emailTemplate.EmailbodyEn) :
                    new StringBuilder(emailTemplate.EmailbodyAr);

               
                emailBody = emailBody.Replace(_activationLink, resetLink);

                string emailTitle = languageId == (int)LanguagesEnum.English ?
                    emailTemplate.EmailTitleEn : emailTemplate.EmailTitleAr;

                return SendEmail(emailTitle, emailBody.ToString(), email);
            }
            catch (System.Exception e)
            {
                LogException(e);
                return false;
            }
        }
        public bool SendEmail(string emailTitle, string emailBody, string mailReceiver)
        {
            try
            {
                var mailCredentials = _credentialRepository.GetCredentials();
                var email = "noreply@joinsolutions.app";
                //Initialize mail message
                MailMessage message = new MailMessage();
                message.From = new MailAddress(email,emailTitle);
                message.To.Add(mailReceiver);
                message.Subject = emailTitle.Replace("\r\n", "");
                message.IsBodyHtml = true;
                message.Body = emailBody;
                message.Priority = MailPriority.High;

                //create mail smtp client
                SmtpClient smtpClient = new SmtpClient("joinsolutions.app");
                smtpClient.UseDefaultCredentials = false;

                smtpClient.Port = 587;
                smtpClient.EnableSsl = false;
                //smtpClient.DeliveryMethod = SmtpDeliveryMethod.Network;

                //var email = "assisted.vip@gmail.com";
                var password = "Z0xuw3$1";

                smtpClient.Credentials = new NetworkCredential(email, password);
                smtpClient.Send(message);
                return true;
            }
            catch (Exception e)
            {
                LogException(e);
                return false;
            }

        }

        public bool SendNewOfferEmail(string emailTitle, string emailBody, string mailReceiver, IFormFile file, int languageId)
        {
            try
            {

                var emailTemplate = _emailTemplateRepository.GetEmailsTemplate((int)EmailsTemplatesEnum.Common);

                string commonEmailBody = languageId == (int)LanguagesEnum.English ?
                    emailTemplate.EmailbodyEn :
                    emailTemplate.EmailbodyAr;

                commonEmailBody = commonEmailBody.Replace(_emailBody, emailBody);

                var mailCredentials = _credentialRepository.GetCredentials();
                var email = "noreply@joinsolutions.app";
                //Initialize mail message
                MailMessage message = new MailMessage();
                message.From = new MailAddress(email, emailTitle);
                message.To.Add(mailReceiver);
                message.Subject = emailTitle.Replace("\r\n", "");
                message.IsBodyHtml = true;
                message.Body = commonEmailBody;

                byte[] fileBytes;
                using (var memoryStream = new MemoryStream())
                {
                    file.CopyToAsync(memoryStream);
                    fileBytes = memoryStream.ToArray();
                }

                var filename = file.FileName;
                var contentType = file.ContentType;

                string fileName = Path.GetFileName(file.FileName);
                message.Attachments.Add(new Attachment(file.OpenReadStream(), fileName));

                message.Priority = MailPriority.High;

                //create mail smtp client
                SmtpClient smtpClient = new SmtpClient("joinsolutions.app");
                smtpClient.UseDefaultCredentials = false;

                smtpClient.Port = 587;
                smtpClient.EnableSsl = false;
                //smtpClient.DeliveryMethod = SmtpDeliveryMethod.Network;

                //var email = "assisted.vip@gmail.com";
                var password = "Z0xuw3$1";

                smtpClient.Credentials = new NetworkCredential(email, password);
                smtpClient.Send(message);
                return true;
            }
            catch (Exception e)
            {
                LogException(e);
                return false;
            }

        }

        public bool SendCommonEmail(string emailTitle, string emailBody, string mailReceiver, int languageId)
        {
            try
            {

                var emailTemplate = _emailTemplateRepository.GetEmailsTemplate((int)EmailsTemplatesEnum.Common);

                StringBuilder commonEmailBody = languageId == (int)LanguagesEnum.English ?
                    new StringBuilder(emailTemplate.EmailbodyEn) :
                    new StringBuilder(emailTemplate.EmailbodyAr);

                commonEmailBody = commonEmailBody.Replace(_emailBody, emailBody);

                var mailCredentials = _credentialRepository.GetCredentials();
                var email = "noreply@joinsolutions.app";
                //Initialize mail message
                MailMessage message = new MailMessage();
                message.From = new MailAddress(email, emailTitle);
                message.To.Add(mailReceiver);
                message.Subject = emailTitle.Replace("\r\n","");
                message.IsBodyHtml = true;
                message.Body = commonEmailBody.ToString();

                message.Priority = MailPriority.High;

                //create mail smtp client
                SmtpClient smtpClient = new SmtpClient("joinsolutions.app");
                smtpClient.UseDefaultCredentials = false;

                smtpClient.Port = 587;
                smtpClient.EnableSsl = false;
                //smtpClient.DeliveryMethod = SmtpDeliveryMethod.Network;

                //var email = "assisted.vip@gmail.com";
                var password = "Z0xuw3$1";

                smtpClient.Credentials = new NetworkCredential(email, password);
                smtpClient.Send(message);
                return true;
            }
            catch (Exception e)
            {
                LogException(e);
                return false;
            }

        }
        public List<EmailsTemplate> GetJobEmailsTemplate()
        {
            try
            {
                return _emailTemplateRepository.GetJobEmailsTemplate();
            }
            catch (Exception e)
            {
                LogException(e);
                return null;
            }

        }

        public bool SendCRRejectionEmail(int languageId, string userName, string reason, string mailReceiver, string webAppLink)
        {
            try
            {
                var emailTemplate = _emailTemplateRepository.GetEmailsTemplate((int)EmailsTemplatesEnum.CRRejectionEmail);

                StringBuilder emailBody = languageId == (int)LanguagesEnum.English ?
                    new StringBuilder(emailTemplate.EmailbodyEn) :
                    new StringBuilder(emailTemplate.EmailbodyAr);

                emailBody = emailBody.Replace(_userName, userName);
                emailBody = emailBody.Replace(_rejectionReason, reason);
                //emailBody = emailBody.Replace(_activationLink, string.Format("{0}calendar/{1}", webAppLink,uuId));
                emailBody = emailBody.Replace(_activationLink, string.Format("{0}calendar", webAppLink));

                string emailTitle = languageId == (int)LanguagesEnum.English ?
                    emailTemplate.EmailTitleEn : emailTemplate.EmailTitleAr;

                return SendEmail(emailTitle, emailBody.ToString(), mailReceiver);
            }
            catch (System.Exception e)
            {
                LogException(e);
                return false;
            }
        }

       public CurrentResponse SendJobEmail(int languageId, SendCandidateMailViewModel sendCandidateMailViewModel)
        {
            try
            {
                //var candidate = _candidateRepository.GetJobCandidateByUuId(languageId,sendCandidateMailViewModel.candidateUuId);
                //var sourcer = _userRepository.GetUserById(candidate.SourcerId);
                //var job = _jobRepository.GetVwJobByUuId(candidate.JobUuId , languageId);
                //var employer = _userRepository.GetUserById(job.EmployerId);

                //sendCandidateMailViewModel.firstMailSubject = sendCandidateMailViewModel.firstMailSubject.Replace(_jobTitle, job.JobTitle);
                //sendCandidateMailViewModel.firstMailSubject = sendCandidateMailViewModel.firstMailSubject.Replace(_companyName, job.CompanyName);

                //sendCandidateMailViewModel.firstMailDescription  = sendCandidateMailViewModel.firstMailDescription.Replace(_firstName, candidate.CandidateName);
                //sendCandidateMailViewModel.firstMailDescription = sendCandidateMailViewModel.firstMailDescription.Replace(_websiteSourcer, "Recruitment Website");
                //sendCandidateMailViewModel.firstMailDescription = sendCandidateMailViewModel.firstMailDescription.Replace(_fieldType, job.CompanyIndustry);
                //sendCandidateMailViewModel.firstMailDescription  = sendCandidateMailViewModel.firstMailDescription.Replace(_companyName, job.CompanyName);
                //sendCandidateMailViewModel.firstMailDescription  = sendCandidateMailViewModel.firstMailDescription.Replace(_jobTitle, job.JobTitle);
                //sendCandidateMailViewModel.firstMailDescription  = sendCandidateMailViewModel.firstMailDescription.Replace(_yourName, job.EmployerName);
                //sendCandidateMailViewModel.firstMailDescription = sendCandidateMailViewModel.firstMailDescription.Replace(_yourEmailSignature, employer.Email);

                //var body = System.Xml.Linq.XElement.Parse(sendCandidateMailViewModel.firstMailDescription).ToString();
                var isSentSuccessfully = SendCommonEmail(sendCandidateMailViewModel.firstMailSubject, sendCandidateMailViewModel.firstMailDescription, sendCandidateMailViewModel.email,(int)LanguagesEnum.English); 

                if(isSentSuccessfully)
                    CreateResponse(null, (int)HttpStatusCode.OK, "Mail is sent Successffully");

                else
                    CreateResponse(null, (int)HttpStatusCode.BadRequest, "Failed to send email");

                return _currentResponse;
            }
            catch (System.Exception e)
            {
                LogException(e);
                return CreateResponse(null, (int)HttpStatusCode.BadRequest, "Failed to send email");
            }
        }

        #endregion
    }
}
