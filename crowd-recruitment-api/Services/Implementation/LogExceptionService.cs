﻿using Data.Tables;
using Repositories.Interfaces;
using Services.Interfaces;
using Services.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Services.Implementation
{
    public class LogExceptionService : BaseService<LogException,LogException>, Interfaces.ILogExceptionService
    {
        private readonly ILogExceptionRepository _logExceptionRepository;

        public LogExceptionService(ILogExceptionRepository logExceptionRepository) : base(logExceptionRepository)
        {
            _logExceptionRepository = logExceptionRepository;
        }

  
        #region Methods

        #endregion
    }
}
