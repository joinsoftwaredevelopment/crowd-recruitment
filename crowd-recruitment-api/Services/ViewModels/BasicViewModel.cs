﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Services.ViewModels
{
    public class BasicViewModel
    {
        public int id { set; get; }
        public string name { set; get; }
        public int languageId { set; get; }
    }
}
