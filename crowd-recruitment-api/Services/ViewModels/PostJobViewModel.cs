﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Services.ViewModels
{
    public class PostJobViewModel
    {
        public string UUID { get; set; }
        public string JobTitle { get; set; }
        public string JobDescription { get; set; }
        public int companyIndustryId { get; set; }
        public int jobLocationId { get; set; }
        public DateTime JobDate { get; set; }
        public int SeniorityLevelId { get; set; }
        public int EmployerTypeId { get; set; }
        public int JobNationalityId { get; set; }
        public int ExperienceLevelId { get; set; }
        public int JobStatusId { get; set; }
        public int PeopleToHireCount { get; set; }
        public bool IsContinuousHire { get; set; }
        public int TimetoHireId { get; set; }
        public int NumberOfComingDays { get; set; }
        public int SourcingOusideYourCompanyId { get; set; }
        public int FindingJobDifficultyLevelId { get; set; }
        public string FirstMailSubject { get; set; }
        public string FirstMailDescription { get; set; }
        public string FollowUp1Days { get; set; }
        public string Followup2Days { get; set; }
        public bool IsDeleted { get; set; }
        public int JobLocationId { get; set; }
        public int OfficeLocationId { get; set; }
        public string mustHaveQualification { get; set; }
        public string niceToHaveQualification { get; set; }
        public string companiesNotToSourceFrom { get; set; }
        public string HiringNeeds { get; set; }
        public string whenYouNeedToHire { get; set; }
        public bool sourcingOutsideOfJoin { get; set; }
    }
}
