﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Services.ViewModels
{
    public class SourcerIndustryViewModel
    {
        public string Uuid { get; set; }
        public int? IndustryId { get; set; }

    }
}
