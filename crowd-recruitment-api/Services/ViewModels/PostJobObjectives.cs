﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Services.ViewModels
{
    public class PostJobObjectives
    {
        public string UUID { get; set; }
        public bool isContinuousHire { get; set; }
        public int peopleToHireCount { get; set; }
        public int whenYouNeedToHire { get; set; }
        public bool sourcingOutsideOfJoin { get; set; }
        public string findingJobDifficultyLevelId { get; set; }
    }
}
