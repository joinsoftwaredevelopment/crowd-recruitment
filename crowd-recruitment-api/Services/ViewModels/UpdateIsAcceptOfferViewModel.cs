﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Services.ViewModels
{
    public class UpdateIsAcceptOfferViewModel
    {
        public string submissionUuid { get; set; }
        public bool isOfferAccepted { get; set; }
    }
}
