﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Services.ViewModels
{
    public class UpdateEmployerDetailsViewModel
    {
        public string uuid { get; set; }
        public string firstName { get; set; }
        public string lastName { get; set; }
        public string companyName { get; set; }
        public string phoneNumber { get; set; }
        public string email { get; set; }
    }
}
