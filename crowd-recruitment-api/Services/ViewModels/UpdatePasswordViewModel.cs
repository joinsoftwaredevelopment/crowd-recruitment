﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Services.ViewModels
{
    public class UpdatePasswordViewModel
    {
        public string uuid { get; set; }
        public string password { get; set; }
        public string newPassword { get; set; }
    }
}
