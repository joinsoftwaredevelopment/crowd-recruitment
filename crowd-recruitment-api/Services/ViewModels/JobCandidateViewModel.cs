﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Services.ViewModels
{
   public class JobCandidateViewModel
    {
        public int JobId { get; set; }
        public string JobUuId { get; set; }
        public string JobTitle { get; set; }
        public DateTime? JobDate { get; set; }
        public int SourcerId { get; set; }
        public string SourcerName { get; set; }
        public string SubmissionUuId { get; set; }
        public string CandidateName { get; set; }
        public string CvName { get; set; }
        public string LinkedInProfile { get; set; }
        public string CountryName { get; set; }
        public DateTime? SubmissionDate { get; set; }
        public int? LanguageId { get; set; }
    }
}
