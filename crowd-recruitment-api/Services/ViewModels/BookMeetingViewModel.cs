﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Services.ViewModels
{
    public class BookMeetingViewModel
    {
        public string uuid { get; set; }
        public string timePeriod { get; set; }
        public string timeZone { get; set; }
        public string mettingDate { get; set; }
        public string interviewerName { get; set; }
        public string zoomLink { get; set; }
        public string email { get; set; }
        public string userName { get; set; }

    }
}
