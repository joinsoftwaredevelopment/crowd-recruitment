﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Services.ViewModels
{
    public class JwtApiContract
    {
        public string SecretKey { get; set; }
        public string EncryptKey { get; set; }
        public string Audience { get; set; }
        public string Issuer { get; set; }
        public TimeSpan ExpriationTime { get; set; }
    }
}
