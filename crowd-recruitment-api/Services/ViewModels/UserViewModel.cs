﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Services.ViewModels
{
    public class UserViewModel
    {
        public string uuId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string PhoneNumber { get; set; }
        public string Email { get; set; }
        public bool? IsEmailVerified { get; set; }
        public string Password { get; set; }
        public int RoleId { get; set; }
        public int LanguageId { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public bool Active { get; set; }
        public bool? Accepted { get; set; }
    }
}
