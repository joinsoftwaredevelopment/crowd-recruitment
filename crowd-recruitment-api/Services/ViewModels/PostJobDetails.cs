﻿
using System;
using System.Collections.Generic;
using System.Text;

namespace Services.ViewModels
{
    public class PostJobDetails
    {
        public string uuId { get; set; }
        public string JobTitle { get; set; }
        public string JobDescription { get; set; }
        public int industryId { get; set; }
        public int jobLocationId { get; set; }
        public int SeniorityLevelId { get; set; }
        public int EmployerTypeId { get; set; }
        public int JobNationalityId { get; set; }
        public string requirements { get; set; }
        public int jobTemplateId { get; set; }
    }
}
