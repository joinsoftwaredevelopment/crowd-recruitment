﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Text;

namespace Services.ViewModels
{
    public class SourcerCVViewModel
    {
        public string JobUuId { get; set; }
        public string Uuid { get; set; }
        public string candidateName { get; set; }
        public string CvName { get; set; }
        public string LinkedInProfile { get; set; }
        public string Email { get; set; }
        public int LocationId { get; set; }
    }
}
