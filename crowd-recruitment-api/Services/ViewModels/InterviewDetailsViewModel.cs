﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Services.ViewModels
{
    public class InterviewDetailsViewModel
    {
        public string employerUuid { get; set; }
        public int id { get; set; }
          public string submissionUUid { get; set; }
          public int interviewerId { get; set; }
          public string interviewComment { get; set; }
          public int interviewTypeId { get; set; }
         public int interviewerTypeId { get; set; }
         public DateTime interviewDate { get; set; }
          public TimeSpan interviewTime { get; set; }
          public int duration { get; set; }
          public int createdBy { get; set; }
          public DateTime createdDate { get; set; }
        public int? templateId { get; set; }
        public string emailSubject { get; set; }
        public string emailDescription { get; set; }

    }
}
