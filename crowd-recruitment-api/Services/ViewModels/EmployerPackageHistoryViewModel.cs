﻿using System;

namespace Services.ViewModels
{
    public class EmployerPackageHistoryViewModel
    {
        public int Id { get; set; }

        public int Userid { get; set; }

        public string PackageName { get; set; }

        public DateTime PackagePurchaseDate { get; set; }

        public DateTime PackageExpiryDate { get; set; }
    }
}
