﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Services.ViewModels
{
    public class EmployerDetailsViewModel
    {
        public string Uuid { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string CompanyName { get; set; }
        public string PhoneNumber { get; set; }
        public string Email { get; set; }
        public DateTime CreatedDate { get; set; }
        public bool? IsEmailVerified { get; set; }
        public string Password { get; set; }
        public int RoleId { get; set; }
        public int LanguageId { get; set; }
        public bool Active { get; set; }
        public int PackageId { get; set; }
        public string NewPassword { get; set; }
        public string ConfirmedPassword { get; set; }
    }
}
