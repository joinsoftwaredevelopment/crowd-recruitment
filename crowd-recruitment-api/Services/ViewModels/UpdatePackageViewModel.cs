﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Services.ViewModels
{
    public class UpdatePackageViewModel
    {
        public string uuid { get; set; }
        public int packageId { get; set; }
    }
}
