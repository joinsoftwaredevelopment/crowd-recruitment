﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Services.ViewModels
{
    public class PostJobEngagment
    {
        public string uuid { get; set; }        
        public string firstMailSubject { get; set; }
        public string firstMailDescription { get; set; }
        public int statusId { get; set; }
        public int engagementTemplateId { get; set; }
        
    }
}
