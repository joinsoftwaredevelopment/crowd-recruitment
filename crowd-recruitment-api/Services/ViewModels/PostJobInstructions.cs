﻿using Data.Views;
using System;
using System.Collections.Generic;
using System.Text;

namespace Services.ViewModels
{
    public class PostJobInstructions
    {
        public string uuid { get; set; }
        public int experienceLevelId { get; set; }
        public int currencyId { get; set; }
        public int minSalary { get; set; }
        public int maxSalary { get; set; }
        public List<mustHaveQualification> mustHaveSkills { get; set; }
        public List<niceHaveQualification> niceHaveQualification { get; set; }
        public List<notToSource> notToSource { get; set; }
    }
}
