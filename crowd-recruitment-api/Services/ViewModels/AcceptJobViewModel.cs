﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Services.ViewModels
{
    public class AcceptJobViewModel
    {
        public string uuid { get; set; }
        public int numberOfSubmissions { get; set; }
        public decimal commissionVaue { get; set; }
    }
}
