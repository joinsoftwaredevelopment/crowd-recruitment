﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services.ViewModels
{
    public class ValidationMessageResult
    {
        #region Prop
        public List<KeyValuePair<string, string[]>> errors { get; set; }
        public bool isValid { get { return !errors.Any(); } }
        public string sucessMessage { get; set; }

        #endregion

        #region Constructor
        public ValidationMessageResult()
        {
            errors = new List<KeyValuePair<string, string[]>>();
        }
        #endregion

        #region Methods
        public void AddError(string key, string value)
        {
            errors.Add(new KeyValuePair<string, string[]>(key, new string[] { value}));
        }
        public string GetAllErrorsConcatenated()
        {
            return string.Join("\r\n", errors.Select(o => string.Join("\r\n", o.Value.Select(o => o))));
        }
        #endregion
    }
}
