﻿
using System;
using System.Collections.Generic;
using System.Text;

namespace Services.ViewModels
{
    public class SourcerPaymentModel
    {
        public int sourcerId { set; get; }
        public DateTime startDate { set; get; }
        public DateTime endDate { set; get; }

    }
}
