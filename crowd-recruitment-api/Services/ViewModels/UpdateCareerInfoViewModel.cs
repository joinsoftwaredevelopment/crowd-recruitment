﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Services.ViewModels
{
    public class UpdateCareerInfoViewModel
    {
        public string uuid { get; set; }
        public int experienceTypeId { get; set; }
        public bool hasAccessToDifferentWebsite { get; set; }
        public int industryId { get; set; }
    }
}
