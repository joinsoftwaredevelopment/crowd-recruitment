﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Services.ViewModels
{
    public class InterviewEmailDetailsViewModel
    {
        public string companyName { get; set; }
        public string candidateName { get; set; }
        public string interviewerName { get; set; }
        public DateTime interviewDate { get; set; }
        public TimeSpan time { get; set; }
        public string interviewDuration { get; set; }
        public string candidateEmail { get; set; }
        public string subject { get; set; }
        public string description { get; set; }
        public string jobTitle { get; set; }
    }
}
