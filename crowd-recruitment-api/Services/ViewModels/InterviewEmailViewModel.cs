﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Services.ViewModels
{
    public class InterviewEmailViewModel
    {
        public int interviewId { get; set; }
        public int? templateId { get; set; }
        public string emailSubject { get; set; }
        public string emailDescription { get; set; }
        public bool isCanceled { get; set; }
        public bool isEmailSent { get; set; }

    }
}
