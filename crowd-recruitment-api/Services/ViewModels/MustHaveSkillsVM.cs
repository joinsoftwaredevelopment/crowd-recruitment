﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Services.ViewModels
{
    public class MustHaveSkillsVM
    {
        public string id { get; set; }
        public string name { get; set; }
    }
}
