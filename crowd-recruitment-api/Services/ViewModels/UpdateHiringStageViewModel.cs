﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Services.ViewModels
{
    public class UpdateHiringStageViewModel
    {
        public string submissionUuid { get; set; }
        public int hiringStageId { get; set; }
    }
}
