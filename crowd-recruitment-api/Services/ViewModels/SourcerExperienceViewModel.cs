﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Services.ViewModels
{
    public class SourcerExperienceViewModel
    {
        public string Uuid { get; set; }
        public int? ExperienceTypeId { get; set; }

    }
}
