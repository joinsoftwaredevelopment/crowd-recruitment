﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Services.ViewModels
{
    public class LogInViewModel
    {
        public string Email { get; set; }
        public string Password { get; set; }
        public string RoleId { get; set; }
    }
}
