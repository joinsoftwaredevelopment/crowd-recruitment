﻿using Data.Tables;
using System;
using System.Collections.Generic;
using System.Text;

namespace Services.ViewModels
{
    public class InstructionsInfoVM
    {
        public Job job { get; set; }
        public IEnumerable<MustHaveSkillsVM> mustHaveSkillsVM { get; set; }
        public IEnumerable<NiceHaveSkillsVM> niceHaveSkillsVM { get; set; }
        public IEnumerable<NotToSourceVM> notToSourceVM { get; set; }

    }
}
