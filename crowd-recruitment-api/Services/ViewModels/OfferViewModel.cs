﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Text;

namespace Services.ViewModels
{
    public class OfferViewModel
    {
        public string submissionUuid { get; set; }
        public string offerSubject { get; set; }
        public string offerDescription { get; set; }
        public IFormFile offerFile { get; set; }
    }
}
