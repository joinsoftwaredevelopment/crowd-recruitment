﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Services.ViewModels
{
    public class UpdateIsHiredViewModel
    {
        public string submissionUuid { get; set; }
        public bool isHired { get; set; }
    }
}
