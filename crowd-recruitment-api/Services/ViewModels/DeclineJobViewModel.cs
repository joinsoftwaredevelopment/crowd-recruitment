﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Services.ViewModels
{
    public class DeclineJobViewModel
    {
        public string uuid { get; set; }
        public string rejectionReason { get; set; }
    }
}
