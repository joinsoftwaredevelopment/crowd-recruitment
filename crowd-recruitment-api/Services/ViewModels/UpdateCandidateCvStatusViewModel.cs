﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Services.ViewModels
{
    public class UpdateCandidateCvStatusViewModel
    {
        public string submissionUuid { get; set; }
        public bool isCvApproved { get; set; }
    }
}
