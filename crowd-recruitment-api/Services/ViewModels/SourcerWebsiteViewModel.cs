﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Services.ViewModels
{
    public class SourcerWebsiteViewModel
    {
        public string Uuid { get; set; }
        public bool? HasAccessToDifferentWebsite { get; set; }

    }
}
