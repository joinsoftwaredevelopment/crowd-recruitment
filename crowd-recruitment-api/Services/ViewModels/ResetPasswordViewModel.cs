﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Services.ViewModels
{
    public class ResetPasswordViewModel
    {
        public string Password { get; set; }
        public string Uuid { get; set; }
    }
}
