﻿using Microsoft.AspNetCore.Mvc.Filters;
using Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebAPICore.Filters
{
    public class ExceptionFilter : ExceptionFilterAttribute
    {
        private readonly ILogExceptionService _logExceptionService;

        public ExceptionFilter(ILogExceptionService logExceptionService)
        {
            _logExceptionService = logExceptionService;
        }

        public override void OnException(ExceptionContext context)
        {
            _logExceptionService.LogException(context.Exception);
            base.OnException(context);
        }
    }
}
