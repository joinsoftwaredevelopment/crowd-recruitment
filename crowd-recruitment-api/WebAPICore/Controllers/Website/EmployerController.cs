﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Services.Interfaces;
using Services.Middleware;
using Services.ViewModels;
using System.Net;
using WebAPICore.Filters;

namespace WebAPICore.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class EmployerController : ControllerBase
    {
        private readonly IEmployerService _employerService;
        private readonly IJobService _jobService;
        public EmployerController(IEmployerService employerService, IJobService jobService)
        {
            _employerService = employerService;
            _jobService = jobService;
        }

        [HttpPost]
        [Route("create-employer")]
        public IActionResult Post(EmployerDetailsViewModel employerDetailsViewModel)
        {

            var responseObj = _employerService.AddEmployer(employerDetailsViewModel);
            if (responseObj.status == (int)HttpStatusCode.OK)
                return Ok(responseObj);
            else
                throw new RestException(responseObj);

        }


        #region Post Job

        [HttpGet]
        [Route("GetJobDetailsInfo")]
        public IActionResult GetJobDetailsInfo(string uuid, int? tempId)
        {
            var responseObj = _jobService.GetJobDetailsInfo(uuid, tempId);
            if (responseObj.status == (int)HttpStatusCode.OK)
                return Ok(responseObj);
            else
                throw new RestException(responseObj);
        }

        [HttpGet]
        [Route("GetJobInstructionsInfo")]
        public IActionResult GetJobInstructionsInfo(string uuid)
        {
            var responseObj = _jobService.GetJobInstructionsInfo(uuid);
            if (responseObj.status == (int)HttpStatusCode.OK)
                return Ok(responseObj);
            else
                throw new RestException(responseObj);
        }


        [HttpGet]
        [Route("GetJobEngagementsInfo")]
        public IActionResult GetJobEngagementsInfo(string uuid)
        {
            var responseObj = _jobService.GetJobEngagementsInfo(uuid);
            if (responseObj.status == (int)HttpStatusCode.OK)
                return Ok(responseObj);
            else
                throw new RestException(responseObj);
        }

        [HttpPost]
        [Authorize]
        [Route("CreateJob")]
        public IActionResult CreateJob(PostJobDetails postJobViewModel)
        {
            var responseObj = new CurrentResponse();
            // check for job uuid if is not null then update else create
            if (postJobViewModel.uuId != null && postJobViewModel.uuId != "0")
            {
                responseObj = _jobService.UpdateJob(postJobViewModel);
            }

            else
            {
                postJobViewModel.uuId = ((UserViewModel)HttpContext.Items["User"]).uuId;
                responseObj = _jobService.CreateJob(postJobViewModel);
            }

            if (responseObj.status == (int)HttpStatusCode.OK)
                return Ok(responseObj);
            else
                throw new RestException(responseObj);
        }


        [HttpGet]
        [Route("GetJobByUuid")]
        public IActionResult GetJobByUuid(string uuid)
        {
            var responseObj = _jobService.GetJobByUuid(uuid);
            if (responseObj.status == (int)HttpStatusCode.OK)
                return Ok(responseObj);
            else
                throw new RestException(responseObj);
        }

        [HttpPost]
        [Authorize]
        [Route("UpdateJobInstructions")]
        public ActionResult UpdateJobInstructions(PostJobInstructions postJobViewModel)
        {
            // list of job skills in view model 
            var responseObj = _jobService.UpdateJobInstructions(postJobViewModel);

            if (responseObj.status == (int)HttpStatusCode.OK)
                return Ok(responseObj);
            else
                throw new RestException(responseObj);
        }

        [HttpPost]
        [Authorize]
        [Route("UpdateJobObjectives")]
        public ActionResult UpdateJobObjectives(PostJobObjectives postJobViewModel)
        {
            var responseObj = _jobService.UpdateJobObjectives(postJobViewModel);

            if (responseObj.status == (int)HttpStatusCode.OK)
                return Ok(responseObj);
            else
                throw new RestException(responseObj);
        }

        [HttpPost]
        [Authorize]
        [Route("UpdateJobEngagment")]
        public ActionResult UpdateJobEngagment(PostJobEngagment postJobViewModel)
        {
            var responseObj = _jobService.UpdateJobEngagement(postJobViewModel);

            if (responseObj.status == (int)HttpStatusCode.OK)
                return Ok(responseObj);
            else
                throw new RestException(responseObj);
        }

        [HttpGet]
        [Authorize]
        [Route("job-summaries")]
        public ActionResult GetJobSummariesByEmp()
        {
            var uuId = ((UserViewModel)HttpContext.Items["User"]).uuId;
            var responseObj = _jobService.GetJobSummariesByEmployer(2, uuId);

            if (responseObj.status == (int)HttpStatusCode.OK)
                return Ok(responseObj);
            else
                throw new RestException(responseObj);
        }

        [HttpGet]
        [Authorize]
        [Route("GetAllJobs")]
        public ActionResult GetAllJobs()
        {
            var uuId = ((UserViewModel)HttpContext.Items["User"]).uuId;
            var responseObj = _jobService.GetAllJobs(2, uuId);

            if (responseObj.status == (int)HttpStatusCode.OK)
                return Ok(responseObj);
            else
                throw new RestException(responseObj);

        }

        [HttpGet]
        [Authorize]
        [Route("job-drafts")]
        public ActionResult GetJobDraftsByEmp()
        {
            var uuId = ((UserViewModel)HttpContext.Items["User"]).uuId;
            var responseObj = _jobService.GetJobDraftsByEmployer(2, uuId);

            if (responseObj.status == (int)HttpStatusCode.OK)
                return Ok(responseObj);
            else
                throw new RestException(responseObj);

        }


        [HttpGet]
        [Authorize]
        [Route("job-closed")]
        public ActionResult GetJobClosedByEmployer()
        {
            var uuId = ((UserViewModel)HttpContext.Items["User"]).uuId;
            var responseObj = _jobService.GetJobClosedByEmployer(2, uuId);

            if (responseObj.status == (int)HttpStatusCode.OK)
                return Ok(responseObj);
            else
                throw new RestException(responseObj);

        }


        [HttpPost]
        [Authorize]
        [Route("CloseJob")]
        public IActionResult CloseJob(CloseJob closeJob)
        {
            var responseObj = new CurrentResponse();

            responseObj = _jobService.CloseJob(closeJob);

            if (responseObj.status == (int)HttpStatusCode.OK)
                return Ok(responseObj);
            else
                throw new RestException(responseObj);
        }

        [HttpGet]
        [Authorize]
        [Route("DeleteJob")]
        public IActionResult DeleteJob(string uuid)
        {
            var responseObj = new CurrentResponse();

            responseObj = _jobService.DeleteJob(uuid);

            if (responseObj.status == (int)HttpStatusCode.OK)
                return Ok(responseObj);
            else
                throw new RestException(responseObj);
        }

        #endregion

    }
}
