﻿using Microsoft.AspNetCore.Mvc;
using Services.Interfaces;
using Services.Middleware;
using Services.ViewModels;
using System.Net;
using WebAPICore.Filters;

namespace WebAPICore.Controllers.Website
{
    [Route("api/[controller]")]
    [ApiController]
    public class EmployerPackageHistoryController : ControllerBase
    {

        private readonly IEmployerPackageHistoryService _employerPackageHistoryService;
        public EmployerPackageHistoryController(IEmployerPackageHistoryService employerPackageHistoryService)
        {
            _employerPackageHistoryService = employerPackageHistoryService;
        }

        [HttpGet]
        [Authorize]
        [Route("get-all")]
        public IActionResult GetAll()
        {
            var uuId = ((UserViewModel)HttpContext.Items["User"]).uuId;
            var responseObj = _employerPackageHistoryService.GetAllPackagesHistory(uuId);
            if (responseObj.status == (int)HttpStatusCode.OK)
                return Ok(responseObj);
            else
                throw new RestException(responseObj);
        }
    }
}
