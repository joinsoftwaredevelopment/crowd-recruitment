﻿using System.Net;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Services.Interfaces;
using Services.Middleware;
using Services.ViewModels;
using WebAPICore.Filters;

namespace WebAPICore.Controllers.Website
{
    [Route("api/[controller]")]
    [ApiController]
    public class CandidateController : ControllerBase
    {
        public readonly ICandidateService _candidateService;
        public readonly IEmailTemplateService _emailTemplateService;

        public CandidateController(ICandidateService candidateService, IEmailTemplateService emailTemplateService)
        {
            _candidateService = candidateService;
            _emailTemplateService = emailTemplateService;
        }

        [HttpGet]
        [Authorize]
        [Route("get-job-candidates")]
        public ActionResult GetJobCandidates(string jobUuId)
        {
            var uuId = ((UserViewModel)HttpContext.Items["User"]).uuId;
            var responseObj = _candidateService.GetJobCandidates(2, jobUuId);

            if (responseObj.status == (int)HttpStatusCode.OK)
                return Ok(responseObj);
            else
                throw new RestException(responseObj);

        }

        [HttpGet]
        [Authorize]
        [Route("get-accepted-job-candidates")]
        public ActionResult GetAcceptedJobCandidates(string jobUuId)
        {
            var uuId = ((UserViewModel)HttpContext.Items["User"]).uuId;
            var responseObj = _candidateService.GetAcceptedJobCandidates(2, jobUuId);

            if (responseObj.status == (int)HttpStatusCode.OK)
                return Ok(responseObj);
            else
                throw new RestException(responseObj);

        }

        [HttpGet]
        [Authorize]
        [Route("get-job-candidate-by-uuId")]
        public ActionResult GetJobCandidateByUuId(string candidateUuId)
        {
            var uuId = ((UserViewModel)HttpContext.Items["User"]).uuId;
            var responseObj = _candidateService.GetJobCandidateByUuId(2, candidateUuId);

            if (responseObj.status == (int)HttpStatusCode.OK)
                return Ok(responseObj);
            else
                throw new RestException(responseObj);

        }

        [HttpGet]
        [Route("GetSourcerCandidateBySourcerUuId")]
        public ActionResult GetSourcerCandidateBySourcerUuId(string sourcerUuId, string jobUuId)
        {
            var responseObj = _candidateService.GetSourcerCandidateBySourcerUuId(2, sourcerUuId, jobUuId);

            if (responseObj.status == (int)HttpStatusCode.OK)
                return Ok(responseObj);
            else
                throw new RestException(responseObj);

        }

        [HttpGet]
        [Route("GetSourcerCandidateBySubmissionStatusId")]
        public ActionResult GetSourcerCandidateBySubmissionStatusId(string sourcerUuId , int submissionStatusId, string jobUuId)
        {
            var responseObj = _candidateService.GetSourcerCandidateBySubmissionStatusId(2, sourcerUuId, submissionStatusId, jobUuId);

            if (responseObj.status == (int)HttpStatusCode.OK)
                return Ok(responseObj);
            else
                throw new RestException(responseObj);

        }


        [HttpPost]
        [Route("AcceptCandidate")]
        public IActionResult AcceptCandidate(UserRequestViewModel userRequestViewModel)
        {
            var responseObj = _candidateService.AcceptCandidate(userRequestViewModel.uuid);
            if (responseObj.status == (int)HttpStatusCode.OK)
                return Ok(responseObj);
            else
                throw new RestException(responseObj);
        }

        [HttpPost]
        [Route("DeclineCandidate")]
        public IActionResult DeclineCandidate(UserRequestViewModel userRequestViewModel)
        {
            var responseObj = _candidateService.DeclineCandidate(userRequestViewModel);
            if (responseObj.status == (int)HttpStatusCode.OK)
                return Ok(responseObj);
            else
                throw new RestException(responseObj);
        }

        [HttpPost]
        [Route("EmployerAcceptCandidate")]
        public IActionResult EmployerAcceptCandidate(UserRequestViewModel userRequestViewModel)
        {
            var responseObj = _candidateService.EmployerAcceptCandidate(userRequestViewModel.uuid);
            if (responseObj.status == (int)HttpStatusCode.OK)
                return Ok(responseObj);
            else
                throw new RestException(responseObj);
        }

        [HttpPost]
        [Route("EmployerDeclineCandidate")]
        public IActionResult EmployerDeclineCandidate(UserRequestViewModel userRequestViewModel)
        {
            var responseObj = _candidateService.EmployerDeclineCandidate(userRequestViewModel);
            if (responseObj.status == (int)HttpStatusCode.OK)
                return Ok(responseObj);
            else
                throw new RestException(responseObj);
        }

        [HttpPost]
        [Route("SendMail")]
        public ActionResult SendMail(SendCandidateMailViewModel sendCandidateMailViewModel)
        {
            // send mail here
            var responseObj = _emailTemplateService.SendJobEmail(2,sendCandidateMailViewModel);
            if (responseObj.status == (int)HttpStatusCode.OK)
                return Ok(responseObj);
            else
                throw new RestException(responseObj);
        }

        [HttpGet]
        [Route("CandidateStatusHistory")]
        public IActionResult CandidateStatusHistory(string candidateUuId)
        {
            var responseObj = _candidateService.GetCandidateStatusHistoryByUuId(2,candidateUuId);
            if (responseObj.status == (int)HttpStatusCode.OK)
                return Ok(responseObj);
            else
                throw new RestException(responseObj);
        }

        [HttpPost]
        [Route("update-hiring-stage")]
        public IActionResult UpdateHiringStage(UpdateHiringStageViewModel updateHiringStageViewModel)
        {
            var responseObj = _candidateService.UpdateHiringStage(updateHiringStageViewModel);
            if (responseObj.status == (int)HttpStatusCode.OK)
                return Ok(responseObj);
            else
                throw new RestException(responseObj);
        }

        [HttpPost]
        [Route("update-isAccept-offer")]
        public IActionResult UpdateIsAcceptOffer(UpdateIsAcceptOfferViewModel updateIsAcceptOfferViewModel)
        {
            var responseObj = _candidateService.UpdateIsAcceptOffer(updateIsAcceptOfferViewModel);
            if (responseObj.status == (int)HttpStatusCode.OK)
                return Ok(responseObj);
            else
                throw new RestException(responseObj);
        }

        [HttpPost]
        [Route("update-isHired")]
        public IActionResult UpdateIsHired(UpdateIsHiredViewModel updateIsHiredViewModel)
        {
            var responseObj = _candidateService.UpdateIsHired(updateIsHiredViewModel);
            if (responseObj.status == (int)HttpStatusCode.OK)
                return Ok(responseObj);
            else
                throw new RestException(responseObj);
        }

        [HttpPost]
        [Route("Update-CandidateCv-Status")]
        public IActionResult UpdateCandidateCvStatus(UpdateCandidateCvStatusViewModel updateCandidateCvStatusViewModel)
        {
            var responseObj = _candidateService.UpdateCandidateCvStatus(updateCandidateCvStatusViewModel);
            if (responseObj.status == (int)HttpStatusCode.OK)
                return Ok(responseObj);
            else
                throw new RestException(responseObj);
        }
    }
}