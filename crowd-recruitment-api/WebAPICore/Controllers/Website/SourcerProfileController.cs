﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Services.Interfaces;
using Services.Middleware;
using Services.ViewModels;
using System.Net;
using WebAPICore.Filters;

namespace WebAPICore.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SourcerProfileController : ControllerBase
    {
        private readonly ISourcerProfileService _sourcerProfileService;
        public SourcerProfileController(ISourcerProfileService sourcerProfileService)
        {
            _sourcerProfileService = sourcerProfileService;
        }


        [HttpGet]
        [Route("GetBasicInfo")]
        public IActionResult GetBasicInfo(string uuid)
        {
            var responseObj = _sourcerProfileService.GetBasicInfo(uuid);
            if (responseObj.status == (int)HttpStatusCode.OK)
                return Ok(responseObj);
            else
                throw new RestException(responseObj);

        }


        [HttpPost]
        [Route("UpdateBasicInfo")]
        public IActionResult UpdateBasicInfo(SourcerBasicViewModel sourcerBasicViewModel)
        {
            var responseObj = _sourcerProfileService.UpdateBasicInfo(sourcerBasicViewModel);
            if (responseObj.status == (int)HttpStatusCode.OK)
                return Ok(responseObj);
            else
                throw new RestException(responseObj);

        }


        [HttpGet]
        [Route("GetCareerInfo")]
        public IActionResult GetCareerInfo(string uuid)
        {
            var responseObj = _sourcerProfileService.GetBasicInfo(uuid);
            if (responseObj.status == (int)HttpStatusCode.OK)
                return Ok(responseObj);
            else
                throw new RestException(responseObj);

        }

        [HttpPost]
        [Route("UpdateCareerInfo")]
        public IActionResult UpdateCareerInfo(UpdateCareerInfoViewModel updateCareerInfoViewModel)
        {
            var responseObj = _sourcerProfileService.UpdateCareerInfo(updateCareerInfoViewModel);
            if (responseObj.status == (int)HttpStatusCode.OK)
                return Ok(responseObj);
            else
                throw new RestException(responseObj);

        }
    }
}
