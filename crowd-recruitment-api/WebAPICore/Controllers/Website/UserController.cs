﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Data.Views;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Services.Interfaces;
using Services.Middleware;
using Services.ViewModels;

namespace WebAPICore.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserController : ControllerBase
    {
        private readonly IUserService _userService;
        public UserController(IUserService userService)
        {
            _userService = userService;
        }

        
        [HttpPost]
        [Route("LogIn")]
        public IActionResult LogIn(LogInViewModel logInViewModel)
        {
            var responseModel = _userService.LogIn(logInViewModel);

            if (responseModel.status == (int)HttpStatusCode.OK)
                return Ok(responseModel);
            else
                throw new RestException(responseModel);
        }

        [HttpPost]
        [Route("Send-Reset-Email")]
        public IActionResult SendResetEmail(LogInViewModel logInViewModel)
        {
            var responseModel = _userService.SendResetEmail(logInViewModel.Email);

            if (responseModel.status == (int)HttpStatusCode.OK)
                return Ok(responseModel);
            else
                throw new RestException(responseModel);
        }

        [HttpPost]
        [Route("Rese-Password")]
        public IActionResult ResePassword(ResetPasswordViewModel resetPasswordViewModel)
        {
            var responseModel = _userService.ResePassword(resetPasswordViewModel);

            if (responseModel.status == (int)HttpStatusCode.OK)
                return Ok(responseModel);
            else
                throw new RestException(responseModel);
        }

        // get check if time is booked

        [HttpPost]
        [Route("BookMeeting")]
        public IActionResult BookMeeting(BookMeeting bookMeeting)
        {
            var responseModel = _userService.BookMeeting(bookMeeting);

            if (responseModel.status == (int)HttpStatusCode.OK)
                return Ok(responseModel);
            else
                throw new RestException(responseModel);
        }

        [HttpGet]
        [Route("GetMeetingByUuid")]
        public IActionResult GetMeetingByUuid(string uuid)
        {
            var responseModel = _userService.GetMeetingInfo(uuid);

            return Ok(responseModel);
        }

        [HttpGet]
        [Route("GetCountries")]
        public IActionResult GetCountries()
        {

            var responseModel = _userService.GetAllCountries(2);
            return Ok(responseModel);
        }

        [HttpGet]
        [Route("GetCurrencies")]
        public IActionResult GetCurrencies()
        {

            var responseModel = _userService.GetAllCurrencies(2);
            return Ok(responseModel);
        }

        [HttpPost]
        [Route("UpdatePassword")]
        public IActionResult UpdatePassword(UpdatePasswordViewModel updatePasswordViewModel)
        {
            updatePasswordViewModel.uuid = ((UserViewModel)HttpContext.Items["User"]).uuId;
            var responseObj = _userService.UpdatePassword(updatePasswordViewModel);
            if (responseObj.status == (int)HttpStatusCode.OK)
                return Ok(responseObj);
            else
                throw new RestException(responseObj);

        }

        [HttpPost]
        [Route("verify-email-address")]
        public IActionResult VerifyEmailAddress([FromQuery]string uuId)
        {
            var responseModel = _userService.VerifyUserEmailByUuId(uuId);

            if (responseModel.status == (int)HttpStatusCode.OK)
                return Ok(responseModel);
            else
                throw new RestException(responseModel);
        }

        [HttpGet]
        [Route("GetUserByUuId")]
        public IActionResult GetUserByUuId(string uuid)
        {
            var responseModel = _userService.GetUserByUuId(uuid);

            return Ok(responseModel);
        }

        [HttpPost]
        [Route("resend-email")]
        public IActionResult ResendEmail([FromQuery]string uuid)
        {
            var responseModel = _userService.ResendEmail(uuid , 2);
            return Ok(responseModel);
        }

        [HttpPost]
        [Route("change-email")]
        public IActionResult ChangeEmail(string uuid , string Email)
        {
            var responseModel = _userService.ChangeEmail(uuid, Email , 2);

            if (responseModel.status == (int)HttpStatusCode.OK)
                return Ok(responseModel);
            else
                throw new RestException(responseModel);
        }
    }
}
