﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Repositories.Common.Loggers;
using Services.Interfaces;
using Services.Middleware;
using Services.ViewModels;
using WebAPICore.Filters;

namespace WebAPICore.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SourcerController : ControllerBase
    {
        public readonly ISourcerService _sourcerService;
        private IWebHostEnvironment _hostEnvironment;

        public SourcerController(ISourcerService sourcerService, IWebHostEnvironment environment)
        {
            _sourcerService = sourcerService;
            _hostEnvironment = environment;
        }

        // POST: api/Sourcer
        [HttpPost]
        [Route("create-sourcer")]
        public ActionResult CreateSourcer(SourcerViewModel sourcerViewModel)
        {
            //sourcerViewModel.PhoneNumber = string.Format("+{0}",sourcerViewModel.PhoneNumber);
            var responseObj = _sourcerService.CreateSourcer(sourcerViewModel);

            if (responseObj.status == (int)HttpStatusCode.OK)
                return Ok(responseObj);
            else
                throw new RestException(responseObj);
        }

        [HttpGet]
        [Authorize]
        [Route("get-sourcer-experienceId")]
        public ActionResult GetSourcerExperienceId()
        {
            var responseObj = _sourcerService
                .GetSourcerExperienceId(((UserViewModel)HttpContext.Items["User"]).uuId);

            if (responseObj.status == (int)HttpStatusCode.OK)
                return Ok(responseObj);
            else
                throw new RestException(responseObj);
        }

        [HttpPost]
        [Authorize]
        [Route("update-sourcer-experience")]
        public ActionResult UpdateSourcerExperience(SourcerExperienceViewModel model)
        {
            model.Uuid = ((UserViewModel)HttpContext.Items["User"]).uuId;
            var responseObj = _sourcerService.UpdateSourcerExperience(model);

            if (responseObj.status == (int)HttpStatusCode.OK)
                return Ok(responseObj);
            else
                throw new RestException(responseObj);
        }



        [HttpGet]
        [Authorize]
        [Route("get-sourcer-jobTitle")]
        public ActionResult GetJobtitleName()
        {
            var responseObj = _sourcerService
                .GetJobtitleName(((UserViewModel)HttpContext.Items["User"]).uuId);

            if (responseObj.status == (int)HttpStatusCode.OK)
                return Ok(responseObj);
            else
                throw new RestException(responseObj);
        }

        [HttpPost]
        [Authorize]
        [Route("update-sourcer-jobtitle")]
        public ActionResult UpdateSourcerJobtitle(SourcerJobViewModel model)
        {
            model.Uuid = ((UserViewModel)HttpContext.Items["User"]).uuId;
            var responseObj = _sourcerService.UpdateSourcerJobtitle(model);

            if (responseObj.status == (int)HttpStatusCode.OK)
                return Ok(responseObj);
            else
                throw new RestException(responseObj);
        }


        [HttpGet]
        [Authorize]
        [Route("get-sourcer-website")]
        public ActionResult GetSourcerhasAccessToWebsite()
        {
            var responseObj = _sourcerService
                .GetSourcerhasAccessToWebsite(((UserViewModel)HttpContext.Items["User"]).uuId);

            if (responseObj.status == (int)HttpStatusCode.OK)
                return Ok(responseObj);
            else
                throw new RestException(responseObj);
        }
        [HttpPost]
        [Authorize]
        [Route("update-sourcer-websites")]
        public ActionResult UpdateSourcerWebsites(SourcerWebsiteViewModel model)
        {
            model.Uuid = ((UserViewModel)HttpContext.Items["User"]).uuId;
            var responseObj = _sourcerService.UpdateSourcerWebsites(model);

            if (responseObj.status == (int)HttpStatusCode.OK)
                return Ok(responseObj);
            else
                throw new RestException(responseObj);
        }


        [HttpGet]
        [Authorize]
        [Route("get-sourcer-industryId")]
        public ActionResult GetSourcerIndustryId()
        {
            var responseObj = _sourcerService
                .GetSourcerIndustryId(((UserViewModel)HttpContext.Items["User"]).uuId);

            if (responseObj.status == (int)HttpStatusCode.OK)
                return Ok(responseObj);
            else
                throw new RestException(responseObj);
        }

        [HttpPost]
        [Authorize]
        [Route("update-sourcer-industry")]
        public ActionResult UpdateSourcerIndustry(SourcerIndustryViewModel model)
        {
            model.Uuid = ((UserViewModel)HttpContext.Items["User"]).uuId;
            var responseObj = _sourcerService.UpdateSourcerIndustry(model);

            if (responseObj.status == (int)HttpStatusCode.OK)
                return Ok(responseObj);
            else
                throw new RestException(responseObj);


        }

        [HttpPost]
        [Authorize]
        [Route("update-sourcer-cv")]
        public async Task<ActionResult> UpdateSourcerCV([FromForm]SourcerCVViewModel model)
        {
            var formFile = Request.Form.Files[0];
            model.Uuid = ((UserViewModel)HttpContext.Items["User"]).uuId;

            //server path
            var serverPath = _hostEnvironment.ContentRootPath;
            string path = Path.Combine(serverPath, "UploadCVs");

            // Check if directory exist, if not then create them
            if (!Directory.Exists(path))
                Directory.CreateDirectory(path);

            var filePath = Path.Combine(path, model.CvName);
            using (FileStream fs = System.IO.File.Create(filePath))
            {
                formFile.CopyTo(fs);
            }

            var responseObj =  _sourcerService.UpdateSourcerCV(model);

            if (responseObj.status == (int)HttpStatusCode.OK)
                return Ok(responseObj);
            else
                throw new RestException(responseObj);
        }

        [HttpGet]
        [Route("open-pdf")]
        public ActionResult GetPublicLink(string fileName)
        {
            var serverPath = _hostEnvironment.ContentRootPath;
            string path = Path.Combine(serverPath, "UploadCVs");
            var filePath = Path.Combine(path, "coolfreecv_resume_en_03_n.pdf");
            return new PhysicalFileResult(filePath, "application/pdf");
        }

        [HttpGet]
        [Route("open-pdf2")]
        public ActionResult GetPublicLink2(string fileName)
        {
            var serverPath = _hostEnvironment.ContentRootPath;
            string path = Path.Combine(serverPath, "UploadCVs");
            var filePath = Path.Combine(path, fileName);
            return new PhysicalFileResult(filePath, "application/pdf");

            //Stream stream = System.IO.File.OpenRead(filePath);
            //string mimeType = "application/pdf";
            //return new FileStreamResult(stream, mimeType)
            //{
            //    FileDownloadName = "CV.pdf"
            //};

            //return File(stream, "application/pdf", "CV.pdf");

        }

        [HttpGet]
        [Route("GetSourcersBalances")]
        public IActionResult GetSourcersBalances(string sourcerUuid)
        {
            var responseObj = _sourcerService.GetSourcerBalances(sourcerUuid);
            if (responseObj.status == (int)HttpStatusCode.OK)
                return Ok(responseObj);
            else
                throw new RestException(responseObj);
        }

        //[HttpGet]
        //[Route("GetSourcersBalanceSummary")]
        //public IActionResult GetSourcersBalanceSummary(string sourcerUuid)
        //{
        //    var responseObj = _sourcerService.GetSourcerBalancesSummary(sourcerUuid);
        //    if (responseObj.status == (int)HttpStatusCode.OK)
        //        return Ok(responseObj);
        //    else
        //        throw new RestException(responseObj);
        //}
    }
}
