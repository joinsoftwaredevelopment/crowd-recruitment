﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Services.Interfaces;
using Services.Middleware;
using Services.ViewModels;
using System.Net;
using WebAPICore.Filters;

namespace WebAPICore.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class EmployerProfileController : ControllerBase
    {
        private readonly IEmployerProfileService _employerProfileService;
        public EmployerProfileController(IEmployerProfileService employerProfileService)
        {
            _employerProfileService = employerProfileService;
        }


        [HttpGet]
        [Route("GetEmployerDetails")]
        public IActionResult GetEmployerDetails(string uuid)
        {
            var empUuId = ((UserViewModel)HttpContext.Items["User"]).uuId;
            var responseObj = _employerProfileService.GetEmployerDetails(empUuId);
            if (responseObj.status == (int)HttpStatusCode.OK)
                return Ok(responseObj);
            else
                throw new RestException(responseObj);

        }


        [HttpPost]
        [Route("UpdateEmployerDetails")]
        public IActionResult UpdateEmployerDetails(UpdateEmployerDetailsViewModel updateEmployerDetailsViewModel)
        {
            var responseObj = _employerProfileService.UpdateEmployerDetails(updateEmployerDetailsViewModel);
            if (responseObj.status == (int)HttpStatusCode.OK)
                return Ok(responseObj);
            else
                throw new RestException(responseObj);

        }

        [HttpPost]
        [Route("UpdatePackage")]
        public IActionResult UpdatePackage(UpdatePackageViewModel updatePackageViewModel)
        {
            updatePackageViewModel.uuid = ((UserViewModel)HttpContext.Items["User"]).uuId;
            var responseObj = _employerProfileService.UpdatePackage(updatePackageViewModel);
            if (responseObj.status == (int)HttpStatusCode.OK)
                return Ok(responseObj);
            else
                throw new RestException(responseObj);
        }
    }
}
