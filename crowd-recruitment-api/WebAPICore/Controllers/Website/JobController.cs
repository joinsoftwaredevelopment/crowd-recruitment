﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Services.Interfaces;
using Services.Middleware;
using Services.ViewModels;
using WebAPICore.Filters;

namespace WebAPICore.Controllers.Website
{
    [Route("api/[controller]")]
    [ApiController]
    public class JobController : ControllerBase
    {
        public readonly IJobService _jobService;
        public readonly IUserService _userService;
        private IWebHostEnvironment _hostEnvironment;

        public JobController(IJobService jobService, IWebHostEnvironment environment, IUserService userService)
        {
            _jobService = jobService;
            _hostEnvironment = environment;
            _userService = userService;
        }

        // POST: api/Job
        [HttpGet]
        [Authorize]
        [Route("job-summaries")]
        public ActionResult GetJobSummaries()
        {
            var uuId = ((UserViewModel)HttpContext.Items["User"]).uuId;
            var responseObj = _jobService.GetJobSummaries(2, uuId);

            if (responseObj.status == (int)HttpStatusCode.OK)
                return Ok(responseObj);
            else
                throw new RestException(responseObj);

        }

        [HttpGet]
        [Authorize]
        [Route("sourcer-job-summaries")]
        public ActionResult GetSourcerJobSummaries(bool isActive)
        {
            var uuId = ((UserViewModel)HttpContext.Items["User"]).uuId;
            var responseObj = _jobService.GetSourcerJobSummaries(2, uuId, isActive);

            if (responseObj.status == (int)HttpStatusCode.OK)
                return Ok(responseObj);
            else
                throw new RestException(responseObj);
        }

        [HttpGet]
        [Authorize]
        [Route("sourcer-job-submissions")]
        public ActionResult GetSourcerJobSubmissions(string jobUuId)
        {
            var uuId = ((UserViewModel)HttpContext.Items["User"]).uuId;
            var responseObj = _jobService.GetSourcerJobSubmissions(2, uuId, jobUuId);

            if (responseObj.status == (int)HttpStatusCode.OK)
                return Ok(responseObj);
            else
                throw new RestException(responseObj);
        }


        [HttpPost]
        [Authorize]
        [Route("add-sourcer-submission")]
        public async Task<ActionResult> AddSourcerSubmission([FromForm]SourcerCVViewModel model)
        {
            bool isMembershipActive = _jobService.IsEmployerMembershipActive(model.JobUuId);

            if(!isMembershipActive)
            {
                CurrentResponse currentResponse = new CurrentResponse();
                currentResponse.status = (int)HttpStatusCode.BadRequest;
                currentResponse.message = "Employer job is deactivated";

                throw new RestException(currentResponse);
            }

            var formFile = Request.Form.Files[0];
            model.Uuid = ((UserViewModel)HttpContext.Items["User"]).uuId;

            //server path
            var serverPath = _hostEnvironment.ContentRootPath;
            string path = Path.Combine(serverPath, "UploadCVs");

            // Check if directory exist, if not then create them
            if (!Directory.Exists(path))
                Directory.CreateDirectory(path);

            var filePath = Path.Combine(path, model.CvName);
            using (FileStream fs = System.IO.File.Create(filePath))
            {
                formFile.CopyTo(fs);
            }

            var responseObj = _jobService.AddSourcerSubmission(model);

            if (responseObj.status == (int)HttpStatusCode.OK)
                return Ok(responseObj);
            else
                throw new RestException(responseObj);
        }

        [HttpGet]
        [Authorize]
        [Route("get-job-details")]
        public ActionResult GetJobDetails(string jobUuId)
        {
            var responseObj = _jobService.GetVwJobByUuId(jobUuId,2);

            if (responseObj.status == (int)HttpStatusCode.OK)
                return Ok(responseObj);
            else
                throw new RestException(responseObj);
        }

        [HttpGet]
        [Authorize]
        [Route("GetAllIndustries")]
        public ActionResult GetAllIndustries()
        {
            var responseObj = _jobService.GetAllIndustries();

            if (responseObj.status == (int)HttpStatusCode.OK)
                return Ok(responseObj);
            else
                throw new RestException(responseObj);
        }



        [HttpGet]
        [Authorize]
        [Route("GetTempByIndustryId")]
        public ActionResult GetTempByIndustryId(int industryId)
        {
            var responseObj = _jobService.GetTempByIndustryId(industryId);

            if (responseObj.status == (int)HttpStatusCode.OK)
                return Ok(responseObj);
            else
                throw new RestException(responseObj);
        }

        [HttpGet]
        [Authorize]
        [Route("GetAllJobTemps")]
        public ActionResult GetAllJobTemps()
        {
            var responseObj = _jobService.GetAllJobTemps();

            if (responseObj.status == (int)HttpStatusCode.OK)
                return Ok(responseObj);
            else
                throw new RestException(responseObj);
        }

        [HttpGet]
        [Authorize]
        [Route("GetJobTempById")]
        public ActionResult GetJobTempById(int templateId)
        {
            var responseObj = _jobService.GetJobTempById(templateId);

            if (responseObj.status == (int)HttpStatusCode.OK)
                return Ok(responseObj);
            else
                throw new RestException(responseObj);
        }

    }
}