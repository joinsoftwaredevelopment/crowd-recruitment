﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Services.Interfaces;
using Services.Middleware;
using Services.ViewModels;
using System.IO;
using System.Net;
using WebAPICore.Filters;

namespace WebAPICore.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class InterviewController : ControllerBase
    {
        private readonly IInterviewService _interviewService;
        public InterviewController(IInterviewService interviewService)
        {
            _interviewService = interviewService;
        }

        #region Adding, Cancel Interview

        [HttpPost]
        [Authorize]
        [Route("add-interview-details")]
        public ActionResult AddInterviewDetails(InterviewDetailsViewModel model)
        {
            model.employerUuid = ((UserViewModel)HttpContext.Items["User"]).uuId;
            var responseObj = _interviewService.AddInterviewDetails(model);

            if (responseObj.status == (int)HttpStatusCode.OK)
                return Ok(responseObj);
            else
                throw new RestException(responseObj);
        }

        //[HttpPost]
        //[Authorize]
        //[Route("add-interview-emails")]
        //public ActionResult AddInterviewEmails(InterviewEmailViewModel model)
        //{
        //    var responseObj = _interviewService.AddInterviewEmails(model);

        //    if (responseObj.status == (int)HttpStatusCode.OK)
        //        return Ok(responseObj);
        //    else
        //        throw new RestException(responseObj);
        //}

        [HttpPost]
        [Route("add-offer")]
        public ActionResult AddOffer([FromForm] OfferViewModel body)
        {
            //byte[] fileBytes;
            //using (var memoryStream = new MemoryStream())
            //{
            //    body.offerFile.CopyToAsync(memoryStream);
            //    fileBytes = memoryStream.ToArray();
            //}

            //var filename = body.offerFile.FileName;
            //var contentType = body.offerFile.ContentType;

            //return Ok();

            var responseObj = _interviewService.SendOffer(body);

            if (responseObj.status == (int)HttpStatusCode.OK)
                return Ok(responseObj);
            else
                throw new RestException(responseObj);
        }


        [HttpGet]
        [Authorize]
        [Route("cancel-interview")]
        public ActionResult CancelInterview(int interviewId,bool isCanceled)
        {
            var responseObj = _interviewService.CancelInterview(interviewId, isCanceled);

            if (responseObj.status == (int)HttpStatusCode.OK)
                return Ok(responseObj);
            else
                throw new RestException(responseObj);
        }

        #endregion

        #region Get Information

        [HttpGet]
        [Authorize]
        [Route("get-all-interviewers")]
        public ActionResult GetAllInterviewers()
        {
            var responseObj = _interviewService
                .GetAllInterviewers(2);

            if (responseObj.status == (int)HttpStatusCode.OK)
                return Ok(responseObj);
            else
                throw new RestException(responseObj);
        }

        

        [HttpGet]
        [Authorize]
        [Route("get-duration-times")]
        public ActionResult GetDurationTimes()
        {
            var responseObj = _interviewService
                .GetDurationTimes(2);

            if (responseObj.status == (int)HttpStatusCode.OK)
                return Ok(responseObj);
            else
                throw new RestException(responseObj);
        }


        [HttpGet]
        [Authorize]
        [Route("get-all-interviews")]
        public ActionResult GetAllInterviews()
        {
            var responseObj = _interviewService
                .GetAllInterviews();

            if (responseObj.status == (int)HttpStatusCode.OK)
                return Ok(responseObj);
            else
                throw new RestException(responseObj);
        }

        [HttpGet]
        [Authorize]
        [Route("get-all-interviews-byUuid")]
        public ActionResult GetAllInterviewsByUuid(string uuid)
        {
            var responseObj = _interviewService
                .GetAllInterviewsByUuid(uuid);

            if (responseObj.status == (int)HttpStatusCode.OK)
                return Ok(responseObj);
            else
                throw new RestException(responseObj);
        }

        [HttpGet]
        [Authorize]
        [Route("get-interview-details-byId")]
        public ActionResult GetInterviewDetails(int interviewId)
        {
            var responseObj = _interviewService
                .GetInterviewDetailsById(interviewId);

            if (responseObj.status == (int)HttpStatusCode.OK)
                return Ok(responseObj);
            else
                throw new RestException(responseObj);
        }


        [HttpGet]
        [Authorize]
        [Route("get-interview-emails-byId")]
        public ActionResult GetInterviewEmails(int interviewId)
        {
            var responseObj = _interviewService
                .GetInterviewEmailsById(interviewId);

            if (responseObj.status == (int)HttpStatusCode.OK)
                return Ok(responseObj);
            else
                throw new RestException(responseObj);
        }

        [HttpGet]
        [Authorize]
        [Route("get-interview-temps")]
        public ActionResult GetInterviewTemplates()
        {
            var responseObj = _interviewService.GetInterviewTemplates();

            if (responseObj.status == (int)HttpStatusCode.OK)
                return Ok(responseObj);
            else
                throw new RestException(responseObj);
        }

        [HttpGet]
        [Authorize]
        [Route("get-offer-temps")]
        public ActionResult GetOfferTemplates()
        {
            var responseObj = _interviewService.GetOfferTemplates();

            if (responseObj.status == (int)HttpStatusCode.OK)
                return Ok(responseObj);
            else
                throw new RestException(responseObj);
        }

        [HttpGet]
        [Authorize]
        [Route("get-interview-temp-byId")]
        public ActionResult GetInterviewTempById(int templateId)
        {
            var responseObj = _interviewService.GetInterviewTempById(templateId);

            if (responseObj.status == (int)HttpStatusCode.OK)
                return Ok(responseObj);
            else
                throw new RestException(responseObj);
        }

        #endregion

    }
}
