﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Services.Interfaces;
using Services.Middleware;
using Services.ViewModels;

namespace WebAPICore.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ManageSourcerController : ControllerBase
    {
        public readonly ISourcerService _sourcerService;
        public readonly IAdminService _adminService;

        public ManageSourcerController(ISourcerService sourcerService, IAdminService adminService)
        {
            _sourcerService = sourcerService;
            _adminService = adminService;
        }

        [HttpGet]
        [Route("GetAllSourcers")]
        public IActionResult GetAllSourcers(int userStatusId)
        {
            var responseObj = _sourcerService.GetAllSourcers(userStatusId);
            if (responseObj.status == (int)HttpStatusCode.OK)
                return Ok(responseObj);
            else
                throw new RestException(responseObj);
        }

        [HttpGet]
        [Route("GetSourcerById")]
        public IActionResult GetSourcerById(string uuid)
      {
            var responseObj = _sourcerService.GetAllSourcerInfo(uuid);
            if (responseObj.status == (int)HttpStatusCode.OK)
                return Ok(responseObj);
            else
                throw new RestException(responseObj);
        }

        [HttpPost]
        [Route("AcceptSourcer")]
        public IActionResult AcceptSourcer(UserRequestViewModel userRequestViewModel)
        {
            var responseObj = _sourcerService.AcceptSourcer(userRequestViewModel.uuid);
            if (responseObj.status == (int)HttpStatusCode.OK)
                return Ok(responseObj);
            else
                throw new RestException(responseObj);
        }

        [HttpPost]
        [Route("DeclineSourcer")]
        public IActionResult DeclineSourcer(UserRequestViewModel userRequestViewModel)
        {
            var responseObj = _sourcerService.DeclineSourcer(userRequestViewModel);
            if (responseObj.status == (int)HttpStatusCode.OK)
                return Ok(responseObj);
            else
                throw new RestException(responseObj);
        }

        [HttpGet]
        [Route("GetVwAdminSourcersBalance")]
        public IActionResult GetVwAdminSourcersBalance()
        {
            var responseObj = _adminService.GetVwAdminSourcersBalance();
            if (responseObj.status == (int)HttpStatusCode.OK)
                return Ok(responseObj);
            else
                throw new RestException(responseObj);
        }


        [HttpGet]
        [Route("GetVwAdminSourcersBalanceByWeek")]
        public IActionResult GetVwAdminSourcersBalanceByWeek(DateTime startDate, DateTime endDate)
        {
            var responseObj = _adminService.GetVwAdminSourcersBalanceByWeek(startDate, endDate);
            if (responseObj.status == (int)HttpStatusCode.OK)
                return Ok(responseObj);
            else
                throw new RestException(responseObj);
        }

        [HttpPost]
        [Route("UpdateAdminSourcersBalance")]
        public IActionResult UpdateAdminSourcersBalance(SourcerPaymentModel sourcerPayment)
        {
            var responseObj = _adminService.UpdateAdminSourcersBalance(sourcerPayment);
            if (responseObj.status == (int)HttpStatusCode.OK)
                return Ok(responseObj);
            else
                throw new RestException(responseObj);
        }
    }
}
