﻿using System;
using System.Net;
using Microsoft.AspNetCore.Mvc;
using Services.Interfaces;
using Services.Middleware;
using Services.ViewModels;

namespace WebAPICore.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AdminController : ControllerBase
    {
        private readonly IAdminService _adminService;
        public AdminController(IAdminService adminService)
        {
            _adminService = adminService;
        }


        [HttpPost]
        [Route("LogIn")]
        public IActionResult LogIn(LogInViewModel logInViewModel)
        {
            var responseModel = _adminService.LogIn(logInViewModel);

            if (responseModel.status == (int)HttpStatusCode.OK)
                return Ok(responseModel);
            else
                throw new RestException(responseModel);
        }

        [HttpGet]
        [Route("GetJobSourcers")]
        public ActionResult GetJobSourcers(string jobUuId)
        {
            var responseModel = _adminService.GetJobSourcers(jobUuId);

            if (responseModel.status == (int)HttpStatusCode.OK)
                return Ok(responseModel);
            else
                throw new RestException(responseModel);
        }

        [HttpGet]
        [Route("UpdateEmployerPakeage")]
        public void UpdateEmployerPackages()
        {
            _adminService.UpdateEmployerPackages();
        }
    }
}