﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Services.Interfaces;
using Services.Middleware;
using Services.ViewModels;

namespace WebAPICore.Controllers.Admin
{
    [Route("api/[controller]")]
    [ApiController]
    public class CommissionController : ControllerBase
    {
        private readonly IJobService _jobService;
        public CommissionController(IJobService jobService)
        {
            _jobService = jobService;
        }

        [HttpGet]
        //[Authorize]
        [Route("GetAllJobs")]
        public ActionResult GetAllJobs(int statusId)
        
        {
            var responseObj = _jobService.GetAllJobs(statusId);

            if (responseObj.status == (int)HttpStatusCode.OK)
                return Ok(responseObj);
            else
                throw new RestException(responseObj);

        }

        [HttpGet]
        //[Authorize]
        [Route("GetAllJobDetails")]
        public ActionResult GetAllJobDetails(string uuid)
        {
            var responseObj = _jobService.GetJobByUuid(uuid);

            if (responseObj.status == (int)HttpStatusCode.OK)
                return Ok(responseObj);
            else
                throw new RestException(responseObj);

        }

        [HttpPost]
        [Route("AcceptJob")]
        public IActionResult AcceptJob(AcceptJobViewModel acceptJobViewModel)
        {
            var responseObj = _jobService.AcceptJob(acceptJobViewModel.uuid,
                acceptJobViewModel.numberOfSubmissions, acceptJobViewModel.commissionVaue);

            if (responseObj.status == (int)HttpStatusCode.OK)
                return Ok(responseObj);
            else
                throw new RestException(responseObj);
        }

        [HttpPost]
        [Route("DeclineJob")]
        public IActionResult DeclineJob(DeclineJobViewModel declineJobViewModel)
        {
            var responseObj = _jobService.DeclineJob(declineJobViewModel.uuid,
                declineJobViewModel.rejectionReason);

            if (responseObj.status == (int)HttpStatusCode.OK)
                return Ok(responseObj);
            else
                throw new RestException(responseObj);
        }


    }
}
