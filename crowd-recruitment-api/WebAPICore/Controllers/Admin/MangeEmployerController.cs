﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Services.Interfaces;
using Services.Middleware;
using Services.ViewModels;

namespace WebAPICore.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class MangeEmployerController : ControllerBase
    {
        private readonly IEmployerService _employerService;
        public MangeEmployerController(IEmployerService employerService)
        {
            _employerService = employerService;
        }

        [HttpGet]
        [Route("GetAllEmployers")]
        public IActionResult GetAllEmployers(int userStatusId)
        {
            var responseObj = _employerService.GetAllEmployers(userStatusId);
            if (responseObj.status == (int)HttpStatusCode.OK)
                return Ok(responseObj);
            else
                throw new RestException(responseObj);
        }

        [HttpPost]
        [Route("AcceptEmployer")]
        public IActionResult AcceptEmployer(UserRequestViewModel userRequestViewModel)
        {
            var responseObj = _employerService.AcceptEmployer(userRequestViewModel.uuid);
            if (responseObj.status == (int)HttpStatusCode.OK)
                return Ok(responseObj);
            else
                throw new RestException(responseObj);
        }

        [HttpPost]
        [Route("DeclineEmployer")]
        public IActionResult DeclineEmployer(UserRequestViewModel userRequestViewModel)
        {
            var responseObj = _employerService.DeclineEmployer(userRequestViewModel);
            if (responseObj.status == (int)HttpStatusCode.OK)
                return Ok(responseObj);
            else
                throw new RestException(responseObj);
        }

    }
}
