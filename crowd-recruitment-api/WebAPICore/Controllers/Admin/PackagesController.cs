﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Services.Interfaces;
using Services.Middleware;
using Services.ViewModels;

namespace WebAPICore.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PackagesController : ControllerBase
    {
        private readonly IPackagesService _packagesService;
        public PackagesController(IPackagesService packagesService)
        {
            _packagesService = packagesService;
        }

        //  Add package
        [HttpPost]
        [Route("AddPackage")]
        public IActionResult AddPackage(AddPackageViewModel addPackageViewModel)
        {
            var responseModel = _packagesService.AddPackage(addPackageViewModel);

            if (responseModel.status == (int)HttpStatusCode.OK)
                return Ok(responseModel);
            else
                throw new RestException(responseModel);
        }

        // Get Package by id
        [HttpGet]
        [Route("GetPackageById")]
        public IActionResult GetPackageById(int packageId)
        {
            var responseModel = _packagesService.GetPackageById(packageId);

            if (responseModel.status == (int)HttpStatusCode.OK)
                return Ok(responseModel);
            else
                throw new RestException(responseModel);
        }

        // Edit package
        [HttpPost]
        [Route("EditPackage")]
        public IActionResult EditPackage(AddPackageViewModel addPackageViewModel)
        {
            var responseModel = _packagesService.EditPackage(addPackageViewModel);

            if (responseModel.status == (int)HttpStatusCode.OK)
                return Ok(responseModel);
            else
                throw new RestException(responseModel);
        }

        // Delete package
        [HttpGet]
        [Route("DeletePackage")]
        public IActionResult DeletePackage(int packageId)
        {
            var responseModel = _packagesService.DeletePackage(packageId);

            if (responseModel.status == (int)HttpStatusCode.OK)
                return Ok(responseModel);
            else
                throw new RestException(responseModel);
        }

        // Get all packages
        [HttpGet]
        [Route("GetAllPackages")]
        public IActionResult GetAllPackages()
        {
            var responseModel = _packagesService.GetAllPackages();

            if (responseModel.status == (int)HttpStatusCode.OK)
                return Ok(responseModel);
            else
                throw new RestException(responseModel);
        }

    }
}
