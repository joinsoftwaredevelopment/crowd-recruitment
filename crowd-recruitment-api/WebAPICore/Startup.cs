using System;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.OpenApi.Models;
using Data.Tables;
using Services.Interfaces;
using Services.Implementation;
using AutoMapper;
using Services.Common;
using Repositories.Interfaces;
using Repositories.Implementation;
using Services.Middleware;
using WebAPICore.Helpers;
using Helpers;
using Hangfire;
using Hangfire.SqlServer;

namespace WebAPICore
{
    public class Startup
    {

        private static IServiceCollection _serviceCollection;
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers().AddNewtonsoftJson(x => x.SerializerSettings.ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore);

            // configure strongly typed settings object
            services.Configure<AppSettings>(Configuration.GetSection("AppSettings"));

            services.AddDataProtection();

            // Register the Swagger generator, defining 1 or more Swagger documents
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "Crowd-Recruitment", Version = "v1" });
            });

            services.AddControllers().AddNewtonsoftJson();

            services.AddDbContext<recruitmentContext>(options =>
            options.UseSqlServer(Configuration.GetConnectionString("DevConnection")));

            //services.AddCors(option => option.AddPolicy("CorsPolicy", policy => {
            //    policy.AllowAnyHeader().AllowAnyMethod().AllowAnyOrigin();
            //}));

            services.AddCors(options =>
            {
                options.AddPolicy("CorsPolicy",
                    builder => builder.AllowAnyOrigin()
                    .AllowAnyMethod()
                    .AllowAnyHeader()
                    );
            });

            services.AddHangfire(configuration => configuration
        .SetDataCompatibilityLevel(CompatibilityLevel.Version_170)
        .UseSimpleAssemblyNameTypeSerializer()
        .UseRecommendedSerializerSettings()
        .UseSqlServerStorage(Configuration.GetConnectionString("DevConnection"), new SqlServerStorageOptions
        {
            CommandBatchMaxTimeout = TimeSpan.FromMinutes(5),
            SlidingInvisibilityTimeout = TimeSpan.FromMinutes(5),
            QueuePollInterval = TimeSpan.Zero,
            UseRecommendedIsolationLevel = true,
            DisableGlobalLocks = true
        }));

            services.AddHangfireServer();
            services.AddMvc();
            // Auto Mapper Configurations
            var mapperConfig = new MapperConfiguration(mc =>
            {
                mc.AddProfile(new MappingProfile());
            });

            IMapper mapper = mapperConfig.CreateMapper();
            services.AddSingleton(mapper);

            //Repositories

            # region Repository dependency resolve
            services.AddScoped<ILogExceptionRepository, LogExceptionRepository>();
            services.AddScoped<ICredentialRepository, CredentialRepository>();
            services.AddScoped<ISourcerRepository, SourcerRepository>();
            services.AddScoped<IUserRepository, UserRepository>();
            services.AddScoped<IEmployerRepository, EmployerRepository>();
            services.AddScoped<IEmployerProfileRepository, EmployerProfileRepository>();
            services.AddScoped<ISourcerProfileRepository, SourcerProfileRepository>();
            services.AddScoped<IAdminRepository, AdminRepository>();
            services.AddScoped<IPackagesRepository, PackagesRepository>();
            services.AddScoped<IJobRepository, JobRepository>();
            services.AddScoped<IEmailTemplateRepository, EmailTemplateRepository>();
            services.AddScoped<ICandidateRepository, CandidateRepository>();
            services.AddScoped<IInterviewRepositpry, InterviewRepository>();
            services.AddScoped<IEmployerPackageHistoryRepository, EmployerPackageHistoryRepository>();

            #endregion

            //Services
            # region Services dependency resolve
            services.AddScoped<ILogExceptionService, LogExceptionService>();
            services.AddScoped<ICredentialService, CredentialService>();
            services.AddScoped<ISourcerService, SourcerService>();
            services.AddScoped<IUserService, UserService>();
            services.AddScoped<IEmployerService, EmployerService>();
            services.AddScoped<IEmployerProfileService, EmployerProfileService>();
            services.AddScoped<ISourcerProfileService, SourcerProfileService>();
            services.AddScoped<IAdminService, AdminService>();
            services.AddScoped<IPackagesService, PackagesService>();
            services.AddScoped<IJobService, JobService>();
            services.AddScoped<ICipherService, CipherService>();
            services.AddScoped<IJwtTokenGenerator, JwtTokenGenerator>();
            services.AddScoped<IEmailTemplateService, EmailTemplateService>();
            services.AddScoped<ICandidateService, CandidateService>();
            services.AddScoped<IInterviewService, InterviewService>();
            services.AddScoped<IEmployerPackageHistoryService, EmployerPackageHistoryService>();
            #endregion
            _serviceCollection = services;
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            app.UseMiddleware<ErrorHandlingMiddleware>();

            if (env.IsDevelopment())
            {
                ///app.UseDeveloperExceptionPage();
            }

            app.UseHangfireDashboard("/Admin/jobs", new DashboardOptions()
            {
                Authorization = new[] { new HangFireAuthorizationFilter() }
            });

            app.UseHangfireServer();

            // Enable middleware to serve generated Swagger as a JSON endpoint.
            app.UseSwagger();
            // Enable middleware to serve swagger-ui (HTML, JS, CSS, etc.),changes
            // specifying the Swagger JSON endpoint.
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "Crowd-Recruitment");
            });

            app.UseRouting();
            app.UseCors("CorsPolicy");

            app.UseAuthentication();
            app.UseAuthorization();

            // custom jwt auth middleware
            app.UseMiddleware<JwtMiddleware>();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
                endpoints.MapHangfireDashboard();
            });

            RecurringJob.AddOrUpdate(() => UpdatePackages(), Cron.Minutely());
        }

        public void UpdatePackages()
        {
            IAdminService adminService = _serviceCollection.BuildServiceProvider().GetService<IAdminService>();
            adminService.UpdateEmployerPackages();
        }
    }
}
